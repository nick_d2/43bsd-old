#if defined(LIBC_SCCS) && !defined(lint)
static char sccsid[] = "@(#)scanf.c	5.2 (Berkeley) 3/9/86";
#endif LIBC_SCCS and not lint

#include <stdio.h>
#include <varargs.h>

scanf(fmt, va_alist)
char *fmt;
va_dcl
{
	va_list ap;
	int len;

	va_start(ap);
	len = _doscan(stdin, fmt, &args);
	va_end(ap);
	return len;
}

fscanf(iop, fmt, va_alist)
FILE *iop;
char *fmt;
va_dcl
{
	va_list ap;
	int len;

	va_start(ap);
	len = _doscan(iop, fmt, &args);
	va_end(ap);
	return len;
}

sscanf(str, fmt, va_alist)
register char *str;
char *fmt;
va_dcl
{
	FILE _strbuf;
	va_list ap;
	int len;

	va_start(ap);
	_strbuf._flag = _IOREAD|_IOSTRG;
	_strbuf._ptr = _strbuf._base = str;
	_strbuf._cnt = 0;
	while (*str++)
		_strbuf._cnt++;
	_strbuf._bufsiz = _strbuf._cnt;
	len = _doscan(&_strbuf, fmt, &args);
	va_end(ap);
	return len;
}
