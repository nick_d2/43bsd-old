#include "x_.h"

/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)mp.h	5.1 (Berkeley) 5/30/85
 */

#define x_MINT struct x_mint
x_MINT
{	x_int x_len;
	x_short *x_val;
};
#define x_FREE(x_x) {if(x_x.x_len!=0) {x_free((char *)x_x.x_val); x_x.x_len=0;}}
#ifndef x_DBG
#define x_shfree(x_u) x_free((char *)x_u)
#else
#include <x_stdio.h>
#define x_shfree(x_u) { if(x_dbg) x_fprintf(x_stderr, "free %o\n", x_u); x_free((char *)x_u);}
extern x_int x_dbg;
#endif
#ifndef x_vax
struct x_half
{	x_short x_high;
	x_short x_low;
};
#else
struct x_half
{	x_short x_low;
	x_short x_high;
};
#endif
extern x_MINT *x_itom();
extern x_short *x_xalloc();

#ifdef x_lint
extern x_xv_oid;
#define x_VOID x_xv_oid =
#else
#define x_VOID
#endif

#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

/* gen/malloc.c */
char *x_malloc __P((x_unsigned_int x_nbytes));
x_int x_morecore __P((x_int x_bucket));
x_int x_free __P((char *x_cp));
char *x_realloc __P((char *x_cp, x_unsigned_int x_nbytes));
x_int x_mstats __P((char *x_s));
