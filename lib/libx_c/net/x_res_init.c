#include "x_.h"

#include <x_stdlib.h>
#include <x_strings.h>
#include <x_unistd.h>
#include <arpa/x_inet.h>
/*
 * Copyright (c) 1985 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)res_init.c	6.5 (Berkeley) 4/11/86";
#endif

#include <sys/x_types.h>
#include <sys/x_socket.h>
#include <netinet/x_in.h>
#include <x_stdio.h>
#include <arpa/x_nameser.h>
#include <x_resolv.h>

/*
 * Resolver configuration file. Contains the address of the
 * inital name server to query and the default domain for
 * non fully qualified domain names.
 */

#ifdef x_CONFFILE
char    *x_conffile = x_CONFFILE;
#else
char    *x_conffile = "/etc/resolv.conf";
#endif

/*
 * Resolver state default settings
 */

#ifndef x_RES_TIMEOUT
#define x_RES_TIMEOUT 4
#endif

struct x_state x__res = {
    x_RES_TIMEOUT,                 /* retransmition time interval */
    4,                           /* number of times to retransmit */
    x_RES_RECURSE|x_RES_DEFNAMES,    /* options flags */
    1,                           /* number of name servers */
};

/*
 * Set up default settings.  If the configuration file exist, the values
 * there will have precedence.  Otherwise, the server address is set to
 * INADDR_ANY and the default domain name comes from the gethostname().
 *
 * The configuration file should only be used if you want to redefine your
 * domain or run without a server on your machine.
 *
 * Return 0 if completes successfully, -1 on error
 */
x_int x_res_init() {
    register x_FILE *x_fp;
    char x_buf[x_BUFSIZ], *x_cp;
    extern x_u_long x_inet_addr();
    extern char *x_index();
    extern char *x_strcpy(), *x_strncpy();
#ifdef x_DEBUG
    extern char *x_getenv();
#endif
    x_int x_n = 0;    /* number of nameserver records read from file */

    x__res.x_nsaddr.x_sin_addr.x_s_addr = x_INADDR_ANY;
    x__res.x_nsaddr.x_sin_family = x_AF_INET;
    x__res.x_nsaddr.x_sin_port = x_htons(x_NAMESERVER_PORT);
    x__res.x_nscount = 1;
    x__res.x_defdname[0] = '\0';

    if ((x_fp = x_fopen(x_conffile, "r")) != x_NULL) {
        /* read the config file */
        while (x_fgets(x_buf, sizeof(x_buf), x_fp) != x_NULL) {
            /* read default domain name */
            if (!x_strncmp(x_buf, "domain", sizeof("domain") - 1)) {
                x_cp = x_buf + sizeof("domain") - 1;
                while (*x_cp == ' ' || *x_cp == '\t')
                    x_cp++;
                if (*x_cp == '\0')
                    continue;
                (void)x_strncpy(x__res.x_defdname, x_cp, sizeof(x__res.x_defdname));
                x__res.x_defdname[sizeof(x__res.x_defdname) - 1] = '\0';
                if ((x_cp = x_index(x__res.x_defdname, '\n')) != x_NULL)
                    *x_cp = '\0';
                continue;
            }
            /* read nameservers to query */
            if (!x_strncmp(x_buf, "nameserver", 
               sizeof("nameserver") - 1) && (x_n < x_MAXNS)) {
                x_cp = x_buf + sizeof("nameserver") - 1;
                while (*x_cp == ' ' || *x_cp == '\t')
                    x_cp++;
                if (*x_cp == '\0')
                    continue;
                x__res.x_nsaddr_list[x_n].x_sin_addr.x_s_addr = x_inet_addr(x_cp);
                if (x__res.x_nsaddr_list[x_n].x_sin_addr.x_s_addr == (unsigned)-1) 
                    x__res.x_nsaddr_list[x_n].x_sin_addr.x_s_addr = x_INADDR_ANY;
                x__res.x_nsaddr_list[x_n].x_sin_family = x_AF_INET;
                x__res.x_nsaddr_list[x_n].x_sin_port = x_htons(x_NAMESERVER_PORT);
                if ( ++x_n >= x_MAXNS) { 
                    x_n = x_MAXNS;
#ifdef x_DEBUG
                    if ( x__res.x_options & x_RES_DEBUG )
                        x_printf("MAXNS reached, reading resolv.conf\n");
#endif
                }
                continue;
            }
        }
        if ( x_n > 1 ) 
            x__res.x_nscount = x_n;
        (void) x_fclose(x_fp);
    }
    if (x__res.x_defdname[0] == 0) {
        if (x_gethostname(x_buf, sizeof(x__res.x_defdname)) == 0 &&
           (x_cp = x_index(x_buf, '.')))
             (void)x_strcpy(x__res.x_defdname, x_cp + 1);
    }

#ifdef x_DEBUG
    /* Allow user to override the local domain definition */
    if ((x_cp = x_getenv("LOCALDOMAIN")) != x_NULL)
        (void)x_strncpy(x__res.x_defdname, x_cp, sizeof(x__res.x_defdname));
#endif
    x__res.x_options |= x_RES_INIT;
    return(0);
}
