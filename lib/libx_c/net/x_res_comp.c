#include "x_.h"

/*
 * Copyright (c) 1985 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)res_comp.c	6.7 (Berkeley) 3/11/86";
#endif

#include <sys/x_types.h>
#include <x_stdio.h>
#include <arpa/x_nameser.h>


/*
 * Expand compressed domain name 'comp_dn' to full domain name.
 * 'msg' is a pointer to the begining of the message,
 * 'eomorig' points to the first location after the message,
 * 'exp_dn' is a pointer to a buffer of size 'length' for the result.
 * Return size of compressed name or -1 if there was an error.
 */
x_int x_dn_expand(x_msg, x_eomorig, x_comp_dn, x_exp_dn, x_length) char *x_msg; char *x_eomorig; char *x_comp_dn; char *x_exp_dn; x_int x_length; {
	register char *x_cp, *x_dn;
	register x_int x_n, x_c;
	char *x_eom;
	x_int x_len = -1;

	x_dn = x_exp_dn;
	x_cp = x_comp_dn;
	x_eom = x_exp_dn + x_length - 1;
	/*
	 * fetch next label in domain name
	 */
	while (x_n = *x_cp++) {
		/*
		 * Check for indirection
		 */
		switch (x_n & x_INDIR_MASK) {
		case 0:
			if (x_dn != x_exp_dn) {
				if (x_dn >= x_eom)
					return (-1);
				*x_dn++ = '.';
			}
			if (x_dn+x_n >= x_eom)
				return (-1);
			while (--x_n >= 0) {
				if ((x_c = *x_cp++) == '.') {
					if (x_dn+x_n+1 >= x_eom)
						return (-1);
					*x_dn++ = '\\';
				}
				*x_dn++ = x_c;
				if (x_cp >= x_eomorig)	/* out of range */
					return(-1);
			}
			break;

		case x_INDIR_MASK:
			if (x_len < 0)
				x_len = x_cp - x_comp_dn + 1;
			x_cp = x_msg + (((x_n & 0x3f) << 8) | (*x_cp & 0xff));
			if (x_cp < x_msg || x_cp >= x_eomorig)	/* out of range */
				return(-1);
			break;

		x_default:
			return (-1);			/* flag error */
		}
	}
	*x_dn = '\0';
	if (x_len < 0)
		x_len = x_cp - x_comp_dn;
	return (x_len);
}

/*
 * Compress domain name 'exp_dn' into 'comp_dn'.
 * Return the size of the compressed name or -1.
 * 'length' is the size of the array pointed to by 'comp_dn'.
 * 'dnptrs' is a list of pointers to previous compressed names. dnptrs[0]
 * is a pointer to the beginning of the message. The list ends with NULL.
 * 'lastdnptr' is a pointer to the end of the arrary pointed to
 * by 'dnptrs'. Side effect is to update the list of pointers for
 * labels inserted into the message as we compress the name.
 * If 'dnptr' is NULL, we don't try to compress names. If 'lastdnptr'
 * is NULL, we don't update the list.
 */
x_int x_dn_comp(x_exp_dn, x_comp_dn, x_length, x_dnptrs, x_lastdnptr) char *x_exp_dn; char *x_comp_dn; x_int x_length; char **x_dnptrs; char **x_lastdnptr; {
	register char *x_cp, *x_dn;
	register x_int x_c, x_l;
	char **x_cpp, **x_lpp, *x_sp, *x_eob;
	char *x_msg;

	x_dn = x_exp_dn;
	x_cp = x_comp_dn;
	x_eob = x_cp + x_length;
	if (x_dnptrs != x_NULL) {
		if ((x_msg = *x_dnptrs++) != x_NULL) {
			for (x_cpp = x_dnptrs; *x_cpp != x_NULL; x_cpp++)
				;
			x_lpp = x_cpp;	/* end of list to search */
		}
	} else
		x_msg = x_NULL;
	for (x_c = *x_dn++; x_c != '\0'; ) {
		/* look to see if we can use pointers */
		if (x_msg != x_NULL) {
			if ((x_l = x_dn_find(x_dn-1, x_msg, x_dnptrs, x_lpp)) >= 0) {
				if (x_cp+1 >= x_eob)
					return (-1);
				*x_cp++ = (x_l >> 8) | x_INDIR_MASK;
				*x_cp++ = x_l % 256;
				return (x_cp - x_comp_dn);
			}
			/* not found, save it */
			if (x_lastdnptr != x_NULL && x_cpp < x_lastdnptr-1) {
				*x_cpp++ = x_cp;
				*x_cpp = x_NULL;
			}
		}
		x_sp = x_cp++;	/* save ptr to length byte */
		do {
			if (x_c == '.') {
				x_c = *x_dn++;
				break;
			}
			if (x_c == '\\') {
				if ((x_c = *x_dn++) == '\0')
					break;
			}
			if (x_cp >= x_eob)
				return (-1);
			*x_cp++ = x_c;
		} while ((x_c = *x_dn++) != '\0');
		/* catch trailing '.'s but not '..' */
		if ((x_l = x_cp - x_sp - 1) == 0 && x_c == '\0') {
			x_cp--;
			break;
		}
		if (x_l <= 0 || x_l > x_MAXLABEL)
			return (-1);
		*x_sp = x_l;
	}
	if (x_cp >= x_eob)
		return (-1);
	*x_cp++ = '\0';
	return (x_cp - x_comp_dn);
}

/*
 * Skip over a compressed domain name. Return the size or -1.
 */
x_int x_dn_skip(x_comp_dn) char *x_comp_dn; {
	register char *x_cp;
	register x_int x_n;

	x_cp = x_comp_dn;
	while (x_n = *x_cp++) {
		/*
		 * check for indirection
		 */
		switch (x_n & x_INDIR_MASK) {
		case 0:		/* normal case, n == len */
			x_cp += x_n;
			continue;
		x_default:	/* illegal type */
			return (-1);
		case x_INDIR_MASK:	/* indirection */
			x_cp++;
		}
		break;
	}
	return (x_cp - x_comp_dn);
}

/*
 * Search for expanded name from a list of previously compressed names.
 * Return the offset from msg if found or -1.
 */
x_int x_dn_find(x_exp_dn, x_msg, x_dnptrs, x_lastdnptr) char *x_exp_dn; char *x_msg; char **x_dnptrs; char **x_lastdnptr; {
	register char *x_dn, *x_cp, **x_cpp;
	register x_int x_n;
	char *x_sp;

	for (x_cpp = x_dnptrs + 1; x_cpp < x_lastdnptr; x_cpp++) {
		x_dn = x_exp_dn;
		x_sp = x_cp = *x_cpp;
		while (x_n = *x_cp++) {
			/*
			 * check for indirection
			 */
			switch (x_n & x_INDIR_MASK) {
			case 0:		/* normal case, n == len */
				while (--x_n >= 0) {
					if (*x_dn == '\\')
						x_dn++;
					if (*x_dn++ != *x_cp++)
						goto x_next;
				}
				if ((x_n = *x_dn++) == '\0' && *x_cp == '\0')
					return (x_sp - x_msg);
				if (x_n == '.')
					continue;
				goto x_next;

			x_default:	/* illegal type */
				return (-1);

			case x_INDIR_MASK:	/* indirection */
				x_cp = x_msg + (((x_n & 0x3f) << 8) | (*x_cp & 0xff));
			}
		}
		if (*x_dn == '\0')
			return (x_sp - x_msg);
	x_next:	;
	}
	return (-1);
}

/*
 * Routines to insert/extract short/long's. Must account for byte
 * order and non-alignment problems. This code at least has the
 * advantage of being portable.
 */

x_u_short x_getshort(x_msgp) char *x_msgp; {
	register x_u_char *x_p = (x_u_char *) x_msgp;
#ifdef x_vax
	/*
	 * vax compiler doesn't put shorts in registers
	 */
	register x_int x_u_long x_u;
#else
	register x_int x_u_short x_u;
#endif

	x_u = *x_p++ << 8;
	return ((x_u_short)(x_u | *x_p));
}

x_u_long x_getlong(x_msgp) char *x_msgp; {
	register x_u_char *x_p = (x_u_char *) x_msgp;
	register x_int x_u_long x_u;

	x_u = *x_p++; x_u <<= 8;
	x_u |= *x_p++; x_u <<= 8;
	x_u |= *x_p++; x_u <<= 8;
	return (x_u | *x_p);
}


x_int x_putshort(x_s, x_msgp) x_int x_s; register char *x_msgp; {

	x_msgp[1] = x_s;
	x_msgp[0] = x_s >> 8;
}

x_int x_putlong(x_l, x_msgp) register x_int x_u_long x_l; register char *x_msgp; {

	x_msgp[3] = x_l;
	x_msgp[2] = (x_l >>= 8);
	x_msgp[1] = (x_l >>= 8);
	x_msgp[0] = x_l >> 8;
}
