#include "x_.h"

#include <x_netdb.h>
#include <x_unistd.h>
#include <arpa/x_inet.h>

/*
 * Copyright (c) 1985 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)res_send.c	6.14 (Berkeley) 7/2/86";
#endif

/*
 * Send query to name server and wait for reply.
 */

#include <sys/x_param.h>
#include <sys/x_time.h>
#include <sys/x_socket.h>
#include <sys/x_uio.h>
#include <netinet/x_in.h>
#include <x_stdio.h>
#include <x_errno.h>
#include <arpa/x_nameser.h>
#include <x_resolv.h>

extern x_int x_errno;

static x_int x_s = -1;	/* socket used for communications */

#define x_KEEPOPEN (x_RES_USEVC|x_RES_STAYOPEN)

x_int x_res_send(x_buf, x_buflen, x_answer, x_anslen) char *x_buf; x_int x_buflen; char *x_answer; x_int x_anslen; {
	register x_int x_n;
	x_int x_retry, x_v_circuit, x_resplen, x_ns;
	x_int x_gotsomewhere = 0;
	x_u_short x_id, x_len;
	char *x_cp;
	x_fd_set x_dsmask;
	struct x_timeval x_timeout;
	x_HEADER *x_hp = (x_HEADER *) x_buf;
	x_HEADER *x_anhp = (x_HEADER *) x_answer;
	struct x_iovec x_iov[2];
	x_int x_terrno = x_ETIMEDOUT;

#ifdef x_DEBUG
	if (x__res.x_options & x_RES_DEBUG) {
		x_printf("res_send()\n");
		x_p_query(x_buf);
	}
#endif
	if (!(x__res.x_options & x_RES_INIT))
		if (x_res_init() == -1) {
			return(-1);
		}
	x_v_circuit = (x__res.x_options & x_RES_USEVC) || x_buflen > x_PACKETSZ;
	x_id = x_hp->x_id;
	/*
	 * Send request, RETRY times, or until successful
	 */
	for (x_retry = x__res.x_retry; x_retry > 0; x_retry--) {
	   for (x_ns = 0; x_ns < x__res.x_nscount; x_ns++) {
#ifdef x_DEBUG
		if (x__res.x_options & x_RES_DEBUG)
			x_printf("Querying server (# %d) address = %s\n", x_ns+1,
			      x_inet_ntoa(x__res.x_nsaddr_list[x_ns].x_sin_addr.x_s_addr));
#endif
		if (x_v_circuit) {
			/*
			 * Use virtual circuit.
			 */
			if (x_s < 0) {
				x_s = x_socket(x_AF_INET, x_SOCK_STREAM, 0);
				if (x_s < 0) {
					x_terrno = x_errno;
#ifdef x_DEBUG
					if (x__res.x_options & x_RES_DEBUG)
					    x_perror("socket failed");
#endif
					continue;
				}
				if (x_connect(x_s, &(x__res.x_nsaddr_list[x_ns]),
				   sizeof(struct x_sockaddr)) < 0) {
					x_terrno = x_errno;
#ifdef x_DEBUG
					if (x__res.x_options & x_RES_DEBUG)
					    x_perror("connect failed");
#endif
					(void) x_close(x_s);
					x_s = -1;
					continue;
				}
			}
			/*
			 * Send length & message
			 */
			x_len = x_htons((x_u_short)x_buflen);
			x_iov[0].x_iov_base = (x_caddr_t)&x_len;
			x_iov[0].x_iov_len = sizeof(x_len);
			x_iov[1].x_iov_base = x_buf;
			x_iov[1].x_iov_len = x_buflen;
			if (x_writev(x_s, x_iov, 2) != sizeof(x_len) + x_buflen) {
				x_terrno = x_errno;
#ifdef x_DEBUG
				if (x__res.x_options & x_RES_DEBUG)
					x_perror("write failed");
#endif
				(void) x_close(x_s);
				x_s = -1;
				continue;
			}
			/*
			 * Receive length & response
			 */
			x_cp = x_answer;
			x_len = sizeof(x_short);
			while (x_len != 0 &&
			    (x_n = x_read(x_s, (char *)x_cp, (x_int)x_len)) > 0) {
				x_cp += x_n;
				x_len -= x_n;
			}
			if (x_n <= 0) {
				x_terrno = x_errno;
#ifdef x_DEBUG
				if (x__res.x_options & x_RES_DEBUG)
					x_perror("read failed");
#endif
				(void) x_close(x_s);
				x_s = -1;
				continue;
			}
			x_cp = x_answer;
			x_resplen = x_len = x_ntohs(*(x_u_short *)x_cp);
			while (x_len != 0 &&
			   (x_n = x_read(x_s, (char *)x_cp, (x_int)x_len)) > 0) {
				x_cp += x_n;
				x_len -= x_n;
			}
			if (x_n <= 0) {
				x_terrno = x_errno;
#ifdef x_DEBUG
				if (x__res.x_options & x_RES_DEBUG)
					x_perror("read failed");
#endif
				(void) x_close(x_s);
				x_s = -1;
				continue;
			}
		} else {
			/*
			 * Use datagrams.
			 */
			if (x_s < 0)
				x_s = x_socket(x_AF_INET, x_SOCK_DGRAM, 0);
#if	x_BSD >= 43
			if (x_connect(x_s, &x__res.x_nsaddr_list[x_ns],
			    sizeof(struct x_sockaddr)) < 0 ||
			    x_send(x_s, x_buf, x_buflen, 0) != x_buflen) {
#ifdef x_DEBUG
				if (x__res.x_options & x_RES_DEBUG)
					x_perror("connect");
#endif
				continue;
			}
#else
			if (x_sendto(x_s, x_buf, x_buflen, 0, &x__res.x_nsaddr_list[x_ns],
			    sizeof(struct x_sockaddr)) != x_buflen) {
#ifdef x_DEBUG
				if (x__res.x_options & x_RES_DEBUG)
					x_perror("sendto");
#endif
				continue;
			}
#endif
			/*
			 * Wait for reply
			 */
			x_timeout.x_tv_sec = (x__res.x_retrans << (x__res.x_retry - x_retry))
				/ x__res.x_nscount;
			if (x_timeout.x_tv_sec <= 0)
				x_timeout.x_tv_sec = 1;
			x_timeout.x_tv_usec = 0;
x_wait:
			x_FD_ZERO(&x_dsmask);
			x_FD_SET(x_s, &x_dsmask);
			x_n = x_select(x_s+1, &x_dsmask, (x_fd_set *)x_NULL,
				(x_fd_set *)x_NULL, &x_timeout);
			if (x_n < 0) {
#ifdef x_DEBUG
				if (x__res.x_options & x_RES_DEBUG)
					x_perror("select");
#endif
				continue;
			}
			if (x_n == 0) {
				/*
				 * timeout
				 */
#ifdef x_DEBUG
				if (x__res.x_options & x_RES_DEBUG)
					x_printf("timeout\n");
#endif
				x_gotsomewhere = 1;
				continue;
			}
			if ((x_resplen = x_recv(x_s, x_answer, x_anslen, 0)) <= 0) {
#ifdef x_DEBUG
				if (x__res.x_options & x_RES_DEBUG)
					x_perror("recvfrom");
#endif
				continue;
			}
			x_gotsomewhere = 1;
			if (x_id != x_anhp->x_id) {
				/*
				 * response from old query, ignore it
				 */
#ifdef x_DEBUG
				if (x__res.x_options & x_RES_DEBUG) {
					x_printf("old answer:\n");
					x_p_query(x_answer);
				}
#endif
				goto x_wait;
			}
			if (!(x__res.x_options & x_RES_IGNTC) && x_anhp->x_tc) {
				/*
				 * get rest of answer
				 */
#ifdef x_DEBUG
				if (x__res.x_options & x_RES_DEBUG)
					x_printf("truncated answer\n");
#endif
				(void) x_close(x_s);
				x_s = -1;
				/*
				 * retry decremented on continue
				 * to desired starting value
				 */
				x_retry = x__res.x_retry + 1;
				x_v_circuit = 1;
				continue;
			}
		}
#ifdef x_DEBUG
		if (x__res.x_options & x_RES_DEBUG) {
			x_printf("got answer:\n");
			x_p_query(x_answer);
		}
#endif
		/*
		 * We are going to assume that the first server is preferred
		 * over the rest (i.e. it is on the local machine) and only
		 * keep that one open.
		 */
		if ((x__res.x_options & x_KEEPOPEN) == x_KEEPOPEN && x_ns == 0) {
			return (x_resplen);
		} else {
			(void) x_close(x_s);
			x_s = -1;
			return (x_resplen);
		}
	   }
	}
	if (x_s >= 0) {
		(void) x_close(x_s);
		x_s = -1;
	}
	if (x_v_circuit == 0)
		if (x_gotsomewhere == 0)
			x_errno = x_ECONNREFUSED;
		else
			x_errno = x_ETIMEDOUT;
	else
		x_errno = x_terrno;
	return (-1);
}

/*
 * This routine is for closing the socket if a virtual circuit is used and
 * the program wants to close it.  This provides support for endhostent()
 * which expects to close the socket.
 *
 * This routine is not expected to be user visible.
 */
x_int x__res_close() {
	if (x_s != -1) {
		(void) x_close(x_s);
		x_s = -1;
	}
}
