#include "x_.h"

#include <x_strings.h>
/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)getprotoname.c	5.3 (Berkeley) 5/19/86";
#endif

#include <x_netdb.h>

extern x_int x__proto_stayopen;

struct x_protoent *x_getprotobyname(x_name) register char *x_name; {
	register struct x_protoent *x_p;
	register char **x_cp;

	x_setprotoent(x__proto_stayopen);
	while (x_p = x_getprotoent()) {
		if (x_strcmp(x_p->x_p_name, x_name) == 0)
			break;
		for (x_cp = x_p->x_p_aliases; *x_cp != 0; x_cp++)
			if (x_strcmp(*x_cp, x_name) == 0)
				goto x_found;
	}
x_found:
	if (!x__proto_stayopen)
		x_endprotoent();
	return (x_p);
}
