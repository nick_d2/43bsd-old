#include "x_.h"

/*
 * Copyright (c) 1980, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)route.h	7.1 (Berkeley) 6/4/86
 */

/*
 * Kernel resident routing tables.
 * 
 * The routing tables are initialized when interface addresses
 * are set by making entries for all directly connected interfaces.
 */

/*
 * A route consists of a destination address and a reference
 * to a routing entry.  These are often held by protocols
 * in their control blocks, e.g. inpcb.
 */
struct x_route {
	struct	x_rtentry *x_ro_rt;
	struct	x_sockaddr x_ro_dst;
};

/*
 * We distinguish between routes to hosts and routes to networks,
 * preferring the former if available.  For each route we infer
 * the interface to use from the gateway address supplied when
 * the route was entered.  Routes that forward packets through
 * gateways are marked so that the output routines know to address the
 * gateway rather than the ultimate destination.
 */
struct x_rtentry {
	x_u_long	x_rt_hash;		/* to speed lookups */
	struct	x_sockaddr x_rt_dst;	/* key */
	struct	x_sockaddr x_rt_gateway;	/* value */
	x_short	x_rt_flags;		/* up/down?, host/net */
	x_short	x_rt_refcnt;		/* # held references */
	x_u_long	x_rt_use;			/* raw # packets forwarded */
	struct	x_ifnet *x_rt_ifp;		/* the answer: interface to use */
};

#define	x_RTF_UP		0x1		/* route useable */
#define	x_RTF_GATEWAY	0x2		/* destination is a gateway */
#define	x_RTF_HOST	0x4		/* host entry (net otherwise) */
#define	x_RTF_DYNAMIC	0x10		/* created dynamically (by redirect) */

/*
 * Routing statistics.
 */
struct	x_rtstat {
	x_short	x_rts_badredirect;	/* bogus redirect calls */
	x_short	x_rts_dynamic;		/* routes created by redirects */
	x_short	x_rts_newgateway;		/* routes modified by redirects */
	x_short	x_rts_unreach;		/* lookups which failed */
	x_short	x_rts_wildcard;		/* lookups satisfied by a wildcard */
};

#ifdef x_KERNEL
#define	x_RTFREE(x_rt) \
	if ((x_rt)->x_rt_refcnt == 1) \
		x_rtfree(x_rt); \
	else \
		(x_rt)->x_rt_refcnt--;

#ifdef	x_GATEWAY
#define	x_RTHASHSIZ	64
#else
#define	x_RTHASHSIZ	8
#endif
#if	(x_RTHASHSIZ & (x_RTHASHSIZ - 1)) == 0
#define x_RTHASHMOD(x_h)	((x_h) & (x_RTHASHSIZ - 1))
#else
#define x_RTHASHMOD(x_h)	((x_h) % x_RTHASHSIZ)
#endif
struct	x_mbuf *x_rthost[x_RTHASHSIZ];
struct	x_mbuf *x_rtnet[x_RTHASHSIZ];
struct	x_rtstat	x_rtstat;
#endif
