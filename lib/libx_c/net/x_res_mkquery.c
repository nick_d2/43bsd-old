#include "x_.h"

#include <x_strings.h>
/*
 * Copyright (c) 1985 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)res_mkquery.c	6.3 (Berkeley) 3/17/86";
#endif

#include <x_stdio.h>
#include <sys/x_types.h>
#include <netinet/x_in.h>
#include <arpa/x_nameser.h>
#include <x_resolv.h>

extern	char *x_sprintf();

/*
 * Form all types of queries.
 * Returns the size of the result or -1.
 */
x_int x_res_mkquery(x_op, x_dname, x_class, x_type, x_data, x_datalen, x_newrr, x_buf, x_buflen) x_int x_op;			/* opcode of query */ char *x_dname;		/* domain name */ x_int x_class; x_int x_type;	/* class and type of query */ char *x_data;		/* resource record data */ x_int x_datalen;		/* length of data */ struct x_rrec *x_newrr;	/* new rr for modify or append */ char *x_buf;		/* buffer to put query */ x_int x_buflen;		/* size of buffer */ {
	register x_HEADER *x_hp;
	register char *x_cp;
	register x_int x_n;
	char x_dnbuf[x_MAXDNAME];
	char *x_dnptrs[10], **x_dpp, **x_lastdnptr;
	extern char *x_index();

#ifdef x_DEBUG
	if (x__res.x_options & x_RES_DEBUG)
		x_printf("res_mkquery(%d, %s, %d, %d)\n", x_op, x_dname, x_class, x_type);
#endif
	/*
	 * Initialize header fields.
	 */
	x_hp = (x_HEADER *) x_buf;
	x_hp->x_id = x_htons(++x__res.x_id);
	x_hp->x_opcode = x_op;
	x_hp->x_qr = x_hp->x_aa = x_hp->x_tc = x_hp->x_ra = 0;
	x_hp->x_pr = (x__res.x_options & x_RES_PRIMARY) != 0;
	x_hp->x_rd = (x__res.x_options & x_RES_RECURSE) != 0;
	x_hp->x_rcode = x_NOERROR;
	x_hp->x_qdcount = 0;
	x_hp->x_ancount = 0;
	x_hp->x_nscount = 0;
	x_hp->x_arcount = 0;
	x_cp = x_buf + sizeof(x_HEADER);
	x_buflen -= sizeof(x_HEADER);
	x_dpp = x_dnptrs;
	*x_dpp++ = x_buf;
	*x_dpp++ = x_NULL;
	x_lastdnptr = x_dnptrs + sizeof(x_dnptrs)/sizeof(x_dnptrs[0]);
	/*
	 * If the domain name contains no dots (single label), then
	 * append the default domain name to the one given.
	 */
	if ((x__res.x_options & x_RES_DEFNAMES) && x_dname != 0 && x_dname[0] != '\0' &&
	    x_index(x_dname, '.') == x_NULL) {
		if (!(x__res.x_options & x_RES_INIT))
			if (x_res_init() == -1)
				return(-1);
		if (x__res.x_defdname[0] != '\0')
			x_dname = x_sprintf(x_dnbuf, "%s.%s", x_dname, x__res.x_defdname);
	}
	/*
	 * perform opcode specific processing
	 */
	switch (x_op) {
	case x_QUERY:
	case x_CQUERYM:
	case x_CQUERYU:
		x_buflen -= x_QFIXEDSZ;
		if ((x_n = x_dn_comp(x_dname, x_cp, x_buflen, x_dnptrs, x_lastdnptr)) < 0)
			return (-1);
		x_cp += x_n;
		x_buflen -= x_n;
		x_putshort(x_type, x_cp);
		x_cp += sizeof(x_u_short);
		x_putshort(x_class, x_cp);
		x_cp += sizeof(x_u_short);
		x_hp->x_qdcount = x_htons(1);
		if (x_op == x_QUERY || x_data == x_NULL)
			break;
		/*
		 * Make an additional record for completion domain.
		 */
		x_buflen -= x_RRFIXEDSZ;
		if ((x_n = x_dn_comp(x_data, x_cp, x_buflen, x_dnptrs, x_lastdnptr)) < 0)
			return (-1);
		x_cp += x_n;
		x_buflen -= x_n;
		x_putshort(x_T_NULL, x_cp);
		x_cp += sizeof(x_u_short);
		x_putshort(x_class, x_cp);
		x_cp += sizeof(x_u_short);
		x_putlong(0, x_cp);
		x_cp += sizeof(x_u_long);
		x_putshort(0, x_cp);
		x_cp += sizeof(x_u_short);
		x_hp->x_arcount = x_htons(1);
		break;

	case x_IQUERY:
		/*
		 * Initialize answer section
		 */
		if (x_buflen < 1 + x_RRFIXEDSZ + x_datalen)
			return (-1);
		*x_cp++ = '\0';	/* no domain name */
		x_putshort(x_type, x_cp);
		x_cp += sizeof(x_u_short);
		x_putshort(x_class, x_cp);
		x_cp += sizeof(x_u_short);
		x_putlong(0, x_cp);
		x_cp += sizeof(x_u_long);
		x_putshort(x_datalen, x_cp);
		x_cp += sizeof(x_u_short);
		if (x_datalen) {
			x_bcopy(x_data, x_cp, x_datalen);
			x_cp += x_datalen;
		}
		x_hp->x_ancount = x_htons(1);
		break;

#ifdef x_notdef
	case x_UPDATED:
		/*
		 * Put record to be added or deleted in additional section
		 */
		x_buflen -= x_RRFIXEDSZ + x_datalen;
		if ((x_n = x_dn_comp(x_dname, x_cp, x_buflen, x_NULL, x_NULL)) < 0)
			return (-1);
		x_cp += x_n;
		*((x_u_short *)x_cp) = x_htons(x_type);
		x_cp += sizeof(x_u_short);
		*((x_u_short *)x_cp) = x_htons(x_class);
		x_cp += sizeof(x_u_short);
		*((x_u_long *)x_cp) = 0;
		x_cp += sizeof(x_u_long);
		*((x_u_short *)x_cp) = x_htons(x_datalen);
		x_cp += sizeof(x_u_short);
		if (x_datalen) {
			x_bcopy(x_data, x_cp, x_datalen);
			x_cp += x_datalen;
		}
		break;

	case x_UPDATEM:
		/*
		 * Record to be modified followed by its replacement
		 */
		x_buflen -= x_RRFIXEDSZ + x_datalen;
		if ((x_n = x_dn_comp(x_dname, x_cp, x_buflen, x_dnptrs, x_lastdnptr)) < 0)
			return (-1);
		x_cp += x_n;
		*((x_u_short *)x_cp) = x_htons(x_type);
		x_cp += sizeof(x_u_short);
		*((x_u_short *)x_cp) = x_htons(x_class);
		x_cp += sizeof(x_u_short);
		*((x_u_long *)x_cp) = 0;
		x_cp += sizeof(x_u_long);
		*((x_u_short *)x_cp) = x_htons(x_datalen);
		x_cp += sizeof(x_u_short);
		if (x_datalen) {
			x_bcopy(x_data, x_cp, x_datalen);
			x_cp += x_datalen;
		}

	case x_UPDATEA:
		x_buflen -= x_RRFIXEDSZ + x_newrr->x_r_size;
		if ((x_n = x_dn_comp(x_dname, x_cp, x_buflen, x_dnptrs, x_lastdnptr)) < 0)
			return (-1);
		x_cp += x_n;
		*((x_u_short *)x_cp) = x_htons(x_newrr->x_r_type);
		x_cp += sizeof(x_u_short);
		*((x_u_short *)x_cp) = x_htons(x_newrr->x_r_type);
		x_cp += sizeof(x_u_short);
		*((x_u_long *)x_cp) = x_htonl(x_newrr->x_r_ttl);
		x_cp += sizeof(x_u_long);
		*((x_u_short *)x_cp) = x_htons(x_newrr->x_r_size);
		x_cp += sizeof(x_u_short);
		if (x_newrr->x_r_size) {
			x_bcopy(x_newrr->x_r_data, x_cp, x_newrr->x_r_size);
			x_cp += x_newrr->x_r_size;
		}
		break;
#endif
	}
	return (x_cp - x_buf);
}
