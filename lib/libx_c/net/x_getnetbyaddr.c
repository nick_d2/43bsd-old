#include "x_.h"

/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)getnetbyaddr.c	5.3 (Berkeley) 5/19/86";
#endif

#include <x_netdb.h>

extern x_int x__net_stayopen;

struct x_netent *x_getnetbyaddr(x_net, x_type) register x_int x_net; register x_int x_type; {
	register struct x_netent *x_p;

	x_setnetent(x__net_stayopen);
	while (x_p = x_getnetent())
		if (x_p->x_n_addrtype == x_type && x_p->x_n_net == x_net)
			break;
	if (!x__net_stayopen)
		x_endnetent();
	return (x_p);
}
