#include "x_.h"

/*
 * Copyright (c) 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)if_arp.h	7.1 (Berkeley) 6/4/86
 */

/*
 * Address Resolution Protocol.
 *
 * See RFC 826 for protocol description.  ARP packets are variable
 * in size; the arphdr structure defines the fixed-length portion.
 * Protocol type values are the same as those for 10 Mb/s Ethernet.
 * It is followed by the variable-sized fields ar_sha, arp_spa,
 * arp_tha and arp_tpa in that order, according to the lengths
 * specified.  Field names used correspond to RFC 826.
 */
struct	x_arphdr {
	x_u_short	x_ar_hrd;		/* format of hardware address */
#define x_ARPHRD_ETHER 	1	/* ethernet hardware address */
	x_u_short	x_ar_pro;		/* format of protocol address */
	x_u_char	x_ar_hln;		/* length of hardware address */
	x_u_char	x_ar_pln;		/* length of protocol address */
	x_u_short	x_ar_op;		/* one of: */
#define	x_ARPOP_REQUEST	1	/* request to resolve address */
#define	x_ARPOP_REPLY	2	/* response to previous request */
/*
 * The remaining fields are variable in size,
 * according to the sizes above.
 */
/*	u_char	ar_sha[];	/* sender hardware address */
/*	u_char	ar_spa[];	/* sender protocol address */
/*	u_char	ar_tha[];	/* target hardware address */
/*	u_char	ar_tpa[];	/* target protocol address */
};

/*
 * ARP ioctl request
 */
struct x_arpreq {
	struct	x_sockaddr x_arp_pa;		/* protocol address */
	struct	x_sockaddr x_arp_ha;		/* hardware address */
	x_int	x_arp_flags;			/* flags */
};
/*  arp_flags and at_flags field values */
#define	x_ATF_INUSE	0x01	/* entry in use */
#define x_ATF_COM		0x02	/* completed entry (enaddr valid) */
#define	x_ATF_PERM	0x04	/* permanent entry */
#define	x_ATF_PUBL	0x08	/* publish entry (respond for other host) */
#define	x_ATF_USETRAILERS	0x10	/* has requested trailers */
