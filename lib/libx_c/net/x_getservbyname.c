#include "x_.h"

#include <x_strings.h>
/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)getservbyname.c	5.3 (Berkeley) 5/19/86";
#endif

#include <x_netdb.h>

extern x_int x__serv_stayopen;

struct x_servent *x_getservbyname(x_name, x_proto) char *x_name; char *x_proto; {
	register struct x_servent *x_p;
	register char **x_cp;

	x_setservent(x__serv_stayopen);
	while (x_p = x_getservent()) {
		if (x_strcmp(x_name, x_p->x_s_name) == 0)
			goto x_gotname;
		for (x_cp = x_p->x_s_aliases; *x_cp; x_cp++)
			if (x_strcmp(x_name, *x_cp) == 0)
				goto x_gotname;
		continue;
x_gotname:
		if (x_proto == 0 || x_strcmp(x_p->x_s_proto, x_proto) == 0)
			break;
	}
	if (!x__serv_stayopen)
		x_endservent();
	return (x_p);
}
