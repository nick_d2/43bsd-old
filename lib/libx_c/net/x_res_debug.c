#include "x_.h"

#include <x_resolv.h>
#include <arpa/x_inet.h>
/*
 * Copyright (c) 1985 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)res_debug.c	5.13 (Berkeley) 3/9/86";
#endif

#if defined(x_lint) && !defined(x_DEBUG)
#define x_DEBUG
#endif

#include <sys/x_types.h>
#include <netinet/x_in.h>
#include <x_stdio.h>
#include <arpa/x_nameser.h>

extern char *x_p_cdname(), *x_p_rr(), *x_p_type(), *x_p_class();
extern char *x_inet_ntoa();

char *x_opcodes[] = {
	"QUERY",
	"IQUERY",
	"CQUERYM",
	"CQUERYU",
	"4",
	"5",
	"6",
	"7",
	"8",
	"9",
	"10",
	"UPDATEA",
	"UPDATED",
	"UPDATEM",
	"ZONEINIT",
	"ZONEREF",
};

char *x_rcodes[] = {
	"NOERROR",
	"FORMERR",
	"SERVFAIL",
	"NXDOMAIN",
	"NOTIMP",
	"REFUSED",
	"6",
	"7",
	"8",
	"9",
	"10",
	"11",
	"12",
	"13",
	"14",
	"NOCHANGE",
};

x_int x_p_query(x_msg) char *x_msg; {
#ifdef x_DEBUG
	x_fp_query(x_msg,x_stdout);
#endif
}

/*
 * Print the contents of a query.
 * This is intended to be primarily a debugging routine.
 */
x_int x_fp_query(x_msg, x_file) char *x_msg; x_FILE *x_file; {
#ifdef x_DEBUG
	register char *x_cp;
	register x_HEADER *x_hp;
	register x_int x_n;

	/*
	 * Print header fields.
	 */
	x_hp = (x_HEADER *)x_msg;
	x_cp = x_msg + sizeof(x_HEADER);
	x_fprintf(x_file,"HEADER:\n");
	x_fprintf(x_file,"\topcode = %s", x_opcodes[x_hp->x_opcode]);
	x_fprintf(x_file,", id = %d", x_ntohs(x_hp->x_id));
	x_fprintf(x_file,", rcode = %s\n", x_rcodes[x_hp->x_rcode]);
	x_fprintf(x_file,"\theader flags: ");
	if (x_hp->x_qr)
		x_fprintf(x_file," qr");
	if (x_hp->x_aa)
		x_fprintf(x_file," aa");
	if (x_hp->x_tc)
		x_fprintf(x_file," tc");
	if (x_hp->x_rd)
		x_fprintf(x_file," rd");
	if (x_hp->x_ra)
		x_fprintf(x_file," ra");
	if (x_hp->x_pr)
		x_fprintf(x_file," pr");
	x_fprintf(x_file,"\n\tqdcount = %d", x_ntohs(x_hp->x_qdcount));
	x_fprintf(x_file,", ancount = %d", x_ntohs(x_hp->x_ancount));
	x_fprintf(x_file,", nscount = %d", x_ntohs(x_hp->x_nscount));
	x_fprintf(x_file,", arcount = %d\n\n", x_ntohs(x_hp->x_arcount));
	/*
	 * Print question records.
	 */
	if (x_n = x_ntohs(x_hp->x_qdcount)) {
		x_fprintf(x_file,"QUESTIONS:\n");
		while (--x_n >= 0) {
			x_fprintf(x_file,"\t");
			x_cp = x_p_cdname(x_cp, x_msg, x_file);
			if (x_cp == x_NULL)
				return;
			x_fprintf(x_file,", type = %s", x_p_type(x_getshort(x_cp)));
			x_cp += sizeof(x_u_short);
			x_fprintf(x_file,", class = %s\n\n", x_p_class(x_getshort(x_cp)));
			x_cp += sizeof(x_u_short);
		}
	}
	/*
	 * Print authoritative answer records
	 */
	if (x_n = x_ntohs(x_hp->x_ancount)) {
		x_fprintf(x_file,"ANSWERS:\n");
		while (--x_n >= 0) {
			x_fprintf(x_file,"\t");
			x_cp = x_p_rr(x_cp, x_msg, x_file);
			if (x_cp == x_NULL)
				return;
		}
	}
	/*
	 * print name server records
	 */
	if (x_n = x_ntohs(x_hp->x_nscount)) {
		x_fprintf(x_file,"NAME SERVERS:\n");
		while (--x_n >= 0) {
			x_fprintf(x_file,"\t");
			x_cp = x_p_rr(x_cp, x_msg, x_file);
			if (x_cp == x_NULL)
				return;
		}
	}
	/*
	 * print additional records
	 */
	if (x_n = x_ntohs(x_hp->x_arcount)) {
		x_fprintf(x_file,"ADDITIONAL RECORDS:\n");
		while (--x_n >= 0) {
			x_fprintf(x_file,"\t");
			x_cp = x_p_rr(x_cp, x_msg, x_file);
			if (x_cp == x_NULL)
				return;
		}
	}
#endif
}

char *x_p_cdname(x_cp, x_msg, x_file) char *x_cp; char *x_msg; x_FILE *x_file; {
#ifdef x_DEBUG
	char x_name[x_MAXDNAME];
	x_int x_n;

	if ((x_n = x_dn_expand(x_msg, x_msg + 512, x_cp, x_name, sizeof(x_name))) < 0)
		return (x_NULL);
	if (x_name[0] == '\0') {
		x_name[0] = '.';
		x_name[1] = '\0';
	}
	x_fputs(x_name, x_file);
	return (x_cp + x_n);
#endif
}

/*
 * Print resource record fields in human readable form.
 */
char *x_p_rr(x_cp, x_msg, x_file) char *x_cp; char *x_msg; x_FILE *x_file; {
#ifdef x_DEBUG
	x_int x_type, x_class, x_dlen, x_n, x_c;
	struct x_in_addr x_inaddr;
	char *x_cp1;

	if ((x_cp = x_p_cdname(x_cp, x_msg, x_file)) == x_NULL)
		return (x_NULL);			/* compression error */
	x_fprintf(x_file,"\n\ttype = %s", x_p_type(x_type = x_getshort(x_cp)));
	x_cp += sizeof(x_u_short);
	x_fprintf(x_file,", class = %s", x_p_class(x_class = x_getshort(x_cp)));
	x_cp += sizeof(x_u_short);
	x_fprintf(x_file,", ttl = %u", x_getlong(x_cp));
	x_cp += sizeof(x_u_long);
	x_fprintf(x_file,", dlen = %d\n", x_dlen = x_getshort(x_cp));
	x_cp += sizeof(x_u_short);
	x_cp1 = x_cp;
	/*
	 * Print type specific data, if appropriate
	 */
	switch (x_type) {
	case x_T_A:
		switch (x_class) {
		case x_C_IN:
			x_bcopy(x_cp, (char *)&x_inaddr, sizeof(x_inaddr));
			if (x_dlen == 4) {
				x_fprintf(x_file,"\tinternet address = %s\n",
					x_inet_ntoa(x_inaddr));
				x_cp += x_dlen;
			} else if (x_dlen == 7) {
				x_fprintf(x_file,"\tinternet address = %s",
					x_inet_ntoa(x_inaddr));
				x_fprintf(x_file,", protocol = %d", x_cp[4]);
				x_fprintf(x_file,", port = %d\n",
					(x_cp[5] << 8) + x_cp[6]);
				x_cp += x_dlen;
			}
			break;
		}
		break;
	case x_T_CNAME:
	case x_T_MB:
#ifdef x_OLDRR
	case x_T_MD:
	case x_T_MF:
#endif
	case x_T_MG:
	case x_T_MR:
	case x_T_NS:
	case x_T_PTR:
		x_fprintf(x_file,"\tdomain name = ");
		x_cp = x_p_cdname(x_cp, x_msg, x_file);
		x_fprintf(x_file,"\n");
		break;

	case x_T_HINFO:
		if (x_n = *x_cp++) {
			x_fprintf(x_file,"\tCPU=%.*s\n", x_n, x_cp);
			x_cp += x_n;
		}
		if (x_n = *x_cp++) {
			x_fprintf(x_file,"\tOS=%.*s\n", x_n, x_cp);
			x_cp += x_n;
		}
		break;

	case x_T_SOA:
		x_fprintf(x_file,"\torigin = ");
		x_cp = x_p_cdname(x_cp, x_msg, x_file);
		x_fprintf(x_file,"\n\tmail addr = ");
		x_cp = x_p_cdname(x_cp, x_msg, x_file);
		x_fprintf(x_file,"\n\tserial=%ld", x_getlong(x_cp));
		x_cp += sizeof(x_u_long);
		x_fprintf(x_file,", refresh=%ld", x_getlong(x_cp));
		x_cp += sizeof(x_u_long);
		x_fprintf(x_file,", retry=%ld", x_getlong(x_cp));
		x_cp += sizeof(x_u_long);
		x_fprintf(x_file,", expire=%ld", x_getlong(x_cp));
		x_cp += sizeof(x_u_long);
		x_fprintf(x_file,", min=%ld\n", x_getlong(x_cp));
		x_cp += sizeof(x_u_long);
		break;

	case x_T_MX:
		x_fprintf(x_file,"\tpreference = %ld,",x_getshort(x_cp));
		x_cp += sizeof(x_u_short);
		x_fprintf(x_file," name = ");
		x_cp = x_p_cdname(x_cp, x_msg, x_file);
		break;

	case x_T_MINFO:
		x_fprintf(x_file,"\trequests = ");
		x_cp = x_p_cdname(x_cp, x_msg, x_file);
		x_fprintf(x_file,"\n\terrors = ");
		x_cp = x_p_cdname(x_cp, x_msg, x_file);
		break;

	case x_T_UINFO:
		x_fprintf(x_file,"\t%s\n", x_cp);
		x_cp += x_dlen;
		break;

	case x_T_UID:
	case x_T_GID:
		if (x_dlen == 4) {
			x_fprintf(x_file,"\t%ld\n", x_getlong(x_cp));
			x_cp += sizeof(x_int);
		}
		break;

	case x_T_WKS:
		if (x_dlen < sizeof(x_u_long) + 1)
			break;
		x_bcopy(x_cp, (char *)&x_inaddr, sizeof(x_inaddr));
		x_cp += sizeof(x_u_long);
		x_fprintf(x_file,"\tinternet address = %s, protocol = %d\n\t",
			x_inet_ntoa(x_inaddr), *x_cp++);
		x_n = 0;
		while (x_cp < x_cp1 + x_dlen) {
			x_c = *x_cp++;
			do {
 				if (x_c & 0200)
					x_fprintf(x_file," %d", x_n);
 				x_c <<= 1;
			} while (++x_n & 07);
		}
		x_putc('\n',x_file);
		break;

	x_default:
		x_fprintf(x_file,"\t???\n");
		x_cp += x_dlen;
	}
	if (x_cp != x_cp1 + x_dlen)
		x_fprintf(x_file,"packet size error (%#x != %#x)\n", x_cp, x_cp1+x_dlen);
	x_fprintf(x_file,"\n");
	return (x_cp);
#endif
}

static	char x_nbuf[20];
extern	char *x_sprintf();

/*
 * Return a string for the type
 */
char *x_p_type(x_type) x_int x_type; {
	switch (x_type) {
	case x_T_A:
		return("A");
	case x_T_NS:		/* authoritative server */
		return("NS");
#ifdef x_OLDRR
	case x_T_MD:		/* mail destination */
		return("MD");
	case x_T_MF:		/* mail forwarder */
		return("MF");
#endif
	case x_T_CNAME:		/* connonical name */
		return("CNAME");
	case x_T_SOA:		/* start of authority zone */
		return("SOA");
	case x_T_MB:		/* mailbox domain name */
		return("MB");
	case x_T_MG:		/* mail group member */
		return("MG");
	case x_T_MX:		/* mail routing info */
		return("MX");
	case x_T_MR:		/* mail rename name */
		return("MR");
	case x_T_NULL:		/* null resource record */
		return("NULL");
	case x_T_WKS:		/* well known service */
		return("WKS");
	case x_T_PTR:		/* domain name pointer */
		return("PTR");
	case x_T_HINFO:		/* host information */
		return("HINFO");
	case x_T_MINFO:		/* mailbox information */
		return("MINFO");
	case x_T_AXFR:		/* zone transfer */
		return("AXFR");
	case x_T_MAILB:		/* mail box */
		return("MAILB");
	case x_T_MAILA:		/* mail address */
		return("MAILA");
	case x_T_ANY:		/* matches any type */
		return("ANY");
	case x_T_UINFO:
		return("UINFO");
	case x_T_UID:
		return("UID");
	case x_T_GID:
		return("GID");
	x_default:
		return (x_sprintf(x_nbuf, "%d", x_type));
	}
}

/*
 * Return a mnemonic for class
 */
char *x_p_class(x_class) x_int x_class; {

	switch (x_class) {
	case x_C_IN:		/* internet class */
		return("IN");
	case x_C_ANY:		/* matches any class */
		return("ANY");
	x_default:
		return (x_sprintf(x_nbuf, "%d", x_class));
	}
}
