#include "x_.h"

#include <x_strings.h>
#include <x_unistd.h>
/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)rexec.c	5.2 (Berkeley) 3/9/86";
#endif

#include <sys/x_types.h>
#include <sys/x_socket.h>

#include <netinet/x_in.h>

#include <x_stdio.h>
#include <x_netdb.h>
#include <x_errno.h>

extern	x_errno;
char	*x_index(), *x_sprintf();
x_int	x_rexecoptions;
char	*x_getpass(), *x_getlogin();

x_int x_rexec(x_ahost, x_rport, x_name, x_pass, x_cmd, x_fd2p) char **x_ahost; x_int x_rport; char *x_name; char *x_pass; char *x_cmd; x_int *x_fd2p; {
	x_int x_s, x_timo = 1, x_s3;
	struct x_sockaddr_in x_sin, x_sin2, x_from;
	char x_c;
	x_short x_port;
	struct x_hostent *x_hp;

	x_hp = x_gethostbyname(*x_ahost);
	if (x_hp == 0) {
		x_fprintf(x_stderr, "%s: unknown host\n", *x_ahost);
		return (-1);
	}
	*x_ahost = x_hp->x_h_name;
	x_ruserpass(x_hp->x_h_name, &x_name, &x_pass);
x_retry:
	x_s = x_socket(x_AF_INET, x_SOCK_STREAM, 0);
	if (x_s < 0) {
		x_perror("rexec: socket");
		return (-1);
	}
	x_sin.x_sin_family = x_hp->x_h_addrtype;
	x_sin.x_sin_port = x_rport;
	x_bcopy(x_hp->x_h_addr, (x_caddr_t)&x_sin.x_sin_addr, x_hp->x_h_length);
	if (x_connect(x_s, &x_sin, sizeof(x_sin)) < 0) {
		if (x_errno == x_ECONNREFUSED && x_timo <= 16) {
			(void) x_close(x_s);
			x_sleep(x_timo);
			x_timo *= 2;
			goto x_retry;
		}
		x_perror(x_hp->x_h_name);
		return (-1);
	}
	if (x_fd2p == 0) {
		(void) x_write(x_s, "", 1);
		x_port = 0;
	} else {
		char x_num[8];
		x_int x_s2, x_sin2len;
		
		x_s2 = x_socket(x_AF_INET, x_SOCK_STREAM, 0);
		if (x_s2 < 0) {
			(void) x_close(x_s);
			return (-1);
		}
		x_listen(x_s2, 1);
		x_sin2len = sizeof (x_sin2);
		if (x_getsockname(x_s2, (char *)&x_sin2, &x_sin2len) < 0 ||
		  x_sin2len != sizeof (x_sin2)) {
			x_perror("getsockname");
			(void) x_close(x_s2);
			goto x_bad;
		}
		x_port = x_ntohs((x_u_short)x_sin2.x_sin_port);
		(void) x_sprintf(x_num, "%d", x_port);
		(void) x_write(x_s, x_num, x_strlen(x_num)+1);
		{ x_int x_len = sizeof (x_from);
		  x_s3 = x_accept(x_s2, &x_from, &x_len, 0);
		  x_close(x_s2);
		  if (x_s3 < 0) {
			x_perror("accept");
			x_port = 0;
			goto x_bad;
		  }
		}
		*x_fd2p = x_s3;
	}
	(void) x_write(x_s, x_name, x_strlen(x_name) + 1);
	/* should public key encypt the password here */
	(void) x_write(x_s, x_pass, x_strlen(x_pass) + 1);
	(void) x_write(x_s, x_cmd, x_strlen(x_cmd) + 1);
	if (x_read(x_s, &x_c, 1) != 1) {
		x_perror(*x_ahost);
		goto x_bad;
	}
	if (x_c != 0) {
		while (x_read(x_s, &x_c, 1) == 1) {
			(void) x_write(2, &x_c, 1);
			if (x_c == '\n')
				break;
		}
		goto x_bad;
	}
	return (x_s);
x_bad:
	if (x_port)
		(void) x_close(*x_fd2p);
	(void) x_close(x_s);
	return (-1);
}
