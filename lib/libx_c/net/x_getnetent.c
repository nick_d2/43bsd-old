#include "x_.h"

#include <arpa/x_inet.h>
/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)getnetent.c	5.3 (Berkeley) 5/19/86";
#endif

#include <x_stdio.h>
#include <sys/x_types.h>
#include <sys/x_socket.h>
#include <x_netdb.h>
#include <x_ctype.h>

#define	x_MAXALIASES	35

static char x_NETDB[] = "/etc/networks";
static x_FILE *x_netf = x_NULL;
static char x_line[x_BUFSIZ+1];
static struct x_netent x_net;
static char *x_net_aliases[x_MAXALIASES];
x_int x__net_stayopen;
static char *x_any();

#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

static char *x_any __P((register char *x_cp, char *x_match));

x_int x_setnetent(x_f) x_int x_f; {
	if (x_netf == x_NULL)
		x_netf = x_fopen(x_NETDB, "r" );
	else
		x_rewind(x_netf);
	x__net_stayopen |= x_f;
}

x_int x_endnetent() {
	if (x_netf) {
		x_fclose(x_netf);
		x_netf = x_NULL;
	}
	x__net_stayopen = 0;
}

struct x_netent *x_getnetent() {
	char *x_p;
	register char *x_cp, **x_q;

	if (x_netf == x_NULL && (x_netf = x_fopen(x_NETDB, "r" )) == x_NULL)
		return (x_NULL);
x_again:
	x_p = x_fgets(x_line, x_BUFSIZ, x_netf);
	if (x_p == x_NULL)
		return (x_NULL);
	if (*x_p == '#')
		goto x_again;
	x_cp = x_any(x_p, "#\n");
	if (x_cp == x_NULL)
		goto x_again;
	*x_cp = '\0';
	x_net.x_n_name = x_p;
	x_cp = x_any(x_p, " \t");
	if (x_cp == x_NULL)
		goto x_again;
	*x_cp++ = '\0';
	while (*x_cp == ' ' || *x_cp == '\t')
		x_cp++;
	x_p = x_any(x_cp, " \t");
	if (x_p != x_NULL)
		*x_p++ = '\0';
	x_net.x_n_net = x_inet_network(x_cp);
	x_net.x_n_addrtype = x_AF_INET;
	x_q = x_net.x_n_aliases = x_net_aliases;
	if (x_p != x_NULL) 
		x_cp = x_p;
	while (x_cp && *x_cp) {
		if (*x_cp == ' ' || *x_cp == '\t') {
			x_cp++;
			continue;
		}
		if (x_q < &x_net_aliases[x_MAXALIASES - 1])
			*x_q++ = x_cp;
		x_cp = x_any(x_cp, " \t");
		if (x_cp != x_NULL)
			*x_cp++ = '\0';
	}
	*x_q = x_NULL;
	return (&x_net);
}

static char *x_any(x_cp, x_match) register char *x_cp; char *x_match; {
	register char *x_mp, x_c;

	while (x_c = *x_cp) {
		for (x_mp = x_match; *x_mp; x_mp++)
			if (*x_mp == x_c)
				return (x_cp);
		x_cp++;
	}
	return ((char *)0);
}
