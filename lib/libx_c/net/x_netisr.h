#include "x_.h"

/*
 * Copyright (c) 1980, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)netisr.h	7.1 (Berkeley) 6/4/86
 */

/*
 * The networking code runs off software interrupts.
 *
 * You can switch into the network by doing splnet() and return by splx().
 * The software interrupt level for the network is higher than the software
 * level for the clock (so you can enter the network in routines called
 * at timeout time).
 */
#ifdef x_vax
#define	x_setsoftnet()	x_mtpr(x_SIRR, 12)
#endif

/*
 * Each ``pup-level-1'' input queue has a bit in a ``netisr'' status
 * word which is used to de-multiplex a single software
 * interrupt used for scheduling the network code to calls
 * on the lowest level routine of each protocol.
 */
#define	x_NETISR_RAW	0		/* same as AF_UNSPEC */
#define	x_NETISR_IP	2		/* same as AF_INET */
#define	x_NETISR_IMP	3		/* same as AF_IMPLINK */
#define	x_NETISR_NS	6		/* same as AF_NS */

#define	x_schednetisr(x_anisr)	{ x_netisr |= 1<<(x_anisr); x_setsoftnet(); }

#ifndef x_LOCORE
#ifdef x_KERNEL
x_int	x_netisr;				/* scheduling bits for network */
#endif
#endif
