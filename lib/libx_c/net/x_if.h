#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)if.h	7.1 (Berkeley) 6/4/86
 */

/*
 * Structures defining a network interface, providing a packet
 * transport mechanism (ala level 0 of the PUP protocols).
 *
 * Each interface accepts output datagrams of a specified maximum
 * length, and provides higher level routines with input datagrams
 * received from its medium.
 *
 * Output occurs when the routine if_output is called, with three parameters:
 *	(*ifp->if_output)(ifp, m, dst)
 * Here m is the mbuf chain to be sent and dst is the destination address.
 * The output routine encapsulates the supplied datagram if necessary,
 * and then transmits it on its medium.
 *
 * On input, each interface unwraps the data received by it, and either
 * places it on the input queue of a internetwork datagram routine
 * and posts the associated software interrupt, or passes the datagram to a raw
 * packet input routine.
 *
 * Routines exist for locating interfaces by their addresses
 * or for locating a interface on a certain network, as well as more general
 * routing and gateway routines maintaining information used to locate
 * interfaces.  These routines live in the files if.c and route.c
 */

/*
 * Structure defining a queue for a network interface.
 *
 * (Would like to call this struct ``if'', but C isn't PL/1.)
 */
struct x_ifnet {
	char	*x_if_name;		/* name, e.g. ``en'' or ``lo'' */
	x_short	x_if_unit;		/* sub-unit for lower level driver */
	x_short	x_if_mtu;			/* maximum transmission unit */
	x_short	x_if_flags;		/* up/down, broadcast, etc. */
	x_short	x_if_timer;		/* time 'til if_watchdog called */
	x_int	x_if_metric;		/* routing metric (external only) */
	struct	x_ifaddr *x_if_addrlist;	/* linked list of addresses per if */
	struct	x_ifqueue {
		struct	x_mbuf *x_ifq_head;
		struct	x_mbuf *x_ifq_tail;
		x_int	x_ifq_len;
		x_int	x_ifq_maxlen;
		x_int	x_ifq_drops;
	} x_if_snd;			/* output queue */
/* procedure handles */
	x_int	(*x_if_init)();		/* init routine */
	x_int	(*x_if_output)();		/* output routine */
	x_int	(*x_if_ioctl)();		/* ioctl routine */
	x_int	(*x_if_reset)();		/* bus reset routine */
	x_int	(*x_if_watchdog)();	/* timer routine */
/* generic interface statistics */
	x_int	x_if_ipackets;		/* packets received on interface */
	x_int	x_if_ierrors;		/* input errors on interface */
	x_int	x_if_opackets;		/* packets sent on interface */
	x_int	x_if_oerrors;		/* output errors on interface */
	x_int	x_if_collisions;		/* collisions on csma interfaces */
/* end statistics */
	struct	x_ifnet *x_if_next;
};

#define	x_IFF_UP		0x1		/* interface is up */
#define	x_IFF_BROADCAST	0x2		/* broadcast address valid */
#define	x_IFF_DEBUG	0x4		/* turn on debugging */
#define	x_IFF_LOOPBACK	0x8		/* is a loopback net */
#define	x_IFF_POINTOPOINT	0x10		/* interface is point-to-point link */
#define	x_IFF_NOTRAILERS	0x20		/* avoid use of trailers */
#define	x_IFF_RUNNING	0x40		/* resources allocated */
#define	x_IFF_NOARP	0x80		/* no address resolution protocol */
/* next two not supported now, but reserved: */
#define	x_IFF_PROMISC	0x100		/* receive all packets */
#define	x_IFF_ALLMULTI	0x200		/* receive all multicast packets */
/* flags set internally only: */
#define	x_IFF_CANTCHANGE	(x_IFF_BROADCAST | x_IFF_POINTOPOINT | x_IFF_RUNNING)

/*
 * Output queues (ifp->if_snd) and internetwork datagram level (pup level 1)
 * input routines have queues of messages stored on ifqueue structures
 * (defined above).  Entries are added to and deleted from these structures
 * by these macros, which should be called with ipl raised to splimp().
 */
#define	x_IF_QFULL(x_ifq)		((x_ifq)->x_ifq_len >= (x_ifq)->x_ifq_maxlen)
#define	x_IF_DROP(x_ifq)		((x_ifq)->x_ifq_drops++)
#define	x_IF_ENQUEUE(x_ifq, x_m) { \
	(x_m)->x_m_act = 0; \
	if ((x_ifq)->x_ifq_tail == 0) \
		(x_ifq)->x_ifq_head = x_m; \
	else \
		(x_ifq)->x_ifq_tail->x_m_act = x_m; \
	(x_ifq)->x_ifq_tail = x_m; \
	(x_ifq)->x_ifq_len++; \
}
#define	x_IF_PREPEND(x_ifq, x_m) { \
	(x_m)->x_m_act = (x_ifq)->x_ifq_head; \
	if ((x_ifq)->x_ifq_tail == 0) \
		(x_ifq)->x_ifq_tail = (x_m); \
	(x_ifq)->x_ifq_head = (x_m); \
	(x_ifq)->x_ifq_len++; \
}
/*
 * Packets destined for level-1 protocol input routines
 * have a pointer to the receiving interface prepended to the data.
 * IF_DEQUEUEIF extracts and returns this pointer when dequeueing the packet.
 * IF_ADJ should be used otherwise to adjust for its presence.
 */
#define	x_IF_ADJ(x_m) { \
	(x_m)->x_m_off += sizeof(struct x_ifnet *); \
	(x_m)->x_m_len -= sizeof(struct x_ifnet *); \
	if ((x_m)->x_m_len == 0) { \
		struct x_mbuf *x_n; \
		x_MFREE((x_m), x_n); \
		(x_m) = x_n; \
	} \
}
#define	x_IF_DEQUEUEIF(x_ifq, x_m, x_ifp) { \
	(x_m) = (x_ifq)->x_ifq_head; \
	if (x_m) { \
		if (((x_ifq)->x_ifq_head = (x_m)->x_m_act) == 0) \
			(x_ifq)->x_ifq_tail = 0; \
		(x_m)->x_m_act = 0; \
		(x_ifq)->x_ifq_len--; \
		(x_ifp) = *(x_mtod((x_m), struct x_ifnet **)); \
		x_IF_ADJ(x_m); \
	} \
}
#define	x_IF_DEQUEUE(x_ifq, x_m) { \
	(x_m) = (x_ifq)->x_ifq_head; \
	if (x_m) { \
		if (((x_ifq)->x_ifq_head = (x_m)->x_m_act) == 0) \
			(x_ifq)->x_ifq_tail = 0; \
		(x_m)->x_m_act = 0; \
		(x_ifq)->x_ifq_len--; \
	} \
}

#define	x_IFQ_MAXLEN	50
#define	x_IFNET_SLOWHZ	1		/* granularity is 1 second */

/*
 * The ifaddr structure contains information about one address
 * of an interface.  They are maintained by the different address families,
 * are allocated and attached when an address is set, and are linked
 * together so all addresses for an interface can be located.
 */
struct x_ifaddr {
	struct	x_sockaddr x_ifa_addr;	/* address of interface */
	union {
		struct	x_sockaddr x_ifu_broadaddr;
		struct	x_sockaddr x_ifu_dstaddr;
	} x_ifa_ifu;
#define	x_ifa_broadaddr	x_ifa_ifu.x_ifu_broadaddr	/* broadcast address */
#define	x_ifa_dstaddr	x_ifa_ifu.x_ifu_dstaddr	/* other end of p-to-p link */
	struct	x_ifnet *x_ifa_ifp;		/* back-pointer to interface */
	struct	x_ifaddr *x_ifa_next;	/* next address for interface */
};

/*
 * Interface request structure used for socket
 * ioctl's.  All interface ioctl's must have parameter
 * definitions which begin with ifr_name.  The
 * remainder may be interface specific.
 */
struct	x_ifreq {
#define	x_IFNAMSIZ	16
	char	x_ifr_name[x_IFNAMSIZ];		/* if name, e.g. "en0" */
	union {
		struct	x_sockaddr x_ifru_addr;
		struct	x_sockaddr x_ifru_dstaddr;
		struct	x_sockaddr x_ifru_broadaddr;
		x_short	x_ifru_flags;
		x_int	x_ifru_metric;
		x_caddr_t	x_ifru_data;
	} x_ifr_ifru;
#define	x_ifr_addr	x_ifr_ifru.x_ifru_addr	/* address */
#define	x_ifr_dstaddr	x_ifr_ifru.x_ifru_dstaddr	/* other end of p-to-p link */
#define	x_ifr_broadaddr	x_ifr_ifru.x_ifru_broadaddr	/* broadcast address */
#define	x_ifr_flags	x_ifr_ifru.x_ifru_flags	/* flags */
#define	x_ifr_metric	x_ifr_ifru.x_ifru_metric	/* metric */
#define	x_ifr_data	x_ifr_ifru.x_ifru_data	/* for use by interface */
};

/*
 * Structure used in SIOCGIFCONF request.
 * Used to retrieve interface configuration
 * for machine (useful for programs which
 * must know all networks accessible).
 */
struct	x_ifconf {
	x_int	x_ifc_len;		/* size of associated buffer */
	union {
		x_caddr_t	x_ifcu_buf;
		struct	x_ifreq *x_ifcu_req;
	} x_ifc_ifcu;
#define	x_ifc_buf	x_ifc_ifcu.x_ifcu_buf	/* buffer address */
#define	x_ifc_req	x_ifc_ifcu.x_ifcu_req	/* array of structures returned */
};

#ifdef x_KERNEL
#include "../net/x_if_arp.h"
struct	x_ifqueue x_rawintrq;		/* raw packet input queue */
struct	x_ifnet *x_ifnet;
struct	x_ifaddr *x_ifa_ifwithaddr(), *x_ifa_ifwithnet();
struct	x_ifaddr *x_ifa_ifwithdstaddr();
#else
#include <net/x_if_arp.h>
#endif
