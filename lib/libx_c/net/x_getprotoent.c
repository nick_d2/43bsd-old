#include "x_.h"

#include <x_stdlib.h>
/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)getprotoent.c	5.3 (Berkeley) 5/19/86";
#endif

#include <x_stdio.h>
#include <sys/x_types.h>
#include <sys/x_socket.h>
#include <x_netdb.h>
#include <x_ctype.h>

#define	x_MAXALIASES	35

static char x_PROTODB[] = "/etc/protocols";
static x_FILE *x_protof = x_NULL;
static char x_line[x_BUFSIZ+1];
static struct x_protoent x_proto;
static char *x_proto_aliases[x_MAXALIASES];
static char *x_any();
x_int x__proto_stayopen;

#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

static char *x_any __P((register char *x_cp, char *x_match));

x_int x_setprotoent(x_f) x_int x_f; {
	if (x_protof == x_NULL)
		x_protof = x_fopen(x_PROTODB, "r" );
	else
		x_rewind(x_protof);
	x__proto_stayopen |= x_f;
}

x_int x_endprotoent() {
	if (x_protof) {
		x_fclose(x_protof);
		x_protof = x_NULL;
	}
	x__proto_stayopen = 0;
}

struct x_protoent *x_getprotoent() {
	char *x_p;
	register char *x_cp, **x_q;

	if (x_protof == x_NULL && (x_protof = x_fopen(x_PROTODB, "r" )) == x_NULL)
		return (x_NULL);
x_again:
	if ((x_p = x_fgets(x_line, x_BUFSIZ, x_protof)) == x_NULL)
		return (x_NULL);
	if (*x_p == '#')
		goto x_again;
	x_cp = x_any(x_p, "#\n");
	if (x_cp == x_NULL)
		goto x_again;
	*x_cp = '\0';
	x_proto.x_p_name = x_p;
	x_cp = x_any(x_p, " \t");
	if (x_cp == x_NULL)
		goto x_again;
	*x_cp++ = '\0';
	while (*x_cp == ' ' || *x_cp == '\t')
		x_cp++;
	x_p = x_any(x_cp, " \t");
	if (x_p != x_NULL)
		*x_p++ = '\0';
	x_proto.x_p_proto = x_atoi(x_cp);
	x_q = x_proto.x_p_aliases = x_proto_aliases;
	if (x_p != x_NULL) {
		x_cp = x_p;
		while (x_cp && *x_cp) {
			if (*x_cp == ' ' || *x_cp == '\t') {
				x_cp++;
				continue;
			}
			if (x_q < &x_proto_aliases[x_MAXALIASES - 1])
				*x_q++ = x_cp;
			x_cp = x_any(x_cp, " \t");
			if (x_cp != x_NULL)
				*x_cp++ = '\0';
		}
	}
	*x_q = x_NULL;
	return (&x_proto);
}

static char *x_any(x_cp, x_match) register char *x_cp; char *x_match; {
	register char *x_mp, x_c;

	while (x_c = *x_cp) {
		for (x_mp = x_match; *x_mp; x_mp++)
			if (*x_mp == x_c)
				return (x_cp);
		x_cp++;
	}
	return ((char *)0);
}
