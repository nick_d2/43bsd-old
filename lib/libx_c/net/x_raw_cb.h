#include "x_.h"

/*
 * Copyright (c) 1980, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)raw_cb.h	7.1 (Berkeley) 6/4/86
 */

/*
 * Raw protocol interface control block.  Used
 * to tie a socket to the generic raw interface.
 */
struct x_rawcb {
	struct	x_rawcb *x_rcb_next;	/* doubly linked list */
	struct	x_rawcb *x_rcb_prev;
	struct	x_socket *x_rcb_socket;	/* back pointer to socket */
	struct	x_sockaddr x_rcb_faddr;	/* destination address */
	struct	x_sockaddr x_rcb_laddr;	/* socket's address */
	struct	x_sockproto x_rcb_proto;	/* protocol family, protocol */
	x_caddr_t	x_rcb_pcb;		/* protocol specific stuff */
	struct	x_mbuf *x_rcb_options;	/* protocol specific options */
	struct	x_route x_rcb_route;	/* routing information */
	x_short	x_rcb_flags;
};

/*
 * Since we can't interpret canonical addresses,
 * we mark an address present in the flags field.
 */
#define	x_RAW_LADDR	01
#define	x_RAW_FADDR	02
#define	x_RAW_DONTROUTE	04		/* no routing, default */

#define	x_sotorawcb(x_so)		((struct x_rawcb *)(x_so)->x_so_pcb)

/*
 * Nominal space allocated to a raw socket.
 */
#define	x_RAWSNDQ		2048
#define	x_RAWRCVQ		2048

/*
 * Format of raw interface header prepended by
 * raw_input after call from protocol specific
 * input routine.
 */
struct x_raw_header {
	struct	x_sockproto x_raw_proto;	/* format of packet */
	struct	x_sockaddr x_raw_dst;	/* dst address for rawintr */
	struct	x_sockaddr x_raw_src;	/* src address for sbappendaddr */
};

#ifdef x_KERNEL
struct x_rawcb x_rawcb;			/* head of list */
#endif
