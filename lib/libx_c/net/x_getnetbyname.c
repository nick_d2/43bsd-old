#include "x_.h"

#include <x_strings.h>
/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)getnetbyname.c	5.3 (Berkeley) 5/19/86";
#endif

#include <x_netdb.h>

extern x_int x__net_stayopen;

struct x_netent *x_getnetbyname(x_name) register char *x_name; {
	register struct x_netent *x_p;
	register char **x_cp;

	x_setnetent(x__net_stayopen);
	while (x_p = x_getnetent()) {
		if (x_strcmp(x_p->x_n_name, x_name) == 0)
			break;
		for (x_cp = x_p->x_n_aliases; *x_cp != 0; x_cp++)
			if (x_strcmp(*x_cp, x_name) == 0)
				goto x_found;
	}
x_found:
	if (!x__net_stayopen)
		x_endnetent();
	return (x_p);
}
