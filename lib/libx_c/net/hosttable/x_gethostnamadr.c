#include "x_.h"

#include <x_strings.h>
/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)gethostnamadr.c	5.5 (Berkeley) 3/9/86";
#endif

#include <x_stdio.h>
#include <x_netdb.h>
#include <sys/x_file.h>
#include <x_ndbm.h>
#include <x_ctype.h>

#define	x_MAXALIASES	35

static struct x_hostent x_host;
static char *x_host_aliases[x_MAXALIASES];
static char x_hostbuf[x_BUFSIZ+1];
static char *x_host_addrs[2];

x_int x_h_errno;

/*
 * The following is shared with gethostent.c
 */
extern	char *x__host_file;
x_DBM	*x__host_db = (x_DBM *)x_NULL;
x_int	x__host_stayopen;	/* set by sethostent(), cleared by endhostent() */

#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

static struct x_hostent *x_fetchhost __P((x_datum x_key));

static struct x_hostent *x_fetchhost(x_key) x_datum x_key; {
        register char *x_cp, *x_tp, **x_ap;
	x_int x_naliases;

        if (x_key.x_dptr == 0)
                return ((struct x_hostent *)x_NULL);
	x_key = x_dbm_fetch(x__host_db, x_key);
	if (x_key.x_dptr == 0)
                return ((struct x_hostent *)x_NULL);
        x_cp = x_key.x_dptr;
	x_tp = x_hostbuf;
	x_host.x_h_name = x_tp;
	while (*x_tp++ = *x_cp++)
		;
	x_bcopy(x_cp, (char *)&x_naliases, sizeof(x_int)); x_cp += sizeof (x_int);
	for (x_ap = x_host_aliases; x_naliases > 0; x_naliases--) {
		*x_ap++ = x_tp;
		while (*x_tp++ = *x_cp++)
			;
	}
	*x_ap = (char *)x_NULL;
	x_host.x_h_aliases = x_host_aliases;
	x_bcopy(x_cp, (char *)&x_host.x_h_addrtype, sizeof (x_int));
	x_cp += sizeof (x_int);
	x_bcopy(x_cp, (char *)&x_host.x_h_length, sizeof (x_int));
	x_cp += sizeof (x_int);
	x_host.x_h_addr_list = x_host_addrs;
	x_host.x_h_addr = x_tp;
	x_bcopy(x_cp, x_tp, x_host.x_h_length);
        return (&x_host);
}

struct x_hostent *x_gethostbyname(x_nam) register char *x_nam; {
	register struct x_hostent *x_hp;
	register char **x_cp;
        x_datum x_key;
	char x_lowname[128];
	register char *x_lp = x_lowname;
	
	while (*x_nam)
		if (x_isupper(*x_nam))
			*x_lp++ = x_tolower(*x_nam++);
		else
			*x_lp++ = *x_nam++;
	*x_lp = '\0';

	if ((x__host_db == (x_DBM *)x_NULL)
	  && ((x__host_db = x_dbm_open(x__host_file, x_O_RDONLY)) == (x_DBM *)x_NULL)) {
		x_sethostent(x__host_stayopen);
		while (x_hp = x_gethostent()) {
			if (x_strcmp(x_hp->x_h_name, x_lowname) == 0)
				break;
			for (x_cp = x_hp->x_h_aliases; x_cp != 0 && *x_cp != 0; x_cp++)
				if (x_strcmp(*x_cp, x_lowname) == 0)
					goto x_found;
		}
	x_found:
		if (!x__host_stayopen)
			x_endhostent();
		return (x_hp);
	}
        x_key.x_dptr = x_lowname;
        x_key.x_dsize = x_strlen(x_lowname);
	x_hp = x_fetchhost(x_key);
	if (!x__host_stayopen) {
		x_dbm_close(x__host_db);
		x__host_db = (x_DBM *)x_NULL;
	}
	if ( x_hp == x_NULL)
		x_h_errno = x_HOST_NOT_FOUND;
        return (x_hp);
}

struct x_hostent *x_gethostbyaddr(x_addr, x_length, x_type) char *x_addr; register x_int x_length; register x_int x_type; {
	register struct x_hostent *x_hp;
        x_datum x_key;

	if ((x__host_db == (x_DBM *)x_NULL)
	  && ((x__host_db = x_dbm_open(x__host_file, x_O_RDONLY)) == (x_DBM *)x_NULL)) {
		x_sethostent(x__host_stayopen);
		while (x_hp = x_gethostent()) {
			if (x_hp->x_h_addrtype == x_type && x_hp->x_h_length == x_length
			    && x_bcmp(x_hp->x_h_addr, x_addr, x_length) == 0)
				break;
		}
		if (!x__host_stayopen)
			x_endhostent();
		if ( x_hp == x_NULL)
			x_h_errno = x_HOST_NOT_FOUND;
		return (x_hp);
	}
        x_key.x_dptr = x_addr;
        x_key.x_dsize = x_length;
	x_hp = x_fetchhost(x_key);
	if (!x__host_stayopen) {
		x_dbm_close(x__host_db);
		x__host_db = (x_DBM *)x_NULL;
	}
	if ( x_hp == x_NULL)
		x_h_errno = x_HOST_NOT_FOUND;
        return (x_hp);
}
