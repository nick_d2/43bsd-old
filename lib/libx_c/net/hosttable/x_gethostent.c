#include "x_.h"

#include <arpa/x_inet.h>
/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)gethostent.c	5.3 (Berkeley) 3/9/86";
#endif

#include <x_stdio.h>
#include <sys/x_types.h>
#include <sys/x_socket.h>
#include <x_netdb.h>
#include <x_ctype.h>
#include <x_ndbm.h>

/*
 * Internet version.
 */
#define	x_MAXALIASES	35
#define	x_MAXADDRSIZE	14

static x_FILE *x_hostf = x_NULL;
static char x_line[x_BUFSIZ+1];
static char x_hostaddr[x_MAXADDRSIZE];
static struct x_hostent x_host;
static char *x_host_aliases[x_MAXALIASES];
static char *x_host_addrs[] = {
	x_hostaddr,
	x_NULL
};

/*
 * The following is shared with gethostnamadr.c
 */
char	*x__host_file = "/etc/hosts";
x_int	x__host_stayopen;
x_DBM	*x__host_db;	/* set by gethostbyname(), gethostbyaddr() */

static char *x_any();

#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

static char *x_any __P((register char *x_cp, char *x_match));

x_int x_sethostent(x_f) x_int x_f; {
	if (x_hostf != x_NULL)
		x_rewind(x_hostf);
	x__host_stayopen |= x_f;
}

x_int x_endhostent() {
	if (x_hostf) {
		x_fclose(x_hostf);
		x_hostf = x_NULL;
	}
	if (x__host_db) {
		x_dbm_close(x__host_db);
		x__host_db = (x_DBM *)x_NULL;
	}
	x__host_stayopen = 0;
}

struct x_hostent *x_gethostent() {
	char *x_p;
	register char *x_cp, **x_q;

	if (x_hostf == x_NULL && (x_hostf = x_fopen(x__host_file, "r" )) == x_NULL)
		return (x_NULL);
x_again:
	if ((x_p = x_fgets(x_line, x_BUFSIZ, x_hostf)) == x_NULL)
		return (x_NULL);
	if (*x_p == '#')
		goto x_again;
	x_cp = x_any(x_p, "#\n");
	if (x_cp == x_NULL)
		goto x_again;
	*x_cp = '\0';
	x_cp = x_any(x_p, " \t");
	if (x_cp == x_NULL)
		goto x_again;
	*x_cp++ = '\0';
	/* THIS STUFF IS INTERNET SPECIFIC */
	x_host.x_h_addr_list = x_host_addrs;
	*((x_u_long *)x_host.x_h_addr) = x_inet_addr(x_p);
	x_host.x_h_length = sizeof (x_u_long);
	x_host.x_h_addrtype = x_AF_INET;
	while (*x_cp == ' ' || *x_cp == '\t')
		x_cp++;
	x_host.x_h_name = x_cp;
	x_q = x_host.x_h_aliases = x_host_aliases;
	x_cp = x_any(x_cp, " \t");
	if (x_cp != x_NULL) 
		*x_cp++ = '\0';
	while (x_cp && *x_cp) {
		if (*x_cp == ' ' || *x_cp == '\t') {
			x_cp++;
			continue;
		}
		if (x_q < &x_host_aliases[x_MAXALIASES - 1])
			*x_q++ = x_cp;
		x_cp = x_any(x_cp, " \t");
		if (x_cp != x_NULL)
			*x_cp++ = '\0';
	}
	*x_q = x_NULL;
	return (&x_host);
}

x_int x_sethostfile(x_file) char *x_file; {
	x__host_file = x_file;
}

static char *x_any(x_cp, x_match) register char *x_cp; char *x_match; {
	register char *x_mp, x_c;

	while (x_c = *x_cp) {
		for (x_mp = x_match; *x_mp; x_mp++)
			if (*x_mp == x_c)
				return (x_cp);
		x_cp++;
	}
	return ((char *)0);
}
