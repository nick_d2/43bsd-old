#include "x_.h"

/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)getproto.c	5.3 (Berkeley) 5/19/86";
#endif

#include <x_netdb.h>

extern x_int x__proto_stayopen;

struct x_protoent *x_getprotobynumber(x_proto) register x_int x_proto; {
	register struct x_protoent *x_p;

	x_setprotoent(x__proto_stayopen);
	while (x_p = x_getprotoent())
		if (x_p->x_p_proto == x_proto)
			break;
	if (!x__proto_stayopen)
		x_endprotoent();
	return (x_p);
}
