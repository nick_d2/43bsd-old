#include "x_.h"

#include <x_netdb.h>
#include <x_stdlib.h>
#include <x_strings.h>
#include <x_mp.h>
#include <x_unistd.h>
/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)ruserpass.c	5.2 (Berkeley) 3/9/86";
#endif

#include <x_stdio.h>
#include <x_utmp.h>
#include <x_ctype.h>
#include <sys/x_types.h>
#include <sys/x_stat.h>
#include <x_errno.h>

char	*x_renvlook(), *x_malloc(), *x_index(), *x_getenv(), *x_getpass(), *x_getlogin();
struct	x_utmp *x_getutmp();
static	x_FILE *x_cfile;

#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

static x_int x_renv __P((char *x_host, char **x_aname, char **x_apass));
static char *x_renvlook __P((char *x_host));
static x_int x_rnetrc __P((char *x_host, char **x_aname, char **x_apass));
static x_int x_token __P((void));
static char *x_nbsencrypt __P((char *x_str, char *x_key, char *x_result));
static char *x_nbsdecrypt __P((char *x_cpt, char *x_key, char *x_result));
static char *x_nbs8encrypt __P((char *x_str, char *x_key));
static char *x_nbs8decrypt __P((char *x_crp, char *x_key));
static x_int x_enblkclr __P((char *x_blk, char *x_str));
static char *x_deblkclr __P((char *x_blk));
static x_int x_enblknot __P((char *x_blk, char *x_crp));
static char *x_deblknot __P((char *x_blk));
static x_int x_nbssetkey __P((char *x_key));
static x_int x_blkencrypt __P((char *x_block, x_int x_edflag));
static struct x_utmp *x_getutmp __P((char *x_sttyname));
static x_int x_sreverse __P((register char *x_sto, register char *x_sfrom));
static char *x_mkenvkey __P((x_int x_mch));

x_int x_ruserpass(x_host, x_aname, x_apass) char *x_host; char **x_aname; char **x_apass; {

	x_renv(x_host, x_aname, x_apass);
	if (*x_aname == 0 || *x_apass == 0)
		x_rnetrc(x_host, x_aname, x_apass);
	if (*x_aname == 0) {
		char *x_myname = x_getlogin();
		*x_aname = x_malloc(16);
		x_printf("Name (%s:%s): ", x_host, x_myname);
		x_fflush(x_stdout);
		if (x_read(2, *x_aname, 16) <= 0)
			x_exit(1);
		if ((*x_aname)[0] == '\n')
			*x_aname = x_myname;
		else
			if (x_index(*x_aname, '\n'))
				*x_index(*x_aname, '\n') = 0;
	}
	if (*x_aname && *x_apass == 0) {
		x_printf("Password (%s:%s): ", x_host, *x_aname);
		x_fflush(x_stdout);
		*x_apass = x_getpass("");
	}
}

static x_int x_renv(x_host, x_aname, x_apass) char *x_host; char **x_aname; char **x_apass; {
	register char *x_cp;
	char *x_stemp, x_fgetlogin, *x_comma;

	x_cp = x_renvlook(x_host);
	if (x_cp == x_NULL)
		return;
	if (!x_isalpha(x_cp[0]))
		return;
	x_comma = x_index(x_cp, ',');
	if (x_comma == 0)
		return;
	if (*x_aname == 0) {
		*x_aname = x_malloc(x_comma - x_cp + 1);
		x_strncpy(*x_aname, x_cp, x_comma - x_cp);
	} else
		if (x_strncmp(*x_aname, x_cp, x_comma - x_cp))
			return;
	x_comma++;
	x_cp = x_malloc(x_strlen(x_comma)+1);
	x_strcpy(x_cp, x_comma);
	*x_apass = x_malloc(16);
	x_mkpwclear(x_cp, x_host[0], *x_apass);
}

static char *x_renvlook(x_host) char *x_host; {
	register char *x_cp, **x_env;
	extern char **x_environ;

	x_env = x_environ;
	for (x_env = x_environ; *x_env != x_NULL; x_env++)
		if (!x_strncmp(*x_env, "MACH", 4)) {
			x_cp = x_index(*x_env, '=');
			if (x_cp == 0)
				continue;
			if (x_strncmp(*x_env+4, x_host, x_cp-(*x_env+4)))
				continue;
			return (x_cp+1);
		}
	return (x_NULL);
}

#define	x_DEFAULT	1
#define	x_LOGIN	2
#define	x_PASSWD	3
#define	x_NOTIFY	4
#define	x_WRITE	5
#define	x_YES	6
#define	x_NO	7
#define	x_COMMAND	8
#define	x_FORCE	9
#define	x_ID	10
#define	x_MACHINE	11

static char x_tokval[100];

static struct x_toktab {
	char *x_tokstr;
	x_int x_tval;
} x_toktab[]= {
	"default",	x_DEFAULT,
	"login",	x_LOGIN,
	"password",	x_PASSWD,
	"notify",	x_NOTIFY,
	"write",	x_WRITE,
	"yes",		x_YES,
	"y",		x_YES,
	"no",		x_NO,
	"n",		x_NO,
	"command",	x_COMMAND,
	"force",	x_FORCE,
	"machine",	x_MACHINE,
	0,		0
};

static x_int x_rnetrc(x_host, x_aname, x_apass) char *x_host; char **x_aname; char **x_apass; {
	char *x_hdir, x_buf[x_BUFSIZ];
	x_int x_t;
	struct x_stat x_stb;
	extern x_int x_errno;

	x_hdir = x_getenv("HOME");
	if (x_hdir == x_NULL)
		x_hdir = ".";
	x_sprintf(x_buf, "%s/.netrc", x_hdir);
	x_cfile = x_fopen(x_buf, "r");
	if (x_cfile == x_NULL) {
		if (x_errno != x_ENOENT)
			x_perror(x_buf);
		return;
	}
x_next:
	while ((x_t = x_token())) switch(x_t) {

	case x_DEFAULT:
		(void) x_token();
		continue;

	case x_MACHINE:
		if (x_token() != x_ID || x_strcmp(x_host, x_tokval))
			continue;
		while ((x_t = x_token()) && x_t != x_MACHINE) switch(x_t) {

		case x_LOGIN:
			if (x_token())
				if (*x_aname == 0) { 
					*x_aname = x_malloc(x_strlen(x_tokval) + 1);
					x_strcpy(*x_aname, x_tokval);
				} else {
					if (x_strcmp(*x_aname, x_tokval))
						goto x_next;
				}
			break;
		case x_PASSWD:
			if (x_fstat(x_fileno(x_cfile), &x_stb) >= 0
			    && (x_stb.x_st_mode & 077) != 0) {
	x_fprintf(x_stderr, "Error - .netrc file not correct mode.\n");
	x_fprintf(x_stderr, "Remove password or correct mode.\n");
				x_exit(1);
			}
			if (x_token() && *x_apass == 0) {
				*x_apass = x_malloc(x_strlen(x_tokval) + 1);
				x_strcpy(*x_apass, x_tokval);
			}
			break;
		case x_COMMAND:
		case x_NOTIFY:
		case x_WRITE:
		case x_FORCE:
			(void) x_token();
			break;
		x_default:
	x_fprintf(x_stderr, "Unknown .netrc option %s\n", x_tokval);
			break;
		}
		goto x_done;
	}
x_done:
	x_fclose(x_cfile);
}

static x_int x_token() {
	char *x_cp;
	x_int x_c;
	struct x_toktab *x_t;

	if (x_feof(x_cfile))
		return (0);
	while ((x_c = x_getc(x_cfile)) != x_EOF &&
	    (x_c == '\n' || x_c == '\t' || x_c == ' ' || x_c == ','))
		continue;
	if (x_c == x_EOF)
		return (0);
	x_cp = x_tokval;
	if (x_c == '"') {
		while ((x_c = x_getc(x_cfile)) != x_EOF && x_c != '"') {
			if (x_c == '\\')
				x_c = x_getc(x_cfile);
			*x_cp++ = x_c;
		}
	} else {
		*x_cp++ = x_c;
		while ((x_c = x_getc(x_cfile)) != x_EOF
		    && x_c != '\n' && x_c != '\t' && x_c != ' ' && x_c != ',') {
			if (x_c == '\\')
				x_c = x_getc(x_cfile);
			*x_cp++ = x_c;
		}
	}
	*x_cp = 0;
	if (x_tokval[0] == 0)
		return (0);
	for (x_t = x_toktab; x_t->x_tokstr; x_t++)
		if (!x_strcmp(x_t->x_tokstr, x_tokval))
			return (x_t->x_tval);
	return (x_ID);
}
/* rest is nbs.c stolen from berknet */

char *x_deblknot(), *x_deblkclr();
char *x_nbs8decrypt(), *x_nbs8encrypt();
static char	x_E[48];

/*
 * The E bit-selection table.
 */
static char	x_e[] = {
	32, 1, 2, 3, 4, 5,
	 4, 5, 6, 7, 8, 9,
	 8, 9,10,11,12,13,
	12,13,14,15,16,17,
	16,17,18,19,20,21,
	20,21,22,23,24,25,
	24,25,26,27,28,29,
	28,29,30,31,32, 1,
};
static char *x_nbsencrypt(x_str, x_key, x_result) char *x_str; char *x_key; char *x_result; {
	static char x_buf[20],x_oldbuf[20];
	register x_int x_j;
	x_result[0] = 0;
	x_strcpy(x_oldbuf,x_key);
	while(*x_str){
		for(x_j=0;x_j<10;x_j++)x_buf[x_j] = 0;
		for(x_j=0;x_j<8 && *x_str;x_j++)x_buf[x_j] = *x_str++;
		x_strcat(x_result,x_nbs8encrypt(x_buf,x_oldbuf));
		x_strcat(x_result,"$");
		x_strcpy(x_oldbuf,x_buf);
		}
	return(x_result);
	}
static char *x_nbsdecrypt(x_cpt, x_key, x_result) char *x_cpt; char *x_key; char *x_result; {
	char *x_s;
	char x_c,x_oldbuf[20];
	x_result[0] = 0;
	x_strcpy(x_oldbuf,x_key);
	while(*x_cpt){
		for(x_s = x_cpt;*x_s && *x_s != '$';x_s++);
		x_c = *x_s;
		*x_s = 0;
		x_strcpy(x_oldbuf,x_nbs8decrypt(x_cpt,x_oldbuf));
		x_strcat(x_result,x_oldbuf);
		if(x_c == 0)break;
		x_cpt = x_s + 1;
		}
	return(x_result);
	}

static char *x_nbs8encrypt(x_str, x_key) char *x_str; char *x_key; {
	static char x_keyblk[100], x_blk[100];
	register x_int x_i;

	x_enblkclr(x_keyblk,x_key);
	x_nbssetkey(x_keyblk);

	for(x_i=0;x_i<48;x_i++) x_E[x_i] = x_e[x_i];
	x_enblkclr(x_blk,x_str);
	x_blkencrypt(x_blk,0);			/* forward dir */

	return(x_deblknot(x_blk));
}

static char *x_nbs8decrypt(x_crp, x_key) char *x_crp; char *x_key; {
	static char x_keyblk[100], x_blk[100];
	register x_int x_i;

	x_enblkclr(x_keyblk,x_key);
	x_nbssetkey(x_keyblk);

	for(x_i=0;x_i<48;x_i++) x_E[x_i] = x_e[x_i];
	x_enblknot(x_blk,x_crp);
	x_blkencrypt(x_blk,1);			/* backward dir */

	return(x_deblkclr(x_blk));
}

static x_int x_enblkclr(x_blk, x_str)		/* ignores top bit of chars in string str */ char *x_blk; char *x_str; {
	register x_int x_i,x_j;
	char x_c;
	for(x_i=0;x_i<70;x_i++)x_blk[x_i] = 0;
	for(x_i=0; (x_c= *x_str) && x_i<64; x_str++){
		for(x_j=0; x_j<7; x_j++, x_i++)
			x_blk[x_i] = (x_c>>(6-x_j)) & 01;
		x_i++;
		}
	}

static char *x_deblkclr(x_blk) char *x_blk; {
	register x_int x_i,x_j;
	char x_c;
	static char x_iobuf[30];
	for(x_i=0; x_i<10; x_i++){
		x_c = 0;
		for(x_j=0; x_j<7; x_j++){
			x_c <<= 1;
			x_c |= x_blk[8*x_i+x_j];
			}
		x_iobuf[x_i] = x_c;
	}
	x_iobuf[x_i] = 0;
	return(x_iobuf);
	}

static x_int x_enblknot(x_blk, x_crp) char *x_blk; char *x_crp; {
	register x_int x_i,x_j;
	char x_c;
	for(x_i=0;x_i<70;x_i++)x_blk[x_i] = 0;
	for(x_i=0; (x_c= *x_crp) && x_i<64; x_crp++){
		if(x_c>'Z') x_c -= 6;
		if(x_c>'9') x_c -= 7;
		x_c -= '.';
		for(x_j=0; x_j<6; x_j++, x_i++)
			x_blk[x_i] = (x_c>>(5-x_j)) & 01;
		}
	}

static char *x_deblknot(x_blk) char *x_blk; {
	register x_int x_i,x_j;
	char x_c;
	static char x_iobuf[30];
	for(x_i=0; x_i<11; x_i++){
		x_c = 0;
		for(x_j=0; x_j<6; x_j++){
			x_c <<= 1;
			x_c |= x_blk[6*x_i+x_j];
			}
		x_c += '.';
		if(x_c > '9')x_c += 7;
		if(x_c > 'Z')x_c += 6;
		x_iobuf[x_i] = x_c;
	}
	x_iobuf[x_i] = 0;
	return(x_iobuf);
}

/*
 * This program implements the
 * Proposed Federal Information Processing
 *  Data Encryption Standard.
 * See Federal Register, March 17, 1975 (40FR12134)
 */

/*
 * Initial permutation,
 */
static	char	x_IP[] = {
	58,50,42,34,26,18,10, 2,
	60,52,44,36,28,20,12, 4,
	62,54,46,38,30,22,14, 6,
	64,56,48,40,32,24,16, 8,
	57,49,41,33,25,17, 9, 1,
	59,51,43,35,27,19,11, 3,
	61,53,45,37,29,21,13, 5,
	63,55,47,39,31,23,15, 7,
};

/*
 * Final permutation, FP = IP^(-1)
 */
static	char	x_FP[] = {
	40, 8,48,16,56,24,64,32,
	39, 7,47,15,55,23,63,31,
	38, 6,46,14,54,22,62,30,
	37, 5,45,13,53,21,61,29,
	36, 4,44,12,52,20,60,28,
	35, 3,43,11,51,19,59,27,
	34, 2,42,10,50,18,58,26,
	33, 1,41, 9,49,17,57,25,
};

/*
 * Permuted-choice 1 from the key bits
 * to yield C and D.
 * Note that bits 8,16... are left out:
 * They are intended for a parity check.
 */
static	char	x_PC1_C[] = {
	57,49,41,33,25,17, 9,
	 1,58,50,42,34,26,18,
	10, 2,59,51,43,35,27,
	19,11, 3,60,52,44,36,
};

static	char	x_PC1_D[] = {
	63,55,47,39,31,23,15,
	 7,62,54,46,38,30,22,
	14, 6,61,53,45,37,29,
	21,13, 5,28,20,12, 4,
};

/*
 * Sequence of shifts used for the key schedule.
*/
static	char	x_shifts[] = {
	1,1,2,2,2,2,2,2,1,2,2,2,2,2,2,1,
};

/*
 * Permuted-choice 2, to pick out the bits from
 * the CD array that generate the key schedule.
 */
static	char	x_PC2_C[] = {
	14,17,11,24, 1, 5,
	 3,28,15, 6,21,10,
	23,19,12, 4,26, 8,
	16, 7,27,20,13, 2,
};

static	char	x_PC2_D[] = {
	41,52,31,37,47,55,
	30,40,51,45,33,48,
	44,49,39,56,34,53,
	46,42,50,36,29,32,
};

/*
 * The C and D arrays used to calculate the key schedule.
 */

static	char	x_C[28];
static	char	x_D[28];
/*
 * The key schedule.
 * Generated from the key.
 */
static	char	x_KS[16][48];

/*
 * Set up the key schedule from the key.
 */

static x_int x_nbssetkey(x_key) char *x_key; {
	register x_int x_i, x_j, x_k;
	x_int x_t;

	/*
	 * First, generate C and D by permuting
	 * the key.  The low order bit of each
	 * 8-bit char is not used, so C and D are only 28
	 * bits apiece.
	 */
	for (x_i=0; x_i<28; x_i++) {
		x_C[x_i] = x_key[x_PC1_C[x_i]-1];
		x_D[x_i] = x_key[x_PC1_D[x_i]-1];
	}
	/*
	 * To generate Ki, rotate C and D according
	 * to schedule and pick up a permutation
	 * using PC2.
	 */
	for (x_i=0; x_i<16; x_i++) {
		/*
		 * rotate.
		 */
		for (x_k=0; x_k<x_shifts[x_i]; x_k++) {
			x_t = x_C[0];
			for (x_j=0; x_j<28-1; x_j++)
				x_C[x_j] = x_C[x_j+1];
			x_C[27] = x_t;
			x_t = x_D[0];
			for (x_j=0; x_j<28-1; x_j++)
				x_D[x_j] = x_D[x_j+1];
			x_D[27] = x_t;
		}
		/*
		 * get Ki. Note C and D are concatenated.
		 */
		for (x_j=0; x_j<24; x_j++) {
			x_KS[x_i][x_j] = x_C[x_PC2_C[x_j]-1];
			x_KS[x_i][x_j+24] = x_D[x_PC2_D[x_j]-28-1];
		}
	}
}


/*
 * The 8 selection functions.
 * For some reason, they give a 0-origin
 * index, unlike everything else.
 */
static	char	x_S[8][64] = {
	14, 4,13, 1, 2,15,11, 8, 3,10, 6,12, 5, 9, 0, 7,
	 0,15, 7, 4,14, 2,13, 1,10, 6,12,11, 9, 5, 3, 8,
	 4, 1,14, 8,13, 6, 2,11,15,12, 9, 7, 3,10, 5, 0,
	15,12, 8, 2, 4, 9, 1, 7, 5,11, 3,14,10, 0, 6,13,

	15, 1, 8,14, 6,11, 3, 4, 9, 7, 2,13,12, 0, 5,10,
	 3,13, 4, 7,15, 2, 8,14,12, 0, 1,10, 6, 9,11, 5,
	 0,14, 7,11,10, 4,13, 1, 5, 8,12, 6, 9, 3, 2,15,
	13, 8,10, 1, 3,15, 4, 2,11, 6, 7,12, 0, 5,14, 9,

	10, 0, 9,14, 6, 3,15, 5, 1,13,12, 7,11, 4, 2, 8,
	13, 7, 0, 9, 3, 4, 6,10, 2, 8, 5,14,12,11,15, 1,
	13, 6, 4, 9, 8,15, 3, 0,11, 1, 2,12, 5,10,14, 7,
	 1,10,13, 0, 6, 9, 8, 7, 4,15,14, 3,11, 5, 2,12,

	 7,13,14, 3, 0, 6, 9,10, 1, 2, 8, 5,11,12, 4,15,
	13, 8,11, 5, 6,15, 0, 3, 4, 7, 2,12, 1,10,14, 9,
	10, 6, 9, 0,12,11, 7,13,15, 1, 3,14, 5, 2, 8, 4,
	 3,15, 0, 6,10, 1,13, 8, 9, 4, 5,11,12, 7, 2,14,

	 2,12, 4, 1, 7,10,11, 6, 8, 5, 3,15,13, 0,14, 9,
	14,11, 2,12, 4, 7,13, 1, 5, 0,15,10, 3, 9, 8, 6,
	 4, 2, 1,11,10,13, 7, 8,15, 9,12, 5, 6, 3, 0,14,
	11, 8,12, 7, 1,14, 2,13, 6,15, 0, 9,10, 4, 5, 3,

	12, 1,10,15, 9, 2, 6, 8, 0,13, 3, 4,14, 7, 5,11,
	10,15, 4, 2, 7,12, 9, 5, 6, 1,13,14, 0,11, 3, 8,
	 9,14,15, 5, 2, 8,12, 3, 7, 0, 4,10, 1,13,11, 6,
	 4, 3, 2,12, 9, 5,15,10,11,14, 1, 7, 6, 0, 8,13,

	 4,11, 2,14,15, 0, 8,13, 3,12, 9, 7, 5,10, 6, 1,
	13, 0,11, 7, 4, 9, 1,10,14, 3, 5,12, 2,15, 8, 6,
	 1, 4,11,13,12, 3, 7,14,10,15, 6, 8, 0, 5, 9, 2,
	 6,11,13, 8, 1, 4,10, 7, 9, 5, 0,15,14, 2, 3,12,

	13, 2, 8, 4, 6,15,11, 1,10, 9, 3,14, 5, 0,12, 7,
	 1,15,13, 8,10, 3, 7, 4,12, 5, 6,11, 0,14, 9, 2,
	 7,11, 4, 1, 9,12,14, 2, 0, 6,10,13,15, 3, 5, 8,
	 2, 1,14, 7, 4,10, 8,13,15,12, 9, 0, 3, 5, 6,11,
};

/*
 * P is a permutation on the selected combination
 * of the current L and key.
 */
static	char	x_P[] = {
	16, 7,20,21,
	29,12,28,17,
	 1,15,23,26,
	 5,18,31,10,
	 2, 8,24,14,
	32,27, 3, 9,
	19,13,30, 6,
	22,11, 4,25,
};

/*
 * The current block, divided into 2 halves.
 */
static	char	x_L[32], x_R[32];
static	char	x_tempL[32];
static	char	x_f[32];

/*
 * The combination of the key and the input, before selection.
 */
static	char	x_preS[48];

/*
 * The payoff: encrypt a block.
 */

static x_int x_blkencrypt(x_block, x_edflag) char *x_block; x_int x_edflag; {
	x_int x_i, x_ii;
	register x_int x_t, x_j, x_k;

	/*
	 * First, permute the bits in the input
	 */
	for (x_j=0; x_j<64; x_j++)
		x_L[x_j] = x_block[x_IP[x_j]-1];
	/*
	 * Perform an encryption operation 16 times.
	 */
	for (x_ii=0; x_ii<16; x_ii++) {
		/*
		 * Set direction
		 */
		if (x_edflag)
			x_i = 15-x_ii;
		else
			x_i = x_ii;
		/*
		 * Save the R array,
		 * which will be the new L.
		 */
		for (x_j=0; x_j<32; x_j++)
			x_tempL[x_j] = x_R[x_j];
		/*
		 * Expand R to 48 bits using the E selector;
		 * exclusive-or with the current key bits.
		 */
		for (x_j=0; x_j<48; x_j++)
			x_preS[x_j] = x_R[x_E[x_j]-1] ^ x_KS[x_i][x_j];
		/*
		 * The pre-select bits are now considered
		 * in 8 groups of 6 bits each.
		 * The 8 selection functions map these
		 * 6-bit quantities into 4-bit quantities
		 * and the results permuted
		 * to make an f(R, K).
		 * The indexing into the selection functions
		 * is peculiar; it could be simplified by
		 * rewriting the tables.
		 */
		for (x_j=0; x_j<8; x_j++) {
			x_t = 6*x_j;
			x_k = x_S[x_j][(x_preS[x_t+0]<<5)+
				(x_preS[x_t+1]<<3)+
				(x_preS[x_t+2]<<2)+
				(x_preS[x_t+3]<<1)+
				(x_preS[x_t+4]<<0)+
				(x_preS[x_t+5]<<4)];
			x_t = 4*x_j;
			x_f[x_t+0] = (x_k>>3)&01;
			x_f[x_t+1] = (x_k>>2)&01;
			x_f[x_t+2] = (x_k>>1)&01;
			x_f[x_t+3] = (x_k>>0)&01;
		}
		/*
		 * The new R is L ^ f(R, K).
		 * The f here has to be permuted first, though.
		 */
		for (x_j=0; x_j<32; x_j++)
			x_R[x_j] = x_L[x_j] ^ x_f[x_P[x_j]-1];
		/*
		 * Finally, the new L (the original R)
		 * is copied back.
		 */
		for (x_j=0; x_j<32; x_j++)
			x_L[x_j] = x_tempL[x_j];
	}
	/*
	 * The output L and R are reversed.
	 */
	for (x_j=0; x_j<32; x_j++) {
		x_t = x_L[x_j];
		x_L[x_j] = x_R[x_j];
		x_R[x_j] = x_t;
	}
	/*
	 * The final output
	 * gets the inverse permutation of the very original.
	 */
	for (x_j=0; x_j<64; x_j++)
		x_block[x_j] = x_L[x_FP[x_j]-1];
}
/*
	getutmp()
	return a pointer to the system utmp structure associated with
	terminal sttyname, e.g. "/dev/tty3"
	Is version independent-- will work on v6 systems
	return NULL if error
*/
static struct x_utmp *x_getutmp(x_sttyname) char *x_sttyname; {
	static struct x_utmp x_utmpstr;
	x_FILE *x_fdutmp;

	if(x_sttyname == x_NULL || x_sttyname[0] == 0)return(x_NULL);

	x_fdutmp = x_fopen("/etc/utmp","r");
	if(x_fdutmp == x_NULL)return(x_NULL);

	while(x_fread(&x_utmpstr,1,sizeof x_utmpstr,x_fdutmp) == sizeof x_utmpstr)
		if(x_strcmp(x_utmpstr.x_ut_line,x_sttyname+5) == 0){
			x_fclose(x_fdutmp);
			return(&x_utmpstr);
		}
	x_fclose(x_fdutmp);
	return(x_NULL);
}

static x_int x_sreverse(x_sto, x_sfrom) register char *x_sto; register char *x_sfrom; {
	register x_int x_i;

	x_i = x_strlen(x_sfrom);
	while (x_i >= 0)
		*x_sto++ = x_sfrom[x_i--];
}

static char *x_mkenvkey(x_mch) x_int x_mch; {
	static char x_skey[40];
	register struct x_utmp *x_putmp;
	char x_stemp[40], x_stemp1[40], x_sttyname[30];
	register char *x_sk,*x_p;

	if (x_isatty(2))
		x_strcpy(x_sttyname,x_ttyname(2));
	else if (x_isatty(0))
		x_strcpy(x_sttyname,x_ttyname(0));
	else if (x_isatty(1))
		x_strcpy(x_sttyname,x_ttyname(1));
	else
		return (x_NULL);
	x_putmp = x_getutmp(x_sttyname);
	if (x_putmp == x_NULL)
		return (x_NULL);
	x_sk = x_skey;
	x_p = x_putmp->x_ut_line;
	while (*x_p)
		*x_sk++ = *x_p++;
	*x_sk++ = x_mch;
	x_sprintf(x_stemp, "%ld", x_putmp->x_ut_time);
	x_sreverse(x_stemp1, x_stemp);
	x_p = x_stemp1;
	while (*x_p)
		*x_sk++ = *x_p++;
	*x_sk = 0;
	return (x_skey);
}

x_int x_mkpwunclear(x_spasswd, x_mch, x_sencpasswd) char *x_spasswd; x_int x_mch; char *x_sencpasswd; {
	register char *x_skey;

	if (x_spasswd[0] == 0) {
		x_sencpasswd[0] = 0;
		return;
	}
	x_skey = x_mkenvkey(x_mch);
	if (x_skey == x_NULL) {
		x_fprintf(x_stderr, "Can't make key\n");
		x_exit(1);
	}
	x_nbsencrypt(x_spasswd, x_skey, x_sencpasswd);
}

x_int x_mkpwclear(x_sencpasswd, x_mch, x_spasswd) char *x_sencpasswd; x_int x_mch; char *x_spasswd; {
	register char *x_skey;

	if (x_sencpasswd[0] == 0) {
		x_spasswd[0] = 0;
		return;
	}
	x_skey = x_mkenvkey(x_mch);
	if (x_skey == x_NULL) {
		x_fprintf(x_stderr, "Can't make key\n");
		x_exit(1);
	}
	x_nbsdecrypt(x_sencpasswd, x_skey, x_spasswd);
}
