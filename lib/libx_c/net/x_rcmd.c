#include "x_.h"

#include <x_strings.h>
#include <x_unistd.h>
#include <arpa/x_inet.h>
/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)rcmd.c	5.11 (Berkeley) 5/6/86";
#endif

#include <x_stdio.h>
#include <x_ctype.h>
#include <x_pwd.h>
#include <sys/x_param.h>
#include <sys/x_file.h>
#include <sys/x_signal.h>
#include <sys/x_socket.h>
#include <sys/x_stat.h>

#include <netinet/x_in.h>

#include <x_netdb.h>
#include <x_errno.h>

extern	x_errno;
char	*x_index(), *x_sprintf();

x_int x_rcmd(x_ahost, x_rport, x_locuser, x_remuser, x_cmd, x_fd2p) char **x_ahost; x_int x_rport; char *x_locuser; char *x_remuser; char *x_cmd; x_int *x_fd2p; {
	x_int x_s, x_timo = 1, x_pid, x_oldmask;
	struct x_sockaddr_in x_sin, x_sin2, x_from;
	char x_c;
	x_int x_lport = x_IPPORT_RESERVED - 1;
	struct x_hostent *x_hp;

	x_pid = x_getpid();
	x_hp = x_gethostbyname(*x_ahost);
	if (x_hp == 0) {
		x_fprintf(x_stderr, "%s: unknown host\n", *x_ahost);
		return (-1);
	}
	*x_ahost = x_hp->x_h_name;
	x_oldmask = x_sigblock(x_sigmask(x_SIGURG));
	for (;;) {
		x_s = x_rresvport(&x_lport);
		if (x_s < 0) {
			if (x_errno == x_EAGAIN)
				x_fprintf(x_stderr, "socket: All ports in use\n");
			else
				x_perror("rcmd: socket");
			x_sigsetmask(x_oldmask);
			return (-1);
		}
		x_fcntl(x_s, x_F_SETOWN, x_pid);
		x_sin.x_sin_family = x_hp->x_h_addrtype;
		x_bcopy(x_hp->x_h_addr_list[0], (x_caddr_t)&x_sin.x_sin_addr, x_hp->x_h_length);
		x_sin.x_sin_port = x_rport;
		if (x_connect(x_s, (x_caddr_t)&x_sin, sizeof (x_sin), 0) >= 0)
			break;
		(void) x_close(x_s);
		if (x_errno == x_EADDRINUSE) {
			x_lport--;
			continue;
		}
		if (x_errno == x_ECONNREFUSED && x_timo <= 16) {
			x_sleep(x_timo);
			x_timo *= 2;
			continue;
		}
		if (x_hp->x_h_addr_list[1] != x_NULL) {
			x_int x_oerrno = x_errno;

			x_fprintf(x_stderr,
			    "connect to address %s: ", x_inet_ntoa(x_sin.x_sin_addr));
			x_errno = x_oerrno;
			x_perror(0);
			x_hp->x_h_addr_list++;
			x_bcopy(x_hp->x_h_addr_list[0], (x_caddr_t)&x_sin.x_sin_addr,
			    x_hp->x_h_length);
			x_fprintf(x_stderr, "Trying %s...\n",
				x_inet_ntoa(x_sin.x_sin_addr));
			continue;
		}
		x_perror(x_hp->x_h_name);
		x_sigsetmask(x_oldmask);
		return (-1);
	}
	x_lport--;
	if (x_fd2p == 0) {
		x_write(x_s, "", 1);
		x_lport = 0;
	} else {
		char x_num[8];
		x_int x_s2 = x_rresvport(&x_lport), x_s3;
		x_int x_len = sizeof (x_from);

		if (x_s2 < 0)
			goto x_bad;
		x_listen(x_s2, 1);
		(void) x_sprintf(x_num, "%d", x_lport);
		if (x_write(x_s, x_num, x_strlen(x_num)+1) != x_strlen(x_num)+1) {
			x_perror("write: setting up stderr");
			(void) x_close(x_s2);
			goto x_bad;
		}
		x_s3 = x_accept(x_s2, &x_from, &x_len, 0);
		(void) x_close(x_s2);
		if (x_s3 < 0) {
			x_perror("accept");
			x_lport = 0;
			goto x_bad;
		}
		*x_fd2p = x_s3;
		x_from.x_sin_port = x_ntohs((x_u_short)x_from.x_sin_port);
		if (x_from.x_sin_family != x_AF_INET ||
		    x_from.x_sin_port >= x_IPPORT_RESERVED) {
			x_fprintf(x_stderr,
			    "socket: protocol failure in circuit setup.\n");
			goto x_bad2;
		}
	}
	(void) x_write(x_s, x_locuser, x_strlen(x_locuser)+1);
	(void) x_write(x_s, x_remuser, x_strlen(x_remuser)+1);
	(void) x_write(x_s, x_cmd, x_strlen(x_cmd)+1);
	if (x_read(x_s, &x_c, 1) != 1) {
		x_perror(*x_ahost);
		goto x_bad2;
	}
	if (x_c != 0) {
		while (x_read(x_s, &x_c, 1) == 1) {
			(void) x_write(2, &x_c, 1);
			if (x_c == '\n')
				break;
		}
		goto x_bad2;
	}
	x_sigsetmask(x_oldmask);
	return (x_s);
x_bad2:
	if (x_lport)
		(void) x_close(*x_fd2p);
x_bad:
	(void) x_close(x_s);
	x_sigsetmask(x_oldmask);
	return (-1);
}

x_int x_rresvport(x_alport) x_int *x_alport; {
	struct x_sockaddr_in x_sin;
	x_int x_s;

	x_sin.x_sin_family = x_AF_INET;
	x_sin.x_sin_addr.x_s_addr = x_INADDR_ANY;
	x_s = x_socket(x_AF_INET, x_SOCK_STREAM, 0);
	if (x_s < 0)
		return (-1);
	for (;;) {
		x_sin.x_sin_port = x_htons((x_u_short)*x_alport);
		if (x_bind(x_s, (x_caddr_t)&x_sin, sizeof (x_sin)) >= 0)
			return (x_s);
		if (x_errno != x_EADDRINUSE) {
			(void) x_close(x_s);
			return (-1);
		}
		(*x_alport)--;
		if (*x_alport == x_IPPORT_RESERVED/2) {
			(void) x_close(x_s);
			x_errno = x_EAGAIN;		/* close */
			return (-1);
		}
	}
}

x_int x_ruserok(x_rhost, x_superuser, x_ruser, x_luser) char *x_rhost; x_int x_superuser; char *x_ruser; char *x_luser; {
	x_FILE *x_hostf;
	char x_fhost[x_MAXHOSTNAMELEN];
	x_int x_first = 1;
	register char *x_sp, *x_p;
	x_int x_baselen = -1;

	x_sp = x_rhost;
	x_p = x_fhost;
	while (*x_sp) {
		if (*x_sp == '.') {
			if (x_baselen == -1)
				x_baselen = x_sp - x_rhost;
			*x_p++ = *x_sp++;
		} else {
			*x_p++ = x_isupper(*x_sp) ? x_tolower(*x_sp++) : *x_sp++;
		}
	}
	*x_p = '\0';
	x_hostf = x_superuser ? (x_FILE *)0 : x_fopen("/etc/hosts.equiv", "r");
x_again:
	if (x_hostf) {
		if (!x__validuser(x_hostf, x_fhost, x_luser, x_ruser, x_baselen)) {
			(void) x_fclose(x_hostf);
			return(0);
		}
		(void) x_fclose(x_hostf);
	}
	if (x_first == 1) {
		struct x_stat x_sbuf;
		struct x_passwd *x_pwd;
		char x_pbuf[x_MAXPATHLEN];

		x_first = 0;
		if ((x_pwd = x_getpwnam(x_luser)) == x_NULL)
			return(-1);
		(void)x_strcpy(x_pbuf, x_pwd->x_pw_dir);
		(void)x_strcat(x_pbuf, "/.rhosts");
		if ((x_hostf = x_fopen(x_pbuf, "r")) == x_NULL)
			return(-1);
		(void)x_fstat(x_fileno(x_hostf), &x_sbuf);
		if (x_sbuf.x_st_uid && x_sbuf.x_st_uid != x_pwd->x_pw_uid) {
			x_fclose(x_hostf);
			return(-1);
		}
		goto x_again;
	}
	return (-1);
}

x_int x__validuser(x_hostf, x_rhost, x_luser, x_ruser, x_baselen) x_FILE *x_hostf; char *x_rhost; char *x_luser; char *x_ruser; x_int x_baselen; {
	char *x_user;
	char x_ahost[x_MAXHOSTNAMELEN];
	register char *x_p;

	while (x_fgets(x_ahost, sizeof (x_ahost), x_hostf)) {
		x_p = x_ahost;
		while (*x_p != '\n' && *x_p != ' ' && *x_p != '\t' && *x_p != '\0') {
			*x_p = x_isupper(*x_p) ? x_tolower(*x_p) : *x_p;
			x_p++;
		}
		if (*x_p == ' ' || *x_p == '\t') {
			*x_p++ = '\0';
			while (*x_p == ' ' || *x_p == '\t')
				x_p++;
			x_user = x_p;
			while (*x_p != '\n' && *x_p != ' ' && *x_p != '\t' && *x_p != '\0')
				x_p++;
		} else
			x_user = x_p;
		*x_p = '\0';
		if (x__checkhost(x_rhost, x_ahost, x_baselen) &&
		    !x_strcmp(x_ruser, *x_user ? x_user : x_luser)) {
			return (0);
		}
	}
	return (-1);
}

x_int x__checkhost(x_rhost, x_lhost, x_len) char *x_rhost; char *x_lhost; x_int x_len; {
	static char x_ldomain[x_MAXHOSTNAMELEN + 1];
	static char *x_domainp = x_NULL;
	register char *x_cp;

	if (x_len == -1)
		return(!x_strcmp(x_rhost, x_lhost));
	if (x_strncmp(x_rhost, x_lhost, x_len))
		return(0);
	if (!x_strcmp(x_rhost, x_lhost))
		return(1);
	if (*(x_lhost + x_len) != '\0')
		return(0);
	if (!x_domainp) {
		if (x_gethostname(x_ldomain, sizeof(x_ldomain)) == -1) {
			x_domainp = (char *)1;
			return(0);
		}
		x_ldomain[x_MAXHOSTNAMELEN] = x_NULL;
		if ((x_domainp = x_index(x_ldomain, '.') + 1) == (char *)1)
			return(0);
		x_cp = x_domainp;
		while (*x_cp) {
			*x_cp = x_isupper(*x_cp) ? x_tolower(*x_cp) : *x_cp;
			x_cp++;
		}
	}
	if (x_domainp == (char *)1)
		return(0);
	return(!x_strcmp(x_domainp, x_rhost + x_len +1));
}
