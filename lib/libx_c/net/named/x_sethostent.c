#include "x_.h"

#include <x_netdb.h>
/*
 * Copyright (c) 1985 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)sethostent.c	6.3 (Berkeley) 4/10/86";
#endif

#include <sys/x_types.h>
#include <arpa/x_nameser.h>
#include <netinet/x_in.h>
#include <x_resolv.h>

x_int x_sethostent(x_stayopen) x_int x_stayopen; {
	if (x_stayopen)
		x__res.x_options |= x_RES_STAYOPEN | x_RES_USEVC;
}

x_int x_endhostent() {
	x__res.x_options &= ~(x_RES_STAYOPEN | x_RES_USEVC);
	x__res_close();
}

x_int x_sethostfile(x_name) char *x_name; {
#ifdef x_lint
x_name = x_name;
#endif
}
