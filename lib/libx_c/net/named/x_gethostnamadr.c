#include "x_.h"

#include <x_strings.h>
/*
 * Copyright (c) 1985 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)gethostnamadr.c	6.12 (Berkeley) 5/19/86";
#endif

#include <sys/x_types.h>
#include <sys/x_socket.h>
#include <netinet/x_in.h>
#include <x_ctype.h>
#include <x_netdb.h>
#include <x_stdio.h>
#include <x_errno.h>
#include <arpa/x_inet.h>
#include <arpa/x_nameser.h>
#include <x_resolv.h>

#define	x_MAXALIASES	35
#define x_MAXADDRS	35

static char *x_h_addr_ptrs[x_MAXADDRS + 1];

static struct x_hostent x_host;
static char *x_host_aliases[x_MAXALIASES];
static char x_hostbuf[x_BUFSIZ+1];
static struct x_in_addr x_host_addr;
static char x_HOSTDB[] = "/etc/hosts";
static x_FILE *x_hostf = x_NULL;
static char x_line[x_BUFSIZ+1];
static char x_hostaddr[x_MAXADDRS];
static char *x_host_addrs[2];
static x_int x_stayopen = 0;
static char *x_any();

typedef union {
    x_HEADER x_qb1;
    char x_qb2[x_PACKETSZ];
} x_querybuf;

static union {
    x_long x_al;
    char x_ac;
} x_align;


x_int x_h_errno;
extern x_errno;

#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

static struct x_hostent *x_getanswer __P((char *x_msg, x_int x_msglen, x_int x_iquery));
static char *x_any __P((register char *x_cp, char *x_match));

static struct x_hostent *x_getanswer(x_msg, x_msglen, x_iquery) char *x_msg; x_int x_msglen; x_int x_iquery; {
	register x_HEADER *x_hp;
	register char *x_cp;
	register x_int x_n;
	x_querybuf x_answer;
	char *x_eom, *x_bp, **x_ap;
	x_int x_type, x_class, x_buflen, x_ancount, x_qdcount;
	x_int x_haveanswer, x_getclass = x_C_ANY;
	char **x_hap;

	x_n = x_res_send(x_msg, x_msglen, (char *)&x_answer, sizeof(x_answer));
	if (x_n < 0) {
#ifdef x_DEBUG
		x_int x_terrno;
		x_terrno = x_errno;
		if (x__res.x_options & x_RES_DEBUG)
			x_printf("res_send failed\n");
		x_errno = x_terrno;
#endif
		x_h_errno = x_TRY_AGAIN;
		return (x_NULL);
	}
	x_eom = (char *)&x_answer + x_n;
	/*
	 * find first satisfactory answer
	 */
	x_hp = (x_HEADER *) &x_answer;
	x_ancount = x_ntohs(x_hp->x_ancount);
	x_qdcount = x_ntohs(x_hp->x_qdcount);
	if (x_hp->x_rcode != x_NOERROR || x_ancount == 0) {
#ifdef x_DEBUG
		if (x__res.x_options & x_RES_DEBUG)
			x_printf("rcode = %d, ancount=%d\n", x_hp->x_rcode, x_ancount);
#endif
		switch (x_hp->x_rcode) {
			case x_NXDOMAIN:
				/* Check if it's an authoritive answer */
				if (x_hp->x_aa)
					x_h_errno = x_HOST_NOT_FOUND;
				else
					x_h_errno = x_TRY_AGAIN;
				break;
			case x_SERVFAIL:
				x_h_errno = x_TRY_AGAIN;
				break;
			case x_NOERROR:
				x_h_errno = x_NO_ADDRESS;
				break;
			case x_FORMERR:
			case x_NOTIMP:
			case x_REFUSED:
				x_h_errno = x_NO_RECOVERY;
		}
		return (x_NULL);
	}
	x_bp = x_hostbuf;
	x_buflen = sizeof(x_hostbuf);
	x_cp = (char *)&x_answer + sizeof(x_HEADER);
	if (x_qdcount) {
		if (x_iquery) {
			if ((x_n = x_dn_expand((char *)&x_answer, x_eom,
			     x_cp, x_bp, x_buflen)) < 0) {
				x_h_errno = x_NO_RECOVERY;
				return (x_NULL);
			}
			x_cp += x_n + x_QFIXEDSZ;
			x_host.x_h_name = x_bp;
			x_n = x_strlen(x_bp) + 1;
			x_bp += x_n;
			x_buflen -= x_n;
		} else
			x_cp += x_dn_skip(x_cp) + x_QFIXEDSZ;
		while (--x_qdcount > 0)
			x_cp += x_dn_skip(x_cp) + x_QFIXEDSZ;
	} else if (x_iquery) {
		if (x_hp->x_aa)
			x_h_errno = x_HOST_NOT_FOUND;
		else
			x_h_errno = x_TRY_AGAIN;
		return (x_NULL);
	}
	x_ap = x_host_aliases;
	x_host.x_h_aliases = x_host_aliases;
	x_hap = x_h_addr_ptrs;
	x_host.x_h_addr_list = x_h_addr_ptrs;
	x_haveanswer = 0;
	while (--x_ancount >= 0 && x_cp < x_eom) {
		if ((x_n = x_dn_expand((char *)&x_answer, x_eom, x_cp, x_bp, x_buflen)) < 0)
			break;
		x_cp += x_n;
		x_type = x_getshort(x_cp);
 		x_cp += sizeof(x_u_short);
		x_class = x_getshort(x_cp);
 		x_cp += sizeof(x_u_short) + sizeof(x_u_long);
		x_n = x_getshort(x_cp);
		x_cp += sizeof(x_u_short);
		if (x_type == x_T_CNAME) {
			x_cp += x_n;
			if (x_ap >= &x_host_aliases[x_MAXALIASES-1])
				continue;
			*x_ap++ = x_bp;
			x_n = x_strlen(x_bp) + 1;
			x_bp += x_n;
			x_buflen -= x_n;
			continue;
		}
		if (x_type == x_T_PTR) {
			if ((x_n = x_dn_expand((char *)&x_answer, x_eom,
			    x_cp, x_bp, x_buflen)) < 0) {
				x_cp += x_n;
				continue;
			}
			x_cp += x_n;
			x_host.x_h_name = x_bp;
			return(&x_host);
		}
		if (x_type != x_T_A)  {
#ifdef x_DEBUG
			if (x__res.x_options & x_RES_DEBUG)
				x_printf("unexpected answer type %d, size %d\n",
					x_type, x_n);
#endif
			x_cp += x_n;
			continue;
		}
		if (x_haveanswer) {
			if (x_n != x_host.x_h_length) {
				x_cp += x_n;
				continue;
			}
			if (x_class != x_getclass) {
				x_cp += x_n;
				continue;
			}
		} else {
			x_host.x_h_length = x_n;
			x_getclass = x_class;
			x_host.x_h_addrtype = (x_class == x_C_IN) ? x_AF_INET : x_AF_UNSPEC;
			if (!x_iquery) {
				x_host.x_h_name = x_bp;
				x_bp += x_strlen(x_bp) + 1;
			}
		}

		x_bp += ((x_u_long)x_bp % sizeof(x_align));

		if (x_bp + x_n >= &x_hostbuf[sizeof(x_hostbuf)]) {
#ifdef x_DEBUG
			if (x__res.x_options & x_RES_DEBUG)
				x_printf("size (%d) too big\n", x_n);
#endif
			break;
		}
		x_bcopy(x_cp, *x_hap++ = x_bp, x_n);
		x_bp +=x_n;
		x_cp += x_n;
		x_haveanswer++;
	}
	if (x_haveanswer) {
		*x_ap = x_NULL;
		*x_hap = x_NULL;
		return (&x_host);
	} else {
		x_h_errno = x_TRY_AGAIN;
		return (x_NULL);
	}
}

struct x_hostent *x_gethostbyname(x_name) char *x_name; {
	x_int x_n;
	x_querybuf x_buf;
	register struct x_hostent *x_hp;
	extern struct x_hostent *x__gethtbyname();

	x_n = x_res_mkquery(x_QUERY, x_name, x_C_IN, x_T_A, (char *)x_NULL, 0, x_NULL,
		(char *)&x_buf, sizeof(x_buf));
	if (x_n < 0) {
#ifdef x_DEBUG
		if (x__res.x_options & x_RES_DEBUG)
			x_printf("res_mkquery failed\n");
#endif
		return (x_NULL);
	}
	x_hp = x_getanswer((char *)&x_buf, x_n, 0);
	if (x_hp == x_NULL && x_errno == x_ECONNREFUSED)
		x_hp = x__gethtbyname(x_name);
	return(x_hp);
}

struct x_hostent *x_gethostbyaddr(x_addr, x_len, x_type) char *x_addr; x_int x_len; x_int x_type; {
	x_int x_n;
	x_querybuf x_buf;
	register struct x_hostent *x_hp;
	char x_qbuf[x_MAXDNAME];
	extern struct x_hostent *x__gethtbyaddr();
	
	if (x_type != x_AF_INET)
		return (x_NULL);
	(void)x_sprintf(x_qbuf, "%d.%d.%d.%d.in-addr.arpa",
		((unsigned)x_addr[3] & 0xff),
		((unsigned)x_addr[2] & 0xff),
		((unsigned)x_addr[1] & 0xff),
		((unsigned)x_addr[0] & 0xff));
	x_n = x_res_mkquery(x_QUERY, x_qbuf, x_C_IN, x_T_PTR, (char *)x_NULL, 0, x_NULL,
		(char *)&x_buf, sizeof(x_buf));
	if (x_n < 0) {
#ifdef x_DEBUG
		if (x__res.x_options & x_RES_DEBUG)
			x_printf("res_mkquery failed\n");
#endif
		return (x_NULL);
	}
	x_hp = x_getanswer((char *)&x_buf, x_n, 1);
	if (x_hp == x_NULL && x_errno == x_ECONNREFUSED)
		x_hp = x__gethtbyaddr(x_addr, x_len, x_type);
	if (x_hp == x_NULL)
		return(x_NULL);
	x_hp->x_h_addrtype = x_type;
	x_hp->x_h_length = x_len;
	x_h_addr_ptrs[0] = (char *)&x_host_addr;
	x_h_addr_ptrs[1] = (char *)0;
	x_host_addr = *(struct x_in_addr *)x_addr;
	return(x_hp);
}


x_int x__sethtent(x_f) x_int x_f; {
	if (x_hostf == x_NULL)
		x_hostf = x_fopen(x_HOSTDB, "r" );
	else
		x_rewind(x_hostf);
	x_stayopen |= x_f;
}

x_int x__endhtent() {
	if (x_hostf && !x_stayopen) {
		(void) x_fclose(x_hostf);
		x_hostf = x_NULL;
	}
}

struct x_hostent *x__gethtent() {
	char *x_p;
	register char *x_cp, **x_q;

	if (x_hostf == x_NULL && (x_hostf = x_fopen(x_HOSTDB, "r" )) == x_NULL)
		return (x_NULL);
x_again:
	if ((x_p = x_fgets(x_line, x_BUFSIZ, x_hostf)) == x_NULL)
		return (x_NULL);
	if (*x_p == '#')
		goto x_again;
	x_cp = x_any(x_p, "#\n");
	if (x_cp == x_NULL)
		goto x_again;
	*x_cp = '\0';
	x_cp = x_any(x_p, " \t");
	if (x_cp == x_NULL)
		goto x_again;
	*x_cp++ = '\0';
	/* THIS STUFF IS INTERNET SPECIFIC */
	x_host.x_h_addr_list = x_host_addrs;
	x_host.x_h_addr = x_hostaddr;
	*((x_u_long *)x_host.x_h_addr) = x_inet_addr(x_p);
	x_host.x_h_length = sizeof (x_u_long);
	x_host.x_h_addrtype = x_AF_INET;
	while (*x_cp == ' ' || *x_cp == '\t')
		x_cp++;
	x_host.x_h_name = x_cp;
	x_q = x_host.x_h_aliases = x_host_aliases;
	x_cp = x_any(x_cp, " \t");
	if (x_cp != x_NULL) 
		*x_cp++ = '\0';
	while (x_cp && *x_cp) {
		if (*x_cp == ' ' || *x_cp == '\t') {
			x_cp++;
			continue;
		}
		if (x_q < &x_host_aliases[x_MAXALIASES - 1])
			*x_q++ = x_cp;
		x_cp = x_any(x_cp, " \t");
		if (x_cp != x_NULL)
			*x_cp++ = '\0';
	}
	*x_q = x_NULL;
	return (&x_host);
}

static char *x_any(x_cp, x_match) register char *x_cp; char *x_match; {
	register char *x_mp, x_c;

	while (x_c = *x_cp) {
		for (x_mp = x_match; *x_mp; x_mp++)
			if (*x_mp == x_c)
				return (x_cp);
		x_cp++;
	}
	return ((char *)0);
}

struct x_hostent *x__gethtbyname(x_name) char *x_name; {
	register struct x_hostent *x_p;
	register char **x_cp;
	char x_lowname[128];
	register char *x_lp = x_lowname;
	
	while (*x_name)
		if (x_isupper(*x_name))
			*x_lp++ = x_tolower(*x_name++);
		else
			*x_lp++ = *x_name++;
	*x_lp = '\0';

	x__sethtent(0);
	while (x_p = x__gethtent()) {
		if (x_strcmp(x_p->x_h_name, x_lowname) == 0)
			break;
		for (x_cp = x_p->x_h_aliases; *x_cp != 0; x_cp++)
			if (x_strcmp(*x_cp, x_lowname) == 0)
				goto x_found;
	}
x_found:
	x__endhtent();
	return (x_p);
}

struct x_hostent *x__gethtbyaddr(x_addr, x_len, x_type) char *x_addr; x_int x_len; x_int x_type; {
	register struct x_hostent *x_p;

	x__sethtent(0);
	while (x_p = x__gethtent())
		if (x_p->x_h_addrtype == x_type && !x_bcmp(x_p->x_h_addr, x_addr, x_len))
			break;
	x__endhtent();
	return (x_p);
}
