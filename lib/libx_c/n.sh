#!/bin/sh

rm -rf temp_h temp_c
find ../.. -name '*.c' -print >all_c.txt

find ../../include -name '*.h' -print |\
sed -e 's:^\.\./\.\./include/::' |\
while read i
do
  mkdir --parents temp_h/`dirname $i`
  cp ../../include/$i temp_h/$i
done

find ../../sys/h -name '*.h' -print |\
sed -e 's:^\.\./\.\./sys/h/::' |\
while read i
do
  mkdir --parents temp_h/sys/`dirname $i`
  cp ../../sys/h/$i temp_h/sys
done

for i in net netimp netinet netns stand vax vaxif vaxmba vaxuba
do
  find ../../sys/$i -name '*.h' -print |\
  sed -e "s:^\.\./\.\./sys/$i/::" |\
  while read j
  do
    mkdir --parents temp_h/$i/`dirname $j`
    cp ../../sys/$i/$j temp_h/$i/$j
  done
done

(
  cd temp_h
  ln -s vax machine
  ln -s machine/frame.h frame.h
  for i in errno.h signal.h syslog.h
  do
    ln -s sys/$i $i
  done
)

find ../libc -name '*.[ch]' -print |\
sed -e 's:^\.\./libc/::' |\
while read i
do
  mkdir --parents temp_c/`dirname $i`
  cp ../libc/$i temp_c/$i
done

#(
#  cd temp_c/vax/stdio
#  ln -s ../gen/DEFS.h DEFS.h
#)
rm -rf temp_c/vax
mkdir -p temp_c/vax
cp syscall.c temp_c/vax
