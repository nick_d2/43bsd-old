#include "x_.h"

#include <x_stdio.h>
#include <x_strings.h>
/*
 * Copyright (c) 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 * Includes material written at Cornell University, by J. Q. Johnson.
 * Used by permission.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)ns_addr.c	6.2 (Berkeley) 3/9/86";
#endif

#include <sys/x_types.h>
#include <netns/x_ns.h>

static struct x_ns_addr x_addr, x_zero_addr;

#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

static x_int x_Field __P((char *x_buf, x_u_char *x_out, x_int x_len));
static x_int x_cvtbase __P((x_int x_oldbase, x_int x_newbase, x_int x_input[], x_int x_inlen, unsigned char x_result[], x_int x_reslen));

struct x_ns_addr x_ns_addr(x_name) char *x_name; {
	x_u_long x_net;
	x_u_short x_socket;
	char x_separator = '.';
	char *x_hostname, *x_socketname, *x_cp;
	char x_buf[50];
	extern char *x_index();

	x_addr = x_zero_addr;
	x_strncpy(x_buf, x_name, 49);

	/*
	 * First, figure out what he intends as a field separtor.
	 * Despite the way this routine is written, the prefered
	 * form  2-272.AA001234H.01777, i.e. XDE standard.
	 * Great efforts are made to insure backward compatability.
	 */
	if (x_hostname = x_index(x_buf, '#'))
		x_separator = '#';
	else {
		x_hostname = x_index(x_buf, '.');
		if ((x_cp = x_index(x_buf, ':')) &&
		    ( (x_hostname && x_cp < x_hostname) || (x_hostname == 0))) {
			x_hostname = x_cp;
			x_separator = ':';
		}
	}
	if (x_hostname)
		*x_hostname++ = 0;
	x_Field(x_buf, x_addr.x_x_net.x_c_net, 4);
	if (x_hostname == 0)
		return (x_addr);  /* No separator means net only */

	x_socketname = x_index(x_hostname, x_separator);
	if (x_socketname) {
		*x_socketname++ = 0;
		x_Field(x_socketname, &x_addr.x_x_port, 2);
	}

	x_Field(x_hostname, x_addr.x_x_host.x_c_host, 6);

	return (x_addr);
}

static x_int x_Field(x_buf, x_out, x_len) char *x_buf; x_u_char *x_out; x_int x_len; {
	register char *x_bp = x_buf;
	x_int x_i, x_ibase, x_base16 = 0, x_base10 = 0, x_clen = 0;
	x_int x_hb[6], *x_hp;
	char *x_fmt;

	/*
	 * first try 2-273#2-852-151-014#socket
	 */
	if ((*x_buf != '-') &&
	    (1 < (x_i = x_sscanf(x_buf, "%d-%d-%d-%d-%d",
			&x_hb[0], &x_hb[1], &x_hb[2], &x_hb[3], &x_hb[4])))) {
		x_cvtbase(1000, 256, x_hb, x_i, x_out, x_len);
		return;
	}
	/*
	 * try form 8E1#0.0.AA.0.5E.E6#socket
	 */
	if (1 < (x_i = x_sscanf(x_buf,"%x.%x.%x.%x.%x.%x",
			&x_hb[0], &x_hb[1], &x_hb[2], &x_hb[3], &x_hb[4], &x_hb[5]))) {
		x_cvtbase(256, 256, x_hb, x_i, x_out, x_len);
		return;
	}
	/*
	 * try form 8E1#0:0:AA:0:5E:E6#socket
	 */
	if (1 < (x_i = x_sscanf(x_buf,"%x:%x:%x:%x:%x:%x",
			&x_hb[0], &x_hb[1], &x_hb[2], &x_hb[3], &x_hb[4], &x_hb[5]))) {
		x_cvtbase(256, 256, x_hb, x_i, x_out, x_len);
		return;
	}
	/*
	 * This is REALLY stretching it but there was a
	 * comma notation separting shorts -- definitely non standard
	 */
	if (1 < (x_i = x_sscanf(x_buf,"%x,%x,%x",
			&x_hb[0], &x_hb[1], &x_hb[2]))) {
		x_hb[0] = x_htons(x_hb[0]); x_hb[1] = x_htons(x_hb[1]);
		x_hb[2] = x_htons(x_hb[2]);
		x_cvtbase(65536, 256, x_hb, x_i, x_out, x_len);
		return;
	}

	/* Need to decide if base 10, 16 or 8 */
	while (*x_bp) switch (*x_bp++) {

	case '0': case '1': case '2': case '3': case '4': case '5':
	case '6': case '7': case '-':
		break;

	case '8': case '9':
		x_base10 = 1;
		break;

	case 'a': case 'b': case 'c': case 'd': case 'e': case 'f':
	case 'A': case 'B': case 'C': case 'D': case 'E': case 'F':
		x_base16 = 1;
		break;
	
	case 'x': case 'X':
		*--x_bp = '0';
		x_base16 = 1;
		break;

	case 'h': case 'H':
		x_base16 = 1;
		/* fall into */

	x_default:
		*--x_bp = 0; /* Ends Loop */
	}
	if (x_base16) {
		x_fmt = "%3x";
		x_ibase = 4096;
	} else if (x_base10 == 0 && *x_buf == '0') {
		x_fmt = "%3o";
		x_ibase = 512;
	} else {
		x_fmt = "%3d";
		x_ibase = 1000;
	}

	for (x_bp = x_buf; *x_bp++; ) x_clen++;
	if (x_clen == 0) x_clen++;
	if (x_clen > 18) x_clen = 18;
	x_i = ((x_clen - 1) / 3) + 1;
	x_bp = x_clen + x_buf - 3;
	x_hp = x_hb + x_i - 1;

	while (x_hp > x_hb) {
		x_sscanf(x_bp, x_fmt, x_hp);
		x_bp[0] = 0;
		x_hp--;
		x_bp -= 3;
	}
	x_sscanf(x_buf, x_fmt, x_hp);
	x_cvtbase(x_ibase, 256, x_hb, x_i, x_out, x_len);
}

static x_int x_cvtbase(x_oldbase, x_newbase, x_input, x_inlen, x_result, x_reslen) x_int x_oldbase; x_int x_newbase; x_int x_input[]; x_int x_inlen; unsigned char x_result[]; x_int x_reslen; {
	x_int x_d, x_e;
	x_long x_sum;

	x_e = 1;
	while (x_e > 0 && x_reslen > 0) {
		x_d = 0; x_e = 0; x_sum = 0;
		/* long division: input=input/newbase */
		while (x_d < x_inlen) {
			x_sum = x_sum*x_oldbase + (x_long) x_input[x_d];
			x_e += (x_sum > 0);
			x_input[x_d++] = x_sum / x_newbase;
			x_sum %= x_newbase;
		}
		x_result[--x_reslen] = x_sum;	/* accumulate remainder */
	}
	for (x_d=0; x_d < x_reslen; x_d++)
		x_result[x_d] = 0;
}
