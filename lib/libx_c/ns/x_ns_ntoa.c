#include "x_.h"

#include <x_stdio.h>
/*
 * Copyright (c) 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)ns_ntoa.c	6.3 (Berkeley) 3/9/86";
#endif

#include <sys/x_types.h>
#include <netns/x_ns.h>

#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

static char *x_spectHex __P((char *x_p0));

char *x_ns_ntoa(x_addr) struct x_ns_addr x_addr; {
	static char x_obuf[40];
	char *x_spectHex();
	union { union x_ns_net x_net_e; x_u_long x_long_e; } x_net;
	x_u_short x_port = x_htons(x_addr.x_x_port);
	register char *x_cp;
	char *x_cp2;
	register x_u_char *x_up = x_addr.x_x_host.x_c_host;
	x_u_char *x_uplim = x_up + 6;

	x_net.x_net_e = x_addr.x_x_net;
	x_sprintf(x_obuf, "%lx", x_ntohl(x_net.x_long_e));
	x_cp = x_spectHex(x_obuf);
	x_cp2 = x_cp + 1;
	while (*x_up==0 && x_up < x_uplim) x_up++;
	if (x_up == x_uplim) {
		if (x_port) {
			x_sprintf(x_cp, ".0");
			x_cp += 2;
		}
	} else {
		x_sprintf(x_cp, ".%x", *x_up++);
		while (x_up < x_uplim) {
			while (*x_cp) x_cp++;
			x_sprintf(x_cp, "%02x", *x_up++);
		}
		x_cp = x_spectHex(x_cp2);
	}
	if (x_port) {
		x_sprintf(x_cp, ".%x", x_port);
		x_spectHex(x_cp + 1);
	}
	return (x_obuf);
}

static char *x_spectHex(x_p0) char *x_p0; {
	x_int x_ok = 0;
	x_int x_nonzero = 0;
	register char *x_p = x_p0;
	for (; *x_p; x_p++) switch (*x_p) {

	case 'a': case 'b': case 'c': case 'd': case 'e': case 'f':
		*x_p += ('A' - 'a');
		/* fall into . . . */
	case 'A': case 'B': case 'C': case 'D': case 'E': case 'F':
		x_ok = 1;
	case '1': case '2': case '3': case '4': case '5':
	case '6': case '7': case '8': case '9':
		x_nonzero = 1;
	}
	if (x_nonzero && !x_ok) { *x_p++ = 'H'; *x_p = 0; }
	return (x_p);
}
