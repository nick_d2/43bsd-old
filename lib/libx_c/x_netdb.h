#include "x_.h"

/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)netdb.h	5.7 (Berkeley) 5/12/86
 */

/*
 * Structures returned by network
 * data base library.  All addresses
 * are supplied in host order, and
 * returned in network order (suitable
 * for use in system calls).
 */
struct	x_hostent {
	char	*x_h_name;	/* official name of host */
	char	**x_h_aliases;	/* alias list */
	x_int	x_h_addrtype;	/* host address type */
	x_int	x_h_length;	/* length of address */
	char	**x_h_addr_list;	/* list of addresses from name server */
#define	x_h_addr	x_h_addr_list[0]	/* address, for backward compatiblity */
};

/*
 * Assumption here is that a network number
 * fits in 32 bits -- probably a poor one.
 */
struct	x_netent {
	char		*x_n_name;	/* official name of net */
	char		**x_n_aliases;	/* alias list */
	x_int		x_n_addrtype;	/* net address type */
	x_unsigned_long	x_n_net;		/* network # */
};

struct	x_servent {
	char	*x_s_name;	/* official service name */
	char	**x_s_aliases;	/* alias list */
	x_int	x_s_port;		/* port # */
	char	*x_s_proto;	/* protocol to use */
};

struct	x_protoent {
	char	*x_p_name;	/* official protocol name */
	char	**x_p_aliases;	/* alias list */
	x_int	x_p_proto;	/* protocol # */
};

struct x_hostent	*x_gethostbyname(), *x_gethostbyaddr(), *x_gethostent();
struct x_netent	*x_getnetbyname(), *x_getnetbyaddr(), *x_getnetent();
struct x_servent	*x_getservbyname(), *x_getservbyport(), *x_getservent();
struct x_protoent	*x_getprotobyname(), *x_getprotobynumber(), *x_getprotoent();

/*
 * Error return codes from gethostbyname() and gethostbyaddr()
 */

extern  x_int x_h_errno;	

#define	x_HOST_NOT_FOUND	1 /* Authoritive Answer Host not found */
#define	x_TRY_AGAIN	2 /* Non-Authoritive Host not found, or SERVERFAIL */
#define	x_NO_RECOVERY	3 /* Non recoverable errors, FORMERR, REFUSED, NOTIMP */
#define x_NO_ADDRESS	4 /* Valid host name, no address, look for MX record */

#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

/* net/getservent.c */
x_int x_setservent __P((x_int x_f));
x_int x_endservent __P((void));
struct x_servent *x_getservent __P((void));
/* net/getnetbyaddr.c */
struct x_netent *x_getnetbyaddr __P((register x_int x_net, register x_int x_type));
/* net/ruserpass.c */
x_int x_ruserpass __P((char *x_host, char **x_aname, char **x_apass));
x_int x_mkpwunclear __P((char *x_spasswd, x_int x_mch, char *x_sencpasswd));
x_int x_mkpwclear __P((char *x_sencpasswd, x_int x_mch, char *x_spasswd));
/* net/getnetbyname.c */
struct x_netent *x_getnetbyname __P((register char *x_name));
/* net/getservbyport.c */
struct x_servent *x_getservbyport __P((x_int x_port, char *x_proto));
/* net/getprotoent.c */
x_int x_setprotoent __P((x_int x_f));
x_int x_endprotoent __P((void));
struct x_protoent *x_getprotoent __P((void));
/* net/hosttable/gethostnamadr.c */
struct x_hostent *x_gethostbyname __P((register char *x_nam));
struct x_hostent *x_gethostbyaddr __P((char *x_addr, register x_int x_length, register x_int x_type));
/* net/hosttable/gethostent.c */
x_int x_sethostent __P((x_int x_f));
x_int x_endhostent __P((void));
struct x_hostent *x_gethostent __P((void));
x_int x_sethostfile __P((char *x_file));
/* net/getnetent.c */
x_int x_setnetent __P((x_int x_f));
x_int x_endnetent __P((void));
struct x_netent *x_getnetent __P((void));
/* net/rexec.c */
x_int x_rexec __P((char **x_ahost, x_int x_rport, char *x_name, char *x_pass, char *x_cmd, x_int *x_fd2p));
/* net/getservbyname.c */
struct x_servent *x_getservbyname __P((char *x_name, char *x_proto));
/* net/named/gethostnamadr.c */
struct x_hostent *x_gethostbyname __P((char *x_name));
struct x_hostent *x_gethostbyaddr __P((char *x_addr, x_int x_len, x_int x_type));
x_int x__sethtent __P((x_int x_f));
x_int x__endhtent __P((void));
struct x_hostent *x__gethtent __P((void));
struct x_hostent *x__gethtbyname __P((char *x_name));
struct x_hostent *x__gethtbyaddr __P((char *x_addr, x_int x_len, x_int x_type));
/* net/named/sethostent.c */
x_int x_sethostent __P((x_int x_stayopen));
x_int x_endhostent __P((void));
x_int x_sethostfile __P((char *x_name));
/* net/getproto.c */
struct x_protoent *x_getprotobynumber __P((register x_int x_proto));
/* net/getprotoname.c */
struct x_protoent *x_getprotobyname __P((register char *x_name));
