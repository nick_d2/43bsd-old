#include "x_.h"

/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)ftp.h	5.2 (Berkeley) 5/30/85
 */

/*
 * Definitions for FTP
 * See RFC-765
 */

/*
 * Reply codes.
 */
#define x_PRELIM		1	/* positive preliminary */
#define x_COMPLETE	2	/* positive completion */
#define x_CONTINUE	3	/* positive intermediate */
#define x_TRANSIENT	4	/* transient negative completion */
#define x_ERROR		5	/* permanent negative completion */

/*
 * Type codes
 */
#define	x_TYPE_A		1	/* ASCII */
#define	x_TYPE_E		2	/* EBCDIC */
#define	x_TYPE_I		3	/* image */
#define	x_TYPE_L		4	/* local byte size */

/*
 * Form codes
 */
#define	x_FORM_N		1	/* non-print */
#define	x_FORM_T		2	/* telnet format effectors */
#define	x_FORM_C		3	/* carriage control (ASA) */

/*
 * Structure codes
 */
#define	x_STRU_F		1	/* file (no record structure) */
#define	x_STRU_R		2	/* record structure */
#define	x_STRU_P		3	/* page structure */

/*
 * Mode types
 */
#define	x_MODE_S		1	/* stream */
#define	x_MODE_B		2	/* block */
#define	x_MODE_C		3	/* compressed */

/*
 * Record Tokens
 */
#define	x_REC_ESC		'\377'	/* Record-mode Escape */
#define	x_REC_EOR		'\001'	/* Record-mode End-of-Record */
#define x_REC_EOF		'\002'	/* Record-mode End-of-File */

/*
 * Block Header
 */
#define	x_BLK_EOR		0x80	/* Block is End-of-Record */
#define	x_BLK_EOF		0x40	/* Block is End-of-File */
#define x_BLK_ERRORS	0x20	/* Block is suspected of containing errors */
#define	x_BLK_RESTART	0x10	/* Block is Restart Marker */

#define	x_BLK_BYTECOUNT	2	/* Bytes in this block */
