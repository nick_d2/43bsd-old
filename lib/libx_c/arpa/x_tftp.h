#include "x_.h"

/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)tftp.h	5.1 (Berkeley) 5/30/85
 */

/*
 * Trivial File Transfer Protocol (IEN-133)
 */
#define	x_SEGSIZE		512		/* data segment size */

/*
 * Packet types.
 */
#define	x_RRQ	01			/* read request */
#define	x_WRQ	02			/* write request */
#define	x_DATA	03			/* data packet */
#define	x_ACK	04			/* acknowledgement */
#define	x_ERROR	05			/* error code */

struct	x_tftphdr {
	x_short	x_th_opcode;		/* packet type */
	union {
		x_short	x_tu_block;	/* block # */
		x_short	x_tu_code;	/* error code */
		char	x_tu_stuff[1];	/* request packet stuff */
	} x_th_u;
	char	x_th_data[1];		/* data or error string */
};

#define	x_th_block	x_th_u.x_tu_block
#define	x_th_code		x_th_u.x_tu_code
#define	x_th_stuff	x_th_u.x_tu_stuff
#define	x_th_msg		x_th_data

/*
 * Error codes.
 */
#define	x_EUNDEF		0		/* not defined */
#define	x_ENOTFOUND	1		/* file not found */
#define	x_EACCESS		2		/* access violation */
#define	x_ENOSPACE	3		/* disk full or allocation exceeded */
#define	x_EBADOP		4		/* illegal TFTP operation */
#define	x_EBADID		5		/* unknown transfer ID */
#define	x_EEXISTS		6		/* file already exists */
#define	x_ENOUSER		7		/* no such user */
