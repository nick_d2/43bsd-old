#include "x_.h"

/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)nameser.h	5.11 (Berkeley) 2/14/86
 */

/*
 * Define constants based on rfc883
 */
#define x_PACKETSZ	512		/* maximum packet size */
#define x_MAXDNAME	256		/* maximum domain name */
#define x_MAXCDNAME	255		/* maximum compressed domain name */
#define x_MAXLABEL	63		/* maximum length of domain label */
	/* Number of bytes of fixed size data in query structure */
#define x_QFIXEDSZ	4
	/* number of bytes of fixed size data in resource record */
#define x_RRFIXEDSZ	10

/*
 * Internet nameserver port number
 */
#define x_NAMESERVER_PORT	53

/*
 * Currently defined opcodes
 */
#define x_QUERY		0		/* standard query */
#define x_IQUERY		1		/* inverse query */
#define x_CQUERYM		2		/* completion query (multiple) */
#define x_CQUERYU		3		/* completion query (unique) */
	/* non standard */
#define x_UPDATEA		100		/* add resource record */
#define x_UPDATED		101		/* delete resource record */
#define x_UPDATEM		102		/* modify resource record */
#define x_ZONEINIT	103		/* initial zone transfer */
#define x_ZONEREF		104		/* incremental zone referesh */

/*
 * Currently defined response codes
 */
#define x_NOERROR		0		/* no error */
#define x_FORMERR		1		/* format error */
#define x_SERVFAIL	2		/* server failure */
#define x_NXDOMAIN	3		/* non existent domain */
#define x_NOTIMP		4		/* not implemented */
#define x_REFUSED		5		/* query refused */
	/* non standard */
#define x_NOCHANGE	100		/* update failed to change db */

/*
 * Type values for resources and queries
 */
#define x_T_A		1		/* host address */
#define x_T_NS		2		/* authoritative server */
#define x_T_MD		3		/* mail destination */
#define x_T_MF		4		/* mail forwarder */
#define x_T_CNAME		5		/* connonical name */
#define x_T_SOA		6		/* start of authority zone */
#define x_T_MB		7		/* mailbox domain name */
#define x_T_MG		8		/* mail group member */
#define x_T_MR		9		/* mail rename name */
#define x_T_NULL		10		/* null resource record */
#define x_T_WKS		11		/* well known service */
#define x_T_PTR		12		/* domain name pointer */
#define x_T_HINFO		13		/* host information */
#define x_T_MINFO		14		/* mailbox information */
#define x_T_MX		15		/* mail routing information */
	/* non standard */
#define x_T_UINFO		100		/* user (finger) information */
#define x_T_UID		101		/* user ID */
#define x_T_GID		102		/* group ID */
	/* Query type values which do not appear in resource records */
#define x_T_AXFR		252		/* transfer zone of authority */
#define x_T_MAILB		253		/* transfer mailbox records */
#define x_T_MAILA		254		/* transfer mail agent records */
#define x_T_ANY		255		/* wildcard match */

/*
 * Values for class field
 */

#define x_C_IN		1		/* the arpa internet */
#define x_C_CHAOS		3		/* for chaos net at MIT */
	/* Query class values which do not appear in resource records */
#define x_C_ANY		255		/* wildcard match */

/*
 * Structure for query header, the order of the fields is machine and
 * compiler dependent, in our case, the bits within a byte are assignd 
 * least significant first, while the order of transmition is most 
 * significant first.  This requires a somewhat confusing rearrangement.
 */

typedef struct {
	x_u_short	x_id;		/* query identification number */
#if defined (x_sun) || defined (x_sel) || defined (x_pyr) || defined (x_is68k) \
|| x_defined (x_tahoe) || x_defined (x_BIT_ZERO_ON_LEFT)
	/* Bit zero on left:  Gould and similar architectures */
			/* fields in third byte */
	x_u_char	x_qr:1;		/* response flag */
	x_u_char	x_opcode:4;	/* purpose of message */
	x_u_char	x_aa:1;		/* authoritive answer */
	x_u_char	x_tc:1;		/* truncated message */
	x_u_char	x_rd:1;		/* recursion desired */
			/* fields in fourth byte */
	x_u_char	x_ra:1;		/* recursion available */
	x_u_char	x_pr:1;		/* primary server required (non standard) */
	x_u_char	x_unused:2;	/* unused bits */
	x_u_char	x_rcode:4;	/* response code */
#else
#if defined (x_vax) || defined (x_BIT_ZERO_ON_RIGHT)
	/* Bit zero on right:  VAX */
			/* fields in third byte */
	x_u_char	x_rd:1;		/* recursion desired */
	x_u_char	x_tc:1;		/* truncated message */
	x_u_char	x_aa:1;		/* authoritive answer */
	x_u_char	x_opcode:4;	/* purpose of message */
	x_u_char	x_qr:1;		/* response flag */
			/* fields in fourth byte */
	x_u_char	x_rcode:4;	/* response code */
	x_u_char	x_unused:2;	/* unused bits */
	x_u_char	x_pr:1;		/* primary server required (non standard) */
	x_u_char	x_ra:1;		/* recursion available */
#else
	/* you must determine what the correct bit order is for your compiler */
	x_UNDEFINED_BIT_ORDER;
#endif
#endif
			/* remaining bytes */
	x_u_short	x_qdcount;	/* number of question entries */
	x_u_short	x_ancount;	/* number of answer entries */
	x_u_short	x_nscount;	/* number of authority entries */
	x_u_short	x_arcount;	/* number of resource entries */
} x_HEADER;

/*
 * Defines for handling compressed domain names
 */
#define x_INDIR_MASK	0xc0

/*
 * Structure for passing resource records around.
 */
struct x_rrec {
	x_short	x_r_zone;			/* zone number */
	x_short	x_r_class;		/* class number */
	x_short	x_r_type;			/* type number */
	x_u_long	x_r_ttl;			/* time to live */
	x_int	x_r_size;			/* size of data area */
	char	*x_r_data;		/* pointer to data */
};

extern	x_u_short	x_getshort();
extern	x_u_long	x_getlong();

#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

/* net/res_send.c */
x_int x_res_send __P((char *x_buf, x_int x_buflen, char *x_answer, x_int x_anslen));
x_int x__res_close __P((void));
/* net/res_comp.c */
x_int x_dn_expand __P((char *x_msg, char *x_eomorig, char *x_comp_dn, char *x_exp_dn, x_int x_length));
x_int x_dn_comp __P((char *x_exp_dn, char *x_comp_dn, x_int x_length, char **x_dnptrs, char **x_lastdnptr));
x_int x_dn_skip __P((char *x_comp_dn));
x_int x_dn_find __P((char *x_exp_dn, char *x_msg, char **x_dnptrs, char **x_lastdnptr));
x_u_short x_getshort __P((char *x_msgp));
x_u_long x_getlong __P((char *x_msgp));
x_int x_putshort __P((x_int x_s, register char *x_msgp));
x_int x_putlong __P((register x_int x_u_long x_l, register char *x_msgp));
/* net/res_init.c */
x_int x_res_init __P((void));
/* net/res_mkquery.c */
x_int x_res_mkquery __P((x_int x_op, char *x_dname, x_int x_class, x_int x_type, char *x_data, x_int x_datalen, struct x_rrec *x_newrr, char *x_buf, x_int x_buflen));
