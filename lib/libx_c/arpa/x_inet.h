#include "x_.h"

/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)inet.h	5.1 (Berkeley) 5/30/85
 */

/*
 * External definitions for
 * functions in inet(3N)
 */
x_unsigned_long x_inet_addr();
char	*x_inet_ntoa();
struct	x_in_addr x_inet_makeaddr();
x_unsigned_long x_inet_network();

#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

/* inet/inet_network.c */
x_u_long x_inet_network __P((register char *x_cp));
/* inet/inet_makeaddr.c */
struct x_in_addr x_inet_makeaddr __P((x_int x_net, x_int x_host));
/* inet/inet_addr.c */
x_u_long x_inet_addr __P((register char *x_cp));
/* inet/inet_ntoa.c */
char *x_inet_ntoa __P((struct x_in_addr x_in));
