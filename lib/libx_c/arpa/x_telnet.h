#include "x_.h"

/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)telnet.h	5.1 (Berkeley) 5/30/85
 */

/*
 * Definitions for the TELNET protocol.
 */
#define	x_IAC	255		/* interpret as command: */
#define	x_DONT	254		/* you are not to use option */
#define	x_DO	253		/* please, you use option */
#define	x_WONT	252		/* I won't use option */
#define	x_WILL	251		/* I will use option */
#define	x_SB	250		/* interpret as subnegotiation */
#define	x_GA	249		/* you may reverse the line */
#define	x_EL	248		/* erase the current line */
#define	x_EC	247		/* erase the current character */
#define	x_AYT	246		/* are you there */
#define	x_AO	245		/* abort output--but let prog finish */
#define	x_IP	244		/* interrupt process--permanently */
#define	x_BREAK	243		/* break */
#define	x_DM	242		/* data mark--for connect. cleaning */
#define	x_NOP	241		/* nop */
#define	x_SE	240		/* end sub negotiation */
#define x_EOR     239             /* end of record (transparent mode) */

#define x_SYNCH	242		/* for telfunc calls */

#ifdef x_TELCMDS
char *x_telcmds[] = {
	"SE", "NOP", "DMARK", "BRK", "IP", "AO", "AYT", "EC",
	"EL", "GA", "SB", "WILL", "WONT", "DO", "DONT", "IAC",
};
#endif

/* telnet options */
#define x_TELOPT_BINARY	0	/* 8-bit data path */
#define x_TELOPT_ECHO	1	/* echo */
#define	x_TELOPT_RCP	2	/* prepare to reconnect */
#define	x_TELOPT_SGA	3	/* suppress go ahead */
#define	x_TELOPT_NAMS	4	/* approximate message size */
#define	x_TELOPT_STATUS	5	/* give status */
#define	x_TELOPT_TM	6	/* timing mark */
#define	x_TELOPT_RCTE	7	/* remote controlled transmission and echo */
#define x_TELOPT_NAOL 	8	/* negotiate about output line width */
#define x_TELOPT_NAOP 	9	/* negotiate about output page size */
#define x_TELOPT_NAOCRD	10	/* negotiate about CR disposition */
#define x_TELOPT_NAOHTS	11	/* negotiate about horizontal tabstops */
#define x_TELOPT_NAOHTD	12	/* negotiate about horizontal tab disposition */
#define x_TELOPT_NAOFFD	13	/* negotiate about formfeed disposition */
#define x_TELOPT_NAOVTS	14	/* negotiate about vertical tab stops */
#define x_TELOPT_NAOVTD	15	/* negotiate about vertical tab disposition */
#define x_TELOPT_NAOLFD	16	/* negotiate about output LF disposition */
#define x_TELOPT_XASCII	17	/* extended ascic character set */
#define	x_TELOPT_LOGOUT	18	/* force logout */
#define	x_TELOPT_BM	19	/* byte macro */
#define	x_TELOPT_DET	20	/* data entry terminal */
#define	x_TELOPT_SUPDUP	21	/* supdup protocol */
#define	x_TELOPT_SUPDUPOUTPUT 22	/* supdup output */
#define	x_TELOPT_SNDLOC	23	/* send location */
#define	x_TELOPT_TTYPE	24	/* terminal type */
#define	x_TELOPT_EOR	25	/* end or record */
#define x_TELOPT_EXOPL	255	/* extended-options-list */

#ifdef x_TELOPTS
#define	x_NTELOPTS	(1+x_TELOPT_EOR)
char *x_telopts[x_NTELOPTS] = {
	"BINARY", "ECHO", "RCP", "SUPPRESS GO AHEAD", "NAME",
	"STATUS", "TIMING MARK", "RCTE", "NAOL", "NAOP",
	"NAOCRD", "NAOHTS", "NAOHTD", "NAOFFD", "NAOVTS",
	"NAOVTD", "NAOLFD", "EXTEND ASCII", "LOGOUT", "BYTE MACRO",
	"DATA ENTRY TERMINAL", "SUPDUP", "SUPDUP OUTPUT",
	"SEND LOCATION", "TERMINAL TYPE", "END OF RECORD",
};
#endif

/* sub-option qualifiers */
#define	x_TELQUAL_IS	0	/* option is... */
#define	x_TELQUAL_SEND	1	/* send option */
