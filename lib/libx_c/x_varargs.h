#include "x_.h"

/*	varargs.h	4.1	83/05/03	*/

typedef char *va_list;
# define va_dcl x_int va_alist;
# define va_start(x_list) x_list = (char *) &va_alist
# define va_end(x_list)
# define va_arg(x_list,x_mode) ((x_mode *)(x_list += sizeof(x_mode)))[-1]
