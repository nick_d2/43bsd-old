#include "x_.h"

/*	pwd.h	4.1	83/05/03	*/

struct	x_passwd { /* see getpwent(3) */
	char	*x_pw_name;
	char	*x_pw_passwd;
	x_int	x_pw_uid;
	x_int	x_pw_gid;
	x_int	x_pw_quota;
	char	*x_pw_comment;
	char	*x_pw_gecos;
	char	*x_pw_dir;
	char	*x_pw_shell;
};

struct x_passwd *x_getpwent(), *x_getpwuid(), *x_getpwnam();

#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

/* compat-4.1/getpw.c */
x_int x_getpw __P((x_int x_uid, char x_buf[]));
/* gen/getpwnamuid.c */
struct x_passwd *x_getpwnam __P((char *x_nam));
struct x_passwd *x_getpwuid __P((x_int x_uid));
/* gen/crypt.c */
x_int x_setkey __P((char *x_key));
x_int x_encrypt __P((char *x_block, x_int x_edflag));
char *x_crypt __P((char *x_pw, char *x_salt));
/* gen/getpwent.c */
x_int x_setpwent __P((void));
x_int x_endpwent __P((void));
struct x_passwd *x_getpwent __P((void));
x_int x_setpwfile __P((char *x_file));
/* gen/initgroups.c */
x_int x_initgroups __P((char *x_uname, x_int x_agroup));
