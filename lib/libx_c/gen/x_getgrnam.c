#include "x_.h"

#include <x_strings.h>
#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)getgrnam.c	5.2 (Berkeley) 3/9/86";
#endif

#include <x_grp.h>

struct x_group *x_getgrnam(x_name) register char *x_name; {
	register struct x_group *x_p;
	struct x_group *x_getgrent();

	x_setgrent();
	while( (x_p = x_getgrent()) && x_strcmp(x_p->x_gr_name,x_name) );
	x_endgrent();
	return(x_p);
}
