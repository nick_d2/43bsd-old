#include "x_.h"

#include <x_stdlib.h>
#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)getgrent.c	5.2 (Berkeley) 3/9/86";
#endif

#include <x_stdio.h>
#include <x_grp.h>

#define	x_MAXGRP	200

static char x_GROUP[] = "/etc/group";
static x_FILE *x_grf = x_NULL;
static char x_line[x_BUFSIZ+1];
static struct x_group x_group;
static char *x_gr_mem[x_MAXGRP];

#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

static char *x_grskip __P((register char *x_p, register x_int x_c));

x_int x_setgrent() {
	if( !x_grf )
		x_grf = x_fopen( x_GROUP, "r" );
	else
		x_rewind( x_grf );
}

x_int x_endgrent() {
	if( x_grf ){
		x_fclose( x_grf );
		x_grf = x_NULL;
	}
}

static char *x_grskip(x_p, x_c) register char *x_p; register x_int x_c; {
	while( *x_p && *x_p != x_c ) ++x_p;
	if( *x_p ) *x_p++ = 0;
	return( x_p );
}

struct x_group *x_getgrent() {
	register char *x_p, **x_q;

	if( !x_grf && !(x_grf = x_fopen( x_GROUP, "r" )) )
		return(x_NULL);
	if( !(x_p = x_fgets( x_line, x_BUFSIZ, x_grf )) )
		return(x_NULL);
	x_group.x_gr_name = x_p;
	x_group.x_gr_passwd = x_p = x_grskip(x_p,':');
	x_group.x_gr_gid = x_atoi( x_p = x_grskip(x_p,':') );
	x_group.x_gr_mem = x_gr_mem;
	x_p = x_grskip(x_p,':');
	x_grskip(x_p,'\n');
	x_q = x_gr_mem;
	while( *x_p ){
		if (x_q < &x_gr_mem[x_MAXGRP-1])
			*x_q++ = x_p;
		x_p = x_grskip(x_p,',');
	}
	*x_q = x_NULL;
	return( &x_group );
}
