#include "x_.h"

#include <x_strings.h>
/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)fstab.c	5.2 (Berkeley) 3/9/86";
#endif

#include <x_fstab.h>
#include <x_stdio.h>
#include <x_ctype.h>

static	struct x_fstab x_fs;
static	char x_line[x_BUFSIZ+1];
static	x_FILE *x_fs_file = 0;

#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

static char *x_fsskip __P((register char *x_p));
static char *x_fsdigit __P((x_int *x_backp, char *x_string, x_int x_end));
static x_int x_fstabscan __P((struct x_fstab *x_fs));

static char *x_fsskip(x_p) register char *x_p; {

	while (*x_p && *x_p != ':')
		++x_p;
	if (*x_p)
		*x_p++ = 0;
	return (x_p);
}

static char *x_fsdigit(x_backp, x_string, x_end) x_int *x_backp; char *x_string; x_int x_end; {
	register x_int x_value = 0;
	register char *x_cp;

	for (x_cp = x_string; *x_cp && x_isdigit(*x_cp); x_cp++) {
		x_value *= 10;
		x_value += *x_cp - '0';
	}
	if (*x_cp == '\0')
		return ((char *)0);
	*x_backp = x_value;
	while (*x_cp && *x_cp != x_end)
		x_cp++;
	if (*x_cp == '\0')
		return ((char *)0);
	return (x_cp+1);
}

static x_int x_fstabscan(x_fs) struct x_fstab *x_fs; {
	register char *x_cp;

	x_cp = x_fgets(x_line, 256, x_fs_file);
	if (x_cp == x_NULL)
		return (x_EOF);
	x_fs->x_fs_spec = x_cp;
	x_cp = x_fsskip(x_cp);
	x_fs->x_fs_file = x_cp;
	x_cp = x_fsskip(x_cp);
	x_fs->x_fs_type = x_cp;
	x_cp = x_fsskip(x_cp);
	x_cp = x_fsdigit(&x_fs->x_fs_freq, x_cp, ':');
	if (x_cp == 0)
		return (3);
	x_cp = x_fsdigit(&x_fs->x_fs_passno, x_cp, '\n');
	if (x_cp == 0)
		return (4);
	return (5);
}
	
x_int x_setfsent() {

	if (x_fs_file)
		x_endfsent();
	if ((x_fs_file = x_fopen(x_FSTAB, "r")) == x_NULL) {
		x_fs_file = 0;
		return (0);
	}
	return (1);
}

x_int x_endfsent() {

	if (x_fs_file) {
		x_fclose(x_fs_file);
		x_fs_file = 0;
	}
	return (1);
}

struct x_fstab *x_getfsent() {
	x_int x_nfields;

	if ((x_fs_file == 0) && (x_setfsent() == 0))
		return ((struct x_fstab *)0);
	x_nfields = x_fstabscan(&x_fs);
	if (x_nfields == x_EOF || x_nfields != 5)
		return ((struct x_fstab *)0);
	return (&x_fs);
}

struct x_fstab *x_getfsspec(x_name) char *x_name; {
	register struct x_fstab *x_fsp;

	if (x_setfsent() == 0)	/* start from the beginning */
		return ((struct x_fstab *)0);
	while((x_fsp = x_getfsent()) != 0)
		if (x_strcmp(x_fsp->x_fs_spec, x_name) == 0)
			return (x_fsp);
	return ((struct x_fstab *)0);
}

struct x_fstab *x_getfsfile(x_name) char *x_name; {
	register struct x_fstab *x_fsp;

	if (x_setfsent() == 0)	/* start from the beginning */
		return ((struct x_fstab *)0);
	while ((x_fsp = x_getfsent()) != 0)
		if (x_strcmp(x_fsp->x_fs_file, x_name) == 0)
			return (x_fsp);
	return ((struct x_fstab *)0);
}

struct x_fstab *x_getfstype(x_type) char *x_type; {
	register struct x_fstab *x_fs;

	if (x_setfsent() == 0)
		return ((struct x_fstab *)0);
	while ((x_fs = x_getfsent()) != 0)
		if (x_strcmp(x_fs->x_fs_type, x_type) == 0)
			return (x_fs);
	return ((struct x_fstab *)0);
}
