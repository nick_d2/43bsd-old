#include "x_.h"

#include <x_stdlib.h>
#include <x_unistd.h>
#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)system.c	5.2 (Berkeley) 3/9/86";
#endif

#include	<x_signal.h>

x_int x_system(x_s) char *x_s; {
	x_int x_status, x_pid, x_w;
	register x_int (*x_istat)(), (*x_qstat)();

	if ((x_pid = x_vfork()) == 0) {
		x_execl("/bin/sh", "sh", "-c", x_s, 0);
		x__exit(127);
	}
	x_istat = x_signal(x_SIGINT, x_SIG_IGN);
	x_qstat = x_signal(x_SIGQUIT, x_SIG_IGN);
	while ((x_w = x_wait(&x_status)) != x_pid && x_w != -1)
		;
	if (x_w == -1)
		x_status = -1;
	x_signal(x_SIGINT, x_istat);
	x_signal(x_SIGQUIT, x_qstat);
	return(x_status);
}
