#include "x_.h"

#include <x_stdlib.h>
#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)gcvt.c	5.2 (Berkeley) 3/9/86";
#endif

/*
 * gcvt  - Floating output conversion to
 * minimal length string
 */

char	*x_ecvt();

char *x_gcvt(x_number, x_ndigit, x_buf) double x_number; x_int x_ndigit; char *x_buf; {
	x_int x_sign, x_decpt;
	register char *x_p1, *x_p2;
	register x_int x_i;

	x_p1 = x_ecvt(x_number, x_ndigit, &x_decpt, &x_sign);
	x_p2 = x_buf;
	if (x_sign)
		*x_p2++ = '-';
	for (x_i=x_ndigit-1; x_i>0 && x_p1[x_i]=='0'; x_i--)
		x_ndigit--;
	if (x_decpt >= 0 && x_decpt-x_ndigit > 4
	 || x_decpt < 0 && x_decpt < -3) { /* use E-style */
		x_decpt--;
		*x_p2++ = *x_p1++;
		*x_p2++ = '.';
		for (x_i=1; x_i<x_ndigit; x_i++)
			*x_p2++ = *x_p1++;
		*x_p2++ = 'e';
		if (x_decpt<0) {
			x_decpt = -x_decpt;
			*x_p2++ = '-';
		} else
			*x_p2++ = '+';
		*x_p2++ = x_decpt/10 + '0';
		*x_p2++ = x_decpt%10 + '0';
	} else {
		if (x_decpt<=0) {
			if (*x_p1!='0')
				*x_p2++ = '.';
			while (x_decpt<0) {
				x_decpt++;
				*x_p2++ = '0';
			}
		}
		for (x_i=1; x_i<=x_ndigit; x_i++) {
			*x_p2++ = *x_p1++;
			if (x_i==x_decpt)
				*x_p2++ = '.';
		}
		if (x_ndigit<x_decpt) {
			while (x_ndigit++<x_decpt)
				*x_p2++ = '0';
			*x_p2++ = '.';
		}
	}
	if (x_p2[-1]=='.')
		x_p2--;
	*x_p2 = '\0';
	return(x_buf);
}
