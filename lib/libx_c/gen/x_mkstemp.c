#include "x_.h"

#include <x_stdlib.h>
#include <x_unistd.h>
/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)mkstemp.c	5.2 (Berkeley) 3/9/86";
#endif

#include <sys/x_file.h>

x_int x_mkstemp(x_as) char *x_as; {
	register char *x_s;
	register x_unsigned_int x_pid;
	register x_int x_fd, x_i;

	x_pid = x_getpid();
	x_s = x_as;
	while (*x_s++)
		/* void */;
	x_s--;
	while (*--x_s == 'X') {
		*x_s = (x_pid % 10) + '0';
		x_pid /= 10;
	}
	x_s++;
	x_i = 'a';
	while ((x_fd = x_open(x_as, x_O_CREAT|x_O_EXCL|x_O_RDWR, 0600)) == -1) {
		if (x_i == 'z')
			return(-1);
		*x_s = x_i++;
	}
	return(x_fd);
}
