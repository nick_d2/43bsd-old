#include "x_.h"

#include <x_mp.h>
#include <x_unistd.h>
/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)popen.c	5.4 (Berkeley) 3/26/86";
#endif

#include <x_stdio.h>
#include <x_signal.h>

#define	x_tst(x_a,x_b)	(*x_mode == 'r'? (x_b) : (x_a))
#define	x_RDR	0
#define	x_WTR	1

extern	char *x_malloc();

static	x_int *x_popen_pid;
static	x_int x_nfiles;

x_FILE *x_popen(x_cmd, x_mode) char *x_cmd; char *x_mode; {
	x_int x_p[2];
	x_int x_myside, x_hisside, x_pid;

	if (x_nfiles <= 0)
		x_nfiles = x_getdtablesize();
	if (x_popen_pid == x_NULL) {
		x_popen_pid = (x_int *)x_malloc(x_nfiles * sizeof *x_popen_pid);
		if (x_popen_pid == x_NULL)
			return (x_NULL);
		for (x_pid = 0; x_pid < x_nfiles; x_pid++)
			x_popen_pid[x_pid] = -1;
	}
	if (x_pipe(x_p) < 0)
		return (x_NULL);
	x_myside = x_tst(x_p[x_WTR], x_p[x_RDR]);
	x_hisside = x_tst(x_p[x_RDR], x_p[x_WTR]);
	if ((x_pid = x_vfork()) == 0) {
		/* myside and hisside reverse roles in child */
		x_close(x_myside);
		if (x_hisside != x_tst(0, 1)) {
			x_dup2(x_hisside, x_tst(0, 1));
			x_close(x_hisside);
		}
		x_execl("/bin/sh", "sh", "-c", x_cmd, (char *)x_NULL);
		x__exit(127);
	}
	if (x_pid == -1) {
		x_close(x_myside);
		x_close(x_hisside);
		return (x_NULL);
	}
	x_popen_pid[x_myside] = x_pid;
	x_close(x_hisside);
	return (x_fdopen(x_myside, x_mode));
}

x_int x_pclose(x_ptr) x_FILE *x_ptr; {
	x_int x_child, x_pid, x_status, x_omask;

	x_child = x_popen_pid[x_fileno(x_ptr)];
	x_popen_pid[x_fileno(x_ptr)] = -1;
	x_fclose(x_ptr);
	if (x_child == -1)
		return (-1);
	x_omask = x_sigblock(x_sigmask(x_SIGINT)|x_sigmask(x_SIGQUIT)|x_sigmask(x_SIGHUP));
	while ((x_pid = x_wait(&x_status)) != x_child && x_pid != -1)
		;
	(void) x_sigsetmask(x_omask);
	return (x_pid == -1 ? -1 : x_status);
}
