#include "x_.h"

#include <x_stdlib.h>
#include <x_mp.h>
#include <x_unistd.h>
/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)malloc.c	5.6 (Berkeley) 3/9/86";
#endif

/*
 * malloc.c (Caltech) 2/21/82
 * Chris Kingsley, kingsley@cit-20.
 *
 * This is a very fast storage allocator.  It allocates blocks of a small 
 * number of different sizes, and keeps free lists of each size.  Blocks that
 * don't exactly fit are passed up to the next larger size.  In this 
 * implementation, the available sizes are 2^n-4 (or 2^n-10) bytes long.
 * This is designed for use in a virtual memory environment.
 */

#include <sys/x_types.h>

#define	x_NULL 0

/*
 * The overhead on a block is at least 4 bytes.  When free, this space
 * contains a pointer to the next free block, and the bottom two bits must
 * be zero.  When in use, the first byte is set to MAGIC, and the second
 * byte is the size index.  The remaining bytes are for alignment.
 * If range checking is enabled then a second word holds the size of the
 * requested block, less 1, rounded up to a multiple of sizeof(RMAGIC).
 * The order of elements is critical: ov_magic must overlay the low order
 * bits of ov_next, and ov_magic can not be a valid ov_next bit pattern.
 */
union	x_overhead {
	union	x_overhead *x_ov_next;	/* when free */
	struct {
		x_u_char	x_ovu_magic;	/* magic number */
		x_u_char	x_ovu_index;	/* bucket # */
#ifdef x_RCHECK
		x_u_short	x_ovu_rmagic;	/* range magic number */
		x_u_int	x_ovu_size;	/* actual block size */
#endif
	} x_ovu;
#define	x_ov_magic	x_ovu.x_ovu_magic
#define	x_ov_index	x_ovu.x_ovu_index
#define	x_ov_rmagic	x_ovu.x_ovu_rmagic
#define	x_ov_size		x_ovu.x_ovu_size
};

#define	x_MAGIC		0xef		/* magic # on accounting info */
#define x_RMAGIC		0x5555		/* magic # on range info */

#ifdef x_RCHECK
#define	x_RSLOP		sizeof (x_u_short)
#else
#define	x_RSLOP		0
#endif

/*
 * nextf[i] is the pointer to the next free block of size 2^(i+3).  The
 * smallest allocatable block is 8 bytes.  The overhead information
 * precedes the data area returned to the user.
 */
#define	x_NBUCKETS 30
static	union x_overhead *x_nextf[x_NBUCKETS];
extern	char *x_sbrk();

static	x_int x_pagesz;			/* page size */
static	x_int x_pagebucket;			/* page size bucket */

#ifdef x_MSTATS
/*
 * nmalloc[i] is the difference between the number of mallocs and frees
 * for a given block size.
 */
static x_int	x_u_int x_nmalloc[x_NBUCKETS];
#include <x_stdio.h>
#endif

#if defined(x_DEBUG) || defined(x_RCHECK)
#define	x_ASSERT(x_p)   if (!(x_p)) x_botch("p")
#include <x_stdio.h>
#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

static x_int x_botch __P((char *x_s));
static x_int x_findbucket __P((union x_overhead *x_freep, x_int x_srchlen));

static x_int x_botch(x_s) char *x_s; {
	x_fprintf(x_stderr, "\r\nassertion botched: %s\r\n", x_s);
 	(void) x_fflush(x_stderr);		/* just in case user buffered it */
	x_abort();
}
#else
#define	x_ASSERT(x_p)
#endif

char *x_malloc(x_nbytes) x_unsigned_int x_nbytes; {
  	register union x_overhead *x_op;
  	register x_int x_bucket;
	register x_unsigned_int x_amt, x_n;

	/*
	 * First time malloc is called, setup page size and
	 * align break pointer so all data will be page aligned.
	 */
	if (x_pagesz == 0) {
		x_pagesz = x_n = x_getpagesize();
		x_op = (union x_overhead *)x_sbrk(0);
  		x_n = x_n - sizeof (*x_op) - ((x_int)x_op & (x_n - 1));
		if (x_n < 0)
			x_n += x_pagesz;
  		if (x_n) {
  			if (x_sbrk(x_n) == (char *)-1)
				return (x_NULL);
		}
		x_bucket = 0;
		x_amt = 8;
		while (x_pagesz > x_amt) {
			x_amt <<= 1;
			x_bucket++;
		}
		x_pagebucket = x_bucket;
	}
	/*
	 * Convert amount of memory requested into closest block size
	 * stored in hash buckets which satisfies request.
	 * Account for space used per block for accounting.
	 */
	if (x_nbytes <= (x_n = x_pagesz - sizeof (*x_op) - x_RSLOP)) {
#ifndef x_RCHECK
		x_amt = 8;	/* size of first bucket */
		x_bucket = 0;
#else
		x_amt = 16;	/* size of first bucket */
		x_bucket = 1;
#endif
		x_n = -(sizeof (*x_op) + x_RSLOP);
	} else {
		x_amt = x_pagesz;
		x_bucket = x_pagebucket;
	}
	while (x_nbytes > x_amt + x_n) {
		x_amt <<= 1;
		if (x_amt == 0)
			return (x_NULL);
		x_bucket++;
	}
	/*
	 * If nothing in hash bucket right now,
	 * request more memory from the system.
	 */
  	if ((x_op = x_nextf[x_bucket]) == x_NULL) {
  		x_morecore(x_bucket);
  		if ((x_op = x_nextf[x_bucket]) == x_NULL)
  			return (x_NULL);
	}
	/* remove from linked list */
  	x_nextf[x_bucket] = x_op->x_ov_next;
	x_op->x_ov_magic = x_MAGIC;
	x_op->x_ov_index = x_bucket;
#ifdef x_MSTATS
  	x_nmalloc[x_bucket]++;
#endif
#ifdef x_RCHECK
	/*
	 * Record allocated size of block and
	 * bound space with magic numbers.
	 */
	x_op->x_ov_size = (x_nbytes + x_RSLOP - 1) & ~(x_RSLOP - 1);
	x_op->x_ov_rmagic = x_RMAGIC;
  	*(x_u_short *)((x_caddr_t)(x_op + 1) + x_op->x_ov_size) = x_RMAGIC;
#endif
  	return ((char *)(x_op + 1));
}

/*
 * Allocate more memory to the indicated bucket.
 */
x_int x_morecore(x_bucket) x_int x_bucket; {
  	register union x_overhead *x_op;
	register x_int x_sz;		/* size of desired block */
  	x_int x_amt;			/* amount to allocate */
  	x_int x_nblks;			/* how many blocks we get */

	/*
	 * sbrk_size <= 0 only for big, FLUFFY, requests (about
	 * 2^30 bytes on a VAX, I think) or for a negative arg.
	 */
	x_sz = 1 << (x_bucket + 3);
#ifdef x_DEBUG
	x_ASSERT(x_sz > 0);
#else
	if (x_sz <= 0)
		return;
#endif
	if (x_sz < x_pagesz) {
		x_amt = x_pagesz;
  		x_nblks = x_amt / x_sz;
	} else {
		x_amt = x_sz + x_pagesz;
		x_nblks = 1;
	}
	x_op = (union x_overhead *)x_sbrk(x_amt);
	/* no more room! */
  	if ((x_int)x_op == -1)
  		return;
	/*
	 * Add new memory allocated to that on
	 * free list for this hash bucket.
	 */
  	x_nextf[x_bucket] = x_op;
  	while (--x_nblks > 0) {
		x_op->x_ov_next = (union x_overhead *)((x_caddr_t)x_op + x_sz);
		x_op = (union x_overhead *)((x_caddr_t)x_op + x_sz);
  	}
}

x_int x_free(x_cp) char *x_cp; {   
  	register x_int x_size;
	register union x_overhead *x_op;

  	if (x_cp == x_NULL)
  		return;
	x_op = (union x_overhead *)((x_caddr_t)x_cp - sizeof (union x_overhead));
#ifdef x_DEBUG
  	x_ASSERT(x_op->x_ov_magic == x_MAGIC);		/* make sure it was in use */
#else
	if (x_op->x_ov_magic != x_MAGIC)
		return;				/* sanity */
#endif
#ifdef x_RCHECK
  	x_ASSERT(x_op->x_ov_rmagic == x_RMAGIC);
	x_ASSERT(*(x_u_short *)((x_caddr_t)(x_op + 1) + x_op->x_ov_size) == x_RMAGIC);
#endif
  	x_size = x_op->x_ov_index;
  	x_ASSERT(x_size < x_NBUCKETS);
	x_op->x_ov_next = x_nextf[x_size];	/* also clobbers ov_magic */
  	x_nextf[x_size] = x_op;
#ifdef x_MSTATS
  	x_nmalloc[x_size]--;
#endif
}

/*
 * When a program attempts "storage compaction" as mentioned in the
 * old malloc man page, it realloc's an already freed block.  Usually
 * this is the last block it freed; occasionally it might be farther
 * back.  We have to search all the free lists for the block in order
 * to determine its bucket: 1st we make one pass thru the lists
 * checking only the first block in each; if that fails we search
 * ``realloc_srchlen'' blocks in each list for a match (the variable
 * is extern so the caller can modify it).  If that fails we just copy
 * however many bytes was given to realloc() and hope it's not huge.
 */
x_int x_realloc_srchlen = 4;	/* 4 should be plenty, -1 =>'s whole list */

char *x_realloc(x_cp, x_nbytes) char *x_cp; x_unsigned_int x_nbytes; {   
  	register x_int x_u_int x_onb, x_i;
	union x_overhead *x_op;
  	char *x_res;
	x_int x_was_alloced = 0;

  	if (x_cp == x_NULL)
  		return (x_malloc(x_nbytes));
	x_op = (union x_overhead *)((x_caddr_t)x_cp - sizeof (union x_overhead));
	if (x_op->x_ov_magic == x_MAGIC) {
		x_was_alloced++;
		x_i = x_op->x_ov_index;
	} else {
		/*
		 * Already free, doing "compaction".
		 *
		 * Search for the old block of memory on the
		 * free list.  First, check the most common
		 * case (last element free'd), then (this failing)
		 * the last ``realloc_srchlen'' items free'd.
		 * If all lookups fail, then assume the size of
		 * the memory block being realloc'd is the
		 * largest possible (so that all "nbytes" of new
		 * memory are copied into).  Note that this could cause
		 * a memory fault if the old area was tiny, and the moon
		 * is gibbous.  However, that is very unlikely.
		 */
		if ((x_i = x_findbucket(x_op, 1)) < 0 &&
		    (x_i = x_findbucket(x_op, x_realloc_srchlen)) < 0)
			x_i = x_NBUCKETS;
	}
	x_onb = 1 << (x_i + 3);
	if (x_onb < x_pagesz)
		x_onb -= sizeof (*x_op) + x_RSLOP;
	else
		x_onb += x_pagesz - sizeof (*x_op) - x_RSLOP;
	/* avoid the copy if same size block */
	if (x_was_alloced) {
		if (x_i) {
			x_i = 1 << (x_i + 2);
			if (x_i < x_pagesz)
				x_i -= sizeof (*x_op) + x_RSLOP;
			else
				x_i += x_pagesz - sizeof (*x_op) - x_RSLOP;
		}
		if (x_nbytes <= x_onb && x_nbytes > x_i) {
#ifdef x_RCHECK
			x_op->x_ov_size = (x_nbytes + x_RSLOP - 1) & ~(x_RSLOP - 1);
			*(x_u_short *)((x_caddr_t)(x_op + 1) + x_op->x_ov_size) = x_RMAGIC;
#endif
			return(x_cp);
		} else
			x_free(x_cp);
	}
  	if ((x_res = x_malloc(x_nbytes)) == x_NULL)
  		return (x_NULL);
  	if (x_cp != x_res)		/* common optimization if "compacting" */
		x_bcopy(x_cp, x_res, (x_nbytes < x_onb) ? x_nbytes : x_onb);
  	return (x_res);
}

/*
 * Search ``srchlen'' elements of each free list for a block whose
 * header starts at ``freep''.  If srchlen is -1 search the whole list.
 * Return bucket number, or -1 if not found.
 */
static x_int x_findbucket(x_freep, x_srchlen) union x_overhead *x_freep; x_int x_srchlen; {
	register union x_overhead *x_p;
	register x_int x_i, x_j;

	for (x_i = 0; x_i < x_NBUCKETS; x_i++) {
		x_j = 0;
		for (x_p = x_nextf[x_i]; x_p && x_j != x_srchlen; x_p = x_p->x_ov_next) {
			if (x_p == x_freep)
				return (x_i);
			x_j++;
		}
	}
	return (-1);
}

#ifdef x_MSTATS
/*
 * mstats - print out statistics about malloc
 * 
 * Prints two lines of numbers, one showing the length of the free list
 * for each size category, the second showing the number of mallocs -
 * frees for each size category.
 */
x_int x_mstats(x_s) char *x_s; {
  	register x_int x_i, x_j;
  	register union x_overhead *x_p;
  	x_int x_totfree = 0,
  	x_totused = 0;

  	x_fprintf(x_stderr, "Memory allocation statistics %s\nfree:\t", x_s);
  	for (x_i = 0; x_i < x_NBUCKETS; x_i++) {
  		for (x_j = 0, x_p = x_nextf[x_i]; x_p; x_p = x_p->x_ov_next, x_j++)
  			;
  		x_fprintf(x_stderr, " %d", x_j);
  		x_totfree += x_j * (1 << (x_i + 3));
  	}
  	x_fprintf(x_stderr, "\nused:\t");
  	for (x_i = 0; x_i < x_NBUCKETS; x_i++) {
  		x_fprintf(x_stderr, " %d", x_nmalloc[x_i]);
  		x_totused += x_nmalloc[x_i] * (1 << (x_i + 3));
  	}
  	x_fprintf(x_stderr, "\n\tTotal in use: %d, total free: %d\n",
	    x_totused, x_totfree);
}
#endif
