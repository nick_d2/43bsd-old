#include "x_.h"

#include <x_stdlib.h>
#include <x_unistd.h>
/*
 * Copyright (c) 1985 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)abort.c	5.3 (Berkeley) 3/9/86";
#endif

/* C library -- abort */

#include "x_signal.h"

x_int x_abort() {
	x_sigblock(~0);
	x_signal(x_SIGILL, x_SIG_DFL);
	x_sigsetmask(~x_sigmask(x_SIGILL));
	x_kill(x_getpid(), x_SIGILL);
}
