#include "x_.h"

#include <x_stdio.h>
#include <x_strings.h>
#include <x_unistd.h>
/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)perror.c	5.2 (Berkeley) 3/9/86";
#endif

/*
 * Print the error indicated
 * in the cerror cell.
 */
#include <sys/x_types.h>
#include <sys/x_uio.h>

x_int	x_errno;
x_int	x_sys_nerr;
char	*x_sys_errlist[];
x_int x_perror(x_s) char *x_s; {
	struct x_iovec x_iov[4];
	register struct x_iovec *x_v = x_iov;

	if (x_s && *x_s) {
		x_v->x_iov_base = x_s;
		x_v->x_iov_len = x_strlen(x_s);
		x_v++;
		x_v->x_iov_base = ": ";
		x_v->x_iov_len = 2;
		x_v++;
	}
	x_v->x_iov_base = x_errno < x_sys_nerr ? x_sys_errlist[x_errno] : "Unknown error";
	x_v->x_iov_len = x_strlen(x_v->x_iov_base);
	x_v++;
	x_v->x_iov_base = "\n";
	x_v->x_iov_len = 1;
	x_writev(2, x_iov, (x_v - x_iov) + 1);
}
