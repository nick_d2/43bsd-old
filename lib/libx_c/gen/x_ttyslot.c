#include "x_.h"

#include <x_strings.h>
#include <x_unistd.h>
/*
 * Copyright (c) 1984 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)ttyslot.c	5.2 (Berkeley) 3/9/86";
#endif

/*
 * Return the number of the slot in the utmp file
 * corresponding to the current user: try for file 0, 1, 2.
 * Definition is the line number in the /etc/ttys file.
 */
#include <x_ttyent.h>

char	*x_ttyname();
char	*x_rindex();

#define	x_NULL	0

x_int x_ttyslot() {
	register struct x_ttyent *x_ty;
	register char *x_tp, *x_p;
	register x_int x_s;

	if ((x_tp = x_ttyname(0)) == x_NULL &&
	    (x_tp = x_ttyname(1)) == x_NULL &&
	    (x_tp = x_ttyname(2)) == x_NULL)
		return(0);
	if ((x_p = x_rindex(x_tp, '/')) == x_NULL)
		x_p = x_tp;
	else
		x_p++;
	x_setttyent();
	x_s = 0;
	while ((x_ty = x_getttyent()) != x_NULL) {
		x_s++;
		if (x_strcmp(x_ty->x_ty_name, x_p) == 0) {
			x_endttyent();
			return (x_s);
		}
	}
	x_endttyent();
	return (0);
}
