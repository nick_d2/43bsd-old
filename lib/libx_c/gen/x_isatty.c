#include "x_.h"

#include <x_unistd.h>
#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)isatty.c	5.2 (Berkeley) 3/9/86";
#endif

/*
 * Returns 1 iff file is a tty
 */

#include <x_sgtty.h>

x_int x_isatty(x_f) x_int x_f; {
	struct x_sgttyb x_ttyb;

	if (x_ioctl(x_f, x_TIOCGETP, &x_ttyb) < 0)
		return(0);
	return(1);
}
