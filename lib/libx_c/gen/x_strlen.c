#include "x_.h"

#include <x_strings.h>
#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)strlen.c	5.2 (Berkeley) 3/9/86";
#endif

/*
 * Returns the number of
 * non-NULL bytes in string argument.
 */

x_int x_strlen(x_s) register char *x_s; {
	register x_int x_n;

	x_n = 0;
	while (*x_s++)
		x_n++;
	return(x_n);
}
