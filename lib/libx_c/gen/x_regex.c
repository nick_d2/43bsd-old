#include "x_.h"

#include <x_regex.h>
/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)regex.c	5.2 (Berkeley) 3/9/86";
#endif

#

/*
 * routines to do regular expression matching
 *
 * Entry points:
 *
 *	re_comp(s)
 *		char *s;
 *	 ... returns 0 if the string s was compiled successfully,
 *		     a pointer to an error message otherwise.
 *	     If passed 0 or a null string returns without changing
 *           the currently compiled re (see note 11 below).
 *
 *	re_exec(s)
 *		char *s;
 *	 ... returns 1 if the string s matches the last compiled regular
 *		       expression, 
 *		     0 if the string s failed to match the last compiled
 *		       regular expression, and
 *		    -1 if the compiled regular expression was invalid 
 *		       (indicating an internal error).
 *
 * The strings passed to both re_comp and re_exec may have trailing or
 * embedded newline characters; they are terminated by nulls.
 *
 * The identity of the author of these routines is lost in antiquity;
 * this is essentially the same as the re code in the original V6 ed.
 *
 * The regular expressions recognized are described below. This description
 * is essentially the same as that for ed.
 *
 *	A regular expression specifies a set of strings of characters.
 *	A member of this set of strings is said to be matched by
 *	the regular expression.  In the following specification for
 *	regular expressions the word `character' means any character but NUL.
 *
 *	1.  Any character except a special character matches itself.
 *	    Special characters are the regular expression delimiter plus
 *	    \ [ . and sometimes ^ * $.
 *	2.  A . matches any character.
 *	3.  A \ followed by any character except a digit or ( )
 *	    matches that character.
 *	4.  A nonempty string s bracketed [s] (or [^s]) matches any
 *	    character in (or not in) s. In s, \ has no special meaning,
 *	    and ] may only appear as the first letter. A substring 
 *	    a-b, with a and b in ascending ASCII order, stands for
 *	    the inclusive range of ASCII characters.
 *	5.  A regular expression of form 1-4 followed by * matches a
 *	    sequence of 0 or more matches of the regular expression.
 *	6.  A regular expression, x, of form 1-8, bracketed \(x\)
 *	    matches what x matches.
 *	7.  A \ followed by a digit n matches a copy of the string that the
 *	    bracketed regular expression beginning with the nth \( matched.
 *	8.  A regular expression of form 1-8, x, followed by a regular
 *	    expression of form 1-7, y matches a match for x followed by
 *	    a match for y, with the x match being as long as possible
 *	    while still permitting a y match.
 *	9.  A regular expression of form 1-8 preceded by ^ (or followed
 *	    by $), is constrained to matches that begin at the left
 *	    (or end at the right) end of a line.
 *	10. A regular expression of form 1-9 picks out the longest among
 *	    the leftmost matches in a line.
 *	11. An empty regular expression stands for a copy of the last
 *	    regular expression encountered.
 */

/*
 * constants for re's
 */
#define	x_CBRA	1
#define	x_CCHR	2
#define	x_CDOT	4
#define	x_CCL	6
#define	x_NCCL	8
#define	x_CDOL	10
#define	x_CEOF	11
#define	x_CKET	12
#define	x_CBACK	18

#define	x_CSTAR	01

#define	x_ESIZE	512
#define	x_NBRA	9

static	char	x_expbuf[x_ESIZE], *x_braslist[x_NBRA], *x_braelist[x_NBRA];
static	char	x_circf;

/*
 * compile the regular expression argument into a dfa
 */
#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

static x_int x_advance __P((register char *x_lp, register char *x_ep));

char *x_re_comp(x_sp) register char *x_sp; {
	register x_int	x_c;
	register char	*x_ep = x_expbuf;
	x_int	x_cclcnt, x_numbra = 0;
	char	*x_lastep = 0;
	char	x_bracket[x_NBRA];
	char	*x_bracketp = &x_bracket[0];
	static	char	*x_retoolong = "Regular expression too long";

#define	x_comerr(x_msg) {x_expbuf[0] = 0; x_numbra = 0; return(x_msg); }

	if (x_sp == 0 || *x_sp == '\0') {
		if (*x_ep == 0)
			return("No previous regular expression");
		return(0);
	}
	if (*x_sp == '^') {
		x_circf = 1;
		x_sp++;
	}
	else
		x_circf = 0;
	for (;;) {
		if (x_ep >= &x_expbuf[x_ESIZE])
			x_comerr(x_retoolong);
		if ((x_c = *x_sp++) == '\0') {
			if (x_bracketp != x_bracket)
				x_comerr("unmatched \\(");
			*x_ep++ = x_CEOF;
			*x_ep++ = 0;
			return(0);
		}
		if (x_c != '*')
			x_lastep = x_ep;
		switch (x_c) {

		case '.':
			*x_ep++ = x_CDOT;
			continue;

		case '*':
			if (x_lastep == 0 || *x_lastep == x_CBRA || *x_lastep == x_CKET)
				goto x_defchar;
			*x_lastep |= x_CSTAR;
			continue;

		case '$':
			if (*x_sp != '\0')
				goto x_defchar;
			*x_ep++ = x_CDOL;
			continue;

		case '[':
			*x_ep++ = x_CCL;
			*x_ep++ = 0;
			x_cclcnt = 1;
			if ((x_c = *x_sp++) == '^') {
				x_c = *x_sp++;
				x_ep[-2] = x_NCCL;
			}
			do {
				if (x_c == '\0')
					x_comerr("missing ]");
				if (x_c == '-' && x_ep [-1] != 0) {
					if ((x_c = *x_sp++) == ']') {
						*x_ep++ = '-';
						x_cclcnt++;
						break;
					}
					while (x_ep[-1] < x_c) {
						*x_ep = x_ep[-1] + 1;
						x_ep++;
						x_cclcnt++;
						if (x_ep >= &x_expbuf[x_ESIZE])
							x_comerr(x_retoolong);
					}
				}
				*x_ep++ = x_c;
				x_cclcnt++;
				if (x_ep >= &x_expbuf[x_ESIZE])
					x_comerr(x_retoolong);
			} while ((x_c = *x_sp++) != ']');
			x_lastep[1] = x_cclcnt;
			continue;

		case '\\':
			if ((x_c = *x_sp++) == '(') {
				if (x_numbra >= x_NBRA)
					x_comerr("too many \\(\\) pairs");
				*x_bracketp++ = x_numbra;
				*x_ep++ = x_CBRA;
				*x_ep++ = x_numbra++;
				continue;
			}
			if (x_c == ')') {
				if (x_bracketp <= x_bracket)
					x_comerr("unmatched \\)");
				*x_ep++ = x_CKET;
				*x_ep++ = *--x_bracketp;
				continue;
			}
			if (x_c >= '1' && x_c < ('1' + x_NBRA)) {
				*x_ep++ = x_CBACK;
				*x_ep++ = x_c - '1';
				continue;
			}
			*x_ep++ = x_CCHR;
			*x_ep++ = x_c;
			continue;

		x_defchar:
		x_default:
			*x_ep++ = x_CCHR;
			*x_ep++ = x_c;
		}
	}
}

/* 
 * match the argument string against the compiled re
 */
x_int x_re_exec(x_p1) register char *x_p1; {
	register char	*x_p2 = x_expbuf;
	register x_int	x_c;
	x_int	x_rv;

	for (x_c = 0; x_c < x_NBRA; x_c++) {
		x_braslist[x_c] = 0;
		x_braelist[x_c] = 0;
	}
	if (x_circf)
		return((x_advance(x_p1, x_p2)));
	/*
	 * fast check for first character
	 */
	if (*x_p2 == x_CCHR) {
		x_c = x_p2[1];
		do {
			if (*x_p1 != x_c)
				continue;
			if (x_rv = x_advance(x_p1, x_p2))
				return(x_rv);
		} while (*x_p1++);
		return(0);
	}
	/*
	 * regular algorithm
	 */
	do
		if (x_rv = x_advance(x_p1, x_p2))
			return(x_rv);
	while (*x_p1++);
	return(0);
}

/* 
 * try to match the next thing in the dfa
 */
static x_int x_advance(x_lp, x_ep) register char *x_lp; register char *x_ep; {
	register char	*x_curlp;
	x_int	x_ct, x_i;
	x_int	x_rv;

	for (;;)
		switch (*x_ep++) {

		case x_CCHR:
			if (*x_ep++ == *x_lp++)
				continue;
			return(0);

		case x_CDOT:
			if (*x_lp++)
				continue;
			return(0);

		case x_CDOL:
			if (*x_lp == '\0')
				continue;
			return(0);

		case x_CEOF:
			return(1);

		case x_CCL:
			if (x_cclass(x_ep, *x_lp++, 1)) {
				x_ep += *x_ep;
				continue;
			}
			return(0);

		case x_NCCL:
			if (x_cclass(x_ep, *x_lp++, 0)) {
				x_ep += *x_ep;
				continue;
			}
			return(0);

		case x_CBRA:
			x_braslist[*x_ep++] = x_lp;
			continue;

		case x_CKET:
			x_braelist[*x_ep++] = x_lp;
			continue;

		case x_CBACK:
			if (x_braelist[x_i = *x_ep++] == 0)
				return(-1);
			if (x_backref(x_i, x_lp)) {
				x_lp += x_braelist[x_i] - x_braslist[x_i];
				continue;
			}
			return(0);

		case x_CBACK|x_CSTAR:
			if (x_braelist[x_i = *x_ep++] == 0)
				return(-1);
			x_curlp = x_lp;
			x_ct = x_braelist[x_i] - x_braslist[x_i];
			while (x_backref(x_i, x_lp))
				x_lp += x_ct;
			while (x_lp >= x_curlp) {
				if (x_rv = x_advance(x_lp, x_ep))
					return(x_rv);
				x_lp -= x_ct;
			}
			continue;

		case x_CDOT|x_CSTAR:
			x_curlp = x_lp;
			while (*x_lp++)
				;
			goto x_star;

		case x_CCHR|x_CSTAR:
			x_curlp = x_lp;
			while (*x_lp++ == *x_ep)
				;
			x_ep++;
			goto x_star;

		case x_CCL|x_CSTAR:
		case x_NCCL|x_CSTAR:
			x_curlp = x_lp;
			while (x_cclass(x_ep, *x_lp++, x_ep[-1] == (x_CCL|x_CSTAR)))
				;
			x_ep += *x_ep;
			goto x_star;

		x_star:
			do {
				x_lp--;
				if (x_rv = x_advance(x_lp, x_ep))
					return(x_rv);
			} while (x_lp > x_curlp);
			return(0);

		x_default:
			return(-1);
		}
}

x_int x_backref(x_i, x_lp) register x_int x_i; register char *x_lp; {
	register char	*x_bp;

	x_bp = x_braslist[x_i];
	while (*x_bp++ == *x_lp++)
		if (x_bp >= x_braelist[x_i])
			return(1);
	return(0);
}

x_int x_cclass(x_set, x_c, x_af) register char *x_set; x_int x_c; x_int x_af; {
	register x_int	x_n;

	if (x_c == 0)
		return(0);
	x_n = *x_set++;
	while (--x_n)
		if (*x_set++ == x_c)
			return(x_af);
	return(! x_af);
}
