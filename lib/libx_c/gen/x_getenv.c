#include "x_.h"

#include <x_stdlib.h>
#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)getenv.c	5.2 (Berkeley) 3/9/86";
#endif

/*
 *	getenv(name)
 *	returns ptr to value associated with name, if any, else NULL
 */
#define x_NULL	0
extern	char **x_environ;
char	*x_nvmatch();

#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

static char *x_nvmatch __P((register char *x_s1, register char *x_s2));

char *x_getenv(x_name) register char *x_name; {
	register char **x_p = x_environ;
	register char *x_v;

	while (*x_p != x_NULL)
		if ((x_v = x_nvmatch(x_name, *x_p++)) != x_NULL)
			return(x_v);
	return(x_NULL);
}

/*
 *	s1 is either name, or name=value
 *	s2 is name=value
 *	if names match, return value of s2, else NULL
 *	used for environment searching: see getenv
 */

static char *x_nvmatch(x_s1, x_s2) register char *x_s1; register char *x_s2; {

	while (*x_s1 == *x_s2++)
		if (*x_s1++ == '=')
			return(x_s2);
	if (*x_s1 == '\0' && *(x_s2-1) == '=')
		return(x_s2);
	return(x_NULL);
}
