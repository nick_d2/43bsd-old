#include "x_.h"

#include <x_unistd.h>
#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)swab.c	5.3 (Berkeley) 3/9/86";
#endif

/*
 * Swab bytes
 * Jeffrey Mogul, Stanford
 */

x_int x_swab(x_from, x_to, x_n) register char *x_from; register char *x_to; register x_int x_n; {
	register x_unsigned_long x_temp;
	
	x_n >>= 1; x_n++;
#define	x_STEP	x_temp = *x_from++,*x_to++ = *x_from++,*x_to++ = x_temp
	/* round to multiple of 8 */
	while ((--x_n) & 07)
		x_STEP;
	x_n >>= 3;
	while (--x_n >= 0) {
		x_STEP; x_STEP; x_STEP; x_STEP;
		x_STEP; x_STEP; x_STEP; x_STEP;
	}
}
