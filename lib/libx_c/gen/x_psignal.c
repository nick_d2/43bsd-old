#include "x_.h"

#include <x_strings.h>
#include <x_unistd.h>
/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)psignal.c	5.2 (Berkeley) 3/9/86";
#endif

/*
 * Print the name of the signal indicated
 * along with the supplied message.
 */
#include <x_signal.h>

extern	char *x_sys_siglist[];

x_int x_psignal(x_sig, x_s) x_unsigned_int x_sig; char *x_s; {
	register char *x_c;
	register x_int x_n;

	x_c = "Unknown signal";
	if (x_sig < x_NSIG)
		x_c = x_sys_siglist[x_sig];
	x_n = x_strlen(x_s);
	if (x_n) {
		x_write(2, x_s, x_n);
		x_write(2, ": ", 2);
	}
	x_write(2, x_c, x_strlen(x_c));
	x_write(2, "\n", 1);
}
