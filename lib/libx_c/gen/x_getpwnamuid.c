#include "x_.h"

#include <x_strings.h>
#include <x_unistd.h>
/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)getpwnamuid.c	5.2 (Berkeley) 3/9/86";
#endif

#include <x_stdio.h>
#include <x_pwd.h>
#include <x_ndbm.h>

#include <sys/x_file.h>

static char x_line[x_BUFSIZ+1];
static struct x_passwd x_passwd;

/*
 * The following are shared with getpwent.c
 */
extern	char *x__pw_file;
x_DBM	*x__pw_db;
x_int	x__pw_stayopen;

#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

static struct x_passwd *x_fetchpw __P((x_datum x_key));

static struct x_passwd *x_fetchpw(x_key) x_datum x_key; {
        register char *x_cp, *x_tp;

        if (x_key.x_dptr == 0)
                return ((struct x_passwd *)x_NULL);
	x_key = x_dbm_fetch(x__pw_db, x_key);
	if (x_key.x_dptr == 0)
                return ((struct x_passwd *)x_NULL);
        x_cp = x_key.x_dptr;
	x_tp = x_line;

#define	x_EXPAND(x_e)	x_passwd.x_pw_/**/x_e = x_tp; while (*x_tp++ = *x_cp++);
	x_EXPAND(x_name);
	x_EXPAND(x_passwd);
	x_bcopy(x_cp, (char *)&x_passwd.x_pw_uid, sizeof (x_int));
	x_cp += sizeof (x_int);
	x_bcopy(x_cp, (char *)&x_passwd.x_pw_gid, sizeof (x_int));
	x_cp += sizeof (x_int);
	x_bcopy(x_cp, (char *)&x_passwd.x_pw_quota, sizeof (x_int));
	x_cp += sizeof (x_int);
	x_EXPAND(x_comment);
	x_EXPAND(x_gecos);
	x_EXPAND(x_dir);
	x_EXPAND(x_shell);
        return (&x_passwd);
}

struct x_passwd *x_getpwnam(x_nam) char *x_nam; {
        x_datum x_key;
	register struct x_passwd *x_pw;

        if (x__pw_db == (x_DBM *)0 &&
	    (x__pw_db = x_dbm_open(x__pw_file, x_O_RDONLY)) == (x_DBM *)0) {
	x_oldcode:
		x_setpwent();
		while ((x_pw = x_getpwent()) && x_strcmp(x_nam, x_pw->x_pw_name))
			;
		if (!x__pw_stayopen)
			x_endpwent();
		return (x_pw);
	}
	if (x_flock(x_dbm_dirfno(x__pw_db), x_LOCK_SH) < 0) {
		x_dbm_close(x__pw_db);
		x__pw_db = (x_DBM *)0;
		goto x_oldcode;
	}
        x_key.x_dptr = x_nam;
        x_key.x_dsize = x_strlen(x_nam);
	x_pw = x_fetchpw(x_key);
	(void) x_flock(x_dbm_dirfno(x__pw_db), x_LOCK_UN);
	if (!x__pw_stayopen) {
		x_dbm_close(x__pw_db);
		x__pw_db = (x_DBM *)0;
	}
        return (x_pw);
}

struct x_passwd *x_getpwuid(x_uid) x_int x_uid; {
        x_datum x_key;
	register struct x_passwd *x_pw;

        if (x__pw_db == (x_DBM *)0 &&
	    (x__pw_db = x_dbm_open(x__pw_file, x_O_RDONLY)) == (x_DBM *)0) {
	x_oldcode:
		x_setpwent();
		while ((x_pw = x_getpwent()) && x_pw->x_pw_uid != x_uid)
			;
		if (!x__pw_stayopen)
			x_endpwent();
		return (x_pw);
	}
	if (x_flock(x_dbm_dirfno(x__pw_db), x_LOCK_SH) < 0) {
		x_dbm_close(x__pw_db);
		x__pw_db = (x_DBM *)0;
		goto x_oldcode;
	}
        x_key.x_dptr = (char *) &x_uid;
        x_key.x_dsize = sizeof x_uid;
	x_pw = x_fetchpw(x_key);
	(void) x_flock(x_dbm_dirfno(x__pw_db), x_LOCK_UN);
	if (!x__pw_stayopen) {
		x_dbm_close(x__pw_db);
		x__pw_db = (x_DBM *)0;
	}
        return (x_pw);
}
