#include "x_.h"

#include <x_strings.h>
/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)getttynam.c	5.2 (Berkeley) 3/9/86";
#endif

#include <x_ttyent.h>

struct x_ttyent *x_getttynam(x_tty) char *x_tty; {
	register struct x_ttyent *x_t;

	x_setttyent();
	while (x_t = x_getttyent()) {
		if (x_strcmp(x_tty, x_t->x_ty_name) == 0)
			break;
	}
	x_endttyent();
	return (x_t);
}
