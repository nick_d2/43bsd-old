#include "x_.h"

#include <x_strings.h>
#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)rindex.c	5.2 (Berkeley) 3/9/86";
#endif

/*
 * Return the ptr in sp at which the character c last
 * appears; NULL if not found
 */

#define x_NULL 0

char *x_rindex(x_sp, x_c) register char *x_sp; x_int x_c; {
	register char *x_r;

	x_r = x_NULL;
	do {
		if (*x_sp == x_c)
			x_r = x_sp;
	} while (*x_sp++);
	return(x_r);
}
