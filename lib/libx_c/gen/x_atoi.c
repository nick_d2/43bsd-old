#include "x_.h"

#include <x_stdlib.h>
#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)atoi.c	5.2 (Berkeley) 3/9/86";
#endif

x_int x_atoi(x_p) register char *x_p; {
	register x_int x_n;
	register x_int x_f;

	x_n = 0;
	x_f = 0;
	for(;;x_p++) {
		switch(*x_p) {
		case ' ':
		case '\t':
			continue;
		case '-':
			x_f++;
		case '+':
			x_p++;
		}
		break;
	}
	while(*x_p >= '0' && *x_p <= '9')
		x_n = x_n*10 + *x_p++ - '0';
	return(x_f? -x_n: x_n);
}
