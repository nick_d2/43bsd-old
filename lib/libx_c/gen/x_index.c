#include "x_.h"

#include <x_strings.h>
#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)index.c	5.2 (Berkeley) 3/9/86";
#endif

/*
 * Return the ptr in sp at which the character c appears;
 * NULL if not found
 */

#define	x_NULL	0

char *x_index(x_sp, x_c) register char *x_sp; x_int x_c; {
	do {
		if (*x_sp == x_c)
			return(x_sp);
	} while (*x_sp++);
	return(x_NULL);
}
