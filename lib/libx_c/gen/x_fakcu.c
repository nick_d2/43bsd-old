#include "x_.h"

#include <x_stdlib.h>
#include <x_stdio.h>
#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)fakcu.c	5.2 (Berkeley) 3/9/86";
#endif

/*
 * Null cleanup routine to resolve reference in exit() 
 * if not using stdio.
 */
x_int x__cleanup() {
}
