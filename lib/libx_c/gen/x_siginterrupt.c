#include "x_.h"

#include <x_unistd.h>
/*
 * Copyright (c) 1985 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)siginterrupt.c	5.2 (Berkeley) 3/9/86";
#endif

#include <x_signal.h>

/*
 * Set signal state to prevent restart of system calls
 * after an instance of the indicated signal.
 */
x_int x_siginterrupt(x_sig, x_flag) x_int x_sig; x_int x_flag; {
	struct x_sigvec x_sv;
	x_int x_ret;

	if ((x_ret = x_sigvec(x_sig, 0, &x_sv)) < 0)
		return (x_ret);
	if (x_flag)
		x_sv.x_sv_flags |= x_SV_INTERRUPT;
	else
		x_sv.x_sv_flags &= ~x_SV_INTERRUPT;
	return (x_sigvec(x_sig, &x_sv, 0));
}
