#include "x_.h"

#include <x_mp.h>
#include <x_unistd.h>
/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)closedir.c	5.2 (Berkeley) 3/9/86";
#endif

#include <sys/x_param.h>
#include <sys/x_dir.h>

/*
 * close a directory.
 */
void x_closedir(x_dirp) register x_DIR *x_dirp; {
	x_close(x_dirp->x_dd_fd);
	x_dirp->x_dd_fd = -1;
	x_dirp->x_dd_loc = 0;
	x_free(x_dirp);
}
