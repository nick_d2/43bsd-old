#include "x_.h"

#include <x_stdlib.h>
#include <x_strings.h>
#include <x_unistd.h>
#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)execvp.c	5.2 (Berkeley) 3/9/86";
#endif

/*
 *	execlp(name, arg,...,0)	(like execl, but does path search)
 *	execvp(name, argv)	(like execv, but does path search)
 */
#include <x_errno.h>
#define	x_NULL	0

static	char x_shell[] =	"/bin/sh";
char	*x_execat(), *x_getenv();
extern	x_errno;

#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

static char *x_execat __P((register char *x_s1, register char *x_s2, char *x_si));

x_int x_execlp(x_name, x_argv) char *x_name; char *x_argv; {
	return(x_execvp(x_name, &x_argv));
}

x_int x_execvp(x_name, x_argv) char *x_name; char **x_argv; {
	char *x_pathstr;
	register char *x_cp;
	char x_fname[128];
	char *x_newargs[256];
	x_int x_i;
	register x_unsigned_int x_etxtbsy = 1;
	register x_int x_eacces = 0;

	if ((x_pathstr = x_getenv("PATH")) == x_NULL)
		x_pathstr = ":/bin:/usr/bin";
	x_cp = x_index(x_name, '/')? "": x_pathstr;

	do {
		x_cp = x_execat(x_cp, x_name, x_fname);
	x_retry:
		x_execv(x_fname, x_argv);
		switch(x_errno) {
		case x_ENOEXEC:
			x_newargs[0] = "sh";
			x_newargs[1] = x_fname;
			for (x_i=1; x_newargs[x_i+1]=x_argv[x_i]; x_i++) {
				if (x_i>=254) {
					x_errno = x_E2BIG;
					return(-1);
				}
			}
			x_execv(x_shell, x_newargs);
			return(-1);
		case x_ETXTBSY:
			if (++x_etxtbsy > 5)
				return(-1);
			x_sleep(x_etxtbsy);
			goto x_retry;
		case x_EACCES:
			x_eacces++;
			break;
		case x_ENOMEM:
		case x_E2BIG:
			return(-1);
		}
	} while (x_cp);
	if (x_eacces)
		x_errno = x_EACCES;
	return(-1);
}

static char *x_execat(x_s1, x_s2, x_si) register char *x_s1; register char *x_s2; char *x_si; {
	register char *x_s;

	x_s = x_si;
	while (*x_s1 && *x_s1 != ':')
		*x_s++ = *x_s1++;
	if (x_si != x_s)
		*x_s++ = '/';
	while (*x_s2)
		*x_s++ = *x_s2++;
	*x_s = '\0';
	return(*x_s1? ++x_s1: 0);
}
