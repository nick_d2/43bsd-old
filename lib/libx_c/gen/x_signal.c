#include "x_.h"

#include <x_unistd.h>
/*
 * Copyright (c) 1985 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)signal.c	5.2 (Berkeley) 3/9/86";
#endif

/*
 * Almost backwards compatible signal.
 */
#include <x_signal.h>

x_int (*x_signal(x_s, x_a))() x_int x_s; x_int (*x_a)(); {
	struct x_sigvec x_osv, x_sv;
	static x_int x_mask[x_NSIG];
	static x_int x_flags[x_NSIG];

	x_sv.x_sv_handler = x_a;
	x_sv.x_sv_mask = x_mask[x_s];
	x_sv.x_sv_flags = x_flags[x_s];
	if (x_sigvec(x_s, &x_sv, &x_osv) < 0)
		return (x_BADSIG);
	if (x_sv.x_sv_mask != x_osv.x_sv_mask || x_sv.x_sv_flags != x_osv.x_sv_flags) {
		x_mask[x_s] = x_sv.x_sv_mask = x_osv.x_sv_mask;
		x_flags[x_s] = x_sv.x_sv_flags = x_osv.x_sv_flags;
		if (x_sigvec(x_s, &x_sv, 0) < 0)
			return (x_BADSIG);
	}
	return (x_osv.x_sv_handler);
}
