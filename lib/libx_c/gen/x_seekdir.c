#include "x_.h"

#include <x_unistd.h>
/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)seekdir.c	5.2 (Berkeley) 3/9/86";
#endif

#include <sys/x_param.h>
#include <sys/x_dir.h>

/*
 * seek to an entry in a directory.
 * Only values returned by "telldir" should be passed to seekdir.
 */
void x_seekdir(x_dirp, x_loc) register x_DIR *x_dirp; x_long x_loc; {
	x_long x_curloc, x_base, x_offset;
	struct x_direct *x_dp;
	extern x_long x_lseek();

	x_curloc = x_telldir(x_dirp);
	if (x_loc == x_curloc)
		return;
	x_base = x_loc & ~(x_DIRBLKSIZ - 1);
	x_offset = x_loc & (x_DIRBLKSIZ - 1);
	(void) x_lseek(x_dirp->x_dd_fd, x_base, 0);
	x_dirp->x_dd_loc = 0;
	while (x_dirp->x_dd_loc < x_offset) {
		x_dp = x_readdir(x_dirp);
		if (x_dp == x_NULL)
			return;
	}
}
