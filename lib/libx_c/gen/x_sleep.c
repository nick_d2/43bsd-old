#include "x_.h"

#include <x_unistd.h>
/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)sleep.c	5.2 (Berkeley) 3/9/86";
#endif

#include <sys/x_time.h>
#include <x_signal.h>

#define	x_setvec(x_vec, x_a) \
	x_vec.x_sv_handler = x_a; x_vec.x_sv_mask = x_vec.x_sv_onstack = 0

static x_int x_ringring;

#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

static x_int x_sleepx __P((void));

x_int x_sleep(x_n) x_unsigned_int x_n; {
	x_int x_sleepx(), x_omask;
	struct x_itimerval x_itv, x_oitv;
	register struct x_itimerval *x_itp = &x_itv;
	struct x_sigvec x_vec, x_ovec;

	if (x_n == 0)
		return;
	x_timerclear(&x_itp->x_it_interval);
	x_timerclear(&x_itp->x_it_value);
	if (x_setitimer(x_ITIMER_REAL, x_itp, &x_oitv) < 0)
		return;
	x_itp->x_it_value.x_tv_sec = x_n;
	if (x_timerisset(&x_oitv.x_it_value)) {
		if (x_timercmp(&x_oitv.x_it_value, &x_itp->x_it_value, >))
			x_oitv.x_it_value.x_tv_sec -= x_itp->x_it_value.x_tv_sec;
		else {
			x_itp->x_it_value = x_oitv.x_it_value;
			/*
			 * This is a hack, but we must have time to
			 * return from the setitimer after the alarm
			 * or else it'll be restarted.  And, anyway,
			 * sleep never did anything more than this before.
			 */
			x_oitv.x_it_value.x_tv_sec = 1;
			x_oitv.x_it_value.x_tv_usec = 0;
		}
	}
	x_setvec(x_vec, x_sleepx);
	(void) x_sigvec(x_SIGALRM, &x_vec, &x_ovec);
	x_omask = x_sigblock(x_sigmask(x_SIGALRM));
	x_ringring = 0;
	(void) x_setitimer(x_ITIMER_REAL, x_itp, (struct x_itimerval *)0);
	while (!x_ringring)
		x_sigpause(x_omask &~ x_sigmask(x_SIGALRM));
	(void) x_sigvec(x_SIGALRM, &x_ovec, (struct x_sigvec *)0);
	(void) x_sigsetmask(x_omask);
	(void) x_setitimer(x_ITIMER_REAL, &x_oitv, (struct x_itimerval *)0);
}

static x_int x_sleepx() {

	x_ringring = 1;
}
