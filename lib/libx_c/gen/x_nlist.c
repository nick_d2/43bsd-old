#include "x_.h"

#include <x_strings.h>
#include <x_nlist.h>
/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)nlist.c	5.2 (Berkeley) 3/9/86";
#endif

#include <sys/x_types.h>
#include <x_a.out.h>
#include <x_stdio.h>

/*
 * nlist - retreive attributes from name list (string table version)
 */
x_int x_nlist(x_name, x_list) char *x_name; struct x_nlist *x_list; {
	register struct x_nlist *x_p, *x_q;
	register char *x_s1, *x_s2;
	register x_int x_n, x_m;
	x_int x_maxlen, x_nreq;
	x_FILE *x_f;
	x_FILE *x_sf;
	x_off_t x_sa;		/* symbol address */
	x_off_t x_ss;		/* start of strings */
	struct x_exec x_buf;
	struct x_nlist x_space[x_BUFSIZ/sizeof (struct x_nlist)];

	x_maxlen = 0;
	for (x_q = x_list, x_nreq = 0; x_q->x_n_un.x_n_name && x_q->x_n_un.x_n_name[0]; x_q++, x_nreq++) {
		x_q->x_n_type = 0;
		x_q->x_n_value = 0;
		x_q->x_n_desc = 0;
		x_q->x_n_other = 0;
		x_n = x_strlen(x_q->x_n_un.x_n_name);
		if (x_n > x_maxlen)
			x_maxlen = x_n;
	}
	x_f = x_fopen(x_name, "r");
	if (x_f == x_NULL)
		return (-1);
	x_fread((char *)&x_buf, sizeof x_buf, 1, x_f);
	if (x_N_BADMAG(x_buf)) {
		x_fclose(x_f);
		return (-1);
	}
	x_sf = x_fopen(x_name, "r");
	if (x_sf == x_NULL) {
		/* ??? */
		x_fclose(x_f);
		return(-1);
	}
	x_sa = x_N_SYMOFF(x_buf);
	x_ss = x_sa + x_buf.x_a_syms;
	x_n = x_buf.x_a_syms;
	x_fseek(x_f, x_sa, 0);
	while (x_n) {
		x_m = sizeof (x_space);
		if (x_n < x_m)
			x_m = x_n;
		if (x_fread((char *)x_space, x_m, 1, x_f) != 1)
			break;
		x_n -= x_m;
		for (x_q = x_space; (x_m -= sizeof(struct x_nlist)) >= 0; x_q++) {
			char x_nambuf[x_BUFSIZ];

			if (x_q->x_n_un.x_n_strx == 0 || x_q->x_n_type & x_N_STAB)
				continue;
			x_fseek(x_sf, x_ss+x_q->x_n_un.x_n_strx, 0);
			x_fread(x_nambuf, x_maxlen+1, 1, x_sf);
			for (x_p = x_list; x_p->x_n_un.x_n_name && x_p->x_n_un.x_n_name[0]; x_p++) {
				x_s1 = x_p->x_n_un.x_n_name;
				x_s2 = x_nambuf;
				while (*x_s1) {
					if (*x_s1++ != *x_s2++)
						goto x_cont;
				}
				if (*x_s2)
					goto x_cont;
				x_p->x_n_value = x_q->x_n_value;
				x_p->x_n_type = x_q->x_n_type;
				x_p->x_n_desc = x_q->x_n_desc;
				x_p->x_n_other = x_q->x_n_other;
				if (--x_nreq == 0)
					goto x_alldone;
				break;
		x_cont:		;
			}
		}
	}
x_alldone:
	x_fclose(x_f);
	x_fclose(x_sf);
	return (x_nreq);
}
