#include "x_.h"

#include <x_unistd.h>
/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)time.c	5.3 (Berkeley) 3/9/86";
#endif

/*
 * Backwards compatible time call.
 */
#include <sys/x_types.h>
#include <sys/x_time.h>

x_long x_time(x_t) x_time_t *x_t; {
	struct x_timeval x_tt;

	if (x_gettimeofday(&x_tt, (struct x_timezone *)0) < 0)
		return (-1);
	if (x_t)
		*x_t = x_tt.x_tv_sec;
	return (x_tt.x_tv_sec);
}
