#include "x_.h"

#include <x_unistd.h>
/*
 * Copyright (c) 1985 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)ualarm.c	5.2 (Berkeley) 3/9/86";
#endif

#include <sys/x_time.h>

#define	x_USPS	1000000		/* # of microseconds in a second */

/*
 * Generate a SIGALRM signal in ``usecs'' microseconds.
 * If ``reload'' is non-zero, keep generating SIGALRM
 * every ``reload'' microseconds after the first signal.
 */
x_unsigned_int x_ualarm(x_usecs, x_reload) register x_unsigned_int x_usecs; register x_unsigned_int x_reload; {
	struct x_itimerval x_new, x_old;

	x_new.x_it_interval.x_tv_usec = x_reload % x_USPS;
	x_new.x_it_interval.x_tv_sec = x_reload / x_USPS;
	
	x_new.x_it_value.x_tv_usec = x_usecs % x_USPS;
	x_new.x_it_value.x_tv_sec = x_usecs / x_USPS;

	if (x_setitimer(x_ITIMER_REAL, &x_new, &x_old) == 0)
		return (x_old.x_it_value.x_tv_sec * x_USPS + x_old.x_it_value.x_tv_usec);
	/* else */
		return (-1);
}
