#include "x_.h"

#include <x_mp.h>
#include <x_unistd.h>
/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)opendir.c	5.2 (Berkeley) 3/9/86";
#endif

#include <sys/x_param.h>
#include <sys/x_dir.h>

/*
 * open a directory.
 */
x_DIR *x_opendir(x_name) char *x_name; {
	register x_DIR *x_dirp;
	register x_int x_fd;

	if ((x_fd = x_open(x_name, 0)) == -1)
		return x_NULL;
	if ((x_dirp = (x_DIR *)x_malloc(sizeof(x_DIR))) == x_NULL) {
		x_close (x_fd);
		return x_NULL;
	}
	x_dirp->x_dd_fd = x_fd;
	x_dirp->x_dd_loc = 0;
	return x_dirp;
}
