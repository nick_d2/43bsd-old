#include "x_.h"

#include <x_strings.h>
#include <x_pwd.h>
#include <x_unistd.h>
/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)initgroups.c	5.3 (Berkeley) 4/27/86";
#endif

/*
 * initgroups
 */
#include <x_stdio.h>
#include <sys/x_param.h>
#include <x_grp.h>

struct x_group *x_getgrent();

x_int x_initgroups(x_uname, x_agroup) char *x_uname; x_int x_agroup; {
	x_int x_groups[x_NGROUPS], x_ngroups = 0;
	register struct x_group *x_grp;
	register x_int x_i;

	if (x_agroup >= 0)
		x_groups[x_ngroups++] = x_agroup;
	x_setgrent();
	while (x_grp = x_getgrent()) {
		if (x_grp->x_gr_gid == x_agroup)
			continue;
		for (x_i = 0; x_grp->x_gr_mem[x_i]; x_i++)
			if (!x_strcmp(x_grp->x_gr_mem[x_i], x_uname)) {
				if (x_ngroups == x_NGROUPS) {
x_fprintf(x_stderr, "initgroups: %s is in too many groups\n", x_uname);
					goto x_toomany;
				}
				x_groups[x_ngroups++] = x_grp->x_gr_gid;
			}
	}
x_toomany:
	x_endgrent();
	if (x_setgroups(x_ngroups, x_groups) < 0) {
		x_perror("setgroups");
		return (-1);
	}
	return (0);
}
