#include "x_.h"

#include <x_unistd.h>
/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)alarm.c	5.2 (Berkeley) 3/9/86";
#endif

/*
 * Backwards compatible alarm.
 */
#include <sys/x_time.h>

x_int x_alarm(x_secs) x_int x_secs; {
	struct x_itimerval x_it, x_oitv;
	register struct x_itimerval *x_itp = &x_it;

	x_timerclear(&x_itp->x_it_interval);
	x_itp->x_it_value.x_tv_sec = x_secs;
	x_itp->x_it_value.x_tv_usec = 0;
	if (x_setitimer(x_ITIMER_REAL, x_itp, &x_oitv) < 0)
		return (-1);
	if (x_oitv.x_it_value.x_tv_usec)
		x_oitv.x_it_value.x_tv_sec++;
	return (x_oitv.x_it_value.x_tv_sec);
}
