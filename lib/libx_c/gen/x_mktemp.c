#include "x_.h"

#include <x_stdlib.h>
#include <x_unistd.h>
#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)mktemp.c	5.2 (Berkeley) 3/9/86";
#endif

char *x_mktemp(x_as) char *x_as; {
	register char *x_s;
	register x_unsigned_int x_pid;
	register x_int x_i;

	x_pid = x_getpid();
	x_s = x_as;
	while (*x_s++)
		;
	x_s--;
	while (*--x_s == 'X') {
		*x_s = (x_pid%10) + '0';
		x_pid /= 10;
	}
	x_s++;
	x_i = 'a';
	while (x_access(x_as, 0) != -1) {
		if (x_i=='z')
			return("/");
		*x_s = x_i++;
	}
	return(x_as);
}
