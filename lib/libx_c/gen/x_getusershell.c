#include "x_.h"

#include <x_stdlib.h>
#include <x_mp.h>
#include <x_unistd.h>
/*
 * Copyright (c) 1985 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)getusershell.c	5.4 (Berkeley) 7/25/86";
#endif

#include <sys/x_param.h>
#include <sys/x_file.h>
#include <sys/x_stat.h>
#include <x_ctype.h>
#include <x_stdio.h>

#define x_SHELLS "/etc/shells"

/*
 * Do not add local shells here.  They should be added in /etc/shells
 */
static char *x_okshells[] =
    { "/bin/sh", "/bin/csh", 0 };

static char **x_shells, *x_strings;
static char **x_curshell = x_NULL;
extern char **x_initshells();

/*
 * Get a list of shells from SHELLS, if it exists.
 */
#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

static char **x_initshells __P((void));

char *x_getusershell() {
	char *x_ret;

	if (x_curshell == x_NULL)
		x_curshell = x_initshells();
	x_ret = *x_curshell;
	if (x_ret != x_NULL)
		x_curshell++;
	return (x_ret);
}

x_int x_endusershell() {
	
	if (x_shells != x_NULL)
		x_free((char *)x_shells);
	x_shells = x_NULL;
	if (x_strings != x_NULL)
		x_free(x_strings);
	x_strings = x_NULL;
	x_curshell = x_NULL;
}

x_int x_setusershell() {

	x_curshell = x_initshells();
}

static char **x_initshells() {
	register char **x_sp, *x_cp;
	register x_FILE *x_fp;
	struct x_stat x_statb;
	extern char *x_malloc(), *x_calloc();

	if (x_shells != x_NULL)
		x_free((char *)x_shells);
	x_shells = x_NULL;
	if (x_strings != x_NULL)
		x_free(x_strings);
	x_strings = x_NULL;
	if ((x_fp = x_fopen(x_SHELLS, "r")) == (x_FILE *)0)
		return(x_okshells);
	if (x_fstat(x_fileno(x_fp), &x_statb) == -1) {
		(void)x_fclose(x_fp);
		return(x_okshells);
	}
	if ((x_strings = x_malloc((unsigned)x_statb.x_st_size)) == x_NULL) {
		(void)x_fclose(x_fp);
		return(x_okshells);
	}
	x_shells = (char **)x_calloc((unsigned)x_statb.x_st_size / 3, sizeof (char *));
	if (x_shells == x_NULL) {
		(void)x_fclose(x_fp);
		x_free(x_strings);
		x_strings = x_NULL;
		return(x_okshells);
	}
	x_sp = x_shells;
	x_cp = x_strings;
	while (x_fgets(x_cp, x_MAXPATHLEN + 1, x_fp) != x_NULL) {
		while (*x_cp != '#' && *x_cp != '/' && *x_cp != '\0')
			x_cp++;
		if (*x_cp == '#' || *x_cp == '\0')
			continue;
		*x_sp++ = x_cp;
		while (!x_isspace(*x_cp) && *x_cp != '#' && *x_cp != '\0')
			x_cp++;
		*x_cp++ = '\0';
	}
	*x_sp = (char *)0;
	(void)x_fclose(x_fp);
	return (x_shells);
}
