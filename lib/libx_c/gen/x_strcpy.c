#include "x_.h"

#include <x_strings.h>
#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)strcpy.c	5.2 (Berkeley) 3/9/86";
#endif

/*
 * Copy string s2 to s1.  s1 must be large enough.
 * return s1
 */

char *x_strcpy(x_s1, x_s2) register char *x_s1; register char *x_s2; {
	register char *x_os1;

	x_os1 = x_s1;
	while (*x_s1++ = *x_s2++)
		;
	return(x_os1);
}
