#include "x_.h"

#include <x_unistd.h>
/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)telldir.c	5.2 (Berkeley) 3/9/86";
#endif

#include <sys/x_param.h>
#include <sys/x_dir.h>

/*
 * return a pointer into a directory
 */
x_long x_telldir(x_dirp) x_DIR *x_dirp; {
	extern x_long x_lseek();

	return (x_lseek(x_dirp->x_dd_fd, 0L, 1) - x_dirp->x_dd_size + x_dirp->x_dd_loc);
}
