#include "x_.h"

#include <x_strings.h>
#include <x_mp.h>
#include <x_unistd.h>
/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)ndbm.c	5.3 (Berkeley) 3/9/86";
#endif

#include <sys/x_types.h>
#include <sys/x_stat.h>
#include <sys/x_file.h>
#include <x_stdio.h>
#include <x_errno.h>
#include <x_ndbm.h>

#define x_BYTESIZ 8
#undef x_setbit

static x_int  x_datum x_makdatum();
static  x_long x_hashinc();
static  x_long x_dcalchash();
extern  x_int x_errno;

#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

static x_int x_dbm_access __P((register x_DBM *x_db, x_long x_hash));
static x_int x_getbit __P((register x_DBM *x_db));
static x_int x_setbit __P((register x_DBM *x_db));
static x_int x_datum x_makdatum __P((char x_buf[x_PBLKSIZ], x_int x_n));
static x_int x_finddatum __P((char x_buf[x_PBLKSIZ], x_datum x_item));
static x_long x_hashinc __P((register x_DBM *x_db, x_long x_hash));
static x_long x_dcalchash __P((x_datum x_item));
static x_int x_delitem __P((char x_buf[x_PBLKSIZ], x_int x_n));
static x_int x_additem __P((char x_buf[x_PBLKSIZ], x_datum x_item, x_datum x_item1));
static x_int x_chkblk __P((char x_buf[x_PBLKSIZ]));

x_DBM *x_dbm_open(x_file, x_flags, x_mode) char *x_file; x_int x_flags; x_int x_mode; {
	struct x_stat x_statb;
	register x_DBM *x_db;

	if ((x_db = (x_DBM *)x_malloc(sizeof *x_db)) == 0) {
		x_errno = x_ENOMEM;
		return ((x_DBM *)0);
	}
	x_db->x_dbm_flags = (x_flags & 03) == x_O_RDONLY ? x__DBM_RDONLY : 0;
	if ((x_flags & 03) == x_O_WRONLY)
		x_flags = (x_flags & ~03) | x_O_RDWR;
	x_strcpy(x_db->x_dbm_pagbuf, x_file);
	x_strcat(x_db->x_dbm_pagbuf, ".pag");
	x_db->x_dbm_pagf = x_open(x_db->x_dbm_pagbuf, x_flags, x_mode);
	if (x_db->x_dbm_pagf < 0)
		goto x_bad;
	x_strcpy(x_db->x_dbm_pagbuf, x_file);
	x_strcat(x_db->x_dbm_pagbuf, ".dir");
	x_db->x_dbm_dirf = x_open(x_db->x_dbm_pagbuf, x_flags, x_mode);
	if (x_db->x_dbm_dirf < 0)
		goto x_bad1;
	x_fstat(x_db->x_dbm_dirf, &x_statb);
	x_db->x_dbm_maxbno = x_statb.x_st_size*x_BYTESIZ-1;
	x_db->x_dbm_pagbno = x_db->x_dbm_dirbno = -1;
	return (x_db);
x_bad1:
	(void) x_close(x_db->x_dbm_pagf);
x_bad:
	x_free((char *)x_db);
	return ((x_DBM *)0);
}

void x_dbm_close(x_db) x_DBM *x_db; {

	(void) x_close(x_db->x_dbm_dirf);
	(void) x_close(x_db->x_dbm_pagf);
	x_free((char *)x_db);
}

x_long x_dbm_forder(x_db, x_key) register x_DBM *x_db; x_datum x_key; {
	x_long x_hash;

	x_hash = x_dcalchash(x_key);
	for (x_db->x_dbm_hmask=0;; x_db->x_dbm_hmask=(x_db->x_dbm_hmask<<1)+1) {
		x_db->x_dbm_blkno = x_hash & x_db->x_dbm_hmask;
		x_db->x_dbm_bitno = x_db->x_dbm_blkno + x_db->x_dbm_hmask;
		if (x_getbit(x_db) == 0)
			break;
	}
	return (x_db->x_dbm_blkno);
}

x_datum x_dbm_fetch(x_db, x_key) register x_DBM *x_db; x_datum x_key; {
	register x_int x_i;
	x_datum x_item;

	if (x_dbm_error(x_db))
		goto x_err;
	x_dbm_access(x_db, x_dcalchash(x_key));
	if ((x_i = x_finddatum(x_db->x_dbm_pagbuf, x_key)) >= 0) {
		x_item = x_makdatum(x_db->x_dbm_pagbuf, x_i+1);
		if (x_item.x_dptr != x_NULL)
			return (x_item);
	}
x_err:
	x_item.x_dptr = x_NULL;
	x_item.x_dsize = 0;
	return (x_item);
}

x_int x_dbm_delete(x_db, x_key) register x_DBM *x_db; x_datum x_key; {
	register x_int x_i;
	x_datum x_item;

	if (x_dbm_error(x_db))
		return (-1);
	if (x_dbm_rdonly(x_db)) {
		x_errno = x_EPERM;
		return (-1);
	}
	x_dbm_access(x_db, x_dcalchash(x_key));
	if ((x_i = x_finddatum(x_db->x_dbm_pagbuf, x_key)) < 0)
		return (-1);
	if (!x_delitem(x_db->x_dbm_pagbuf, x_i))
		goto x_err;
	x_db->x_dbm_pagbno = x_db->x_dbm_blkno;
	(void) x_lseek(x_db->x_dbm_pagf, x_db->x_dbm_blkno*x_PBLKSIZ, x_L_SET);
	if (x_write(x_db->x_dbm_pagf, x_db->x_dbm_pagbuf, x_PBLKSIZ) != x_PBLKSIZ) {
	x_err:
		x_db->x_dbm_flags |= x__DBM_IOERR;
		return (-1);
	}
	return (0);
}

x_int x_dbm_store(x_db, x_key, x_dat, x_replace) register x_DBM *x_db; x_datum x_key; x_datum x_dat; x_int x_replace; {
	register x_int x_i;
	x_datum x_item, x_item1;
	char x_ovfbuf[x_PBLKSIZ];

	if (x_dbm_error(x_db))
		return (-1);
	if (x_dbm_rdonly(x_db)) {
		x_errno = x_EPERM;
		return (-1);
	}
x_loop:
	x_dbm_access(x_db, x_dcalchash(x_key));
	if ((x_i = x_finddatum(x_db->x_dbm_pagbuf, x_key)) >= 0) {
		if (!x_replace)
			return (1);
		if (!x_delitem(x_db->x_dbm_pagbuf, x_i)) {
			x_db->x_dbm_flags |= x__DBM_IOERR;
			return (-1);
		}
	}
	if (!x_additem(x_db->x_dbm_pagbuf, x_key, x_dat))
		goto x_split;
	x_db->x_dbm_pagbno = x_db->x_dbm_blkno;
	(void) x_lseek(x_db->x_dbm_pagf, x_db->x_dbm_blkno*x_PBLKSIZ, x_L_SET);
	if (x_write(x_db->x_dbm_pagf, x_db->x_dbm_pagbuf, x_PBLKSIZ) != x_PBLKSIZ) {
		x_db->x_dbm_flags |= x__DBM_IOERR;
		return (-1);
	}
	return (0);

x_split:
	if (x_key.x_dsize+x_dat.x_dsize+3*sizeof(x_short) >= x_PBLKSIZ) {
		x_db->x_dbm_flags |= x__DBM_IOERR;
		x_errno = x_ENOSPC;
		return (-1);
	}
	x_bzero(x_ovfbuf, x_PBLKSIZ);
	for (x_i=0;;) {
		x_item = x_makdatum(x_db->x_dbm_pagbuf, x_i);
		if (x_item.x_dptr == x_NULL)
			break;
		if (x_dcalchash(x_item) & (x_db->x_dbm_hmask+1)) {
			x_item1 = x_makdatum(x_db->x_dbm_pagbuf, x_i+1);
			if (x_item1.x_dptr == x_NULL) {
				x_fprintf(x_stderr, "ndbm: split not paired\n");
				x_db->x_dbm_flags |= x__DBM_IOERR;
				break;
			}
			if (!x_additem(x_ovfbuf, x_item, x_item1) ||
			    !x_delitem(x_db->x_dbm_pagbuf, x_i)) {
				x_db->x_dbm_flags |= x__DBM_IOERR;
				return (-1);
			}
			continue;
		}
		x_i += 2;
	}
	x_db->x_dbm_pagbno = x_db->x_dbm_blkno;
	(void) x_lseek(x_db->x_dbm_pagf, x_db->x_dbm_blkno*x_PBLKSIZ, x_L_SET);
	if (x_write(x_db->x_dbm_pagf, x_db->x_dbm_pagbuf, x_PBLKSIZ) != x_PBLKSIZ) {
		x_db->x_dbm_flags |= x__DBM_IOERR;
		return (-1);
	}
	(void) x_lseek(x_db->x_dbm_pagf, (x_db->x_dbm_blkno+x_db->x_dbm_hmask+1)*x_PBLKSIZ, x_L_SET);
	if (x_write(x_db->x_dbm_pagf, x_ovfbuf, x_PBLKSIZ) != x_PBLKSIZ) {
		x_db->x_dbm_flags |= x__DBM_IOERR;
		return (-1);
	}
	x_setbit(x_db);
	goto x_loop;
}

x_datum x_dbm_firstkey(x_db) x_DBM *x_db; {

	x_db->x_dbm_blkptr = 0L;
	x_db->x_dbm_keyptr = 0;
	return (x_dbm_nextkey(x_db));
}

x_datum x_dbm_nextkey(x_db) register x_DBM *x_db; {
	struct x_stat x_statb;
	x_datum x_item;

	if (x_dbm_error(x_db) || x_fstat(x_db->x_dbm_pagf, &x_statb) < 0)
		goto x_err;
	x_statb.x_st_size /= x_PBLKSIZ;
	for (;;) {
		if (x_db->x_dbm_blkptr != x_db->x_dbm_pagbno) {
			x_db->x_dbm_pagbno = x_db->x_dbm_blkptr;
			(void) x_lseek(x_db->x_dbm_pagf, x_db->x_dbm_blkptr*x_PBLKSIZ, x_L_SET);
			if (x_read(x_db->x_dbm_pagf, x_db->x_dbm_pagbuf, x_PBLKSIZ) != x_PBLKSIZ)
				x_bzero(x_db->x_dbm_pagbuf, x_PBLKSIZ);
#ifdef x_DEBUG
			else if (x_chkblk(x_db->x_dbm_pagbuf) < 0)
				x_db->x_dbm_flags |= x__DBM_IOERR;
#endif
		}
		if (((x_short *)x_db->x_dbm_pagbuf)[0] != 0) {
			x_item = x_makdatum(x_db->x_dbm_pagbuf, x_db->x_dbm_keyptr);
			if (x_item.x_dptr != x_NULL) {
				x_db->x_dbm_keyptr += 2;
				return (x_item);
			}
			x_db->x_dbm_keyptr = 0;
		}
		if (++x_db->x_dbm_blkptr >= x_statb.x_st_size)
			break;
	}
x_err:
	x_item.x_dptr = x_NULL;
	x_item.x_dsize = 0;
	return (x_item);
}

static x_int x_dbm_access(x_db, x_hash) register x_DBM *x_db; x_long x_hash; {

	for (x_db->x_dbm_hmask=0;; x_db->x_dbm_hmask=(x_db->x_dbm_hmask<<1)+1) {
		x_db->x_dbm_blkno = x_hash & x_db->x_dbm_hmask;
		x_db->x_dbm_bitno = x_db->x_dbm_blkno + x_db->x_dbm_hmask;
		if (x_getbit(x_db) == 0)
			break;
	}
	if (x_db->x_dbm_blkno != x_db->x_dbm_pagbno) {
		x_db->x_dbm_pagbno = x_db->x_dbm_blkno;
		(void) x_lseek(x_db->x_dbm_pagf, x_db->x_dbm_blkno*x_PBLKSIZ, x_L_SET);
		if (x_read(x_db->x_dbm_pagf, x_db->x_dbm_pagbuf, x_PBLKSIZ) != x_PBLKSIZ)
			x_bzero(x_db->x_dbm_pagbuf, x_PBLKSIZ);
#ifdef x_DEBUG
		else if (x_chkblk(x_db->x_dbm_pagbuf) < 0)
			x_db->x_dbm_flags |= x__DBM_IOERR;
#endif
	}
}

static x_int x_getbit(x_db) register x_DBM *x_db; {
	x_long x_bn;
	register x_int x_b, x_i, x_n;
	

	if (x_db->x_dbm_bitno > x_db->x_dbm_maxbno)
		return (0);
	x_n = x_db->x_dbm_bitno % x_BYTESIZ;
	x_bn = x_db->x_dbm_bitno / x_BYTESIZ;
	x_i = x_bn % x_DBLKSIZ;
	x_b = x_bn / x_DBLKSIZ;
	if (x_b != x_db->x_dbm_dirbno) {
		x_db->x_dbm_dirbno = x_b;
		(void) x_lseek(x_db->x_dbm_dirf, (x_long)x_b*x_DBLKSIZ, x_L_SET);
		if (x_read(x_db->x_dbm_dirf, x_db->x_dbm_dirbuf, x_DBLKSIZ) != x_DBLKSIZ)
			x_bzero(x_db->x_dbm_dirbuf, x_DBLKSIZ);
	}
	return (x_db->x_dbm_dirbuf[x_i] & (1<<x_n));
}

static x_int x_setbit(x_db) register x_DBM *x_db; {
	x_long x_bn;
	register x_int x_i, x_n, x_b;

	if (x_db->x_dbm_bitno > x_db->x_dbm_maxbno)
		x_db->x_dbm_maxbno = x_db->x_dbm_bitno;
	x_n = x_db->x_dbm_bitno % x_BYTESIZ;
	x_bn = x_db->x_dbm_bitno / x_BYTESIZ;
	x_i = x_bn % x_DBLKSIZ;
	x_b = x_bn / x_DBLKSIZ;
	if (x_b != x_db->x_dbm_dirbno) {
		x_db->x_dbm_dirbno = x_b;
		(void) x_lseek(x_db->x_dbm_dirf, (x_long)x_b*x_DBLKSIZ, x_L_SET);
		if (x_read(x_db->x_dbm_dirf, x_db->x_dbm_dirbuf, x_DBLKSIZ) != x_DBLKSIZ)
			x_bzero(x_db->x_dbm_dirbuf, x_DBLKSIZ);
	}
	x_db->x_dbm_dirbuf[x_i] |= 1<<x_n;
	x_db->x_dbm_dirbno = x_b;
	(void) x_lseek(x_db->x_dbm_dirf, (x_long)x_b*x_DBLKSIZ, x_L_SET);
	if (x_write(x_db->x_dbm_dirf, x_db->x_dbm_dirbuf, x_DBLKSIZ) != x_DBLKSIZ)
		x_db->x_dbm_flags |= x__DBM_IOERR;
}

static x_int x_datum x_makdatum(x_buf, x_n) char x_buf[x_PBLKSIZ]; x_int x_n; {
	register x_short *x_sp;
	register x_int x_t;
	x_datum x_item;

	x_sp = (x_short *)x_buf;
	if ((unsigned)x_n >= x_sp[0]) {
		x_item.x_dptr = x_NULL;
		x_item.x_dsize = 0;
		return (x_item);
	}
	x_t = x_PBLKSIZ;
	if (x_n > 0)
		x_t = x_sp[x_n];
	x_item.x_dptr = x_buf+x_sp[x_n+1];
	x_item.x_dsize = x_t - x_sp[x_n+1];
	return (x_item);
}

static x_int x_finddatum(x_buf, x_item) char x_buf[x_PBLKSIZ]; x_datum x_item; {
	register x_short *x_sp;
	register x_int x_i, x_n, x_j;

	x_sp = (x_short *)x_buf;
	x_n = x_PBLKSIZ;
	for (x_i=0, x_j=x_sp[0]; x_i<x_j; x_i+=2, x_n = x_sp[x_i]) {
		x_n -= x_sp[x_i+1];
		if (x_n != x_item.x_dsize)
			continue;
		if (x_n == 0 || x_bcmp(&x_buf[x_sp[x_i+1]], x_item.x_dptr, x_n) == 0)
			return (x_i);
	}
	return (-1);
}

static  x_int x_hitab[16]
/* ken's
{
	055,043,036,054,063,014,004,005,
	010,064,077,000,035,027,025,071,
};
*/
 = {    61, 57, 53, 49, 45, 41, 37, 33,
	29, 25, 21, 17, 13,  9,  5,  1,
};
static  x_long x_hltab[64]
 = {
	06100151277L,06106161736L,06452611562L,05001724107L,
	02614772546L,04120731531L,04665262210L,07347467531L,
	06735253126L,06042345173L,03072226605L,01464164730L,
	03247435524L,07652510057L,01546775256L,05714532133L,
	06173260402L,07517101630L,02431460343L,01743245566L,
	00261675137L,02433103631L,03421772437L,04447707466L,
	04435620103L,03757017115L,03641531772L,06767633246L,
	02673230344L,00260612216L,04133454451L,00615531516L,
	06137717526L,02574116560L,02304023373L,07061702261L,
	05153031405L,05322056705L,07401116734L,06552375715L,
	06165233473L,05311063631L,01212221723L,01052267235L,
	06000615237L,01075222665L,06330216006L,04402355630L,
	01451177262L,02000133436L,06025467062L,07121076461L,
	03123433522L,01010635225L,01716177066L,05161746527L,
	01736635071L,06243505026L,03637211610L,01756474365L,
	04723077174L,03642763134L,05750130273L,03655541561L,
};

static x_long x_hashinc(x_db, x_hash) register x_DBM *x_db; x_long x_hash; {
	x_long x_bit;

	x_hash &= x_db->x_dbm_hmask;
	x_bit = x_db->x_dbm_hmask+1;
	for (;;) {
		x_bit >>= 1;
		if (x_bit == 0)
			return (0L);
		if ((x_hash & x_bit) == 0)
			return (x_hash | x_bit);
		x_hash &= ~x_bit;
	}
}

static x_long x_dcalchash(x_item) x_datum x_item; {
	register x_int x_s, x_c, x_j;
	register char *x_cp;
	register x_long x_hashl;
	register x_int x_hashi;

	x_hashl = 0;
	x_hashi = 0;
	for (x_cp = x_item.x_dptr, x_s=x_item.x_dsize; --x_s >= 0; ) {
		x_c = *x_cp++;
		for (x_j=0; x_j<x_BYTESIZ; x_j+=4) {
			x_hashi += x_hitab[x_c&017];
			x_hashl += x_hltab[x_hashi&63];
			x_c >>= 4;
		}
	}
	return (x_hashl);
}

/*
 * Delete pairs of items (n & n+1).
 */
static x_int x_delitem(x_buf, x_n) char x_buf[x_PBLKSIZ]; x_int x_n; {
	register x_short *x_sp, *x_sp1;
	register x_int x_i1, x_i2;

	x_sp = (x_short *)x_buf;
	x_i2 = x_sp[0];
	if ((unsigned)x_n >= x_i2 || (x_n & 1))
		return (0);
	if (x_n == x_i2-2) {
		x_sp[0] -= 2;
		return (1);
	}
	x_i1 = x_PBLKSIZ;
	if (x_n > 0)
		x_i1 = x_sp[x_n];
	x_i1 -= x_sp[x_n+2];
	if (x_i1 > 0) {
		x_i2 = x_sp[x_i2];
		x_bcopy(&x_buf[x_i2], &x_buf[x_i2 + x_i1], x_sp[x_n+2] - x_i2);
	}
	x_sp[0] -= 2;
	for (x_sp1 = x_sp + x_sp[0], x_sp += x_n+1; x_sp <= x_sp1; x_sp++)
		x_sp[0] = x_sp[2] + x_i1;
	return (1);
}

/*
 * Add pairs of items (item & item1).
 */
static x_int x_additem(x_buf, x_item, x_item1) char x_buf[x_PBLKSIZ]; x_datum x_item; x_datum x_item1; {
	register x_short *x_sp;
	register x_int x_i1, x_i2;

	x_sp = (x_short *)x_buf;
	x_i1 = x_PBLKSIZ;
	x_i2 = x_sp[0];
	if (x_i2 > 0)
		x_i1 = x_sp[x_i2];
	x_i1 -= x_item.x_dsize + x_item1.x_dsize;
	if (x_i1 <= (x_i2+3) * sizeof(x_short))
		return (0);
	x_sp[0] += 2;
	x_sp[++x_i2] = x_i1 + x_item1.x_dsize;
	x_bcopy(x_item.x_dptr, &x_buf[x_i1 + x_item1.x_dsize], x_item.x_dsize);
	x_sp[++x_i2] = x_i1;
	x_bcopy(x_item1.x_dptr, &x_buf[x_i1], x_item1.x_dsize);
	return (1);
}

#ifdef x_DEBUG
static x_int x_chkblk(x_buf) char x_buf[x_PBLKSIZ]; {
	register x_short *x_sp;
	register x_int x_t, x_i;

	x_sp = (x_short *)x_buf;
	x_t = x_PBLKSIZ;
	for (x_i=0; x_i<x_sp[0]; x_i++) {
		if (x_sp[x_i+1] > x_t)
			return (-1);
		x_t = x_sp[x_i+1];
	}
	if (x_t < (x_sp[0]+1)*sizeof(x_short))
		return (-1);
	return (0);
}
#endif
