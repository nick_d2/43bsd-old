#include "x_.h"

#include <sys/x_time.h>
#include <x_stdlib.h>
#include <x_stdio.h>
#include <x_strings.h>
/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)timezone.c	5.2 (Berkeley) 3/9/86";
#endif

/*
 * The arguments are the number of minutes of time
 * you are westward from Greenwich and whether DST is in effect.
 * It returns a string
 * giving the name of the local timezone.
 *
 * Sorry, I don't know all the names.
 */

static struct x_zone {
	x_int	x_offset;
	char	*x_stdzone;
	char	*x_dlzone;
} x_zonetab[] = {
	-1*60, "MET", "MET DST",	/* Middle European */
	-2*60, "EET", "EET DST",	/* Eastern European */
	4*60, "AST", "ADT",		/* Atlantic */
	5*60, "EST", "EDT",		/* Eastern */
	6*60, "CST", "CDT",		/* Central */
	7*60, "MST", "MDT",		/* Mountain */
	8*60, "PST", "PDT",		/* Pacific */
#ifdef x_notdef
	/* there's no way to distinguish this from WET */
	0, "GMT", 0,			/* Greenwich */
#endif
	0*60, "WET", "WET DST",		/* Western European */
	-10*60, "EST", "EST",		/* Aust: Eastern */
	-10*60+30, "CST", "CST",	/* Aust: Central */
	-8*60, "WST", 0,		/* Aust: Western */
	-1
};

char *x_timezone(x_zone, x_dst) x_int x_zone; x_int x_dst; {
	register struct x_zone *x_zp;
	static char x_czone[10];
	char *x_sign;
	register char *x_p, *x_q;
	char *x_getenv(), *x_index();

	if (x_p = x_getenv("TZNAME")) {
		if (x_q = x_index(x_p, ',')) {
			if (x_dst)
				return(++x_q);
			else {
				*x_q = '\0';
				x_strncpy(x_czone, x_p, sizeof(x_czone)-1);
				x_czone[sizeof(x_czone)-1] = '\0';
				*x_q = ',';
				return (x_czone);
			}
		}
		return(x_p);
	}
	for (x_zp=x_zonetab; x_zp->x_offset!=-1; x_zp++)
		if (x_zp->x_offset==x_zone) {
			if (x_dst && x_zp->x_dlzone)
				return(x_zp->x_dlzone);
			if (!x_dst && x_zp->x_stdzone)
				return(x_zp->x_stdzone);
		}
	if (x_zone<0) {
		x_zone = -x_zone;
		x_sign = "+";
	} else
		x_sign = "-";
	x_sprintf(x_czone, "GMT%s%d:%02d", x_sign, x_zone/60, x_zone%60);
	return(x_czone);
}
