#include "x_.h"

#include <x_strings.h>
#include <x_unistd.h>
/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)getwd.c	5.2 (Berkeley) 3/9/86";
#endif

/*
 * getwd() returns the pathname of the current working directory. On error
 * an error message is copied to pathname and null pointer is returned.
 */
#include <sys/x_param.h>
#include <sys/x_stat.h>
#include <sys/x_dir.h>

#define x_GETWDERR(x_s)	x_strcpy(x_pathname, (x_s));

char *x_strcpy();
static x_int x_pathsize;			/* pathname length */

#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

static char *x_prepend __P((register char *x_dirname, register char *x_pathname));

char *x_getwd(x_pathname) char *x_pathname; {
	char x_pathbuf[x_MAXPATHLEN];		/* temporary pathname buffer */
	char *x_pnptr = &x_pathbuf[(sizeof x_pathbuf)-1]; /* pathname pointer */
	char x_curdir[x_MAXPATHLEN];	/* current directory buffer */
	char *x_dptr = x_curdir;		/* directory pointer */
	char *x_prepend();		/* prepend dirname to pathname */
	x_dev_t x_cdev, x_rdev;		/* current & root device number */
	x_ino_t x_cino, x_rino;		/* current & root inode number */
	x_DIR *x_dirp;			/* directory stream */
	struct x_direct *x_dir;		/* directory entry struct */
	struct x_stat x_d, x_dd;		/* file status struct */

	x_pathsize = 0;
	*x_pnptr = '\0';
	if (x_stat("/", &x_d) < 0) {
		x_GETWDERR("getwd: can't stat /");
		return (x_NULL);
	}
	x_rdev = x_d.x_st_dev;
	x_rino = x_d.x_st_ino;
	x_strcpy(x_dptr, "./");
	x_dptr += 2;
	if (x_stat(x_curdir, &x_d) < 0) {
		x_GETWDERR("getwd: can't stat .");
		return (x_NULL);
	}
	for (;;) {
		if (x_d.x_st_ino == x_rino && x_d.x_st_dev == x_rdev)
			break;		/* reached root directory */
		x_cino = x_d.x_st_ino;
		x_cdev = x_d.x_st_dev;
		x_strcpy(x_dptr, "../");
		x_dptr += 3;
		if ((x_dirp = x_opendir(x_curdir)) == x_NULL) {
			x_GETWDERR("getwd: can't open ..");
			return (x_NULL);
		}
		x_fstat(x_dirp->x_dd_fd, &x_d);
		if (x_cdev == x_d.x_st_dev) {
			if (x_cino == x_d.x_st_ino) {
				/* reached root directory */
				x_closedir(x_dirp);
				break;
			}
			do {
				if ((x_dir = x_readdir(x_dirp)) == x_NULL) {
					x_closedir(x_dirp);
					x_GETWDERR("getwd: read error in ..");
					return (x_NULL);
				}
			} while (x_dir->x_d_ino != x_cino);
		} else
			do {
				if ((x_dir = x_readdir(x_dirp)) == x_NULL) {
					x_closedir(x_dirp);
					x_GETWDERR("getwd: read error in ..");
					return (x_NULL);
				}
				x_strcpy(x_dptr, x_dir->x_d_name);
				x_lstat(x_curdir, &x_dd);
			} while(x_dd.x_st_ino != x_cino || x_dd.x_st_dev != x_cdev);
		x_closedir(x_dirp);
		x_pnptr = x_prepend("/", x_prepend(x_dir->x_d_name, x_pnptr));
	}
	if (*x_pnptr == '\0')		/* current dir == root dir */
		x_strcpy(x_pathname, "/");
	else
		x_strcpy(x_pathname, x_pnptr);
	return (x_pathname);
}

/*
 * prepend() tacks a directory name onto the front of a pathname.
 */
static char *x_prepend(x_dirname, x_pathname) register char *x_dirname; register char *x_pathname; {
	register x_int x_i;			/* directory name size counter */

	for (x_i = 0; *x_dirname != '\0'; x_i++, x_dirname++)
		continue;
	if ((x_pathsize += x_i) < x_MAXPATHLEN)
		while (x_i-- > 0)
			*--x_pathname = *--x_dirname;
	return (x_pathname);
}
