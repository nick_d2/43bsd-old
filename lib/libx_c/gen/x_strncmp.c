#include "x_.h"

#include <x_strings.h>
#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)strncmp.c	5.2 (Berkeley) 3/9/86";
#endif

/*
 * Compare strings (at most n bytes):  s1>s2: >0  s1==s2: 0  s1<s2: <0
 */

x_int x_strncmp(x_s1, x_s2, x_n) register char *x_s1; register char *x_s2; register x_int x_n; {

	while (--x_n >= 0 && *x_s1 == *x_s2++)
		if (*x_s1++ == '\0')
			return(0);
	return(x_n<0 ? 0 : *x_s1 - *--x_s2);
}
