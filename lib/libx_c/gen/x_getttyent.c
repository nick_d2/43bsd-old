#include "x_.h"

/*
 * Copyright (c) 1985 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)getttyent.c	5.4 (Berkeley) 5/19/86";
#endif

#include <x_stdio.h>
#include <x_strings.h>
#include <x_ttyent.h>

static char x_TTYFILE[] = "/etc/ttys";
static char x_zapchar;
static x_FILE *x_tf = x_NULL;
#define x_LINE 256
static char x_line[x_LINE];
static struct x_ttyent x_tty;

#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

static char *x_skip __P((register char *x_p));
static char *x_value __P((register char *x_p));

x_int x_setttyent() {
	if (x_tf == x_NULL)
		x_tf = x_fopen(x_TTYFILE, "r");
	else
		x_rewind(x_tf);
}

x_int x_endttyent() {
	if (x_tf != x_NULL) {
		(void) x_fclose(x_tf);
		x_tf = x_NULL;
	}
}

#define x_QUOTED	1

/*
 * Skip over the current field, removing quotes,
 * and return a pointer to the next field.
 */
static char *x_skip(x_p) register char *x_p; {
	register char *x_t = x_p;
	register x_int x_c;
	register x_int x_q = 0;

	for (; (x_c = *x_p) != '\0'; x_p++) {
		if (x_c == '"') {
			x_q ^= x_QUOTED;	/* obscure, but nice */
			continue;
		}
		if (x_q == x_QUOTED && *x_p == '\\' && *(x_p+1) == '"')
			x_p++;
		*x_t++ = *x_p;
		if (x_q == x_QUOTED)
			continue;
		if (x_c == '#') {
			x_zapchar = x_c;
			*x_p = 0;
			break;
		}
		if (x_c == '\t' || x_c == ' ' || x_c == '\n') {
			x_zapchar = x_c;
			*x_p++ = 0;
			while ((x_c = *x_p) == '\t' || x_c == ' ' || x_c == '\n')
				x_p++;
			break;
		}
	}
	*--x_t = '\0';
	return (x_p);
}

static char *x_value(x_p) register char *x_p; {
	if ((x_p = x_index(x_p,'=')) == 0)
		return(x_NULL);
	x_p++;			/* get past the = sign */
	return(x_p);
}

struct x_ttyent *x_getttyent() {
	register char *x_p;
	register x_int x_c;

	if (x_tf == x_NULL) {
		if ((x_tf = x_fopen(x_TTYFILE, "r")) == x_NULL)
			return (x_NULL);
	}
	do {
		x_p = x_fgets(x_line, x_LINE, x_tf);
		if (x_p == x_NULL)
			return (x_NULL);
		while ((x_c = *x_p) == '\t' || x_c == ' ' || x_c == '\n')
			x_p++;
	} while (x_c == '\0' || x_c == '#');
	x_zapchar = 0;
	x_tty.x_ty_name = x_p;
	x_p = x_skip(x_p);
	x_tty.x_ty_getty = x_p;
	x_p = x_skip(x_p);
	x_tty.x_ty_type = x_p;
	x_p = x_skip(x_p);
	x_tty.x_ty_status = 0;
	x_tty.x_ty_window = x_NULL;
	for (; *x_p; x_p = x_skip(x_p)) {
#define x_space(x_x) ((x_c = x_p[x_x]) == ' ' || x_c == '\t' || x_c == '\n')
		if (x_strncmp(x_p, "on", 2) == 0 && x_space(2))
			x_tty.x_ty_status |= x_TTY_ON;
		else if (x_strncmp(x_p, "off", 3) == 0 && x_space(3))
			x_tty.x_ty_status &= ~x_TTY_ON;
		else if (x_strncmp(x_p, "secure", 6) == 0 && x_space(6))
			x_tty.x_ty_status |= x_TTY_SECURE;
		else if (x_strncmp(x_p, "window=", 7) == 0)
			x_tty.x_ty_window = x_value(x_p);
		else
			break;
	}
	if (x_zapchar == '#' || *x_p == '#')
		while ((x_c = *++x_p) == ' ' || x_c == '\t')
			;
	x_tty.x_ty_comment = x_p;
	if (*x_p == 0)
		x_tty.x_ty_comment = 0;
	if (x_p = x_index(x_p, '\n'))
		*x_p = '\0';
	return(&x_tty);
}
