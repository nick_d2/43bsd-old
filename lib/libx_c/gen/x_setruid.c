#include "x_.h"

#include <x_unistd.h>
/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)setruid.c	5.2 (Berkeley) 3/9/86";
#endif

x_int x_setruid(x_ruid) x_int x_ruid; {

	return (x_setreuid(x_ruid, -1));
}
