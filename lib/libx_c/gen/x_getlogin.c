#include "x_.h"

#include <x_unistd.h>
#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)getlogin.c	5.3 (Berkeley) 5/9/86";
#endif

#include <x_utmp.h>

static	char x_UTMP[]	= "/etc/utmp";
static	struct x_utmp x_ubuf;

char *x_getlogin() {
	register x_int x_me, x_uf;
	register char *x_cp;

	if (!(x_me = x_ttyslot()))
		return(0);
	if ((x_uf = x_open(x_UTMP, 0)) < 0)
		return (0);
	x_lseek (x_uf, (x_long)(x_me*sizeof(x_ubuf)), 0);
	if (x_read(x_uf, (char *)&x_ubuf, sizeof (x_ubuf)) != sizeof (x_ubuf)) {
		x_close(x_uf);
		return (0);
	}
	x_close(x_uf);
	if (x_ubuf.x_ut_name[0] == '\0')
		return (0);
	x_ubuf.x_ut_name[sizeof (x_ubuf.x_ut_name)] = ' ';
	for (x_cp = x_ubuf.x_ut_name; *x_cp++ != ' '; )
		;
	*--x_cp = '\0';
	return (x_ubuf.x_ut_name);
}
