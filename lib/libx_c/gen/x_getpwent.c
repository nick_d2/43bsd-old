#include "x_.h"

#include <x_stdlib.h>
/*
 * Copyright (c) 1984 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)getpwent.c	5.2 (Berkeley) 3/9/86";
#endif

#include <x_stdio.h>
#include <x_pwd.h>
#include <x_ndbm.h>

static char x_EMPTY[] = "";
static x_FILE *x_pwf = x_NULL;
static char x_line[x_BUFSIZ+1];
static struct x_passwd x_passwd;

/*
 * The following are shared with getpwnamuid.c
 */
char	*x__pw_file = "/etc/passwd";
x_DBM	*x__pw_db;
x_int	x__pw_stayopen;

#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

static char *x_pwskip __P((register char *x_p));

x_int x_setpwent() {
	if (x_pwf == x_NULL)
		x_pwf = x_fopen(x__pw_file, "r");
	else
		x_rewind(x_pwf);
}

x_int x_endpwent() {
	if (x_pwf != x_NULL) {
		x_fclose(x_pwf);
		x_pwf = x_NULL;
	}
	if (x__pw_db != (x_DBM *)0) {
		x_dbm_close(x__pw_db);
		x__pw_db = (x_DBM *)0;
		x__pw_stayopen = 0;
	}
}

static char *x_pwskip(x_p) register char *x_p; {
	while (*x_p && *x_p != ':' && *x_p != '\n')
		++x_p;
	if (*x_p)
		*x_p++ = 0;
	return(x_p);
}

struct x_passwd *x_getpwent() {
	register char *x_p;

	if (x_pwf == x_NULL) {
		if ((x_pwf = x_fopen( x__pw_file, "r" )) == x_NULL)
			return(0);
	}
	x_p = x_fgets(x_line, x_BUFSIZ, x_pwf);
	if (x_p == x_NULL)
		return(0);
	x_passwd.x_pw_name = x_p;
	x_p = x_pwskip(x_p);
	x_passwd.x_pw_passwd = x_p;
	x_p = x_pwskip(x_p);
	x_passwd.x_pw_uid = x_atoi(x_p);
	x_p = x_pwskip(x_p);
	x_passwd.x_pw_gid = x_atoi(x_p);
	x_passwd.x_pw_quota = 0;
	x_passwd.x_pw_comment = x_EMPTY;
	x_p = x_pwskip(x_p);
	x_passwd.x_pw_gecos = x_p;
	x_p = x_pwskip(x_p);
	x_passwd.x_pw_dir = x_p;
	x_p = x_pwskip(x_p);
	x_passwd.x_pw_shell = x_p;
	while (*x_p && *x_p != '\n')
		x_p++;
	*x_p = '\0';
	return(&x_passwd);
}

x_int x_setpwfile(x_file) char *x_file; {
	x__pw_file = x_file;
}
