#include "x_.h"

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)ctype_.c	5.4 (Berkeley) 3/9/86";
#endif

#include	<x_ctype.h>

char x__ctype_[1 + 256] = {
	0,
	x__C,	x__C,	x__C,	x__C,	x__C,	x__C,	x__C,	x__C,
	x__C,	x__C|x__S,	x__C|x__S,	x__C|x__S,	x__C|x__S,	x__C|x__S,	x__C,	x__C,
	x__C,	x__C,	x__C,	x__C,	x__C,	x__C,	x__C,	x__C,
	x__C,	x__C,	x__C,	x__C,	x__C,	x__C,	x__C,	x__C,
	x__S|x__B,	x__P,	x__P,	x__P,	x__P,	x__P,	x__P,	x__P,
	x__P,	x__P,	x__P,	x__P,	x__P,	x__P,	x__P,	x__P,
	x__N,	x__N,	x__N,	x__N,	x__N,	x__N,	x__N,	x__N,
	x__N,	x__N,	x__P,	x__P,	x__P,	x__P,	x__P,	x__P,
	x__P,	x__U|x__X,	x__U|x__X,	x__U|x__X,	x__U|x__X,	x__U|x__X,	x__U|x__X,	x__U,
	x__U,	x__U,	x__U,	x__U,	x__U,	x__U,	x__U,	x__U,
	x__U,	x__U,	x__U,	x__U,	x__U,	x__U,	x__U,	x__U,
	x__U,	x__U,	x__U,	x__P,	x__P,	x__P,	x__P,	x__P,
	x__P,	x__L|x__X,	x__L|x__X,	x__L|x__X,	x__L|x__X,	x__L|x__X,	x__L|x__X,	x__L,
	x__L,	x__L,	x__L,	x__L,	x__L,	x__L,	x__L,	x__L,
	x__L,	x__L,	x__L,	x__L,	x__L,	x__L,	x__L,	x__L,
	x__L,	x__L,	x__L,	x__P,	x__P,	x__P,	x__P,	x__C
};
