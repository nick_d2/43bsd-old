#include "x_.h"

#include <x_unistd.h>
/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)setuid.c	5.2 (Berkeley) 3/9/86";
#endif

/*
 * Backwards compatible setuid.
 */
x_int x_setuid(x_uid) x_int x_uid; {

	return (x_setreuid(x_uid, x_uid));
}
