#include "x_.h"

#include <x_unistd.h>
/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)readdir.c	5.2 (Berkeley) 3/9/86";
#endif

#include <sys/x_param.h>
#include <sys/x_dir.h>

/*
 * get next entry in a directory.
 */
struct x_direct *x_readdir(x_dirp) register x_DIR *x_dirp; {
	register struct x_direct *x_dp;

	for (;;) {
		if (x_dirp->x_dd_loc == 0) {
			x_dirp->x_dd_size = x_read(x_dirp->x_dd_fd, x_dirp->x_dd_buf, 
			    x_DIRBLKSIZ);
			if (x_dirp->x_dd_size <= 0)
				return x_NULL;
		}
		if (x_dirp->x_dd_loc >= x_dirp->x_dd_size) {
			x_dirp->x_dd_loc = 0;
			continue;
		}
		x_dp = (struct x_direct *)(x_dirp->x_dd_buf + x_dirp->x_dd_loc);
		if (x_dp->x_d_reclen <= 0 ||
		    x_dp->x_d_reclen > x_DIRBLKSIZ + 1 - x_dirp->x_dd_loc)
			return x_NULL;
		x_dirp->x_dd_loc += x_dp->x_d_reclen;
		if (x_dp->x_d_ino == 0)
			continue;
		return (x_dp);
	}
}
