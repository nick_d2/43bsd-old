#include "x_.h"

#include <x_unistd.h>
#include <x_time.h>
/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)ctime.c	5.5 (Berkeley) 3/9/86";
#endif

/*
 * This routine converts time as follows.
 * The epoch is 0000 Jan 1 1970 GMT.
 * The argument time is in seconds since then.
 * The localtime(t) entry returns a pointer to an array
 * containing
 *  seconds (0-59)
 *  minutes (0-59)
 *  hours (0-23)
 *  day of month (1-31)
 *  month (0-11)
 *  year-1970
 *  weekday (0-6, Sun is 0)
 *  day of the year
 *  daylight savings flag
 *
 * The routine calls the system to determine the local
 * timezone and whether Daylight Saving Time is permitted locally.
 * (DST is then determined by the current local rules)
 *
 * The routine does not work
 * in Saudi Arabia which runs on Solar time.
 *
 * asctime(tvec))
 * where tvec is produced by localtime
 * returns a ptr to a character string
 * that has the ascii time in the form
 *	Thu Jan 01 00:00:00 1970\n\0
 *	0123456789012345678901234 5
 *	0	  1	    2
 *
 * ctime(t) just calls localtime, then asctime.
 */

#include <sys/x_time.h>
#include <sys/x_types.h>
#include <sys/x_timeb.h>

static	char	x_cbuf[26];
static	x_int	x_dmsize[12] =
{
	31,
	28,
	31,
	30,
	31,
	30,
	31,
	31,
	30,
	31,
	30,
	31
};

/*
 * The following table is used for 1974 and 1975 and
 * gives the day number of the first day after the Sunday of the
 * change.
 */
struct x_dstab {
	x_int	x_dayyr;
	x_int	x_daylb;
	x_int	x_dayle;
};

static struct x_dstab x_usdaytab[] = {
	1974,	5,	333,	/* 1974: Jan 6 - last Sun. in Nov */
	1975,	58,	303,	/* 1975: Last Sun. in Feb - last Sun in Oct */
	0,	119,	303,	/* all other years: end Apr - end Oct */
};
static struct x_dstab x_ausdaytab[] = {
	1970,	400,	0,	/* 1970: no daylight saving at all */
	1971,	303,	0,	/* 1971: daylight saving from Oct 31 */
	1972,	303,	58,	/* 1972: Jan 1 -> Feb 27 & Oct 31 -> dec 31 */
	0,	303,	65,	/* others: -> Mar 7, Oct 31 -> */
};

/*
 * The European tables ... based on hearsay
 * Believed correct for:
 *	WE:	Great Britain, Portugal?
 *	ME:	Belgium, Luxembourg, Netherlands, Denmark, Norway,
 *		Austria, Poland, Czechoslovakia, Sweden, Switzerland,
 *		DDR, DBR, France, Spain, Hungary, Italy, Jugoslavia
 *		Finland (EE timezone, but ME dst rules)
 * Eastern European dst is unknown, we'll make it ME until someone speaks up.
 *	EE:	Bulgaria, Greece, Rumania, Turkey, Western Russia
 *
 * Ireland is unpredictable.  (Years when Easter Sunday just happens ...)
 * Years before 1983 are suspect.
 */
static struct x_dstab x_wedaytab[] = {
	1983,	89,	296,	/* 1983: end March - end Oct */
	0,	89,	303,	/* others: end March - end Oct */
};

static struct x_dstab x_medaytab[] = {
	1983,	89,	296,	/* 1983: end March - end Oct */
	0,	89,	272,	/* others: end March - end Sep */
};

/*
 * Canada, same as the US, except no early 70's fluctuations.
 * Can this really be right ??
 */
static struct x_dstab x_candaytab[] = {
	0,	119,	303,	/* all years: end Apr - end Oct */
};

static struct x_dayrules {
	x_int		x_dst_type;	/* number obtained from system */
	x_int		x_dst_hrs;	/* hours to add when dst on */
	struct	x_dstab *	x_dst_rules;	/* one of the above */
	enum {x_STH,x_NTH}	x_dst_hemi;	/* southern, northern hemisphere */
} x_dayrules [] = {
	x_DST_USA,	1,	x_usdaytab,	x_NTH,
	x_DST_AUST,	1,	x_ausdaytab,	x_STH,
	x_DST_WET,	1,	x_wedaytab,	x_NTH,
	x_DST_MET,	1,	x_medaytab,	x_NTH,
	x_DST_EET,	1,	x_medaytab,	x_NTH,	/* XXX */
	x_DST_CAN,	1,	x_candaytab,	x_NTH,
	-1,
};

struct x_tm	*x_gmtime();
char		*x_ct_numb();
struct x_tm	*x_localtime();
char	*x_ctime();
char	*x_ct_num();
char	*x_asctime();

#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

static x_int x_sunday __P((register struct x_tm *x_t, register x_int x_d));
static char *x_ct_numb __P((register char *x_cp, x_int x_n));

char *x_ctime(x_t) x_time_t *x_t; {
	return(x_asctime(x_localtime(x_t)));
}

struct x_tm *x_localtime(x_tim) x_time_t *x_tim; {
	register x_int x_dayno;
	register struct x_tm *x_ct;
	register x_int x_dalybeg, x_daylend;
	register struct x_dayrules *x_dr;
	register struct x_dstab *x_ds;
	x_int x_year;
	x_time_t x_copyt;
	struct x_timeval x_curtime;
	static struct x_timezone x_zone;
	static x_int x_init = 0;

	if (!x_init) {
		x_gettimeofday(&x_curtime, &x_zone);
		x_init++;
	}
	x_copyt = *x_tim - (x_time_t)x_zone.x_tz_minuteswest*60;
	x_ct = x_gmtime(&x_copyt);
	x_dayno = x_ct->x_tm_yday;
	for (x_dr = x_dayrules; x_dr->x_dst_type >= 0; x_dr++)
		if (x_dr->x_dst_type == x_zone.x_tz_dsttime)
			break;
	if (x_dr->x_dst_type >= 0) {
		x_year = x_ct->x_tm_year + 1900;
		for (x_ds = x_dr->x_dst_rules; x_ds->x_dayyr; x_ds++)
			if (x_ds->x_dayyr == x_year)
				break;
		x_dalybeg = x_ds->x_daylb;	/* first Sun after dst starts */
		x_daylend = x_ds->x_dayle;	/* first Sun after dst ends */
		x_dalybeg = x_sunday(x_ct, x_dalybeg);
		x_daylend = x_sunday(x_ct, x_daylend);
		switch (x_dr->x_dst_hemi) {
		case x_NTH:
		    if (!(
		       (x_dayno>x_dalybeg || (x_dayno==x_dalybeg && x_ct->x_tm_hour>=2)) &&
		       (x_dayno<x_daylend || (x_dayno==x_daylend && x_ct->x_tm_hour<1))
		    ))
			    return(x_ct);
		    break;
		case x_STH:
		    if (!(
		       (x_dayno>x_dalybeg || (x_dayno==x_dalybeg && x_ct->x_tm_hour>=2)) ||
		       (x_dayno<x_daylend || (x_dayno==x_daylend && x_ct->x_tm_hour<2))
		    ))
			    return(x_ct);
		    break;
		x_default:
		    return(x_ct);
		}
	        x_copyt += x_dr->x_dst_hrs*60*60;
		x_ct = x_gmtime(&x_copyt);
		x_ct->x_tm_isdst++;
	}
	return(x_ct);
}

/*
 * The argument is a 0-origin day number.
 * The value is the day number of the last
 * Sunday on or before the day.
 */
static x_int x_sunday(x_t, x_d) register struct x_tm *x_t; register x_int x_d; {
	if (x_d >= 58)
		x_d += x_dysize(x_t->x_tm_year) - 365;
	return(x_d - (x_d - x_t->x_tm_yday + x_t->x_tm_wday + 700) % 7);
}

struct x_tm *x_gmtime(x_tim) x_time_t *x_tim; {
	register x_int x_d0, x_d1;
	x_long x_hms, x_day;
	register x_int *x_tp;
	static struct x_tm x_xtime;

	/*
	 * break initial number into days
	 */
	x_hms = *x_tim % 86400;
	x_day = *x_tim / 86400;
	if (x_hms<0) {
		x_hms += 86400;
		x_day -= 1;
	}
	x_tp = (x_int *)&x_xtime;

	/*
	 * generate hours:minutes:seconds
	 */
	*x_tp++ = x_hms%60;
	x_d1 = x_hms/60;
	*x_tp++ = x_d1%60;
	x_d1 /= 60;
	*x_tp++ = x_d1;

	/*
	 * day is the day number.
	 * generate day of the week.
	 * The addend is 4 mod 7 (1/1/1970 was Thursday)
	 */

	x_xtime.x_tm_wday = (x_day+7340036)%7;

	/*
	 * year number
	 */
	if (x_day>=0) for(x_d1=70; x_day >= x_dysize(x_d1); x_d1++)
		x_day -= x_dysize(x_d1);
	else for (x_d1=70; x_day<0; x_d1--)
		x_day += x_dysize(x_d1-1);
	x_xtime.x_tm_year = x_d1;
	x_xtime.x_tm_yday = x_d0 = x_day;

	/*
	 * generate month
	 */

	if (x_dysize(x_d1)==366)
		x_dmsize[1] = 29;
	for(x_d1=0; x_d0 >= x_dmsize[x_d1]; x_d1++)
		x_d0 -= x_dmsize[x_d1];
	x_dmsize[1] = 28;
	*x_tp++ = x_d0+1;
	*x_tp++ = x_d1;
	x_xtime.x_tm_isdst = 0;
	return(&x_xtime);
}

char *x_asctime(x_t) struct x_tm *x_t; {
	register char *x_cp, *x_ncp;
	register x_int *x_tp;

	x_cp = x_cbuf;
	for (x_ncp = "Day Mon 00 00:00:00 1900\n"; *x_cp++ = *x_ncp++;);
	x_ncp = &"SunMonTueWedThuFriSat"[3*x_t->x_tm_wday];
	x_cp = x_cbuf;
	*x_cp++ = *x_ncp++;
	*x_cp++ = *x_ncp++;
	*x_cp++ = *x_ncp++;
	x_cp++;
	x_tp = &x_t->x_tm_mon;
	x_ncp = &"JanFebMarAprMayJunJulAugSepOctNovDec"[(*x_tp)*3];
	*x_cp++ = *x_ncp++;
	*x_cp++ = *x_ncp++;
	*x_cp++ = *x_ncp++;
	x_cp = x_ct_numb(x_cp, *--x_tp);
	x_cp = x_ct_numb(x_cp, *--x_tp+100);
	x_cp = x_ct_numb(x_cp, *--x_tp+100);
	x_cp = x_ct_numb(x_cp, *--x_tp+100);
	if (x_t->x_tm_year>=100) {
		x_cp[1] = '2';
		x_cp[2] = '0' + (x_t->x_tm_year-100) / 100;
	}
	x_cp += 2;
	x_cp = x_ct_numb(x_cp, x_t->x_tm_year+100);
	return(x_cbuf);
}

x_int x_dysize(x_y) x_int x_y; {
	if((x_y%4) == 0)
		return(366);
	return(365);
}

static char *x_ct_numb(x_cp, x_n) register char *x_cp; x_int x_n; {
	x_cp++;
	if (x_n>=10)
		*x_cp++ = (x_n/10)%10 + '0';
	else
		*x_cp++ = ' ';
	*x_cp++ = x_n%10 + '0';
	return(x_cp);
}
