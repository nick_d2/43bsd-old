#include "x_.h"

#include <x_stdlib.h>
#include <x_mp.h>
#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)calloc.c	5.2 (Berkeley) 3/9/86";
#endif

/*
 * Calloc - allocate and clear memory block
 */
char *x_calloc(x_num, x_size) register x_unsigned_int x_num; register x_unsigned_int x_size; {
	extern char *x_malloc();
	register char *x_p;

	x_size *= x_num;
	if (x_p = x_malloc(x_size))
		x_bzero(x_p, x_size);
	return (x_p);
}

x_int x_cfree(x_p, x_num, x_size) char *x_p; x_unsigned_int x_num; x_unsigned_int x_size; {
	x_free(x_p);
}
