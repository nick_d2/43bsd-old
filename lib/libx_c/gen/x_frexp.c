#include "x_.h"

#include <x_math.h>
#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)frexp.c	5.2 (Berkeley) 3/9/86";
#endif

/*
 *	the call
 *		x = frexp(arg,&exp);
 *	must return a double fp quantity x which is <1.0
 *	and the corresponding binary exponent "exp".
 *	such that
 *		arg = x*2^exp
 *	if the argument is 0.0, return 0.0 mantissa and 0 exponent.
 */

double x_frexp(x_x, x_i) double x_x; x_int *x_i; {
	x_int x_neg;
	x_int x_j;
	x_j = 0;
	x_neg = 0;
	if(x_x<0){
		x_x = -x_x;
		x_neg = 1;
		}
	if(x_x>=1.0)
		while(x_x>=1.0){
			x_j = x_j+1;
			x_x = x_x/2;
			}
	else if(x_x<0.5 && x_x != 0.0)
		while(x_x<0.5){
			x_j = x_j-1;
			x_x = 2*x_x;
			}
	*x_i = x_j;
	if(x_neg) x_x = -x_x;
	return(x_x);
	}
