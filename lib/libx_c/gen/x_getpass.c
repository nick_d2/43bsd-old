#include "x_.h"

#include <x_unistd.h>
#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)getpass.c	5.2 (Berkeley) 3/9/86";
#endif

#include <x_stdio.h>
#include <x_signal.h>
#include <x_sgtty.h>

char *x_getpass(x_prompt) char *x_prompt; {
	struct x_sgttyb x_ttyb;
	x_int x_flags;
	register char *x_p;
	register x_int x_c;
	x_FILE *x_fi;
	static char x_pbuf[9];
	x_int (*x_signal())();
	x_int (*x_sig)();

	if ((x_fi = x_fdopen(x_open("/dev/tty", 2), "r")) == x_NULL)
		x_fi = x_stdin;
	else
		x_setbuf(x_fi, (char *)x_NULL);
	x_sig = x_signal(x_SIGINT, x_SIG_IGN);
	x_ioctl(x_fileno(x_fi), x_TIOCGETP, &x_ttyb);
	x_flags = x_ttyb.x_sg_flags;
	x_ttyb.x_sg_flags &= ~x_ECHO;
	x_ioctl(x_fileno(x_fi), x_TIOCSETP, &x_ttyb);
	x_fprintf(x_stderr, "%s", x_prompt); x_fflush(x_stderr);
	for (x_p=x_pbuf; (x_c = x_getc(x_fi))!='\n' && x_c!=x_EOF;) {
		if (x_p < &x_pbuf[8])
			*x_p++ = x_c;
	}
	*x_p = '\0';
	x_fprintf(x_stderr, "\n"); x_fflush(x_stderr);
	x_ttyb.x_sg_flags = x_flags;
	x_ioctl(x_fileno(x_fi), x_TIOCSETP, &x_ttyb);
	x_signal(x_SIGINT, x_sig);
	if (x_fi != x_stdin)
		x_fclose(x_fi);
	return(x_pbuf);
}
