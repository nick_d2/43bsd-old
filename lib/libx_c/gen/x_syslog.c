#include "x_.h"

#include <x_syslog.h>
#include <sys/x_time.h>
#include <x_stdio.h>
#include <x_unistd.h>
#include <x_signal.h>
#include <x_time.h>
/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)syslog.c	5.9 (Berkeley) 5/7/86";
#endif

/*
 * SYSLOG -- print message on log file
 *
 * This routine looks a lot like printf, except that it
 * outputs to the log file instead of the standard output.
 * Also:
 *	adds a timestamp,
 *	prints the module name in front of the message,
 *	has some other formatting types (or will sometime),
 *	adds a newline on the end of the message.
 *
 * The output of this routine is intended to be read by /etc/syslogd.
 *
 * Author: Eric Allman
 * Modified to use UNIX domain IPC by Ralph Campbell
 */

#include <sys/x_types.h>
#include <sys/x_socket.h>
#include <sys/x_file.h>
#include <sys/x_signal.h>
#include <sys/x_syslog.h>
#include <x_netdb.h>
#include <x_strings.h>

#define	x_MAXLINE	1024			/* max message size */
#define x_NULL	0			/* manifest */

#define x_PRIMASK(x_p)	(1 << ((x_p) & x_LOG_PRIMASK))
#define x_PRIFAC(x_p)	(((x_p) & x_LOG_FACMASK) >> 3)
#define x_IMPORTANT 	x_LOG_ERR

static char	x_logname[] = "/dev/log";
static char	x_ctty[] = "/dev/console";

static x_int	x_LogFile = -1;		/* fd for log */
static x_int	x_LogStat	= 0;		/* status bits, set by openlog() */
static char	*x_LogTag = "syslog";	/* string to tag the entry with */
static x_int	x_LogMask = 0xff;		/* mask of priorities to be logged */
static x_int	x_LogFacility = x_LOG_USER;	/* default facility code */

static struct x_sockaddr x_SyslogAddr;	/* AF_UNIX address of local logger */

extern	x_int x_errno, x_sys_nerr;
extern	char *x_sys_errlist[];

x_int x_syslog(x_pri, x_fmt, x_p0, x_p1, x_p2, x_p3, x_p4) x_int x_pri; char *x_fmt; x_int x_p0; x_int x_p1; x_int x_p2; x_int x_p3; x_int x_p4; {
	char x_buf[x_MAXLINE + 1], x_outline[x_MAXLINE + 1];
	register char *x_b, *x_f, *x_o;
	register x_int x_c;
	x_long x_now;
	x_int x_pid, x_olderrno = x_errno;

	/* see if we should just throw out this message */
	if (x_pri <= 0 || x_PRIFAC(x_pri) >= x_LOG_NFACILITIES || (x_PRIMASK(x_pri) & x_LogMask) == 0)
		return;
	if (x_LogFile < 0)
		x_openlog(x_LogTag, x_LogStat | x_LOG_NDELAY, 0);

	/* set default facility if none specified */
	if ((x_pri & x_LOG_FACMASK) == 0)
		x_pri |= x_LogFacility;

	/* build the message */
	x_o = x_outline;
	x_sprintf(x_o, "<%d>", x_pri);
	x_o += x_strlen(x_o);
	x_time(&x_now);
	x_sprintf(x_o, "%.15s ", x_ctime(&x_now) + 4);
	x_o += x_strlen(x_o);
	if (x_LogTag) {
		x_strcpy(x_o, x_LogTag);
		x_o += x_strlen(x_o);
	}
	if (x_LogStat & x_LOG_PID) {
		x_sprintf(x_o, "[%d]", x_getpid());
		x_o += x_strlen(x_o);
	}
	if (x_LogTag) {
		x_strcpy(x_o, ": ");
		x_o += 2;
	}

	x_b = x_buf;
	x_f = x_fmt;
	while ((x_c = *x_f++) != '\0' && x_c != '\n' && x_b < &x_buf[x_MAXLINE]) {
		if (x_c != '%') {
			*x_b++ = x_c;
			continue;
		}
		if ((x_c = *x_f++) != 'm') {
			*x_b++ = '%';
			*x_b++ = x_c;
			continue;
		}
		if ((unsigned)x_olderrno > x_sys_nerr)
			x_sprintf(x_b, "error %d", x_olderrno);
		else
			x_strcpy(x_b, x_sys_errlist[x_olderrno]);
		x_b += x_strlen(x_b);
	}
	*x_b++ = '\n';
	*x_b = '\0';
	x_sprintf(x_o, x_buf, x_p0, x_p1, x_p2, x_p3, x_p4);
	x_c = x_strlen(x_outline);
	if (x_c > x_MAXLINE)
		x_c = x_MAXLINE;

	/* output the message to the local logger */
	if (x_sendto(x_LogFile, x_outline, x_c, 0, &x_SyslogAddr, sizeof x_SyslogAddr) >= 0)
		return;
	if (!(x_LogStat & x_LOG_CONS))
		return;

	/* output the message to the console */
	x_pid = x_vfork();
	if (x_pid == -1)
		return;
	if (x_pid == 0) {
		x_int x_fd;

		x_signal(x_SIGALRM, x_SIG_DFL);
		x_sigsetmask(x_sigblock(0) & ~x_sigmask(x_SIGALRM));
		x_alarm(5);
		x_fd = x_open(x_ctty, x_O_WRONLY);
		x_alarm(0);
		x_strcat(x_o, "\r");
		x_o = x_index(x_outline, '>') + 1;
		x_write(x_fd, x_o, x_c + 1 - (x_o - x_outline));
		x_close(x_fd);
		x__exit(0);
	}
	if (!(x_LogStat & x_LOG_NOWAIT))
		while ((x_c = x_wait((x_int *)0)) > 0 && x_c != x_pid)
			;
}

/*
 * OPENLOG -- open system log
 */

x_int x_openlog(x_ident, x_logstat, x_logfac) char *x_ident; x_int x_logstat; x_int x_logfac; {
	if (x_ident != x_NULL)
		x_LogTag = x_ident;
	x_LogStat = x_logstat;
	if (x_logfac != 0)
		x_LogFacility = x_logfac & x_LOG_FACMASK;
	if (x_LogFile >= 0)
		return;
	x_SyslogAddr.x_sa_family = x_AF_UNIX;
	x_strncpy(x_SyslogAddr.x_sa_data, x_logname, sizeof x_SyslogAddr.x_sa_data);
	if (x_LogStat & x_LOG_NDELAY) {
		x_LogFile = x_socket(x_AF_UNIX, x_SOCK_DGRAM, 0);
		x_fcntl(x_LogFile, x_F_SETFD, 1);
	}
}

/*
 * CLOSELOG -- close the system log
 */

x_int x_closelog() {

	(void) x_close(x_LogFile);
	x_LogFile = -1;
}

/*
 * SETLOGMASK -- set the log mask level
 */
x_int x_setlogmask(x_pmask) x_int x_pmask; {
	x_int x_omask;

	x_omask = x_LogMask;
	if (x_pmask != 0)
		x_LogMask = x_pmask;
	return (x_omask);
}
