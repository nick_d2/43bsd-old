#include "x_.h"

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)getgrgid.c	5.2 (Berkeley) 3/9/86";
#endif

#include <x_grp.h>

struct x_group *x_getgrgid(x_gid) register x_int x_gid; {
	register struct x_group *x_p;
	struct x_group *x_getgrent();

	x_setgrent();
	while( (x_p = x_getgrent()) && x_p->x_gr_gid != x_gid );
	x_endgrent();
	return(x_p);
}
