#include "x_.h"

#include <x_stdlib.h>
#include <x_mp.h>
#include <x_unistd.h>
/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)valloc.c	5.2 (Berkeley) 3/9/86";
#endif

char	*x_malloc();

char *x_valloc(x_i) x_int x_i; {
	x_int x_valsiz = x_getpagesize(), x_j;
	char *x_cp = x_malloc(x_i + (x_valsiz-1));

	x_j = ((x_int)x_cp + (x_valsiz-1)) &~ (x_valsiz-1);
	return ((char *)x_j);
}
