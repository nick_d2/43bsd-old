#include "x_.h"

#include <x_strings.h>
#include <x_unistd.h>
#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)ttyname.c	5.2 (Berkeley) 3/9/86";
#endif

/*
 * ttyname(f): return "/dev/ttyXX" which the the name of the
 * tty belonging to file f.
 *  NULL if it is not a tty
 */

#define	x_NULL	0
#include <sys/x_param.h>
#include <sys/x_dir.h>
#include <sys/x_stat.h>

static	char	x_dev[]	= "/dev/";
char	*x_strcpy();
char	*x_strcat();

char *x_ttyname(x_f) x_int x_f; {
	struct x_stat x_fsb;
	struct x_stat x_tsb;
	register struct x_direct *x_db;
	register x_DIR *x_df;
	static char x_rbuf[32];

	if (x_isatty(x_f)==0)
		return(x_NULL);
	if (x_fstat(x_f, &x_fsb) < 0)
		return(x_NULL);
	if ((x_fsb.x_st_mode&x_S_IFMT) != x_S_IFCHR)
		return(x_NULL);
	if ((x_df = x_opendir(x_dev)) == x_NULL)
		return(x_NULL);
	while ((x_db = x_readdir(x_df)) != x_NULL) {
		if (x_db->x_d_ino != x_fsb.x_st_ino)
			continue;
		x_strcpy(x_rbuf, x_dev);
		x_strcat(x_rbuf, x_db->x_d_name);
		if (x_stat(x_rbuf, &x_tsb) < 0)
			continue;
		if (x_tsb.x_st_dev == x_fsb.x_st_dev && x_tsb.x_st_ino == x_fsb.x_st_ino) {
			x_closedir(x_df);
			return(x_rbuf);
		}
	}
	x_closedir(x_df);
	return(x_NULL);
}
