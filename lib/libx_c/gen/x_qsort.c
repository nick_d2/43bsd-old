#include "x_.h"

#include <x_stdlib.h>
/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)qsort.c	5.2 (Berkeley) 3/9/86";
#endif

/*
 * qsort.c:
 * Our own version of the system qsort routine which is faster by an average
 * of 25%, with lows and highs of 10% and 50%.
 * The THRESHold below is the insertion sort threshold, and has been adjusted
 * for records of size 48 bytes.
 * The MTHREShold is where we stop finding a better median.
 */

#define		x_THRESH		4		/* threshold for insertion */
#define		x_MTHRESH		6		/* threshold for median */

static  x_int		(*x_qcmp)();		/* the comparison routine */
static  x_int		x_qsz;			/* size of each record */
static  x_int		x_thresh;			/* THRESHold in chars */
static  x_int		x_mthresh;		/* MTHRESHold in chars */

/*
 * qsort:
 * First, set up some global parameters for qst to share.  Then, quicksort
 * with qst(), and then a cleanup insertion sort ourselves.  Sound simple?
 * It's not...
 */

#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

static x_int x_qst __P((char *x_base, char *x_max));

x_int x_qsort(x_base, x_n, x_size, x_compar) char *x_base; x_int x_n; x_int x_size; x_int (*x_compar)(); {
	register char x_c, *x_i, *x_j, *x_lo, *x_hi;
	char *x_min, *x_max;

	if (x_n <= 1)
		return;
	x_qsz = x_size;
	x_qcmp = x_compar;
	x_thresh = x_qsz * x_THRESH;
	x_mthresh = x_qsz * x_MTHRESH;
	x_max = x_base + x_n * x_qsz;
	if (x_n >= x_THRESH) {
		x_qst(x_base, x_max);
		x_hi = x_base + x_thresh;
	} else {
		x_hi = x_max;
	}
	/*
	 * First put smallest element, which must be in the first THRESH, in
	 * the first position as a sentinel.  This is done just by searching
	 * the first THRESH elements (or the first n if n < THRESH), finding
	 * the min, and swapping it into the first position.
	 */
	for (x_j = x_lo = x_base; (x_lo += x_qsz) < x_hi; )
		if (x_qcmp(x_j, x_lo) > 0)
			x_j = x_lo;
	if (x_j != x_base) {
		/* swap j into place */
		for (x_i = x_base, x_hi = x_base + x_qsz; x_i < x_hi; ) {
			x_c = *x_j;
			*x_j++ = *x_i;
			*x_i++ = x_c;
		}
	}
	/*
	 * With our sentinel in place, we now run the following hyper-fast
	 * insertion sort.  For each remaining element, min, from [1] to [n-1],
	 * set hi to the index of the element AFTER which this one goes.
	 * Then, do the standard insertion sort shift on a character at a time
	 * basis for each element in the frob.
	 */
	for (x_min = x_base; (x_hi = x_min += x_qsz) < x_max; ) {
		while (x_qcmp(x_hi -= x_qsz, x_min) > 0)
			/* void */;
		if ((x_hi += x_qsz) != x_min) {
			for (x_lo = x_min + x_qsz; --x_lo >= x_min; ) {
				x_c = *x_lo;
				for (x_i = x_j = x_lo; (x_j -= x_qsz) >= x_hi; x_i = x_j)
					*x_i = *x_j;
				*x_i = x_c;
			}
		}
	}
}

/*
 * qst:
 * Do a quicksort
 * First, find the median element, and put that one in the first place as the
 * discriminator.  (This "median" is just the median of the first, last and
 * middle elements).  (Using this median instead of the first element is a big
 * win).  Then, the usual partitioning/swapping, followed by moving the
 * discriminator into the right place.  Then, figure out the sizes of the two
 * partions, do the smaller one recursively and the larger one via a repeat of
 * this code.  Stopping when there are less than THRESH elements in a partition
 * and cleaning up with an insertion sort (in our caller) is a huge win.
 * All data swaps are done in-line, which is space-losing but time-saving.
 * (And there are only three places where this is done).
 */

static x_int x_qst(x_base, x_max) char *x_base; char *x_max; {
	register char x_c, *x_i, *x_j, *x_jj;
	register x_int x_ii;
	char *x_mid, *x_tmp;
	x_int x_lo, x_hi;

	/*
	 * At the top here, lo is the number of characters of elements in the
	 * current partition.  (Which should be max - base).
	 * Find the median of the first, last, and middle element and make
	 * that the middle element.  Set j to largest of first and middle.
	 * If max is larger than that guy, then it's that guy, else compare
	 * max with loser of first and take larger.  Things are set up to
	 * prefer the middle, then the first in case of ties.
	 */
	x_lo = x_max - x_base;		/* number of elements as chars */
	do	{
		x_mid = x_i = x_base + x_qsz * ((x_lo / x_qsz) >> 1);
		if (x_lo >= x_mthresh) {
			x_j = (x_qcmp((x_jj = x_base), x_i) > 0 ? x_jj : x_i);
			if (x_qcmp(x_j, (x_tmp = x_max - x_qsz)) > 0) {
				/* switch to first loser */
				x_j = (x_j == x_jj ? x_i : x_jj);
				if (x_qcmp(x_j, x_tmp) < 0)
					x_j = x_tmp;
			}
			if (x_j != x_i) {
				x_ii = x_qsz;
				do	{
					x_c = *x_i;
					*x_i++ = *x_j;
					*x_j++ = x_c;
				} while (--x_ii);
			}
		}
		/*
		 * Semi-standard quicksort partitioning/swapping
		 */
		for (x_i = x_base, x_j = x_max - x_qsz; ; ) {
			while (x_i < x_mid && x_qcmp(x_i, x_mid) <= 0)
				x_i += x_qsz;
			while (x_j > x_mid) {
				if (x_qcmp(x_mid, x_j) <= 0) {
					x_j -= x_qsz;
					continue;
				}
				x_tmp = x_i + x_qsz;	/* value of i after swap */
				if (x_i == x_mid) {
					/* j <-> mid, new mid is j */
					x_mid = x_jj = x_j;
				} else {
					/* i <-> j */
					x_jj = x_j;
					x_j -= x_qsz;
				}
				goto x_swap;
			}
			if (x_i == x_mid) {
				break;
			} else {
				/* i <-> mid, new mid is i */
				x_jj = x_mid;
				x_tmp = x_mid = x_i;	/* value of i after swap */
				x_j -= x_qsz;
			}
		x_swap:
			x_ii = x_qsz;
			do	{
				x_c = *x_i;
				*x_i++ = *x_jj;
				*x_jj++ = x_c;
			} while (--x_ii);
			x_i = x_tmp;
		}
		/*
		 * Look at sizes of the two partitions, do the smaller
		 * one first by recursion, then do the larger one by
		 * making sure lo is its size, base and max are update
		 * correctly, and branching back.  But only repeat
		 * (recursively or by branching) if the partition is
		 * of at least size THRESH.
		 */
		x_i = (x_j = x_mid) + x_qsz;
		if ((x_lo = x_j - x_base) <= (x_hi = x_max - x_i)) {
			if (x_lo >= x_thresh)
				x_qst(x_base, x_j);
			x_base = x_i;
			x_lo = x_hi;
		} else {
			if (x_hi >= x_thresh)
				x_qst(x_i, x_max);
			x_max = x_j;
		}
	} while (x_lo >= x_thresh);
}
