#include "x_.h"

#include <x_stdlib.h>
#include <x_strings.h>
#include <x_mp.h>
#include <x_unistd.h>
/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)scandir.c	5.2 (Berkeley) 3/9/86";
#endif

/*
 * Scan the directory dirname calling select to make a list of selected
 * directory entries then sort using qsort and compare routine dcomp.
 * Returns the number of entries and a pointer to a list of pointers to
 * struct direct (through namelist). Returns -1 if there were any errors.
 */

#include <sys/x_types.h>
#include <sys/x_stat.h>
#include <sys/x_dir.h>

x_int x_scandir(x_dirname, x_namelist, x_select, x_dcomp) char *x_dirname; struct x_direct *(*x_namelist[]); x_int (*x_select)(); x_int (*x_dcomp)(); {
	register struct x_direct *x_d, *x_p, **x_names;
	register x_int x_nitems;
	register char *x_cp1, *x_cp2;
	struct x_stat x_stb;
	x_long x_arraysz;
	x_DIR *x_dirp;

	if ((x_dirp = x_opendir(x_dirname)) == x_NULL)
		return(-1);
	if (x_fstat(x_dirp->x_dd_fd, &x_stb) < 0)
		return(-1);

	/*
	 * estimate the array size by taking the size of the directory file
	 * and dividing it by a multiple of the minimum size entry. 
	 */
	x_arraysz = (x_stb.x_st_size / 24);
	x_names = (struct x_direct **)x_malloc(x_arraysz * sizeof(struct x_direct *));
	if (x_names == x_NULL)
		return(-1);

	x_nitems = 0;
	while ((x_d = x_readdir(x_dirp)) != x_NULL) {
		if (x_select != x_NULL && !(*x_select)(x_d))
			continue;	/* just selected names */
		/*
		 * Make a minimum size copy of the data
		 */
		x_p = (struct x_direct *)x_malloc(x_DIRSIZ(x_d));
		if (x_p == x_NULL)
			return(-1);
		x_p->x_d_ino = x_d->x_d_ino;
		x_p->x_d_reclen = x_d->x_d_reclen;
		x_p->x_d_namlen = x_d->x_d_namlen;
		for (x_cp1 = x_p->x_d_name, x_cp2 = x_d->x_d_name; *x_cp1++ = *x_cp2++; );
		/*
		 * Check to make sure the array has space left and
		 * realloc the maximum size.
		 */
		if (++x_nitems >= x_arraysz) {
			if (x_fstat(x_dirp->x_dd_fd, &x_stb) < 0)
				return(-1);	/* just might have grown */
			x_arraysz = x_stb.x_st_size / 12;
			x_names = (struct x_direct **)x_realloc((char *)x_names,
				x_arraysz * sizeof(struct x_direct *));
			if (x_names == x_NULL)
				return(-1);
		}
		x_names[x_nitems-1] = x_p;
	}
	x_closedir(x_dirp);
	if (x_nitems && x_dcomp != x_NULL)
		x_qsort(x_names, x_nitems, sizeof(struct x_direct *), x_dcomp);
	*x_namelist = x_names;
	return(x_nitems);
}

/*
 * Alphabetic order comparison routine for those who want it.
 */
x_int x_alphasort(x_d1, x_d2) struct x_direct **x_d1; struct x_direct **x_d2; {
	return(x_strcmp((*x_d1)->x_d_name, (*x_d2)->x_d_name));
}
