#include "x_.h"

/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)ndbm.h	5.1 (Berkeley) 5/30/85
 */

/*
 * Hashed key data base library.
 */
#define x_PBLKSIZ 1024
#define x_DBLKSIZ 4096

typedef struct {
	x_int	x_dbm_dirf;		/* open directory file */
	x_int	x_dbm_pagf;		/* open page file */
	x_int	x_dbm_flags;		/* flags, see below */
	x_long	x_dbm_maxbno;		/* last ``bit'' in dir file */
	x_long	x_dbm_bitno;		/* current bit number */
	x_long	x_dbm_hmask;		/* hash mask */
	x_long	x_dbm_blkptr;		/* current block for dbm_nextkey */
	x_int	x_dbm_keyptr;		/* current key for dbm_nextkey */
	x_long	x_dbm_blkno;		/* current page to read/write */
	x_long	x_dbm_pagbno;		/* current page in pagbuf */
	char	x_dbm_pagbuf[x_PBLKSIZ];	/* page file block buffer */
	x_long	x_dbm_dirbno;		/* current block in dirbuf */
	char	x_dbm_dirbuf[x_DBLKSIZ];	/* directory file block buffer */
} x_DBM;

#define x__DBM_RDONLY	0x1	/* data base open read-only */
#define x__DBM_IOERR	0x2	/* data base I/O error */

#define x_dbm_rdonly(x_db)	((x_db)->x_dbm_flags & x__DBM_RDONLY)

#define x_dbm_error(x_db)	((x_db)->x_dbm_flags & x__DBM_IOERR)
	/* use this one at your own risk! */
#define x_dbm_clearerr(x_db)	((x_db)->x_dbm_flags &= ~x__DBM_IOERR)

/* for flock(2) and fstat(2) */
#define x_dbm_dirfno(x_db)	((x_db)->x_dbm_dirf)
#define x_dbm_pagfno(x_db)	((x_db)->x_dbm_pagf)

typedef struct {
	char	*x_dptr;
	x_int	x_dsize;
} x_datum;

/*
 * flags to dbm_store()
 */
#define x_DBM_INSERT	0
#define x_DBM_REPLACE	1

x_DBM	*x_dbm_open();
void	x_dbm_close();
x_datum	x_dbm_fetch();
x_datum	x_dbm_firstkey();
x_datum	x_dbm_nextkey();
x_long	x_dbm_forder();
x_int	x_dbm_delete();
x_int	x_dbm_store();

#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

/* gen/ndbm.c */
x_DBM *x_dbm_open __P((char *x_file, x_int x_flags, x_int x_mode));
void x_dbm_close __P((x_DBM *x_db));
x_long x_dbm_forder __P((register x_DBM *x_db, x_datum x_key));
x_datum x_dbm_fetch __P((register x_DBM *x_db, x_datum x_key));
x_int x_dbm_delete __P((register x_DBM *x_db, x_datum x_key));
x_int x_dbm_store __P((register x_DBM *x_db, x_datum x_key, x_datum x_dat, x_int x_replace));
x_datum x_dbm_firstkey __P((x_DBM *x_db));
x_datum x_dbm_nextkey __P((register x_DBM *x_db));
