#include "x_.h"

/*	grp.h	4.1	83/05/03	*/

struct	x_group { /* see getgrent(3) */
	char	*x_gr_name;
	char	*x_gr_passwd;
	x_int	x_gr_gid;
	char	**x_gr_mem;
};

struct x_group *x_getgrent(), *x_getgrgid(), *x_getgrnam();

#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

/* gen/getgrent.c */
x_int x_setgrent __P((void));
x_int x_endgrent __P((void));
struct x_group *x_getgrent __P((void));
/* gen/getgrgid.c */
struct x_group *x_getgrgid __P((register x_int x_gid));
/* gen/getgrnam.c */
struct x_group *x_getgrnam __P((register char *x_name));
