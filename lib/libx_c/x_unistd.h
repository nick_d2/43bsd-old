#include "x_.h"

#include <sys/x_resource.h>
#include <sys/x_signal.h>
#include <sys/x_socket.h>
#include <sys/x_stat.h>
#include <sys/x_time.h>
#include <sys/x_types.h>
#include <sys/x_uio.h> 
#include <sys/x_wait.h> 

#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

/* compat-4.1/nice.c */
x_int x_nice __P((x_int x_incr));
/* compat-4.1/pause.c */
x_int x_pause __P((void));
/* compat-4.1/tell.c */
x_long x_tell __P((x_int x_f));
/* compat-4.1/utime.c */
x_int x_utime __P((char *x_name, x_int x_otv[]));
/* vax/syscall.c */
x_int x_accept __P((x_int x_s, struct x_sockaddr *x_a, x_int *x_l));
x_int x_access __P((char *x_p, x_int x_m));
x_int x_acct __P((char *x_f));
x_int x_adjtime __P((struct x_timeval *x_delta, struct x_timeval *x_odelta));
x_int x_bind __P((x_int x_s, struct x_sockaddr *x_n, x_int x_l));
char *x_brk __P((char *x_a));
x_int x_chdir __P((char *x_s));
x_int x_chmod __P((char *x_s, x_int x_m));
x_int x_chown __P((char *x_s, x_int x_u, x_int x_g));
x_int x_chroot __P((char *x_d));
x_int x_close __P((x_int x_f));
x_int x_connect __P((x_int x_s, struct x_sockaddr *x_n, x_int x_l));
x_int x_dup __P((x_int x_f));
x_int x_dup2 __P((x_int x_o, x_int x_n));
void x_execve __P((char *x_s, char *x_v[], char *x_e[]));
void x__exit __P((x_int x_s));
x_int x_fchmod __P((x_int x_f, x_int x_m));
x_int x_fchown __P((x_int x_f, x_int x_u, x_int x_g));
x_int x_fcntl __P((x_int x_f, x_int x_c, x_int x_a));
x_int x_flock __P((x_int x_f, x_int x_o));
x_int x_fork __P((void));
x_int x_fsync __P((x_int x_f));
x_int x_fstat __P((x_int x_f, struct x_stat *x_b));
x_int x_ftruncate __P((x_int x_d, x_off_t x_l));
x_int x_getdtablesize __P((void));
x_gid_t x_getegid __P((void));
x_uid_t x_geteuid __P((void));
x_gid_t x_getgid __P((void));
x_int x_getgroups __P((x_int x_n, x_int *x_g));
x_long x_gethostid __P((void));
x_int x_gethostname __P((char *x_n, x_int x_l));
x_int x_getitimer __P((x_int x_w, struct x_itimerval *x_v));
x_int x_getpagesize __P((void));
x_int x_getpeername __P((x_int x_s, struct x_sockaddr *x_n, x_int *x_l));
x_int x_getpgrp __P((x_int x_p));
x_int x_getpid __P((void));
x_int x_getppid __P((void));
x_int x_getpriority __P((x_int x_w, x_int x_who));
x_int x_getrlimit __P((x_int x_res, struct x_rlimit *x_rip));
x_int x_getrusage __P((x_int x_res, struct x_rusage *x_rip));
x_int x_getsockname __P((x_int x_s, char *x_name, x_int *x_namelen));
x_int x_getsockopt __P((x_int x_s, x_int x_level, x_int x_opt, char *x_buf, x_int *x_len));
x_int x_gettimeofday __P((struct x_timeval *x_t, struct x_timezone *x_z));
x_uid_t x_getuid __P((void));
x_int x_ioctl __P((x_int x_d, x_u_long x_r, char *x_p));
x_int x_kill __P((x_int x_p, x_int x_s));
x_int x_killpg __P((x_int x_pg, x_int x_s));
x_int x_link __P((char *x_a, char *x_b));
x_int x_listen __P((x_int x_s, x_int x_b));
x_off_t x_lseek __P((x_int x_f, x_off_t x_o, x_int x_d));
x_int x_lstat __P((char *x_s, struct x_stat *x_b));
x_int x_madvise __P((char *x_a, x_int x_l, x_int x_b));
x_int x_mmap __P((char *x_a, x_int x_l, x_int x_p, x_int x_s, x_int x_f, x_off_t x_o));
x_int x_mincore __P((char *x_a, x_int x_l, char *x_v));
x_int x_mkdir __P((char *x_p, x_int x_m));
x_int x_mknod __P((char *x_n, x_int x_m, x_int x_a));
x_int x_mount __P((char *x_s, char *x_n, x_int x_f));
x_int x_mprotect __P((char *x_a, x_int x_l, x_int x_p));
x_int x_mremap __P((char *x_a, x_int x_l, x_int x_p, x_int x_s, x_int x_f));
x_int x_munmap __P((char *x_a, x_int x_l));
x_int x_pipe __P((x_int x_f[2]));
void x_profil __P((char *x_b, x_int x_s, x_int x_o, x_int x_i));
x_int x_ptrace __P((x_int x_r, x_int x_p, x_int *x_a, x_int x_d));
x_int x_quota __P((x_int x_c, x_int x_u, x_int x_a, char *x_ad));
x_int x_read __P((x_int x_f, char *x_b, x_int x_l));
x_int x_readv __P((x_int x_d, struct x_iovec *x_v, x_int x_l));
x_int x_readlink __P((char *x_p, char *x_b, x_int x_s));
void x_reboot __P((x_int x_h));
x_int x_recv __P((x_int x_s, char *x_b, x_int x_l, x_int x_f));
x_int x_recvfrom __P((x_int x_s, char *x_b, x_int x_l, x_int x_f, struct x_sockaddr *x_fr, x_int *x_fl));
x_int x_recvmsg __P((x_int x_s, struct x_msghdr x_m[], x_int x_f));
x_int x_rename __P((char *x_f, char *x_t));
x_int x_rmdir __P((char *x_p));
char *x_sbrk __P((x_int x_i));
x_int x_select __P((x_int x_n, x_fd_set *x_r, x_fd_set *x_w, x_fd_set *x_e, struct x_timeval *x_t));
x_int x_send __P((x_int x_s, char *x_m, x_int x_l, x_int x_f));
x_int x_sendto __P((x_int x_s, char *x_m, x_int x_l, x_int x_f, struct x_sockaddr *x_t, x_int x_tl));
x_int x_sendmsg __P((x_int x_s, struct x_msghdr x_m[], x_int x_l));
x_int x_setgroups __P((x_int x_n, x_int *x_g));
x_int x_sethostid __P((x_long x_h));
x_int x_sethostname __P((char *x_n, x_int x_l));
x_int x_setitimer __P((x_int x_w, struct x_itimerval *x_v, struct x_itimerval *x_ov));
x_int x_setpgrp __P((x_int x_g, x_int x_pg));
x_int x_setpriority __P((x_int x_w, x_int x_who, x_int x_pri));
x_int x_setquota __P((char *x_s, char *x_f));
x_int x_setregid __P((x_int x_r, x_int x_e));
x_int x_setreuid __P((x_int x_r, x_int x_e));
x_int x_setrlimit __P((x_int x_res, struct x_rlimit *x_rip));
x_int x_setsockopt __P((x_int x_s, x_int x_level, x_int x_opt, char *x_buf, x_int x_len));
x_int x_settimeofday __P((struct x_timeval *x_t, struct x_timezone *x_z));
x_int x_shutdown __P((x_int x_s, x_int x_h));
x_int (*x_signal __P((x_int x_c, x_int (*x_f)(void)))) __P((void));
x_int x_sigvec __P((x_int x_c, struct x_sigvec *x_f, struct x_sigvec *x_m));
x_int x_sigblock __P((x_int x_m));
x_int x_sigsetmask __P((x_int x_m));
void x_sigpause __P((x_int x_m));
x_int x_sigreturn __P((struct x_sigcontext *x_scp));
x_int x_sigstack __P((struct x_sigstack *x_ss, struct x_sigstack *x_oss));
x_int x_socket __P((x_int x_a, x_int x_t, x_int x_p));
x_int x_socketpair __P((x_int x_d, x_int x_t, x_int x_p, x_int x_s[2]));
x_int x_stat __P((char *x_s, struct x_stat *x_b));
char *x_stk __P((char *x_a));
char *x_sstk __P((x_int x_a));
x_int x_swapon __P((char *x_s));
x_int x_symlink __P((char *x_t, char *x_f));
void x_sync __P((void));
x_int x_truncate __P((char *x_p, x_off_t x_l));
x_int x_umask __P((x_int x_n));
x_int x_umount __P((char *x_s));
x_int x_unlink __P((char *x_s));
x_int x_utimes __P((char *x_f, struct x_timeval x_t[2]));
x_int x_vfork __P((void));
void x_vhangup __P((void));
x_int x_wait __P((union x_wait *x_s));
x_int x_wait3 __P((union x_wait *x_s, x_int x_o, struct x_rusage *x_r));
x_int x_write __P((x_int x_f, char *x_b, x_int x_l));
x_int x_writev __P((x_int x_f, struct x_iovec *x_v, x_int x_l));
/* gen/alarm.c */
x_int x_alarm __P((x_int x_secs));
/* gen/ttyslot.c */
x_int x_ttyslot __P((void));
/* gen/usleep.c */
x_int x_usleep __P((x_unsigned_int x_n));
/* gen/ualarm.c */
x_unsigned_int x_ualarm __P((register x_unsigned_int x_usecs, register x_unsigned_int x_reload));
/* gen/seteuid.c */
x_int x_seteuid __P((x_int x_euid));
/* gen/setgid.c */
x_int x_setgid __P((x_int x_gid));
/* gen/setuid.c */
x_int x_setuid __P((x_int x_uid));
/* gen/isatty.c */
x_int x_isatty __P((x_int x_f));
/* gen/sleep.c */
x_int x_sleep __P((x_unsigned_int x_n));
/* gen/getpass.c */
char *x_getpass __P((char *x_prompt));
/* gen/ttyname.c */
char *x_ttyname __P((x_int x_f));
/* gen/setrgid.c */
x_int x_setrgid __P((x_int x_rgid));
/* gen/setegid.c */
x_int x_setegid __P((x_int x_egid));
/* gen/swab.c */
x_int x_swab __P((register char *x_from, register char *x_to, register x_int x_n));
/* gen/setruid.c */
x_int x_setruid __P((x_int x_ruid));
/* gen/getusershell.c */
char *x_getusershell __P((void));
x_int x_endusershell __P((void));
x_int x_setusershell __P((void));
/* gen/getlogin.c */
char *x_getlogin __P((void));
/* gen/execvp.c */
x_int x_execlp __P((char *x_name, char *x_argv));
x_int x_execvp __P((char *x_name, char **x_argv));
/* gen/getwd.c */
char *x_getwd __P((char *x_pathname));
