#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)if_imp.h	7.1 (Berkeley) 6/4/86
 */

/*
 * Structure of IMP 1822 long leader.
 */
struct x_control_leader {
	x_u_char	x_dl_format;	/* leader format */
	x_u_char	x_dl_network;	/* src/dest network */
	x_u_char	x_dl_flags;	/* leader flags */
	x_u_char	x_dl_mtype;	/* message type */
	x_u_char	x_dl_htype;	/* handling type */
	x_u_char	x_dl_host;	/* host number */
	x_u_short	x_dl_imp;		/* imp field */
	x_u_char	x_dl_link;	/* link number */
	x_u_char	x_dl_subtype;	/* message subtype */
};

struct x_imp_leader {
	struct	x_control_leader x_il_dl;
#define	x_il_format	x_il_dl.x_dl_format
#define	x_il_network	x_il_dl.x_dl_network
#define	x_il_flags	x_il_dl.x_dl_flags
#define	x_il_mtype	x_il_dl.x_dl_mtype
#define	x_il_htype	x_il_dl.x_dl_htype
#define	x_il_host		x_il_dl.x_dl_host
#define	x_il_imp		x_il_dl.x_dl_imp
#define	x_il_link		x_il_dl.x_dl_link
#define	x_il_subtype	x_il_dl.x_dl_subtype
	x_u_short	x_il_length;	/* message length */
};

#define	x_IMP_DROPCNT	2	/* # of noops from imp to ignore */
/* insure things are even... */
#define	x_IMPMTU		((8159 / x_NBBY) & ~01)

/*
 * IMP-host flags
 */
#define	x_IMP_NFF		0xf	/* 96-bit (new) format */
#define	x_IMP_TRACE	0x8	/* trace message route */

#define	x_IMP_DMASK	0x3	/* host going down mask */

/*
 * IMP-host message types.
 */
#define	x_IMPTYPE_DATA		0	/* data for protocol */
#define	x_IMPTYPE_BADLEADER	1	/* leader error */
#define	x_IMPTYPE_DOWN		2	/* imp going down */
#define	x_IMPTYPE_NOOP		4	/* noop seen during initialization */
#define	x_IMPTYPE_RFNM		5	/* request for new messages */
#define	x_IMPTYPE_HOSTDEAD	6	/* host doesn't respond */
#define	x_IMPTYPE_HOSTUNREACH	7	/* host unreachable */
#define	x_IMPTYPE_BADDATA		8	/* data error */
#define	x_IMPTYPE_INCOMPLETE	9	/* incomplete message, send rest */
#define	x_IMPTYPE_RESET		10	/* reset complete */
/* non-blocking IMP interface */
#define	x_IMPTYPE_RETRY		11	/* IMP refused, try again */
#define	x_IMPTYPE_NOTIFY		12	/* IMP refused, will notify */
#define	x_IMPTYPE_TRYING		13	/* IMP refused, still rexmt'ng */
#define	x_IMPTYPE_READY		14	/* ready for next message */

/*
 * IMPTYPE_DOWN subtypes.
 */
#define	x_IMPDOWN_GOING		0	/* 30 secs */
#define	x_IMPDOWN_PM		1	/* hardware PM */
#define	x_IMPDOWN_RELOAD		2	/* software reload */
#define	x_IMPDOWN_RESTART		3	/* emergency restart */

/*
 * IMPTYPE_BADLEADER subtypes.
 */
#define	x_IMPLEADER_ERR		0	/* error flip-flop set */
#define	x_IMPLEADER_SHORT		1	/* leader < 80 bits */
#define	x_IMPLEADER_TYPE		2	/* illegal type field */
#define	x_IMPLEADER_OPPOSITE	3	/* opposite leader type */

/*
 * IMPTYPE_HOSTDEAD subtypes.
 */
#define	x_IMPHOST_NORDY		1	/* ready-line negated */
#define	x_IMPHOST_TARDY		2	/* tardy receiving mesgs */
#define	x_IMPHOST_NOEXIST		3	/* NCC doesn't know host */
#define	x_IMPHOST_IMPSOFT		4	/* IMP software won't allow mesgs */
#define	x_IMPHOST_PM		5	/* host down for scheduled PM */
#define	x_IMPHOST_HARDSCHED	6	/* " " " " hardware work */
#define	x_IMPHOST_SOFTSCHED	7	/* " " " " software work */
#define	x_IMPHOST_RESTART		8	/* host down for emergency restart */
#define	x_IMPHOST_POWER		9	/* down because of power outage */
#define	x_IMPHOST_BREAKPOINT	10	/* host stopped at a breakpoint */
#define	x_IMPHOST_HARDWARE	11	/* hardware failure */
#define	x_IMPHOST_NOTUP		12	/* host not scheduled to be up */
/* 13-14 currently unused */
#define	x_IMPHOST_COMINGUP	15	/* host in process of coming up */

/*
 * IMPTYPE_HOSTUNREACH subtypes.
 */
#define	x_IMPREACH_IMP		0	/* destination IMP can't be reached */
#define	x_IMPREACH_HOSTUP		1	/* destination host isn't up */
#define	x_IMPREACH_LEADER		2	/* host doesn't support long leader */
#define	x_IMPREACH_PROHIBITED	3	/* communication is prohibited */

/*
 * IMPTYPE_INCOMPLETE subtypes.
 */
#define	x_IMPCOMPLETE_SLOW	0	/* host didn't take data fast enough */
#define	x_IMPCOMPLETE_TOOLONG	1	/* message was too long */
#define	x_IMPCOMPLETE_TIMEOUT	2	/* mesg transmission time > 15 sec. */
#define	x_IMPCOMPLETE_FAILURE	3	/* IMP/circuit failure */
#define	x_IMPCOMPLETE_NOSPACE	4	/* no resources within 15 sec. */
#define	x_IMPCOMPLETE_IMPIO	5	/* src IMP I/O failure during receipt */

/*
 * IMPTYPE_RETRY subtypes.
 */
#define	x_IMPRETRY_BUFFER		0	/* IMP buffer wasn't available */
#define	x_IMPRETRY_BLOCK		1	/* connection block unavailable */

/*
 * Data structure shared between IMP protocol module and hardware
 * interface driver.  Used to allow layering of IMP routines on top
 * of varying device drivers.  NOTE: there's a possible problem 
 * with ambiguity in the ``unit'' definition which is implicitly
 * shared by the both IMP and device code.  If we have two IMPs,
 * with each on top of a device of the same unit, things won't work.
 * The assumption is if you've got multiple IMPs, then they all run
 * on top of the same type of device, or they must have different units.
 */
struct x_impcb {
	char	x_ic_oactive;		/* output in progress */
	x_int	(*x_ic_init)();		/* hardware init routine */
	x_int	(*x_ic_start)();		/* hardware start output routine */
};

/*
 * State of an IMP.
 */
#define	x_IMPS_DOWN	0		/* unavailable, don't use */
#define	x_IMPS_GOINGDOWN	1		/* been told we go down soon */
#define	x_IMPS_INIT	2		/* coming up */
#define	x_IMPS_UP		3		/* ready to go */
#define	x_IMPS_RESET	4		/* reset in progress */

#define	x_IMPTV_DOWN	(30*60)		/* going down timer 30 secs */

#ifdef x_IMPLEADERS
char *x_impleaders[x_IMPTYPE_READY+1] = {
	"DATA", "BADLEADER", "DOWN", "bad", "NOOP", "RFNM", "HOSTDEAD",
	"HOSTUNREACH", "BADDATA", "INCOMPLETE", "RESET", "RETRY",
	"NOTIFY", "TRYING", "READY"
};
#endif
