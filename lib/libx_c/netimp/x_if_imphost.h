#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)if_imphost.h	7.1 (Berkeley) 6/4/86
 */

/*
 * Host structure used with IMP's.
 * Used to hold outgoing packets which
 * would exceed allowed RFNM count.
 *
 * These structures are packed into
 * mbuf's and kept as small as possible.
 */
struct x_host {
	struct	x_mbuf *x_h_q;		/* holding queue */
	struct	x_in_addr x_h_addr;		/* host's address */
	x_u_char	x_h_qcnt;          	/* size of holding q */
	x_u_char	x_h_timer;		/* used to stay off deletion */
	x_u_char	x_h_rfnm;			/* # outstanding rfnm's */
	x_u_char	x_h_flags;		/* see below */
};

/*
 * A host structure is kept around (even when there are no
 * references to it) for a spell to avoid constant reallocation
 * and also to reflect IMP status back to sites which aren't
 * directly connected to the IMP.  When structures are marked
 * free, a timer is started; when the timer expires the structure
 * is scavenged.
 */
#define	x_HF_INUSE	0x1
#define	x_HF_DEAD		(1<<x_IMPTYPE_HOSTDEAD)
#define	x_HF_UNREACH	(1<<x_IMPTYPE_HOSTUNREACH)

#define	x_HOSTTIMER	128		/* keep structure around awhile */

/*
 * Host structures, as seen inside an mbuf.
 * Hashing on the host address is used to
 * select an index into the first mbuf.  Collisions
 * are then resolved by searching successive
 * mbuf's at the same index.  Reclamation is done
 * automatically at the time a structure is free'd.
 */
#define	x_HPMBUF	((x_MLEN - sizeof(x_int)) / sizeof(struct x_host))
#if x_vax
#define	x_HOSTHASH(x_a)	((((x_a).x_s_addr>>24)+(x_a).x_s_addr) % x_HPMBUF)
#endif

/*
 * In-line expansions for queuing operations on
 * host message holding queue.  Queue is maintained
 * as circular list with the head pointing to the
 * last message in the queue.
 */
#define	x_HOST_ENQUE(x_hp, x_m) { \
	register struct x_mbuf *x_n; \
	(x_hp)->x_h_qcnt++; \
	if ((x_n = (x_hp)->x_h_q) == 0) \
		(x_hp)->x_h_q = (x_m)->x_m_act = (x_m); \
	else { \
		(x_m)->x_m_act = x_n->x_m_act; \
		(x_hp)->x_h_q = x_n->x_m_act = (x_m); \
	} \
}
#define	x_HOST_DEQUE(x_hp, x_m) { \
	if ((x_m) = (x_hp)->x_h_q) { \
		if ((x_m)->x_m_act == (x_m)) \
			(x_hp)->x_h_q = 0; \
		else { \
			(x_m) = (x_m)->x_m_act; \
			(x_hp)->x_h_q->x_m_act = (x_m)->x_m_act; \
		} \
		(x_hp)->x_h_qcnt--; \
		(x_m)->x_m_act = 0; \
	} \
}

struct x_hmbuf {
	x_int	x_hm_count;		/* # of struct's in use */
	struct	x_host x_hm_hosts[x_HPMBUF];	/* data structures proper */
};

#ifdef x_KERNEL
struct x_host *x_hostlookup();
struct x_host *x_hostenter();
struct x_mbuf *x_hostdeque();
#endif
