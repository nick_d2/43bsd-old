#include "x_.h"

/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)strings.h	5.1 (Berkeley) 5/30/85
 */

/*
 * External function definitions
 * for routines described in string(3).
 */
char	*x_strcat();
char	*x_strncat();
x_int	x_strcmp();
x_int	x_strncmp();
char	*x_strcpy();
char	*x_strncpy();
x_int	x_strlen();
char	*x_index();
char	*x_rindex();

#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

/* gen/index.c */
char *x_index __P((register char *x_sp, x_int x_c));
/* gen/rindex.c */
char *x_rindex __P((register char *x_sp, x_int x_c));
/* gen/strcmp.c */
x_int x_strcmp __P((register char *x_s1, register char *x_s2));
/* gen/strcpy.c */
char *x_strcpy __P((register char *x_s1, register char *x_s2));
/* gen/strlen.c */
x_int x_strlen __P((register char *x_s));
/* gen/strcat.c */
char *x_strcat __P((register char *x_s1, register char *x_s2));
/* gen/strncat.c */
char *x_strncat __P((register char *x_s1, register char *x_s2, register x_int x_n));
/* gen/strncmp.c */
x_int x_strncmp __P((register char *x_s1, register char *x_s2, register x_int x_n));
/* gen/strncpy.c */
char *x_strncpy __P((register char *x_s1, register char *x_s2, x_int x_n));
