#include "x_.h"


/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)resolv.h	5.4 (Berkeley) 2/22/86
 */

/*
 * Global defines and variables for resolver stub.
 */


#define	x_MAXNS		3		/* max # name servers we'll track */


struct x_state {
	x_int	x_retrans;	 	/* retransmition time interval */
	x_int	x_retry;			/* number of times to retransmit */
	x_long	x_options;		/* option flags - see below. */
	x_int	x_nscount;		/* number of name servers */
	struct	x_sockaddr_in x_nsaddr_list[x_MAXNS];	/* address of name server */
#define	x_nsaddr	x_nsaddr_list[0]		/* for backward compatibility */
	x_u_short	x_id;			/* current packet id */
	char	x_defdname[x_MAXDNAME];	/* default domain */
};

/*
 * Resolver options
 */
#define x_RES_INIT	0x0001		/* address initialized */
#define x_RES_DEBUG	0x0002		/* print debug messages */
#define x_RES_AAONLY	0x0004		/* authoritative answers only */
#define x_RES_USEVC	0x0008		/* use virtual circuit */
#define x_RES_PRIMARY	0x0010		/* query primary server only */
#define x_RES_IGNTC	0x0020		/* ignore trucation errors */
#define x_RES_RECURSE	0x0040		/* recursion desired */
#define x_RES_DEFNAMES	0x0080		/* use default domain name */
#define x_RES_STAYOPEN	0x0100		/* Keep TCP socket open */

extern struct x_state x__res;
extern char *x_p_cdname(), *x_p_rr(), *x_p_type(), *x_p_class();

#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

/* net/res_debug.c */
x_int x_p_query __P((char *x_msg));
x_int x_fp_query __P((char *x_msg, x_FILE *x_file));
char *x_p_cdname __P((char *x_cp, char *x_msg, x_FILE *x_file));
char *x_p_rr __P((char *x_cp, char *x_msg, x_FILE *x_file));
char *x_p_type __P((x_int x_type));
char *x_p_class __P((x_int x_class));
