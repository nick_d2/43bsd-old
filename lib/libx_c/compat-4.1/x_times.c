#include "x_.h"

#include <sys/x_times.h>
#include <x_unistd.h>
/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)times.c	5.2 (Berkeley) 3/9/86";
#endif

#include <sys/x_time.h>
#include <sys/x_resource.h>

/*
 * Backwards compatible times.
 */
struct x_tms {
	x_int	x_tms_utime;		/* user time */
	x_int	x_tms_stime;		/* system time */
	x_int	x_tms_cutime;		/* user time, children */
	x_int	x_tms_cstime;		/* system time, children */
};

#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

static x_int x_scale60 __P((register struct x_timeval *x_tvp));

x_int x_times(x_tmsp) register struct x_tms *x_tmsp; {
	struct x_rusage x_ru;

	if (x_getrusage(x_RUSAGE_SELF, &x_ru) < 0)
		return (-1);
	x_tmsp->x_tms_utime = x_scale60(&x_ru.x_ru_utime);
	x_tmsp->x_tms_stime = x_scale60(&x_ru.x_ru_stime);
	if (x_getrusage(x_RUSAGE_CHILDREN, &x_ru) < 0)
		return (-1);
	x_tmsp->x_tms_cutime = x_scale60(&x_ru.x_ru_utime);
	x_tmsp->x_tms_cstime = x_scale60(&x_ru.x_ru_stime);
	return (0);
}

static x_int x_scale60(x_tvp) register struct x_timeval *x_tvp; {

	return (x_tvp->x_tv_sec * 60 + x_tvp->x_tv_usec / 16667);
}
