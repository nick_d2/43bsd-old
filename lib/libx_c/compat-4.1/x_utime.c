#include "x_.h"

#include <x_unistd.h>
/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)utime.c	5.2 (Berkeley) 3/9/86";
#endif

#include <sys/x_time.h>
/*
 * Backwards compatible utime.
 */

x_int x_utime(x_name, x_otv) char *x_name; x_int x_otv[]; {
	struct x_timeval x_tv[2];

	x_tv[0].x_tv_sec = x_otv[0]; x_tv[0].x_tv_usec = 0;
	x_tv[1].x_tv_sec = x_otv[1]; x_tv[1].x_tv_usec = 0;
	return (x_utimes(x_name, x_tv));
}
