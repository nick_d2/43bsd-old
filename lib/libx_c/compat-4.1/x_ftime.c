#include "x_.h"

#include <sys/x_timeb.h>
#include <x_unistd.h>
/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)ftime.c	5.2 (Berkeley) 3/9/86";
#endif

#include <sys/x_types.h>
#include <sys/x_time.h>

/*
 * Backwards compatible ftime.
 */

/* from old timeb.h */
struct x_timeb {
	x_time_t	x_time;
	x_u_short	x_millitm;
	x_short	x_timezone;
	x_short	x_dstflag;
};

x_int x_ftime(x_tp) register struct x_timeb *x_tp; {
	struct x_timeval x_t;
	struct x_timezone x_tz;

	if (x_gettimeofday(&x_t, &x_tz) < 0)
		return (-1);
	x_tp->x_time = x_t.x_tv_sec;
	x_tp->x_millitm = x_t.x_tv_usec / 1000;
	x_tp->x_timezone = x_tz.x_tz_minuteswest;
	x_tp->x_dstflag = x_tz.x_tz_dsttime;
}
