#include "x_.h"

#include <sys/x_vlimit.h>
#include <x_unistd.h>
/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)vlimit.c	5.2 (Berkeley) 3/9/86";
#endif

/*
 * (Almost) backwards compatible vlimit.
 */
#include <sys/x_time.h>
#include <sys/x_resource.h>
#include <x_errno.h>

/* LIM_NORAISE is not emulated */
#define	x_LIM_NORAISE	0	/* if <> 0, can't raise limits */
#define	x_LIM_CPU		1	/* max secs cpu time */
#define	x_LIM_FSIZE	2	/* max size of file created */
#define	x_LIM_DATA	3	/* max growth of data space */
#define	x_LIM_STACK	4	/* max growth of stack */
#define	x_LIM_CORE	5	/* max size of ``core'' file */
#define	x_LIM_MAXRSS	6	/* max desired data+stack core usage */

#define	x_NLIMITS		6

x_int x_vlimit(x_limit, x_value) x_int x_limit; x_int x_value; {
	struct x_rlimit x_rlim;

	if (x_limit <= 0 || x_limit > x_NLIMITS)
		return (x_EINVAL);
	if (x_value == -1) {
		if (x_getrlimit(x_limit - 1, &x_rlim) < 0)
			return (-1);
		return (x_rlim.x_rlim_cur);
	}
	x_rlim.x_rlim_cur = x_value;
	x_rlim.x_rlim_max = x_RLIM_INFINITY;
	return (x_setrlimit(x_limit - 1, &x_rlim));
}
