#include "x_.h"

#include <x_unistd.h>
/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)pause.c	5.2 (Berkeley) 3/9/86";
#endif

/*
 * Backwards compatible pause.
 */
x_int x_pause() {

	x_sigpause(x_sigblock(0));
}
