#include "x_.h"

#include <x_unistd.h>
/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)tell.c	5.2 (Berkeley) 3/9/86";
#endif

/*
 * return offset in file.
 */

x_long	x_lseek();

x_long x_tell(x_f) x_int x_f; {
	return(x_lseek(x_f, 0L, 1));
}
