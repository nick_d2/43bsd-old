#include "x_.h"

#include <x_pwd.h>
/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)getpw.c	5.2 (Berkeley) 3/9/86";
#endif

#include	<x_stdio.h>

x_int x_getpw(x_uid, x_buf) x_int x_uid; char x_buf[]; {
	static x_FILE *x_pwf;
	register x_int x_n, x_c;
	register char *x_bp;

	if(x_pwf == 0)
		x_pwf = x_fopen("/etc/passwd", "r");
	if(x_pwf == x_NULL)
		return(1);
	x_rewind(x_pwf);

	for (;;) {
		x_bp = x_buf;
		while((x_c=x_getc(x_pwf)) != '\n') {
			if(x_c == x_EOF)
				return(1);
			*x_bp++ = x_c;
		}
		*x_bp++ = '\0';
		x_bp = x_buf;
		x_n = 3;
		while(--x_n)
		while((x_c = *x_bp++) != ':')
			if(x_c == '\n')
				return(1);
		while((x_c = *x_bp++) != ':') {
			if(x_c<'0' || x_c>'9')
				continue;
			x_n = x_n*10+x_c-'0';
		}
		if(x_n == x_uid)
			return(0);
	}
}
