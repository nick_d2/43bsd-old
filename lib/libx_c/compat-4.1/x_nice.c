#include "x_.h"

#include <x_unistd.h>
/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)nice.c	5.2 (Berkeley) 3/9/86";
#endif

#include <sys/x_time.h>
#include <sys/x_resource.h>

/*
 * Backwards compatible nice.
 */
x_int x_nice(x_incr) x_int x_incr; {
	x_int x_prio;
	extern x_int x_errno;

	x_errno = 0;
	x_prio = x_getpriority(x_PRIO_PROCESS, 0);
	if (x_prio == -1 && x_errno)
		return (-1);
	return (x_setpriority(x_PRIO_PROCESS, 0, x_prio + x_incr));
}
