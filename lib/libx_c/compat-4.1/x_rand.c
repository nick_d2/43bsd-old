#include "x_.h"

#include <x_stdlib.h>
/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)rand.c	5.2 (Berkeley) 3/9/86";
#endif

static	x_long	x_randx = 1;

x_int x_srand(x_x) x_unsigned_int x_x; {
	x_randx = x_x;
}

x_int x_rand() {
	return((x_randx = x_randx * 1103515245 + 12345) & 0x7fffffff);
}
