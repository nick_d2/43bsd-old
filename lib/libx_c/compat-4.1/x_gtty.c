#include "x_.h"

#include <x_unistd.h>
/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)gtty.c	5.2 (Berkeley) 3/9/86";
#endif

/*
 * Writearound to old gtty system call.
 */

#include <x_sgtty.h>

x_int x_gtty(x_fd, x_ap) x_int x_fd; struct x_sgttyb *x_ap; {

	return(x_ioctl(x_fd, x_TIOCGETP, x_ap));
}
