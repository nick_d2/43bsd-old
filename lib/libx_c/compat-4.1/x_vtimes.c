#include "x_.h"

#include <sys/x_vtimes.h>
#include <x_unistd.h>
/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)vtimes.c	5.2 (Berkeley) 3/9/86";
#endif

#include <sys/x_time.h>
#include <sys/x_resource.h>

/*
 * Backwards compatible vtimes.
 */
struct x_vtimes {
	x_int	x_vm_utime;		/* user time (60'ths) */
	x_int	x_vm_stime;		/* system time (60'ths) */
	/* divide next two by utime+stime to get averages */
	x_unsigned_int x_vm_idsrss;		/* integral of d+s rss */
	x_unsigned_int x_vm_ixrss;		/* integral of text rss */
	x_int	x_vm_maxrss;		/* maximum rss */
	x_int	x_vm_majflt;		/* major page faults */
	x_int	x_vm_minflt;		/* minor page faults */
	x_int	x_vm_nswap;		/* number of swaps */
	x_int	x_vm_inblk;		/* block reads */
	x_int	x_vm_oublk;		/* block writes */
};

#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

static x_int x_getvtimes __P((register struct x_rusage *x_aru, register struct x_vtimes *x_avt));
static x_int x_scale60 __P((register struct x_timeval *x_tvp));

x_int x_vtimes(x_par, x_chi) register struct x_vtimes *x_par; register struct x_vtimes *x_chi; {
	struct x_rusage x_ru;

	if (x_par) {
		if (x_getrusage(x_RUSAGE_SELF, &x_ru) < 0)
			return (-1);
		x_getvtimes(&x_ru, x_par);
	}
	if (x_chi) {
		if (x_getrusage(x_RUSAGE_CHILDREN, &x_ru) < 0)
			return (-1);
		x_getvtimes(&x_ru, x_chi);
	}
	return (0);
}

static x_int x_getvtimes(x_aru, x_avt) register struct x_rusage *x_aru; register struct x_vtimes *x_avt; {

	x_avt->x_vm_utime = x_scale60(&x_aru->x_ru_utime);
	x_avt->x_vm_stime = x_scale60(&x_aru->x_ru_stime);
	x_avt->x_vm_idsrss = ((x_aru->x_ru_idrss+x_aru->x_ru_isrss) / 100) * 60;
	x_avt->x_vm_ixrss = x_aru->x_ru_ixrss / 100 * 60;
	x_avt->x_vm_maxrss = x_aru->x_ru_maxrss;
	x_avt->x_vm_majflt = x_aru->x_ru_majflt;
	x_avt->x_vm_minflt = x_aru->x_ru_minflt;
	x_avt->x_vm_nswap = x_aru->x_ru_nswap;
	x_avt->x_vm_inblk = x_aru->x_ru_inblock;
	x_avt->x_vm_oublk = x_aru->x_ru_oublock;
}

static x_int x_scale60(x_tvp) register struct x_timeval *x_tvp; {

	return (x_tvp->x_tv_sec * 60 + x_tvp->x_tv_usec / 16667);
}
