#include "x_.h"

/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)lastlog.h	5.1 (Berkeley) 5/30/85
 */

struct x_lastlog {
	x_time_t	x_ll_time;
	char	x_ll_line[8];
	char	x_ll_host[16];		/* same as in utmp */
};
