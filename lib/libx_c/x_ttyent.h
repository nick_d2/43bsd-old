#include "x_.h"

/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)ttyent.h	5.1 (Berkeley) 5/30/85
 */

struct	x_ttyent { /* see getttyent(3) */
	char	*x_ty_name;	/* terminal device name */
	char	*x_ty_getty;	/* command to execute, usually getty */
	char	*x_ty_type;	/* terminal type for termcap (3X) */
	x_int	x_ty_status;	/* status flags (see below for defines) */
	char 	*x_ty_window;	/* command to start up window manager */
	char	*x_ty_comment;	/* usually the location of the terminal */
};

#define x_TTY_ON		0x1	/* enable logins (startup getty) */
#define x_TTY_SECURE	0x2	/* allow root to login */

extern struct x_ttyent *x_getttyent();
extern struct x_ttyent *x_getttynam();

#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

/* gen/getttynam.c */
struct x_ttyent *x_getttynam __P((char *x_tty));
/* gen/getttyent.c */
x_int x_setttyent __P((void));
x_int x_endttyent __P((void));
struct x_ttyent *x_getttyent __P((void));
