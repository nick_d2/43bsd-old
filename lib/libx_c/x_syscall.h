#include "x_.h"

/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)syscall.h	5.4 (Berkeley) 4/3/86
 */

#define	x_SYS_exit	1
#define	x_SYS_fork	2
#define	x_SYS_read	3
#define	x_SYS_write	4
#define	x_SYS_open	5
#define	x_SYS_close	6
				/*  7 is old: wait */
#define	x_SYS_creat	8
#define	x_SYS_link	9
#define	x_SYS_unlink	10
#define	x_SYS_execv	11
#define	x_SYS_chdir	12
				/* 13 is old: time */
#define	x_SYS_mknod	14
#define	x_SYS_chmod	15
#define	x_SYS_chown	16
				/* 17 is old: sbreak */
				/* 18 is old: stat */
#define	x_SYS_lseek	19
#define	x_SYS_getpid	20
#define	x_SYS_mount	21
#define	x_SYS_umount	22
				/* 23 is old: setuid */
#define	x_SYS_getuid	24
				/* 25 is old: stime */
#define	x_SYS_ptrace	26
				/* 27 is old: alarm */
				/* 28 is old: fstat */
				/* 29 is old: pause */
				/* 30 is old: utime */
				/* 31 is old: stty */
				/* 32 is old: gtty */
#define	x_SYS_access	33
				/* 34 is old: nice */
				/* 35 is old: ftime */
#define	x_SYS_sync	36
#define	x_SYS_kill	37
#define	x_SYS_stat	38
				/* 39 is old: setpgrp */
#define	x_SYS_lstat	40
#define	x_SYS_dup		41
#define	x_SYS_pipe	42
				/* 43 is old: times */
#define	x_SYS_profil	44
				/* 45 is unused */
				/* 46 is old: setgid */
#define	x_SYS_getgid	47
				/* 48 is old: sigsys */
				/* 49 is unused */
				/* 50 is unused */
#define	x_SYS_acct	51
				/* 52 is old: phys */
				/* 53 is old: syslock */
#define	x_SYS_ioctl	54
#define	x_SYS_reboot	55
				/* 56 is old: mpxchan */
#define	x_SYS_symlink	57
#define	x_SYS_readlink	58
#define	x_SYS_execve	59
#define	x_SYS_umask	60
#define	x_SYS_chroot	61
#define	x_SYS_fstat	62
				/* 63 is unused */
#define	x_SYS_getpagesize 64
#define	x_SYS_mremap	65
				/* 66 is old: vfork */
				/* 67 is old: vread */
				/* 68 is old: vwrite */
#define	x_SYS_sbrk	69
#define	x_SYS_sstk	70
#define	x_SYS_mmap	71
				/* 72 is old: vadvise */
#define	x_SYS_munmap	73
#define	x_SYS_mprotect	74
#define	x_SYS_madvise	75
#define	x_SYS_vhangup	76
				/* 77 is old: vlimit */
#define	x_SYS_mincore	78
#define	x_SYS_getgroups	79
#define	x_SYS_setgroups	80
#define	x_SYS_getpgrp	81
#define	x_SYS_setpgrp	82
#define	x_SYS_setitimer	83
#define	x_SYS_wait	84
#define	x_SYS_swapon	85
#define	x_SYS_getitimer	86
#define	x_SYS_gethostname	87
#define	x_SYS_sethostname	88
#define	x_SYS_getdtablesize 89
#define	x_SYS_dup2	90
#define	x_SYS_getdopt	91
#define	x_SYS_fcntl	92
#define	x_SYS_select	93
#define	x_SYS_setdopt	94
#define	x_SYS_fsync	95
#define	x_SYS_setpriority	96
#define	x_SYS_socket	97
#define	x_SYS_connect	98
#define	x_SYS_accept	99
#define	x_SYS_getpriority	100
#define	x_SYS_send	101
#define	x_SYS_recv	102
#define	x_SYS_sigreturn	103
#define	x_SYS_bind	104
#define	x_SYS_setsockopt	105
#define	x_SYS_listen	106
				/* 107 was vtimes */
#define	x_SYS_sigvec	108
#define	x_SYS_sigblock	109
#define	x_SYS_sigsetmask	110
#define	x_SYS_sigpause	111
#define	x_SYS_sigstack	112
#define	x_SYS_recvmsg	113
#define	x_SYS_sendmsg	114
				/* 115 is old vtrace */
#define	x_SYS_gettimeofday 116
#define	x_SYS_getrusage	117
#define	x_SYS_getsockopt	118
				/* 119 is old resuba */
#define	x_SYS_readv	120
#define	x_SYS_writev	121
#define	x_SYS_settimeofday 122
#define	x_SYS_fchown	123
#define	x_SYS_fchmod	124
#define	x_SYS_recvfrom	125
#define	x_SYS_setreuid	126
#define	x_SYS_setregid	127
#define	x_SYS_rename	128
#define	x_SYS_truncate	129
#define	x_SYS_ftruncate	130
#define	x_SYS_flock	131
				/* 132 is unused */
#define	x_SYS_sendto	133
#define	x_SYS_shutdown	134
#define	x_SYS_socketpair	135
#define	x_SYS_mkdir	136
#define	x_SYS_rmdir	137
#define	x_SYS_utimes	138
				/* 139 is unused */
#define	x_SYS_adjtime	140
#define	x_SYS_getpeername	141
#define	x_SYS_gethostid	142
#define	x_SYS_sethostid	143
#define	x_SYS_getrlimit	144
#define	x_SYS_setrlimit	145
#define	x_SYS_killpg	146
				/* 147 is unused */
#define	x_SYS_setquota	148
#define	x_SYS_quota	149
#define	x_SYS_getsockname	150
