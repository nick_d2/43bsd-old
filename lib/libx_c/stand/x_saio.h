#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)saio.h	7.1 (Berkeley) 6/5/86
 */

/*
 * Header file for standalone package
 */

/*
 * Io block: includes an
 * inode, cells for the use of seek, etc,
 * and a buffer.
 */
struct	x_iob {
	x_int	x_i_flgs;		/* see F_ below */
	struct	x_inode x_i_ino;	/* inode, if file */
	x_int	x_i_unit;		/* pseudo device unit */
	x_daddr_t	x_i_boff;		/* block offset on device */
	x_daddr_t	x_i_cyloff;	/* cylinder offset on device */
	x_off_t	x_i_offset;	/* seek offset in file */
	x_daddr_t	x_i_bn;		/* 1st block # of next read */
	char	*x_i_ma;		/* memory address of i/o buffer */
	x_int	x_i_cc;		/* character count of transfer */
	x_int	x_i_error;	/* error # return */
	x_int	x_i_errcnt;	/* error count for driver retries */
	x_int	x_i_errblk;	/* block # in error for error reporting */
	char	x_i_buf[x_MAXBSIZE];/* i/o buffer */
	union {
		struct x_fs x_ui_fs;	/* file system super block info */
		char x_dummy[x_SBSIZE];
	} x_i_un;
};
#define x_i_fs x_i_un.x_ui_fs
#define x_NULL 0

#define x_F_READ		0x1	/* file opened for reading */
#define x_F_WRITE		0x2	/* file opened for writing */
#define x_F_ALLOC		0x4	/* buffer allocated */
#define x_F_FILE		0x8	/* file instead of device */
#define x_F_NBSF		0x10	/* no bad sector forwarding */
#define x_F_SSI		0x40	/* set skip sector inhibit */
/* io types */
#define	x_F_RDDATA	0x0100	/* read data */
#define	x_F_WRDATA	0x0200	/* write data */
#define x_F_HDR		0x0400	/* include header on next i/o */
#define x_F_CHECK		0x0800	/* perform check of data read/write */
#define x_F_HCHECK	0x1000	/* perform check of header and data */

#define	x_F_TYPEMASK	0xff00

/*
 * Device switch.
 */
struct x_devsw {
	char	*x_dv_name;
	x_int	(*x_dv_strategy)();
	x_int	(*x_dv_open)();
	x_int	(*x_dv_close)();
	x_int	(*x_dv_ioctl)();
};

struct x_devsw x_devsw[];

/*
 * Drive description table.
 * Returned from SAIODEVDATA call.
 */
struct x_st {
	x_short	x_nsect;		/* # sectors/track */
	x_short	x_ntrak;		/* # tracks/surfaces/heads */
	x_short	x_nspc;		/* # sectors/cylinder */
	x_short	x_ncyl;		/* # cylinders */
	x_short	*x_off;		/* partition offset table (cylinders) */
};

/*
 * Request codes. Must be the same a F_XXX above
 */
#define	x_READ	1
#define	x_WRITE	2

#define	x_NBUFS	4

char	x_b[x_NBUFS][x_MAXBSIZE];
x_daddr_t	x_blknos[x_NBUFS];

#define	x_NFILES	4
struct	x_iob x_iob[x_NFILES];

extern	x_int x_errno;	/* just like unix */

/* error codes */
#define	x_EBADF	1	/* bad file descriptor */
#define	x_EOFFSET	2	/* relative seek not supported */
#define	x_EDEV	3	/* improper device specification on open */
#define	x_ENXIO	4	/* unknown device specified */
#define	x_EUNIT	5	/* improper unit specification */
#define	x_ESRCH	6	/* directory search for file failed */
#define	x_EIO	7	/* generic error */
#define	x_ECMD	10	/* undefined driver command */
#define	x_EBSE	11	/* bad sector error */
#define	x_EWCK	12	/* write check error */
#define	x_EECC	13	/* uncorrectable ecc error */
#define	x_EHER	14	/* hard error */

/* ioctl's -- for disks just now */
#define	x_SAIOHDR		(('d'<<8)|1)	/* next i/o includes header */
#define	x_SAIOCHECK	(('d'<<8)|2)	/* next i/o checks data */
#define	x_SAIOHCHECK	(('d'<<8)|3)	/* next i/o checks header & data */
#define	x_SAIONOBAD	(('d'<<8)|4)	/* inhibit bad sector forwarding */
#define	x_SAIODOBAD	(('d'<<8)|5)	/* enable bad sector forwarding */
#define	x_SAIOECCLIM	(('d'<<8)|6)	/* set limit to ecc correction, bits */
#define	x_SAIORETRIES	(('d'<<8)|7)	/* set retry count for unit */
#define	x_SAIODEVDATA	(('d'<<8)|8)	/* get device data */
#define	x_SAIOSSI		(('d'<<8)|9)	/* set skip sector inhibit */
#define	x_SAIONOSSI	(('d'<<8)|10)	/* inhibit skip sector handling */
#define	x_SAIOSSDEV	(('d'<<8)|11)	/* is device skip sector type? */
#define	x_SAIODEBUG	(('d'<<8)|12)	/* enable/disable debugging */
#define	x_SAIOGBADINFO	(('d'<<8)|13)	/* get bad-sector table */
