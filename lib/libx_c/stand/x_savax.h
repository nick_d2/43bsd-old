#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)savax.h	7.1 (Berkeley) 6/5/86
 */

/*
 * Standalone definitions peculiar to vaxen
 * The mba devices in the standalone system are addressed as 
 *	xx(unit,section)
 * where unit is
 *	8*mbanum+drive
 * The mbadrv macro gives the address of the device registers
 * for the specified unit; the mbamba macro gives the address of the
 * mba registers themselves.
 *
 * The uba devices are also addressed by giving, as unit,
 *	8*ubanum+drive
 * The ubamem macro converts a specified unibus address (ala pdp-11)
 * into a unibus memory address space address.
 */

x_int	x_cpu;		/* see <sys/cpu.h> */

#define	x_MAXNMBA	4
#define	x_MAXNUBA	4
struct	x_mba_regs **x_mbaddr;
x_int	x_mbaact;
x_caddr_t	*x_umaddr;
struct	x_uba_regs **x_ubaddr;

#define	x_UNITTOMBA(x_unit)		((x_unit)>>3)
#define	x_UNITTODRIVE(x_unit)	((x_unit)&07)

#define	x_mbamba(x_unit)		(x_mbaddr[x_UNITTOMBA(x_unit)])
#define	x_mbadrv(x_unit) 		(&x_mbamba(x_unit)->x_mba_drv[x_UNITTODRIVE(x_unit)])

#define	x_UNITTOUBA(x_unit)		((x_unit)>>3)
#define	x_ubauba(x_unit)		(x_ubaddr[x_UNITTOUBA(x_unit)])
#define	x_ubamem(x_unit, x_off)	((x_umaddr[x_UNITTOUBA(x_unit)]+x_ubdevreg(x_off)))

#define	x_PHYSUBA0	0x20006000
#define	x_PHYSMBA0	0x20010000
#define	x_PHYSMBA1	0x20012000
#define	x_PHYSUMEM	0x2013e000

/*
 * RM03/5 (4-byte header plus CRC) format information:
 * codes for sector header word 1
 */
#define	x_HDR1_FMT22	0x1000	/* standard 16 bit format */
#define	x_HDR1_OKSCT	0xc000	/* sector ok */
#define	x_HDR1_SSF	0x2000	/* skip sector flag */
