#include "x_.h"

/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)a.out.h	5.1 (Berkeley) 5/30/85
 */

/*
 * Definitions of the a.out header
 * and magic numbers are shared with
 * the kernel.
 */
#include <sys/x_exec.h>

/*
 * Macros which take exec structures as arguments and tell whether
 * the file has a reasonable magic number or offsets to text|symbols|strings.
 */
#define	x_N_BADMAG(x_x) \
    (((x_x).x_a_magic)!=x_OMAGIC && ((x_x).x_a_magic)!=x_NMAGIC && ((x_x).x_a_magic)!=x_ZMAGIC)

#define	x_N_TXTOFF(x_x) \
	((x_x).x_a_magic==x_ZMAGIC ? 1024 : sizeof (struct x_exec))
#define x_N_SYMOFF(x_x) \
	(x_N_TXTOFF(x_x) + (x_x).x_a_text+(x_x).x_a_data + (x_x).x_a_trsize+(x_x).x_a_drsize)
#define	x_N_STROFF(x_x) \
	(x_N_SYMOFF(x_x) + (x_x).x_a_syms)

/*
 * Format of a relocation datum.
 */
struct x_relocation_info {
	x_int	x_r_address;	/* address which is relocated */
x_unsigned_int	x_r_symbolnum:24,	/* local symbol ordinal */
		x_r_pcrel:1, 	/* was relocated pc relative already */
		x_r_length:2,	/* 0=byte, 1=word, 2=long */
		x_r_extern:1,	/* does not include value of sym referenced */
		:4;		/* nothing, yet */
};

/*
 * Format of a symbol table entry; this file is included by <a.out.h>
 * and should be used if you aren't interested the a.out header
 * or relocation information.
 */
struct	x_nlist {
	union {
		char	*x_n_name;	/* for use when in-core */
		x_long	x_n_strx;		/* index into file string table */
	} x_n_un;
unsigned char	x_n_type;		/* type flag, i.e. N_TEXT etc; see below */
	char	x_n_other;	/* unused */
	x_short	x_n_desc;		/* see <stab.h> */
x_unsigned_long	x_n_value;	/* value of this symbol (or sdb offset) */
};
#define	x_n_hash	x_n_desc		/* used internally by ld */

/*
 * Simple values for n_type.
 */
#define	x_N_UNDF	0x0		/* undefined */
#define	x_N_ABS	0x2		/* absolute */
#define	x_N_TEXT	0x4		/* text */
#define	x_N_DATA	0x6		/* data */
#define	x_N_BSS	0x8		/* bss */
#define	x_N_COMM	0x12		/* common (internal to ld) */
#define	x_N_FN	0x1f		/* file name symbol */

#define	x_N_EXT	01		/* external bit, or'ed in */
#define	x_N_TYPE	0x1e		/* mask for all the type bits */

/*
 * Sdb entries have some of the N_STAB bits set.
 * These are given in <stab.h>
 */
#define	x_N_STAB	0xe0		/* if any of these bits set, a SDB entry */

/*
 * Format for namelist values.
 */
#define	x_N_FORMAT	"%08x"
