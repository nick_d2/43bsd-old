#include "x_.h"

/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)utmp.h	5.1 (Berkeley) 5/30/85
 */

/*
 * Structure of utmp and wtmp files.
 *
 * Assuming the number 8 is unwise.
 */
struct x_utmp {
	char	x_ut_line[8];		/* tty name */
	char	x_ut_name[8];		/* user id */
	char	x_ut_host[16];		/* host name, if remote */
	x_long	x_ut_time;		/* time on */
};
