#include "x_.h"

/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)protocol.h	5.1 (Berkeley) 6/4/85
 */

/*
 * Routing Information Protocol
 *
 * Derived from Xerox NS Routing Information Protocol
 * by changing 32-bit net numbers to sockaddr's and
 * padding stuff to 32-bit boundaries.
 */
#define	x_RIPVERSION	1

struct x_netinfo {
	struct	x_sockaddr x_rip_dst;	/* destination net/host */
	x_int	x_rip_metric;		/* cost of route */
};

struct x_rip {
	x_u_char	x_rip_cmd;		/* request/response */
	x_u_char	x_rip_vers;		/* protocol version # */
	x_u_char	x_rip_res1[2];		/* pad to 32-bit boundary */
	union {
		struct	x_netinfo x_ru_nets[1];	/* variable length... */
		char	x_ru_tracefile[1];	/* ditto ... */
	} x_ripun;
#define	x_rip_nets	x_ripun.x_ru_nets
#define	x_rip_tracefile	x_ripun.x_ru_tracefile
};
 
/*
 * Packet types.
 */
#define	x_RIPCMD_REQUEST		1	/* want info */
#define	x_RIPCMD_RESPONSE		2	/* responding to request */
#define	x_RIPCMD_TRACEON		3	/* turn tracing on */
#define	x_RIPCMD_TRACEOFF		4	/* turn it off */

#define	x_RIPCMD_MAX		5
#ifdef x_RIPCMDS
char *x_ripcmds[x_RIPCMD_MAX] =
  { "#0", "REQUEST", "RESPONSE", "TRACEON", "TRACEOFF" };
#endif

#define	x_HOPCNT_INFINITY		16	/* per Xerox NS */
#define	x_MAXPACKETSIZE		512	/* max broadcast size */

/*
 * Timer values used in managing the routing table.
 * Every update forces an entry's timer to be reset.  After
 * EXPIRE_TIME without updates, the entry is marked invalid,
 * but held onto until GARBAGE_TIME so that others may
 * see it "be deleted".
 */
#define	x_TIMER_RATE		30	/* alarm clocks every 30 seconds */

#define	x_SUPPLY_INTERVAL		30	/* time to supply tables */

#define	x_EXPIRE_TIME		180	/* time to mark entry invalid */
#define	x_GARBAGE_TIME		240	/* time to garbage collect */
