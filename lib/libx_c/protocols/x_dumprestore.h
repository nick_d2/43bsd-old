#include "x_.h"

/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)dumprestore.h	5.1 (Berkeley) 6/5/85
 */

/*
 * TP_BSIZE is the size of file blocks on the dump tapes.
 * Note that TP_BSIZE must be a multiple of DEV_BSIZE.
 *
 * NTREC is the number of TP_BSIZE blocks that are written
 * in each tape record. HIGHDENSITYTREC is the number of
 * TP_BSIZE blocks that are written in each tape record on
 * 6250 BPI or higher density tapes.
 *
 * TP_NINDIR is the number of indirect pointers in a TS_INODE
 * or TS_ADDR record. Note that it must be a power of two.
 */
#define x_TP_BSIZE	1024
#define x_NTREC   	10
#define x_HIGHDENSITYTREC	32
#define x_TP_NINDIR	(x_TP_BSIZE/2)

#define x_TS_TAPE 	1
#define x_TS_INODE	2
#define x_TS_BITS 	3
#define x_TS_ADDR 	4
#define x_TS_END  	5
#define x_TS_CLRI 	6
#define x_OFS_MAGIC   	(x_int)60011
#define x_NFS_MAGIC   	(x_int)60012
#define x_CHECKSUM	(x_int)84446

union x_u_spcl {
	char x_dummy[x_TP_BSIZE];
	struct	x_s_spcl {
		x_int	x_c_type;
		x_time_t	x_c_date;
		x_time_t	x_c_ddate;
		x_int	x_c_volume;
		x_daddr_t	x_c_tapea;
		x_ino_t	x_c_inumber;
		x_int	x_c_magic;
		x_int	x_c_checksum;
		struct	x_dinode	x_c_dinode;
		x_int	x_c_count;
		char	x_c_addr[x_TP_NINDIR];
	} x_s_spcl;
} x_u_spcl;

#define x_spcl x_u_spcl.x_s_spcl

#define	x_DUMPOUTFMT	"%-16s %c %s"		/* for printf */
						/* name, incno, ctime(date) */
#define	x_DUMPINFMT	"%16s %c %[^\n]\n"	/* inverse for scanf */
