#include "x_.h"

/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

/*	@(#)timed.h	1.6	(Berkeley)	5/28/86	*/

/*
 * Time Synchronization Protocol
 */

#define	x_TSPVERSION	1
#define x_ANYADDR 	x_NULL

struct x_tsp {
	x_u_char	x_tsp_type;
	x_u_char	x_tsp_vers;
	x_u_short	x_tsp_seq;
	union {
		struct x_timeval x_tspu_time;
		char x_tspu_hopcnt;
	} x_tsp_u;
	char x_tsp_name[x_MAXHOSTNAMELEN];
};

#define	x_tsp_time	x_tsp_u.x_tspu_time
#define	x_tsp_hopcnt	x_tsp_u.x_tspu_hopcnt
 
/*
 * Command types.
 */
#define	x_TSP_ANY			0	/* match any types */
#define	x_TSP_ADJTIME		1	/* send adjtime */
#define	x_TSP_ACK			2	/* generic acknowledgement */
#define	x_TSP_MASTERREQ		3	/* ask for master's name */ 
#define	x_TSP_MASTERACK		4	/* acknowledge master request */
#define	x_TSP_SETTIME		5	/* send network time */
#define	x_TSP_MASTERUP		6	/* inform slaves that master is up */
#define	x_TSP_SLAVEUP		7	/* slave is up but not polled */
#define	x_TSP_ELECTION		8	/* advance candidature for master */
#define	x_TSP_ACCEPT		9	/* support candidature of master */
#define	x_TSP_REFUSE		10	/* reject candidature of master */
#define	x_TSP_CONFLICT		11	/* two or more masters present */
#define	x_TSP_RESOLVE		12	/* masters' conflict resolution */
#define	x_TSP_QUIT		13	/* reject candidature if master is up */
#define	x_TSP_DATE		14	/* reset the time (date command) */
#define	x_TSP_DATEREQ		15	/* remote request to reset the time */
#define	x_TSP_DATEACK		16	/* acknowledge time setting  */
#define	x_TSP_TRACEON		17	/* turn tracing on */
#define	x_TSP_TRACEOFF		18	/* turn tracing off */
#define	x_TSP_MSITE		19	/* find out master's site */
#define	x_TSP_MSITEREQ		20	/* remote master's site request */
#define	x_TSP_TEST		21	/* for testing election algo */
#define	x_TSP_SETDATE		22	/* New from date command */
#define	x_TSP_SETDATEREQ		23	/* New remote for above */
#define	x_TSP_LOOP		24	/* loop detection packet */

#define	x_TSPTYPENUMBER		25

#ifdef x_TSPTYPES
char *x_tsptype[x_TSPTYPENUMBER] =
  { "ANY", "ADJTIME", "ACK", "MASTERREQ", "MASTERACK", "SETTIME", "MASTERUP", 
  "SLAVEUP", "ELECTION", "ACCEPT", "REFUSE", "CONFLICT", "RESOLVE", "QUIT", 
  "DATE", "DATEREQ", "DATEACK", "TRACEON", "TRACEOFF", "MSITE", "MSITEREQ",
  "TEST", "SETDATE", "SETDATEREQ", "LOOP" };
#endif
