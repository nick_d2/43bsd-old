#include "x_.h"

/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)rwhod.h	5.1 (Berkeley) 5/28/85
 */

/*
 * rwho protocol packet format.
 */
struct	x_outmp {
	char	x_out_line[8];		/* tty name */
	char	x_out_name[8];		/* user id */
	x_long	x_out_time;		/* time on */
};

struct	x_whod {
	char	x_wd_vers;		/* protocol version # */
	char	x_wd_type;		/* packet type, see below */
	char	x_wd_pad[2];
	x_int	x_wd_sendtime;		/* time stamp by sender */
	x_int	x_wd_recvtime;		/* time stamp applied by receiver */
	char	x_wd_hostname[32];	/* hosts's name */
	x_int	x_wd_loadav[3];		/* load average as in uptime */
	x_int	x_wd_boottime;		/* time system booted */
	struct	x_whoent {
		struct	x_outmp x_we_utmp;	/* active tty info */
		x_int	x_we_idle;	/* tty idle time */
	} x_wd_we[1024 / sizeof (struct x_whoent)];
};

#define	x_WHODVERSION	1
#define	x_WHODTYPE_STATUS	1		/* host status */
