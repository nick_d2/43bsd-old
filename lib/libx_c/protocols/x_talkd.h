#include "x_.h"

/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)talkd.h	5.2 (Berkeley) 3/13/86
 */

#include <sys/x_types.h>
#include <sys/x_socket.h>
/*
 * This describes the protocol used by the talk server and clients.
 *
 * The talk server acts a repository of invitations, responding to
 * requests by clients wishing to rendezvous for the purpose of
 * holding a conversation.  In normal operation, a client, the caller,
 * initiates a rendezvous by sending a CTL_MSG to the server of
 * type LOOK_UP.  This causes the server to search its invitation
 * tables to check if an invitation currently exists for the caller
 * (to speak to the callee specified in the message).  If the lookup
 * fails, the caller then sends an ANNOUNCE message causing the server
 * to broadcast an announcement on the callee's login ports requesting
 * contact.  When the callee responds, the local server uses the
 * recorded invitation to respond with the appropriate rendezvous
 * address and the caller and callee client programs establish a
 * stream connection through which the conversation takes place.
 */

/*
 * Client->server request message format.
 */
typedef struct {
	x_u_char	x_vers;		/* protocol version */
	x_u_char	x_type;		/* request type, see below */
	x_u_char	x_answer;		/* not used */
	x_u_char	x_pad;
	x_u_long	x_id_num;		/* message id */
	struct	x_sockaddr x_addr;
	struct	x_sockaddr x_ctl_addr;
	x_long	x_pid;		/* caller's process id */
#define	x_NAME_SIZE	12
	char	x_l_name[x_NAME_SIZE];/* caller's name */
	char	x_r_name[x_NAME_SIZE];/* callee's name */
#define	x_TTY_SIZE	16
	char	x_r_tty[x_TTY_SIZE];/* callee's tty name */
} x_CTL_MSG;

/*
 * Server->client response message format.
 */
typedef struct {
	x_u_char	x_vers;		/* protocol version */
	x_u_char	x_type;		/* type of request message, see below */
	x_u_char	x_answer;		/* respose to request message, see below */
	x_u_char	x_pad;
	x_u_long	x_id_num;		/* message id */
	struct	x_sockaddr x_addr;	/* address for establishing conversation */
} x_CTL_RESPONSE;

#define	x_TALK_VERSION	1		/* protocol version */

/* message type values */
#define x_LEAVE_INVITE	0	/* leave invitation with server */
#define x_LOOK_UP		1	/* check for invitation by callee */
#define x_DELETE		2	/* delete invitation by caller */
#define x_ANNOUNCE	3	/* announce invitation by caller */

/* answer values */
#define x_SUCCESS		0	/* operation completed properly */
#define x_NOT_HERE	1	/* callee not logged in */
#define x_FAILED		2	/* operation failed for unexplained reason */
#define x_MACHINE_UNKNOWN	3	/* caller's machine name unknown */
#define x_PERMISSION_DENIED 4	/* callee's tty doesn't permit announce */
#define x_UNKNOWN_REQUEST	5	/* request has invalid type value */
#define	x_BADVERSION	6	/* request has invalid protocol version */
#define	x_BADADDR		7	/* request has invalid addr value */
#define	x_BADCTLADDR	8	/* request has invalid ctl_addr value */

/*
 * Operational parameters.
 */
#define x_MAX_LIFE	60	/* max time daemon saves invitations */
/* RING_WAIT should be 10's of seconds less than MAX_LIFE */
#define x_RING_WAIT	30	/* time to wait before resending invitation */
