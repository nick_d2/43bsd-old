#include "x_.h"


#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

/* gen/regex.c */
char *x_re_comp __P((register char *x_sp));
x_int x_re_exec __P((register char *x_p1));
x_int x_backref __P((register x_int x_i, register char *x_lp));
x_int x_cclass __P((register char *x_set, x_int x_c, x_int x_af));
