#include "x_.h"

/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)fstab.h	5.1 (Berkeley) 5/30/85
 */

/*
 * File system table, see fstab (5)
 *
 * Used by dump, mount, umount, swapon, fsck, df, ...
 *
 * The fs_spec field is the block special name.  Programs
 * that want to use the character special name must create
 * that name by prepending a 'r' after the right most slash.
 * Quota files are always named "quotas", so if type is "rq",
 * then use concatenation of fs_file and "quotas" to locate
 * quota file.
 */
#define	x_FSTAB		"/etc/fstab"

#define	x_FSTAB_RW	"rw"	/* read/write device */
#define	x_FSTAB_RQ	"rq"	/* read/write with quotas */
#define	x_FSTAB_RO	"ro"	/* read-only device */
#define	x_FSTAB_SW	"sw"	/* swap device */
#define	x_FSTAB_XX	"xx"	/* ignore totally */

struct	x_fstab{
	char	*x_fs_spec;		/* block special device name */
	char	*x_fs_file;		/* file system path prefix */
	char	*x_fs_type;		/* FSTAB_* */
	x_int	x_fs_freq;		/* dump frequency, in days */
	x_int	x_fs_passno;		/* pass number on parallel dump */
};

struct	x_fstab *x_getfsent();
struct	x_fstab *x_getfsspec();
struct	x_fstab *x_getfsfile();
struct	x_fstab *x_getfstype();
x_int	x_setfsent();
x_int	x_endfsent();

#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

/* gen/fstab.c */
x_int x_setfsent __P((void));
x_int x_endfsent __P((void));
struct x_fstab *x_getfsent __P((void));
struct x_fstab *x_getfsspec __P((char *x_name));
struct x_fstab *x_getfsfile __P((char *x_name));
struct x_fstab *x_getfstype __P((char *x_type));
