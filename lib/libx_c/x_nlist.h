#include "x_.h"

/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)nlist.h	5.1 (Berkeley) 5/30/85
 */

/*
 * Format of a symbol table entry; this file is included by <a.out.h>
 * and should be used if you aren't interested the a.out header
 * or relocation information.
 */
struct	x_nlist {
	char	*x_n_name;	/* for use when in-core */
	unsigned char x_n_type;	/* type flag, i.e. N_TEXT etc; see below */
	char	x_n_other;	/* unused */
	x_short	x_n_desc;		/* see <stab.h> */
	x_unsigned_long x_n_value;	/* value of this symbol (or sdb offset) */
};
#define	x_n_hash	x_n_desc		/* used internally by ld */

/*
 * Simple values for n_type.
 */
#define	x_N_UNDF	0x0		/* undefined */
#define	x_N_ABS	0x2		/* absolute */
#define	x_N_TEXT	0x4		/* text */
#define	x_N_DATA	0x6		/* data */
#define	x_N_BSS	0x8		/* bss */
#define	x_N_COMM	0x12		/* common (internal to ld) */
#define	x_N_FN	0x1f		/* file name symbol */

#define	x_N_EXT	01		/* external bit, or'ed in */
#define	x_N_TYPE	0x1e		/* mask for all the type bits */

/*
 * Sdb entries have some of the N_STAB bits set.
 * These are given in <stab.h>
 */
#define	x_N_STAB	0xe0		/* if any of these bits set, a SDB entry */

/*
 * Format for namelist values.
 */
#define	x_N_FORMAT	"%08x"

#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

/* gen/nlist.c */
x_int x_nlist __P((char *x_name, struct x_nlist *x_list));
