#include "x_.h"

/*
 * Copyright (c) 1985 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)memory.h	5.1 (Berkeley) 85/08/05
 */

/*
 * Definitions of the Sys5 compat memory manipulation routines
 */

extern char *x_memccpy();
extern char *x_memchr();
extern x_int x_memcmp();
extern char *x_memcpy();
extern char *x_memset();

#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

/* compat-sys5/memccpy.c */
char *x_memccpy __P((register char *x_t, register char *x_f, register x_int x_c, register x_int x_n));
/* compat-sys5/memset.c */
char *x_memset __P((register char *x_s, register x_int x_c, register x_int x_n));
/* compat-sys5/memcmp.c */
x_int x_memcmp __P((register char *x_s1, register char *x_s2, register x_int x_n));
/* compat-sys5/memcpy.c */
char *x_memcpy __P((register char *x_t, register char *x_f, register x_int x_n));
/* compat-sys5/memchr.c */
char *x_memchr __P((register char *x_s, register x_int x_c, register x_int x_n));
