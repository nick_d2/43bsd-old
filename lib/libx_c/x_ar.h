#include "x_.h"

/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)ar.h	5.1 (Berkeley) 5/30/85
 */

#define	x_ARMAG	"!<arch>\n"
#define	x_SARMAG	8

#define	x_ARFMAG	"`\n"

struct x_ar_hdr {
	char	x_ar_name[16];
	char	x_ar_date[12];
	char	x_ar_uid[6];
	char	x_ar_gid[6];
	char	x_ar_mode[8];
	char	x_ar_size[10];
	char	x_ar_fmag[2];
};
