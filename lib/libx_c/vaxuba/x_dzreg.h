#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)dzreg.h	7.1 (Berkeley) 6/5/86
 */

/*
 * DZ-11/DZ-32 Registers and bits.
 */
struct x_dzdevice {
	x_short x_dzcsr;
	x_short x_dzrbuf;
	union {
		struct {
			char	x_dztcr0;
			char	x_dzdtr0;
			char	x_dztbuf0;
			char	x_dzbrk0;
		} x_dz11;
		struct {
			x_short	x_dzlcs0;
			char	x_dztbuf0;
			char	x_dzlnen0;
		} x_dz32;
	} x_dzun;
};

#define x_dzlpr	x_dzrbuf
#define x_dzmsr	x_dzun.x_dz11.x_dzbrk0
#define x_dztcr	x_dzun.x_dz11.x_dztcr0
#define x_dzdtr	x_dzun.x_dz11.x_dzdtr0
#define x_dztbuf	x_dzun.x_dz11.x_dztbuf0
#define x_dzlcs	x_dzun.x_dz32.x_dzlcs0
#define	x_dzbrk	x_dzmsr
#define x_dzlnen	x_dzun.x_dz32.x_dzlnen0
#define x_dzmtsr	x_dzun.x_dz32.x_dztbuf0

/* bits in dzlpr */
#define	x_BITS7	0020
#define	x_BITS8	0030
#define	x_TWOSB	0040
#define	x_PENABLE	0100
#define	x_OPAR	0200

/* bits in dzrbuf */
#define	x_DZ_PE	010000
#define	x_DZ_FE	020000
#define	x_DZ_DO	040000

/* bits in dzcsr */
#define	x_DZ_32	000001		/* DZ32 mode */
#define	x_DZ_MIE	000002		/* Modem Interrupt Enable */
#define	x_DZ_CLR	000020		/* Reset dz */
#define	x_DZ_MSE	000040		/* Master Scan Enable */
#define	x_DZ_RIE	000100		/* Receiver Interrupt Enable */
#define x_DZ_MSC	004000		/* Modem Status Change */
#define	x_DZ_SAE	010000		/* Silo Alarm Enable */
#define	x_DZ_TIE	040000		/* Transmit Interrupt Enable */
#define	x_DZ_IEN	(x_DZ_32|x_DZ_MIE|x_DZ_MSE|x_DZ_RIE|x_DZ_TIE)

/* flags for modem-control */
#define	x_DZ_ON	x_DZ_DTR
#define	x_DZ_OFF	0

/* bits in dzlcs */
#define x_DZ_ACK	0100000		/* ACK bit in dzlcs */
#define x_DZ_RTS	0010000		/* Request To Send */
#define	x_DZ_ST	0004000		/* Secondary Transmit */
#define	x_DZ_BRK	0002000		/* Break */
#define x_DZ_DTR	0001000		/* Data Terminal Ready */
#define	x_DZ_LE	0000400		/* Line Enable */
#define	x_DZ_DSR	0000200		/* Data Set Ready */
#define	x_DZ_RI	0000100		/* Ring Indicate */
#define x_DZ_CD	0000040		/* Carrier Detect */
#define	x_DZ_CTS	0000020		/* Clear To Send */
#define	x_DZ_SR	0000010		/* Secondary Receive */
 
/* bits in dm lsr, copied from dmreg.h */
#define	x_DML_DSR		0000400		/* data set ready, not a real DM bit */
#define	x_DML_RNG		0000200		/* ring */
#define	x_DML_CAR		0000100		/* carrier detect */
#define	x_DML_CTS		0000040		/* clear to send */
#define	x_DML_SR		0000020		/* secondary receive */
#define	x_DML_ST		0000010		/* secondary transmit */
#define	x_DML_RTS		0000004		/* request to send */
#define	x_DML_DTR		0000002		/* data terminal ready */
#define	x_DML_LE		0000001		/* line enable */
