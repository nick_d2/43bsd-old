#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)vpreg.h	7.1 (Berkeley) 6/5/86
 */

#define	x_VPPRI	(x_PZERO-1)

struct	x_vpdevice {
	x_short	x_plbcr;
	x_short	x_pbxaddr;
	x_short	x_prbcr;
	x_u_short x_pbaddr;
	x_short	x_plcsr;
	x_short	x_plbuf;
	x_short	x_prcsr;
	x_u_short x_prbuf;
};

#define	x_VP_ERROR	0100000
#define	x_VP_DTCINTR	0040000
#define	x_VP_DMAACT	0020000
#define	x_VP_READY	0000200
#define	x_VP_IENABLE	0000100
#define	x_VP_TERMCOM	0000040
#define	x_VP_FFCOM	0000020
#define	x_VP_EOTCOM	0000010
#define	x_VP_CLRCOM	0000004
#define	x_VP_RESET	0000002
#define	x_VP_SPP		0000001
