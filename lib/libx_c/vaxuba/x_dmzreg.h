#include "x_.h"

/*
 * Copyright (c) 1985, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)dmzreg.h	7.1 (Berkeley) 6/5/86
 */

/*
 * HISTORY
 * 23-Apr-85  Joe Camaratta (jcc) at Siemens RTL
 *	Header file for DEC's DMZ32
 */

struct x_dmzdevice {
	x_short x_dmz_config;	/* configuration cntl and status register */
	x_short x_dmz_diag;		/* diagnostic control and status register */
	struct {
		x_short x_octet_csr;	/* octet control and status */
		x_short x_octet_lprm;	/* line parameter */
		union{
			x_short x_octet_rb;		/* receiver buffer */
			x_short x_octet_rsp;	/* receive silo parameter */
		} x_octet_receive;
		union{
			x_u_short x_word;		/* word */
			x_u_char x_bytes[2];	/* bytes */
		} x_octet_ir;			/* indirect registers */
	} x_octet[3];
	x_short x_dmz_unused[2];
};

#define x_octet_sato x_octet_rsp

/* aliases for asynchronous indirect control registers */
#define	x_IR_TBUF		000	/* transmit character */
#define	x_IR_RMSTSC	000	/* receive modem status, transmit silo count */
#define	x_IR_LCTMR	010	/* line control and transmit modem */
#define	x_IR_TBA		020	/* transmit buffer address register */
#define	x_IR_TCC		030	/* transmit character count (DMA) */

#define	x_octet_tbf	x_octet_ir.x_bytes[0]	/* transmit buffer */
#define	x_octet_tbf2	x_octet_ir.x_word		/* transmit buffer, 2 chars */
#define	x_octet_rmstsc	x_octet_ir.x_word	/* rcv modem status, xmit silo count */
#define	x_octet_lctmr	x_octet_ir.x_word		/* line control, xmit modem */
#define	x_octet_tba	x_octet_ir.x_word		/* transmit buffer address */
#define	x_octet_tcc	x_octet_ir.x_word		/* transmit character count */

/* bits in octet_csr */
#define	x_DMZ_TRDY	0100000		/* transmit ready */
#define	x_DMZ_TIE		0040000		/* transmit interrupt enable */
#define	x_DMZ_NXM		0030000		/* non-existant memory */
#define	x_DMZ_LIN		0003400		/* transmit line number */
#define	x_DMZ_RRDY	0000200		/* receiver data available */
#define	x_DMZ_RIE		0000100		/* receiver interrupt enable */
#define	x_DMZ_RESET	0000040		/* master reset */
#define	x_DMZ_IAD		0000037		/* indirect address register */

#define	x_DMZ_IE		(x_DMZ_TIE | x_DMZ_RIE)	/* enable transmit and receive */

/* bits in octet_lprm (taken from dmfreg.h) */
#define	x_DMZ_6BT		0010		/* 6 bits per character */
#define	x_DMZ_7BT		0020		/* 7 bits per character */
#define	x_DMZ_8BT		0030		/* 8 bits per character */
#define	x_DMZ_PEN		0040		/* parity enable */
#define	x_DMZ_EPR		0100		/* even parity */
#define	x_DMZ_SCD		0200		/* stop code */
#define	x_DMZ_XTE		0170000		/* transmit rate */
#define	x_DMZ_RRT		0007400		/* receive rate */
#define	x_DMZ_LSL		0000007		/* line select */

/* baud rates */
#define	x_BR_50		000
#define	x_BR_75		001
#define	x_BR_110		002
#define	x_BR_134_5	003
#define	x_BR_150		004
#define	x_BR_300		005
#define	x_BR_600		006
#define	x_BR_1200		007
#define	x_BR_1800		010
#define	x_BR_2000		011
#define	x_BR_2400		012
#define	x_BR_3600		013
#define	x_BR_4800		014
#define	x_BR_7200		015
#define	x_BR_9600		016
#define	x_BR_19200	017

/* bits in octet_rb (taken from dmfreg.h) */
#define	x_DMZ_DSC		0004000		/* data set change */
#define	x_DMZ_PE		0010000		/* parity error */
#define	x_DMZ_FE		0020000		/* framing error */
#define	x_DMZ_DO		0040000		/* data overrun */
#define	x_DMZ_DV		0100000		/* data valid */
#define	x_DMZ_RL		0003400		/* line */
#define	x_DMZ_RD		0000377		/* data */
#define	x_DMZ_AT		0000377		/* alarm timeout */

/* bits in dmz_rmstsc */
#define	x_DMZ_TSC		0x00ff		/* transmit silo count */
#define	x_DMZ_USRR	0x0400		/* user modem signal (pin 25) */
#define	x_DMF_SR		0x0800		/* secondary receive */
#define	x_DMZ_CTS		0x1000		/* clear to send */
#define	x_DMZ_CAR		0x2000		/* carrier detect */
#define	x_DMZ_RNG		0x4000		/* ring */
#define	x_DMZ_DSR		0x8000		/* data set ready */

/* bits in dmz_lctmr (tms half) */
#define	x_DMZ_USRW	0x0100		/* user modem signal (pin 18) */
#define	x_DMZ_DTR		0x0200		/* data terminal ready */
#define	x_DMZ_RATE	0x0400		/* data signal rate select */
#define	x_DMF_ST		0x0800		/* secondary transmit */
#define	x_DMZ_RTS		0x1000		/* request to send */
#define	x_DMZ_PREEMPT	0x8000		/* preempt output */

/* bits in octet_lctmr (lc half) */
#define	x_DMZ_MIE		0040		/* modem interrupt enable */
#define	x_DMZ_FLS		0020		/* flush transmit silo */
#define	x_DMZ_BRK		0010		/* send break bit */
#define	x_DMZ_RE		0004		/* receive enable */
#define	x_DMZ_AUT		0002		/* auto XON/XOFF */
#define	x_DMZ_TE		0001		/* transmit enable */
#define	x_DMZ_CF		0300		/* control function */

#define	x_DMZ_LCE		(x_DMZ_MIE|x_DMZ_RE|x_DMZ_TE)
#define	x_DMZ_ON		(x_DMZ_DTR|x_DMZ_RTS|x_DMZ_LCE)
#define	x_DMZ_OFF		x_DMZ_LCE


/* bits in octet_tcc */
#define	x_DMZ_HA		0140000		/* high address bits */

/* bits added to dm lsr for DMGET/DMSET */
#define	x_DML_USR		0001000		/* usr modem sig, not a real DM bit */
#define	x_DML_DSR		0000400		/* data set ready, not a real DM bit */

#define	x_DMZ_SIZ		32		/* size of DMZ output silo (per line) */

#define	x_DMZ(x_a)		(x_a/24)
#define	x_OCTET(x_a)	((x_a%24)/8)
#define	x_LINE(x_a)		((x_a%24)%8)

#define	x_DMZ_NOC_MASK	03
#define	x_DMZ_INTERFACE	000
