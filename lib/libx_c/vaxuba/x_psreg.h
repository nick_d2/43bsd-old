#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)psreg.h	7.1 (Berkeley) 6/5/86
 */


/*
 *	The Real Nitty Gritty Device Registers
 */

struct x_psdevice {
	x_short x_ps_data;		/* data register */
	x_short x_ps_addr;		/* address register */
	x_short x_ps_wcount;		/* word count register */
	x_short x_ps_busaddr;		/* unibus address register */
	x_short x_ps_iostat;		/* io status register */
};

/*
 *	Possible ioctl's
 */
#define x_PSIOAUTOREFRESH		x__IO(x_p, 0)	/* auto refresh */
#define x_PSIOSINGLEREFRESH	x__IO(x_p, 1)	/* single refresh */
#define x_PSIOAUTOMAP		x__IO(x_p, 2)	/* auto map */
#define x_PSIOSINGLEMAP		x__IO(x_p, 3)	/* single map */
#define x_PSIODOUBLEBUFFER	x__IO(x_p, 4)	/* double buffer */
#define x_PSIOSINGLEBUFFER	x__IO(x_p, 5)	/* single buffer */
#define x_PSIOWAITREFRESH		x__IO(x_p, 6)	/* await refresh */
#define x_PSIOWAITMAP		x__IO(x_p, 7)	/* await map */
#define x_PSIOWAITHIT		x__IO(x_p, 8)	/* await hit */
#define x_PSIOSTOPREFRESH		x__IO(x_p, 9)	/* stop refresh */
#define x_PSIOSTOPMAP		x__IO(x_p,10)	/* stop map */
#define x_PSIOGETADDR		x__IOR(x_p,11, x_int)	/* get Unibus address */
#define x_PSIOTIMEREFRESH		x__IO(x_p,12)	/* time refresh */

/*
 *	Picture system io status register bits
 */

#define x_DIOREADY	0100000
#define x_PSAHOLD		040000
#define x_PSRESET		020000
#define x_DIORESET	010000
#define x_DMARESET	04000
#define x_PSIE		0400
#define x_DMAREADY	0200
#define x_DMAIE		0100
#define x_PASSIVE		010
#define x_DMAIN		04
#define x_NEXEM		02
#define x_GO		01

/*
 *	Picture system memory mapping control registers: SCB 0177400-0177410
 */

#define x_EXMMR_DMA	0177400
#define x_EXMMR_DIO	0177404
#define x_EXMMR_RC	0177405
#define x_EXMMR_MAPOUT	0177406
#define x_EXMMR_MAPIN	0177407
#define x_EXMSR		0177410

/*
 *	Extended memory status register bits
 */

#define x_DBERROR		0100000
#define x_SBERROR		040000
#define x_MEMREADY	0200
#define x_DBIE		0100
#define x_MMENBL		02
#define x_INITMEM		01

/*
 *	Size of extended memory
 */

#define x_NEXMPAGES	(256*2)
#define x_WORDSPERPAGE	(256)

/*
 *	MAP picture processor registers: SCB 0177750-0177753
 */

#define x_MAOL		0177750
#define x_MAOA		0177751
#define x_MAIA		0177752
#define x_MASR		0177753
#define x_MAMSR		0177754

/*
 *	MAP status register bits
 */

#define x_PPDONE		0100000
#define x_FIFOFULL	040000
#define x_FIFOEMPTY	020000
#define x_HIT		010000
#define x_IB		04000
#define x_TAKE		02000
#define x_MMODE		01400
#define x_MOSTOPPED	0200
#define x_IOUT		0100
#define x_MAO		040
#define x_MAI		020
#define x_HIT_HOLD	010
#define x_RSR_HOLD	04
#define x_VEC_HOLD	02
#define x_MAP_RESET	01

/*
 *	Refresh controller registers: SCB 0177730-0177737
 */

#define x_RFCSN		0177730
#define x_RFSN		0177731
#define x_RFAWA		0177732
#define x_RFAWL		0177733
#define x_RFAIA		0177734
#define x_RFASA		0177735
#define x_RFAIL		0177736
#define x_RFSR		0177737

/*
 *	Refresh controller status register bits
 */

#define x_RFSTOPPED	0100000
#define x_RFHOLD		040000
#define x_RFSTART		020000
#define x_AUTOREF		010000
#define x_RFBLANK		04000
#define x_RIGHT		02000
#define x_LGFIFO_FULL	01000
#define x_NOT_EXEC	0200
#define x_SKIPSEG		0100
#define x_WRITEBACK	040
#define x_SEARCH		020
#define x_MATCH_HOLD	010
#define x_MATCH_DEC	04
#define x_SEARCH_MODE	03

/*
 *	Interrupt control
 */

#define x_RTCREQ		0177760
#define x_RTCIE		0177761
#define x_SYSREQ		0177762
#define x_SYSIE		0177763
#define x_DEVREQ		0177764
#define x_DEVIE		0177765

/*
 *	System interrupt request bits
 */

#define x_LPEN_REQ	0200
#define x_MATCH_REQ	0100
#define x_WBSTOP_REQ	040
#define x_RFSTOP_REQ	020
#define x_MOSTOP_REQ	010
#define x_JUMP_REQ	04
#define x_HIT_REQ		02
#define x_HALT_REQ	01

/*
 *	Real-Time Clock registers
 */

#define x_RTCCNT		0177744
#define x_RTCSR		0177745

/*
 *	Real-Time Clock status register bits
 */

#define x_HZ120		040
#define x_EXT		020
#define x_SYNC		010
#define x_EXTSEL2		04
#define x_EXTSEL1		02
#define x_RUN		01

/*
 *	Control dials a/d registers
 */

#define x_ADDR0		0177500
#define x_ADDR1		0177501
#define x_ADDR2		0177502
#define x_ADDR3		0177503
#define x_ADDR4		0177504
#define x_ADDR5		0177505
#define x_ADDR6		0177506
#define x_ADDR7		0177507

/*
 *	Function switches and lights
 */

#define x_FSWR		0177626
#define x_FSLR		0177627
