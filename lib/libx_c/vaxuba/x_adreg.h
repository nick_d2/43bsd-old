#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)adreg.h	7.1 (Berkeley) 6/5/86
 */

struct x_addevice {
	x_short x_ad_csr;			/* Control status register */
	x_short x_ad_data;			/* Data buffer */
};

#define x_AD_CHAN		x_ADIOSCHAN
#define x_AD_READ		x_ADIOGETW
#define	x_ADIOSCHAN	x__IOW(x_a, 0, x_int)		/* set channel */
#define	x_ADIOGETW	x__IOR(x_a, 1, x_int)		/* read one word */

/*
 * Unibus CSR register bits
 */

#define x_AD_START		01
#define x_AD_SCHMITT		020
#define x_AD_CLOCK		040
#define x_AD_IENABLE		0100
#define x_AD_DONE 		0200
#define x_AD_INCENABLE		040000
#define x_AD_ERROR		0100000
