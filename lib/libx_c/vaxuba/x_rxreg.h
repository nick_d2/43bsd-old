#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)rxreg.h	7.1 (Berkeley) 6/5/86
 */

#ifdef x_KERNEL
#include "x_ioctl.h"
#else
#include <sys/x_ioctl.h>
#endif

/*
 * RX02 registers
 */
struct x_rxdevice {
	x_short	x_rxcs;		/* control/status register */
	x_short	x_rxdb;		/* data buffer register */
};

/*
 * RX211 Command and Status Register (RX2CS)
 */
#define	x_RX_DRV0		0x0000	/* select drive 0 */
#define	x_RX_DRV1		0x0010	/* select drive 1 */
#define	x_RX_DONE		0x0020	/* function complete */
#define	x_RX_INTR		0x0040	/* interrupt enable */
#define	x_RX_TREQ		0x0080	/* transfer request (data only)	*/
#define	x_RX_SDEN		0x0000	/* single density */
#define	x_RX_DDEN		0x0100	/* double density */
#define	x_RX_EXT		0x3000	/* extended address bits */
#define	x_RX_INIT		0x4000	/* initialize RX211 interface */
#define	x_RX_ERR		0x8000	/* general error bit */

/*
 * RX211 control function bits (0-3 of RX2CS)
 */
#define	x_RX_FILL		0x0001	/* fill the buffer */
#define	x_RX_EMPTY	0x0003	/* empty the buffer */
#define	x_RX_WRITE	0x0005	/* write the buffer to disk */
#define	x_RX_READ		0x0007	/* read a disk sector to the buffer */
#define	x_RX_FORMAT	0x0009	/* set the media density (format) */
#define	x_RX_RDSTAT	0x000b	/* read the disk status */
#define	x_RX_WDDS		0x000d	/* write a deleted-data sector */
#define	x_RX_RDERR	0x000f	/* read the error registers */

#define	x_RXCS_BITS \
"\20\20RX_ERR\17RX_INIT\11RX_DDEN\10RX_TREQ\7RX_IE\6RX_DONE\5RX_DRV1"

/*
 * RX211 Error and Status Register (RX2ES) --
 * information is located in RX2DB after completion of function.
 * The READY bit's value is available only after a "read status".
 */
#define	x_RXES_CRCERR	0x0001	/* CRC error (data read error) */
#define	x_RXES_IDONE	0x0004	/* reinitialization complete */
#define x_RXES_DENERR	0x0010	/* density error */
#define	x_RXES_DBLDEN	0x0020	/* set if double density */
#define	x_RXES_DDMARK	0x0040	/* deleted-data mark */
#define	x_RXES_READY	0x0080	/* drive is ready */

#define	x_RXES_BITS \
"\20\14RXES_NXM\13RXES_WCOF\11RXES_DRV1\10RXES_RDY\7RXES_DDMK\6RXES_DDEN\5\
x_RXES_DNER\4x_RXES_ACLO\3x_RXES_ID\1x_RXES_CRC"

/* 
 * Ioctl commands, move to dkio.h later
 */
#define x_RXIOC_FORMAT	x__IOW(x_d, 1, x_int)	/* format the disk */
#define x_RXIOC_WDDS	x__IOW(x_d, 2, x_int)	/* write `deleted data' mark */
					/* on next sector */
#define x_RXIOC_RDDSMK	x__IOR(x_d, 3, x_int)	/* did last read sector contain */
					/* `deleted data'?*/
#define	x_RXIOC_GDENS	x__IOR(x_d, 4, x_int)	/* return density of current disk */

#ifdef x_RXDEFERR
/*
 * Table of values for definitive error code (rxxt[0] & 0xff)
 */
struct x_rxdeferr {
	x_short	x_errval;
	char	*x_errmsg;
} x_rxdeferr[] = {
	{ 0010,	"Can't find home on drive 0" },
	{ 0020,	"Can't find home on drive 1" },
	{ 0040,	"Bad track number requested" },
	{ 0050,	"Home found too soon" },
	{ 0070,	"Can't find desired sector" },
	{ 0110,	"No SEP clock seen" },
	{ 0120,	"No preamble found" },
	{ 0130,	"Preamble, but no ID mark" },
	{ 0140, "Header CRC error"},
	{ 0150,	"Track addr wrong in header" },
	{ 0160,	"Too many tries for ID AM" },
	{ 0170,	"No data AM found" },
	{ 0200,	"Data CRC error" },
	{ 0220,	"Maintenance test failure" },
	{ 0230,	"Word count overflow" },
	{ 0240,	"Density error" },
	{ 0250,	"Set-density protocol bad" },
	{ 0,	"Undefined error code" }
};
#endif
