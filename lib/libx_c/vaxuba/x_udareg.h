#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)udareg.h	7.1 (Berkeley) 6/5/86
 */

/*
 * UDA-50 registers and structures
 */

struct x_udadevice {
	x_short	x_udaip;		/* initialization and polling */
	x_short	x_udasa;		/* status and address */
};

#define	x_UDA_ERR		0100000	/* error bit */
#define	x_UDA_STEP4	0040000	/* step 4 has started */
#define	x_UDA_STEP3	0020000	/* step 3 has started */
#define	x_UDA_STEP2	0010000	/* step 2 has started */
#define	x_UDA_STEP1	0004000	/* step 1 has started */
#define	x_UDA_NV		0002000	/* no host settable interrupt vector */
#define	x_UDA_QB		0001000	/* controller supports Q22 bus */
#define	x_UDA_DI		0000400	/* controller implements diagnostics */
#define	x_UDA_IE		0000200	/* interrupt enable */
#define	x_UDA_PI		0000001	/* host requests adapter purge interrupts */
#define	x_UDA_GO		0000001	/* start operation, after init */


/*
 * UDA Communications Area
 */

struct x_udaca {
	x_short	x_ca_xxx1;	/* unused */
	char	x_ca_xxx2;	/* unused */
	char	x_ca_bdp;		/* BDP to purge */
	x_short	x_ca_cmdint;	/* command queue transition interrupt flag */
	x_short	x_ca_rspint;	/* response queue transition interrupt flag */
	x_long	x_ca_rspdsc[x_NRSP];/* response descriptors */
	x_long	x_ca_cmddsc[x_NCMD];/* command descriptors */
};

#define	x_ca_ringbase	x_ca_rspdsc[0]

#define	x_UDA_OWN	0x80000000	/* UDA owns this descriptor */
#define	x_UDA_INT	0x40000000	/* allow interrupt on ring transition */

/*
 * MSCP packet info
 */
struct x_mscp_header {
	x_short	x_uda_msglen;	/* length of MSCP packet */
	char	x_uda_credits;	/* low 4 bits: credits, high 4 bits: msgtype */
	char	x_uda_vcid;	/* virtual circuit id */
};
