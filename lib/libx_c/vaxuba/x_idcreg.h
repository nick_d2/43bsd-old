#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)idcreg.h	7.1 (Berkeley) 6/5/86
 */

#define	x_NRB02SECT	40	/* RB02 sectors/track */
#define	x_NRB02TRK	2	/* RB02 tracks/cylinder */
#define	x_NRB02CYL	512	/* RB02 cylinders/disk */
#define	x_NRB80SECT	31	/* RB80 sectors/track */
#define	x_NRB80TRK	14	/* RB80 tracks/cylinder */
#define	x_NRB80CYL	559	/* RB80 cylinders/disk */

struct x_idcdevice
{
	x_int	x_idccsr;		/* control status register */
	x_int	x_idcbar;		/* bus address register */
	x_int	x_idcbcr;		/* byte count register */
	x_int	x_idcdar;		/* disk address register */
	x_int	x_idcmpr;		/* multi-purpose register */
	x_int	x_idceccpos;	/* ecc position register */
	x_int	x_idceccpat;	/* ecc pattern register */
	x_int	x_idcreset;	/* master reset register */
};

/* idccsr */
#define	x_IDC_TOI		0x10000000	/* time out inhibit */
#define	x_IDC_ASSI	0x08000000	/* automatic skip sector inhibit */
#define	x_IDC_R80		0x04000000	/* selected disk is R80 */
#define	x_IDC_MTN		0x02000000	/* maintenance */
#define	x_IDC_IR		0x01000000	/* interrupt request */
#define	x_IDC_SSE		0x00800000	/* R80 skip sector error */
#define	x_IDC_SSEI	0x00400000	/* R80 skip sector error inhibit */
#define	x_IDC_ECS		0x00300000	/* R80 ecc status */
#define	x_IDC_ECS_NONE	0x00000000	/*   no data error */
#define	x_IDC_ECS_HARD	0x00200000	/*   hard ecc error */
#define	x_IDC_ECS_SOFT	0x00300000	/*   soft ecc error */
#define	x_IDC_ATTN	0x000f0000	/* attention bits */
#define	x_IDC_ERR		0x00008000	/* composite error */
#define	x_IDC_DE		0x00004000	/* drive error */
#define	x_IDC_NXM		0x00002000	/* non-existant memory */
#define	x_IDC_DLT		0x00001000	/* data late */
#define	x_IDC_HNF		x_IDC_DLT		/* header not found */
#define	x_IDC_DCK		0x00000800	/* data check */
#define	x_IDC_OPI		0x00000400	/* operation incomplete */
#define	x_IDC_DS		0x00000300	/* drive select bits */
#define	x_IDC_CRDY	0x00000080	/* controller ready */
#define	x_IDC_IE		0x00000040	/* interrupt enable */
#define	x_IDC_FUNC	0x0000000e	/* function code */
#define	x_IDC_DRDY	0x00000001	/* drive ready */

#define	x_IDC_HARD	(x_IDC_NXM|x_IDC_DE)

#define	x_IDCCSR_BITS \
"\20\35TOI\34ASSI\33R80\32MTN\31IR\30SSE\27SSEI\26ECS1\25ECS0\24ATN3\
\23x_ATN2\22x_ATN1\21x_ATN0\20Ex_RR\17Dx_E\16x_NXM\15Dx_LT\14Dx_CK\13x_OPI\12Dx_S1\11Dx_S0\
\10x_CRDY\7x_IE\4x_F2\3x_F1\2x_F0\1Dx_RDY"

/* function codes */
#define	x_IDC_NOP		000		/* no operation */
#define	x_IDC_WCHK	002		/* write check data */
#define	x_IDC_GETSTAT	004		/* get status */
#define	x_IDC_SEEK	006		/* seek */
#define	x_IDC_RHDR	010		/* read header */
#define	x_IDC_WRITE	012		/* write data */
#define	x_IDC_READ	014		/* read data */
#define	x_IDC_RNOHCHK	016		/* read data w/o header check */

/* idcmpr for RL02 get status command */
#define	x_IDCGS_RST	010		/* reset */
#define	x_IDCGS_GS	002		/* get status, must be 1 */
#define	x_IDCGS_M		001		/* mark, must be 1 */
#define	x_IDCGS_GETSTAT	(x_IDCGS_RST|x_IDCGS_GS|x_IDCGS_M)

/* RL02 status word */
#define	x_IDCDS_WDE	0100000		/* write data error */
#define	x_IDCDS_HCE	0040000		/* head current error */
#define	x_IDCDS_WL	0020000		/* write lock */
#define	x_IDCDS_SKTO	0010000		/* seek timeout */
#define	x_IDCDS_SPD	0004000		/* spindle error */
#define	x_IDCDS_WGE	0002000		/* write gate error */
#define	x_IDCDS_VC	0001000		/* volume check */
#define	x_IDCDS_DSE	0000400		/* drive select error */
#define	x_IDCDS_HS	0000100		/* head select */
#define	x_IDCDS_CO	0000040		/* cover open */
#define	x_IDCDS_HO	0000020		/* heads out */
#define	x_IDCDS_BH	0000010		/* brush home */
#define	x_IDCDS_STATE	0000007		/* drive state */

#define	x_IDCRB02DS_BITS \
"\10\20WDE\17HCE\16WL\15SKTO\14SPD\13WBE\12VC\11DSE\
\7x_HS\6x_CO\5x_HO\4x_BH\3x_STC\2x_STB\1x_STA"

/* R80 status word */
#define	x_IDCDS_WTP	0020000		/* write protect */
#define	x_IDCDS_DRDY	0010000		/* driver ready */
#define	x_IDCDS_ONCY	0004000		/* on cylinder */
#define	x_IDCDS_SKE	0002000		/* seek error */
#define	x_IDCDS_PLGV	0001000		/* plug valid */
#define	x_IDCDS_FLT	0000400		/* fault */

#define	x_IDCRB80DS_BITS \
"\10\16WTP\15DRDY\14ONCY\13SKE\12PLGV\11FLT\5SEC4\4SEC3\3SEC2\2SEC1\1SEC0"
