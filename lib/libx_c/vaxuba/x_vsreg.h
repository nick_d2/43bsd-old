#include "x_.h"

/* @(#)vsreg.h	7.1 (MIT) 6/5/86 */
 /****************************************************************************
 *									    *
 *  Copyright (c) 1983, 1984 by						    *
 *  DIGITAL EQUIPMENT CORPORATION, Maynard, Massachusetts.		    *
 *  All rights reserved.						    *
 * 									    *
 *  This software is furnished on an as-is basis and may be used and copied *
 *  only with inclusion of the above copyright notice. This software or any *
 *  other copies thereof may be provided or otherwise made available to     *
 *  others only for non-commercial purposes.  No title to or ownership of   *
 *  the software is hereby transferred.					    *
 * 									    *
 *  The information in this software is  subject to change without notice   *
 *  and  should  not  be  construed as  a commitment by DIGITAL EQUIPMENT   *
 *  CORPORATION.							    *
 * 									    *
 *  DIGITAL assumes no responsibility for the use  or  reliability of its   *
 *  software on equipment which is not supplied by DIGITAL.		    *
 * 									    *
 *									    *
 ****************************************************************************/

/* 
 * vsreg.h - VS100 Registers and Bits
 * 
 * Author:	Christopher A. Kent
 *		Digital Equipment Corporation
 *		Western Research Lab
 * Date:	Tue Jun 14 1983
 */

struct x_vsdevice{
	x_u_short	x_vs_csr0;		/* Control and Status */
	x_u_short	x_vs_csr1;		/* Interrupt Reason */
	x_u_short	x_vs_csr2;		/* Keyboard Receive */
	x_u_short	x_vs_csr3;		/* Function Parameter Low */
	x_u_short	x_vs_csr4;		/* Function Parameter High */
	x_u_short	x_vs_csr5;		/* Cursor Position X */
	x_u_short	x_vs_csr6;		/* Cursor Position Y */
	x_u_short	x_vs_csr7;		/* Interrupt Vector */
	x_u_short	x_vs_csr8;		/* Spare 1 */
	x_u_short	x_vs_csr9;		/* Spare 2 */
	x_u_short	x_vs_csra;		/* Spare 3 */
	x_u_short	x_vs_csrb;		/* Spare 4 */
	x_u_short	x_vs_csrc;		/* Spare 5 */
	x_u_short	x_vs_csrd;		/* Spare 6 */
	x_u_short	x_vs_csre;		/* Spare 7 */
	x_u_short	x_vs_csrf;		/* Interrupt Vector (2Bs) */
};

/* 
 * CSR0 - Control and Status
 */

#define	x_VS_LNK_TRNS	0100000		/* Link Transition */
#define	x_VS_LNK_AVL	0040000		/* Link Available */
#define	x_VS_LNK_ERR	0020000		/* Link Error */
#define	x_VS_XMIT_ON	0010000		/* Transmitter On */
#define	x_VS_MNT_MODE	0004000		/* Maintenance Mode */
#define	x_VS_CRC_DIS	0002000		/* CRC Disable */
#define	x_VS_MNT_DONE	0001000		/* Maintenance Done */
#define	x_VS_SPARE	0000400		/* Spare */
#define	x_VS_OWN		0000200		/* Owner */
#define	x_VS_IE		0000100		/* Interrupt Enable */
#define	x_VS_FCN		0000076		/* Function Code */
#define	x_VS_GO		0000001		/* GO! */

struct x_vs_csr{
    union{
	x_u_short	x__register;
	struct{
	    unsigned x__go : 1;
	    unsigned x__function : 5;
	    unsigned x__ie : 1;
	    unsigned x__own : 1;
	    unsigned x__spare : 1;
	    unsigned x__mainDone : 1;
	    unsigned x__CRCdisable : 1;
	    unsigned x__mainMode : 1;
	    unsigned x__xmitOn : 1;
	    unsigned x__linkErr : 1;
	    unsigned x__linkAvail : 1;
	    unsigned x__linkTran : 1;
	}x__bits;
    }x__X;
};

#define	x_csr_reg		x__X.x__register
#define x_csr_go		x__X.x__bits.x__go
#define x_csr_ie		x__X.x__bits.x__ie
#define	x_csr_own		x__X.x__bits.x__own
#define	x_csr_mainDone	x__X.x__bits.x__mainDone
#define	x_csr_CRCdisable	x__X.x__bits.x__CRCdisable
#define x_csr_mainMode	x__X.x__bits.x__mainMode
#define	x_csr_xmitOn	x__X.x__bits.x__xmitOn
#define	x_csr_linkErr	x__X.x__bits.x__linkErr
#define	x_csr_linkAvail	x__X.x__bits.x__linkAvail
#define	x_csr_linkTran	x__X.x__bits.x__linkTran

/* Function Codes */

#define	x_VS_INIT		01		/* Initialize Display */
#define	x_VS_SEND		02		/* Send Packet */
#define	x_VS_START	03		/* Start Microcode */
#define	x_VS_ABORT	04		/* Abort Command Chain */
#define	x_VS_PWRUP	05		/* Power Up Reset */
/**/
#define	x_VS_ENABBA	020		/* Enable BBA */
#define	x_VS_DISBBA	021		/* Disable BBA */
#define	x_VS_INFINITE	022		/* Inifinite Retries */
#define	x_VS_FINITE	023		/* Finite Retries */

/* amount to shift to get function code into right place */

#define	x_VS_FCSHIFT	01

/* 
 * CSR1 - Interrupt Reason
 */

#define	x_vs_irr		x_vs_csr1

#define	x_VS_ERROR	0100000		/* Any error */
#define	x_VS_REASON	0077777		/* Reason Mask */
#define	x_VSIRR_BITS \
"\20\20ERROR\10PWRUP\6TABLET\5MOUSE\4BUTTON\3START\2DONE\1INIT"

#define	x_VS_INT_US	0
#define	x_VS_INT_ID	01
#define	x_VS_INT_CD	02
#define	x_VS_INT_SE	04
#define	x_VS_INT_BE	010
#define	x_VS_INT_MM	020
#define	x_VS_INT_TM	040
#define	x_VS_INT_PWR	0200

struct x_vs_intr{
    union{
	x_u_short	 x__register;		/* whole register */
	struct{
	    unsigned x__reason : 14;	/* Reason bits */
	    unsigned x__diagnostic : 1;	/* Diagnostic Error bit */
	    unsigned x__error : 1;	/* Error bit */
	}x__bits;
    }x__X;
};

#define	x_intr_reg	x__X.x__register
#define	x_intr_reason	x__X.x__bits.x__reason
#define	x_intr_diagnostic	x__X.x__bits.x__diagnostic	/* not in rev 2b */
#define	x_intr_error	x__X.x__bits.x__error

/* 
 * CSR2 - Keyboard Receive
 */

#define	x_vs_krr		x_vs_csr2

#define	x_VS_KBDEV	0007000		/* Device mask */
#define	x_VS_KBT		0000400		/* Transition direction */
#define	x_VS_KBKEY	0000377		/* Key mask */

struct x_vs_kbd{
    union{
	x_u_short	 x__register;		/* whole register */
	struct{
	    unsigned x__key : 8;		/* Key number */
	    unsigned x__transition : 1;	/* Transition direction */
	    unsigned x__device : 3;	/* Device */
	    unsigned x__x : 4;		/* Unused */
	}x__bits;
    }x__X;
};

#define	x_kbd_reg		x__X.x__register
#define	x_kbd_key		x__X.x__bits.x__key
#define	x_kbd_transition	x__X.x__bits.x__transition
#define	x_kbd_device	x__X.x__bits.x__device

#define	x_VS_KBTUP	0		/* up */
#define	x_VS_KBTDOWN	1		/* down */

/* 
 * CSR3/4 Function Parameter Address
 */

#define	x_vs_pr1		x_vs_csr3
#define	x_vs_pr2		x_vs_csr4

struct x_vs_fparm{
    union{
	struct{
	    x_u_short x__plow;	/* low 16 bits of address */
	    x_u_short x__phigh;	/* high 16 bits of address */
	}x__parts;
	x_caddr_t x__pall;
    }x__X;
};
#define	x_fparm_low	x__X.x__parts.x__plow
#define	x_fparm_high	x__X.x__parts.x__phigh
#define	x_fparm_all	x__X.x__pall

/* 
 * CSR5/6 - Cursor position
 */

#define	x_vs_cxr		x_vs_csr5
#define	x_vs_cyr		x_vs_csr6


/* 
 * CSR 7 - Interrupt vector in fiber cable machines
 */

#define	x_vs_ivr		x_vs_csr7

/* 
 * CSR 8 through 14 Spare
 */

#define	x_vs_spr2		x_vs_csr8
#define	x_vs_spr3		x_vs_csr9
#define	x_vs_spr4		x_vs_csra
#define	x_vs_spr5		x_vs_csrb
#define	x_vs_spr6		x_vs_csrc
#define	x_vs_spr7		x_vs_csrd
#define	x_vs_spr8		x_vs_csre

/* 
 * CSR 15 - Interrupt vector in rev 2B
 */

#define	x_vs_ivr2		x_vs_csrf
