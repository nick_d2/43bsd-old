#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)dhreg.h	7.1 (Berkeley) 6/5/86
 */

/* 
 * DH-11 device register definitions.
 */
struct x_dhdevice {
	union {
		x_short	x_dhcsr;		/* control-status register */
		char	x_dhcsrl;		/* low byte for line select */
	} x_un;
	x_short	x_dhrcr;			/* receive character register */
	x_short	x_dhlpr;			/* line parameter register */
	x_u_short x_dhcar;			/* current address register */
	x_short	x_dhbcr;			/* byte count register */
	x_u_short	x_dhbar;			/* buffer active register */
	x_short	x_dhbreak;		/* break control register */
	x_short	x_dhsilo;			/* silo status register */
};

/* Bits in dhcsr */
#define	x_DH_TI	0100000		/* transmit interrupt */
#define	x_DH_SI	0040000		/* storage interrupt */
#define	x_DH_TIE	0020000		/* transmit interrupt enable */
#define	x_DH_SIE	0010000		/* storage interrupt enable */
#define	x_DH_MC	0004000		/* master clear */
#define	x_DH_NXM	0002000		/* non-existant memory */
#define	x_DH_MM	0001000		/* maintenance mode */
#define	x_DH_CNI	0000400		/* clear non-existant memory interrupt */
#define	x_DH_RI	0000200		/* receiver interrupt */
#define	x_DH_RIE	0000100		/* receiver interrupt enable */

/* Bits in dhlpr */
#define	x_BITS6	01
#define	x_BITS7	02
#define	x_BITS8	03
#define	x_TWOSB	04
#define	x_PENABLE	020
/* DEC manuals incorrectly say this bit causes generation of even parity. */
#define	x_OPAR	040
#define	x_HDUPLX	040000

#define	x_DH_IE	(x_DH_TIE|x_DH_SIE|x_DH_RIE)

/* Bits in dhrcr */
#define	x_DH_PE		0010000		/* parity error */
#define	x_DH_FE		0020000		/* framing error */
#define	x_DH_DO		0040000		/* data overrun */
