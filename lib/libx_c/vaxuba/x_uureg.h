#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)uureg.h	7.1 (Berkeley) 6/5/86
 */


/*
 * DL11-E/DL11-W UNIBUS (for TU58) controller registers
 */
struct x_uudevice {
	x_short	x_rcs;	/* receiver status register */
	x_short	x_rdb;	/* receiver data buffer register */
	x_short	x_tcs;	/* transmitter status register */
	x_short	x_tdb;	/* transmitter data buffer register */
};

/*
 * Receiver/transmitter status register status/command bits
 */
#define x_UUCS_DONE	0x80	/* done/ready */
#define	x_UUCS_READY	0x80
#define x_UUCS_INTR	0x40	/* interrupt enable */
#define	x_UUCS_MAINT	0x02	/* maintenance check (xmitter only) */
#define	x_UUCS_BREAK	0x01	/* send break (xmitter only) */

/*
 * Receiver data buffer register status bits
 */
#define	x_UURDB_ERROR	0x8000	/* Error (overrun or break) */
#define x_UURDB_ORUN	0x4000	/* Data overrun error */
#define	x_UURDB_BREAK	0x2000	/* TU58 break */

#define	x_UUDB_DMASK	0x00ff	/* data mask (send and receive data) */

