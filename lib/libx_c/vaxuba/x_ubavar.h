#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)ubavar.h	7.1 (Berkeley) 6/5/86
 */

/*
 * This file contains definitions related to the kernel structures
 * for dealing with the unibus adapters.
 *
 * Each uba has a uba_hd structure.
 * Each unibus controller which is not a device has a uba_ctlr structure.
 * Each unibus device has a uba_device structure.
 */

#ifndef x_LOCORE
/*
 * Per-uba structure.
 *
 * This structure holds the interrupt vector for the uba,
 * and its address in physical and virtual space.  At boot time
 * we determine the devices attached to the uba's and their
 * interrupt vectors, filling in uh_vec.  We free the map
 * register and bdp resources of the uba into the structures
 * defined here.
 *
 * During normal operation, resources are allocated and returned
 * to the structures here.  We watch the number of passive releases
 * on each uba, and if the number is excessive may reset the uba.
 * 
 * When uba resources are needed and not available, or if a device
 * which can tolerate no other uba activity (rk07) gets on the bus,
 * then device drivers may have to wait to get to the bus and are
 * queued here.  It is also possible for processes to block in
 * the unibus driver in resource wait (mrwant, bdpwant); these
 * wait states are also recorded here.
 */
struct	x_uba_hd {
	struct	x_uba_regs *x_uh_uba;	/* virt addr of uba */
	struct	x_uba_regs *x_uh_physuba;	/* phys addr of uba */
	x_int	(**x_uh_vec)();		/* interrupt vector */
	struct	x_uba_device *x_uh_actf;	/* head of queue to transfer */
	struct	x_uba_device *x_uh_actl;	/* tail of queue to transfer */
	x_short	x_uh_mrwant;		/* someone is waiting for map reg */
	x_short	x_uh_bdpwant;		/* someone awaits bdp's */
	x_int	x_uh_bdpfree;		/* free bdp's */
	x_int	x_uh_hangcnt;		/* number of ticks hung */
	x_int	x_uh_zvcnt;		/* number of recent 0 vectors */
	x_long	x_uh_zvtime;		/* time over which zvcnt accumulated */
	x_int	x_uh_zvtotal;		/* total number of 0 vectors */
	x_int	x_uh_errcnt;		/* number of errors */
	x_int	x_uh_lastiv;		/* last free interrupt vector */
	x_short	x_uh_users;		/* transient bdp use count */
	x_short	x_uh_xclu;		/* an rk07 is using this uba! */
	x_int	x_uh_lastmem;		/* limit of any unibus memory */
#define	x_UAMSIZ	100
	struct	x_map *x_uh_map;		/* buffered data path regs free */
};

#ifndef x_LOCORE
/*
 * Per-controller structure.
 * (E.g. one for each disk and tape controller, and other things
 * which use and release buffered data paths.)
 *
 * If a controller has devices attached, then there are
 * cross-referenced uba_drive structures.
 * This structure is the one which is queued in unibus resource wait,
 * and saves the information about unibus resources which are used.
 * The queue of devices waiting to transfer is also attached here.
 */
struct x_uba_ctlr {
	struct	x_uba_driver *x_um_driver;
	x_short	x_um_ctlr;	/* controller index in driver */
	x_short	x_um_ubanum;	/* the uba it is on */
	x_short	x_um_alive;	/* controller exists */
	x_int	(**x_um_intr)();	/* interrupt handler(s) */
	x_caddr_t	x_um_addr;	/* address of device in i/o space */
	struct	x_uba_hd *x_um_hd;
/* the driver saves the prototype command here for use in its go routine */
	x_int	x_um_cmd;		/* communication to dgo() */
	x_int	x_um_ubinfo;	/* save unibus registers, etc */
	struct	x_buf x_um_tab;	/* queue of devices for this controller */
};

/*
 * Per ``device'' structure.
 * (A controller has devices or uses and releases buffered data paths).
 * (Everything else is a ``device''.)
 *
 * If a controller has many drives attached, then there will
 * be several uba_device structures associated with a single uba_ctlr
 * structure.
 *
 * This structure contains all the information necessary to run
 * a unibus device such as a dz or a dh.  It also contains information
 * for slaves of unibus controllers as to which device on the slave
 * this is.  A flags field here can also be given in the system specification
 * and is used to tell which dz lines are hard wired or other device
 * specific parameters.
 */
struct x_uba_device {
	struct	x_uba_driver *x_ui_driver;
	x_short	x_ui_unit;	/* unit number on the system */
	x_short	x_ui_ctlr;	/* mass ctlr number; -1 if none */
	x_short	x_ui_ubanum;	/* the uba it is on */
	x_short	x_ui_slave;	/* slave on controller */
	x_int	(**x_ui_intr)();	/* interrupt handler(s) */
	x_caddr_t	x_ui_addr;	/* address of device in i/o space */
	x_short	x_ui_dk;		/* if init 1 set to number for iostat */
	x_int	x_ui_flags;	/* parameter from system specification */
	x_short	x_ui_alive;	/* device exists */
	x_short	x_ui_type;	/* driver specific type information */
	x_caddr_t	x_ui_physaddr;	/* phys addr, for standalone (dump) code */
/* this is the forward link in a list of devices on a controller */
	struct	x_uba_device *x_ui_forw;
/* if the device is connected to a controller, this is the controller */
	struct	x_uba_ctlr *x_ui_mi;
	struct	x_uba_hd *x_ui_hd;
};
#endif

/*
 * Per-driver structure.
 *
 * Each unibus driver defines entries for a set of routines
 * as well as an array of types which are acceptable to it.
 * These are used at boot time by the configuration program.
 */
struct x_uba_driver {
	x_int	(*x_ud_probe)();		/* see if a driver is really there */
	x_int	(*x_ud_slave)();		/* see if a slave is there */
	x_int	(*x_ud_attach)();		/* setup driver for a slave */
	x_int	(*x_ud_dgo)();		/* fill csr/ba to start transfer */
	x_u_short	*x_ud_addr;		/* device csr addresses */
	char	*x_ud_dname;		/* name of a device */
	struct	x_uba_device **x_ud_dinfo;	/* backpointers to ubdinit structs */
	char	*x_ud_mname;		/* name of a controller */
	struct	x_uba_ctlr **x_ud_minfo;	/* backpointers to ubminit structs */
	x_short	x_ud_xclu;		/* want exclusive use of bdp's */
	x_int	(*x_ud_ubamem)();		/* see if dedicated memory is present */
};
#endif

/*
 * Flags to UBA map/bdp allocation routines
 */
#define	x_UBA_NEEDBDP	0x01		/* transfer needs a bdp */
#define	x_UBA_CANTWAIT	0x02		/* don't block me */
#define	x_UBA_NEED16	0x04		/* need 16 bit addresses only */
#define	x_UBA_HAVEBDP	0x08		/* use bdp specified in high bits */

/*
 * Macros to bust return word from map allocation routines.
 */
#define	x_UBAI_BDP(x_i)	((x_int)(((unsigned)(x_i))>>28))
#define	x_UBAI_NMR(x_i)	((x_int)((x_i)>>18)&0x3ff)
#define	x_UBAI_MR(x_i)	((x_int)((x_i)>>9)&0x1ff)
#define	x_UBAI_BOFF(x_i)	((x_int)((x_i)&0x1ff))
#define	x_UBAI_ADDR(x_i)	((x_int)((x_i)&0x3ffff))	/* uba addr (boff+mr) */

#ifndef x_LOCORE
#ifdef x_KERNEL
/*
 * UBA related kernel variables
 */
x_int	x_numuba;					/* number of uba's */
struct	x_uba_hd x_uba_hd[];

/*
 * Ubminit and ubdinit initialize the mass storage controller and
 * device tables specifying possible devices.
 */
extern	struct	x_uba_ctlr x_ubminit[];
extern	struct	x_uba_device x_ubdinit[];

/*
 * UNIbus device address space is mapped by UMEMmap
 * into virtual address umem[][].
 */
extern	struct x_pte x_UMEMmap[][512];	/* uba device addr pte's */
extern	char x_umem[][512*x_NBPG];		/* uba device addr space */

/*
 * Since some VAXen vector their unibus interrupts
 * just adjacent to the system control block, we must
 * allocate space there when running on ``any'' cpu.  This space is
 * used for the vectors for uba0 and uba1 on all cpu's.
 */
extern	x_int (*x_UNIvec[])();			/* unibus vec for uba0 */
#if x_NUBA > 1
extern	x_int (*x_UNI1vec[])();			/* unibus vec for uba1 */
#endif

#if defined(x_VAX780) || defined(x_VAX8600)
/*
 * On 780's, we must set the scb vectors for the nexus of the
 * UNIbus adaptors to vector to locore unibus adaptor interrupt dispatchers
 * which make 780's look like the other VAXen.
 */
extern	x_Xua0int(), x_Xua1int(), x_Xua2int(), x_Xua3int();
#endif
#endif
#endif
