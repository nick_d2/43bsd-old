#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)dmreg.h	7.1 (Berkeley) 6/5/86
 */

/*
 * DM-11 device register definitions.
 */
struct x_dmdevice {
	x_short	x_dmcsr;		/* control status register */
	x_short	x_dmlstat;	/* line status register */
	x_short	x_dmpad1[2];
};

/* bits in dm csr */
#define	x_DM_RF		0100000		/* ring flag */
#define	x_DM_CF		0040000		/* carrier flag */
#define	x_DM_CTS		0020000		/* clear to send */
#define	x_DM_SRF		0010000		/* secondary receive flag */
#define	x_DM_CS		0004000		/* clear scan */
#define	x_DM_CM		0002000		/* clear multiplexor */
#define	x_DM_MM		0001000		/* maintenance mode */
#define	x_DM_STP		0000400		/* step */
#define	x_DM_DONE		0000200		/* scanner is done */
#define	x_DM_IE		0000100		/* interrupt enable */
#define	x_DM_SE		0000040		/* scan enable */
#define	x_DM_BUSY		0000020		/* scan busy */

/* bits in dm lsr */
#define	x_DML_RNG		0000200		/* ring */
#define	x_DML_CAR		0000100		/* carrier detect */
#define	x_DML_CTS		0000040		/* clear to send */
#define	x_DML_SR		0000020		/* secondary receive */
#define	x_DML_ST		0000010		/* secondary transmit */
#define	x_DML_RTS		0000004		/* request to send */
#define	x_DML_DTR		0000002		/* data terminal ready */
#define	x_DML_LE		0000001		/* line enable */

#define	x_DML_ON		(x_DML_DTR|x_DML_RTS|x_DML_LE)
#define	x_DML_OFF		(x_DML_LE)
