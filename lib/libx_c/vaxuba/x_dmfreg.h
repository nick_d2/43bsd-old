#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)dmfreg.h	7.1 (Berkeley) 6/5/86
 */

/*
 * DMF-32 definitions.
 */

/*
 * "dmf" (unqualified) refers to the async portion of the dmf32,
 * "dmfc" to the combo portion,
 * "dmfs" to the sync portion,
 * "dmfl" to the lp portion, and
 * "dmfd" to the dr portion.
 */
struct x_dmfdevice {
	x_short	x_dmfccsr0;		/* combo csr 0 */
	x_short	x_dmfccsr1;		/* combo csr 1 */
	x_short	x_dmfs[4];
	x_short	x_dmfcsr;			/* control-status register */
	x_short	x_dmflpr;			/* line parameter register */
	x_short	x_dmfrbuf;		/* receiver buffer (ro) */
	union {
		x_u_short	x_dmfirw;		/* indirect register word */
		x_u_char	x_dmfirc[2];	/*    "         "    bytes */
	} x_dmfun;
	x_short	x_dmfl_ctrl;	/* line printer control register */
	x_short	x_dmfl_indrct;	/* line printer indirect register */
	x_short	x_dmfd[4];	/* for dr11 (not implemented) */
};

#define	x_dmfrsp	x_dmfrbuf		/* receive silo parameter register (wo) */
#define	x_dmftbuf	x_dmfun.x_dmfirc[0]	/* transmit buffer */
#define	x_dmftsc	x_dmfun.x_dmfirc[0]	/* transmit silo count */
#define	x_dmfrms	x_dmfun.x_dmfirc[1]	/* receive modem status */
#define	x_dmflctms x_dmfun.x_dmfirw	/* line control, transmit modem status */
#define	x_dmftba	x_dmfun.x_dmfirw	/* transmit buffer address */
#define	x_dmftcc	x_dmfun.x_dmfirw	/* transmit character count */

/* bits in dmfcsr */
#define	x_DMF_TI	0100000		/* transmit interrupt */
#define	x_DMF_TIE	0040000		/* transmit interrupt enable */
#define	x_DMF_NXM	0020000		/* non-existant memory */
#define	x_DMF_LIN	0003400		/* transmit line number */
#define	x_DMF_RI	0000200		/* receiver interrupt */
#define	x_DMF_RIE	0000100		/* receiver interrupt enable */
#define	x_DMF_CLR	0000040		/* master reset */
#define	x_DMF_IAD	0000037		/* indirect address register */

#define	x_DMFIR_TBUF	000	/* select tbuf indirect register */
#define	x_DMFIR_LCR	010	/* select lcr indirect register */
#define	x_DMFIR_TBA	020	/* select tba indirect register */
#define	x_DMFIR_TCC	030	/* select tcc indirect register */

/* bits in dmflpr */
#define	x_BITS6	(01<<3)
#define	x_BITS7	(02<<3)
#define	x_BITS8	(03<<3)
#define	x_TWOSB	0200
#define	x_PENABLE	040
#define	x_EPAR	0100

#define	x_DMF_IE	(x_DMF_TIE|x_DMF_RIE)

#define	x_DMF_SILOCNT	32		/* size of DMF output silo (per line) */

/* bits in dmfrbuf */
#define	x_DMF_DSC		0004000		/* data set change */
#define	x_DMF_PE		0010000		/* parity error */
#define	x_DMF_FE		0020000		/* framing error */
#define	x_DMF_DO		0040000		/* data overrun */

/* bits in dmfrms */
#define	x_DMF_USRR	0004		/* user modem signal (pin 25) */
#define	x_DMF_SR		0010		/* secondary receive */
#define	x_DMF_CTS		0020		/* clear to send */
#define	x_DMF_CAR		0040		/* carrier detect */
#define	x_DMF_RNG		0100		/* ring */
#define	x_DMF_DSR		0200		/* data set ready */

/* bits in dmftms */
#define	x_DMF_USRW	0001		/* user modem signal (pin 18) */
#define	x_DMF_DTR		0002		/* data terminal ready */
#define	x_DMF_RATE	0004		/* data signal rate select */
#define	x_DMF_ST		0010		/* secondary transmit */
#define	x_DMF_RTS		0020		/* request to send */
#define	x_DMF_BRK		0040		/* pseudo break bit */
#define	x_DMF_PREEMPT	0200		/* preempt output */

/* flags for modem control */
#define	x_DMF_ON	(x_DMF_DTR|x_DMF_RTS)
#define	x_DMF_OFF	0

/* bits in dmflctms */
#define	x_DMF_MIE		0040		/* modem interrupt enable */
#define	x_DMF_FLUSH	0020		/* flush transmit silo */
#define	x_DMF_RBRK	0010		/* real break bit */
#define	x_DMF_RE		0004		/* receive enable */
#define	x_DMF_AUTOX	0002		/* auto XON/XOFF */
#define	x_DMF_TE		0001		/* transmit enable */

#define	x_DMFLCR_ENA	(x_DMF_MIE|x_DMF_RE|x_DMF_TE)

/* bits in dm lsr, copied from dh.c */
#define	x_DML_USR		0001000		/* usr modem sig, not a real DM bit */
#define	x_DML_DSR		0000400		/* data set ready, not a real DM bit */
#define	x_DML_RNG		0000200		/* ring */
#define	x_DML_CAR		0000100		/* carrier detect */
#define	x_DML_CTS		0000040		/* clear to send */
#define	x_DML_SR		0000020		/* secondary receive */
#define	x_DML_ST		0000010		/* secondary transmit */
#define	x_DML_RTS		0000004		/* request to send */
#define	x_DML_DTR		0000002		/* data terminal ready */
#define	x_DML_LE		0000001		/* line enable */

/* dmf line printer csr def */
#define x_DMFL_PEN	(1<<0)		/* print enable */
#define x_DMFL_RESET	(1<<1)		/* master reset */
#define x_DMFL_FORMAT	(1<<2)		/* format control */
#define x_DMFL_UNUSED	(3<<3)
#define x_DMFL_MAINT	(1<<5)		/* maintenance mode on */
#define x_DMFL_IE		(1<<6)		/* intr enable */
#define x_DMFL_PDONE	(1<<7)		/* print done bit */
#define x_DMFL_INDIR	(7<<8)		/* indirect reg */
#define x_DMFL_UNUSED2	(1<<11)
#define x_DMFL_CONV	(1<<12)		/* connect verify */
#define	x_DMFL_DAVRDY	(1<<13)		/* davfu ready */
#define x_DMFL_OFFLINE	(1<<14)		/* printer offline */
#define x_DMFL_DMAERR	(1<<15)		/* dma error bit */
#define x_DMFL_BUFSIZ	512		/* max chars per dma */
#define x_DMFL_DEFCOLS	132		/* default # of cols/line <=255 */
#define x_DMFL_DEFLINES	66		/* default # of lines/page <=255 */
#define x_DMFL_OPTIONS	((1 << 8) | (1 << 9) | (1 << 15))
					/* auto cr, real ff, no lower to upper */

/*
 *  Bits in the configuration register
 */
#define x_DMFC_CONFMASK	0xf000		/* picks off the configuration bits */
#define	x_DMFC_DR		0x1000		/* DR11 parallel interface */
#define x_DMFC_LP		0x2000		/* LP dma parallel lineprinter i'face */
#define x_DMFC_SYNC	0x4000		/* Synchronous serial interface */
#define x_DMFC_ASYNC	0x8000		/* 8 Serial ports */
