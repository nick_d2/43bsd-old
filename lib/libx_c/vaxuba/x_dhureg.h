#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)dhureg.h	7.1 (Berkeley) 6/5/86
 */

/* 
 * DHU-11 device register definitions.
 */
struct x_dhudevice {
	union {
		x_short	x_csr;		/* control-status register */
		struct {
			char	x_csrl;	/* low byte for line select */
			char	x_csrh;	/* high byte for tx line */
		} x_cb;
	} x_un1;
#define	x_dhucsr	x_un1.x_csr
#define	x_dhucsrl	x_un1.x_cb.x_csrl
#define	x_dhucsrh	x_un1.x_cb.x_csrh
	union {
		x_short	x_rbuf;		/* recv.char/ds.change register (R) */
		x_short	x_timo;		/* delay between recv -> intr (W) */
	} x_un2;
#define	x_dhurbuf	x_un2.x_rbuf
#define	x_dhutimo	x_un2.x_timo
	x_short	x_dhulpr;			/* line parameter register */
	union {
		char	x_fbyte[1];	/* fifo data byte (low byte only) (W) */
		x_short	x_fdata;		/* fifo data word (W) */
		char	x_sbyte[2];	/* line status/fifo size (R) */
	} x_un3;
#define	x_dhubyte	x_un3.x_fbyte[0]
#define x_dhufifo	x_un3.x_fdata
#define x_dhusize	x_un3.x_sbyte[0]
#define x_dhustat	x_un3.x_sbyte[1]
	x_short	x_dhulcr;			/* line control register */
	x_short	x_dhubar1;		/* buffer address register 1 */
	char	x_dhubar2;		/* buffer address register 2 */
	char	x_dhulcr2;		/* xmit enable bit */
	x_short	x_dhubcr;			/* buffer count register */
};

/* Bits in dhucsr */
#define	x_DHU_CS_TIE	0x4000		/* transmit interrupt enable */
#define	x_DHU_CS_DFAIL	0x2000		/* diagnostic fail */
#define	x_DHU_CS_RI	0x0080		/* receiver interrupt */
#define	x_DHU_CS_RIE	0x0040		/* receiver interrupt enable */
#define	x_DHU_CS_MCLR	0x0020		/* master clear */
#define	x_DHU_CS_SST	0x0010		/* skip self test (with DHU_CS_MCLR) */
#define	x_DHU_CS_IAP	0x000f		/* indirect address pointer */

#define	x_DHU_IE	(x_DHU_CS_TIE|x_DHU_CS_RIE)

/* map unit into iap register select */
#define x_DHU_SELECT(x_unit)	((x_unit) & x_DHU_CS_IAP)

/* Transmitter bits in high byte of dhucsr */
#define	x_DHU_CSH_TI	0x80		/* transmit interrupt */
#define	x_DHU_CSH_NXM	0x10		/* transmit dma err: non-exist-mem */
#define	x_DHU_CSH_TLN	0x0f		/* transmit line number */

/* map csrh line bits into line */
#define	x_DHU_TX_LINE(x_csrh)	((x_csrh) & x_DHU_CSH_TLN)

/* Bits in dhurbuf */
#define	x_DHU_RB_VALID	0x8000		/* data valid */
#define	x_DHU_RB_STAT	0x7000		/* status bits */
#define	x_DHU_RB_DO	0x4000		/* data overrun */
#define	x_DHU_RB_FE	0x2000		/* framing error */
#define	x_DHU_RB_PE	0x1000		/* parity error */
#define	x_DHU_RB_RLN	0x0f00		/* receive line number */
#define	x_DHU_RB_RDS	0x00ff		/* receive data/status */
#define x_DHU_RB_DIAG	0x0001		/* if DHU_RB_STAT -> diag vs modem */

/* map rbuf line bits into line */
#define	x_DHU_RX_LINE(x_rbuf)	(((x_rbuf) & x_DHU_RB_RLN) >> 8)

/* Bits in dhulpr */
#define	x_DHU_LP_TSPEED	0xf000
#define	x_DHU_LP_RSPEED	0x0f00
#define	x_DHU_LP_TWOSB	0x0080
#define	x_DHU_LP_EPAR	0x0040
#define	x_DHU_LP_PENABLE	0x0020
#define	x_DHU_LP_BITS8	0x0018
#define	x_DHU_LP_BITS7	0x0010
#define	x_DHU_LP_BITS6	0x0008

/* Bits in dhustat */
#define	x_DHU_ST_DSR	0x80		/* data set ready */
#define	x_DHU_ST_RI	0x20		/* ring indicator */
#define	x_DHU_ST_DCD	0x10		/* carrier detect */
#define	x_DHU_ST_CTS	0x04		/* clear to send */
#define	x_DHU_ST_DHU	0x01		/* always one on a dhu, zero on dhv */

/* Bits in dhulcr */
#define	x_DHU_LC_RTS	0x1000		/* request to send */
#define	x_DHU_LC_DTR	0x0200		/* data terminal ready */
#define	x_DHU_LC_MODEM	0x0100		/* modem control enable */
#define	x_DHU_LC_MAINT	0x00c0		/* maintenance mode */
#define	x_DHU_LC_FXOFF	0x0020		/* force xoff */
#define	x_DHU_LC_OAUTOF	0x0010		/* output auto flow */
#define	x_DHU_LC_BREAK	0x0008		/* break control */
#define	x_DHU_LC_RXEN	0x0004		/* receiver enable */
#define	x_DHU_LC_IAUTOF	0x0002		/* input auto flow */
#define	x_DHU_LC_TXABORT	0x0001		/* transmitter abort */

/* Bits in dhulcr2 */
#define	x_DHU_LC2_TXEN	0x80		/* transmitter enable */

/* Bits in dhubar2 */
#define	x_DHU_BA2_DMAGO	0x80		/* transmit dma start */
#define	x_DHU_BA2_XBA	0x03		/* top two bits of dma address */
#define x_DHU_XBA_SHIFT	16		/* amount to shift xba bits */

/* Bits for dhumctl only:  stat bits are shifted up 16 */
#define	x_DHU_ON	(x_DHU_LC_DTR|x_DHU_LC_RTS|x_DHU_LC_MODEM)
#define	x_DHU_OFF	x_DHU_LC_MODEM

#define	x_DHU_DSR	(x_DHU_ST_DSR << 16)
#define	x_DHU_RNG	(x_DHU_ST_RI << 16)
#define	x_DHU_CAR	(x_DHU_ST_DCD << 16)
#define	x_DHU_CTS	(x_DHU_ST_CTS << 16)

#define	x_DHU_RTS	x_DHU_LC_RTS
#define	x_DHU_DTR	x_DHU_LC_DTR
#define x_DHU_BRK	x_DHU_LC_BREAK
#define x_DHU_LE	x_DHU_LC_MODEM

/* bits in dm lsr, copied from dmreg.h */
#define	x_DML_DSR		0000400		/* data set ready, not a real DM bit */
#define	x_DML_RNG		0000200		/* ring */
#define	x_DML_CAR		0000100		/* carrier detect */
#define	x_DML_CTS		0000040		/* clear to send */
#define	x_DML_SR		0000020		/* secondary receive */
#define	x_DML_ST		0000010		/* secondary transmit */
#define	x_DML_RTS		0000004		/* request to send */
#define	x_DML_DTR		0000002		/* data terminal ready */
#define	x_DML_LE		0000001		/* line enable */
