#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)rkreg.h	7.1 (Berkeley) 6/5/86
 */

#define x_NRK7CYL 	815
#define	x_NRK6CYL		411
#define x_NRKSECT		22
#define x_NRKTRK		3

struct x_rkdevice
{
	x_short	x_rkcs1;		/* control status reg 1 */
	x_short	x_rkwc;		/* word count */
	x_u_short	x_rkba;		/* bus address */
	x_short	x_rkda;		/* disk address */
	x_short	x_rkcs2;		/* control status reg 2 */
	x_short	x_rkds;		/* drive status */
	x_short	x_rker;		/* driver error register */
	x_short	x_rkatt;		/* attention status/offset register */
	x_short	x_rkcyl;		/* current cylinder register */
	x_short	x_rkxxx;
	x_short	x_rkdb;		/* data buffer register */
	x_short	x_rkmr1;		/* maint reg 1 */
	x_short	x_rkec1;		/* burst error bit position */
	x_short	x_rkec2;		/* burst error bit pattern */
	x_short	x_rkmr2;		/* maint reg 2 */
	x_short	x_rkmr3;		/* maint reg 3 */
};

/* rkcs1 */
#define x_RK_CCLR		0100000		/* controller clear (also error) */
#define	x_RK_CERR		x_RK_CCLR
#define	x_RK_DI		0040000		/* drive interrupt */
#define	x_RK_DTCPAR	0020000		/* drive to controller parity */
#define	x_RK_CFMT		0010000		/* 18 bit word format */
#define	x_RK_CTO		0004000		/* controller timeout */
#define	x_RK_CDT		0002000		/* drive type (rk07/rk06) */
/* bits 8 and 9 are the extended bus address */
#define	x_RK_CRDY		0000200		/* controller ready */
#define	x_RK_IE		0000100		/* interrupt enable */
/* bits 1 to 4 are the function code */
#define	x_RK_GO		0000001

/* commands */
#define x_RK_SELECT	000		/* select drive */
#define x_RK_PACK		002		/* pack acknowledge */
#define x_RK_DCLR		004		/* drive clear */
#define	x_RK_UNLOAD	006		/* unload */
#define	x_RK_START	010		/* start spindle */
#define	x_RK_RECAL	012		/* recalibrate */
#define	x_RK_OFFSET	014		/* offset */
#define	x_RK_SEEK		016		/* seek */
#define	x_RK_READ		020		/* read data */
#define	x_RK_WRITE	022		/* write data */
#define	x_RK_RHDR		026		/* read header */
#define	x_RK_WHDR		030		/* write header */

/* rkcs2 */
#define	x_RKCS2_DLT	0100000		/* data late */
#define	x_RKCS2_WCE	0040000		/* write check */
#define	x_RKCS2_UPE	0020000		/* unibus parity */
#define	x_RKCS2_NED	0010000		/* non-existant drive */
#define	x_RKCS2_NEM	0004000		/* non-existant memory */
#define	x_RKCS2_PGE	0002000		/* programming error */
#define	x_RKCS2_MDS	0001000		/* multiple drive select */
#define	x_RKCS2_UFE	0000400		/* unit field error */
#define	x_RKCS2_OR	0000200		/* output ready */
#define	x_RKCS2_IR	0000100		/* input ready */
#define	x_RKCS2_SCLR	0000040		/* subsystem clear */
#define	x_RKCS2_BAI	0000020		/* bus address increment inhibit */
#define	x_RKCS2_RLS	0000010		/* release */
/* bits 0-2 are drive select */

#define	x_RKCS2_BITS \
"\10\20DLT\17WCE\16UPE\15NED\14NEM\13PGE\12MDS\11UFE\
\10x_OR\7x_IR\6x_SCLR\5x_BAI\4x_RLS"

#define	x_RKCS2_HARD		(x_RKCS2_NED|x_RKCS2_PGE)

/* rkds */
#define	x_RKDS_SVAL	0100000		/* status valid */
#define	x_RKDS_CDA	0040000		/* current drive attention */
#define	x_RKDS_PIP	0020000		/* positioning in progress */
/* bit 12 is spare */
#define	x_RKDS_WRL	0004000		/* write lock */
/* bits 9 and 10 are spare */
#define	x_RKDS_DDT	0000400		/* disk drive type */
#define	x_RKDS_DRDY	0000200		/* drive ready */
#define	x_RKDS_VV		0000100		/* volume valid */
#define	x_RKDS_DROT	0000040		/* drive off track */
#define	x_RKDS_SPLS	0000020		/* speed loss */
#define	x_RKDS_ACLO	0000010		/* ac low */
#define	x_RKDS_OFF	0000004		/* offset mode */
#define	x_RKDS_DRA	0000001		/* drive available */

#define	x_RKDS_DREADY	(x_RKDS_DRA|x_RKDS_VV|x_RKDS_DRDY)
#define	x_RKDS_BITS \
"\10\20SVAL\17CDA\16PIP\14WRL\11DDT\
\10Dx_RDY\7x_VV\6Dx_ROT\5x_SPLS\4x_ACLO\3x_OFF\1Dx_RA"
#define	x_RKDS_HARD	(x_RKDS_ACLO|x_RKDS_SPLS)

/* rker */
#define	x_RKER_DCK	0100000		/* data check */
#define	x_RKER_UNS	0040000		/* drive unsafe */
#define	x_RKER_OPI	0020000		/* operation incomplete */
#define	x_RKER_DTE	0010000		/* drive timing error */
#define	x_RKER_WLE	0004000		/* write lock error */
#define	x_RKER_IDAE	0002000		/* invalid disk address error */
#define	x_RKER_COE	0001000		/* cylinder overflow error */
#define	x_RKER_HRVC	0000400		/* header vertical redundancy check */
#define	x_RKER_BSE	0000200		/* bad sector error */
#define	x_RKER_ECH	0000100		/* hard ecc error */
#define	x_RKER_DTYE	0000040		/* drive type error */
#define	x_RKER_FMTE	0000020		/* format error */
#define	x_RKER_DRPAR	0000010		/* control-to-drive parity error */
#define	x_RKER_NXF	0000004		/* non-executable function */
#define	x_RKER_SKI	0000002		/* seek incomplete */
#define	x_RKER_ILF		0000001		/* illegal function */

#define	x_RKER_BITS \
"\10\20DCK\17UNS\16OPI\15DTE\14WLE\13IDAE\12COE\11HRVC\
\10x_BSE\7Ex_CH\6Dx_TYE\5x_FMTE\4Dx_RPAR\3x_NXF\2x_SKI\1x_ILF"
#define	x_RKER_HARD	\
	(x_RKER_WLE|x_RKER_IDAE|x_RKER_COE|x_RKER_DTYE|x_RKER_FMTE|x_RKER_ILF)

/* offset bits in rkas */
#define	x_RKAS_P400	0020		/*  +400 RK06,  +200 RK07 */
#define	x_RKAS_M400	0220		/*  -400 RK06,  -200 RK07 */
#define	x_RKAS_P800	0040		/*  +800 RK06,  +400 RK07 */
#define	x_RKAS_M800	0240		/*  -800 RK06,  -400 RK07 */
#define	x_RKAS_P1200	0060		/*  +800 RK06,  +400 RK07 */
#define	x_RKAS_M1200	0260		/* -1200 RK06, -1200 RK07 */
