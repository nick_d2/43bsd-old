#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)rlreg.h	7.1 (Berkeley) 6/5/86
 */

struct x_rldevice {
	x_short	x_rlcs;		/* control status */
	x_u_short	x_rlba;		/* bus address */
	union {			/* disk address */
		x_u_short	x_seek;		/* disk seek address */
		x_u_short	x_rw;		/* disk read/write address */
		x_u_short	x_getstat;	/* get disk status command */
	} x_rlda;
	union {			/* multi-purpose register */
		x_u_short	x_getstat;	/* get status */
		x_u_short x_readhdr;	/* read header */
		x_u_short	x_rw;		/* read/write word count */
	} x_rlmp;
};

#define	x_NRLCYLN		512	/* number of cylinders per disk */
#define x_NRLTRKS		2	/* number of tracks per cylinder */
#define x_NRLSECT		40	/* number of sectors per track */
#define x_NRLBPSC		256	/* bytes per sector */

/* rlcs */
/* commands */
#define x_RL_NOOP		0000000		/* no-operation */
#define x_RL_WCHECK	0000002		/* write check */
#define x_RL_GETSTAT	0000004		/* get status */
#define	x_RL_SEEK		0000006		/* seek */
#define	x_RL_RHDR		0000010		/* read header */
#define	x_RL_WRITE	0000012		/* write data */
#define	x_RL_READ		0000014		/* read data */
#define	x_RL_RDNCK	0000016		/* read data without hdr check */

#define x_RL_DRDY		0000001		/* When set indicates drive ready */
#define x_RL_BAE		0000060		/* UNIBUS address bits 16 & 17 */
#define	x_RL_IE		0000100		/* interrupt enable */
#define	x_RL_CRDY		0000200		/* controller ready */
#define x_RL_DS0		0000400		/* drive select 0 */
#define x_RL_DS1		0001000		/* drive select 1 */
#define	x_RL_OPI		0002000		/* operation incomplete */
#define	x_RL_DCRC		0004000		/* CRC error occurred */
#define	x_RL_DLT		0010000		/* data late or header not found */
#define	x_RL_NXM		0020000		/* non-existant memory */
#define	x_RL_DE		0040000		/* selected drive flagged an error */
#define	x_RL_ERR		0100000		/* composite error */

#define	x_RL_DCRDY	(x_RL_DRDY | x_RL_CRDY)

#define	x_RLCS_BITS \
"\10\20ERR\17DE\16NXM\15DLT\14DCRC\13OPI\1DRDY"

/* da_seek */
#define	x_RLDA_LOW	0000001		/* lower cylinder seek */
#define	x_RLDA_HGH	0000005		/* higher cylinder seek */
#define	x_RLDA_HSU	0000000		/* upper head select */
#define	x_RLDA_HSL	0000020		/* lower head select */
#define	x_RLDA_CA		0177600		/* cylinder address */

/* da_rw */
#define	x_RLDA_SA		0000077		/* sector address */
#define x_RLDA_HST	0000000		/* upper head select */
#define	x_RLDA_HSB	0000100		/* lower head select */

/* da_getstat */

#define	x_RL_GSTAT	0000003		/* Get status */
#define	x_RL_RESET	0000013		/* get status with reset */

/* mp_getstat */
#define	x_RLMP_STA	0000001		/* drive state: load cartridge */
#define	x_RLMP_STB	0000002		/* drive state: brush cycle */
#define	x_RLMP_STC	0000004		/* drive state: seek */
#define	x_RLMP_BH		0000010		/* set when brushes are home */
#define	x_RLMP_HO		0000020		/* set when brushes over the disk */
#define	x_RLMP_CO		0000040		/* set when cover open */
#define	x_RLMP_HS		0000100		/* indicates selected head:
						0 upper head
						1 lower head */
#define	x_RLMP_DT		0000200		/* indicates drive type:
						0 RL01
						1 RL02 */
#define	x_RLMP_DSE	0000400		/* set on multiple drive selection */
#define	x_RLMP_VC		0001000		/* set on pack mounted and spining */
#define	x_RLMP_WGE	0002000		/* write gate error */
#define	x_RLMP_SPE	0004000		/* spin speed error */
#define	x_RLMP_SKTO	0010000		/*\* seek time out error */
#define x_RLMP_WL		0020000		/* set on protected drive */
#define x_RLMP_CHE	0040000		/* current head error */
#define x_RLMP_WDE	0100000		/* write data error */

/* mp_rhc */
#define	x_RLMP_SA		0000077		/* sector address */
#define	x_RLMP_CA		0177600		/* cylinder address */

/* check these bits after a get status and reset */
#define x_RLMP_STATUS (x_RLMP_WDE|x_RLMP_CHE|x_RLMP_SKTO|x_RLMP_SPE|x_RLMP_WGE \
	|x_RLMP_VC|x_RLMP_DSE|x_RLMP_CO|x_RLMP_HO|x_RLMP_BH|x_RLMP_STC|x_RLMP_STB|x_RLMP_STA)

/* these are the bits that should be on in the above check */
#define x_RLMP_STATOK (x_RLMP_HO|x_RLMP_BH|x_RLMP_STC|x_RLMP_STA)

/* mp_rw */
#define	x_RLMP_WC		0017777		/* word count 2's complement */

#define	x_RLER_BITS \
"\10\20WDE\17CHE\16WL\15SKTO\14SPE\13WGE\12VC\11DSE\
\10Dx_T\7x_HS\6x_CO\5x_HO\4x_BH\3x_STC\2x_STB\1x_STA"
