#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)tmreg.h	7.1 (Berkeley) 6/5/86
 */

/*
 * TM11 controller registers
 */
struct x_tmdevice {
	x_u_short	x_tmer;		/* error register, per drive */
	x_u_short	x_tmcs;		/* control-status register */
	x_short	x_tmbc;		/* byte/frame count */
	x_u_short x_tmba;		/* address */
	x_short	x_tmdb;		/* data buffer */
	x_short	x_tmrd;		/* read lines */
	x_short	x_tmmr;		/* maintenance register */
#ifdef	x_AVIV
	x_short	x_tmfsr;		/* formatter status reading */
#endif
};

#define	x_b_repcnt  x_b_bcount
#define	x_b_command x_b_resid

/* bits in tmcs */
#define	x_TM_GO		0000001
#define	x_TM_OFFL		0000000		/* offline */
#define	x_TM_RCOM		0000002		/* read */
#define	x_TM_WCOM		0000004		/* write */
#define	x_TM_WEOF		0000006		/* write-eof */
#define	x_TM_SFORW	0000010		/* space forward */
#define	x_TM_SREV		0000012		/* space backwards */
#define	x_TM_WIRG		0000014		/* write with xtra interrecord gap */
#define	x_TM_REW		0000016		/* rewind */
#define	x_TM_SENSE	x_TM_IE		/* sense (internal to driver) */

#define	x_tmreverseop(x_cmd)		((x_cmd)==x_TM_SREV || (x_cmd)==x_TM_REW)

/* TM_SNS is a pseudo-op used to get tape status */
#define	x_TM_IE		0000100		/* interrupt enable */
#define	x_TM_CUR		0000200		/* control unit is ready */
#define	x_TM_DCLR		0010000		/* drive clear */
#define	x_TM_D800		0060000		/* select 800 bpi density */
#define	x_TM_ERR		0100000		/* drive error summary */

/* bits in tmer */
#define	x_TMER_ILC	0100000		/* illegal command */
#define	x_TMER_EOF	0040000		/* end of file */
#define	x_TMER_CRE	0020000		/* cyclic redundancy error */
#define	x_TMER_PAE	0010000		/* parity error */
#define	x_TMER_BGL	0004000		/* bus grant late */
#define	x_TMER_EOT	0002000		/* at end of tape */
#define	x_TMER_RLE	0001000		/* record length error */
#define	x_TMER_BTE	0000400		/* bad tape error */
#define	x_TMER_NXM	0000200		/* non-existant memory */
#define	x_TMER_SELR	0000100		/* tape unit properly selected */
#define	x_TMER_BOT	0000040		/* at beginning of tape */
#define	x_TMER_CH7	0000020		/* 7 channel tape */
#define	x_TMER_SDWN	0000010		/* gap settling down */
#define	x_TMER_WRL	0000004		/* tape unit write protected */
#define	x_TMER_RWS	0000002		/* tape unit rewinding */
#define	x_TMER_TUR	0000001		/* tape unit ready */

#define	x_TMER_BITS	\
"\10\20ILC\17EOF\16CRE\15PAE\14BGL\13EOT\12RLE\11BTE\10NXM\
\7x_SELR\6x_BOT\5x_CH7\4x_SDWN\3x_WRL\2x_RWS\1x_TUR"

#define	x_TMER_HARD	(x_TMER_ILC|x_TMER_EOT)
#define	x_TMER_SOFT	(x_TMER_CRE|x_TMER_PAE|x_TMER_BGL|x_TMER_RLE|x_TMER_BTE|x_TMER_NXM)

#ifdef	x_AVIV
/* bits in tmmr (formatter diagnostic reading) */
#define	x_DTS		000000		/* select dead track status */
#   define	x_DTS_MASK	0xff

#define	x_DAB		010000		/* select diagnostic aid bits */
#   define  x_DAB_MASK		037	/* reject code only */

#define	x_RWERR		020000		/* select read-write errors */
#    define x_RWERR_MASK		01777	/* include bit 9 (MAI) */
#    define x_RWERR_BITS \
"\10\12MAI\11CRC ERR\10WTMCHK\7UCE\6PART REC\5MTE\3END DATA CHK\
\2x_VEL x_ERR\1Dx_IAG x_MODE"

#define	x_DRSENSE		030000		/* select drive sense */
#    define x_DRSENSE_MASK	0777
#    define x_DRSENSE_BITS \
"\10\11WRTS\10EOTS\7BOTS\6WNHB\5PROS\4BWDS\3HDNG\2RDYS\1ON LINE"

#define	x_CRCF		040000		/* CRC-F Generator */

#define	x_FSR_BITS \
"\10\20REJ\17TMS\16OVRN\15DATACHK\14SSC\13EOTS\12WRTS\11ROMPS\10CRERR\
\7x_ONLS\6x_BOTS\5x_HDENS\4x_BUPER\3x_FPTS\2x_REWS\1x_RDYS"
#endif
