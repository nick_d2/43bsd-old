#include "x_.h"

/*
 * Copyright (c) 1986 MICOM-Interlan, Inc., Boxborough Mass
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)npreg.h	7.1 (Berkeley) 6/5/86
 */

/*
 *			NPREG.H
 *
 * This file contain definitions of specific hardware interest
 * to be used when communicating with the NI1510 Network Processor
 * Board. More complete information can be found in the NI1510
 * Multibus compatible Ethernet Communications Processor Hardware 
 * Specification.
 */


#define x_NNPCNN		4	/* Number of connections per board */
#define x_NPUNIT(x_a)	((x_minor(x_a) >> 4) & 0x0F)
#define x_NPCONN(x_a)	((x_minor(x_a)) & 0x03)

#define x_TRUE		1
#define x_FALSE		0

#define x_IBOOTADDR	0xF8000l	/* Addr of 80186 Boot ROM */
#define	x_INETBOOT	0xF8087l
#define x_IXEQADDR	0x400		/* Where to begin Board image XEQ */
#define x_DIAGTIME	1200		/* Time for timeout /HZ seconds */

#define	x_DELAYTIME	1000000L		/* delay count */
#define x_NPDELAY(x_N)	{register x_int x_n = (x_N) >> 1; while(--x_n > 0); }

/* Handy macros for talking to the Board */

#define x_RESET(x_x) 	(x_WCSR3(x_x->x_iobase,0xff))
#define x_CLEARINT(x_x)	{x_unsign16 x_y; x_y = x_RCSR2(x_x->x_iobase); }
#define x_INTNI(x_x)	(x_WCSR1(x_x->x_iobase,0xFF))

/* Command and Status Register (CSR) Definitions */

/*
 * CSR0 is the only direct means for data transfer between the host processor
 * and the 3510. Access is controlled by the 80186 who sets the CSR1 Enable and
 * Ready bits to allow writing here. Writing to this register will always
 * result in an interrupt to the 80186.
 */

/* 
 * Bit definitions for CSR1.
 */

#define x_NPRFU	0x01		/* Reserved for Future Use */
#define x_NPHOK	0x02		/* Hardware OK */
#define x_NPLAN	0x04		/* Logic 0 indicates operational LAN exists */
#define x_NP_IP	0x08		/* Interrupt pending from this board */
#define x_NP_IE	0x10		/* Interrupts enabled for this board */
#define x_NPRDR	0x20		/* Set when 80186 writes data into CSR0 */
#define x_NPRDY	0x40		/* CSR0 ready to accept data */
#define x_NPENB	0x80		/* CSR0 available for use by the host */

/*
 * Bit defintions for CSR0 Command Block
 */

#define x_NPLST	0x20		/* Last Command */
#define x_NPCMD	0x80		/* Shared Memory Address */
#define x_NPBGN	0x200		/* Begin Execution in On-Board Memory */
#define x_NPCBI	0x800		/* Interrupt at completion of Command Block */
#define x_NPDMP	0x2000		/* Dump 80186 On-Board Memory to Multibus */
#define x_NPLD	0x8000		/* Load 80186 On-board Memory from Multibus */

/*
 * CSR0 Count definitions. These are the lengths of the Command Blocks for the
 * CSR0 commands above (not counting the Command Word itself).
 */

#define x_LSTCNT	0
#define x_CMDCNT	2
#define x_BGNCNT	2
#define x_CBICNT	1
#define x_DMPCNT	5
#define x_LDCNT	5
#define x_IOCNT	5

/* Macros for reading and writing CSR's (Control and Status Registers) */

#define	x_WCSR0(x_x,x_y)	((x_x)->x_CSR0 = x_y)
#define	x_WCSR1(x_x,x_y)	((x_x)->x_CSR1 = x_y)
#define	x_WCSR2(x_x,x_y)	((x_x)->x_CSR2 = x_y)
#define	x_WCSR3(x_x,x_y)	((x_x)->x_CSR3 = x_y)

#define	x_RCSR0(x_x)	((x_x)->x_CSR0)
#define	x_RCSR1(x_x)	((x_x)->x_CSR1)
#define	x_RCSR2(x_x)	((x_x)->x_CSR2)
#define	x_RCSR3(x_x)	((x_x)->x_CSR3)

#define x_NPRESET		0x01		/* reset the board */
#define	x_NPSTART		0x04		/* start board execution */
#define	x_NPGPANIC	0x05		/* Get panic message */
#define	x_NPINIT		0x06		/* initialize software on board */
#define x_NPSTATS 	0x07
#define	x_NPRCSR0		0x08		/* read CSR0 */
#define	x_NPRCSR1		0x09		/* read CSR1 */
#define	x_NPRCSR2		0x0a		/* read CSR2 */
#define	x_NPRCSR3		0x0b		/* read CSR3 */
#define	x_NPWCSR0		0x0c		/* write CSR0 */
#define	x_NPWCSR1		0x0d		/* write CSR1 */
#define	x_NPWCSR2		0x0e		/* write CSR2 */
#define	x_NPWCSR3		0x0f		/* write CSR3 */
#define x_NPPOLL  	0x10
#define x_NPKILL  	0x11
#define	x_NPSETPROT	0x12		/* set the protocol to use */
#define	x_NPSETBOARD	0x13		/* set board to use */
#define	x_NPSETNPDEB	0x14		/* set nc debuging level */
#define	x_NPSETADDR	0x15		/* set host address */
#define	x_NPNETBOOT	0x16		/* boot from the network */

/* ICP Board Requests */

#define x_ICPLOAD  0x02
#define x_ICPDUMP  0x03
#define x_ICPPANIC 0x05
#define x_ICPPOLL  0x10

/*
 * Typedefs for the VAX 
 */

typedef x_short	x_sign16;			/* 16 bit signed value */
typedef x_unsigned_short x_unsign16;	/* 16 bit unsigned value */
typedef x_unsigned_int x_unsign32;		/* 32 bit unsigned value */
typedef x_long x_paddr_t;			/* Physical addresses */


/*
 * Tunables
 */


#define x_NUMCQE		40		/* Number of CQE's per board */

/* Host configuration word in Status Block */

/*
 * To disable the lock/unlock internal function calls clear the 0x8000
 * bit in the host configuration word (HOSTCONF)
 */

#define	x_HOSTCONF	0x0109	/* See above */
#define	x_LOWBYTE		1
#define	x_HIGHBYTE	0
#define x_BUFFMAPPED	0

/*
 * Memory mapping definintions for PM68DUAL hardware.
 */

#ifdef x_PM68DUAL
#define	x_PISHMEM		0x200000
#define x_PISHMEMSIZE	2
#define x_PIOFF		0x8000		/* change this to unique mem add. */
#define x_PIWINDOW	x_MBUSBUFR + x_PIOFF
#define x_WINDOWSIZE	2
#endif
#define	x_NPMAXXFR	32768		/* Maximum number of bytes / read */ 

/*
 * Define the protocols supported by the NP Driver.
 */

#define x_NONE		0x00	/* No protocols active for a process */
#define x_NPMAINT		0x01	/* Maintenance protocol, superusers only */
#define x_NPNTS		0x02	/* NTS Terminal Server */
#define x_NPDLA		0x04	/* Direct Datalink Access */
#define x_NPXNS		0x06	/* Xerox NS ITP */
#define x_NPTCP		0x08	/* TCP/IP */
#define x_NPISO		0x0A	/* ISO */
#define x_NPCLCONN	0xFF	/* Closed connection, i.e. no protocol */

/*
 * Convert the protocol to a value used in the Device Protocol Mask field
 * of the Shared Memory Status Block.
 */

#define x_PROTOMASK(x_x)	( 1 << (x_x) )

/*
 * Special requests handled by the NP Driver
 */

#define x_OS_STP		03400	/* Shut down connection on I Board */
#define x_NPSTOP		3	/* Conversion from above (OS_STP) */
#define x_NPCHNGP		50	/* Change the protocol on a connection */
#define x_NPCHNGB		51	/* Change the Board number */

/*
 * Miscellaneous
 */

#define x_ON		0x8000  /* Used for Command Q's scan and change flag */
#define x_UBADDRMASK	0x3FFFF /* 18 bit UNIBUS address */
#define x_INTMASK		0xFFFFFFFC /* Used for address validation */
#define x_CMDMASK		0xFFFF	/* Mask ioctl cmd field (see ioctl.h) */
#define x_NPPSADDR	0x324	/* Pointer to addr of on-board panic string */
#define	x_PANLEN		133		/* length of the panic buffer */

/*
 * Map function code from user to I-Board format
 */

#define x_FUNCTMAP(x_x)	(((x_x) << 6) | 077) /* Maps user function to NP funcs */

/*
 * Round up to a 16 byte boundary
 */

#define x_ROUND16(x_x)	(((x_x) + 15) & (~0x0F)) /* Round to 16 byte boundary */
#define x_ADDR24		1 /* Used by iomalloc() to specify 24 bit address */

#define x_NPERRSHIFT	8	/* Used in function ReqDone() */
#define x_NPOK		0

#define x_LOWORD(x_X)	(((x_ushort *)&(x_X))[0])
#define x_HIWORD(x_X)	(((x_ushort *)&(x_X))[1])

/* Everyday flag settings */

#define x_NPSET		1
#define x_NPCLEAR		0

/*
 * Command Queue Elements are the primary data structure for passing data
 * between the driver and the device.
 */

struct x_CQE {

	struct x_npreq *x_cqe_reqid;/* Address of asssociated npreq */
	x_unsign32 x_cqe_famid;	/* Family ID (Process ID) */
	x_unsign16 x_cqe_func;	/* I/O function to be performed */
#ifdef x_mc68000
	char x_cqe_prot;		/* Protocol type for I/O request */
	char x_cqe_lenrpb;	/* Length of the RPB in bytes */
#else
	char x_cqe_lenrpb;	/* Length of the RPB in bytes */
	char x_cqe_prot;		/* Protocol type for I/O request */
#endif
	x_unsign16 x_cqe_ust0;	/* Protocol status return */
	x_unsign16 x_cqe_ust1;	/* Protocol status return */
	x_unsign16 x_cqe_devrsv;	/* Reserved for use by device only! */
#ifdef x_mc68000
	char x_cqe_char;		/* CQE characteristics */
	char x_cqe_sts;		/* Status return from device to user */
	char x_cqe_wind;		/* Buffer mapping window size (page units) */
	char x_cqe_nbuf;		/* Number of data buffers for I/O */
#else
	char x_cqe_sts;		/* Status return from device to user */
	char x_cqe_char;		/* CQE characteristics */
	char x_cqe_nbuf;		/* Number of data buffers for I/O */
	char x_cqe_wind;		/* Buffer mapping window size (page units) */
#endif
	x_unsign16 x_cqe_bcnt;	/* Total number of bytes in the data buffer */
	x_unsign16 x_cqe_unused;	/* Unused */
	x_unsign16 x_cqe_dma[2];	/* Address of the MULTIBUS data buffer */
	x_unsign16 x_rpb1;		/* Word 1 of protocol parameters */
	x_unsign16 x_rpb2;		/* Word 2 of protocol parameters */
	x_unsign16 x_rpb3;		/* Word 3 of protocol parameters */
	x_unsign16 x_rpb4;		/* Word 4 of protocol parameters */
	x_unsign16 x_rpb5;		/* Word 5 of protocol parameters */
	x_unsign16 x_rpb6;		/* Word 6 of protocol parameters */
	x_unsign16 x_rpb7;		/* Word 7 of protocol parameters */
	x_unsign16 x_rpb8;		/* Word 8 of protocol parameters */
	x_unsign16 x_rpb9;		/* Word 9 of protocol parameters */
	x_unsign16 x_rpb10;		/* Word 10 of protocol parameters */
	x_unsign16 x_rpb11;		/* Word 11 of protocol parameters */
	x_unsign16 x_rpb12;		/* Word 12 of protocol parameters */

};

/*
 * NP Driver Request structure contains information about a request
 * maintained solely by the driver. One per CQE, plus a header.
 */

struct x_npreq {

	struct x_npreq *x_forw;	/* Forward pointer for active list */
	struct x_npreq *x_back;	/* Backward pointer for active list */
	struct x_npreq *x_free;	/* Next member on free list */
	struct x_CQE *x_element;	/* CQE associated with this request */
	x_int x_flags;		/* Always useful */
	x_int x_reqcnt;		/* Request count for reqtab */
	x_int x_bufoffset;		/* Offset into buffer for turns */
	x_int	x_bytecnt;	/* Number of bytes to transfer */
	x_caddr_t	x_virtmem;	/* Virtual address of buffer */
	x_int	x_mapbase;	/* Address of the mapping register */
	x_int 	x_mapsize;	/* Size of mapped area */
	x_caddr_t	x_bufaddr;	/* Address of the user buffer */
	struct x_buf x_buf;		/* Buf structure needed for mem. mgmt */
	struct x_proc *x_procp;	/* Pointer to process of requestor */
	x_caddr_t x_user;		/* Structure passed by user from itpuser.h */
	x_int	(*x_intr)();	/* Ptr to routine to call at interrupt time */
};

/*
 * Npmaster structure, one per device, is used for boardwise centralization
 * of relevant information including queues, I/O addresses and request pools.
 */

struct x_npmaster {

	struct x_npmaster *x_next; 	/* Linked list of these, NULL terminator */
	struct x_npspace *x_shmemp;	/* Shared memory address (driver <-> device) */
	struct x_uba_device *x_devp; /* UBA Device for this unit */
	struct x_NPREG   *x_iobase;	/* I/O base address for this board */
	struct x_npreq   *x_reqtab;	/* Header for pool of CQE requests */
	x_int	x_iomapbase;	/* Base index of I/O map reg's allocated */
	x_int x_flags;		/* State of the Board */
	x_int x_unit;		/* Unit number of this device */
	x_int x_vector;		/* Interrupt vector for this unit */
};

struct x_npconn {

	struct x_npmaster *x_unit;	/* Unit number (board) of this connection */
	x_unsign16 x_protocol;	/* Protocol used on this connection */
	struct x_buf x_np_wbuf;	/* write buf structure for raw access */
	struct x_buf x_np_rbuf;	/* read buf structure for raw access */
};

struct x_NPREG {
	x_unsign16 x_CSR0;		/* Control Status Register 0 */
	x_unsign16 x_CSR1;		/* Control Status Register 1 */
	x_unsign16 x_CSR2;		/* Control Status Register 2 */
	x_unsign16 x_CSR3;		/* Control Status Register 3 */

};

/*
 * The following structures are used for communicating with the
 * Intelligent Board and are located in Shared Memory.
 */

/*
 * Status Block
 */

struct x_NpStat{

	x_unsign16 x_sb_drw;	/* Device Request Word */
	x_unsign16 x_sb_hcw;	/* Host Configuration Word */
	x_unsign16 x_sb_dcw;	/* Device Configuration Word */
	x_unsign16 x_sb_dpm;	/* Device Protocol Mask */
	x_unsign16 x_sb_dcq;	/* Offset to Device CQ */
	x_unsign16 x_sb_hcq;	/* Offset to Host CQ */
};

/*
 * Command Queue, two per device. One is owned by the driver and the other
 * is owned by the device.
 */

struct x_CmdQue {

	x_unsign16 x_scanflag;	/* Scan Flag, MSB set if being scanned */
	x_unsign16 x_chngflag;	/* Change Flag, MSB set by initiator */
	x_unsign16 x_cq_wrap;	/* Offset to last CQE entry +2 */
	x_unsign16 x_cq_add;	/* Offset to add a CQE to the queue */
	x_unsign16 x_cq_rem;	/* Offset to remove a CQE from the queue */
	x_unsign16 x_cq_cqe[x_NUMCQE]; /* Command Queue Element Offsets */
};

/*
 * Structure of the shared memory area per board. Declared this way to avoid
 * compiler word alignment vagaries when computing offsets.
 */

struct x_npspace {

	struct x_NpStat x_statblock;	/* Status Block */
	struct x_CmdQue x_devcq;		/* Device's Command Queue */
	struct x_CmdQue x_hostcq;		/* Host's Command Queue */
	struct x_CQE x_elements[x_NUMCQE];	/* Shared Command Queue Elements */
	x_unsign16 x_filler[8];		/* Here for 16 byte alignment */
};

/*
 * Structure of array of base addresses of I-Board controllers
 * (See global data definitions in np.c)
 */

struct x_npbase {
	x_caddr_t x_baseaddr;
};

/* State of the NP Driver as kept in NpState */

#define x_ICPAVAIL	0x01 	/* ICP is waiting for a request */

/* Tells ICP Process that there are no more requests for this board */

#define x_BRDDONE 1

/* Flags used by the driver (npreq structure) to monitor status of requests */

#define x_REQDONE 0x01		/* Request completed */
#define x_IOIFC   0x02		/* Internal Function Code Request */
#define x_IOERR	0x04		/* Error on Request */
#define x_NPPEND	0x08		/* Unused at this time */
#define x_IOABORT 0x10		/* Request aborted by ICP */
#define x_KERNREQ	0x20		/* Request was from the kernel */
#define x_WANTREQ 0x40		/* Process is waiting for a npreq structure */
#define x_NPUIO	0x80		/* Process doing physio */

/* Service Request Commands from the Intelligent Board */

#define x_NOREQ	0x00		/* No service requested */
#define x_NPLOAD  0x01		/* Dump request */
#define x_NPDUMP	0x02		/* Load request */
#define x_NPPANIC	0x100		/* Panic request */

/* Definitions of Status returned from the I-Board */

#define x_NPDONE	0x01		/* Normal completion */
#define x_NPIFC	0x00		/* Internal Function Code request */
#define x_NPPERR  0x80		/* Protocol error */
#define x_NPMERR	0x82		/* Memory allocation failure on I-Board */

/* Definitions of IFC type requests from I-Board */

#define x_NPLOCK	0x64		/* Lock the process's data area */
#define x_NPUNLOCK 0xA4		/* Unlock the process */
#define x_NPREMAP	0x124		/* Window turn */

/* Definition of flags for the Npmaster structure */

#define x_CSRPEND		0x01		/* CSR0 command pending */
#define x_PANICREQ	0x02		/* Panic request */
#define x_DUMPREQ		0x04		/* Dump request */
#define x_LOADREQ		0x08		/* Load request */
#define x_BOARDREQ	0x10		/* Any request by the board */
#define x_BADBOARD	0x20		/* Board disabled */
#define x_AVAILABLE	0x40		/* Board available */
#define x_BRDRESET	0x80		/* Board is being reset */
#define	x_PANIC1	 	0x100		/* Driver wants panic address */
#define	x_PANIC2		0x200		/* Driver wants panic string */
#define x_PANIC3		0x400		/* Clear first byte of panic string */

/*
 * Debugging Constants
 */

#define	x_DEBENTRY	0x0001		/* debug entry points */
#define	x_DEBMEM		0x0002		/* debug memory */
#define	x_DEBREQ		0x0004		/* debug requests */
#define	x_DEBCQE		0x0008		/* debug cqe's */
#define	x_DEBCQ		0x0010		/* debug cq's */
#define	x_DEBMAINT	0x0020		/* debug maintainance requests */
#define	x_DEBINTR		0x0040		/* debug interrupt routines */
#define	x_DEBINIT		0x0080		/* debug initialization routines */
#define	x_DEBIFC		0x0100		/* debug Internal function codes */
#define	x_DEBIOCTL	0x0200		/* debug ioctl calls */
#define	x_DEBOPEN		0x0400		/* debug open calls */
#define	x_DEBIO		0x0800		/* debug read & write calls */
#define	x_DEBCSR		0x1000		/* debug CSR commands */
#define	x_DEBLOCK		0x2000		/* debug lock / unlock calls */
#define x_NOBOARD		0x4000		/* debug user/host interface */
