#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)utreg.h	7.1 (Berkeley) 6/5/86
 */

/*
 * System Industries Model 9700 Tape Drive
 *   emulates TU45 on the UNIBUS
 */

struct x_utdevice {
	x_u_short	x_utcs1;		/* control status register 1 */
	x_short	x_utwc;		/* word count register */
	x_u_short	x_utba;		/* low 16-bits of bus address */
	x_short	x_utfc;		/* frame counter */
	x_u_short	x_utcs2;		/* control status register 2 */
	x_u_short	x_utds;		/* drive status register */
	x_u_short	x_uter;		/* error register */
	x_u_short	x_utas;		/* attention status register */
	x_u_short	x_utcc;		/* NRZI CRC character for validation */
	x_u_short	x_utdb;		/* data buffer reg (not emulated) */
	x_u_short	x_utmr;		/* maintenance reg (not emulated) */
	x_u_short	x_utdt;		/* drive type register (not emulated) */
	x_u_short	x_utsn;		/* serial number reg (not emulated) */
	x_u_short	x_uttc;		/* tape control register */
	x_u_short	x_utbae;		/* buffer address extension register */
	x_u_short	x_utcs3;		/* control and status register 3 */
};

/*
 * utcs1 --
 *   cmds, interrupt enable, extended address bits, and status
 */
#define	x_UT_GO		0x0001		/* go bit */
/* function codes reside in bits 5-1 */
#define	x_UT_NOP		0x0000		/* no operation */
#define	x_UT_REWOFFL	0x0002		/* rewind offline */
#define	x_UT_LOOP		0x0004		/* loop read/write */
#define	x_UT_REW		0x0006		/* rewind */
#define	x_UT_CLEAR	0x0008		/* drive clear */
#define	x_UT_SENSE	0x000a		/* drive sense */
#define	x_UT_PRESET	0x0010		/* read in preset */
#define	x_UT_DIAGN	0x0012		/* diagnostic mode set */
#define	x_UT_ERASE	0x0014		/* erase */
#define	x_UT_WEOF		0x0016		/* write tape mark */
#define	x_UT_SFORW	0x0018		/* forward space block */
#define	x_UT_SREV		0x001a		/* reverse space block */
#define	x_UT_SFORWF	0x001c		/* forward space file */
#define	x_UT_SREVF	0x001e		/* reverse space file */
#define	x_UT_WCHFORW	0x0028		/* write check forward */
#define	x_UT_WCHREV	0x002e		/* write check reverse */
#define	x_UT_WCOM		0x0030		/* write forward */
#define	x_UT_RCOM		0x0038		/* read forward */
#define	x_UT_RREV		0x003e		/* read reverse */
/* the remainder are control and status bits */
#define	x_UT_IE		0x0040		/* interrupt-enable */
#define	x_UT_RDY		0x0080		/* controller ready */
#define	x_UT_EADDR	0x0300		/* extended address bits */
/* bit 10 unused */
#define	x_UT_DVA		0x0800		/* drive available */
/* bit 12 unused */
/* bit 13 - massbus control parity error not emulated */
#define	x_UT_TRE		0x4000		/* transfer error */
#define	x_UT_SC		0x8000		/* special condition */

#define	x_UT_BITS \
"\10\20SC\17TRE\14DVA\10RDY\7IE\1GO"

/*
 * utcs2 --
 *   controller clear, error flags, and unit select
 */
/* bits 0-2 are unit select */
#define	x_UTCS2_BAI	0x0008		/* UNIBUS address increment inhibit */
#define	x_UTCS2_PAT	0x0010		/* parity test */
#define	x_UTCS2_CLR	0x0020		/* controller clear */
#define	x_UTCS2_IR	0x0040		/* input ready (not emulated) */
#define	x_UTCS2_OR	0x0080		/* output ready (not emulated) */
#define	x_UTCS2_RPE	0x0100		/* rom parity error */
#define	x_UTCS2_MXF	0x0200		/* missed transfer */
#define	x_UTCS2_NEM	0x0400		/* non existant memory */
#define	x_UTCS2_PGE	0x0800		/* program error */
#define	x_UTCS2_NED	0x1000		/* non existent drive */
#define	x_UTCS2_PE	0x2000		/* parity error */
#define	x_UTCS2_WCE	0x4000		/* write check error */
#define	x_UTCS2_DLT	0x8000		/* data late */

#define	x_UTCS2_BITS \
"\10\20DLT\17WCE\16PE\15NED\14\NEM\13\PGE\12\MXF\11RPE\10OR\7IR\6CLR\5PAT\4\BAI"

/*
 * utds --
 *   beginning of tape, end of tape, error summary bit, plus lots more
 */
#define	x_UTDS_SLA	0x0001		/* slave attention */
#define	x_UTDS_BOT	0x0002		/* beginning of tape */
#define	x_UTDS_TM		0x0004		/* tape mark */
#define	x_UTDS_IDB	0x0008		/* identification burst */
#define	x_UTDS_SDWN	0x0010		/* slowing down */
#define	x_UTDS_PES	0x0020		/* phase encode status */
#define	x_UTDS_SSC	0x0040		/* slave status change */
#define	x_UTDS_DRY	0x0080		/* drive ready */
#define	x_UTDS_DPR	0x0100		/* drive present (always 1) */
#define	x_UTDS_GCR	0x0200		/* GCR status */
#define	x_UTDS_EOT	0x0400		/* end of tape */
#define	x_UTDS_WRL	0x0800		/* write lock */
#define	x_UTDS_MOL	0x1000		/* medium on line */
#define	x_UTDS_PIP	0x2000		/* positioning in progress */
#define	x_UTDS_ERR	0x4000		/* composite error */
#define	x_UTDS_ATA	0x8000		/* attention active */

#define	x_UTDS_BITS \
"\10\20ATA\17ERR\16PIP\15MOL\14WRL\13EOT\12GCR\11DPR\10DRY\
\7x_SSC\6x_PES\5x_SDWN\4x_IDB\3x_TM\2x_BOT\1x_SLA"

/*
 * uter --
 *   detailed breakdown of error summary bit from cs2
 */
#define	x_UTER_ILF	0x0001		/* illegal function */
#define	x_UTER_ILR	0x0002		/* illegal register (always 0) */
#define	x_UTER_RMR	0x0004		/* register modification refused */
#define	x_UTER_RPE	0x0008		/* read data parity error */
#define	x_UTER_FMT	0x0010		/* format error */
#define	x_UTER_DPAR	0x0020		/* data bus parity error */
#define	x_UTER_INC	0x0040		/* incorrectable data */
#define	x_UTER_PEF	0x0080		/* PE format error */
#define	x_UTER_NSG	0x0100		/* non standard gap */
#define	x_UTER_FCE	0x0200		/* frame count error */
#define	x_UTER_CS		0x0400		/* correctable skew */
#define	x_UTER_NEF	0x0800		/* non executable function */
#define	x_UTER_DTE	0x1000		/* drive timing error */
#define	x_UTER_OPI	0x2000		/* operation incomplete */
#define	x_UTER_UNS	0x4000		/* unsafe */
#define	x_UTER_COR	0x8000		/* correctible data error */

/*
 * These errors we consider "hard"; UTER_OPI and UTER_RPE
 * are considered "soft", at least for the moment.
 */
#define	x_UTER_HARD	(x_UTER_UNS|x_UTER_NEF|x_UTER_DPAR|x_UTER_FMT|x_UTER_RMR|\
			 x_UTER_ILR|x_UTER_ILF)

#define	x_UTER_BITS \
"\10\20COR\17UNS\16OPI\15DTE\14NEF\13CS\12FCE\11NSG\10PEF\
\7x_INC\6Dx_PAR\5x_FMT\4x_RPE\3x_RMR\2x_ILR\1x_ILF"

/*
 * uttc --
 *   tape format and density
 */
/* bits 0-2 are slave select */
#define	x_UTTC_EVPAR	0x0008		/* even parity */
#define	x_UTTC_FMT	0x00f0		/* format select (see below) */
#define	x_UTTC_DEN	0x0700		/* density select (see below) */
/* bit 11 not used */
#define	x_UTTC_EAODTE	0x1000		/* (not emulated) */
#define	x_UTTC_TCW	0x2000		/* tape control write */
#define	x_UTTC_FCS	0x4000		/* frame count status */
#define	x_UTTC_ACCL	0x8000		/* acceleration */

/* the bits to stuff in UTTC_DEN */
#define	x_UT_NRZI		0x0000		/* 800 bpi code */
#define	x_UT_PE		0x0400		/* 1600 bpi code */
#define	x_UT_GCR		0x0500		/* 6250 bpi code */

/* tape formats - only PDP-11 standard is supported */
#define	x_PDP11FMT	0x00c0		/* PDP-11 standard */

#define	x_b_repcnt  x_b_bcount
#define	x_b_command x_b_resid
#define	x_b_state	  x_b_active  
