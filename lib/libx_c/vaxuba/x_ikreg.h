#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)ikreg.h	7.1 (Berkeley) 6/5/86
 */

struct x_ikdevice {
	x_short	x_ik_wc;			/* Unibus word count reg */
	x_u_short	x_ik_ubaddr;		/* Unibus address register */
	x_u_short	x_ik_ustat;		/* Unibus status/command reg */
	x_u_short	x_ik_data;		/* Data register */
	x_u_short	x_ik_xaddr;		/* X address in frame buffer */
	x_u_short	x_ik_yaddr;		/* Y address in frame buffer */
	x_u_short	x_ik_istat;		/* Ikonas status/command reg */
	x_u_short	x_ik_chan;		/* Channel control register */
};

#define x_IK_GETADDR 	x_IKIOGETADDR
#define x_IK_WAITINT 	x_IKIOWAITINT
#define	x_IKIOGETADDR	x__IOR(x_i, 0, x_caddr_t)	/* get Unibus device address */
#define	x_IKIOWAITINT	x__IO(x_i, 1)		/* await device interrupt */

/*
 * Unibus status/command register bits
 */

#define x_IK_GO		01
#define x_IK_IENABLE	0100
#define x_IK_READY	0200
#define x_IK_IKONAS_INTR	0100000

/*
 * Ikonas status/command register bits
 */

#define x_WORD32		0
#define x_RES512		2
#define x_RES1024		3
#define x_READ_SELECT	0
#define x_WRITE_MASK	010
#define x_WRITE_SELECT	020
#define x_HALFWORD	040
#define x_DMAENABLE	0100
#define x_INVISIBLE_IO	0200
#define x_AUTOINCREMENT	0400
#define x_RUN_PROCESSOR	01000
#define x_CLEAR		02000
#define x_BYTE_MODE	04000
#define x_FRAME_ENABLE	010000
#define x_PROC_ENABLE	020000
#define x_RED_SELECT	0
#define x_GREEN_SELECT	040000
#define x_BLUE_SELECT	0100000
#define x_ALPHA_SELECT	0140000

/*
 * Frame buffer controller
 */

#define x_FBC0		060000000
#define x_FBC1		062000000

#define x_VIEWPORT_LOC	0
#define x_VIEWPORT_SIZE	1
#define x_WINDOW_LOC	2
#define x_ZOOM		3
#define x_DISPLAY_RATE	4
#define x_VIDEO_CONTROL	5
#define		x_FORMAT_CONTROL_MASK	03
#define		x_CURSOR_ON		04
#define		x_LOW_RESOL		0
#define		x_HIGH_RESOL		010
#define		x_AUTO_CLEAR		040
#define		x_EXT_SYNC		0100
#define		x_COLOR_MAP_PAGES		0600
#define		x_HIGH_RESOL_SYNC		01000
#define		x_REPEAT_FIELD		02000
#define		x_PIXEL_CLOCK_RATE_MASK	077
#define x_CURSOR_LOC	6
#define x_CURSOR_SHADE	7

#define x_CURSOR_MAP	0400

/*
 * Color map lookup table
 */

#define x_CMAP0		040600000
#define x_CMAP1		040610000

#define x_CHAN_SELECT	02000

/*
 * Frame buffer memories
 */

#define x_MEM0		000000000
#define x_MEM1		004000000

/*
 * Bit-slice processor
 */

#define x_UMEM		040000000
#define x_SCRPAD		040400000
#define x_PROC		041200000

/*
 * Frame grabber
 */

#define x_FMG0		060200000
