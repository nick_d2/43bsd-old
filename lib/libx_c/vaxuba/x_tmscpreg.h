#include "x_.h"

/* @(#)tmscpreg.h	7.1 (Berkeley) 6/5/86 */

/*	@(#)tmscpreg.h	1.1	11/2/84	84/09/25	*/

/****************************************************************
 *								*
 *        Licensed from Digital Equipment Corporation 		*
 *                       Copyright (c) 				*
 *               Digital Equipment Corporation			*
 *                   Maynard, Massachusetts 			*
 *                         1985, 1986 				*
 *                    All rights reserved. 			*
 *								*
 *        The Information in this software is subject to change *
 *   without notice and should not be construed as a commitment *
 *   by  Digital  Equipment  Corporation.   Digital   makes  no *
 *   representations about the suitability of this software for *
 *   any purpose.  It is supplied "As Is" without expressed  or *
 *   implied  warranty. 					*
 *								*
 *        If the Regents of the University of California or its *
 *   licensees modify the software in a manner creating  	*
 *   diriviative copyright rights, appropriate copyright  	*
 *   legends may be placed on  the drivative work in addition   *
 *   to that set forth above. 					*
 *								*
 ****************************************************************/
/*
 * TMSCP registers and structures
 */
 
struct x_tmscpdevice {
	x_short	x_tmscpip;	/* initialization and polling */
	x_short	x_tmscpsa;	/* status and address */
};
 
#define	x_TMSCP_ERR		0100000	/* error bit */
#define	x_TMSCP_STEP4	0040000	/* step 4 has started */
#define	x_TMSCP_STEP3	0020000	/* step 3 has started */
#define	x_TMSCP_STEP2	0010000	/* step 2 has started */
#define	x_TMSCP_STEP1	0004000	/* step 1 has started */
#define	x_TMSCP_NV		0002000	/* no host settable interrupt vector */
#define	x_TMSCP_QB		0001000	/* controller supports Q22 bus */
#define	x_TMSCP_DI		0000400	/* controller implements diagnostics */
#define	x_TMSCP_OD		0000200	/* port allows odd host addr's in the buffer descriptor */
#define	x_TMSCP_IE		0000200	/* interrupt enable */
#define	x_TMSCP_MP		0000100	/* port supports address mapping */
#define	x_TMSCP_LF		0000002	/* host requests last fail response packet */
#define	x_TMSCP_PI		0000001	/* host requests adapter purge interrupts */
#define	x_TMSCP_GO		0000001	/* start operation, after init */
 
 
/*
 * TMSCP Communications Area
 */
 
struct x_tmscpca {
	x_short	x_ca_xxx1;	/* unused */
	char	x_ca_xxx2;	/* unused */
	char	x_ca_bdp;		/* BDP to purge */
	x_short	x_ca_cmdint;	/* command queue transition interrupt flag */
	x_short	x_ca_rspint;	/* response queue transition interrupt flag */
	x_long	x_ca_rspdsc[x_NRSP];/* response descriptors */
	x_long	x_ca_cmddsc[x_NCMD];/* command descriptors */
};
 
#define	x_ca_ringbase	x_ca_rspdsc[0]
 
#define	x_TMSCP_OWN	0x80000000	/* port owns this descriptor (else host
 owns it) */
#define	x_TMSCP_INT	0x40000000	/* allow interrupt on ring transition */
 
#define	x_TMSCP_MAP	0x80000000	/* modifier for mapped buffer descriptors */
 
/*
 * TMSCP packet info (same as MSCP)
 */
struct x_mscp_header {
	x_short	x_tmscp_msglen;	/* length of MSCP packet */
	char	x_tmscp_credits;	/* low 4 bits: credits, high 4 bits: msgtype */
	char	x_tmscp_vcid;	/* virtual circuit id (connection id) */
};
