#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)ubareg.h	7.1 (Berkeley) 6/5/86
 */

/*
 * VAX UNIBUS adapter registers
 */

/*
 * size of unibus address space in pages
 */
#define x_UBAPAGES 512

/*
 * Number of UNIBUS map registers.  We can't use the last 8k of UNIBUS
 * address space for i/o transfers since it is used by the devices,
 * hence have slightly less than 256K of UNIBUS address space.
 */
#define	x_NUBMREG	496

#ifndef x_LOCORE
/*
 * UBA hardware registers
 */
struct x_uba_regs
{
	x_int	x_uba_cnfgr;		/* configuration register */
	x_int	x_uba_cr;			/* control register */
	x_int	x_uba_sr;			/* status register */
	x_int	x_uba_dcr;		/* diagnostic control register */
	x_int	x_uba_fmer;		/* failed map entry register */
	x_int	x_uba_fubar;		/* failed UNIBUS address register */
	x_int	x_pad1[2];
	x_int	x_uba_brsvr[4];
	x_int	x_uba_brrvr[4];		/* receive vector registers */
	x_int	x_uba_dpr[16];		/* buffered data path register */
	x_int	x_pad2[480];
	struct	x_pte x_uba_map[x_NUBMREG];	/* unibus map register */
	x_int	x_pad3[16];		/* no maps for device address space */
};
#endif

#if defined(x_VAX780) || defined(x_VAX8600)
/* uba_cnfgr */
#define	x_UBACNFGR_UBINIT	0x00040000	/* unibus init asserted */
#define	x_UBACNFGR_UBPDN	0x00020000	/* unibus power down */
#define	x_UBACNFGR_UBIC	0x00010000	/* unibus init complete */

#define x_UBACNFGR_BITS \
"\40\40PARFLT\37WSQFLT\36URDFLT\35ISQFLT\34MXTFLT\33XMTFLT\30ADPDN\27ADPUP\23UBINIT\22UBPDN\21UBIC"

/* uba_cr */
#define	x_UBACR_MRD16	0x40000000	/* map reg disable bit 4 */
#define	x_UBACR_MRD8	0x20000000	/* map reg disable bit 3 */
#define	x_UBACR_MRD4	0x10000000	/* map reg disable bit 2 */
#define	x_UBACR_MRD2	0x08000000	/* map reg disable bit 1 */
#define	x_UBACR_MRD1	0x04000000	/* map reg disable bit 0 */
#define	x_UBACR_IFS	0x00000040	/* interrupt field switch */
#define	x_UBACR_BRIE	0x00000020	/* BR interrupt enable */
#define	x_UBACR_USEFIE	0x00000010	/* UNIBUS to SBI error field IE */
#define	x_UBACR_SUEFIE	0x00000008	/* SBI to UNIBUS error field IE */
#define	x_UBACR_CNFIE	0x00000004	/* configuration IE */
#define	x_UBACR_UPF	0x00000002	/* UNIBUS power fail */
#define	x_UBACR_ADINIT	0x00000001	/* adapter init */

/* uba_sr */
#define	x_UBASR_BR7FULL	0x08000000	/* BR7 receive vector reg full */
#define	x_UBASR_BR6FULL	0x04000000	/* BR6 receive vector reg full */
#define	x_UBASR_BR5FULL	0x02000000	/* BR5 receive vector reg full */
#define	x_UBASR_BR4FULL	0x01000000	/* BR4 receive vector reg full */
#define	x_UBASR_RDTO	0x00000400	/* UNIBUS to SBI read data timeout */
#define	x_UBASR_RDS	0x00000200	/* read data substitute */
#define	x_UBASR_CRD	0x00000100	/* corrected read data */
#define	x_UBASR_CXTER	0x00000080	/* command transmit error */
#define	x_UBASR_CXTMO	0x00000040	/* command transmit timeout */
#define	x_UBASR_DPPE	0x00000020	/* data path parity error */
#define	x_UBASR_IVMR	0x00000010	/* invalid map register */
#define	x_UBASR_MRPF	0x00000008	/* map register parity failure */
#define	x_UBASR_LEB	0x00000004	/* lost error */
#define	x_UBASR_UBSTO	0x00000002	/* UNIBUS select timeout */
#define	x_UBASR_UBSSYNTO	0x00000001	/* UNIBUS slave sync timeout */

#define	x_UBASR_BITS \
"\20\13RDTO\12RDS\11CRD\10CXTER\7CXTMO\6DPPE\5IVMR\4MRPF\3LEB\2UBSTO\1UBSSYNTO"

/* uba_brrvr[] */
#define	x_UBABRRVR_AIRI	0x80000000	/* adapter interrupt request */
#define	x_UBABRRVR_DIV	0x0000ffff	/* device interrupt vector field */
#endif
 
/* uba_dpr */
#if defined(x_VAX780) || defined(x_VAX8600)
#define	x_UBADPR_BNE	0x80000000	/* buffer not empty - purge */
#define	x_UBADPR_BTE	0x40000000	/* buffer transfer error */
#define	x_UBADPR_DPF	0x20000000	/* DP function (RO) */
#define	x_UBADPR_BS	0x007f0000	/* buffer state field */
#define	x_UBADPR_BUBA	0x0000ffff	/* buffered UNIBUS address */
#define	x_UBA_PURGE780(x_uba, x_bdp) \
    ((x_uba)->x_uba_dpr[x_bdp] |= x_UBADPR_BNE)
#else
#define x_UBA_PURGE780(x_uba, x_bdp)
#endif
#if x_VAX750
#define	x_UBADPR_ERROR	0x80000000	/* error occurred */
#define	x_UBADPR_NXM	0x40000000	/* nxm from memory */
#define	x_UBADPR_UCE	0x20000000	/* uncorrectable error */
#define	x_UBADPR_PURGE	0x00000001	/* purge bdp */
/* the DELAY is for a hardware problem */
#define	x_UBA_PURGE750(x_uba, x_bdp) { \
    ((x_uba)->x_uba_dpr[x_bdp] |= (x_UBADPR_PURGE|x_UBADPR_NXM|x_UBADPR_UCE)); \
    x_DELAY(8); \
}
#else
#define x_UBA_PURGE750(x_uba, x_bdp)
#endif

/*
 * Macros for fast buffered data path purging in time-critical routines.
 *
 * Too bad C pre-processor doesn't have the power of LISP in macro
 * expansion...
 */

#if defined(x_VAX8600) || defined(x_VAX780) || defined(x_VAX750)
#define	x_UBAPURGE(x_uba, x_bdp) { \
	switch (x_cpu) { \
	case x_VAX_8600: case x_VAX_780: x_UBA_PURGE780((x_uba), (x_bdp)); break; \
	case x_VAX_750: x_UBA_PURGE750((x_uba), (x_bdp)); break; \
	} \
}
#endif
#if !defined(x_VAX8600) && !defined(x_VAX780) && !defined(x_VAX750)
#define	x_UBAPURGE(x_uba, x_bdp)
#endif



/* uba_mr[] */
#define	x_UBAMR_MRV	0x80000000	/* map register valid */
#define	x_UBAMR_BO	0x02000000	/* byte offset bit */
#define	x_UBAMR_DPDB	0x01e00000	/* data path designator field */
#define	x_UBAMR_SBIPFN	0x000fffff	/* SBI page address field */

#define	x_UBAMR_DPSHIFT	21		/* shift to data path designator */

/*
 * Number of unibus buffered data paths and possible uba's per cpu type.
 */
#define	x_NBDP8600	15
#define	x_NBDP780	15
#define	x_NBDP750	3
#define	x_NBDP730	0
#define	x_NBDP630	0
#define	x_MAXNBDP	15

/*
 * Symbolic BUS addresses for UBAs.
 */

#if x_VAX630
#define	x_UMEM630		((x_u_short *)(0x1ffc2000))
#endif

#if x_VAX730
#define	x_UMEM730		((x_u_short *)(0xfc0000))
#endif

#if x_VAX750
#define	x_UMEM750(x_i)	((x_u_short *)(0xfc0000-(x_i)*0x40000))
#endif

#if x_VAX780
#define	x_UMEM780(x_i)	((x_u_short *)(0x20100000+(x_i)*0x40000))
#endif

#if x_VAX8600
#define	x_UMEMA8600(x_i)	((x_u_short *)(0x20100000+(x_i)*0x40000))
#define	x_UMEMB8600(x_i)	((x_u_short *)(0x22100000+(x_i)*0x40000))
#endif

/*
 * Macro to offset a UNIBUS device address, often expressed as
 * something like 0172520 by forcing it into the last 8K of UNIBUS memory
 * space.
 */
#define	x_ubdevreg(x_addr)	(0760000|((x_addr)&017777))

