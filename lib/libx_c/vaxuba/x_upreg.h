#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)upreg.h	7.1 (Berkeley) 6/5/86
 */

/*
 * Unibus rm emulation via sc21:
 * registers and bits.
 */

struct x_updevice
{
	x_u_short	x_upcs1;		/* control and status register 1 */
	x_short	x_upwc;		/* word count register */
	x_u_short	x_upba;		/* UNIBUS address register */
	x_u_short	x_upda;		/* desired address register */
	x_u_short	x_upcs2;		/* control and status register 2 */
	x_u_short	x_upds;		/* drive Status */
	x_u_short	x_uper1;		/* error register 1 */
	x_u_short	x_upas;		/* attention summary */
	x_u_short	x_upla;		/* look ahead */
	x_u_short	x_updb;		/* data buffer */
	x_u_short	x_upmr;		/* maintenance */ 
	x_u_short	x_updt;		/* drive type */
	x_u_short	x_upsn;		/* serial number */
	x_u_short	x_upof;		/* offset register */
	x_u_short	x_updc;		/* desired cylinder address register */
	x_u_short	x_uphr;		/* holding register */
	x_u_short	x_upmr2;		/* maintenance register 2 */
	x_u_short	x_uper2;		/* error register 2 */
	x_u_short	x_upec1;		/* burst error bit position */
	x_u_short	x_upec2;		/* burst error bit pattern */
};

/* Other bits of upcs1 */
#define	x_UP_SC	0100000		/* special condition */
#define	x_UP_TRE	0040000		/* transfer error */
#define	x_UP_PSEL	0010000		/* port select */
#define	x_UP_DVA	0004000		/* drive available */
/* bits 8 and 9 are the extended address bits */
#define	x_UP_RDY	0000200		/* controller ready */
#define	x_UP_IE	0000100		/* interrupt enable */
/* bits 5-1 are the command */
#define	x_UP_GO	0000001

/* commands */
#define	x_UP_NOP		000
#define	x_UP_SEEK		004		/* seek */
#define	x_UP_RECAL	006		/* recalibrate */
#define	x_UP_DCLR		010		/* drive clear */
#define	x_UP_RELEASE	012		/* release */
#define	x_UP_OFFSET	014		/* offset */
#define	x_UP_RTC		016		/* return to center-line */
#define	x_UP_PRESET	020		/* read-in preset */
#define	x_UP_PACK		022		/* pack acknowledge */
#define	x_UP_DMABAND	024		/* dma bandwidth set */
#define	x_UP_SEARCH	030		/* search */
#define	x_UP_WCDATA	050		/* write check data */
#define	x_UP_WCHDR	052		/* write check header and data */
#define	x_UP_WCOM		060		/* write */
#define	x_UP_WHDR		062		/* write header and data */
#define	x_UP_RCOM		070		/* read data */
#define	x_UP_RHDR		072		/* read header and data */
#define	x_UP_BOOT		074		/* boot */
#define	x_UP_FORMAT	076		/* format */

/* upcs2 */
#define	x_UPCS2_DLT	0100000		/* data late */
#define	x_UPCS2_WCE	0040000		/* write check error */
#define	x_UPCS2_UPE	0020000		/* UNIBUS parity error */
#define	x_UPCS2_NED	0010000		/* nonexistent drive */
#define	x_UPCS2_NEM	0004000		/* nonexistent memory */
#define	x_UPCS2_PGE	0002000		/* programming error */
#define	x_UPCS2_MXF	0001000		/* missed transfer */
#define	x_UPCS2_MDPE	0000400		/* massbus data parity error (0) */
#define	x_UPCS2_OR	0000200		/* output ready */
#define	x_UPCS2_IR	0000100		/* input ready */
#define	x_UPCS2_CLR	0000040		/* controller clear */
#define	x_UPCS2_PAT	0000020		/* parity test */
#define	x_UPCS2_BAI	0000010		/* address increment inhibit */
/* bits 0-2 are drive select */

#define	x_UPCS2_BITS \
"\10\20DLT\17WCE\16UPE\15NED\14NEM\13PGE\12MXF\11MDPE\
\10x_OR\7x_IR\6x_CLR\5x_PAT\4x_BAI"

/* upds */
#define	x_UPDS_ATA	0100000		/* attention active */
#define	x_UPDS_ERR	0040000		/* composite drive error */
#define	x_UPDS_PIP	0020000		/* positioning in progress */
#define	x_UPDS_MOL	0010000		/* medium on line */
#define	x_UPDS_WRL	0004000		/* write locked */
#define	x_UPDS_LST	0002000		/* last sector transferred */
#define	x_UPDS_PGM	0001000		/* programmable */
#define	x_UPDS_DPR	0000400		/* drive present */
#define	x_UPDS_DRY	0000200		/* drive ready */
#define	x_UPDS_VV		0000100		/* volume valid */
/* bits 1-5 are spare */
#define	x_UPDS_OM		0000001		/* offset mode */

#define	x_UPDS_DREADY	(x_UPDS_DPR|x_UPDS_DRY|x_UPDS_MOL|x_UPDS_VV)

#define	x_UPDS_BITS \
"\10\20ATA\17ERR\16PIP\15MOL\14WRL\13LST\12PGM\11DPR\10DRY\7VV\1OM"

/* uper1 */
#define	x_UPER1_DCK	0100000		/* data check */
#define	x_UPER1_UNS	0040000		/* drive unsafe */
#define	x_UPER1_OPI	0020000		/* operation incomplete */
#define	x_UPER1_DTE	0010000		/* drive timing error */
#define	x_UPER1_WLE	0004000		/* write lock error */
#define	x_UPER1_IAE	0002000		/* invalid address error */
#define	x_UPER1_AOE	0001000		/* address overflow error */
#define	x_UPER1_HCRC	0000400		/* header crc error */
#define	x_UPER1_HCE	0000200		/* header compare error */
#define	x_UPER1_ECH	0000100		/* ecc hard error */
#define	x_UPER1_WCF	0000040		/* write clock fail (0) */
#define	x_UPER1_FER	0000020		/* format error */
#define	x_UPER1_PAR	0000010		/* parity error */
#define	x_UPER1_RMR	0000004		/* register modification refused */
#define	x_UPER1_ILR	0000002		/* illegal register */
#define	x_UPER1_ILF	0000001		/* illegal function */

#define	x_UPER1_BITS \
"\10\20DCK\17UNS\16OPI\15DTE\14WLE\13IAE\12AOE\11HCRC\10HCE\
\7Ex_CH\6x_WCF\5x_FER\4x_PAR\3x_RMR\2x_ILR\1x_ILF"

/* uphr */
/* write these int uphr and then read back values */
#define	x_UPHR_MAXCYL	0100027		/* max cyl address */
#define	x_UPHR_MAXTRAK	0100030		/* max track address */
#define	x_UPHR_MAXSECT	0100031		/* max sector address */

/* uper2 */
#define	x_UPER2_BSE	0100000		/* bad sector error */
#define	x_UPER2_SKI	0040000		/* seek incomplete */
#define	x_UPER2_OPE	0020000		/* operator plug error */
#define	x_UPER2_IVC	0010000		/* invalid command */
#define	x_UPER2_LSC	0004000		/* loss of sector clock */
#define	x_UPER2_LBC	0002000		/* loss of bit clock */
#define	x_UPER2_MDS	0001000		/* multiple drive select */
#define	x_UPER2_DCU	0000400		/* dc power unsafe */
#define	x_UPER2_DVC	0000200		/* device check */
#define	x_UPER2_ACU	0000100		/* ac power unsafe */
/* bits 5 and 4 are spare */
#define	x_UPER2_DPE	0000010		/* data parity error (0) */
/* bits 2-0 are spare */

#define	x_UPER2_BITS \
"\10\20BSE\17SKI\16OPE\15IVC\14LSC\13LBC\12MDS\11DCU\10DVC\7ACU\4DPE"

/* upof */
#define	x_UPOF_FMT22	0010000		/* 16 bit format */
#define	x_UPOF_ECI	0004000		/* ecc inhibit */
#define	x_UPOF_HCI	0002000		/* header compare inhibit */

/* THE SC21 ACTUALLY JUST IMPLEMENTS ADVANCE/RETARD... */
#define	x_UPOF_P400	0020		/*  +400 uinches */
#define	x_UPOF_M400	0220		/*  -400 uinches */
#define	x_UPOF_P800	0040		/*  +800 uinches */
#define	x_UPOF_M800	0240		/*  -800 uinches */
#define	x_UPOF_P1200	0060		/* +1200 uinches */
#define	x_UPOF_M1200	0260		/* -1200 uinches */
