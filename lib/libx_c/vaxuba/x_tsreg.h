#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)tsreg.h	7.1 (Berkeley) 6/5/86
 */

/*
 * TS11 controller registers
 */
struct	x_tsdevice {
	x_u_short	x_tsdb;		/* data buffer */
	x_u_short	x_tssr;		/* status register */
};

/* Bits in (unibus) status register */
#define	x_TS_SC	0100000		/* special condition (error) */
#define	x_TS_UPE	0040000		/* Unibus parity error */
#define	x_TS_SPE	0020000		/* serial bus parity error */
#define	x_TS_RMR	0010000		/* register modification refused */
#define	x_TS_NXM	0004000		/* nonexistant memory */
#define	x_TS_NBA	0002000		/* need buffer address */
#define	x_TS_XMEM	0001400		/* Unibus xmem bits */
#define	x_TS_SSR	0000200		/* subsytem ready */
#define	x_TS_OFL	0000100		/* off-line */
#define	x_TS_FTC	0000060		/* fatal termination class */
#define	x_TS_TC	0000016		/* termination class */

#define	x_TS_SUCC	000		/* successful termination */
#define	x_TS_ATTN	002		/* attention */
#define	x_TS_ALERT 004		/* tape status alert */
#define	x_TS_REJECT 06		/* function reject */
#define	x_TS_RECOV 010		/* recoverable error */
#define	x_TS_RECNM 012		/* recoverable error, no tape motion */
#define	x_TS_UNREC 014		/* unrecoverable error */
#define	x_TS_FATAL 016		/* fatal error */

#define	x_TSSR_BITS	\
"\10\20SC\17UPE\16SPE\15RMR\14NXM\13NBA\12A17\11A16\10SSR\
\7x_OFL\6x_FC1\5x_FC0\4x_TC2\3x_TC1\2x_TC0\1-"

#define	x_b_repcnt	x_b_bcount
#define	x_b_command	x_b_resid

/* status message */
struct	x_ts_sts {
	x_u_short	x_s_sts;		/* packet header */
	x_u_short	x_s_len;		/* packet length */
	x_u_short x_s_rbpcr;	/* residual frame count */
	x_u_short	x_s_xs0;		/* extended status 0 - 3 */
	x_u_short	x_s_xs1;
	x_u_short	x_s_xs2;
	x_u_short	x_s_xs3;
};

/* Error codes in xstat 0 */
#define	x_TS_TMK	0100000		/* tape mark detected */
#define	x_TS_RLS	0040000		/* record length short */
#define	x_TS_LET	0020000		/* logical end of tape */
#define	x_TS_RLL	0010000		/* record length long */
#define	x_TS_WLE	0004000		/* write lock error */
#define	x_TS_NEF	0002000		/* non-executable function */
#define	x_TS_ILC	0001000		/* illegal command */
#define	x_TS_ILA	0000400		/* illegal address */
#define	x_TS_MOT	0000200		/* capstan is moving */
#define	x_TS_ONL	0000100		/* on-line */
#define	x_TS_IES	0000040		/* interrupt enable status */
#define	x_TS_VCK	0000020		/* volume check */
#define	x_TS_PED	0000010		/* phase-encoded drive */
#define	x_TS_WLK	0000004		/* write locked */
#define	x_TS_BOT	0000002		/* beginning of tape */
#define	x_TS_EOT	0000001		/* end of tape */

#define	x_TSXS0_BITS	\
"\10\20TMK\17RLS\16LET\15RLL\14WLE\13NEF\12ILC\11ILA\10MOT\
\7x_ONL\6x_IES\5x_VCK\4x_PED\3x_WLK\2x_BOT\1Ex_OT"

/* Error codes in xstat 1 */
#define	x_TS_DLT	0100000		/* data late */
#define	x_TS_COR	0020000		/* correctable data */
#define	x_TS_CRS	0010000		/* crease detected */
#define	x_TS_TIG	0004000		/* trash in the gap */
#define	x_TS_DBF	0002000		/* deskew buffer full */
#define	x_TS_SCK	0001000		/* speed check */
#define	x_TS_IPR	0000200		/* invalid preamble */
#define	x_TS_SYN	0000100		/* synchronization failure */
#define	x_TS_IPO	0000040		/* invalid postamble */
#define	x_TS_IED	0000020		/* invalid end of data */
#define	x_TS_POS	0000010		/* postamble short */
#define	x_TS_POL	0000004		/* postamble long */
#define	x_TS_UNC	0000002		/* uncorrectable data */
#define	x_TS_MTE	0000001		/* multitrack error */

#define	x_TSXS1_BITS	\
"\10\20DLT\17-\16COR\15CRS\14TIG\13DBF\12SCK\11-\10IPR\
\7x_SYN\6x_IPO\5x_IED\4x_POS\3x_POL\2Ux_NC\1x_MTE"

/* Error codes in xstat 2 */
#define	x_TS_OPM	0100000		/* operation in progress */
#define	x_TS_SIP	0040000		/* silo parity error */
#define	x_TS_BPE	0020000		/* serial bus parity error */
#define	x_TS_CAF	0010000		/* capstan acceleration failure */
#define	x_TS_WCF	0002000		/* write card fail */
#define	x_TS_DTP	0000400		/* dead track parity */
#define	x_TS_DT	0000377		/* dead tracks */

#define	x_TSXS2_BITS	\
"\10\20OPM\17SIP\16BPE\15CAF\14-\13WCF\12-\11DTP"

/* Error codes in xstat 3 */
#define	x_TS_MEC	0177400		/* microdiagnostic error code */
#define	x_TS_LMX	0000200		/* limit exceeded */
#define	x_TS_OPI	0000100		/* operation incomplete */
#define	x_TS_REV	0000040		/* reverse */
#define	x_TS_CRF	0000020		/* capstan response fail */
#define	x_TS_DCK	0000010		/* density check */
#define	x_TS_NOI	0000004		/* noise record */
#define	x_TS_LXS	0000002		/* limit exceeded statically */
#define	x_TS_RIB	0000001		/* reverse into BOT */

#define	x_TSXS3_BITS	\
"\10\10LMX\7OPI\6REV\5CRF\4DCK\3NOI\2LXS\1RIB"


/* command message */
struct x_ts_cmd {
	x_u_short	x_c_cmd;		/* command */
	x_u_short	x_c_loba;		/* low order buffer address */
	x_u_short	x_c_hiba;		/* high order buffer address */
#define	x_c_repcnt x_c_loba
	x_u_short	x_c_size;		/* byte count */
};

/* commands and command bits */
#define	x_TS_ACK		0100000		/* ack - release command packet */
#define	x_TS_CVC		0040000		/* clear volume check */
#define	x_TS_IE		0000200
#define	x_TS_RCOM		0000001
#define	x_TS_REREAD	0001001		/* read data retry */
#define	x_TS_SETCHR	0000004		/* set characteristics */
#define	x_TS_WCOM		0000005
#define	x_TS_REWRITE	0001005		/* write data retry */
#define	x_TS_RETRY	0001000		/* retry bit for read and write */
#define	x_TS_SFORW	0000010		/* forward space record */
#define	x_TS_SREV		0000410		/* reverse space record */
#define	x_TS_SFORWF	0001010		/* forward space file */
#define	x_TS_SREVF	0001410		/* reverse space file */
#define	x_TS_REW		0002010		/* rewind */
#define	x_TS_OFFL		0000412		/* unload */
#define	x_TS_WEOF		0000011		/* write tape mark */
#define	x_TS_SENSE	0000017		/* get status */

/* characteristics data */
struct x_ts_char {
	x_long	x_char_addr;		/* address of status packet */
	x_u_short	x_char_size;		/* its size */
	x_u_short	x_char_mode;		/* characteristics */
};


/* characteristics */
#define	x_TS_ESS	0200		/* enable skip tape marks stop */
#define	x_TS_ENB	0100		/* ??? */
#define	x_TS_EAI	0040		/* enable attention interrupts */
#define	x_TS_ERI	0020		/* enable message buffer release interrupts */
