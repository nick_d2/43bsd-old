#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)pdma.h	7.1 (Berkeley) 6/5/86
 */

struct x_pdma {
	struct	x_dzdevice *x_p_addr;
	char	*x_p_mem;
	char	*x_p_end;
	x_int	x_p_arg;
	x_int	(*x_p_fcn)();
};
