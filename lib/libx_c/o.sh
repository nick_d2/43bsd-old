#!/bin/sh

(cd temp_c; patch --strip 1) <temp_c.patch
(cd temp_h; patch --strip 1) <temp_h.patch

all_c=`cat all_c.txt`
echo "all_c=$all_c" >&2
all_h=`find temp_h -name '*.h' -print |sed -e 's:^temp_h/::'`
echo "all_h=$all_h" >&2
echo >&2

find temp_c -name '*.c' -print |\
sed -e 's:^temp_c/::' |\
while read i
do
  echo "i=$i" >&2
  ../../cproto-4.6/cproto -itemp_h -Dvax -t -S temp_c/$i |grep -v '^/\*' >a
  if test -s a
  then
    (
      cat <<EOF
#ifndef __P
#ifdef __STDC__
#define __P(args) args
#else
#define __P(args) ()
#endif
#endif

EOF
      cat a
      echo
    ) >temp_c/$i.static
    cp temp_c/$i.static a
  fi
  ../../cproto-4.6/cproto -itemp_h -Dvax -t -H -j temp_c/$i <a |grep -v '^/\*' >b
  if test -s b
  then
    (
      echo "/* $i */" 
      cat b
    ) >temp_c/$i.extern
  fi
  funcdefs=`sed -ne 's/^.*[^0-9A-Za-z_]\([A-Za-z_][0-9A-Za-z_]*\) __P((.*$/\1/p' b`
  echo "funcdefs=$funcdefs" >&2
  cfiles=`for j in $funcdefs; do grep -H "^$j[	 ]*(" $all_c; grep -H "[^0-9A-Za-z_]$j[	 ]*(" $all_c; done |sed -e 's/:.*//' |sort |uniq`
  echo "cfiles=$cfiles" >&2
  if test -n "$cfiles"
  then
    hfiles=`cd temp_h; for j in $funcdefs; do grep -H "^$j[	 ]*(" $all_h; grep -H "[^0-9A-Za-z_]$j[	 ]*(" $all_h; done |sed -e 's/:.*//' |sort |uniq`
    echo "hfiles=$hfiles" >&2
    if test -z "$hfiles"
    then
      sed -e 's/register //g' -e 's/ __P((\|, /&\n/g' <b >c
      structs=`sed -ne 's/^struct[\t ]*\([A-Za-z_][0-9A-Za-z_]*\).*$/\1/p' c`
      echo "structs=$structs" >&2
      unions=`sed -ne 's/^union[\t ]*\([A-Za-z_][0-9A-Za-z_]*\).*$/\1/p' c`
      echo "unions=$unions" >&2
      typedefs=`sed -ne 's/^\([A-Z][0-9A-Za-z_]*\).*$/\1/p' c; sed -ne 's/^\([A-Za-z_][0-9A-Za-z_]*_t\)[^0-9A-Za-z_].*$/\1/p' c`
      echo "typedefs=$typedefs" >&2
      hfiles=`cd temp_h; (for j in $structs; do grep -H "struct[	 ]\+$j" $all_h; done; for j in $unions; do grep -H "union[	 ]\+$j" $all_h; done; for j in $typedefs; do grep -H "$j" $all_h; done) |sed -e 's/:.*//' |sort |uniq`
      echo "hfiles=$hfiles" >&2
      if test -z "$hfiles"
      then
        hfiles="$all_h"
        echo "hfiles=$hfiles" >&2
      fi
    fi
    for j in $hfiles
    do
      echo "j=$j" >&2
      count=`(grep "^#[	 ]*include[	 ]*<$j>" $cfiles; grep "^#[	 ]*include[	 ]*\"$j\"" $cfiles) |wc -l`
      echo "count=$count" >&2
      echo "$count $j"
    done |sort -k 1nr >temp_c/$i.hfiles
  fi
  echo >&2
done

(cd temp_c; patch --strip 1 -R) <temp_c.patch
(cd temp_h; patch --strip 1 -R) <temp_h.patch
