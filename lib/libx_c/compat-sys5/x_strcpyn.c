#include "x_.h"

#include <x_string.h>
/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)strcpyn.c	4.3 (Berkeley) 3/9/86";
#endif

/*
 * Copy s2 to s1, truncating or null-padding to always copy n bytes
 * return s1
 */

char *x_strcpyn(x_s1, x_s2, x_n) register char *x_s1; register char *x_s2; x_int x_n; {
	register x_int x_i;
	register char *x_os1;

	x_os1 = x_s1;
	for (x_i = 0; x_i < x_n; x_i++)
		if ((*x_s1++ = *x_s2++) == '\0') {
			while (++x_i < x_n)
				*x_s1++ = '\0';
			return(x_os1);
		}
	return(x_os1);
}
