#include "x_.h"

#include <x_stdlib.h>
#include <x_strings.h>
#include <x_unistd.h>
/*
 * Copyright (c) 1985 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)getopt.c	4.3 (Berkeley) 3/9/86";
#endif

#include <x_stdio.h>

/*
 * get option letter from argument vector
 */
x_int	x_opterr = 1,		/* if error message should be printed */
	x_optind = 1,		/* index into parent argv vector */
	x_optopt;			/* character checked for validity */
char	*x_optarg;		/* argument associated with option */

#define x_BADCH	(x_int)'?'
#define x_EMSG	""
#define x_tell(x_s)	if (x_opterr) {x_fputs(*x_nargv,x_stderr);x_fputs(x_s,x_stderr); \
		x_fputc(x_optopt,x_stderr);x_fputc('\n',x_stderr);return(x_BADCH);}

x_int x_getopt(x_nargc, x_nargv, x_ostr) x_int x_nargc; char **x_nargv; char *x_ostr; {
	static char	*x_place = x_EMSG;	/* option letter processing */
	register char	*x_oli;		/* option letter list index */
	char	*x_index();

	if(!*x_place) {			/* update scanning pointer */
		if(x_optind >= x_nargc || *(x_place = x_nargv[x_optind]) != '-' || !*++x_place) return(x_EOF);
		if (*x_place == '-') {	/* found "--" */
			++x_optind;
			return(x_EOF);
		}
	}				/* option letter okay? */
	if ((x_optopt = (x_int)*x_place++) == (x_int)':' || !(x_oli = x_index(x_ostr,x_optopt))) {
		if(!*x_place) ++x_optind;
		x_tell(": illegal option -- ");
	}
	if (*++x_oli != ':') {		/* don't need argument */
		x_optarg = x_NULL;
		if (!*x_place) ++x_optind;
	}
	else {				/* need an argument */
		if (*x_place) x_optarg = x_place;	/* no white space */
		else if (x_nargc <= ++x_optind) {	/* no arg */
			x_place = x_EMSG;
			x_tell(": option requires an argument -- ");
		}
	 	else x_optarg = x_nargv[x_optind];	/* white space */
		x_place = x_EMSG;
		++x_optind;
	}
	return(x_optopt);			/* dump back option letter */
}
