#include "x_.h"

#include <x_string.h>
#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)strrchr.c	5.2 (berkeley) 86/03/09";
#endif

/*
 * Return the ptr in sp at which the character c last
 * appears; NULL if not found
 *
 * This routine is just "rindex" renamed.
 */

#define x_NULL 0

char *x_strrchr(x_sp, x_c) register char *x_sp; x_int x_c; {
	register char *x_r;

	x_r = x_NULL;
	do {
		if (*x_sp == x_c)
			x_r = x_sp;
	} while (*x_sp++);
	return(x_r);
}
