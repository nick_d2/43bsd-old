#include "x_.h"

#include <x_string.h>
/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)strcmpn.c	4.3 (Berkeley) 3/9/86";
#endif

/*
 * Compare strings (at most n bytes):  s1>s2: >0  s1==s2: 0  s1<s2: <0
 */

x_int x_strcmpn(x_s1, x_s2, x_n) register char *x_s1; register char *x_s2; register x_int x_n; {

	while (--x_n >= 0 && *x_s1 == *x_s2++)
		if (*x_s1++ == '\0')
			return(0);
	return(x_n<0 ? 0 : *x_s1 - *--x_s2);
}
