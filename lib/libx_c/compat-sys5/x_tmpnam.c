#include "x_.h"

#include <x_stdio.h>
#include <x_unistd.h>
/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)tmpnam.c	4.3 (Berkeley) 3/9/86";
#endif

char *x_tmpnam(x_s) char *x_s; {
	static x_int x_seed;

	x_sprintf(x_s, "temp.%d.%d", x_getpid(), x_seed++);
	return(x_s);
}
