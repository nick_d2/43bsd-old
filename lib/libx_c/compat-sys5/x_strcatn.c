#include "x_.h"

#include <x_string.h>
/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)strcatn.c	4.3 (Berkeley) 3/9/86";
#endif

/*
 * Concatenate s2 on the end of s1.  S1's space must be large enough.
 * At most n characters are moved.
 * Return s1.
 */

char *x_strcatn(x_s1, x_s2, x_n) register char *x_s1; register char *x_s2; register x_int x_n; {
	register char *x_os1;

	x_os1 = x_s1;
	while (*x_s1++)
		;
	--x_s1;
	while (*x_s1++ = *x_s2++)
		if (--x_n < 0) {
			*--x_s1 = '\0';
			break;
		}
	return(x_os1);
}
