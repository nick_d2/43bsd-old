#include "x_.h"

#include <x_string.h>
#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)strchr.c	5.2 (Berkeley) 86/03/09";
#endif

/*
 * Return the ptr in sp at which the character c appears;
 * NULL if not found
 *
 * this routine is just "index" renamed.
 */

#define	x_NULL	0

char *x_strchr(x_sp, x_c) register char *x_sp; x_int x_c; {
	do {
		if (*x_sp == x_c)
			return(x_sp);
	} while (*x_sp++);
	return(x_NULL);
}
