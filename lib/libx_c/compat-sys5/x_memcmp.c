#include "x_.h"

#include <x_memory.h>
/*
 * Copyright (c) 1985 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

/*
 * Sys5 compat routine
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)memcmp.c	5.2 (Berkeley) 86/03/09";
#endif

x_int x_memcmp(x_s1, x_s2, x_n) register char *x_s1; register char *x_s2; register x_int x_n; {
	while (--x_n >= 0)
		if (*x_s1++ != *x_s2++)
			return (*--x_s1 - *--x_s2);
	return (0);
}
