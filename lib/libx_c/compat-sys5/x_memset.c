#include "x_.h"

#include <x_memory.h>
/*
 * Copyright (c) 1985 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

/*
 * Sys5 compat routine
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)memset.c	5.2 (Berkeley) 86/03/09";
#endif

char *x_memset(x_s, x_c, x_n) register char *x_s; register x_int x_c; register x_int x_n; {
	register char *x_p = x_s;

	while (--x_n >= 0)
		*x_s++ = x_c;

	return (x_p);
}
