#include "x_.h"

#include <x_strings.h>
#include <x_string.h>
/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

/*
 * Sys5 compat routine
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)strtok.c	5.2 (Berkeley) 86/03/09";
#endif

char *x_strtok(x_s, x_sep) register char *x_s; register char *x_sep; {
	register char *x_p;
	register x_int x_c;
	static char *x_lasts;

	if (x_s == 0)
		x_s = x_lasts;
	if (x_s == 0)
		return (0);

	while (x_c = *x_s) {
		if (!x_index(x_sep, x_c))
			break;
		x_s++;
	}

	if (x_c == '\0') {
		x_lasts = 0;
		return (0);
	}

	for (x_p = x_s; x_c = *++x_p; )
		if (x_index(x_sep, x_c))
			break;

	if (x_c == '\0')
		x_lasts = 0;
	else {
		*x_p++ = '\0';
		x_lasts = x_p;
	}
	return (x_s);
}
