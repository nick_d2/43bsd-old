#include "x_.h"

#include <x_string.h>
/*
 * Copyright (c) 1985 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

/*
 * Sys5 compat routine
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)strcspn.c	5.2 (Berkeley) 86/03/09";
#endif

x_int x_strcspn(x_s, x_set) register char *x_s; register char *x_set; {
	register x_int x_n = 0;
	register char *x_p;
	register x_int x_c;

	while (x_c = *x_s++) {
		for (x_p = x_set; *x_p; x_p++)
			if (x_c == *x_p)
				break;
		if (*x_p)
			return (x_n);
		x_n++;
	}
	return (x_n);
}
