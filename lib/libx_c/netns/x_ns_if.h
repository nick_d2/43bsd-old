#include "x_.h"

/*
 * Copyright (c) 1985, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)ns_if.h	7.1 (Berkeley) 6/5/86
 */

/*
 * Interface address, xerox version.  One of these structures
 * is allocated for each interface with an internet address.
 * The ifaddr structure contains the protocol-independent part
 * of the structure and is assumed to be first.
 */

struct x_ns_ifaddr {
	struct	x_ifaddr x_ia_ifa;		/* protocol-independent info */
#define	x_ia_addr	x_ia_ifa.x_ifa_addr
#define	x_ia_broadaddr	x_ia_ifa.x_ifa_broadaddr
#define	x_ia_dstaddr	x_ia_ifa.x_ifa_dstaddr
#define	x_ia_ifp		x_ia_ifa.x_ifa_ifp
	union	x_ns_net	x_ia_net;		/* network number of interface */
	x_int	x_ia_flags;
	struct	x_ns_ifaddr *x_ia_next;	/* next in list of internet addresses */
};

/*
 * Given a pointer to an ns_ifaddr (ifaddr),
 * return a pointer to the addr as a sockadd_ns.
 */

#define	x_IA_SNS(x_ia) ((struct x_sockaddr_ns *)(&((struct x_ns_ifaddr *)x_ia)->x_ia_addr))
/*
 * ia_flags
 */
#define	x_IFA_ROUTE	0x01		/* routing entry installed */

/* This is not the right place for this but where is? */
#define	x_ETHERTYPE_NS	0x0600

#ifdef	x_NSIP
struct x_nsip_req {
	struct x_sockaddr x_rq_ns;	/* must be ns format destination */
	struct x_sockaddr x_rq_ip;	/* must be ip format gateway */
	x_short x_rq_flags;
};
#endif

#ifdef	x_KERNEL
struct	x_ns_ifaddr *x_ns_ifaddr;
struct	x_ns_ifaddr *x_ns_iaonnetof();
struct	x_ifqueue	x_nsintrq;	/* XNS input packet queue */
#endif
