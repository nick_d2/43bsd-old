#include "x_.h"

/*
 * Copyright (c) 1984, 1985, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)spp_var.h	7.1 (Berkeley) 6/5/86
 */

/*
 * Sp control block, one per connection
 */
struct x_sppcb {
	struct	x_spidp_q	x_s_q;		/* queue for out-of-order receipt */
	struct	x_nspcb	*x_s_nspcb;	/* backpointer to internet pcb */
	x_u_char	x_s_state;
	x_u_char	x_s_flags;
#define x_SF_AK	0x01			/* Acknowledgement requested */
#define x_SF_DELACK 0x02			/* Ak, waiting to see if we xmit*/
#define x_SF_HI	0x04			/* Show headers on input */
#define x_SF_HO	0x08			/* Show headers on output */
#define x_SF_PI	0x10			/* Packet (datagram) interface */
	x_u_short x_s_mtu;			/* Max packet size for this stream */
/* use sequence fields in headers to store sequence numbers for this
   connection */
	struct	x_spidp x_s_shdr;		/* prototype header to transmit */
#define x_s_cc x_s_shdr.x_si_cc		/* connection control (for EM bit) */
#define x_s_dt x_s_shdr.x_si_dt		/* datastream type */
#define x_s_sid x_s_shdr.x_si_sid		/* source connection identifier */
#define x_s_did x_s_shdr.x_si_did		/* destination connection identifier */
#define x_s_seq x_s_shdr.x_si_seq		/* sequence number */
#define x_s_ack x_s_shdr.x_si_ack		/* acknowledge number */
#define x_s_alo x_s_shdr.x_si_alo		/* allocation number */
#define x_s_dport x_s_shdr.x_si_dna.x_x_port	/* where we are sending */
	struct x_sphdr x_s_rhdr;		/* last received header (in effect!)*/
	x_u_short x_s_rack;			/* their acknowledge number */
	x_u_short x_s_ralo;			/* their allocation number */
	x_u_short x_s_snt;			/* highest packet # we have sent */

/* timeout stuff */
	x_short	x_s_idle;			/* time idle */
	x_short	x_s_timer[x_TCPT_NTIMERS];	/* timers */
	x_short	x_s_rxtshift;		/* log(2) of rexmt exp. backoff */
	x_u_short	x_s_rtseq;		/* packet being timed */
	x_short	x_s_rtt;			/* timer for round trips */
	x_short	x_s_srtt;			/* averaged timer */
	char	x_s_force;		/* which timer expired */

/* out of band data */
	char	x_s_oobflags;
#define x_SF_SOOB	0x08			/* sending out of band data */
#define x_SF_IOOB 0x10			/* receiving out of band data */
	char	x_s_iobc;			/* input characters */
/* debug stuff */
	x_u_short	x_s_want;			/* Last candidate for sending */
};

#define	x_nstosppcb(x_np)	((struct x_sppcb *)(x_np)->x_nsp_pcb)
#define	x_sotosppcb(x_so)	(x_nstosppcb(x_sotonspcb(x_so)))

struct	x_spp_istat {
	x_short	x_hdrops;
	x_short	x_badsum;
	x_short	x_badlen;
	x_short	x_slotim;
	x_short	x_fastim;
	x_short	x_nonucn;
	x_short	x_noconn;
	x_short	x_notme;
	x_short	x_wrncon;
	x_short	x_bdreas;
	x_short	x_gonawy;
	x_short	x_notyet;
	x_short	x_lstdup;
};

#ifdef x_KERNEL
struct x_spp_istat x_spp_istat;
x_u_short x_spp_iss;
extern struct x_sppcb *x_spp_close(), *x_spp_disconnect(),
	*x_spp_usrclosed(), *x_spp_timers(), *x_spp_drop();
#endif

#define	x_SPP_ISSINCR	128
/*
 * SPP sequence numbers are 16 bit integers operated
 * on with modular arithmetic.  These macros can be
 * used to compare such integers.
 */
#define	x_SSEQ_LT(x_a,x_b)	(((x_short)((x_a)-(x_b))) < 0)
#define	x_SSEQ_LEQ(x_a,x_b)	(((x_short)((x_a)-(x_b))) <= 0)
#define	x_SSEQ_GT(x_a,x_b)	(((x_short)((x_a)-(x_b))) > 0)
#define	x_SSEQ_GEQ(x_a,x_b)	(((x_short)((x_a)-(x_b))) >= 0)
