#include "x_.h"

/*
 * Copyright (c) 1984, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)idp_var.h	7.1 (Berkeley) 6/5/86
 */

/*
 * IDP Kernel Structures and Variables
 */
struct	x_idpstat {
	x_int	x_idps_badsum;		/* checksum bad */
	x_int	x_idps_tooshort;		/* packet too short */
	x_int	x_idps_toosmall;		/* not enough data */
	x_int	x_idps_badhlen;		/* ip header length < data size */
	x_int	x_idps_badlen;		/* ip length < ip header length */
};

#ifdef x_KERNEL
struct	x_idpstat	x_idpstat;
#endif
