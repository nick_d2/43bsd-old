#include "x_.h"

/*
 * Copyright (c) 1984, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)idp.h	7.1 (Berkeley) 6/5/86
 */

/*
 * Definitions for NS(tm) Internet Datagram Protocol
 */
struct x_idp {
	x_u_short	x_idp_sum;	/* Checksum */
	x_u_short	x_idp_len;	/* Length, in bytes, including header */
	x_u_char	x_idp_tc;		/* Transport Crontrol (i.e. hop count) */
	x_u_char	x_idp_pt;		/* Packet Type (i.e. level 2 protocol) */
	struct x_ns_addr	x_idp_dna;	/* Destination Network Address */
	struct x_ns_addr	x_idp_sna;	/* Source Network Address */
};
