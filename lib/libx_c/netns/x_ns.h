#include "x_.h"

/*
 * Copyright (c) 1984, 1985, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)ns.h	7.1 (Berkeley) 6/5/86
 */

/*
 * Constants and Structures defined by the Xerox Network Software
 * per "Internet Transport Protocols", XSIS 028112, December 1981
 */

/*
 * Protocols
 */
#define x_NSPROTO_RI	1		/* Routing Information */
#define x_NSPROTO_ECHO	2		/* Echo Protocol */
#define x_NSPROTO_ERROR	3		/* Error Protocol */
#define x_NSPROTO_PE	4		/* Packet Exchange */
#define x_NSPROTO_SPP	5		/* Sequenced Packet */
#define x_NSPROTO_RAW	255		/* Placemarker*/
#define x_NSPROTO_MAX	256		/* Placemarker*/


/*
 * Port/Socket numbers: network standard functions
 */

#define x_NSPORT_RI	1		/* Routing Information */
#define x_NSPORT_ECHO	2		/* Echo */
#define x_NSPORT_RE	3		/* Router Error */

/*
 * Ports < NSPORT_RESERVED are reserved for priveleged
 * processes (e.g. root).
 */
#define x_NSPORT_RESERVED		3000

/* flags passed to ns_output as last parameter */

#define	x_NS_FORWARDING		0x1	/* most of idp header exists */
#define	x_NS_ROUTETOIF		0x10	/* same as SO_DONTROUTE */
#define	x_NS_ALLOWBROADCAST	x_SO_BROADCAST	/* can send broadcast packets */

#define x_NS_MAXHOPS		15

/* flags passed to get/set socket option */
#define	x_SO_HEADERS_ON_INPUT	1
#define	x_SO_HEADERS_ON_OUTPUT	2
#define	x_SO_DEFAULT_HEADERS	3
#define	x_SO_LAST_HEADER		4
#define	x_SO_NSIP_ROUTE		5
#define x_SO_SEQNO		6
#define	x_SO_ALL_PACKETS		7
#define x_SO_MTU			8


/*
 * NS addressing
 */
union x_ns_host {
	x_u_char	x_c_host[6];
	x_u_short	x_s_host[3];
};

union x_ns_net {
	x_u_char	x_c_net[4];
	x_u_short	x_s_net[2];
};

union x_ns_net_u {
	union x_ns_net	x_net_e;
	x_u_long		x_long_e;
};

struct x_ns_addr {
	union x_ns_net	x_x_net;
	union x_ns_host	x_x_host;
	x_u_short	x_x_port;
};

/*
 * Socket address, Xerox style
 */
struct x_sockaddr_ns {
	x_u_short		x_sns_family;
	struct x_ns_addr	x_sns_addr;
	char		x_sns_zero[2];
};
#define x_sns_port x_sns_addr.x_x_port

#ifdef x_vax
#define x_ns_netof(x_a) (*(x_long *) & ((x_a).x_x_net)) /* XXX - not needed */
#endif
#define x_ns_neteqnn(x_a,x_b) (((x_a).x_s_net[0]==(x_b).x_s_net[0]) && \
					((x_a).x_s_net[1]==(x_b).x_s_net[1]))
#define x_ns_neteq(x_a,x_b) x_ns_neteqnn((x_a).x_x_net, (x_b).x_x_net)
#define x_satons_addr(x_sa)	(((struct x_sockaddr_ns *)&(x_sa))->x_sns_addr)
#define x_ns_hosteqnh(x_s,x_t) ((x_s).x_s_host[0] == (x_t).x_s_host[0] && \
	(x_s).x_s_host[1] == (x_t).x_s_host[1] && (x_s).x_s_host[2] == (x_t).x_s_host[2])
#define x_ns_hosteq(x_s,x_t) (x_ns_hosteqnh((x_s).x_x_host,(x_t).x_x_host))
#define x_ns_nullhost(x_x) (((x_x).x_x_host.x_s_host[0]==0) && \
	((x_x).x_x_host.x_s_host[1]==0) && ((x_x).x_x_host.x_s_host[2]==0))

#if !defined(x_vax) && !defined(x_ntohl) && !defined(x_lint)
/*
 * Macros for number representation conversion.
 */
#define	x_ntohl(x_x)	(x_x)
#define	x_ntohs(x_x)	(x_x)
#define	x_htonl(x_x)	(x_x)
#define	x_htons(x_x)	(x_x)
#endif

#if !defined(x_ntohl) && (defined(x_vax) || defined(x_lint))
x_u_short	x_ntohs(), x_htons();
x_u_long	x_ntohl(), x_htonl();
#endif

#ifdef x_KERNEL
extern struct x_domain x_nsdomain;
union x_ns_host x_ns_thishost;
union x_ns_host x_ns_zerohost;
union x_ns_host x_ns_broadhost;
union x_ns_net x_ns_zeronet;
union x_ns_net x_ns_broadnet;
x_u_short x_ns_cksum();
#endif

#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

/* ns/ns_addr.c */
struct x_ns_addr x_ns_addr __P((char *x_name));
/* ns/ns_ntoa.c */
char *x_ns_ntoa __P((struct x_ns_addr x_addr));
