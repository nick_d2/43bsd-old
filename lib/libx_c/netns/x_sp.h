#include "x_.h"

/*
 * Copyright (c) 1984, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)sp.h	7.1 (Berkeley) 6/5/86
 */

/*
 * Definitions for Xerox NS style sequenced packet protocol
 */

struct x_sphdr {
	x_u_char	x_sp_cc;		/* connection control */
	x_u_char	x_sp_dt;		/* datastream type */
#define	x_SP_SP	0x80		/* system packet */
#define	x_SP_SA	0x40		/* send acknowledgement */
#define	x_SP_OB	0x20		/* attention (out of band data) */
#define	x_SP_EM	0x10		/* end of message */
	x_u_short	x_sp_sid;		/* source connection identifier */
	x_u_short	x_sp_did;		/* destination connection identifier */
	x_u_short	x_sp_seq;		/* sequence number */
	x_u_short	x_sp_ack;		/* acknowledge number */
	x_u_short	x_sp_alo;		/* allocation number */
};
