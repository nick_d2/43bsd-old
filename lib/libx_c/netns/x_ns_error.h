#include "x_.h"

/*
 * Copyright (c) 1984, 1985, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)ns_error.h	7.1 (Berkeley) 6/5/86
 */

/*
 * Xerox NS error messages
 */

struct x_ns_errp {
	x_u_short		x_ns_err_num;		/* Error Number */
	x_u_short		x_ns_err_param;		/* Error Parameter */
	struct x_idp	x_ns_err_idp;		/* Initial segment of offending
						   packet */
	x_u_char		x_ns_err_lev2[12];	/* at least this much higher
						   level protocol */
};
struct  x_ns_epidp {
	struct x_idp x_ns_ep_idp;
	struct x_ns_errp x_ns_ep_errp;
};

#define	x_NS_ERR_UNSPEC	0	/* Unspecified Error detected at dest. */
#define	x_NS_ERR_BADSUM	1	/* Bad Checksum detected at dest */
#define	x_NS_ERR_NOSOCK	2	/* Specified socket does not exist at dest*/
#define	x_NS_ERR_FULLUP	3	/* Dest. refuses packet due to resource lim.*/
#define	x_NS_ERR_UNSPEC_T	0x200	/* Unspec. Error occured before reaching dest*/
#define	x_NS_ERR_BADSUM_T	0x201	/* Bad Checksum detected in transit */
#define	x_NS_ERR_UNREACH_HOST	0x202	/* Dest cannot be reached from here*/
#define	x_NS_ERR_TOO_OLD	0x203	/* Packet x'd 15 routers without delivery*/
#define	x_NS_ERR_TOO_BIG	0x204	/* Packet too large to be forwarded through
				   some intermediate gateway.  The error
				   parameter field contains the max packet
				   size that can be accommodated */
#define x_NS_ERR_ATHOST	4
#define x_NS_ERR_ENROUTE	5
#define x_NS_ERR_MAX (x_NS_ERR_ATHOST + x_NS_ERR_ENROUTE + 1)
#define x_ns_err_x(x_c) (((x_c)&0x200) ? ((x_c) - 0x200 + x_NS_ERR_ATHOST) : x_c )

/*
 * Variables related to this implementation
 * of the network systems error message protocol.
 */
struct	x_ns_errstat {
/* statistics related to ns_err packets generated */
	x_int	x_ns_es_error;		/* # of calls to ns_error */
	x_int	x_ns_es_oldshort;		/* no error 'cuz old ip too short */
	x_int	x_ns_es_oldns_err;	/* no error 'cuz old was ns_err */
	x_int	x_ns_es_outhist[x_NS_ERR_MAX];
/* statistics related to input messages processed */
	x_int	x_ns_es_badcode;		/* ns_err_code out of range */
	x_int	x_ns_es_tooshort;		/* packet < IDP_MINLEN */
	x_int	x_ns_es_checksum;		/* bad checksum */
	x_int	x_ns_es_badlen;		/* calculated bound mismatch */
	x_int	x_ns_es_reflect;		/* number of responses */
	x_int	x_ns_es_inhist[x_NS_ERR_MAX];
};

#ifdef x_KERNEL
struct	x_ns_errstat x_ns_errstat;
#endif
