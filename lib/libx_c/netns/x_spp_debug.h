#include "x_.h"

/*
 * Copyright (c) 1984, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)spp_debug.h	7.1 (Berkeley) 6/5/86
 */

struct	x_spp_debug {
	x_u_long	x_sd_time;
	x_short	x_sd_act;
	x_short	x_sd_ostate;
	x_caddr_t	x_sd_cb;
	x_short	x_sd_req;
	struct	x_spidp x_sd_si;
	struct	x_sppcb x_sd_sp;
};

#define	x_SA_INPUT 	0
#define	x_SA_OUTPUT	1
#define	x_SA_USER		2
#define	x_SA_RESPOND	3
#define	x_SA_DROP		4

#ifdef x_SANAMES
char	*x_sanames[] =
    { "input", "output", "user", "respond", "drop" };
#endif

#define	x_SPP_NDEBUG 100
struct	x_spp_debug x_spp_debug[x_SPP_NDEBUG];
x_int	x_spp_debx;
