#include "x_.h"

/*
 * Copyright (c) 1984, 1985, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)ns_pcb.h	7.1 (Berkeley) 6/5/86
 */

/*
 * Ns protocol interface control block.
 */
struct x_nspcb {
	struct	x_nspcb *x_nsp_next;	/* doubly linked list */
	struct	x_nspcb *x_nsp_prev;
	struct	x_nspcb *x_nsp_head;
	struct	x_socket *x_nsp_socket;	/* back pointer to socket */
	struct	x_ns_addr x_nsp_faddr;	/* destination address */
	struct	x_ns_addr x_nsp_laddr;	/* socket's address */
	x_caddr_t	x_nsp_pcb;		/* protocol specific stuff */
	struct	x_route x_nsp_route;	/* routing information */
	struct	x_ns_addr x_nsp_lastdst;	/* validate cached route for dg socks*/
	x_long	x_nsp_notify_param;	/* extra info passed via ns_pcbnotify*/
	x_short	x_nsp_flags;
	x_u_char	x_nsp_dpt;		/* default packet type for idp_output*/
	x_u_char	x_nsp_rpt;		/* last received packet type by
								idp_input() */
};

/* possible flags */

#define x_NSP_IN_ABORT	0x1		/* calling abort through socket */
#define x_NSP_RAWIN	0x2		/* show headers on input */
#define x_NSP_RAWOUT	0x4		/* show header on output */
#define x_NSP_ALL_PACKETS	0x8		/* Turn off higher proto processing */

#define	x_NS_WILDCARD	1

#define x_nsp_lport x_nsp_laddr.x_x_port
#define x_nsp_fport x_nsp_faddr.x_x_port

#define	x_sotonspcb(x_so)		((struct x_nspcb *)((x_so)->x_so_pcb))

/*
 * Nominal space allocated to a ns socket.
 */
#define	x_NSSNDQ		2048
#define	x_NSRCVQ		2048


#ifdef x_KERNEL
struct	x_nspcb x_nspcb;			/* head of list */
struct	x_nspcb *x_ns_pcblookup();
#endif
