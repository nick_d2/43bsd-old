#include "x_.h"

/*
 * Copyright (c) 1984, 1985, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)spidp.h	7.1 (Berkeley) 6/5/86
 */

/*
 * Definitions for NS(tm) Internet Datagram Protocol
 * containing a Sequenced Packet Protocol packet.
 */
struct x_spidp {
	struct x_idp	x_si_i;
	struct x_sphdr 	x_si_s;
};
struct x_spidp_q {
	struct x_spidp_q	*x_si_next;
	struct x_spidp_q	*x_si_prev;
};
#define x_SI(x_x)	((struct x_spidp *)x_x)
#define x_si_sum	x_si_i.x_idp_sum
#define x_si_len	x_si_i.x_idp_len
#define x_si_tc	x_si_i.x_idp_tc
#define x_si_pt	x_si_i.x_idp_pt
#define x_si_dna	x_si_i.x_idp_dna
#define x_si_sna	x_si_i.x_idp_sna
#define x_si_sport	x_si_i.x_idp_sna.x_x_port
#define x_si_cc	x_si_s.x_sp_cc
#define x_si_dt	x_si_s.x_sp_dt
#define x_si_sid	x_si_s.x_sp_sid
#define x_si_did	x_si_s.x_sp_did
#define x_si_seq	x_si_s.x_sp_seq
#define x_si_ack	x_si_s.x_sp_ack
#define x_si_alo	x_si_s.x_sp_alo
