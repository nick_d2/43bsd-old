#include "x_.h"

/*	time.h	1.1	85/03/13	*/

/*
 * Structure returned by gmtime and localtime calls (see ctime(3)).
 */
struct x_tm {
	x_int	x_tm_sec;
	x_int	x_tm_min;
	x_int	x_tm_hour;
	x_int	x_tm_mday;
	x_int	x_tm_mon;
	x_int	x_tm_year;
	x_int	x_tm_wday;
	x_int	x_tm_yday;
	x_int	x_tm_isdst;
};

extern	struct x_tm *x_gmtime(), *x_localtime();
extern	char *x_asctime(), *x_ctime();

#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

/* gen/ctime.c */
char *x_ctime __P((x_time_t *x_t));
struct x_tm *x_localtime __P((x_time_t *x_tim));
struct x_tm *x_gmtime __P((x_time_t *x_tim));
char *x_asctime __P((struct x_tm *x_t));
x_int x_dysize __P((x_int x_y));
