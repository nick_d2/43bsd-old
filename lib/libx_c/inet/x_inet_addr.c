#include "x_.h"

#include <arpa/x_inet.h>
/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)inet_addr.c	5.2 (Berkeley) 3/9/86";
#endif

#include <sys/x_types.h>
#include <x_ctype.h>
#include <netinet/x_in.h>

/*
 * Internet address interpretation routine.
 * All the network library routines call this
 * routine to interpret entries in the data bases
 * which are expected to be an address.
 * The value returned is in network order.
 */
x_u_long x_inet_addr(x_cp) register char *x_cp; {
	register x_int x_u_long x_val, x_base, x_n;
	register char x_c;
	x_u_long x_parts[4], *x_pp = x_parts;

x_again:
	/*
	 * Collect number up to ``.''.
	 * Values are specified as for C:
	 * 0x=hex, 0=octal, other=decimal.
	 */
	x_val = 0; x_base = 10;
	if (*x_cp == '0')
		x_base = 8, x_cp++;
	if (*x_cp == 'x' || *x_cp == 'X')
		x_base = 16, x_cp++;
	while (x_c = *x_cp) {
		if (x_isdigit(x_c)) {
			x_val = (x_val * x_base) + (x_c - '0');
			x_cp++;
			continue;
		}
		if (x_base == 16 && x_isxdigit(x_c)) {
			x_val = (x_val << 4) + (x_c + 10 - (x_islower(x_c) ? 'a' : 'A'));
			x_cp++;
			continue;
		}
		break;
	}
	if (*x_cp == '.') {
		/*
		 * Internet format:
		 *	a.b.c.d
		 *	a.b.c	(with c treated as 16-bits)
		 *	a.b	(with b treated as 24 bits)
		 */
		if (x_pp >= x_parts + 4)
			return (-1);
		*x_pp++ = x_val, x_cp++;
		goto x_again;
	}
	/*
	 * Check for trailing characters.
	 */
	if (*x_cp && !x_isspace(*x_cp))
		return (-1);
	*x_pp++ = x_val;
	/*
	 * Concoct the address according to
	 * the number of parts specified.
	 */
	x_n = x_pp - x_parts;
	switch (x_n) {

	case 1:				/* a -- 32 bits */
		x_val = x_parts[0];
		break;

	case 2:				/* a.b -- 8.24 bits */
		x_val = (x_parts[0] << 24) | (x_parts[1] & 0xffffff);
		break;

	case 3:				/* a.b.c -- 8.8.16 bits */
		x_val = (x_parts[0] << 24) | ((x_parts[1] & 0xff) << 16) |
			(x_parts[2] & 0xffff);
		break;

	case 4:				/* a.b.c.d -- 8.8.8.8 bits */
		x_val = (x_parts[0] << 24) | ((x_parts[1] & 0xff) << 16) |
		      ((x_parts[2] & 0xff) << 8) | (x_parts[3] & 0xff);
		break;

	x_default:
		return (-1);
	}
	x_val = x_htonl(x_val);
	return (x_val);
}
