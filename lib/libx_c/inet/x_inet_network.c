#include "x_.h"

#include <arpa/x_inet.h>
/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)inet_network.c	5.2 (Berkeley) 3/9/86";
#endif

#include <sys/x_types.h>
#include <x_ctype.h>

/*
 * Internet network address interpretation routine.
 * The library routines call this routine to interpret
 * network numbers.
 */
x_u_long x_inet_network(x_cp) register char *x_cp; {
	register x_int x_u_long x_val, x_base, x_n;
	register char x_c;
	x_u_long x_parts[4], *x_pp = x_parts;
	register x_int x_i;

x_again:
	x_val = 0; x_base = 10;
	if (*x_cp == '0')
		x_base = 8, x_cp++;
	if (*x_cp == 'x' || *x_cp == 'X')
		x_base = 16, x_cp++;
	while (x_c = *x_cp) {
		if (x_isdigit(x_c)) {
			x_val = (x_val * x_base) + (x_c - '0');
			x_cp++;
			continue;
		}
		if (x_base == 16 && x_isxdigit(x_c)) {
			x_val = (x_val << 4) + (x_c + 10 - (x_islower(x_c) ? 'a' : 'A'));
			x_cp++;
			continue;
		}
		break;
	}
	if (*x_cp == '.') {
		if (x_pp >= x_parts + 4)
			return (-1);
		*x_pp++ = x_val, x_cp++;
		goto x_again;
	}
	if (*x_cp && !x_isspace(*x_cp))
		return (-1);
	*x_pp++ = x_val;
	x_n = x_pp - x_parts;
	if (x_n > 4)
		return (-1);
	for (x_val = 0, x_i = 0; x_i < x_n; x_i++) {
		x_val <<= 8;
		x_val |= x_parts[x_i] & 0xff;
	}
	return (x_val);
}
