#include "x_.h"

/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)inet_netof.c	5.2 (Berkeley) 3/9/86";
#endif

#include <sys/x_types.h>
#include <netinet/x_in.h>

/*
 * Return the network number from an internet
 * address; handles class a/b/c network #'s.
 */
x_int x_inet_netof(x_in) struct x_in_addr x_in; {
	register x_int x_u_long x_i = x_ntohl(x_in.x_s_addr);

	if (x_IN_CLASSA(x_i))
		return (((x_i)&x_IN_CLASSA_NET) >> x_IN_CLASSA_NSHIFT);
	else if (x_IN_CLASSB(x_i))
		return (((x_i)&x_IN_CLASSB_NET) >> x_IN_CLASSB_NSHIFT);
	else
		return (((x_i)&x_IN_CLASSC_NET) >> x_IN_CLASSC_NSHIFT);
}
