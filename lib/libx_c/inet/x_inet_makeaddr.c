#include "x_.h"

#include <arpa/x_inet.h>
/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)inet_makeaddr.c	5.1 (Berkeley) 3/11/86";
#endif

#include <sys/x_types.h>
#include <netinet/x_in.h>

/*
 * Formulate an Internet address from network + host.  Used in
 * building addresses stored in the ifnet structure.
 */
struct x_in_addr x_inet_makeaddr(x_net, x_host) x_int x_net; x_int x_host; {
	x_u_long x_addr;

	if (x_net < 128)
		x_addr = (x_net << x_IN_CLASSA_NSHIFT) | (x_host & x_IN_CLASSA_HOST);
	else if (x_net < 65536)
		x_addr = (x_net << x_IN_CLASSB_NSHIFT) | (x_host & x_IN_CLASSB_HOST);
	else
		x_addr = (x_net << x_IN_CLASSC_NSHIFT) | (x_host & x_IN_CLASSC_HOST);
	x_addr = x_htonl(x_addr);
	return (*(struct x_in_addr *)&x_addr);
}
