#include "x_.h"

#include <x_stdio.h>
#include <arpa/x_inet.h>
/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)inet_ntoa.c	5.2 (Berkeley) 3/9/86";
#endif

/*
 * Convert network-format internet address
 * to base 256 d.d.d.d representation.
 */
#include <sys/x_types.h>
#include <netinet/x_in.h>

char *x_inet_ntoa(x_in) struct x_in_addr x_in; {
	static char x_b[18];
	register char *x_p;

	x_p = (char *)&x_in;
#define	x_UC(x_b)	(((x_int)x_b)&0xff)
	x_sprintf(x_b, "%d.%d.%d.%d", x_UC(x_p[0]), x_UC(x_p[1]), x_UC(x_p[2]), x_UC(x_p[3]));
	return (x_b);
}
