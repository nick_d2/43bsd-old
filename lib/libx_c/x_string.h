#include "x_.h"

/*
 * Copyright (c) 1985 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)string.h	5.1 (Berkeley) 85/08/05
 */

#include <x_strings.h>

/*
 * these next few are obsolete trash
 */

extern char *x_strcpyn();
extern char *x_strcatn();
extern x_int x_strcmpn();

/*
 * and the rest are Sys5 functions supported just so
 * Sys5 progs will compile easily.
 */

extern char *x_strchr();
extern char *x_strrchr();
extern char *x_strpbrk();
extern x_int x_strspn();
extern x_int x_strcspn();
extern char *x_strtok();

#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

/* compat-sys5/strcmpn.c */
x_int x_strcmpn __P((register char *x_s1, register char *x_s2, register x_int x_n));
/* compat-sys5/strcspn.c */
x_int x_strcspn __P((register char *x_s, register char *x_set));
/* compat-sys5/strtok.c */
char *x_strtok __P((register char *x_s, register char *x_sep));
/* compat-sys5/strpbrk.c */
char *x_strpbrk __P((register char *x_s, register char *x_brk));
/* compat-sys5/strcatn.c */
char *x_strcatn __P((register char *x_s1, register char *x_s2, register x_int x_n));
/* compat-sys5/strchr.c */
char *x_strchr __P((register char *x_sp, x_int x_c));
/* compat-sys5/strspn.c */
x_int x_strspn __P((register char *x_s, register char *x_set));
/* compat-sys5/strrchr.c */
char *x_strrchr __P((register char *x_sp, x_int x_c));
/* compat-sys5/strcpyn.c */
char *x_strcpyn __P((register char *x_s1, register char *x_s2, x_int x_n));
