#include "x_.h"


#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

/* stdio/exit.c */
x_int x_exit __P((x_int x_code));
/* compat-4.1/rand.c */
x_int x_srand __P((x_unsigned_int x_x));
x_int x_rand __P((void));
/* compat-sys5/getopt.c */
x_int x_getopt __P((x_int x_nargc, char **x_nargv, char *x_ostr));
/* gen/abort.c */
x_int x_abort __P((void));
/* gen/mkstemp.c */
x_int x_mkstemp __P((char *x_as));
/* gen/atol.c */
x_long x_atol __P((register char *x_p));
/* gen/system.c */
x_int x_system __P((char *x_s));
/* gen/getenv.c */
char *x_getenv __P((register char *x_name));
/* gen/valloc.c */
char *x_valloc __P((x_int x_i));
/* gen/calloc.c */
char *x_calloc __P((register x_unsigned_int x_num, register x_unsigned_int x_size));
x_int x_cfree __P((char *x_p, x_unsigned_int x_num, x_unsigned_int x_size));
/* gen/random.c */
x_int x_srandom __P((x_unsigned_int x_x));
char *x_initstate __P((x_unsigned_int x_seed, char *x_arg_state, x_int x_n));
char *x_setstate __P((char *x_arg_state));
x_long x_random __P((void));
/* gen/gcvt.c */
char *x_gcvt __P((double x_number, x_int x_ndigit, char *x_buf));
/* gen/mktemp.c */
char *x_mktemp __P((char *x_as));
/* gen/qsort.c */
x_int x_qsort __P((char *x_base, x_int x_n, x_int x_size, x_int (*x_compar)(void)));
/* gen/atoi.c */
x_int x_atoi __P((register char *x_p));
