#include "x_.h"

/*	ctype.h	4.2	85/09/04	*/

#define	x__U	01
#define	x__L	02
#define	x__N	04
#define	x__S	010
#define x__P	020
#define x__C	040
#define x__X	0100
#define	x__B	0200

extern	char	x__ctype_[];

#define	x_isalpha(x_c)	((x__ctype_+1)[x_c]&(x__U|x__L))
#define	x_isupper(x_c)	((x__ctype_+1)[x_c]&x__U)
#define	x_islower(x_c)	((x__ctype_+1)[x_c]&x__L)
#define	x_isdigit(x_c)	((x__ctype_+1)[x_c]&x__N)
#define	x_isxdigit(x_c)	((x__ctype_+1)[x_c]&(x__N|x__X))
#define	x_isspace(x_c)	((x__ctype_+1)[x_c]&x__S)
#define x_ispunct(x_c)	((x__ctype_+1)[x_c]&x__P)
#define x_isalnum(x_c)	((x__ctype_+1)[x_c]&(x__U|x__L|x__N))
#define x_isprint(x_c)	((x__ctype_+1)[x_c]&(x__P|x__U|x__L|x__N|x__B))
#define x_isgraph(x_c)	((x__ctype_+1)[x_c]&(x__P|x__U|x__L|x__N))
#define x_iscntrl(x_c)	((x__ctype_+1)[x_c]&x__C)
#define x_isascii(x_c)	((unsigned)(x_c)<=0177)
#define x_toupper(x_c)	((x_c)-'a'+'A')
#define x_tolower(x_c)	((x_c)-'A'+'a')
#define x_toascii(x_c)	((x_c)&0177)
