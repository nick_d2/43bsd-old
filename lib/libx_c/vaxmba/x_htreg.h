#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)htreg.h	7.1 (Berkeley) 6/5/86
 */

struct	x_htdevice
{
	x_int	x_htcs1;		/* control status register */
	x_int	x_htds;		/* drive status register */
	x_int	x_hter;		/* error register */
	x_int	x_htmr;		/* maintenance register */
	x_int	x_htas;		/* attention status register */
	x_int	x_htfc;		/* frame counter */
	x_int	x_htdt;		/* drive type register */
	x_int	x_htck;		/* nrzi check (crc) error character */
	x_int	x_htsn;		/* serial number register */
	x_int	x_httc;		/* tape controll register */
};

/* htcs1 */
#define	x_HT_GO		000001		/* go bit */
#define	x_HT_SENSE	000000		/* no operations (sense) */
#define	x_HT_REWOFFL	000002		/* rewind offline */
#define	x_HT_REW		000006		/* rewind */
#define	x_HT_DCLR		000010		/* drive clear */
#define	x_HT_RIP		000020		/* read in preset */
#define	x_HT_ERASE	000024		/* erase */
#define	x_HT_WEOF		000026		/* write tape mark */
#define	x_HT_SFORW	000030		/* space forward */
#define	x_HT_SREV		000032		/* space reverse */
#define	x_HT_WCHFWD	000050		/* write check forward */
#define	x_HT_WCHREV	000056		/* write check reverse */
#define	x_HT_WCOM		000060		/* write forward */
#define	x_HT_RCOM		000070		/* read forward */
#define	x_HT_RREV		000076		/* read reverse */

/* htds */
#define	x_HTDS_ATA	0100000		/* attention active */
#define	x_HTDS_ERR	0040000		/* composite error */
#define	x_HTDS_PIP	0020000		/* positioning in progress */
#define	x_HTDS_MOL	0010000		/* medium on line */
#define	x_HTDS_WRL	0004000		/* write lock */
#define	x_HTDS_EOT	0002000		/* end of tape */
/* bit 9 is unused */
#define	x_HTDS_DPR	0000400		/* drive present (always 1) */
#define	x_HTDS_DRY	0000200		/* drive ready */
#define	x_HTDS_SSC	0000100		/* slave status change */
#define	x_HTDS_PES	0000040		/* phase-encoded status */
#define	x_HTDS_SDWN	0000020		/* settle down */
#define	x_HTDS_IDB	0000010		/* identification burst */
#define	x_HTDS_TM		0000004		/* tape mark */
#define	x_HTDS_BOT	0000002		/* beginning of tape */
#define	x_HTDS_SLA	0000001		/* slave attention */

#define	x_HTDS_BITS \
"\10\20ATA\17ERR\16PIP\15MOL\14WRL\13EOT\11DPR\10DRY\
\7x_SSC\6x_PES\5x_SDWN\4x_IDB\3x_TM\2x_BOT\1x_SLA"

/* hter */
#define	x_HTER_CORCRC	0100000		/* correctible data or ecc */
#define	x_HTER_UNS	0040000		/* unsafe */
#define	x_HTER_OPI	0020000		/* operation incomplete */
#define	x_HTER_DTE	0010000		/* drive timing error */
#define	x_HTER_NEF	0004000		/* non-executable function */
#define	x_HTER_CSITM	0002000		/* correctable skew/illegal tape mark */
#define	x_HTER_FCE	0001000		/* frame count error */
#define	x_HTER_NSG	0000400		/* non-standard gap */
#define	x_HTER_PEFLRC	0000200		/* format error or lrc error */
#define	x_HTER_INCVPE	0000100		/* incorrectable data error or vertical
					   parity error */
#define	x_HTER_DPAR	0000040		/* data parity error */
#define	x_HTER_FMT	0000020		/* format error */
#define	x_HTER_CPAR	0000010		/* control bus parity error */
#define	x_HTER_RMR	0000004		/* register modification refused */
#define	x_HTER_ILR	0000002		/* illegal register */
#define	x_HTER_ILF	0000001		/* illegal function */

#define	x_HTER_BITS \
"\10\20CORCRC\17UNS\16OPI\15DTE\14NEF\13CSITM\12FCE\11NSG\10PEFLRC\
\7x_INCVPE\6Dx_PAR\5x_FMT\4x_CPAR\3x_RMR\2x_ILR\1x_ILF"
#define	x_HTER_HARD \
	(x_HTER_UNS|x_HTER_OPI|x_HTER_NEF|x_HTER_DPAR|x_HTER_FMT|x_HTER_CPAR| \
	x_HTER_RMR|x_HTER_ILR|x_HTER_ILF)

/* htdt */
#define	x_HTDT_NSA	0100000		/* not sector addressed; always 1 */
#define	x_HTDT_TAP	0040000		/* tape; always 1 */
#define	x_HTDT_MOH	0020000		/* moving head; always 0 */
#define	x_HTDT_7CH	0010000		/* 7 channel; always 0 */
#define	x_HTDT_DRQ	0004000		/* drive requested; always 0 */
#define	x_HTDT_SPR	0002000		/* slave present */
/* bit 9 is spare */
/* bits 8-0 are formatter/transport type */

/* httc */
#define	x_HTTC_ACCL	0100000		/* transport is not reading/writing */
#define	x_HTTC_FCS	0040000		/* frame count status */
#define	x_HTTC_SAC	0020000		/* slave address change */
#define	x_HTTC_EAODTE	0010000		/* enable abort on data xfer errors */
/* bits 8-10 are density select */
#define	x_HTTC_800BPI	0001400		/* in bits 8-10, dens=1600 */
#define	x_HTTC_1600BPI	0002000		/* in bits 8-10, dens=800 */
/* bits 4-7 are format select */
#define	x_HTTC_PDP11	0000300		/* in bits 4-7, pdp11 normal format */
#define	x_HTTC_EVEN	0000010		/* select even parity */
/* bits 0 - 2 are slave select */

#define	x_b_repcnt  x_b_bcount
#define	x_b_command x_b_resid
