#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)mbareg.h	7.1 (Berkeley) 6/5/86
 */

/*
 * VAX MASSBUS adapter registers
 */

struct x_mba_regs
{
	x_int	x_mba_csr;		/* configuration register */
	x_int	x_mba_cr;			/* control register */
	x_int	x_mba_sr;			/* status register */
	x_int	x_mba_var;		/* virtual address register */
	x_int	x_mba_bcr;		/* byte count register */
	x_int	x_mba_dr;
	x_int	x_mba_pad1[250];
	struct x_mba_drv {		/* per drive registers */
		x_int	x_mbd_cs1;		/* control status */
		x_int	x_mbd_ds;			/* drive status */
		x_int	x_mbd_er1;		/* error register */
		x_int	x_mbd_mr1;		/* maintenance register */
		x_int	x_mbd_as;			/* attention status */
		x_int	x_mbd_da;			/* desired address (disks) */
#define	x_mbd_fc	x_mbd_da				/* frame count (tapes) */
		x_int	x_mbd_dt;			/* drive type */
		x_int	x_mbd_la;			/* look ahead (disks) */
#define	x_mbd_ck	x_mbd_la				/* ??? (tapes) */
		x_int	x_mbd_sn;			/* serial number */
		x_int	x_mbd_of;			/* ??? */
#define	x_mbd_tc	x_mbd_of				/* ??? */
		x_int	x_mbd_fill[22];
	} x_mba_drv[8];
	struct	x_pte x_mba_map[256];	/* io space virtual map */
	x_int	x_mba_pad2[256*5];	/* to size of a nexus */
};

/*
 * Bits in mba_cr
 */
#define	x_MBCR_INIT	0x1		/* init mba */
#define	x_MBCR_IE		0x4		/* enable mba interrupts */

/*
 * Bits in mba_sr
 */
#define	x_MBSR_DTBUSY	0x80000000	/* data transfer busy */
#define	x_MBSR_NRCONF	0x40000000	/* no response confirmation */
#define	x_MBSR_CRD	0x20000000	/* corrected read data */
#define	x_MBSR_CBHUNG	0x00800000	/* control bus hung */
#define	x_MBSR_PGE	0x00080000	/* programming error */
#define	x_MBSR_NED	0x00040000	/* non-existant drive */
#define	x_MBSR_MCPE	0x00020000	/* massbus control parity error */
#define	x_MBSR_ATTN	0x00010000	/* attention from massbus */
#define	x_MBSR_SPE	0x00004000	/* silo parity error */
#define	x_MBSR_DTCMP	0x00002000	/* data transfer completed */
#define	x_MBSR_DTABT	0x00001000	/* data transfer aborted */
#define	x_MBSR_DLT	0x00000800	/* data late */
#define	x_MBSR_WCKUP	0x00000400	/* write check upper */
#define	x_MBSR_WCKLWR	0x00000200	/* write check lower */
#define	x_MBSR_MXF	0x00000100	/* miss transfer error */
#define	x_MBSR_MBEXC	0x00000080	/* massbus exception */
#define	x_MBSR_MDPE	0x00000040	/* massbus data parity error */
#define	x_MBSR_MAPPE	0x00000020	/* page frame map parity error */
#define	x_MBSR_INVMAP	0x00000010	/* invalid map */
#define	x_MBSR_ERRCONF	0x00000008	/* error confirmation */
#define	x_MBSR_RDS	0x00000004	/* read data substitute */
#define	x_MBSR_ISTIMO	0x00000002	/* interface sequence timeout */
#define	x_MBSR_RDTIMO	0x00000001	/* read data timeout */

#define x_MBSR_BITS \
"\20\40DTBUSY\37NRCONF\36CRD\30CBHUNG\24PGE\23NED\22MCPE\21ATTN\
\17x_SPE\16Dx_TCMP\15Dx_TABT\14Dx_LT\13x_WCKUP\12x_WCKLWR\11x_MXF\10x_MBEXC\7x_MDPE\
\6x_MAPPE\5x_INVMAP\4Ex_RRCONF\3x_RDS\2x_ISTIMO\1x_RDTIMO"

#define	x_MBSR_HARD	(x_MBSR_PGE|x_MBSR_ERRCONF|x_MBSR_ISTIMO|x_MBSR_RDTIMO)

#define x_MBSR_EBITS	(~(x_MBSR_DTBUSY|x_MBSR_CRD|x_MBSR_ATTN|x_MBSR_DTCMP))

#ifdef x_KERNEL
extern	char	x_mbsr_bits[];
#endif

/*
 * Commands for mbd_cs1
 */
#define	x_MB_WCOM		0x30
#define	x_MB_RCOM		0x38
#define	x_MB_GO		0x1

/*
 * Bits in mbd_ds.
 */
#define	x_MBDS_ERR	0x00004000	/* error in drive */
#define	x_MBDS_MOL	0x00001000	/* medium on line */
#define	x_MBDS_DPR	0x00000100	/* drive present */
#define	x_MBDS_DRY	0x00000080	/* drive ready */

#define	x_MBDS_DREADY	(x_MBDS_MOL|x_MBDS_DPR|x_MBDS_DRY)

/*
 * Bits in mbd_dt
 */
#define	x_MBDT_NSA	0x8000		/* not sector addressible */
#define	x_MBDT_TAP	0x4000		/* is a tape */
#define	x_MBDT_MOH	0x2000		/* moving head */
#define	x_MBDT_7CH	0x1000		/* 7 channel */
#define	x_MBDT_DRQ	0x800		/* drive request required */
#define	x_MBDT_SPR	0x400		/* slave present */

#define	x_MBDT_TYPE	0x1ff
#define	x_MBDT_MASK	(x_MBDT_NSA|x_MBDT_TAP|x_MBDT_TYPE)

/* type codes for disk drives */
#define	x_MBDT_RP04	020
#define	x_MBDT_RP05	021
#define	x_MBDT_RP06	022
#define	x_MBDT_RP07	042
#define	x_MBDT_RM02	025
#define	x_MBDT_RM03	024
#define	x_MBDT_RM05	027
#define	x_MBDT_RM80	026
#define	x_MBDT_ML11A	0110
#define	x_MBDT_ML11B	0111

/* type codes for tape drives */
#define	x_MBDT_TM03	050
#define	x_MBDT_TE16	051
#define	x_MBDT_TU45	052
#define	x_MBDT_TU77	054
#define	x_MBDT_TU78	0101
