#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)mtreg.h	7.1 (Berkeley) 6/5/86
 */

/*
 * TU78 registers.
 */

struct x_mtdevice {
	x_int	x_mtcs;		/* control status register */
	x_int	x_mter;		/* error register */
	x_int	x_mtca;		/* command address, rec cnt, skp cnt reg */
	x_int	x_mtmr1;		/* maintenance register */
	x_int	x_mtas;		/* attention summary register */
	x_int	x_mtbc;		/* byte count register */
	x_int	x_mtdt;		/* drive type register */
	x_int	x_mtds;		/* drive status register */
	x_int	x_mtsn;		/* serial number register */
	x_int	x_mtmr2;		/* maintenance register */
	x_int	x_mtmr3;		/* maintenance register */
	x_int	x_mtner;		/* non-data transfer error register */
	x_int	x_mtncs[4];	/* non-data transfer command registers */
	x_int	x_mtia;		/* internal address */
	x_int	x_mtid;		/* internal data */
};

/* mtcs */
#define	x_MT_GO		000001		/* go bit */
#define	x_MT_NOOP		000002		/* no operation */
#define	x_MT_UNLOAD	000004		/* unload tape */
#define	x_MT_REW		000006		/* rewind */
#define	x_MT_SENSE	000010		/* sense */
#define	x_MT_DSE		000012		/* data security erase */
#define	x_MT_WTMPE	000014		/* write phase encoded tape mark */
#define	x_MT_WTM		x_MT_WTMPE	/* generic write tape mark */
#define	x_MT_WTMGCR	000016		/* write GCR tape mark */
#define	x_MT_SFORW	000020		/* space forward record */
#define	x_MT_SREV		000022		/* space reverse record */
#define	x_MT_SFORWF	000024		/* space forward file */
#define	x_MT_SREVF	000026		/* space reverse file */
#define	x_MT_SFORWE	000030		/* space forward either */
#define	x_MT_SREVE	000032		/* space reverse either */
#define	x_MT_ERGPE	000034		/* erase tape, set PE */
#define	x_MT_ERASE	x_MT_ERGPE	/* generic erase tape */
#define	x_MT_ERGGCR	000036		/* erase tape, set GCR */
#define	x_MT_CLSPE	000040		/* close file PE */
#define	x_MT_CLS		x_MT_CLSPE	/* generic close file */
#define	x_MT_CLSGCR	000042		/* close file GCR */
#define	x_MT_SLEOT	000044		/* space to logical EOT */
#define	x_MT_SFLEOT	000046		/* space forward file, stop on LEOT */
#define	x_MT_WCHFWD	000050		/* write check forward */
#define	x_MT_WCHREV	000056		/* write check reverse */
#define	x_MT_WRITEPE	000060		/* write phase encoded */
#define	x_MT_WRITE	x_MT_WRITEPE	/* generic write */
#define	x_MT_WRITEGCR	000062		/* write group coded */
#define	x_MT_READ		000070		/* read forward */
#define	x_MT_EXSNS	000072		/* read extended sense error log */
#define	x_MT_READREV	000076		/* read reverse */
#define	x_MT_GCR		000002		/* make generic ops GCR ops */

/* mtds */
#define	x_MTDS_RDY	0100000		/* tape ready */
#define	x_MTDS_PRES	0040000		/* tape unit has power */
#define	x_MTDS_ONL	0020000		/* online */
#define	x_MTDS_REW	0010000		/* tape rewinding */
#define	x_MTDS_PE		0004000		/* tape set for phase encoded */
#define	x_MTDS_BOT	0002000		/* tape at BOT */
#define	x_MTDS_EOT	0001000		/* tape at EOT */
#define	x_MTDS_FPT	0000400		/* write protected */
#define	x_MTDS_AVAIL	0000200		/* unit available */
#define	x_MTDS_SHR	0000100		/* unit is shared */
#define	x_MTDS_MAINT	0000040		/* maintenance mode */
#define	x_MTDS_DSE	0000020		/* DSE in progress */

#define	x_MTDS_BITS	\
"\10\20RDY\17PRES\16ONL\15REW\14PE\13BOT\12EOT\11FPT\10AVAIL\
\7x_SHR\6x_MAINT\5Dx_SE"

/* mter */
#define	x_MTER_INTCODE	0377		/* mask for interrupt code */
#define x_MTER_FAILCODE	0176000		/* failure code */

/* interrupt codes */
#define	x_MTER_DONE	001		/* operation complete */
#define	x_MTER_TM		002		/* unexpected tape mark */
#define	x_MTER_BOT	003		/* unexpected BOT detected */
#define	x_MTER_EOT	004		/* tape positioned beyond EOT */
#define	x_MTER_LEOT	005		/* unexpected LEOT detected */
#define	x_MTER_NOOP	006		/* no-op completed */
#define	x_MTER_RWDING	007		/* rewinding */
#define	x_MTER_FPT	010		/* write protect error */
#define	x_MTER_NOTRDY	011		/* not ready */
#define	x_MTER_NOTAVL	012		/* not available */
#define	x_MTER_OFFLINE	013		/* offline */
#define	x_MTER_NONEX	014		/* unit does not exist */
#define	x_MTER_NOTCAP	015		/* not capable */
#define	x_MTER_ONLINE	017		/* tape came online */
#define	x_MTER_LONGREC	020		/* long tape record */
#define	x_MTER_SHRTREC	021		/* short tape record */
#define	x_MTER_RETRY	022		/* retry */
#define	x_MTER_RDOPP	023		/* read opposite */
#define	x_MTER_UNREAD	024		/* unreadable */
#define	x_MTER_ERROR	025		/* error */
#define	x_MTER_EOTERR	026		/* EOT error */
#define	x_MTER_BADTAPE	027		/* tape position lost */
#define	x_MTER_TMFLTA	030		/* TM fault A */
#define	x_MTER_TUFLTA	031		/* TU fault A */
#define	x_MTER_TMFLTB	032		/* TM fault B */
#define	x_MTER_MBFLT	034		/* Massbus fault */
#define	x_MTER_KEYFAIL	077		/* keypad entry error */

/* mtdt */
#define	x_MTDT_NSA	0100000		/* not sector addressed; always 1 */
#define	x_MTDT_TAP	0040000		/* tape; always 1 */
#define	x_MTDT_MOH	0020000		/* moving head; always 0 */
#define	x_MTDT_7CH	0010000		/* 7 channel; always 0 */
#define	x_MTDT_DRQ	0004000		/* drive request required */
#define	x_MTDT_SPR	0002000		/* slave present; always 1 ??? */
/* bit 9 is spare */
/* bits 8-0 are formatter/transport type */

/* mtid */
#define	x_MTID_RDY	0100000		/* controller ready */
#define	x_MTID_CLR	0040000		/* controller clear */

#define	x_b_repcnt  x_b_bcount
#define	x_b_command x_b_resid
