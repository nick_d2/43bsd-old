#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)mbavar.h	7.1 (Berkeley) 6/5/86
 */

/*
 * This file contains definitions related to the kernel structures
 * for dealing with the massbus adapters.
 *
 * Each mba has a mba_hd structure.
 * Each massbus device has a mba_device structure.
 * Each massbus slave has a mba_slave structure.
 *
 * At boot time we prowl the structures and fill in the pointers
 * for devices which we find.
 */

/*
 * Per-mba structure.
 *
 * The initialization routine uses the information in the mbdinit table
 * to initialize the what is attached to each massbus slot information.
 * It counts the number of devices on each mba (to see if bothering to
 * search/seek is appropriate).
 *
 * During normal operation, the devices attached to the mba which wish
 * to transfer are queued on the mh_act? links.
 */
struct	x_mba_hd {
	x_short	x_mh_active;		/* set if mba is active */
	x_short	x_mh_ndrive;		/* number of devices, to avoid seeks */
	struct	x_mba_regs *x_mh_mba;	/* virt addr of mba */
	struct	x_mba_regs *x_mh_physmba;	/* phys addr of mba */
	struct	x_mba_device *x_mh_mbip[8];	/* what is attached to each dev */
	struct	x_mba_device *x_mh_actf;	/* head of queue to transfer */
	struct	x_mba_device *x_mh_actl;	/* tail of queue to transfer */
};

/*
 * Per-device structure
 * (one for each RM/RP disk, and one for each tape formatter).
 *
 * This structure is used by the device driver as its argument
 * to the massbus driver, and by the massbus driver to locate
 * the device driver for a particular massbus slot.
 *
 * The device drivers hang ready buffers on this structure,
 * and the massbus driver will start i/o on the first such buffer
 * when appropriate.
 */
struct	x_mba_device {
	struct	x_mba_driver *x_mi_driver;
	x_short	x_mi_unit;	/* unit number to the system */
	x_short	x_mi_mbanum;	/* the mba it is on */
	x_short	x_mi_drive;	/* controller on mba */
	x_short	x_mi_dk;		/* driver number for iostat */
	x_short	x_mi_alive;	/* device exists */
	x_short	x_mi_type;	/* driver specific unit type */
	struct	x_buf x_mi_tab;	/* head of queue for this device */
	struct	x_mba_device *x_mi_forw;
/* we could compute these every time, but hereby save time */
	struct	x_mba_regs *x_mi_mba;
	struct	x_mba_drv *x_mi_drv;
	struct	x_mba_hd *x_mi_hd;
};

#define	x_b_bdone	x_b_bufsize		/* redefinition for mi_tab XXX */

/*
 * Tape formatter slaves are specified by
 * the following information which is used
 * at boot time to initialize the tape driver
 * internal tables.
 */
struct	x_mba_slave {
	struct	x_mba_driver *x_ms_driver;
	x_short	x_ms_ctlr;		/* which of several formatters */
	x_short	x_ms_unit;		/* which unit to system */
	x_short	x_ms_slave;		/* which slave to formatter */
	x_short	x_ms_alive;
};

/*
 * Per device-type structure.
 *
 * Each massbus driver defines entries for a set of routines used
 * by the massbus driver, as well as an array of types which are
 * acceptable to it.
 */
struct x_mba_driver {
	x_int	(*x_md_attach)();		/* attach a device */
	x_int	(*x_md_slave)();		/* attach a slave */
	x_int	(*x_md_ustart)();		/* unit start routine */
	x_int	(*x_md_start)();		/* setup a data transfer */
	x_int	(*x_md_dtint)();		/* data transfer complete */
	x_int	(*x_md_ndint)();		/* non-data transfer interrupt */
	x_short	*x_md_type;		/* array of drive type codes */
	char	*x_md_dname, *x_md_sname;	/* device, slave names */
	struct	x_mba_device **x_md_info;	/* backpointers to mbinit structs */
};

/*
 * Possible return values from unit start routines.
 */
#define	x_MBU_NEXT	0		/* skip to next operation */
#define	x_MBU_BUSY	1		/* dual port busy; wait for intr */
#define	x_MBU_STARTED	2		/* non-data transfer started */
#define	x_MBU_DODATA	3		/* data transfer ready; start mba */

/*
 * Possible return values from data transfer interrupt handling routines
 */
#define	x_MBD_DONE	0		/* data transfer complete */
#define	x_MBD_RETRY	1		/* error occurred, please retry */
#define	x_MBD_RESTARTED	2		/* driver restarted i/o itself */
#define	x_MBD_REPOSITION	3		/* driver started unit, not transfer */

/*
 * Possible return values from non-data-transfer interrupt handling routines
 */
#define	x_MBN_DONE	0		/* non-data transfer complete */
#define	x_MBN_RETRY	1		/* failed; retry the operation */
#define	x_MBN_SKIP	2		/* don't do anything */

/*
 * Clear attention status for specified device.
 */
#define	x_mbclrattn(x_mi)	((x_mi)->x_mi_mba->x_mba_drv[0].x_mbd_as = 1 << (x_mi)->x_mi_drive)

/*
 * Kernel definitions related to mba.
 */
#ifdef x_KERNEL
x_int	x_nummba;
#if x_NMBA > 0
struct	x_mba_hd x_mba_hd[x_NMBA];
extern	x_Xmba0int(), x_Xmba1int(), x_Xmba2int(), x_Xmba3int();

extern	struct	x_mba_device x_mbdinit[];
extern	struct	x_mba_slave x_mbsinit[];
#endif
#endif
