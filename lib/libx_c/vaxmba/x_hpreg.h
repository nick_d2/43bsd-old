#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)hpreg.h	7.1 (Berkeley) 6/5/86
 */

struct x_hpdevice
{
	x_int	x_hpcs1;		/* control and status register 1 */
	x_int	x_hpds;		/* drive status */
	x_int	x_hper1;		/* error register 1 */
	x_int	x_hpmr;		/* maintenance */ 
	x_int	x_hpas;		/* attention summary */
	x_int	x_hpda;		/* desired address register */
	x_int	x_hpdt;		/* drive type */
	x_int	x_hpla;		/* look ahead */
	x_int	x_hpsn;		/* serial number */
	x_int	x_hpof;		/* offset register */
	x_int	x_hpdc;		/* desired cylinder address register */
	x_int	x_hpcc;		/* current cylinder */
#define	x_hphr	x_hpcc		/* holding register */
/* on an rp drive, mr2 is called er2 and er2 is called er3 */
/* we use rm terminology here */
	x_int	x_hpmr2;		/* maintenance register 2 */
	x_int	x_hper2;		/* error register 2 */
	x_int	x_hpec1;		/* burst error bit position */
	x_int	x_hpec2;		/* burst error bit pattern */
};

/* hpcs1 */
#define	x_HP_SC	0100000		/* special condition */
#define	x_HP_TRE	0040000		/* transfer error */
#define	x_HP_DVA	0004000		/* drive available */
#define	x_HP_RDY	0000200		/* controller ready */
#define	x_HP_IE	0000100		/* interrupt enable */
/* bits 5-1 are the command */
#define	x_HP_GO	0000001

/* commands */
#define	x_HP_NOP		000		/* no operation */
#define	x_HP_UNLOAD	002		/* offline drive */
#define	x_HP_SEEK		004		/* seek */
#define	x_HP_RECAL	006		/* recalibrate */
#define	x_HP_DCLR		010		/* drive clear */
#define	x_HP_RELEASE	012		/* release */
#define	x_HP_OFFSET	014		/* offset */
#define	x_HP_RTC		016		/* return to centerline */
#define	x_HP_PRESET	020		/* read-in preset */
#define	x_HP_PACK		022		/* pack acknowledge */
#define	x_HP_SEARCH	030		/* search */
#define	x_HP_DIAGNOSE	034		/* diagnose drive */
#define	x_HP_WCDATA	050		/* write check data */
#define	x_HP_WCHDR	052		/* write check header and data */
#define	x_HP_WCOM		060		/* write data */
#define	x_HP_WHDR		062		/* write header */
#define	x_HP_WTRACKD	064		/* write track descriptor */
#define	x_HP_RCOM		070		/* read data */
#define	x_HP_RHDR		072		/* read header and data */
#define	x_HP_RTRACKD	074		/* read track descriptor */
	
/* hpds */
#define	x_HPDS_ATA	0100000		/* attention active */
#define	x_HPDS_ERR	0040000		/* composite drive error */
#define	x_HPDS_PIP	0020000		/* positioning in progress */
#define	x_HPDS_MOL	0010000		/* medium on line */
#define	x_HPDS_WRL	0004000		/* write locked */
#define	x_HPDS_LST	0002000		/* last sector transferred */
#define	x_HPDS_PGM	0001000		/* programmable */
#define	x_HPDS_DPR	0000400		/* drive present */
#define	x_HPDS_DRY	0000200		/* drive ready */
#define	x_HPDS_VV		0000100		/* volume valid */
/* bits 1-5 are spare */
#define	x_HPDS_OM		0000001		/* offset mode */

#define	x_HPDS_DREADY	(x_HPDS_DPR|x_HPDS_DRY|x_HPDS_MOL|x_HPDS_VV)
#define	x_HPDS_BITS \
"\10\20ATA\17ERR\16PIP\15MOL\14WRL\13LST\12PGM\11DPR\10DRY\7VV\1OM"

/* hper1 */
#define	x_HPER1_DCK	0100000		/* data check */
#define	x_HPER1_UNS	0040000		/* drive unsafe */
#define	x_HPER1_OPI	0020000		/* operation incomplete */
#define	x_HPER1_DTE	0010000		/* drive timing error */
#define	x_HPER1_WLE	0004000		/* write lock error */
#define	x_HPER1_IAE	0002000		/* invalid address error */
#define	x_HPER1_AOE	0001000		/* address overflow error */
#define	x_HPER1_HCRC	0000400		/* header crc error */
#define	x_HPER1_HCE	0000200		/* header compare error */
#define	x_HPER1_ECH	0000100		/* ecc hard error */
#define x_HPER1_WCF	0000040		/* write clock fail */
#define	x_HPER1_FER	0000020		/* format error */
#define	x_HPER1_PAR	0000010		/* parity error */
#define	x_HPER1_RMR	0000004		/* register modification refused */
#define	x_HPER1_ILR	0000002		/* illegal register */
#define	x_HPER1_ILF	0000001		/* illegal function */

#define	x_HPER1_BITS \
"\10\20DCK\17UNS\16OPI\15DTE\14WLE\13IAE\12AOE\11HCRC\10HCE\
\7Ex_CH\6x_WCF\5x_FER\4x_PAR\3x_RMR\2x_ILR\1x_ILF"
#define	x_HPER1_HARD    \
	(x_HPER1_WLE|x_HPER1_IAE|x_HPER1_AOE|\
	 x_HPER1_FER|x_HPER1_RMR|x_HPER1_ILR|x_HPER1_ILF)

/* hper2 */
#define	x_HPER2_BSE	0100000		/* bad sector error */
#define	x_HPER2_SKI	0040000		/* seek incomplete */
#define	x_HPER2_OPE	0020000		/* operator plug error */
#define	x_HPER2_IVC	0010000		/* invalid command */
#define	x_HPER2_LSC	0004000		/* loss of system clock */
#define	x_HPER2_LBC	0002000		/* loss of bit check */
#define	x_HPER2_DVC	0000200		/* device check */
#define	x_HPER2_SSE	0000040		/* skip sector error (rm80) */
#define	x_HPER2_DPE	0000010		/* data parity error */

#define	x_HPER2_BITS \
"\10\20BSE\17SKI\16OPE\15IVC\14LSC\13LBC\10DVC\6SSE\4DPE"
#define	x_HPER2_HARD    (x_HPER2_OPE)

/* hpof */
#define	x_HPOF_CMO	0100000		/* command modifier */
#define	x_HPOF_MTD	0040000		/* move track descriptor */
#define	x_HPOF_FMT22	0010000		/* 16 bit format */
#define	x_HPOF_ECI	0004000		/* ecc inhibit */
#define	x_HPOF_HCI	0002000		/* header compare inhibit */
#define	x_HPOF_SSEI	0001000		/* skip sector inhibit */

#define	x_HPOF_P400	020		/*  +400 uinches */
#define	x_HPOF_M400	0220		/*  -400 uinches */
#define	x_HPOF_P800	040		/*  +800 uinches */
#define	x_HPOF_M800	0240		/*  -800 uinches */
#define	x_HPOF_P1200	060		/* +1200 uinches */
#define	x_HPOF_M1200	0260		/* -1200 uinches */

/* hphr (alias hpcc) commands */
#define	x_HPHR_MAXCYL	0x8017		/* maximum cylinder address */
#define	x_HPHR_MAXTRAK	0x8018		/* maximum track address */
#define	x_HPHR_MAXSECT	0x8019		/* maximum sector address */
#define	x_HPHR_FMTENABLE	0xffff		/* enable format command in cs1 */

/* hpmr */
#define	x_HPMR_SZ		0174000		/* ML11 system size */
#define	x_HPMR_ARRTYP	0002000		/* ML11 array type */
#define	x_HPMR_TRT	0001400		/* ML11 transfer rate */

/*
 * Systems Industries kludge: use value in
 * the serial # register to figure out real drive type.
 */
#define	x_SIMB_MB	0xff00		/* model byte value */
#define	x_SIMB_S6	0x2000		/* switch s6 */
#define	x_SIMB_LU	0x0007		/* logical unit (should = drive #) */

#define	x_SI9775D	0x0700		/* 9775 direct */
#define	x_SI9775M	0x0e00		/* 9775 mapped */
#define	x_SI9730D	0x0b00		/* 9730 direct */
#define	x_SI9730M	0x0d00		/* 9730 mapped */
#define	x_SI9766	0x0300		/* 9766 */
#define	x_SI9762	0x0100		/* 9762 */
#define	x_SICAPD	0x0500		/* Capricorn direct */
#define	x_SICAPN	0x0400		/* Capricorn mapped */
#define	x_SI9751D	0x0f00		/* Eagle direct */

#define	x_SIRM03	0x8000		/* RM03 indication */
#define	x_SIRM05	0x0000		/* RM05 pseudo-indication */
