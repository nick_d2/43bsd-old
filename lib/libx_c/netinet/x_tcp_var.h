#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)tcp_var.h	7.1 (Berkeley) 6/5/86
 */

/*
 * Kernel variables for tcp.
 */

/*
 * Tcp control block, one per tcp; fields:
 */
struct x_tcpcb {
	struct	x_tcpiphdr *x_seg_next;	/* sequencing queue */
	struct	x_tcpiphdr *x_seg_prev;
	x_short	x_t_state;		/* state of this connection */
	x_short	x_t_timer[x_TCPT_NTIMERS];	/* tcp timers */
	x_short	x_t_rxtshift;		/* log(2) of rexmt exp. backoff */
	struct	x_mbuf *x_t_tcpopt;		/* tcp options */
	x_u_short	x_t_maxseg;		/* maximum segment size */
	char	x_t_force;		/* 1 if forcing out a byte */
	x_u_char	x_t_flags;
#define	x_TF_ACKNOW	0x01		/* ack peer immediately */
#define	x_TF_DELACK	0x02		/* ack, but try to delay it */
#define	x_TF_NODELAY	0x04		/* don't delay packets to coalesce */
#define	x_TF_NOOPT	0x08		/* don't use tcp options */
#define	x_TF_SENTFIN	0x10		/* have sent FIN */
	struct	x_tcpiphdr *x_t_template;	/* skeletal packet for transmit */
	struct	x_inpcb *x_t_inpcb;		/* back pointer to internet pcb */
/*
 * The following fields are used as in the protocol specification.
 * See RFC783, Dec. 1981, page 21.
 */
/* send sequence variables */
	x_tcp_seq	x_snd_una;		/* send unacknowledged */
	x_tcp_seq	x_snd_nxt;		/* send next */
	x_tcp_seq	x_snd_up;			/* send urgent pointer */
	x_tcp_seq	x_snd_wl1;		/* window update seg seq number */
	x_tcp_seq	x_snd_wl2;		/* window update seg ack number */
	x_tcp_seq	x_iss;			/* initial send sequence number */
	x_u_short	x_snd_wnd;		/* send window */
/* receive sequence variables */
	x_u_short	x_rcv_wnd;		/* receive window */
	x_tcp_seq	x_rcv_nxt;		/* receive next */
	x_tcp_seq	x_rcv_up;			/* receive urgent pointer */
	x_tcp_seq	x_irs;			/* initial receive sequence number */
/*
 * Additional variables for this implementation.
 */
/* receive variables */
	x_tcp_seq	x_rcv_adv;		/* advertised window */
/* retransmit variables */
	x_tcp_seq	x_snd_max;		/* highest sequence number sent
					 * used to recognize retransmits
					 */
/* congestion control (for source quench) */
	x_u_short	x_snd_cwnd;		/* congestion-controlled window */
/* transmit timing stuff */
	x_short	x_t_idle;			/* inactivity time */
	x_short	x_t_rtt;			/* round trip time */
	x_u_short x_max_rcvd;		/* most peer has sent into window */
	x_tcp_seq	x_t_rtseq;		/* sequence number being timed */
	float	x_t_srtt;			/* smoothed round-trip time */
	x_u_short	x_max_sndwnd;		/* largest window peer has offered */
/* out-of-band data */
	char	x_t_oobflags;		/* have some */
	char	x_t_iobc;			/* input character */
#define	x_TCPOOB_HAVEDATA	0x01
#define	x_TCPOOB_HADDATA	0x02
};

#define	x_intotcpcb(x_ip)	((struct x_tcpcb *)(x_ip)->x_inp_ppcb)
#define	x_sototcpcb(x_so)	(x_intotcpcb(x_sotoinpcb(x_so)))

struct	x_tcpstat {
	x_int	x_tcps_badsum;
	x_int	x_tcps_badoff;
	x_int	x_tcps_hdrops;
	x_int	x_tcps_badsegs;
	x_int	x_tcps_unack;
};

#ifdef x_KERNEL
struct	x_inpcb x_tcb;		/* head of queue of active tcpcb's */
struct	x_tcpstat x_tcpstat;	/* tcp statistics */
struct	x_tcpiphdr *x_tcp_template();
struct	x_tcpcb *x_tcp_close(), *x_tcp_drop();
struct	x_tcpcb *x_tcp_timers(), *x_tcp_disconnect(), *x_tcp_usrclosed();
#endif
