#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)icmp_var.h	7.2 (Berkeley) 1/13/87
 */

/*
 * Variables related to this implementation
 * of the internet control message protocol.
 */
struct	x_icmpstat {
/* statistics related to icmp packets generated */
	x_int	x_icps_error;		/* # of calls to icmp_error */
	x_int	x_icps_oldshort;		/* no error 'cuz old ip too short */
	x_int	x_icps_oldicmp;		/* no error 'cuz old was icmp */
	x_int	x_icps_outhist[x_ICMP_MAXTYPE + 1];
/* statistics related to input messages processed */
 	x_int	x_icps_badcode;		/* icmp_code out of range */
	x_int	x_icps_tooshort;		/* packet < ICMP_MINLEN */
	x_int	x_icps_checksum;		/* bad checksum */
	x_int	x_icps_badlen;		/* calculated bound mismatch */
	x_int	x_icps_reflect;		/* number of responses */
	x_int	x_icps_inhist[x_ICMP_MAXTYPE + 1];
};

#ifdef x_KERNEL
struct	x_icmpstat x_icmpstat;
#endif
