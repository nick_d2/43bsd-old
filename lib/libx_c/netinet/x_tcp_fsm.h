#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)tcp_fsm.h	7.1 (Berkeley) 6/5/86
 */

/*
 * TCP FSM state definitions.
 * Per RFC793, September, 1981.
 */

#define	x_TCP_NSTATES	11

#define	x_TCPS_CLOSED		0	/* closed */
#define	x_TCPS_LISTEN		1	/* listening for connection */
#define	x_TCPS_SYN_SENT		2	/* active, have sent syn */
#define	x_TCPS_SYN_RECEIVED	3	/* have send and received syn */
/* states < TCPS_ESTABLISHED are those where connections not established */
#define	x_TCPS_ESTABLISHED	4	/* established */
#define	x_TCPS_CLOSE_WAIT		5	/* rcvd fin, waiting for close */
/* states > TCPS_CLOSE_WAIT are those where user has closed */
#define	x_TCPS_FIN_WAIT_1		6	/* have closed, sent fin */
#define	x_TCPS_CLOSING		7	/* closed xchd FIN; await FIN ACK */
#define	x_TCPS_LAST_ACK		8	/* had fin and close; await FIN ACK */
/* states > TCPS_CLOSE_WAIT && < TCPS_FIN_WAIT_2 await ACK of FIN */
#define	x_TCPS_FIN_WAIT_2		9	/* have closed, fin is acked */
#define	x_TCPS_TIME_WAIT		10	/* in 2*msl quiet wait after close */

#define	x_TCPS_HAVERCVDSYN(x_s)	((x_s) >= x_TCPS_SYN_RECEIVED)
#define	x_TCPS_HAVERCVDFIN(x_s)	((x_s) >= x_TCPS_TIME_WAIT)

#ifdef	x_TCPOUTFLAGS
/*
 * Flags used when sending segments in tcp_output.
 * Basic flags (TH_RST,TH_ACK,TH_SYN,TH_FIN) are totally
 * determined by state, with the proviso that TH_FIN is sent only
 * if all data queued for output is included in the segment.
 */
x_u_char	x_tcp_outflags[x_TCP_NSTATES] = {
    x_TH_RST|x_TH_ACK, 0, x_TH_SYN, x_TH_SYN|x_TH_ACK,
    x_TH_ACK, x_TH_ACK,
    x_TH_FIN|x_TH_ACK, x_TH_FIN|x_TH_ACK, x_TH_FIN|x_TH_ACK, x_TH_ACK, x_TH_ACK,
};
#endif

#ifdef x_KPROF
x_int	x_tcp_acounts[x_TCP_NSTATES][x_PRU_NREQ];
#endif

#ifdef	x_TCPSTATES
char *x_tcpstates[] = {
	"CLOSED",	"LISTEN",	"SYN_SENT",	"SYN_RCVD",
	"ESTABLISHED",	"CLOSE_WAIT",	"FIN_WAIT_1",	"CLOSING",
	"LAST_ACK",	"FIN_WAIT_2",	"TIME_WAIT",
};
#endif
