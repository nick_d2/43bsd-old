#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)ip_var.h	7.1 (Berkeley) 6/5/86
 */

/*
 * Overlay for ip header used by other protocols (tcp, udp).
 */
struct x_ipovly {
	x_caddr_t	x_ih_next, x_ih_prev;	/* for protocol sequence q's */
	x_u_char	x_ih_x1;			/* (unused) */
	x_u_char	x_ih_pr;			/* protocol */
	x_short	x_ih_len;			/* protocol length */
	struct	x_in_addr x_ih_src;		/* source internet address */
	struct	x_in_addr x_ih_dst;		/* destination internet address */
};

/*
 * Ip reassembly queue structure.  Each fragment
 * being reassembled is attached to one of these structures.
 * They are timed out after ipq_ttl drops to 0, and may also
 * be reclaimed if memory becomes tight.
 */
struct x_ipq {
	struct	x_ipq *x_next,*x_prev;	/* to other reass headers */
	x_u_char	x_ipq_ttl;		/* time for reass q to live */
	x_u_char	x_ipq_p;			/* protocol of this fragment */
	x_u_short	x_ipq_id;			/* sequence id for reassembly */
	struct	x_ipasfrag *x_ipq_next,*x_ipq_prev;
					/* to ip headers of fragments */
	struct	x_in_addr x_ipq_src,x_ipq_dst;
};

/*
 * Ip header, when holding a fragment.
 *
 * Note: ipf_next must be at same offset as ipq_next above
 */
struct	x_ipasfrag {
#ifdef x_vax
	x_u_char	x_ip_hl:4,
		x_ip_v:4;
#endif
	x_u_char	x_ipf_mff;		/* copied from (ip_off&IP_MF) */
	x_short	x_ip_len;
	x_u_short	x_ip_id;
	x_short	x_ip_off;
	x_u_char	x_ip_ttl;
	x_u_char	x_ip_p;
	x_u_short	x_ip_sum;
	struct	x_ipasfrag *x_ipf_next;	/* next fragment */
	struct	x_ipasfrag *x_ipf_prev;	/* previous fragment */
};

/*
 * Structure stored in mbuf in inpcb.ip_options
 * and passed to ip_output when ip options are in use.
 * The actual length of the options (including ipopt_dst)
 * is in m_len.
 */
#define x_MAX_IPOPTLEN	40

struct x_ipoption {
	struct	x_in_addr x_ipopt_dst;	/* first-hop dst if source routed */
	char	x_ipopt_list[x_MAX_IPOPTLEN];	/* options proper */
};

struct	x_ipstat {
	x_long	x_ips_total;		/* total packets received */
	x_long	x_ips_badsum;		/* checksum bad */
	x_long	x_ips_tooshort;		/* packet too short */
	x_long	x_ips_toosmall;		/* not enough data */
	x_long	x_ips_badhlen;		/* ip header length < data size */
	x_long	x_ips_badlen;		/* ip length < ip header length */
	x_long	x_ips_fragments;		/* fragments received */
	x_long	x_ips_fragdropped;	/* frags dropped (dups, out of space) */
	x_long	x_ips_fragtimeout;	/* fragments timed out */
	x_long	x_ips_forward;		/* packets forwarded */
	x_long	x_ips_cantforward;	/* packets rcvd for unreachable dest */
	x_long	x_ips_redirectsent;	/* packets forwarded on same net */
};

#ifdef x_KERNEL
/* flags passed to ip_output as last parameter */
#define	x_IP_FORWARDING		0x1		/* most of ip header exists */
#define	x_IP_ROUTETOIF		x_SO_DONTROUTE	/* bypass routing tables */
#define	x_IP_ALLOWBROADCAST	x_SO_BROADCAST	/* can send broadcast packets */

struct	x_ipstat	x_ipstat;
struct	x_ipq	x_ipq;			/* ip reass. queue */
x_u_short	x_ip_id;				/* ip packet ctr, for ids */

struct	x_mbuf *x_ip_srcroute();
#endif
