#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)tcpip.h	7.1 (Berkeley) 6/5/86
 */

/*
 * Tcp+ip header, after ip options removed.
 */
struct x_tcpiphdr {
	struct 	x_ipovly x_ti_i;		/* overlaid ip structure */
	struct	x_tcphdr x_ti_t;		/* tcp header */
};
#define	x_ti_next		x_ti_i.x_ih_next
#define	x_ti_prev		x_ti_i.x_ih_prev
#define	x_ti_x1		x_ti_i.x_ih_x1
#define	x_ti_pr		x_ti_i.x_ih_pr
#define	x_ti_len		x_ti_i.x_ih_len
#define	x_ti_src		x_ti_i.x_ih_src
#define	x_ti_dst		x_ti_i.x_ih_dst
#define	x_ti_sport	x_ti_t.x_th_sport
#define	x_ti_dport	x_ti_t.x_th_dport
#define	x_ti_seq		x_ti_t.x_th_seq
#define	x_ti_ack		x_ti_t.x_th_ack
#define	x_ti_x2		x_ti_t.x_th_x2
#define	x_ti_off		x_ti_t.x_th_off
#define	x_ti_flags	x_ti_t.x_th_flags
#define	x_ti_win		x_ti_t.x_th_win
#define	x_ti_sum		x_ti_t.x_th_sum
#define	x_ti_urp		x_ti_t.x_th_urp
