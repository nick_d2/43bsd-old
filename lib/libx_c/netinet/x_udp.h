#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)udp.h	7.1 (Berkeley) 6/5/86
 */

/*
 * Udp protocol header.
 * Per RFC 768, September, 1981.
 */
struct x_udphdr {
	x_u_short	x_uh_sport;		/* source port */
	x_u_short	x_uh_dport;		/* destination port */
	x_short	x_uh_ulen;		/* udp length */
	x_u_short	x_uh_sum;			/* udp checksum */
};
