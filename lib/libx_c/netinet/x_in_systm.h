#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)in_systm.h	7.1 (Berkeley) 6/5/86
 */

/*
 * Miscellaneous internetwork
 * definitions for kernel.
 */

/*
 * Network types.
 *
 * Internally the system keeps counters in the headers with the bytes
 * swapped so that VAX instructions will work on them.  It reverses
 * the bytes before transmission at each protocol level.  The n_ types
 * represent the types with the bytes in ``high-ender'' order.
 */
typedef x_u_short x_n_short;		/* short as received from the net */
typedef x_u_long	x_n_long;			/* long as received from the net */

typedef	x_u_long	x_n_time;			/* ms since 00:00 GMT, byte rev */

#ifdef x_KERNEL
x_n_time	x_iptime();
#endif
