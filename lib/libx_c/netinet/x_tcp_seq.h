#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)tcp_seq.h	7.1 (Berkeley) 6/5/86
 */

/*
 * TCP sequence numbers are 32 bit integers operated
 * on with modular arithmetic.  These macros can be
 * used to compare such integers.
 */
#define	x_SEQ_LT(x_a,x_b)	((x_int)((x_a)-(x_b)) < 0)
#define	x_SEQ_LEQ(x_a,x_b)	((x_int)((x_a)-(x_b)) <= 0)
#define	x_SEQ_GT(x_a,x_b)	((x_int)((x_a)-(x_b)) > 0)
#define	x_SEQ_GEQ(x_a,x_b)	((x_int)((x_a)-(x_b)) >= 0)

/*
 * Macros to initialize tcp sequence numbers for
 * send and receive from initial send and receive
 * sequence numbers.
 */
#define	x_tcp_rcvseqinit(x_tp) \
	(x_tp)->x_rcv_adv = (x_tp)->x_rcv_nxt = (x_tp)->x_irs + 1

#define	x_tcp_sendseqinit(x_tp) \
	(x_tp)->x_snd_una = (x_tp)->x_snd_nxt = (x_tp)->x_snd_max = (x_tp)->x_snd_up = \
	    (x_tp)->x_iss

#define	x_TCP_ISSINCR	(125*1024)	/* increment for tcp_iss each second */

#ifdef x_KERNEL
x_tcp_seq	x_tcp_iss;		/* tcp initial send seq # */
#endif
