#include "x_.h"

/*
 * Copyright (c) 1985, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)in_var.h	7.1 (Berkeley) 6/5/86
 */

/*
 * Interface address, Internet version.  One of these structures
 * is allocated for each interface with an Internet address.
 * The ifaddr structure contains the protocol-independent part
 * of the structure and is assumed to be first.
 */
struct x_in_ifaddr {
	struct	x_ifaddr x_ia_ifa;		/* protocol-independent info */
#define	x_ia_addr	x_ia_ifa.x_ifa_addr
#define	x_ia_broadaddr	x_ia_ifa.x_ifa_broadaddr
#define	x_ia_dstaddr	x_ia_ifa.x_ifa_dstaddr
#define	x_ia_ifp		x_ia_ifa.x_ifa_ifp
	x_u_long	x_ia_net;			/* network number of interface */
	x_u_long	x_ia_netmask;		/* mask of net part */
	x_u_long	x_ia_subnet;		/* subnet number, including net */
	x_u_long	x_ia_subnetmask;		/* mask of net + subnet */
	struct	x_in_addr x_ia_netbroadcast; /* broadcast addr for (logical) net */
	x_int	x_ia_flags;
	struct	x_in_ifaddr *x_ia_next;	/* next in list of internet addresses */
};
/*
 * Given a pointer to an in_ifaddr (ifaddr),
 * return a pointer to the addr as a sockadd_in.
 */
#define	x_IA_SIN(x_ia) ((struct x_sockaddr_in *)(&((struct x_in_ifaddr *)x_ia)->x_ia_addr))
/*
 * ia_flags
 */
#define	x_IFA_ROUTE	0x01		/* routing entry installed */

#ifdef	x_KERNEL
struct	x_in_ifaddr *x_in_ifaddr;
struct	x_in_ifaddr *x_in_iaonnetof();
struct	x_ifqueue	x_ipintrq;		/* ip packet input queue */
#endif
