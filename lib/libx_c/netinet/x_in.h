#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)in.h	7.1 (Berkeley) 6/5/86
 */

/*
 * Constants and structures defined by the internet system,
 * Per RFC 790, September 1981.
 */

/*
 * Protocols
 */
#define	x_IPPROTO_IP		0		/* dummy for IP */
#define	x_IPPROTO_ICMP		1		/* control message protocol */
#define	x_IPPROTO_GGP		2		/* gateway^2 (deprecated) */
#define	x_IPPROTO_TCP		6		/* tcp */
#define	x_IPPROTO_EGP		8		/* exterior gateway protocol */
#define	x_IPPROTO_PUP		12		/* pup */
#define	x_IPPROTO_UDP		17		/* user datagram protocol */
#define	x_IPPROTO_IDP		22		/* xns idp */

#define	x_IPPROTO_RAW		255		/* raw IP packet */
#define	x_IPPROTO_MAX		256


/*
 * Ports < IPPORT_RESERVED are reserved for
 * privileged processes (e.g. root).
 * Ports > IPPORT_USERRESERVED are reserved
 * for servers, not necessarily privileged.
 */
#define	x_IPPORT_RESERVED		1024
#define	x_IPPORT_USERRESERVED	5000

/*
 * Link numbers
 */
#define	x_IMPLINK_IP		155
#define	x_IMPLINK_LOWEXPER	156
#define	x_IMPLINK_HIGHEXPER	158

/*
 * Internet address (a structure for historical reasons)
 */
struct x_in_addr {
	x_u_long x_s_addr;
};

/*
 * Definitions of bits in internet address integers.
 * On subnets, the decomposition of addresses to host and net parts
 * is done according to subnet mask, not the masks here.
 */
#define	x_IN_CLASSA(x_i)		(((x_long)(x_i) & 0x80000000) == 0)
#define	x_IN_CLASSA_NET		0xff000000
#define	x_IN_CLASSA_NSHIFT	24
#define	x_IN_CLASSA_HOST		0x00ffffff
#define	x_IN_CLASSA_MAX		128

#define	x_IN_CLASSB(x_i)		(((x_long)(x_i) & 0xc0000000) == 0x80000000)
#define	x_IN_CLASSB_NET		0xffff0000
#define	x_IN_CLASSB_NSHIFT	16
#define	x_IN_CLASSB_HOST		0x0000ffff
#define	x_IN_CLASSB_MAX		65536

#define	x_IN_CLASSC(x_i)		(((x_long)(x_i) & 0xc0000000) == 0xc0000000)
#define	x_IN_CLASSC_NET		0xffffff00
#define	x_IN_CLASSC_NSHIFT	8
#define	x_IN_CLASSC_HOST		0x000000ff

#define	x_INADDR_ANY		(x_u_long)0x00000000
#define	x_INADDR_BROADCAST	(x_u_long)0xffffffff	/* must be masked */

/*
 * Socket address, internet style.
 */
struct x_sockaddr_in {
	x_short	x_sin_family;
	x_u_short	x_sin_port;
	struct	x_in_addr x_sin_addr;
	char	x_sin_zero[8];
};

/*
 * Options for use with [gs]etsockopt at the IP level.
 */
#define	x_IP_OPTIONS	1		/* set/get IP per-packet options */

#if !defined(x_vax) && !defined(x_ntohl) && !defined(x_lint)
/*
 * Macros for number representation conversion.
 */
#define	x_ntohl(x_x)	(x_x)
#define	x_ntohs(x_x)	(x_x)
#define	x_htonl(x_x)	(x_x)
#define	x_htons(x_x)	(x_x)
#endif

#if !defined(x_ntohl) && (defined(x_vax) || defined(x_lint))
x_u_short	x_ntohs(), x_htons();
x_u_long	x_ntohl(), x_htonl();
#endif

#ifdef x_KERNEL
extern	struct x_domain x_inetdomain;
extern	struct x_protosw x_inetsw[];
struct	x_in_addr x_in_makeaddr();
x_u_long	x_in_netof(), x_in_lnaof();
#endif

#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

/* inet/inet_netof.c */
x_int x_inet_netof __P((struct x_in_addr x_in));
/* inet/inet_lnaof.c */
x_int x_inet_lnaof __P((struct x_in_addr x_in));
