#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)if_ether.h	7.1 (Berkeley) 6/5/86
 */

/*
 * Structure of a 10Mb/s Ethernet header.
 */
struct	x_ether_header {
	x_u_char	x_ether_dhost[6];
	x_u_char	x_ether_shost[6];
	x_u_short	x_ether_type;
};

#define	x_ETHERTYPE_PUP	0x0200		/* PUP protocol */
#define	x_ETHERTYPE_IP	0x0800		/* IP protocol */
#define x_ETHERTYPE_ARP	0x0806		/* Addr. resolution protocol */

/*
 * The ETHERTYPE_NTRAILER packet types starting at ETHERTYPE_TRAIL have
 * (type-ETHERTYPE_TRAIL)*512 bytes of data followed
 * by an ETHER type (as given above) and then the (variable-length) header.
 */
#define	x_ETHERTYPE_TRAIL		0x1000		/* Trailer packet */
#define	x_ETHERTYPE_NTRAILER	16

#define	x_ETHERMTU	1500
#define	x_ETHERMIN	(60-14)

/*
 * Ethernet Address Resolution Protocol.
 *
 * See RFC 826 for protocol description.  Structure below is adapted
 * to resolving internet addresses.  Field names used correspond to 
 * RFC 826.
 */
struct	x_ether_arp {
	struct	x_arphdr x_ea_hdr;	/* fixed-size header */
	x_u_char	x_arp_sha[6];	/* sender hardware address */
	x_u_char	x_arp_spa[4];	/* sender protocol address */
	x_u_char	x_arp_tha[6];	/* target hardware address */
	x_u_char	x_arp_tpa[4];	/* target protocol address */
};
#define	x_arp_hrd	x_ea_hdr.x_ar_hrd
#define	x_arp_pro	x_ea_hdr.x_ar_pro
#define	x_arp_hln	x_ea_hdr.x_ar_hln
#define	x_arp_pln	x_ea_hdr.x_ar_pln
#define	x_arp_op	x_ea_hdr.x_ar_op


/*
 * Structure shared between the ethernet driver modules and
 * the address resolution code.  For example, each ec_softc or il_softc
 * begins with this structure.
 */
struct	x_arpcom {
	struct 	x_ifnet x_ac_if;		/* network-visible interface */
	x_u_char	x_ac_enaddr[6];		/* ethernet hardware address */
	struct x_in_addr x_ac_ipaddr;	/* copy of ip address- XXX */
};

/*
 * Internet to ethernet address resolution table.
 */
struct	x_arptab {
	struct	x_in_addr x_at_iaddr;	/* internet address */
	x_u_char	x_at_enaddr[6];		/* ethernet address */
	x_u_char	x_at_timer;		/* minutes since last reference */
	x_u_char	x_at_flags;		/* flags */
	struct	x_mbuf *x_at_hold;		/* last packet until resolved/timeout */
};

#ifdef	x_KERNEL
x_u_char x_etherbroadcastaddr[6];
struct	x_arptab *x_arptnew();
char *x_ether_sprintf();
#endif
