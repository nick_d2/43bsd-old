#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)ip.h	7.1 (Berkeley) 6/5/86
 */

/*
 * Definitions for internet protocol version 4.
 * Per RFC 791, September 1981.
 */
#define	x_IPVERSION	4

/*
 * Structure of an internet header, naked of options.
 *
 * We declare ip_len and ip_off to be short, rather than u_short
 * pragmatically since otherwise unsigned comparisons can result
 * against negative integers quite easily, and fail in subtle ways.
 */
struct x_ip {
#ifdef x_vax
	x_u_char	x_ip_hl:4,		/* header length */
		x_ip_v:4;			/* version */
#endif
	x_u_char	x_ip_tos;			/* type of service */
	x_short	x_ip_len;			/* total length */
	x_u_short	x_ip_id;			/* identification */
	x_short	x_ip_off;			/* fragment offset field */
#define	x_IP_DF 0x4000			/* dont fragment flag */
#define	x_IP_MF 0x2000			/* more fragments flag */
	x_u_char	x_ip_ttl;			/* time to live */
	x_u_char	x_ip_p;			/* protocol */
	x_u_short	x_ip_sum;			/* checksum */
	struct	x_in_addr x_ip_src,x_ip_dst;	/* source and dest address */
};

/*
 * Definitions for options.
 */
#define	x_IPOPT_COPIED(x_o)		((x_o)&0x80)
#define	x_IPOPT_CLASS(x_o)		((x_o)&0x60)
#define	x_IPOPT_NUMBER(x_o)		((x_o)&0x1f)

#define	x_IPOPT_CONTROL		0x00
#define	x_IPOPT_RESERVED1		0x20
#define	x_IPOPT_DEBMEAS		0x40
#define	x_IPOPT_RESERVED2		0x60

#define	x_IPOPT_EOL		0		/* end of option list */
#define	x_IPOPT_NOP		1		/* no operation */

#define	x_IPOPT_RR		7		/* record packet route */
#define	x_IPOPT_TS		68		/* timestamp */
#define	x_IPOPT_SECURITY		130		/* provide s,c,h,tcc */
#define	x_IPOPT_LSRR		131		/* loose source route */
#define	x_IPOPT_SATID		136		/* satnet id */
#define	x_IPOPT_SSRR		137		/* strict source route */

/*
 * Offsets to fields in options other than EOL and NOP.
 */
#define	x_IPOPT_OPTVAL		0		/* option ID */
#define	x_IPOPT_OLEN		1		/* option length */
#define x_IPOPT_OFFSET		2		/* offset within option */
#define	x_IPOPT_MINOFF		4		/* min value of above */

/*
 * Time stamp option structure.
 */
struct	x_ip_timestamp {
	x_u_char	x_ipt_code;		/* IPOPT_TS */
	x_u_char	x_ipt_len;		/* size of structure (variable) */
	x_u_char	x_ipt_ptr;		/* index of current entry */
	x_u_char	x_ipt_flg:4,		/* flags, see below */
		x_ipt_oflw:4;		/* overflow counter */
	union {
		x_n_long	x_ipt_time[1];
		struct	x_ipt_ta {
			struct x_in_addr x_ipt_addr;
			x_n_long x_ipt_time;
		} x_ipt_ta[1];
	}
};

/* flag bits for ipt_flg */
#define	x_IPOPT_TS_TSONLY		0		/* timestamps only */
#define	x_IPOPT_TS_TSANDADDR	1		/* timestamps and addresses */
#define	x_IPOPT_TS_PRESPEC	2		/* specified modules only */

/* bits for security (not byte swapped) */
#define	x_IPOPT_SECUR_UNCLASS	0x0000
#define	x_IPOPT_SECUR_CONFID	0xf135
#define	x_IPOPT_SECUR_EFTO	0x789a
#define	x_IPOPT_SECUR_MMMM	0xbc4d
#define	x_IPOPT_SECUR_RESTR	0xaf13
#define	x_IPOPT_SECUR_SECRET	0xd788
#define	x_IPOPT_SECUR_TOPSECRET	0x6bc5

/*
 * Internet implementation parameters.
 */
#define	x_MAXTTL		255		/* maximum time to live (seconds) */
#define	x_IPFRAGTTL	15		/* time to live for frag chains */
#define	x_IPTTLDEC	1		/* subtracted when forwarding */

#define	x_IP_MSS		576		/* default maximum segment size */
