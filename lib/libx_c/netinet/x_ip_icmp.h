#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)ip_icmp.h	7.1 (Berkeley) 6/5/86
 */

/*
 * Interface Control Message Protocol Definitions.
 * Per RFC 792, September 1981.
 */

/*
 * Structure of an icmp header.
 */
struct x_icmp {
	x_u_char	x_icmp_type;		/* type of message, see below */
	x_u_char	x_icmp_code;		/* type sub code */
	x_u_short	x_icmp_cksum;		/* ones complement cksum of struct */
	union {
		x_u_char x_ih_pptr;			/* ICMP_PARAMPROB */
		struct x_in_addr x_ih_gwaddr;	/* ICMP_REDIRECT */
		struct x_ih_idseq {
			x_n_short	x_icd_id;
			x_n_short	x_icd_seq;
		} x_ih_idseq;
		x_int x_ih_void;
	} x_icmp_hun;
#define	x_icmp_pptr	x_icmp_hun.x_ih_pptr
#define	x_icmp_gwaddr	x_icmp_hun.x_ih_gwaddr
#define	x_icmp_id		x_icmp_hun.x_ih_idseq.x_icd_id
#define	x_icmp_seq	x_icmp_hun.x_ih_idseq.x_icd_seq
#define	x_icmp_void	x_icmp_hun.x_ih_void
	union {
		struct x_id_ts {
			x_n_time x_its_otime;
			x_n_time x_its_rtime;
			x_n_time x_its_ttime;
		} x_id_ts;
		struct x_id_ip  {
			struct x_ip x_idi_ip;
			/* options and then 64 bits of data */
		} x_id_ip;
		x_u_long	x_id_mask;
		char	x_id_data[1];
	} x_icmp_dun;
#define	x_icmp_otime	x_icmp_dun.x_id_ts.x_its_otime
#define	x_icmp_rtime	x_icmp_dun.x_id_ts.x_its_rtime
#define	x_icmp_ttime	x_icmp_dun.x_id_ts.x_its_ttime
#define	x_icmp_ip		x_icmp_dun.x_id_ip.x_idi_ip
#define	x_icmp_mask	x_icmp_dun.x_id_mask
#define	x_icmp_data	x_icmp_dun.x_id_data
};

/*
 * Lower bounds on packet lengths for various types.
 * For the error advice packets must first insure that the
 * packet is large enought to contain the returned ip header.
 * Only then can we do the check to see if 64 bits of packet
 * data have been returned, since we need to check the returned
 * ip header length.
 */
#define	x_ICMP_MINLEN	8				/* abs minimum */
#define	x_ICMP_TSLEN	(8 + 3 * sizeof (x_n_time))	/* timestamp */
#define	x_ICMP_MASKLEN	12				/* address mask */
#define	x_ICMP_ADVLENMIN	(8 + sizeof (struct x_ip) + 8)	/* min */
#define	x_ICMP_ADVLEN(x_p)	(8 + ((x_p)->x_icmp_ip.x_ip_hl << 2) + 8)
	/* N.B.: must separately check that ip_hl >= 5 */

/*
 * Definition of type and code field values.
 */
#define	x_ICMP_ECHOREPLY		0		/* echo reply */
#define	x_ICMP_UNREACH		3		/* dest unreachable, codes: */
#define		x_ICMP_UNREACH_NET	0		/* bad net */
#define		x_ICMP_UNREACH_HOST	1		/* bad host */
#define		x_ICMP_UNREACH_PROTOCOL	2		/* bad protocol */
#define		x_ICMP_UNREACH_PORT	3		/* bad port */
#define		x_ICMP_UNREACH_NEEDFRAG	4		/* IP_DF caused drop */
#define		x_ICMP_UNREACH_SRCFAIL	5		/* src route failed */
#define	x_ICMP_SOURCEQUENCH	4		/* packet lost, slow down */
#define	x_ICMP_REDIRECT		5		/* shorter route, codes: */
#define		x_ICMP_REDIRECT_NET	0		/* for network */
#define		x_ICMP_REDIRECT_HOST	1		/* for host */
#define		x_ICMP_REDIRECT_TOSNET	2		/* for tos and net */
#define		x_ICMP_REDIRECT_TOSHOST	3		/* for tos and host */
#define	x_ICMP_ECHO		8		/* echo service */
#define	x_ICMP_TIMXCEED		11		/* time exceeded, code: */
#define		x_ICMP_TIMXCEED_INTRANS	0		/* ttl==0 in transit */
#define		x_ICMP_TIMXCEED_REASS	1		/* ttl==0 in reass */
#define	x_ICMP_PARAMPROB		12		/* ip header bad */
#define	x_ICMP_TSTAMP		13		/* timestamp request */
#define	x_ICMP_TSTAMPREPLY	14		/* timestamp reply */
#define	x_ICMP_IREQ		15		/* information request */
#define	x_ICMP_IREQREPLY		16		/* information reply */
#define	x_ICMP_MASKREQ		17		/* address mask request */
#define	x_ICMP_MASKREPLY		18		/* address mask reply */

#define	x_ICMP_MAXTYPE		18
