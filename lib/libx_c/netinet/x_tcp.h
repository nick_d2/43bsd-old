#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)tcp.h	7.1 (Berkeley) 6/5/86
 */

typedef	x_u_long	x_tcp_seq;
/*
 * TCP header.
 * Per RFC 793, September, 1981.
 */
struct x_tcphdr {
	x_u_short	x_th_sport;		/* source port */
	x_u_short	x_th_dport;		/* destination port */
	x_tcp_seq	x_th_seq;			/* sequence number */
	x_tcp_seq	x_th_ack;			/* acknowledgement number */
#ifdef x_vax
	x_u_char	x_th_x2:4,		/* (unused) */
		x_th_off:4;		/* data offset */
#endif
	x_u_char	x_th_flags;
#define	x_TH_FIN	0x01
#define	x_TH_SYN	0x02
#define	x_TH_RST	0x04
#define	x_TH_PUSH	0x08
#define	x_TH_ACK	0x10
#define	x_TH_URG	0x20
	x_u_short	x_th_win;			/* window */
	x_u_short	x_th_sum;			/* checksum */
	x_u_short	x_th_urp;			/* urgent pointer */
};

#define	x_TCPOPT_EOL	0
#define	x_TCPOPT_NOP	1
#define	x_TCPOPT_MAXSEG	2

/*
 * Default maximum segment size for TCP.
 * With an IP MSS of 576, this is 536,
 * but 512 is probably more convenient.
 */
#ifdef	x_lint
#define	x_TCP_MSS	536
#else
#define	x_TCP_MSS	x_MIN(512, x_IP_MSS - sizeof (struct x_tcpiphdr))
#endif

/*
 * User-settable options (used with setsockopt).
 */
#define	x_TCP_NODELAY	0x01	/* don't delay send to coalesce packets */
#define	x_TCP_MAXSEG	0x02	/* set maximum segment size */
