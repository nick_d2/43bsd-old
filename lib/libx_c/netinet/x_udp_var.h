#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)udp_var.h	7.1 (Berkeley) 6/5/86
 */

/*
 * UDP kernel structures and variables.
 */
struct	x_udpiphdr {
	struct 	x_ipovly x_ui_i;		/* overlaid ip structure */
	struct	x_udphdr x_ui_u;		/* udp header */
};
#define	x_ui_next		x_ui_i.x_ih_next
#define	x_ui_prev		x_ui_i.x_ih_prev
#define	x_ui_x1		x_ui_i.x_ih_x1
#define	x_ui_pr		x_ui_i.x_ih_pr
#define	x_ui_len		x_ui_i.x_ih_len
#define	x_ui_src		x_ui_i.x_ih_src
#define	x_ui_dst		x_ui_i.x_ih_dst
#define	x_ui_sport	x_ui_u.x_uh_sport
#define	x_ui_dport	x_ui_u.x_uh_dport
#define	x_ui_ulen		x_ui_u.x_uh_ulen
#define	x_ui_sum		x_ui_u.x_uh_sum

struct	x_udpstat {
	x_int	x_udps_hdrops;
	x_int	x_udps_badsum;
	x_int	x_udps_badlen;
};

#define	x_UDP_TTL		30		/* time to live for UDP packets */

#ifdef x_KERNEL
struct	x_inpcb x_udb;
struct	x_udpstat x_udpstat;
#endif
