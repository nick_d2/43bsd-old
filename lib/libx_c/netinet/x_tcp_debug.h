#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)tcp_debug.h	7.1 (Berkeley) 6/5/86
 */

struct	x_tcp_debug {
	x_n_time	x_td_time;
	x_short	x_td_act;
	x_short	x_td_ostate;
	x_caddr_t	x_td_tcb;
	struct	x_tcpiphdr x_td_ti;
	x_short	x_td_req;
	struct	x_tcpcb x_td_cb;
};

#define	x_TA_INPUT 	0
#define	x_TA_OUTPUT	1
#define	x_TA_USER		2
#define	x_TA_RESPOND	3
#define	x_TA_DROP		4

#ifdef x_TANAMES
char	*x_tanames[] =
    { "input", "output", "user", "respond", "drop" };
#endif

#define	x_TCP_NDEBUG 100
struct	x_tcp_debug x_tcp_debug[x_TCP_NDEBUG];
x_int	x_tcp_debx;
