#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)tcp_timer.h	7.1 (Berkeley) 6/5/86
 */

/*
 * Definitions of the TCP timers.  These timers are counted
 * down PR_SLOWHZ times a second.
 */
#define	x_TCPT_NTIMERS	4

#define	x_TCPT_REXMT	0		/* retransmit */
#define	x_TCPT_PERSIST	1		/* retransmit persistance */
#define	x_TCPT_KEEP	2		/* keep alive */
#define	x_TCPT_2MSL	3		/* 2*msl quiet time timer */

/*
 * The TCPT_REXMT timer is used to force retransmissions.
 * The TCP has the TCPT_REXMT timer set whenever segments
 * have been sent for which ACKs are expected but not yet
 * received.  If an ACK is received which advances tp->snd_una,
 * then the retransmit timer is cleared (if there are no more
 * outstanding segments) or reset to the base value (if there
 * are more ACKs expected).  Whenever the retransmit timer goes off,
 * we retransmit one unacknowledged segment, and do a backoff
 * on the retransmit timer.
 *
 * The TCPT_PERSIST timer is used to keep window size information
 * flowing even if the window goes shut.  If all previous transmissions
 * have been acknowledged (so that there are no retransmissions in progress),
 * and the window is too small to bother sending anything, then we start
 * the TCPT_PERSIST timer.  When it expires, if the window is nonzero,
 * we go to transmit state.  Otherwise, at intervals send a single byte
 * into the peer's window to force him to update our window information.
 * We do this at most as often as TCPT_PERSMIN time intervals,
 * but no more frequently than the current estimate of round-trip
 * packet time.  The TCPT_PERSIST timer is cleared whenever we receive
 * a window update from the peer.
 *
 * The TCPT_KEEP timer is used to keep connections alive.  If an
 * connection is idle (no segments received) for TCPTV_KEEP amount of time,
 * but not yet established, then we drop the connection.  If the connection
 * is established, then we force the peer to send us a segment by sending:
 *	<SEQ=SND.UNA-1><ACK=RCV.NXT><CTL=ACK>
 * This segment is (deliberately) outside the window, and should elicit
 * an ack segment in response from the peer.  If, despite the TCPT_KEEP
 * initiated segments we cannot elicit a response from a peer in TCPT_MAXIDLE
 * amount of time, then we drop the connection.
 */

#define	x_TCP_TTL		30		/* time to live for TCP segs */
/*
 * Time constants.
 */
#define	x_TCPTV_MSL	( 15*x_PR_SLOWHZ)		/* max seg lifetime */
#define	x_TCPTV_SRTTBASE	0			/* base roundtrip time;
						   if 0, no idea yet */
#define	x_TCPTV_SRTTDFLT	(  3*x_PR_SLOWHZ)		/* assumed RTT if no info */

#define	x_TCPTV_KEEP	( 45*x_PR_SLOWHZ)		/* keep alive - 45 secs */
#define	x_TCPTV_PERSMIN	(  5*x_PR_SLOWHZ)		/* retransmit persistance */

#define	x_TCPTV_MAXIDLE	(  8*x_TCPTV_KEEP)	/* maximum allowable idle
						   time before drop conn */

#define	x_TCPTV_MIN	(  1*x_PR_SLOWHZ)		/* minimum allowable value */
#define	x_TCPTV_MAX	( 30*x_PR_SLOWHZ)		/* maximum allowable value */

#define	x_TCP_LINGERTIME	120			/* linger at most 2 minutes */

#define	x_TCP_MAXRXTSHIFT	12			/* maximum retransmits */

#ifdef	x_TCPTIMERS
char *x_tcptimers[] =
    { "REXMT", "PERSIST", "KEEP", "2MSL" };
#endif

/*
 * Retransmission smoothing constants.
 * Smoothed round trip time is updated by
 *    tp->t_srtt = (tcp_alpha * tp->t_srtt) + ((1 - tcp_alpha) * tp->t_rtt)
 * each time a new value of tp->t_rtt is available.  The initial
 * retransmit timeout is then based on
 *    tp->t_timer[TCPT_REXMT] = tcp_beta * tp->t_srtt;
 * limited, however to be at least TCPTV_MIN and at most TCPTV_MAX.
 */
float	x_tcp_alpha, x_tcp_beta;

/*
 * Initial values of tcp_alpha and tcp_beta.
 * These are conservative: averaging over a long
 * period of time, and allowing for large individual deviations from
 * tp->t_srtt.
 */
#define	x_TCP_ALPHA	0.9
#define	x_TCP_BETA	2.0

/*
 * Force a time value to be in a certain range.
 */
#define	x_TCPT_RANGESET(x_tv, x_value, x_tvmin, x_tvmax) { \
	(x_tv) = (x_value); \
	if ((x_tv) < (x_tvmin)) \
		(x_tv) = (x_tvmin); \
	if ((x_tv) > (x_tvmax)) \
		(x_tv) = (x_tvmax); \
}
