#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)in_pcb.h	7.1 (Berkeley) 6/5/86
 */

/*
 * Common structure pcb for internet protocol implementation.
 * Here are stored pointers to local and foreign host table
 * entries, local and foreign socket numbers, and pointers
 * up (to a socket structure) and down (to a protocol-specific)
 * control block.
 */
struct x_inpcb {
	struct	x_inpcb *x_inp_next,*x_inp_prev;
					/* pointers to other pcb's */
	struct	x_inpcb *x_inp_head;	/* pointer back to chain of inpcb's
					   for this protocol */
	struct	x_in_addr x_inp_faddr;	/* foreign host table entry */
	x_u_short	x_inp_fport;		/* foreign port */
	struct	x_in_addr x_inp_laddr;	/* local host table entry */
	x_u_short	x_inp_lport;		/* local port */
	struct	x_socket *x_inp_socket;	/* back pointer to socket */
	x_caddr_t	x_inp_ppcb;		/* pointer to per-protocol pcb */
	struct	x_route x_inp_route;	/* placeholder for routing entry */
	struct	x_mbuf *x_inp_options;	/* IP options */
};

#define	x_INPLOOKUP_WILDCARD	1
#define	x_INPLOOKUP_SETLOCAL	2

#define	x_sotoinpcb(x_so)	((struct x_inpcb *)(x_so)->x_so_pcb)

#ifdef x_KERNEL
struct	x_inpcb *x_in_pcblookup();
#endif
