#include "x_.h"

/*	ranlib.h	4.1	83/05/03	*/

/*
 * Structure of the __.SYMDEF table of contents for an archive.
 * __.SYMDEF begins with a word giving the number of ranlib structures
 * which immediately follow, and then continues with a string
 * table consisting of a word giving the number of bytes of strings
 * which follow and then the strings themselves.
 * The ran_strx fields index the string table whose first byte is numbered 0.
 */
struct	x_ranlib {
	union {
		x_off_t	x_ran_strx;	/* string table index of */
		char	*x_ran_name;	/* symbol defined by */
	} x_ran_un;
	x_off_t	x_ran_off;		/* library member at this offset */
};
