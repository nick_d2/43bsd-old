#include "x_.h"

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)fputc.c	5.2 (Berkeley) 3/9/86";
#endif

#include <x_stdio.h>

x_int x_fputc(x_c, x_fp) x_int x_c; register x_FILE *x_fp; {
	return(x_putc(x_c, x_fp));
}
