#include "x_.h"

#include <x_unistd.h>
#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)ftell.c	5.2 (Berkeley) 3/9/86";
#endif

/*
 * Return file offset.
 * Coordinates with buffering.
 */

#include	<x_stdio.h>
x_long	x_lseek();


x_long x_ftell(x_iop) register x_FILE *x_iop; {
	register x_long x_tres;
	register x_int x_adjust;

	if (x_iop->x__cnt < 0)
		x_iop->x__cnt = 0;
	if (x_iop->x__flag&x__IOREAD)
		x_adjust = - x_iop->x__cnt;
	else if (x_iop->x__flag&(x__IOWRT|x__IORW)) {
		x_adjust = 0;
		if (x_iop->x__flag&x__IOWRT && x_iop->x__base && (x_iop->x__flag&x__IONBF)==0)
			x_adjust = x_iop->x__ptr - x_iop->x__base;
	} else
		return(-1);
	x_tres = x_lseek(x_fileno(x_iop), 0L, 1);
	if (x_tres<0)
		return(x_tres);
	x_tres += x_adjust;
	return(x_tres);
}
