#include "x_.h"

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)getchar.c	5.2 (Berkeley) 3/9/86";
#endif

/*
 * A subroutine version of the macro getchar.
 */
#include <x_stdio.h>

#undef x_getchar

x_int x_getchar() {
	return(x_getc(x_stdin));
}
