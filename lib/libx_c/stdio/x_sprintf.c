#include "x_.h"

/*
 * Copyright (c) 1987 Regents of the University of California.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms are permitted
 * provided that the above copyright notice and this paragraph are
 * duplicated in all such forms and that any documentation,
 * advertising materials, and other materials related to such
 * distribution and use acknowledge that the software was developed
 * by the University of California, Berkeley.  The name of the
 * University may not be used to endorse or promote products derived
 * from this software without specific prior written permission.
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
 * WARRANTIES OF MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)sprintf.c	5.5 (Berkeley) 6/27/88";
#endif

#include <x_stdio.h>
#ifdef __STDC__
#include <stdarg.h>
#define x__va_start(x_ap, x_arg) va_start(x_ap, x_arg)
#else
#include <varargs.h>
#define x__va_start(x_ap, x_arg) va_start(x_ap)
#endif

#ifdef __STDC__
x_int x_sprintf(char *x_str, char *x_fmt, ...)
#else
x_int x_sprintf(x_str, x_fmt, va_alist) char *x_str; char *x_fmt; va_dcl
#endif
{
	va_list x_ap;
	x_FILE x__strbuf;
	x_int x_len;

	x__va_start(x_ap, x_fmt);
	x__strbuf.x__flag = x__IOWRT+x__IOSTRG;
	x__strbuf.x__ptr = x_str;
	x__strbuf.x__cnt = 32767;
	x_len = x__doprnt(x_fmt, x_ap, &x__strbuf);
	*x__strbuf.x__ptr = 0;
	va_end(x_ap);
	return(x_len);
}
