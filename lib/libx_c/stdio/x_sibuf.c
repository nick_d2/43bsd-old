#include "x_.h"

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)sibuf.c	5.2 (Berkeley) 3/9/86";
#endif

#include <x_stdio.h>

char	x__sibuf[x_BUFSIZ];
