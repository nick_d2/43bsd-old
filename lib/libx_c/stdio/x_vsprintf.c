#include "x_.h"

/*
 * Copyright (c) 1988 Regents of the University of California.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms are permitted
 * provided that the above copyright notice and this paragraph are
 * duplicated in all such forms and that any documentation,
 * advertising materials, and other materials related to such
 * distribution and use acknowledge that the software was developed
 * by the University of California, Berkeley.  The name of the
 * University may not be used to endorse or promote products derived
 * from this software without specific prior written permission.
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
 * WARRANTIES OF MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)vsprintf.c	5.2 (Berkeley) 6/27/88";
#endif

#include <x_stdio.h>
#ifdef __STDC__
#include <stdarg.h>
#define x__va_start(x_ap, x_arg) va_start(x_ap, x_arg)
#else
#include <varargs.h>
#define x__va_start(x_ap, x_arg) va_start(x_ap)
#endif

x_int x_vsprintf(x_str, x_fmt, x_ap) char *x_str; char *x_fmt; va_list x_ap; {
	x_FILE x_f;
	x_int x_len;

	x_f.x__flag = x__IOWRT+x__IOSTRG;
	x_f.x__ptr = x_str;
	x_f.x__cnt = 32767;
	x_len = x__doprnt(x_fmt, x_ap, &x_f);
	*x_f.x__ptr = 0;
	return (x_len);
}
