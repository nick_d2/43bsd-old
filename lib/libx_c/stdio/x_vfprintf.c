#include "x_.h"

/*
 * Copyright (c) 1988 Regents of the University of California.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms are permitted
 * provided that the above copyright notice and this paragraph are
 * duplicated in all such forms and that any documentation,
 * advertising materials, and other materials related to such
 * distribution and use acknowledge that the software was developed
 * by the University of California, Berkeley.  The name of the
 * University may not be used to endorse or promote products derived
 * from this software without specific prior written permission.
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
 * WARRANTIES OF MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)vfprintf.c	5.2 (Berkeley) 6/27/88";
#endif

#include <x_stdio.h>
#ifdef __STDC__
#include <stdarg.h>
#define x__va_start(x_ap, x_arg) va_start(x_ap, x_arg)
#else
#include <varargs.h>
#define x__va_start(x_ap, x_arg) va_start(x_ap)
#endif

x_int x_vfprintf(x_iop, x_fmt, x_ap) x_FILE *x_iop; char *x_fmt; va_list x_ap; {
	x_int x_len;
	char x_localbuf[x_BUFSIZ];

	if (x_iop->x__flag & x__IONBF) {
		x_iop->x__flag &= ~x__IONBF;
		x_iop->x__ptr = x_iop->x__base = x_localbuf;
		x_len = x__doprnt(x_fmt, x_ap, x_iop);
		(void) x_fflush(x_iop);
		x_iop->x__flag |= x__IONBF;
		x_iop->x__base = x_NULL;
		x_iop->x__bufsiz = 0;
		x_iop->x__cnt = 0;
	} else
		x_len = x__doprnt(x_fmt, x_ap, x_iop);

	return (x_ferror(x_iop) ? x_EOF : x_len);
}
