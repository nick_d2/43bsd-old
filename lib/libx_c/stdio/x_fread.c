#include "x_.h"

/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)fread.c	5.2 (Berkeley) 3/9/86";
#endif

#include	<x_stdio.h>

x_int x_fread(x_ptr, x_size, x_count, x_iop) register char *x_ptr; x_unsigned_int x_size; x_unsigned_int x_count; register x_FILE *x_iop; {
	register x_int x_s;
	x_int x_c;

	x_s = x_size * x_count;
	while (x_s > 0) {
		if (x_iop->x__cnt < x_s) {
			if (x_iop->x__cnt > 0) {
				x_bcopy(x_iop->x__ptr, x_ptr, x_iop->x__cnt);
				x_ptr += x_iop->x__cnt;
				x_s -= x_iop->x__cnt;
			}
			/*
			 * filbuf clobbers _cnt & _ptr,
			 * so don't waste time setting them.
			 */
			if ((x_c = x__filbuf(x_iop)) == x_EOF)
				break;
			*x_ptr++ = x_c;
			x_s--;
		}
		if (x_iop->x__cnt >= x_s) {
			x_bcopy(x_iop->x__ptr, x_ptr, x_s);
			x_iop->x__ptr += x_s;
			x_iop->x__cnt -= x_s;
			return (x_count);
		}
	}
	return (x_size != 0 ? x_count - ((x_s + x_size - 1) / x_size) : 0);
}
