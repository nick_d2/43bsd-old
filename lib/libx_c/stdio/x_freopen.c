#include "x_.h"

#include <x_unistd.h>
/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)freopen.c	5.2 (Berkeley) 3/9/86";
#endif

#include <sys/x_types.h>
#include <sys/x_file.h>
#include <x_stdio.h>

x_FILE *x_freopen(x_file, x_mode, x_iop) char *x_file; register char *x_mode; register x_FILE *x_iop; {
	register x_int x_f, x_rw, x_oflags;

	x_rw = (x_mode[1] == '+');

	x_fclose(x_iop);

	switch (*x_mode) {
	case 'a':
		x_oflags = x_O_CREAT | (x_rw ? x_O_RDWR : x_O_WRONLY);
		break;
	case 'r':
		x_oflags = x_rw ? x_O_RDWR : x_O_RDONLY;
		break;
	case 'w':
		x_oflags = x_O_TRUNC | x_O_CREAT | (x_rw ? x_O_RDWR : x_O_WRONLY);
		break;
	x_default:
		return (x_NULL);
	}

	x_f = x_open(x_file, x_oflags, 0666);
	if (x_f < 0)
		return (x_NULL);

	if (*x_mode == 'a')
		x_lseek(x_f, (x_off_t)0, x_L_XTND);

	x_iop->x__cnt = 0;
	x_iop->x__file = x_f;
	x_iop->x__bufsiz = 0;
	if (x_rw)
		x_iop->x__flag = x__IORW;
	else if (*x_mode == 'r')
		x_iop->x__flag = x__IOREAD;
	else
		x_iop->x__flag = x__IOWRT;
	x_iop->x__base = x_iop->x__ptr = x_NULL;
	return (x_iop);
}
