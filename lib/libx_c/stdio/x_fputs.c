#include "x_.h"

/*
 * Copyright (c) 1984 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)fputs.c	5.2 (Berkeley) 3/9/86";
#endif

#include	<x_stdio.h>

x_int x_fputs(x_s, x_iop) register char *x_s; register x_FILE *x_iop; {
	register x_int x_r = 0;
	register x_int x_c;
	x_int x_unbuffered;
	char x_localbuf[x_BUFSIZ];

	x_unbuffered = x_iop->x__flag & x__IONBF;
	if (x_unbuffered) {
		x_iop->x__flag &= ~x__IONBF;
		x_iop->x__ptr = x_iop->x__base = x_localbuf;
		x_iop->x__bufsiz = x_BUFSIZ;
	}

	while (x_c = *x_s++)
		x_r = x_putc(x_c, x_iop);

	if (x_unbuffered) {
		x_fflush(x_iop);
		x_iop->x__flag |= x__IONBF;
		x_iop->x__base = x_NULL;
		x_iop->x__bufsiz = x_NULL;
		x_iop->x__cnt = 0;
	}

	return(x_r);
}
