#include "x_.h"

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)fgetc.c	5.2 (Berkeley) 3/9/86";
#endif

#include <x_stdio.h>

x_int x_fgetc(x_fp) x_FILE *x_fp; {
	return(x_getc(x_fp));
}
