#include "x_.h"

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)getw.c	5.2 (Berkeley) 3/9/86";
#endif

#include <x_stdio.h>

x_int x_getw(x_iop) register x_FILE *x_iop; {
	register x_int x_i;
	register char *x_p;
	x_int x_w;

	x_p = (char *)&x_w;
	for (x_i=sizeof(x_int); --x_i>=0;)
		*x_p++ = x_getc(x_iop);
	if (x_feof(x_iop))
		return(x_EOF);
	return(x_w);
}
