#include "x_.h"

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)putchar.c	5.2 (Berkeley) 3/9/86";
#endif

/*
 * A subroutine version of the macro putchar
 */
#include <x_stdio.h>

#undef x_putchar

x_int x_putchar(x_c) register x_int x_c; {
	x_putc(x_c, x_stdout);
}
