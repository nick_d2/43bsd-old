#include "x_.h"

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)strout.c	5.2 (Berkeley) 3/9/86";
#endif

#include	<x_stdio.h>

x_int x__strout(x_count, x_string, x_adjust, x_file, x_fillch) register x_int x_count; register char *x_string; x_int x_adjust; register x_FILE *x_file; x_int x_fillch; {
	while (x_adjust < 0) {
		if (*x_string=='-' && x_fillch=='0') {
			x_putc(*x_string++, x_file);
			x_count--;
		}
		x_putc(x_fillch, x_file);
		x_adjust++;
	}
	while (--x_count>=0)
		x_putc(*x_string++, x_file);
	while (x_adjust) {
		x_putc(x_fillch, x_file);
		x_adjust--;
	}
}
