#include "x_.h"

#include <x_mp.h>
#include <x_unistd.h>
/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)filbuf.c	5.3 (Berkeley) 3/9/86";
#endif

#include	<x_stdio.h>
#include	<sys/x_types.h>
#include	<sys/x_stat.h>
char	*x_malloc();

x_int x__filbuf(x_iop) register x_FILE *x_iop; {
	x_int x_size;
	struct x_stat x_stbuf;
	extern char *x__smallbuf;
	char x_c;

	if (x_iop->x__flag & x__IORW)
		x_iop->x__flag |= x__IOREAD;

	if ((x_iop->x__flag&x__IOREAD) == 0)
		return(x_EOF);
	if (x_iop->x__flag&(x__IOSTRG|x__IOEOF))
		return(x_EOF);
x_tryagain:
	if (x_iop->x__base==x_NULL) {
		if (x_iop->x__flag&x__IONBF) {
			x_iop->x__base = x__smallbuf ? &x__smallbuf[x_fileno(x_iop)] : &x_c;
			goto x_tryagain;
		}
		if (x_fstat(x_fileno(x_iop), &x_stbuf) < 0 || x_stbuf.x_st_blksize <= x_NULL)
			x_size = x_BUFSIZ;
		else
			x_size = x_stbuf.x_st_blksize;
		if ((x_iop->x__base = x_malloc(x_size)) == x_NULL) {
			x_iop->x__flag |= x__IONBF;
			goto x_tryagain;
		}
		x_iop->x__flag |= x__IOMYBUF;
		x_iop->x__bufsiz = x_size;
	}
	if (x_iop == x_stdin) {
		if (x_stdout->x__flag&x__IOLBF)
			x_fflush(x_stdout);
		if (x_stderr->x__flag&x__IOLBF)
			x_fflush(x_stderr);
	}
	x_iop->x__cnt = x_read(x_fileno(x_iop), x_iop->x__base,
		x_iop->x__flag & x__IONBF ? 1 : x_iop->x__bufsiz);
	x_iop->x__ptr = x_iop->x__base;
	if (x_iop->x__flag & x__IONBF && x_iop->x__base == &x_c)
		x_iop->x__base = x_NULL;
	if (--x_iop->x__cnt < 0) {
		if (x_iop->x__cnt == -1) {
			x_iop->x__flag |= x__IOEOF;
			if (x_iop->x__flag & x__IORW)
				x_iop->x__flag &= ~x__IOREAD;
		} else
			x_iop->x__flag |= x__IOERR;
		x_iop->x__cnt = 0;
		return(x_EOF);
	}
	return(*x_iop->x__ptr++&0377);
}
