#include "x_.h"

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)scanf.c	5.2 (Berkeley) 3/9/86";
#endif

#include <x_stdio.h>
#ifdef __STDC__
#include <stdarg.h>
#define x__va_start(x_ap, x_arg) va_start(x_ap, x_arg)
#else
#include <varargs.h>
#define x__va_start(x_ap, x_arg) va_start(x_ap)
#endif

#ifdef __STDC__
x_int x_scanf(char *x_fmt, ...)
#else
x_int x_scanf(x_fmt, va_alist) char *x_fmt; va_dcl
#endif
{
	va_list x_ap;
	x_int x_len;

	x__va_start(x_ap, x_fmt);
	x_len = x__doscan(x_stdin, x_fmt, &x_args);
	va_end(x_ap);
	return x_len;
}

#ifdef __STDC__
x_int x_fscanf(x_FILE *x_iop, char *x_fmt, ...)
#else
x_int x_fscanf(x_iop, x_fmt, va_alist) x_FILE *x_iop; char *x_fmt; va_dcl
#endif
{
	va_list x_ap;
	x_int x_len;

	x__va_start(x_ap, x_fmt);
	x_len = x__doscan(x_iop, x_fmt, &x_args);
	va_end(x_ap);
	return x_len;
}

#ifdef __STDC__
x_int x_sscanf(register char *x_str, char *x_fmt, ...)
#else
x_int x_sscanf(x_str, x_fmt, va_alist) register char *x_str; char *x_fmt; va_dcl
#endif
{
	x_FILE x__strbuf;
	va_list x_ap;
	x_int x_len;

	x__va_start(x_ap, x_fmt);
	x__strbuf.x__flag = x__IOREAD|x__IOSTRG;
	x__strbuf.x__ptr = x__strbuf.x__base = x_str;
	x__strbuf.x__cnt = 0;
	while (*x_str++)
		x__strbuf.x__cnt++;
	x__strbuf.x__bufsiz = x__strbuf.x__cnt;
	x_len = x__doscan(&x__strbuf, x_fmt, &x_args);
	va_end(x_ap);
	return x_len;
}
