#include "x_.h"

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)ungetc.c	5.3 (Berkeley) 3/26/86";
#endif

#include <x_stdio.h>

x_int x_ungetc(x_c, x_iop) x_int x_c; register x_FILE *x_iop; {
	if (x_c == x_EOF || (x_iop->x__flag & (x__IOREAD|x__IORW)) == 0 ||
	    x_iop->x__ptr == x_NULL || x_iop->x__base == x_NULL)
		return (x_EOF);

	if (x_iop->x__ptr == x_iop->x__base)
		if (x_iop->x__cnt == 0)
			x_iop->x__ptr++;
		else
			return (x_EOF);

	x_iop->x__cnt++;
	*--x_iop->x__ptr = x_c;

	return (x_c);
}
