#include "x_.h"

#include <x_unistd.h>
/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)fdopen.c	5.2 (Berkeley) 3/9/86";
#endif

/*
 * Unix routine to do an "fopen" on file descriptor
 * The mode has to be repeated because you can't query its
 * status
 */

#include <sys/x_types.h>
#include <sys/x_file.h>
#include <x_stdio.h>

x_FILE *x_fdopen(x_fd, x_mode) x_int x_fd; register char *x_mode; {
	extern x_FILE *x__findiop();
	static x_int x_nofile = -1;
	register x_FILE *x_iop;

	if (x_nofile < 0)
		x_nofile = x_getdtablesize();

	if (x_fd < 0 || x_fd >= x_nofile)
		return (x_NULL);

	x_iop = x__findiop();
	if (x_iop == x_NULL)
		return (x_NULL);

	x_iop->x__cnt = 0;
	x_iop->x__file = x_fd;
	x_iop->x__bufsiz = 0;
	x_iop->x__base = x_iop->x__ptr = x_NULL;

	switch (*x_mode) {
	case 'r':
		x_iop->x__flag = x__IOREAD;
		break;
	case 'a':
		x_lseek(x_fd, (x_off_t)0, x_L_XTND);
		/* fall into ... */
	case 'w':
		x_iop->x__flag = x__IOWRT;
		break;
	x_default:
		return (x_NULL);
	}

	if (x_mode[1] == '+')
		x_iop->x__flag = x__IORW;

	return (x_iop);
}
