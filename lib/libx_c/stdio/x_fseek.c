#include "x_.h"

#include <x_unistd.h>
#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)fseek.c	5.3 (Berkeley) 3/9/86";
#endif

/*
 * Seek for standard library.  Coordinates with buffering.
 */

#include	<x_stdio.h>

x_long x_lseek();

x_int x_fseek(x_iop, x_offset, x_ptrname) register x_FILE *x_iop; x_long x_offset; x_int x_ptrname; {
	register x_int x_resync, x_c;
	x_long x_p = -1;			/* can't happen? */

	x_iop->x__flag &= ~x__IOEOF;
	if (x_iop->x__flag&x__IOREAD) {
		if (x_ptrname<2 && x_iop->x__base &&
			!(x_iop->x__flag&x__IONBF)) {
			x_c = x_iop->x__cnt;
			x_p = x_offset;
			if (x_ptrname==0) {
				x_long x_curpos = x_lseek(x_fileno(x_iop), 0L, 1);
				if (x_curpos == -1)
					return (-1);
				x_p += x_c - x_curpos;
			} else
				x_offset -= x_c;
			if(!(x_iop->x__flag&x__IORW) && x_c>0&&x_p<=x_c
			    && x_p>=x_iop->x__base-x_iop->x__ptr){
				x_iop->x__ptr += (x_int)x_p;
				x_iop->x__cnt -= (x_int)x_p;
				return(0);
			}
			x_resync = x_offset&01;
		} else 
			x_resync = 0;
		if (x_iop->x__flag & x__IORW) {
			x_iop->x__ptr = x_iop->x__base;
			x_iop->x__flag &= ~x__IOREAD;
			x_resync = 0;
		}
		x_p = x_lseek(x_fileno(x_iop), x_offset-x_resync, x_ptrname);
		x_iop->x__cnt = 0;
		if (x_resync && x_p != -1)
			if (x_getc(x_iop) == x_EOF)
				x_p = -1;
	}
	else if (x_iop->x__flag & (x__IOWRT|x__IORW)) {
		x_p = x_fflush(x_iop);
		if (x_iop->x__flag & x__IORW) {
			x_iop->x__cnt = 0;
			x_iop->x__flag &= ~x__IOWRT;
			x_iop->x__ptr = x_iop->x__base;
		}
		return(x_lseek(x_fileno(x_iop), x_offset, x_ptrname) == -1 || x_p == x_EOF ?
		    -1 : 0);
	}
	return(x_p==-1?-1:0);
}
