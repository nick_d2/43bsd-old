#include "x_.h"

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)gets.c	5.2 (Berkeley) 3/9/86";
#endif

#include	<x_stdio.h>

char *x_gets(x_s) char *x_s; {
	register x_int x_c;
	register char *x_cs;

	x_cs = x_s;
	while ((x_c = x_getchar()) != '\n' && x_c != x_EOF)
		*x_cs++ = x_c;
	if (x_c == x_EOF && x_cs==x_s)
		return(x_NULL);
	*x_cs++ = '\0';
	return(x_s);
}
