#include "x_.h"

#include <x_unistd.h>
#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)rew.c	5.2 (Berkeley) 3/9/86";
#endif

#include	<x_stdio.h>

x_int x_rewind(x_iop) register x_FILE *x_iop; {
	x_fflush(x_iop);
	x_lseek(x_fileno(x_iop), 0L, 0);
	x_iop->x__cnt = 0;
	x_iop->x__ptr = x_iop->x__base;
	x_iop->x__flag &= ~(x__IOERR|x__IOEOF);
	if (x_iop->x__flag & x__IORW)
		x_iop->x__flag &= ~(x__IOREAD|x__IOWRT);
}
