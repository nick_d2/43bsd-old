#include "x_.h"

#include <x_mp.h>
#include <x_unistd.h>
/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)flsbuf.c	5.2 (Berkeley) 3/9/86";
#endif

#include	<x_stdio.h>
#include	<sys/x_types.h>
#include	<sys/x_stat.h>

char	*x_malloc();

x_int x__flsbuf(x_c, x_iop) x_int x_c; register x_FILE *x_iop; {
	register char *x_base;
	register x_int x_n, x_rn;
	char x_c1;
	x_int x_size;
	struct x_stat x_stbuf;

	if (x_iop->x__flag & x__IORW) {
		x_iop->x__flag |= x__IOWRT;
		x_iop->x__flag &= ~(x__IOEOF|x__IOREAD);
	}

	if ((x_iop->x__flag&x__IOWRT)==0)
		return(x_EOF);
x_tryagain:
	if (x_iop->x__flag&x__IOLBF) {
		x_base = x_iop->x__base;
		*x_iop->x__ptr++ = x_c;
		if (x_iop->x__ptr >= x_base+x_iop->x__bufsiz || x_c == '\n') {
			x_n = x_write(x_fileno(x_iop), x_base, x_rn = x_iop->x__ptr - x_base);
			x_iop->x__ptr = x_base;
			x_iop->x__cnt = 0;
		} else
			x_rn = x_n = 0;
	} else if (x_iop->x__flag&x__IONBF) {
		x_c1 = x_c;
		x_rn = 1;
		x_n = x_write(x_fileno(x_iop), &x_c1, x_rn);
		x_iop->x__cnt = 0;
	} else {
		if ((x_base=x_iop->x__base)==x_NULL) {
			if (x_fstat(x_fileno(x_iop), &x_stbuf) < 0 ||
			    x_stbuf.x_st_blksize <= x_NULL)
				x_size = x_BUFSIZ;
			else
				x_size = x_stbuf.x_st_blksize;
			if ((x_iop->x__base=x_base=x_malloc(x_size)) == x_NULL) {
				x_iop->x__flag |= x__IONBF;
				goto x_tryagain;
			}
			x_iop->x__flag |= x__IOMYBUF;
			x_iop->x__bufsiz = x_size;
			if (x_iop==x_stdout && x_isatty(x_fileno(x_stdout))) {
				x_iop->x__flag |= x__IOLBF;
				x_iop->x__ptr = x_base;
				goto x_tryagain;
			}
			x_rn = x_n = 0;
		} else if ((x_rn = x_n = x_iop->x__ptr - x_base) > 0) {
			x_iop->x__ptr = x_base;
			x_n = x_write(x_fileno(x_iop), x_base, x_n);
		}
		x_iop->x__cnt = x_iop->x__bufsiz-1;
		*x_base++ = x_c;
		x_iop->x__ptr = x_base;
	}
	if (x_rn != x_n) {
		x_iop->x__flag |= x__IOERR;
		return(x_EOF);
	}
	return(x_c);
}

x_int x_fflush(x_iop) register x_FILE *x_iop; {
	register char *x_base;
	register x_int x_n;

	if ((x_iop->x__flag&(x__IONBF|x__IOWRT))==x__IOWRT
	 && (x_base=x_iop->x__base)!=x_NULL && (x_n=x_iop->x__ptr-x_base)>0) {
		x_iop->x__ptr = x_base;
		x_iop->x__cnt = (x_iop->x__flag&(x__IOLBF|x__IONBF)) ? 0 : x_iop->x__bufsiz;
		if (x_write(x_fileno(x_iop), x_base, x_n)!=x_n) {
			x_iop->x__flag |= x__IOERR;
			return(x_EOF);
		}
	}
	return(0);
}

x_int x_fclose(x_iop) register x_FILE *x_iop; {
	register x_int x_r;

	x_r = x_EOF;
	if (x_iop->x__flag&(x__IOREAD|x__IOWRT|x__IORW) && (x_iop->x__flag&x__IOSTRG)==0) {
		x_r = x_fflush(x_iop);
		if (x_close(x_fileno(x_iop)) < 0)
			x_r = x_EOF;
		if (x_iop->x__flag&x__IOMYBUF)
			x_free(x_iop->x__base);
	}
	x_iop->x__cnt = 0;
	x_iop->x__base = (char *)x_NULL;
	x_iop->x__ptr = (char *)x_NULL;
	x_iop->x__bufsiz = 0;
	x_iop->x__flag = 0;
	x_iop->x__file = 0;
	return(x_r);
}
