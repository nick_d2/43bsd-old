#include "x_.h"

#include <x_stdlib.h>
#include <x_unistd.h>
/*
 * Copyright (c) 1983, 1985 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)findiop.c	5.6 (Berkeley) 3/9/86";
#endif

#include <x_stdio.h>
#include <x_errno.h>

extern x_int x_errno;

#define x_active(x_iop)	((x_iop)->x__flag & (x__IOREAD|x__IOWRT|x__IORW))

#define x_NSTATIC	20	/* stdin + stdout + stderr + the usual */

x_FILE x__iob[x_NSTATIC] = {
	{ 0, x_NULL, x_NULL, 0, x__IOREAD,		0 },	/* stdin  */
	{ 0, x_NULL, x_NULL, 0, x__IOWRT,		1 },	/* stdout */
	{ 0, x_NULL, x_NULL, 0, x__IOWRT|x__IONBF,	2 },	/* stderr */
};

extern	char	*x_calloc();

static	char x_sbuf[x_NSTATIC];
char	*x__smallbuf = x_sbuf;
static	x_FILE	**x_iobglue;
static	x_FILE	**x_endglue;

/*
 * Find a free FILE for fopen et al.
 * We have a fixed static array of entries, and in addition
 * may allocate additional entries dynamically, up to the kernel
 * limit on the number of open files.
 * At first just check for a free slot in the fixed static array.
 * If none are available, then we allocate a structure to glue together
 * the old and new FILE entries, which are then no longer contiguous.
 */
x_FILE *x__findiop() {
	register x_FILE **x_iov, *x_iop;
	register x_FILE *x_fp;

	if (x_iobglue == 0) {
		for (x_iop = x__iob; x_iop < x__iob + x_NSTATIC; x_iop++)
			if (!x_active(x_iop))
				return (x_iop);

		if (x__f_morefiles() == 0) {
			x_errno = x_ENOMEM;
			return (x_NULL);
		}
	}

	x_iov = x_iobglue;
	while (*x_iov != x_NULL && x_active(*x_iov))
		if (++x_iov >= x_endglue) {
			x_errno = x_EMFILE;
			return (x_NULL);
		}

	if (*x_iov == x_NULL)
		*x_iov = (x_FILE *)x_calloc(1, sizeof **x_iov);

	return (*x_iov);
}

x_int x__f_morefiles() {
	register x_FILE **x_iov;
	register x_FILE *x_fp;
	register char *x_cp;
	x_int x_nfiles;

	x_nfiles = x_getdtablesize();

	x_iobglue = (x_FILE **)x_calloc(x_nfiles, sizeof *x_iobglue);
	if (x_iobglue == x_NULL)
		return (0);

	x_endglue = x_iobglue + x_nfiles;

	for (x_fp = x__iob, x_iov = x_iobglue; x_fp < &x__iob[x_NSTATIC]; /* void */)
		*x_iov++ = x_fp++;

	x__smallbuf = x_calloc(x_nfiles, sizeof(*x__smallbuf));
	return (1);
}

x_int x_f_prealloc() {
	register x_FILE **x_iov;
	register x_FILE *x_fp;

	if (x_iobglue == x_NULL && x__f_morefiles() == 0)
		return;

	for (x_iov = x_iobglue; x_iov < x_endglue; x_iov++)
		if (*x_iov == x_NULL)
			*x_iov = (x_FILE *)x_calloc(1, sizeof **x_iov);
}

x_int x__fwalk(x_function) register x_int (*x_function)(); {
	register x_FILE **x_iov;
	register x_FILE *x_fp;

	if (x_iobglue == x_NULL) {
		for (x_fp = x__iob; x_fp < &x__iob[x_NSTATIC]; x_fp++)
			if (x_active(x_fp))
				(*x_function)(x_fp);
	} else {
		for (x_iov = x_iobglue; x_iov < x_endglue; x_iov++)
			if (*x_iov && x_active(*x_iov))
				(*x_function)(*x_iov);
	}
}

x_int x__cleanup() {
	extern x_int x_fclose();

	x__fwalk(x_fclose);
}
