#include "x_.h"

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)puts.c	5.2 (Berkeley) 3/9/86";
#endif

#include	<x_stdio.h>

x_int x_puts(x_s) register char *x_s; {
	register x_int x_c;

	while (x_c = *x_s++)
		x_putchar(x_c);
	return(x_putchar('\n'));
}
