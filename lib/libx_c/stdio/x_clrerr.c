#include "x_.h"

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)clrerr.c	5.2 (Berkeley) 3/9/86";
#endif

#include <x_stdio.h>
#undef	x_clearerr

x_int x_clearerr(x_iop) register x_FILE *x_iop; {
	x_iop->x__flag &= ~(x__IOERR|x__IOEOF);
}
