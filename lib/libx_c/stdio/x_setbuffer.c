#include "x_.h"

#include <x_mp.h>
/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)setbuffer.c	5.2 (Berkeley) 3/9/86";
#endif

#include	<x_stdio.h>

x_int x_setbuffer(x_iop, x_buf, x_size) register x_FILE *x_iop; char *x_buf; x_int x_size; {
	if (x_iop->x__base != x_NULL && x_iop->x__flag&x__IOMYBUF)
		x_free(x_iop->x__base);
	x_iop->x__flag &= ~(x__IOMYBUF|x__IONBF|x__IOLBF);
	if ((x_iop->x__base = x_buf) == x_NULL) {
		x_iop->x__flag |= x__IONBF;
		x_iop->x__bufsiz = x_NULL;
	} else {
		x_iop->x__ptr = x_iop->x__base;
		x_iop->x__bufsiz = x_size;
	}
	x_iop->x__cnt = 0;
}

/*
 * set line buffering for either stdout or stderr
 */
x_int x_setlinebuf(x_iop) register x_FILE *x_iop; {
	char *x_buf;
	extern char *x_malloc();

	x_fflush(x_iop);
	x_setbuffer(x_iop, x_NULL, 0);
	x_buf = x_malloc(x_BUFSIZ);
	if (x_buf != x_NULL) {
		x_setbuffer(x_iop, x_buf, x_BUFSIZ);
		x_iop->x__flag |= x__IOLBF|x__IOMYBUF;
	}
}
