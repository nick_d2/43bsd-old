#include "x_.h"

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)fgets.c	5.2 (Berkeley) 3/9/86";
#endif

#include	<x_stdio.h>

char *x_fgets(x_s, x_n, x_iop) char *x_s; x_int x_n; register x_FILE *x_iop; {
	register x_int x_c;
	register char *x_cs;

	x_cs = x_s;
	while (--x_n>0 && (x_c = x_getc(x_iop)) != x_EOF) {
		*x_cs++ = x_c;
		if (x_c=='\n')
			break;
	}
	if (x_c == x_EOF && x_cs==x_s)
		return(x_NULL);
	*x_cs++ = '\0';
	return(x_s);
}
