#include "x_.h"

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)doscan.c	5.2 (Berkeley) 3/9/86";
#endif

#include <x_stdio.h>
#include <x_ctype.h>
#ifdef __STDC__
#include <stdarg.h>
#define x__va_start(x_ap, x_arg) va_start(x_ap, x_arg)
#else
#include <varargs.h>
#define x__va_start(x_ap, x_arg) va_start(x_ap)
#endif

#define	x_SPC	01
#define	x_STP	02

#define	x_SHORT	0
#define	x_REGULAR	1
#define	x_LONG	2
#define	x_INT	0
#define	x_FLOAT	1

static char *x__getccl();

static char x__sctab[256] = {
	0,0,0,0,0,0,0,0,
	0,x_SPC,x_SPC,0,0,0,0,0,
	0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,
	x_SPC,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,
};

#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

static x_int x__innum __P((x_int *x_ptr, x_int x_type, x_int x_len, x_int x_size, x_FILE *x_iop, x_int *x_eofptr));
static x_int x__instr __P((register char *x_ptr, x_int x_type, x_int x_len, register x_FILE *x_iop, x_int *x_eofptr));
static char *x__getccl __P((register unsigned char *x_s));

x_int x__doscan(x_iop, x_fmt, x_ap) x_FILE *x_iop; register char *x_fmt; register va_list x_ap; {
	register x_int x_ch;
	x_int x_nmatch, x_len, x_ch1;
	x_int *x_ptr, x_fileended, x_size;

	x_nmatch = 0;
	x_fileended = 0;
	for (;;) switch (x_ch = *x_fmt++) {
	case '\0': 
		return (x_nmatch);
	case '%': 
		if ((x_ch = *x_fmt++) == '%')
			goto x_def;
		x_ptr = 0;
		if (x_ch != '*')
			x_ptr = va_arg(x_ap, x_int *);
		else
			x_ch = *x_fmt++;
		x_len = 0;
		x_size = x_REGULAR;
		while (x_isdigit(x_ch)) {
			x_len = x_len*10 + x_ch - '0';
			x_ch = *x_fmt++;
		}
		if (x_len == 0)
			x_len = 30000;
		if (x_ch=='l') {
			x_size = x_LONG;
			x_ch = *x_fmt++;
		} else if (x_ch=='h') {
			x_size = x_SHORT;
			x_ch = *x_fmt++;
		} else if (x_ch=='[')
			x_fmt = x__getccl(x_fmt);
		if (x_isupper(x_ch)) {
			x_ch = x_tolower(x_ch);
			x_size = x_LONG;
		}
		if (x_ch == '\0')
			return(-1);
		if (x__innum(x_ptr, x_ch, x_len, x_size, x_iop, &x_fileended) && x_ptr)
			x_nmatch++;
		if (x_fileended)
			return(x_nmatch? x_nmatch: -1);
		break;

	case ' ':
	case '\n':
	case '\t': 
		while ((x_ch1 = x_getc(x_iop))==' ' || x_ch1=='\t' || x_ch1=='\n')
			;
		if (x_ch1 != x_EOF)
			x_ungetc(x_ch1, x_iop);
		break;

	x_default: 
	x_def:
		x_ch1 = x_getc(x_iop);
		if (x_ch1 != x_ch) {
			if (x_ch1==x_EOF)
				return(-1);
			x_ungetc(x_ch1, x_iop);
			return(x_nmatch);
		}
	}
}

static x_int x__innum(x_ptr, x_type, x_len, x_size, x_iop, x_eofptr) x_int *x_ptr; x_int x_type; x_int x_len; x_int x_size; x_FILE *x_iop; x_int *x_eofptr; {
	extern double x_atof();
	register char *x_np;
	char x_numbuf[64];
	register x_int x_c, x_base;
	x_int x_expseen, x_scale, x_negflg, x_c1, x_ndigit;
	x_long x_lcval;

	if (x_type=='c' || x_type=='s' || x_type=='[')
		return(x__instr(x_ptr? (char *)x_ptr: (char *)x_NULL, x_type, x_len, x_iop, x_eofptr));
	x_lcval = 0;
	x_ndigit = 0;
	x_scale = x_INT;
	if (x_type=='e'||x_type=='f')
		x_scale = x_FLOAT;
	x_base = 10;
	if (x_type=='o')
		x_base = 8;
	else if (x_type=='x')
		x_base = 16;
	x_np = x_numbuf;
	x_expseen = 0;
	x_negflg = 0;
	while ((x_c = x_getc(x_iop))==' ' || x_c=='\t' || x_c=='\n');
	if (x_c=='-') {
		x_negflg++;
		*x_np++ = x_c;
		x_c = x_getc(x_iop);
		x_len--;
	} else if (x_c=='+') {
		x_len--;
		x_c = x_getc(x_iop);
	}
	for ( ; --x_len>=0; *x_np++ = x_c, x_c = x_getc(x_iop)) {
		if (x_isdigit(x_c)
		 || x_base==16 && ('a'<=x_c && x_c<='f' || 'A'<=x_c && x_c<='F')) {
			x_ndigit++;
			if (x_base==8)
				x_lcval <<=3;
			else if (x_base==10)
				x_lcval = ((x_lcval<<2) + x_lcval)<<1;
			else
				x_lcval <<= 4;
			x_c1 = x_c;
			if (x_isdigit(x_c))
				x_c -= '0';
			else if ('a'<=x_c && x_c<='f')
				x_c -= 'a'-10;
			else
				x_c -= 'A'-10;
			x_lcval += x_c;
			x_c = x_c1;
			continue;
		} else if (x_c=='.') {
			if (x_base!=10 || x_scale==x_INT)
				break;
			x_ndigit++;
			continue;
		} else if ((x_c=='e'||x_c=='E') && x_expseen==0) {
			if (x_base!=10 || x_scale==x_INT || x_ndigit==0)
				break;
			x_expseen++;
			*x_np++ = x_c;
			x_c = x_getc(x_iop);
			if (x_c!='+'&&x_c!='-'&&('0'>x_c||x_c>'9'))
				break;
		} else
			break;
	}
	if (x_negflg)
		x_lcval = -x_lcval;
	if (x_c != x_EOF) {
		x_ungetc(x_c, x_iop);
		*x_eofptr = 0;
	} else
		*x_eofptr = 1;
 	if (x_ptr==x_NULL || x_np==x_numbuf || (x_negflg && x_np==x_numbuf+1) )/* gene dykes*/
		return(0);
	*x_np++ = 0;
	switch((x_scale<<4) | x_size) {

	case (x_FLOAT<<4) | x_SHORT:
	case (x_FLOAT<<4) | x_REGULAR:
		*(float *)x_ptr = x_atof(x_numbuf);
		break;

	case (x_FLOAT<<4) | x_LONG:
		*(double *)x_ptr = x_atof(x_numbuf);
		break;

	case (x_INT<<4) | x_SHORT:
		*(x_short *)x_ptr = x_lcval;
		break;

	case (x_INT<<4) | x_REGULAR:
		*(x_int *)x_ptr = x_lcval;
		break;

	case (x_INT<<4) | x_LONG:
		*(x_long *)x_ptr = x_lcval;
		break;
	}
	return(1);
}

static x_int x__instr(x_ptr, x_type, x_len, x_iop, x_eofptr) register char *x_ptr; x_int x_type; x_int x_len; register x_FILE *x_iop; x_int *x_eofptr; {
	register x_int x_ch;
	register char *x_optr;
	x_int x_ignstp;

	*x_eofptr = 0;
	x_optr = x_ptr;
	if (x_type=='c' && x_len==30000)
		x_len = 1;
	x_ignstp = 0;
	if (x_type=='s')
		x_ignstp = x_SPC;
	while ((x_ch = x_getc(x_iop)) != x_EOF && x__sctab[x_ch] & x_ignstp)
		;
	x_ignstp = x_SPC;
	if (x_type=='c')
		x_ignstp = 0;
	else if (x_type=='[')
		x_ignstp = x_STP;
	while (x_ch!=x_EOF && (x__sctab[x_ch]&x_ignstp)==0) {
		if (x_ptr)
			*x_ptr++ = x_ch;
		if (--x_len <= 0)
			break;
		x_ch = x_getc(x_iop);
	}
	if (x_ch != x_EOF) {
		if (x_len > 0)
			x_ungetc(x_ch, x_iop);
		*x_eofptr = 0;
	} else
		*x_eofptr = 1;
	if (x_ptr && x_ptr!=x_optr) {
		if (x_type!='c')
			*x_ptr++ = '\0';
		return(1);
	}
	return(0);
}

static char *x__getccl(x_s) register unsigned char *x_s; {
	register x_int x_c, x_t;

	x_t = 0;
	if (*x_s == '^') {
		x_t++;
		x_s++;
	}
	for (x_c = 0; x_c < (sizeof x__sctab / sizeof x__sctab[0]); x_c++)
		if (x_t)
			x__sctab[x_c] &= ~x_STP;
		else
			x__sctab[x_c] |= x_STP;
	if ((x_c = *x_s) == ']' || x_c == '-') {	/* first char is special */
		if (x_t)
			x__sctab[x_c] |= x_STP;
		else
			x__sctab[x_c] &= ~x_STP;
		x_s++;
	}
	while ((x_c = *x_s++) != ']') {
		if (x_c==0)
			return((char *)--x_s);
		else if (x_c == '-' && *x_s != ']' && x_s[-2] < *x_s) {
			for (x_c = x_s[-2] + 1; x_c < *x_s; x_c++)
				if (x_t)
					x__sctab[x_c] |= x_STP;
				else
					x__sctab[x_c] &= ~x_STP;
		} else if (x_t)
			x__sctab[x_c] |= x_STP;
		else
			x__sctab[x_c] &= ~x_STP;
	}
	return((char *)x_s);
}
