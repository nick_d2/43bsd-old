#include "x_.h"

#include <x_mp.h>
/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)setbuf.c	5.2 (Berkeley) 3/9/86";
#endif

#include	<x_stdio.h>

x_int x_setbuf(x_iop, x_buf) register x_FILE *x_iop; char *x_buf; {
	if (x_iop->x__base != x_NULL && x_iop->x__flag&x__IOMYBUF)
		x_free(x_iop->x__base);
	x_iop->x__flag &= ~(x__IOMYBUF|x__IONBF|x__IOLBF);
	if ((x_iop->x__base = x_buf) == x_NULL) {
		x_iop->x__flag |= x__IONBF;
		x_iop->x__bufsiz = x_NULL;
	} else {
		x_iop->x__ptr = x_iop->x__base;
		x_iop->x__bufsiz = x_BUFSIZ;
	}
	x_iop->x__cnt = 0;
}
