#include "x_.h"

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)putw.c	5.2 (Berkeley) 3/9/86";
#endif

#include <x_stdio.h>

x_int x_putw(x_w, x_iop) x_int x_w; register x_FILE *x_iop; {
	register char *x_p;
	register x_int x_i;

	x_p = (char *)&x_w;
	for (x_i=sizeof(x_int); --x_i>=0;)
		x_putc(*x_p++, x_iop);
	return(x_ferror(x_iop));
}
