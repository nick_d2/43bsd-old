#include "x_.h"

#include <x_stdlib.h>
#include <x_stdio.h>
#include <x_unistd.h>
#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)exit.c	5.2 (Berkeley) 3/9/86";
#endif

x_int x_exit(x_code) x_int x_code; {

	x__cleanup();
	x__exit(x_code);
}
