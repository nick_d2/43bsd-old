#include "x_.h"

#include <x_memory.h>
#include <x_strings.h>
/*
 * Copyright (c) 1988 Regents of the University of California.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms are permitted
 * provided that the above copyright notice and this paragraph are
 * duplicated in all such forms and that any documentation,
 * advertising materials, and other materials related to such
 * distribution and use acknowledge that the software was developed
 * by the University of California, Berkeley.  The name of the
 * University may not be used to endorse or promote products derived
 * from this software without specific prior written permission.
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
 * WARRANTIES OF MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 */

#if defined(x_LIBC_SCCS) && !defined(x_lint)
static char x_sccsid[] = "@(#)doprnt.c	5.35 (Berkeley) 6/27/88";
#endif

#include <sys/x_types.h>
#ifdef __STDC__
#include <stdarg.h>
#define x__va_start(x_ap, x_arg) va_start(x_ap, x_arg)
#else
#include <varargs.h>
#define x__va_start(x_ap, x_arg) va_start(x_ap)
#endif
#include <x_stdio.h>
#include <x_ctype.h>

/* 11-bit exponent (VAX G floating point) is 308 decimal digits */
#define	x_MAXEXP		308
/* 128 bit fraction takes up 39 decimal digits; max reasonable precision */
#define	x_MAXFRACT	39

#define	x_DEFPREC		6

#define	x_BUF		(x_MAXEXP+x_MAXFRACT+1)	/* + decimal point */

#define	x_PUTC(x_ch)	(void) x_putc(x_ch, x_fp)

#define	x_ARG() \
	x__ulong = x_flags&x_LONGINT ? va_arg(x_argp, x_long) : \
	    x_flags&x_SHORTINT ? va_arg(x_argp, x_short) : va_arg(x_argp, x_int);

#define	x_todigit(x_c)	((x_c) - '0')
#define	x_tochar(x_n)	((x_n) + '0')

/* have to deal with the negative buffer count kludge */
#define	x_NEGATIVE_COUNT_KLUDGE

#define	x_LONGINT		0x01		/* long integer */
#define	x_LONGDBL		0x02		/* long double; unimplemented */
#define	x_SHORTINT	0x04		/* short integer */
#define	x_ALT		0x08		/* alternate form */
#define	x_LADJUST		0x10		/* left adjustment */
#define	x_ZEROPAD		0x20		/* zero (as opposed to blank) pad */
#define	x_HEXPREFIX	0x40		/* add 0x or 0X prefix */

#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

static x_int x_cvt __P((double x_number, register x_int x_prec, x_int x_flags, char *x_signp, x_int x_fmtch, char *x_startp, char *x_endp));
static char *x_round __P((double x_fract, x_int *x_exp, register char *x_start, register char *x_end, x_int x_ch, char *x_signp));
static char *x_exponent __P((register char *x_p, register x_int x_exp, x_int x_fmtch));

x_int x__doprnt(x_fmt0, x_argp, x_fp) x_u_char *x_fmt0; va_list x_argp; register x_FILE *x_fp; {
	register x_u_char *x_fmt;	/* format string */
	register x_int x_ch;	/* character from fmt */
	register x_int x_cnt;	/* return value accumulator */
	register x_int x_n;		/* random handy integer */
	register char *x_t;	/* buffer pointer */
	double x__double;		/* double precision arguments %[eEfgG] */
	x_u_long x__ulong;		/* integer arguments %[diouxX] */
	x_int x_base;		/* base for [diouxX] conversion */
	x_int x_dprec;		/* decimal precision in [diouxX] */
	x_int x_fieldsz;		/* field size expanded by sign, etc */
	x_int x_flags;		/* flags as above */
	x_int x_fpprec;		/* `extra' floating precision in [eEfgG] */
	x_int x_prec;		/* precision from format (%.3d), or -1 */
	x_int x_realsz;		/* field size expanded by decimal precision */
	x_int x_size;		/* size of converted field or string */
	x_int x_width;		/* width from format (%8d), or 0 */
	char x_sign;		/* sign prefix (' ', '+', '-', or \0) */
	char x_softsign;		/* temporary negative sign for floats */
	char *x_digs;		/* digits for [diouxX] conversion */
	char x_buf[x_BUF];		/* space for %c, %[diouxX], %[eEfgG] */

	if (x_fp->x__flag & x__IORW) {
		x_fp->x__flag |= x__IOWRT;
		x_fp->x__flag &= ~(x__IOEOF|x__IOREAD);
	}
	if ((x_fp->x__flag & x__IOWRT) == 0)
		return (x_EOF);

	x_fmt = x_fmt0;
	x_digs = "0123456789abcdef";
	for (x_cnt = 0;; ++x_fmt) {
		x_n = x_fp->x__cnt;
		for (x_t = (char *)x_fp->x__ptr; (x_ch = *x_fmt) && x_ch != '%';
		     ++x_cnt, ++x_fmt)
			if (--x_n < 0
#ifdef x_NEGATIVE_COUNT_KLUDGE
			    && (!(x_fp->x__flag & x__IOLBF) || -x_n >= x_fp->x__bufsiz)
#endif
			    || x_ch == '\n' && x_fp->x__flag & x__IOLBF) {
				x_fp->x__cnt = x_n;
				x_fp->x__ptr = x_t;
				(void) x__flsbuf((x_u_char)x_ch, x_fp);
				x_n = x_fp->x__cnt;
				x_t = (char *)x_fp->x__ptr;
			} else
				*x_t++ = x_ch;
		x_fp->x__cnt = x_n;
		x_fp->x__ptr = x_t;
		if (!x_ch)
			return (x_cnt);

		x_flags = 0; x_dprec = 0; x_fpprec = 0; x_width = 0;
		x_prec = -1;
		x_sign = '\0';

x_rflag:		switch (*++x_fmt) {
		case ' ':
			/*
			 * ``If the space and + flags both appear, the space
			 * flag will be ignored.''
			 *	-- ANSI X3J11
			 */
			if (!x_sign)
				x_sign = ' ';
			goto x_rflag;
		case '#':
			x_flags |= x_ALT;
			goto x_rflag;
		case '*':
			/*
			 * ``A negative field width argument is taken as a
			 * - flag followed by a  positive field width.''
			 *	-- ANSI X3J11
			 * They don't exclude field widths read from args.
			 */
			if ((x_width = va_arg(x_argp, x_int)) >= 0)
				goto x_rflag;
			x_width = -x_width;
			/* FALLTHROUGH */
		case '-':
			x_flags |= x_LADJUST;
			goto x_rflag;
		case '+':
			x_sign = '+';
			goto x_rflag;
		case '.':
			if (*++x_fmt == '*')
				x_n = va_arg(x_argp, x_int);
			else {
				x_n = 0;
				while (x_isascii(*x_fmt) && x_isdigit(*x_fmt))
					x_n = 10 * x_n + x_todigit(*x_fmt++);
				--x_fmt;
			}
			x_prec = x_n < 0 ? -1 : x_n;
			goto x_rflag;
		case '0':
			/*
			 * ``Note that 0 is taken as a flag, not as the
			 * beginning of a field width.''
			 *	-- ANSI X3J11
			 */
			x_flags |= x_ZEROPAD;
			goto x_rflag;
		case '1': case '2': case '3': case '4':
		case '5': case '6': case '7': case '8': case '9':
			x_n = 0;
			do {
				x_n = 10 * x_n + x_todigit(*x_fmt);
			} while (x_isascii(*++x_fmt) && x_isdigit(*x_fmt));
			x_width = x_n;
			--x_fmt;
			goto x_rflag;
		case 'L':
			x_flags |= x_LONGDBL;
			goto x_rflag;
		case 'h':
			x_flags |= x_SHORTINT;
			goto x_rflag;
		case 'l':
			x_flags |= x_LONGINT;
			goto x_rflag;
		case 'c':
			*(x_t = x_buf) = va_arg(x_argp, x_int);
			x_size = 1;
			x_sign = '\0';
			goto x_pforw;
		case 'D':
			x_flags |= x_LONGINT;
			/*FALLTHROUGH*/
		case 'd':
		case 'i':
			x_ARG();
			if ((x_long)x__ulong < 0) {
				x__ulong = -x__ulong;
				x_sign = '-';
			}
			x_base = 10;
			goto x_number;
		case 'e':
		case 'E':
		case 'f':
		case 'g':
		case 'G':
			x__double = va_arg(x_argp, double);
			/*
			 * don't do unrealistic precision; just pad it with
			 * zeroes later, so buffer size stays rational.
			 */
			if (x_prec > x_MAXFRACT) {
				if (*x_fmt != 'g' && *x_fmt != 'G' || (x_flags&x_ALT))
					x_fpprec = x_prec - x_MAXFRACT;
				x_prec = x_MAXFRACT;
			}
			else if (x_prec == -1)
				x_prec = x_DEFPREC;
			/*
			 * softsign avoids negative 0 if _double is < 0 and
			 * no significant digits will be shown
			 */
			if (x__double < 0) {
				x_softsign = '-';
				x__double = -x__double;
			}
			else
				x_softsign = 0;
			/*
			 * cvt may have to round up past the "start" of the
			 * buffer, i.e. ``intf("%.2f", (double)9.999);'';
			 * if the first char isn't NULL, it did.
			 */
			*x_buf = x_NULL;
			x_size = x_cvt(x__double, x_prec, x_flags, &x_softsign, *x_fmt, x_buf,
			    x_buf + sizeof(x_buf));
			if (x_softsign)
				x_sign = '-';
			x_t = *x_buf ? x_buf : x_buf + 1;
			goto x_pforw;
		case 'n':
			if (x_flags & x_LONGINT)
				*va_arg(x_argp, x_long *) = x_cnt;
			else if (x_flags & x_SHORTINT)
				*va_arg(x_argp, x_short *) = x_cnt;
			else
				*va_arg(x_argp, x_int *) = x_cnt;
			break;
		case 'O':
			x_flags |= x_LONGINT;
			/*FALLTHROUGH*/
		case 'o':
			x_ARG();
			x_base = 8;
			goto x_nosign;
		case 'p':
			/*
			 * ``The argument shall be a pointer to void.  The
			 * value of the pointer is converted to a sequence
			 * of printable characters, in an implementation-
			 * defined manner.''
			 *	-- ANSI X3J11
			 */
			/* NOSTRICT */
			x__ulong = (x_u_long)va_arg(x_argp, void *);
			x_base = 16;
			goto x_nosign;
		case 's':
			if (!(x_t = va_arg(x_argp, char *)))
				x_t = "(null)";
			if (x_prec >= 0) {
				/*
				 * can't use strlen; can only look for the
				 * NUL in the first `prec' characters, and
				 * strlen() will go further.
				 */
				char *x_p, *x_memchr();

				if (x_p = x_memchr(x_t, 0, x_prec)) {
					x_size = x_p - x_t;
					if (x_size > x_prec)
						x_size = x_prec;
				} else
					x_size = x_prec;
			} else
				x_size = x_strlen(x_t);
			x_sign = '\0';
			goto x_pforw;
		case 'U':
			x_flags |= x_LONGINT;
			/*FALLTHROUGH*/
		case 'u':
			x_ARG();
			x_base = 10;
			goto x_nosign;
		case 'X':
			x_digs = "0123456789ABCDEF";
			/* FALLTHROUGH */
		case 'x':
			x_ARG();
			x_base = 16;
			/* leading 0x/X only if non-zero */
			if (x_flags & x_ALT && x__ulong != 0)
				x_flags |= x_HEXPREFIX;

			/* unsigned conversions */
x_nosign:			x_sign = '\0';
			/*
			 * ``... diouXx conversions ... if a precision is
			 * specified, the 0 flag will be ignored.''
			 *	-- ANSI X3J11
			 */
x_number:			if ((x_dprec = x_prec) >= 0)
				x_flags &= ~x_ZEROPAD;

			/*
			 * ``The result of converting a zero value with an
			 * explicit precision of zero is no characters.''
			 *	-- ANSI X3J11
			 */
			x_t = x_buf + x_BUF;
			if (x__ulong != 0 || x_prec != 0) {
				do {
					*--x_t = x_digs[x__ulong % x_base];
					x__ulong /= x_base;
				} while (x__ulong);
				x_digs = "0123456789abcdef";
				if (x_flags & x_ALT && x_base == 8 && *x_t != '0')
					*--x_t = '0'; /* octal leading 0 */
			}
			x_size = x_buf + x_BUF - x_t;

x_pforw:
			/*
			 * All reasonable formats wind up here.  At this point,
			 * `t' points to a string which (if not flags&LADJUST)
			 * should be padded out to `width' places.  If
			 * flags&ZEROPAD, it should first be prefixed by any
			 * sign or other prefix; otherwise, it should be blank
			 * padded before the prefix is emitted.  After any
			 * left-hand padding and prefixing, emit zeroes
			 * required by a decimal [diouxX] precision, then print
			 * the string proper, then emit zeroes required by any
			 * leftover floating precision; finally, if LADJUST,
			 * pad with blanks.
			 */

			/*
			 * compute actual size, so we know how much to pad
			 * fieldsz excludes decimal prec; realsz includes it
			 */
			x_fieldsz = x_size + x_fpprec;
			if (x_sign)
				x_fieldsz++;
			if (x_flags & x_HEXPREFIX)
				x_fieldsz += 2;
			x_realsz = x_dprec > x_fieldsz ? x_dprec : x_fieldsz;

			/* right-adjusting blank padding */
			if ((x_flags & (x_LADJUST|x_ZEROPAD)) == 0 && x_width)
				for (x_n = x_realsz; x_n < x_width; x_n++)
					x_PUTC(' ');
			/* prefix */
			if (x_sign)
				x_PUTC(x_sign);
			if (x_flags & x_HEXPREFIX) {
				x_PUTC('0');
				x_PUTC((char)*x_fmt);
			}
			/* right-adjusting zero padding */
			if ((x_flags & (x_LADJUST|x_ZEROPAD)) == x_ZEROPAD)
				for (x_n = x_realsz; x_n < x_width; x_n++)
					x_PUTC('0');
			/* leading zeroes from decimal precision */
			for (x_n = x_fieldsz; x_n < x_dprec; x_n++)
				x_PUTC('0');

			/* the string or number proper */
			if (x_fp->x__cnt - (x_n = x_size) >= 0 &&
			    (x_fp->x__flag & x__IOLBF) == 0) {
				x_fp->x__cnt -= x_n;
				x_bcopy(x_t, (char *)x_fp->x__ptr, x_n);
				x_fp->x__ptr += x_n;
			} else
				while (--x_n >= 0)
					x_PUTC(*x_t++);
			/* trailing f.p. zeroes */
			while (--x_fpprec >= 0)
				x_PUTC('0');
			/* left-adjusting padding (always blank) */
			if (x_flags & x_LADJUST)
				for (x_n = x_realsz; x_n < x_width; x_n++)
					x_PUTC(' ');
			/* finally, adjust cnt */
			x_cnt += x_width > x_realsz ? x_width : x_realsz;
			break;
		case '\0':	/* "%?" prints ?, unless ? is NULL */
			return (x_cnt);
		x_default:
			x_PUTC((char)*x_fmt);
			x_cnt++;
		}
	}
	/* NOTREACHED */
}

static x_int x_cvt(x_number, x_prec, x_flags, x_signp, x_fmtch, x_startp, x_endp) double x_number; register x_int x_prec; x_int x_flags; char *x_signp; x_int x_fmtch; char *x_startp; char *x_endp; {
	register char *x_p, *x_t;
	register double x_fract;
	x_int x_dotrim, x_expcnt, x_gformat;
	double x_integer, x_tmp, x_modf();
	char *x_exponent(), *x_round();

	x_dotrim = x_expcnt = x_gformat = 0;
	x_fract = x_modf(x_number, &x_integer);

	/* get an extra slot for rounding. */
	x_t = ++x_startp;

	/*
	 * get integer portion of number; put into the end of the buffer; the
	 * .01 is added for modf(356.0 / 10, &integer) returning .59999999...
	 */
	for (x_p = x_endp - 1; x_integer; ++x_expcnt) {
		x_tmp = x_modf(x_integer / 10, &x_integer);
		*x_p-- = x_tochar((x_int)((x_tmp + .01) * 10));
	}
	switch(x_fmtch) {
	case 'f':
		/* reverse integer into beginning of buffer */
		if (x_expcnt)
			for (; ++x_p < x_endp; *x_t++ = *x_p);
		else
			*x_t++ = '0';
		/*
		 * if precision required or alternate flag set, add in a
		 * decimal point.
		 */
		if (x_prec || x_flags&x_ALT)
			*x_t++ = '.';
		/* if requires more precision and some fraction left */
		if (x_fract) {
			if (x_prec)
				do {
					x_fract = x_modf(x_fract * 10, &x_tmp);
					*x_t++ = x_tochar((x_int)x_tmp);
				} while (--x_prec && x_fract);
			if (x_fract)
				x_startp = x_round(x_fract, (x_int *)x_NULL, x_startp,
				    x_t - 1, (char)0, x_signp);
		}
		for (; x_prec--; *x_t++ = '0');
		break;
	case 'e':
	case 'E':
x_eformat:	if (x_expcnt) {
			*x_t++ = *++x_p;
			if (x_prec || x_flags&x_ALT)
				*x_t++ = '.';
			/* if requires more precision and some integer left */
			for (; x_prec && ++x_p < x_endp; --x_prec)
				*x_t++ = *x_p;
			/*
			 * if done precision and more of the integer component,
			 * round using it; adjust fract so we don't re-round
			 * later.
			 */
			if (!x_prec && ++x_p < x_endp) {
				x_fract = 0;
				x_startp = x_round((double)0, &x_expcnt, x_startp,
				    x_t - 1, *x_p, x_signp);
			}
			/* adjust expcnt for digit in front of decimal */
			--x_expcnt;
		}
		/* until first fractional digit, decrement exponent */
		else if (x_fract) {
			/* adjust expcnt for digit in front of decimal */
			for (x_expcnt = -1;; --x_expcnt) {
				x_fract = x_modf(x_fract * 10, &x_tmp);
				if (x_tmp)
					break;
			}
			*x_t++ = x_tochar((x_int)x_tmp);
			if (x_prec || x_flags&x_ALT)
				*x_t++ = '.';
		}
		else {
			*x_t++ = '0';
			if (x_prec || x_flags&x_ALT)
				*x_t++ = '.';
		}
		/* if requires more precision and some fraction left */
		if (x_fract) {
			if (x_prec)
				do {
					x_fract = x_modf(x_fract * 10, &x_tmp);
					*x_t++ = x_tochar((x_int)x_tmp);
				} while (--x_prec && x_fract);
			if (x_fract)
				x_startp = x_round(x_fract, &x_expcnt, x_startp,
				    x_t - 1, (char)0, x_signp);
		}
		/* if requires more precision */
		for (; x_prec--; *x_t++ = '0');

		/* unless alternate flag, trim any g/G format trailing 0's */
		if (x_gformat && !(x_flags&x_ALT)) {
			while (x_t > x_startp && *--x_t == '0');
			if (*x_t == '.')
				--x_t;
			++x_t;
		}
		x_t = x_exponent(x_t, x_expcnt, x_fmtch);
		break;
	case 'g':
	case 'G':
		/* a precision of 0 is treated as a precision of 1. */
		if (!x_prec)
			++x_prec;
		/*
		 * ``The style used depends on the value converted; style e
		 * will be used only if the exponent resulting from the
		 * conversion is less than -4 or greater than the precision.''
		 *	-- ANSI X3J11
		 */
		if (x_expcnt > x_prec || !x_expcnt && x_fract && x_fract < .0001) {
			/*
			 * g/G format counts "significant digits, not digits of
			 * precision; for the e/E format, this just causes an
			 * off-by-one problem, i.e. g/G considers the digit
			 * before the decimal point significant and e/E doesn't
			 * count it as precision.
			 */
			--x_prec;
			x_fmtch -= 2;		/* G->E, g->e */
			x_gformat = 1;
			goto x_eformat;
		}
		/*
		 * reverse integer into beginning of buffer,
		 * note, decrement precision
		 */
		if (x_expcnt)
			for (; ++x_p < x_endp; *x_t++ = *x_p, --x_prec);
		else
			*x_t++ = '0';
		/*
		 * if precision required or alternate flag set, add in a
		 * decimal point.  If no digits yet, add in leading 0.
		 */
		if (x_prec || x_flags&x_ALT) {
			x_dotrim = 1;
			*x_t++ = '.';
		}
		else
			x_dotrim = 0;
		/* if requires more precision and some fraction left */
		if (x_fract) {
			if (x_prec) {
				do {
					x_fract = x_modf(x_fract * 10, &x_tmp);
					*x_t++ = x_tochar((x_int)x_tmp);
				} while(!x_tmp);
				while (--x_prec && x_fract) {
					x_fract = x_modf(x_fract * 10, &x_tmp);
					*x_t++ = x_tochar((x_int)x_tmp);
				}
			}
			if (x_fract)
				x_startp = x_round(x_fract, (x_int *)x_NULL, x_startp,
				    x_t - 1, (char)0, x_signp);
		}
		/* alternate format, adds 0's for precision, else trim 0's */
		if (x_flags&x_ALT)
			for (; x_prec--; *x_t++ = '0');
		else if (x_dotrim) {
			while (x_t > x_startp && *--x_t == '0');
			if (*x_t != '.')
				++x_t;
		}
	}
	return(x_t - x_startp);
}

static char *x_round(x_fract, x_exp, x_start, x_end, x_ch, x_signp) double x_fract; x_int *x_exp; register char *x_start; register char *x_end; x_int x_ch; char *x_signp; {
	double x_tmp;

	if (x_fract)
		(void)x_modf(x_fract * 10, &x_tmp);
	else
		x_tmp = x_todigit(x_ch);
	if (x_tmp > 4)
		for (;; --x_end) {
			if (*x_end == '.')
				--x_end;
			if (++*x_end <= '9')
				break;
			*x_end = '0';
			if (x_end == x_start) {
				if (x_exp) {	/* e/E; increment exponent */
					*x_end = '1';
					++*x_exp;
				}
				else {		/* f; add extra digit */
					*--x_end = '1';
					--x_start;
				}
				break;
			}
		}
	/* ``"%.3f", (double)-0.0004'' gives you a negative 0. */
	else if (*x_signp == '-')
		for (;; --x_end) {
			if (*x_end == '.')
				--x_end;
			if (*x_end != '0')
				break;
			if (x_end == x_start)
				*x_signp = 0;
		}
	return(x_start);
}

static char *x_exponent(x_p, x_exp, x_fmtch) register char *x_p; register x_int x_exp; x_int x_fmtch; {
	register char *x_t;
	char x_expbuf[x_MAXEXP];

	*x_p++ = x_fmtch;
	if (x_exp < 0) {
		x_exp = -x_exp;
		*x_p++ = '-';
	}
	else
		*x_p++ = '+';
	x_t = x_expbuf + x_MAXEXP;
	if (x_exp > 9) {
		do {
			*--x_t = x_tochar(x_exp % 10);
		} while ((x_exp /= 10) > 9);
		*--x_t = x_tochar(x_exp);
		for (; x_t < x_expbuf + x_MAXEXP; *x_p++ = *x_t++);
	}
	else {
		*x_p++ = '0';
		*x_p++ = x_tochar(x_exp);
	}
	return(x_p);
}
