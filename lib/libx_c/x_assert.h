#include "x_.h"

/*	assert.h	4.2	85/01/21	*/

# ifndef x_NDEBUG
# define x__assert(x_ex)	{if (!(x_ex)){x_fprintf(x_stderr,"Assertion failed: file \"%s\", line %d\n", __FILE__, __LINE__);x_exit(1);}}
# define x_assert(x_ex)	x__assert(x_ex)
# else
# define x__assert(x_ex)
# define x_assert(x_ex)
# endif
