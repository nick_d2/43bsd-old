#include "x_.h"

#include <sys/x_types.h>
#ifdef __STDC__
#include <stdarg.h>
#else
#include <varargs.h>
#endif

/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)stdio.h	5.3 (Berkeley) 3/15/86
 */

# ifndef x_FILE
#define	x_BUFSIZ	1024
extern	struct	x__iobuf {
	x_int	x__cnt;
	char	*x__ptr;		/* should be unsigned char */
	char	*x__base;		/* ditto */
	x_int	x__bufsiz;
	x_short	x__flag;
	char	x__file;		/* should be short */
} x__iob[];

#define	x__IOREAD	01
#define	x__IOWRT	02
#define	x__IONBF	04
#define	x__IOMYBUF	010
#define	x__IOEOF	020
#define	x__IOERR	040
#define	x__IOSTRG	0100
#define	x__IOLBF	0200
#define	x__IORW	0400
#define	x_NULL	0
#define	x_FILE	struct x__iobuf
#define	x_EOF	(-1)

#define	x_stdin	(&x__iob[0])
#define	x_stdout	(&x__iob[1])
#define	x_stderr	(&x__iob[2])
#ifndef x_lint
#define	x_getc(x_p)		(--(x_p)->x__cnt>=0? (x_int)(*(unsigned char *)(x_p)->x__ptr++):x__filbuf(x_p))
#endif
#define	x_getchar()	x_getc(x_stdin)
#ifndef x_lint
#define x_putc(x_x, x_p)	(--(x_p)->x__cnt >= 0 ?\
	(x_int)(*(unsigned char *)(x_p)->x__ptr++ = (x_x)) :\
	(((x_p)->x__flag & x__IOLBF) && -(x_p)->x__cnt < (x_p)->x__bufsiz ?\
		((*(x_p)->x__ptr = (x_x)) != '\n' ?\
			(x_int)(*(unsigned char *)(x_p)->x__ptr++) :\
			x__flsbuf(*(unsigned char *)(x_p)->x__ptr, x_p)) :\
		x__flsbuf((unsigned char)(x_x), x_p)))
#endif
#define	x_putchar(x_x)	x_putc(x_x,x_stdout)
#define	x_feof(x_p)		(((x_p)->x__flag&x__IOEOF)!=0)
#define	x_ferror(x_p)	(((x_p)->x__flag&x__IOERR)!=0)
#define	x_fileno(x_p)	((x_p)->x__file)
#define	x_clearerr(x_p)	((x_p)->x__flag &= ~(x__IOERR|x__IOEOF))

x_FILE	*x_fopen();
x_FILE	*x_fdopen();
x_FILE	*x_freopen();
x_FILE	*x_popen();
x_long	x_ftell();
char	*x_fgets();
char	*x_gets();
#if 0 /*def vax*/
char	*x_sprintf();		/* too painful to do right */
#endif
# endif

#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

/* stdio/fprintf.c */
x_int x_fprintf __P((register x_FILE *x_iop, char *x_fmt, ...));
/* stdio/putchar.c */
x_int x_putchar __P((register x_int x_c));
/* stdio/fread.c */
x_int x_fread __P((register char *x_ptr, x_unsigned_int x_size, x_unsigned_int x_count, register x_FILE *x_iop));
/* stdio/setbuf.c */
x_int x_setbuf __P((register x_FILE *x_iop, char *x_buf));
/* stdio/vprintf.c */
x_int x_vprintf __P((char *x_fmt, va_list x_ap));
/* stdio/getw.c */
x_int x_getw __P((register x_FILE *x_iop));
/* stdio/doscan.c */
x_int x__doscan __P((x_FILE *x_iop, register char *x_fmt, register va_list x_ap));
/* stdio/strout.c */
x_int x__strout __P((register x_int x_count, register char *x_string, x_int x_adjust, register x_FILE *x_file, x_int x_fillch));
/* stdio/clrerr.c */
x_int x_clearerr __P((register x_FILE *x_iop));
/* stdio/vfprintf.c */
x_int x_vfprintf __P((x_FILE *x_iop, char *x_fmt, va_list x_ap));
/* stdio/scanf.c */
x_int x_scanf __P((char *x_fmt, ...));
x_int x_fscanf __P((x_FILE *x_iop, char *x_fmt, ...));
x_int x_sscanf __P((register char *x_str, char *x_fmt, ...));
/* stdio/flsbuf.c */
x_int x__flsbuf __P((x_int x_c, register x_FILE *x_iop));
x_int x_fflush __P((register x_FILE *x_iop));
x_int x_fclose __P((register x_FILE *x_iop));
/* stdio/getchar.c */
x_int x_getchar __P((void));
/* stdio/filbuf.c */
x_int x__filbuf __P((register x_FILE *x_iop));
/* stdio/ftell.c */
x_long x_ftell __P((register x_FILE *x_iop));
/* stdio/doprnt.c */
x_int x__doprnt __P((x_u_char *x_fmt0, va_list x_argp, register x_FILE *x_fp));
/* stdio/fputc.c */
x_int x_fputc __P((x_int x_c, register x_FILE *x_fp));
/* stdio/printf.c */
x_int x_printf __P((char *x_fmt, ...));
/* stdio/putw.c */
x_int x_putw __P((x_int x_w, register x_FILE *x_iop));
/* stdio/gets.c */
char *x_gets __P((char *x_s));
/* stdio/fgets.c */
char *x_fgets __P((char *x_s, x_int x_n, register x_FILE *x_iop));
/* stdio/fwrite.c */
x_int x_fwrite __P((register char *x_ptr, x_unsigned_int x_size, x_unsigned_int x_count, register x_FILE *x_iop));
/* stdio/sprintf.c */
x_int x_sprintf __P((char *x_str, char *x_fmt, ...));
/* stdio/puts.c */
x_int x_puts __P((register char *x_s));
/* stdio/fdopen.c */
x_FILE *x_fdopen __P((x_int x_fd, register char *x_mode));
/* stdio/setbuffer.c */
x_int x_setbuffer __P((register x_FILE *x_iop, char *x_buf, x_int x_size));
x_int x_setlinebuf __P((register x_FILE *x_iop));
/* stdio/fputs.c */
x_int x_fputs __P((register char *x_s, register x_FILE *x_iop));
/* stdio/vsprintf.c */
x_int x_vsprintf __P((char *x_str, char *x_fmt, va_list x_ap));
/* stdio/fopen.c */
x_FILE *x_fopen __P((char *x_file, register char *x_mode));
/* stdio/rew.c */
x_int x_rewind __P((register x_FILE *x_iop));
/* stdio/fseek.c */
x_int x_fseek __P((register x_FILE *x_iop, x_long x_offset, x_int x_ptrname));
/* stdio/freopen.c */
x_FILE *x_freopen __P((char *x_file, register char *x_mode, register x_FILE *x_iop));
/* stdio/fgetc.c */
x_int x_fgetc __P((x_FILE *x_fp));
/* stdio/findiop.c */
x_FILE *x__findiop __P((void));
x_int x__f_morefiles __P((void));
x_int x_f_prealloc __P((void));
x_int x__fwalk __P((register x_int (*x_function)(void)));
x_int x__cleanup __P((void));
/* stdio/ungetc.c */
x_int x_ungetc __P((x_int x_c, register x_FILE *x_iop));
/* net/rcmd.c */
x_int x_rcmd __P((char **x_ahost, x_int x_rport, char *x_locuser, char *x_remuser, char *x_cmd, x_int *x_fd2p));
x_int x_rresvport __P((x_int *x_alport));
x_int x_ruserok __P((char *x_rhost, x_int x_superuser, char *x_ruser, char *x_luser));
x_int x__validuser __P((x_FILE *x_hostf, char *x_rhost, char *x_luser, char *x_ruser, x_int x_baselen));
x_int x__checkhost __P((char *x_rhost, char *x_lhost, x_int x_len));
/* compat-sys5/tmpnam.c */
char *x_tmpnam __P((char *x_s));
/* gen/perror.c */
x_int x_perror __P((char *x_s));
/* gen/popen.c */
x_FILE *x_popen __P((char *x_cmd, char *x_mode));
x_int x_pclose __P((x_FILE *x_ptr));
/* gen/fakcu.c */
x_int x__cleanup __P((void));
