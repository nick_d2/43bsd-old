#!/bin/sh

find temp_c -name '*.c' -print |\
sed -e 's:^temp_c/::' |\
while read i
do
  mkdir -p `dirname $i`
  sed \
-e 's/#include <varargs.h>/#ifdef __STDC__\n#include <stdarg.h>\n#define _va_start(ap, arg) va_start(ap, arg)\n#else\n#include <varargs.h>\n#define _va_start(ap, arg) va_start(ap)\n#endif/' \
-e 's/va_start(ap);/_va_start(ap, fmt);/' \
-e 's/^\(#[\t ]*\)\(else\|endif\).*/\1\2/' <temp_c/$i |\
  ../../xify/xify >`dirname $i`/x_`basename $i`
done

find temp_h -name '*.h' -print |\
sed -e 's:^temp_h/::' |\
while read i
do
  mkdir -p `dirname $i`
  sed \
-e 's/#include <varargs.h>/#ifdef __STDC__\n#include <stdarg.h>\n#define _va_start(ap, arg) va_start(ap, arg)\n#else\n#include <varargs.h>\n#define _va_start(ap, arg) va_start(ap)\n#endif/' \
-e 's/va_start(ap);/_va_start(ap, fmt);/' \
-e 's/^\(#[\t ]*\)\(else\|endif\).*/\1\2/' <temp_h/$i |\
  ../../xify/xify >`dirname $i`/x_`basename $i`
done

rm -f machine
ln -s vax machine
rm -f x_frame.h
ln -s machine/x_frame.h x_frame.h
for i in x_errno.h x_signal.h x_syslog.h
do
  rm -f $i
  ln -s sys/$i $i
done

patch x_stdio.h <x_stdio.h.patch
patch x_unistd.h <x_unistd.h.patch
