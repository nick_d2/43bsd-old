/* see usr.bin/lint/llib-lc */

#include <nox_stdio.h>
#include <nox_stdlib.h>

#include <sys/types.h>
#include <sys/time.h>

#include <sys/dir.h>
#include <sys/resource.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/timeb.h>
#include <sys/times.h>
#include <sys/uio.h>
#include <sys/vtimes.h>
#include <sys/wait.h>

#include <netinet/in.h>

#include <netns/ns.h>

#include <arpa/inet.h>

#include <disktab.h>
#include <fstab.h>
#include <grp.h>
#include <ndbm.h>
#include <netdb.h>
#include <nlist.h>
#include <pwd.h>
#include <setjmp.h>
#include <sgtty.h>
#include <signal.h>
#include <stdio.h>
#include <ttyent.h>

#ifdef __STDC__
#include <stdarg.h>
#else
#include <varargs.h>
#endif

#include <errno.h>
#include <syscall.h>

int errno;

int accept(s, a, l) int s; struct sockaddr *a; int *l; {
  nox_fprintf(nox_stderr, "accept() called\n");
  nox_abort();
}

int access(p, m) char *p; int m; {
  nox_fprintf(nox_stderr, "access() called\n");
  nox_abort();
}

int acct(f) char *f; {
  nox_fprintf(nox_stderr, "acct() called\n");
  nox_abort();
}

int adjtime(delta, odelta) struct timeval *delta; struct timeval *odelta; {
  nox_fprintf(nox_stderr, "adjtime() called\n");
  nox_abort();
}

int bind(s, n, l) int s; struct sockaddr *n; int l; {
  nox_fprintf(nox_stderr, "bind() called\n");
  nox_abort();
}

char *brk(a) char *a; {
  nox_fprintf(nox_stderr, "brk() called\n");
  nox_abort();
}

int chdir(s) char *s; {
  nox_fprintf(nox_stderr, "chdir() called\n");
  nox_abort();
}

int chmod(s, m) char *s; int m; {
  nox_fprintf(nox_stderr, "chmod() called\n");
  nox_abort();
}

int chown(s, u, g) char *s; int u; int g; {
  nox_fprintf(nox_stderr, "chown() called\n");
  nox_abort();
}

int chroot(d) char *d; {
  nox_fprintf(nox_stderr, "chroot() called\n");
  nox_abort();
}

int close(f) int f; {
  nox_fprintf(nox_stderr, "close() called\n");
  nox_abort();
}

int connect(s, n, l) int s; struct sockaddr *n; int l; {
  nox_fprintf(nox_stderr, "connect() called\n");
  nox_abort();
}

int dup(f) int f; {
  nox_fprintf(nox_stderr, "dup() called\n");
  nox_abort();
}

int dup2(o, n) int o; int n; {
  nox_fprintf(nox_stderr, "dup2() called\n");
  nox_abort();
}

void execve(s, v, e) char *s; char *v[]; char *e[]; {
  nox_fprintf(nox_stderr, "execve() called\n");
  nox_abort();
}

void _exit(s) int s; {
  nox_fprintf(nox_stderr, "_exit() called\n");
  nox_abort();
}

int fchmod(f, m) int f; int m; {
  nox_fprintf(nox_stderr, "fchmod() called\n");
  nox_abort();
}

int fchown(f, u, g) int f; int u; int g; {
  nox_fprintf(nox_stderr, "fchown() called\n");
  nox_abort();
}

int fcntl(f, c, a) int f; int c; int a; {
  nox_fprintf(nox_stderr, "fcntl() called\n");
  nox_abort();
}

int flock(f, o) int f; int o; {
  nox_fprintf(nox_stderr, "flock() called\n");
  nox_abort();
}

int fork() {
  nox_fprintf(nox_stderr, "fork() called\n");
  nox_abort();
}

int fsync(f) int f; {
  nox_fprintf(nox_stderr, "fsync() called\n");
  nox_abort();
}

int fstat(f, b) int f; struct stat *b; {
  nox_fprintf(nox_stderr, "fstat() called\n");
  nox_abort();
}

int ftruncate(d, l) int d; off_t l; {
  nox_fprintf(nox_stderr, "ftruncate() called\n");
  nox_abort();
}

int getdtablesize() {
  nox_fprintf(nox_stderr, "getdtablesize() called\n");
  nox_abort();
}

gid_t getegid() {
  nox_fprintf(nox_stderr, "getegid() called\n");
  nox_abort();
}

uid_t geteuid() {
  nox_fprintf(nox_stderr, "geteuid() called\n");
  nox_abort();
}

gid_t getgid() {
  nox_fprintf(nox_stderr, "getgid() called\n");
  nox_abort();
}

int getgroups(n, g) int n; int *g; {
  nox_fprintf(nox_stderr, "getgroups() called\n");
  nox_abort();
}

long gethostid() {
  nox_fprintf(nox_stderr, "gethostid() called\n");
  nox_abort();
}

int gethostname(n, l) char *n; int l; {
  nox_fprintf(nox_stderr, "gethostname() called\n");
  nox_abort();
}

int getitimer(w, v) int w; struct itimerval *v; {
  nox_fprintf(nox_stderr, "getitimer() called\n");
  nox_abort();
}

int getpagesize() {
  nox_fprintf(nox_stderr, "getpagesize() called\n");
  nox_abort();
}

int getpeername(s, n, l) int s; struct sockaddr *n; int *l; {
  nox_fprintf(nox_stderr, "getpeername() called\n");
  nox_abort();
}

int getpgrp(p) int p; {
  nox_fprintf(nox_stderr, "getpgrp() called\n");
  nox_abort();
}

int getpid() {
  nox_fprintf(nox_stderr, "getpid() called\n");
  nox_abort();
}

int getppid() {
  nox_fprintf(nox_stderr, "getppid() called\n");
  nox_abort();
}

int getpriority(w, who) int w; int who; {
  nox_fprintf(nox_stderr, "getpriority() called\n");
  nox_abort();
}

int getrlimit(res, rip) int res; struct rlimit *rip; {
  nox_fprintf(nox_stderr, "getrlimit() called\n");
  nox_abort();
}

int getrusage(res, rip) int res; struct rusage *rip; {
  nox_fprintf(nox_stderr, "getrusage() called\n");
  nox_abort();
}

int getsockname(s, name, namelen) int s; char *name; int *namelen; {
  nox_fprintf(nox_stderr, "getsockname() called\n");
  nox_abort();
}

int getsockopt(s, level, opt, buf, len) int s; int level; int opt; char *buf; int *len; {
  nox_fprintf(nox_stderr, "getsockopt() called\n");
  nox_abort();
}

int gettimeofday(t, z) struct timeval *t; struct timezone *z; {
  nox_fprintf(nox_stderr, "gettimeofday() called\n");
  nox_abort();
}

uid_t getuid() {
  nox_fprintf(nox_stderr, "getuid() called\n");
  nox_abort();
}

int ioctl(d, r, p) int d; u_long r; char *p; {
  nox_fprintf(nox_stderr, "ioctl() called\n");
  nox_abort();
}

int kill(p, s) int p; int s; {
  nox_fprintf(nox_stderr, "kill() called\n");
  nox_abort();
}

int killpg(pg, s) int pg; int s; {
  nox_fprintf(nox_stderr, "killpg() called\n");
  nox_abort();
}

int link(a, b) char *a; char *b; {
  nox_fprintf(nox_stderr, "link() called\n");
  nox_abort();
}

int listen(s, b) int s; int b; {
  nox_fprintf(nox_stderr, "listen() called\n");
  nox_abort();
}

off_t lseek(f, o, d) int f; off_t o; int d; {
  nox_fprintf(nox_stderr, "lseek() called\n");
  nox_abort();
}

int lstat(s, b) char *s; struct stat *b; {
  nox_fprintf(nox_stderr, "lstat() called\n");
  nox_abort();
}

#ifdef notdef
int madvise(a, l, b) char *a; int l; int b; {
  nox_fprintf(nox_stderr, "madvise() called\n");
  nox_abort();
}

int mmap(a, l, p, s, f, o) char *a; int l; int p; int s; int f; off_t o; {
  nox_fprintf(nox_stderr, "mmap() called\n");
  nox_abort();
}

int mincore(a, l, v) char *a; int l; char *v; {
  nox_fprintf(nox_stderr, "mincore() called\n");
  nox_abort();
}

#endif
int mkdir(p, m) char *p; int m; {
  nox_fprintf(nox_stderr, "mkdir() called\n");
  nox_abort();
}

int mknod(n, m, a) char *n; int m; int a; {
  nox_fprintf(nox_stderr, "mknod() called\n");
  nox_abort();
}

int mount(s, n, f) char *s; char *n; int f; {
  nox_fprintf(nox_stderr, "mount() called\n");
  nox_abort();
}

#ifdef notdef
int mprotect(a, l, p) char *a; int l; int p; {
  nox_fprintf(nox_stderr, "mprotect() called\n");
  nox_abort();
}

int mremap(a, l, p, s, f) char *a; int l; int p; int s; int f; {
  nox_fprintf(nox_stderr, "mremap() called\n");
  nox_abort();
}

int munmap(a, l) char *a; int l; {
  nox_fprintf(nox_stderr, "munmap() called\n");
  nox_abort();
}
#endif

#ifdef __STDC__
int open(char *f, int m, ...)
#else
int open(f, m, va_alist) char *f; int m; va_dcl
#endif
{
  nox_fprintf(nox_stderr, "open() called\n");
  nox_abort();
}

int pipe(f) int f[2]; {
  nox_fprintf(nox_stderr, "pipe() called\n");
  nox_abort();
}

void profil(b, s, o, i) char *b; int s; int o; int i; {
  nox_fprintf(nox_stderr, "profil() called\n");
  nox_abort();
}

int ptrace(r, p, a, d) int r; int p; int *a; int d; {
  nox_fprintf(nox_stderr, "ptrace() called\n");
  nox_abort();
}

int quota(c, u, a, ad) int c; int u; int a; char *ad; {
  nox_fprintf(nox_stderr, "quota() called\n");
  nox_abort();
}

int read(f, b, l) int f; char *b; int l; {
  nox_fprintf(nox_stderr, "read() called\n");
  nox_abort();
}

int readv(d, v, l) int d; struct iovec *v; int l; {
  nox_fprintf(nox_stderr, "readv() called\n");
  nox_abort();
}

int readlink(p, b, s) char *p; char *b; int s; {
  nox_fprintf(nox_stderr, "readlink() called\n");
  nox_abort();
}

void reboot(h) int h; {
  nox_fprintf(nox_stderr, "reboot() called\n");
  nox_abort();
}

int recv(s, b, l, f) int s; char *b; int l; int f; {
  nox_fprintf(nox_stderr, "recv() called\n");
  nox_abort();
}

int recvfrom(s, b, l, f, fr, fl) int s; char *b; int l; int f; struct sockaddr *fr; int *fl; {
  nox_fprintf(nox_stderr, "recvfrom() called\n");
  nox_abort();
}

int recvmsg(s, m, f) int s; struct msghdr m[]; int f; {
  nox_fprintf(nox_stderr, "recvmsg() called\n");
  nox_abort();
}

int rename(f, t) char *f; char *t; {
  nox_fprintf(nox_stderr, "rename() called\n");
  nox_abort();
}

int rmdir(p) char *p; {
  nox_fprintf(nox_stderr, "rmdir() called\n");
  nox_abort();
}

char *sbrk(i) int i; {
  nox_fprintf(nox_stderr, "sbrk() called\n");
  nox_abort();
}

int select(n, r, w, e, t) int n; fd_set *r; fd_set *w; fd_set *e; struct timeval *t; {
  nox_fprintf(nox_stderr, "select() called\n");
  nox_abort();
}

int send(s, m, l, f) int s; char *m; int l; int f; {
  nox_fprintf(nox_stderr, "send() called\n");
  nox_abort();
}

int sendto(s, m, l, f, t, tl) int s; char *m; int l; int f; struct sockaddr *t; int tl; {
  nox_fprintf(nox_stderr, "sendto() called\n");
  nox_abort();
}

int sendmsg(s, m, l) int s; struct msghdr m[]; int l; {
  nox_fprintf(nox_stderr, "sendmsg() called\n");
  nox_abort();
}

int setgroups(n, g) int n; int *g; {
  nox_fprintf(nox_stderr, "setgroups() called\n");
  nox_abort();
}

int sethostid(h) long h; {
  nox_fprintf(nox_stderr, "sethostid() called\n");
  nox_abort();
}

int sethostname(n, l) char *n; int l; {
  nox_fprintf(nox_stderr, "sethostname() called\n");
  nox_abort();
}

int setitimer(w, v, ov) int w; struct itimerval *v; struct itimerval *ov; {
  nox_fprintf(nox_stderr, "setitimer() called\n");
  nox_abort();
}

int setpgrp(g, pg) int g; int pg; {
  nox_fprintf(nox_stderr, "setpgrp() called\n");
  nox_abort();
}

int setpriority(w, who, pri) int w; int who; int pri; {
  nox_fprintf(nox_stderr, "setpriority() called\n");
  nox_abort();
}

int setquota(s, f) char *s; char *f; {
  nox_fprintf(nox_stderr, "setquota() called\n");
  nox_abort();
}

int setregid(r, e) int r; int e; {
  nox_fprintf(nox_stderr, "setregid() called\n");
  nox_abort();
}

int setreuid(r, e) int r; int e; {
  nox_fprintf(nox_stderr, "setreuid() called\n");
  nox_abort();
}

int setrlimit(res, rip) int res; struct rlimit *rip; {
  nox_fprintf(nox_stderr, "setrlimit() called\n");
  nox_abort();
}

int setsockopt(s, level, opt, buf, len) int s; int level; int opt; char *buf; int len; {
  nox_fprintf(nox_stderr, "setsockopt() called\n");
  nox_abort();
}

int settimeofday(t, z) struct timeval *t; struct timezone *z; {
  nox_fprintf(nox_stderr, "settimeofday() called\n");
  nox_abort();
}

int shutdown(s, h) int s; int h; {
  nox_fprintf(nox_stderr, "shutdown() called\n");
  nox_abort();
}

int (*signal(c, f))() int c; int (*f)(); {
  nox_fprintf(nox_stderr, "signal() called\n");
  nox_abort();
}

int sigvec(c, f, m) int c; struct sigvec *f; struct sigvec *m; {
  nox_fprintf(nox_stderr, "sigvec() called\n");
  nox_abort();
}

int sigblock(m) int m; {
  nox_fprintf(nox_stderr, "sigblock() called\n");
  nox_abort();
}

int sigsetmask(m) int m; {
  nox_fprintf(nox_stderr, "sigsetmask() called\n");
  nox_abort();
}

void sigpause(m) int m; {
  nox_fprintf(nox_stderr, "sigpause() called\n");
  nox_abort();
}

int sigreturn(scp) struct sigcontext *scp; {
  nox_fprintf(nox_stderr, "sigreturn() called\n");
  nox_abort();
}

int sigstack(ss, oss) struct sigstack *ss; struct sigstack *oss; {
  nox_fprintf(nox_stderr, "sigstack() called\n");
  nox_abort();
}

int socket(a, t, p) int a; int t; int p; {
  nox_fprintf(nox_stderr, "socket() called\n");
  nox_abort();
}

int socketpair(d, t, p, s) int d; int t; int p; int s[2]; {
  nox_fprintf(nox_stderr, "socketpair() called\n");
  nox_abort();
}

int stat(s, b) char *s; struct stat *b; {
  nox_fprintf(nox_stderr, "stat() called\n");
  nox_abort();
}

#ifdef notdef
char *stk(a) char *a; {
  nox_fprintf(nox_stderr, "stk() called\n");
  nox_abort();
}

char *sstk(a) int a; {
  nox_fprintf(nox_stderr, "sstk() called\n");
  nox_abort();
}
#endif

int swapon(s) char *s; {
  nox_fprintf(nox_stderr, "swapon() called\n");
  nox_abort();
}

int symlink(t, f) char *t; char *f; {
  nox_fprintf(nox_stderr, "symlink() called\n");
  nox_abort();
}

void sync() {
  nox_fprintf(nox_stderr, "sync() called\n");
  nox_abort();
}

int truncate(p, l) char *p; off_t l; {
  nox_fprintf(nox_stderr, "truncate() called\n");
  nox_abort();
}

int umask(n) int n; {
  nox_fprintf(nox_stderr, "umask() called\n");
  nox_abort();
}

int umount(s) char *s; {
  nox_fprintf(nox_stderr, "umount() called\n");
  nox_abort();
}

int unlink(s) char *s; {
  nox_fprintf(nox_stderr, "unlink() called\n");
  nox_abort();
}

int utimes(f, t) char *f; struct timeval t[2]; {
  nox_fprintf(nox_stderr, "utimes() called\n");
  nox_abort();
}

int vfork() {
  nox_fprintf(nox_stderr, "vfork() called\n");
  nox_abort();
}

void vhangup() {
  nox_fprintf(nox_stderr, "vhangup() called\n");
  nox_abort();
}

int wait(s) union wait *s; {
  nox_fprintf(nox_stderr, "wait() called\n");
  nox_abort();
}

int wait3(s, o, r) union wait *s; int o; struct rusage *r; {
  nox_fprintf(nox_stderr, "wait3() called\n");
  nox_abort();
}

int write(f, b, l) int f; char *b; int l; {
  nox_fprintf(nox_stderr, "write() called\n");
  nox_abort();
}

int writev(f, v, l) int f; struct iovec *v; int l; {
  nox_fprintf(nox_stderr, "writev() called\n");
  nox_abort();
}
