#include "x_.h"

/*	@(#)if_ddnreg.h	7.1 (Berkeley) 6/5/86 */


/************************************************************************\

     ________________________________________________________
    /                                                        \
   |          AAA          CCCCCCCCCCCCCC    CCCCCCCCCCCCCC   |
   |         AAAAA        CCCCCCCCCCCCCCCC  CCCCCCCCCCCCCCCC  |
   |        AAAAAAA       CCCCCCCCCCCCCCCCC CCCCCCCCCCCCCCCCC |
   |       AAAA AAAA      CCCC              CCCC              |
   |      AAAA   AAAA     CCCC              CCCC              |
   |     AAAA     AAAA    CCCC              CCCC              |
   |    AAAA       AAAA   CCCC              CCCC              |
   |   AAAA  AAAAAAAAAAA  CCCCCCCCCCCCCCCCC CCCCCCCCCCCCCCCCC |
   |  AAAA    AAAAAAAAAAA CCCCCCCCCCCCCCCC  CCCCCCCCCCCCCCCC  |
   | AAAA      AAAAAAAAA   CCCCCCCCCCCCCC    CCCCCCCCCCCCCC   |
    \________________________________________________________/

	Copyright (c) 1985 by Advanced Computer Communications
	720 Santa Barbara Street, Santa Barbara, California  93101
	(805) 963-9431

	This software may be duplicated and used on systems
	which are licensed to run U.C. Berkeley versions of
	the UNIX operating system.  Any duplication of any
	part of this software must include a copy of ACC's
	copyright notice.


File:
		if_ddnreg.h

Author:
		Art Berggreen

Project:
		4.2 DDN X.25 network driver

Function:
		This file contains definitions of the hardware
		interface of the ACP625 (IF-11/X25).

Components:

Revision History:
		16-May-1985:	V1.0 - First release.
				Art Berggreen.

\************************************************************************/


/*	if_ddnvar.h	 V1.0	5/16/85	*/

/*
 * ACC IF-11/DDN-X25 interface
 */

struct x_ddnregs {			/* device registers */
	x_u_short	x_csr;			/* control and status register */
	x_u_char	x_iochn;			/* logical channel */
	x_u_char	x_ioadx;			/* address extension (A16,A17) */
	x_u_short	x_ioadl;			/* buffer address (A0-A15) */
	x_u_short	x_iocnt;			/* byte count */
	x_u_char	x_iofcn;			/* UMC funciton code */
	x_u_char	x_iosbf;			/* UMC subfunction code */
	x_u_char	x_ioini;			/* comm regs valid flag */
	x_u_char	x_staack;			/* interrupt acknowledge flag */
	x_u_char	x_ionmi;			/* NMI routine active flag */
	x_u_char	x_xfrgnt;			/* UMR transfer grant flag */
	x_u_char	x_stachn;			/* interrupt channel number */
	x_u_char	x_statyp;			/* interrupt type code */
	x_u_char	x_stacc;			/* completion function code */
	x_u_char	x_stacs;			/* completion subfunction code */
	x_u_short	x_stacnt;			/* completion byte count */
};

#define	x_iovect	x_iochn

/* defines for CSR */

#define x_DDN_UER		0100000		/* UMC error condition */
#define x_DDN_NXM		0040000		/* non-existent memory error */
#define x_DDN_PER		0020000		/* UNIBUS parity error */
#define x_DDN_ZRUN	0010000		/* Z80 running */
#define x_DDN_ZGO		0004000		/* Z80 not in wait state */
#define x_DDN_MBLK	0000200		/* memory swap state (0=main, 1=srv) */
#define	x_DDN_SRV		0000100		/* select UMC service memory */
#define x_DDN_MAIN	0000040		/* select UMC main memory */
#define x_DDN_DMA		0000020		/* DMA enable */
#define x_DDN_WRT		0000010		/* DMA write enable */
#define x_DDN_IEN		0000004		/* interrupt enable */
#define x_DDN_RST		0000002		/* reset */
#define	x_DDN_NMI		0000001		/* cause NMI */

#define x_DDN_BITS \
"\10\20UER\17NXM\16PER\15ZRUN\14ZGO\10MBLK\7SRV\6MAIN\5DMA\4WRT\3IEN\2RST\1NMI"

/* start i/o function code definitions */

#define x_DDNWRT		0	/* write to if-11 */
#define x_DDNRDB		1	/* read from if-11 */
#define x_DDNSTR		2	/* stream flag */
#define x_DDNEOS		(4|x_DDNSTR)  /* end of stream flag */
#define x_DDNABT		8	/* abort flag */
#define x_DDNUMR		16	/* UMR protocol flag */

/* interrupt type definitions */

#define x_DDNSACK		0	/* start i/o ack */
#define x_DDNDONE		1	/* i/o completion */
#define x_DDNXREQ		2	/* UMR protocol transfer request */

/* i/o completion codes */

#define x_DDNIOCOK	0001	/* successful completion */
#define x_DDNIOCOKP 	0002	/* successful completion, more data pending */
#define x_DDNIOCABT 	0361	/* i/o aborted */
#define x_DDNIOCERR 	0321	/* program error */
#define x_DDNIOCOVR 	0363	/* overrun error */
#define x_DDNIOCUBE 	0374	/* non-existant memory or unibus error */

/* UMR protocol transfer grant code definitions */

#define x_DDNXEVN		1	/* start with even address */
#define x_DDNXODD		2	/* start with odd address */
#define x_DDNNUMR		4	/* non-UMR transfer */
#define x_DDNXABT		8	/* abort transfer */
