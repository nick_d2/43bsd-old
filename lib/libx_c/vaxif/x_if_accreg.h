#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)if_accreg.h	7.1 (Berkeley) 6/5/86
 */

/*
 * ACC LH/DH-11 interface
 */

struct x_accdma {
	x_short	x_csr;	/* control and status */
	x_short	x_db;	/* data buffer */
	x_u_short	x_ba;	/* buss address */
	x_short	x_wc;	/* word count */
};

struct x_accdevice {
	struct	x_accdma x_input;
	struct	x_accdma x_output;
};

#define	x_icsr	x_input.x_csr
#define	x_iba	x_input.x_ba
#define	x_iwc	x_input.x_wc
#define	x_ocsr	x_output.x_csr
#define	x_oba	x_output.x_ba
#define	x_owc	x_output.x_wc

/*
 * Bits Common to both input and out CSR's
 */
#define	x_ACC_ERR		0x8000		/* error present */
#define	x_ACC_NXM		0x4000		/* non-existant memory */
#define	x_ACC_RDY		0x0080		/* ready */
#define	x_ACC_IE		0x0040		/* interrupt enable */
#define	x_ACC_RESET	0x0002		/* reset interface */
#define	x_ACC_GO		0x0001		/* start operation */

/*
 * Input Control Status Register
 */
#define x_IN_EOM		0x2000		/* end-of-message recieved */
#define x_IN_HRDY		0x0800		/* host ready */
#define x_IN_IMPBSY	0x0400		/* IMP not ready */
#define x_IN_RMR		0x0200		/* receive master ready error */
#define x_IN_IBF		0x0100		/* input data buffer full */
#define x_IN_WEN		0x0008		/* write enable */
#define x_IN_MRDY		0x0004		/* master ready */

#define x_ACC_INBITS \
"\20\20ERR\17NXM\16EOM\14HRDY\13IMPBSY\12RMR\11IBF\10RDY\7IE\
\4x_WEN\3x_MRDY\2x_RESET\1x_GO"

/*
 * Output Control Status Register
 */
#define x_OUT_TMR		0x0200		/* transmit master ready error */
#define x_OUT_BBACK	0x0008		/* bus back */
#define x_OUT_ENLB 	0x0004		/* enable last bit */

#define x_ACC_OUTBITS \
"\20\20ERR\17NXM\12TMR\10RDY\7IE\4BBACK\3ENLB\2RESET\1GO"
