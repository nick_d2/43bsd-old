#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)if_enreg.h	7.1 (Berkeley) 6/5/86
 */

/*
 * Xerox experimental ethernet registers.
 *
 * N.B.: status register and device address are read/write,
 * device address is read-only, rest are WRITE ONLY!
 */
struct x_endevice {
	x_short	x_en_owc;		/* output word count (10 bits) */
	x_short	x_en_oba;		/* output buffer address */
	x_short	x_en_ostat;	/* output control and status */
	x_short	x_en_odelay;	/* output start delay, 25usec units  */
	x_short	x_en_iwc;		/* input word count */
	x_short	x_en_iba;		/* input buffer address */
	x_short	x_en_istat;	/* input csr */
	x_short	x_en_addr;	/* ~device address (low 8 bits) */
};

/*
 * Control and status bits.
 */
#define x_EN_IERROR	0x8000		/* CRC error, buf ovflo or overrun */
#define	x_EN_OERROR	0x8000		/* collision or output underrun */
#define x_EN_OPDONE	0x0080		/* previous operation completed */
#define x_EN_IEN		0x0040		/* enable interrupt when DONE */
#define	x_EN_PROMISCUOUS	0x0002		/* promiscuous, input any packet */
#define x_EN_GO		0x0001		/* start op bit */

#define	x_EN_BITS	"\10\20ERR\10OPDONE\7IEN\2PROM\1GO"

#define	x_spl_enet()	x_spl5()
