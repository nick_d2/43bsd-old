#include "x_.h"

/*	@(#)if_hdhreg.h	7.1 (Berkeley) 6/5/86 */


/* $Header$ */

/*
 * ACC IF-11/HDH interface
 */

struct x_hdhregs {			/* device registers */
	x_u_short	x_csr;			/* control and status register */
	x_u_char	x_iochn;			/* logical channel */
	x_u_char	x_ioadx;			/* address extension (A16,A17) */
	x_u_short	x_ioadl;			/* buffer address (A0-A15) */
	x_u_short	x_iocnt;			/* byte count */
	x_u_char	x_iofcn;			/* UMC funciton code */
	x_u_char	x_iosbf;			/* UMC subfunction code */
	x_u_char	x_ioini;			/* comm regs valid flag */
	x_u_char	x_staack;			/* interrupt acknowledge flag */
	x_u_char	x_ionmi;			/* NMI routine active flag */
	x_u_char	x_ioxfrg;			/* UMR transfer grant flag */
	x_u_char	x_stachn;			/* interrupt channel number */
	x_u_char	x_statyp;			/* interrupt type code */
	x_u_char	x_stacc;			/* completion function code */
	x_u_char	x_stacs;			/* completion subfunction code */
	x_u_short	x_stacnt;			/* completion byte count */
};

/* defines for CSR */

#define x_HDH_UER		0100000		/* UMC error condition */
#define x_HDH_NXM		0040000		/* non-existent memory error */
#define x_HDH_PER		0020000		/* UNIBUS parity error */
#define x_HDH_ZRUN	0010000		/* Z80 running */
#define x_HDH_ZGO		0004000		/* Z80 not in wait state */
#define x_HDH_MBLK	0000200		/* memory swap state (0=main, 1=srv) */
#define	x_HDH_SRV		0000100		/* select UMC service memory */
#define x_HDH_MAIN	0000040		/* select UMC main memory */
#define x_HDH_DMA		0000020		/* DMA enable */
#define x_HDH_WRT		0000010		/* DMA write enable */
#define x_HDH_IEN		0000004		/* interrupt enable */
#define x_HDH_RST		0000002		/* reset */
#define	x_HDH_NMI		0000001		/* cause NMI */

#define x_HDH_BITS \
"\10\20UER\17NXM\16PER\15ZRUN\14ZGO\10MBLK\7SRV\6MAIN\5DMA\4WRT\3IEN\2RST\1NMI"

/* start i/o function code definitions */

#define x_HDHWRT		0	/* write to if-11 */
#define x_HDHRDB		1	/* read from if-11 */
#define x_HDHSTR		2	/* stream flag */
#define x_HDHEOS		6	/* end of stream flag */
#define x_HDHABT		8	/* abort flag */
#define x_HDHUMR		16	/* UMR protocol flag */

/* interrupt type definitions */

#define x_HDHSACK		0	/* start i/o ack */
#define x_HDHDONE		1	/* i/o completion */
#define x_HDHXREQ		2	/* UMR protocol transfer request */

/* i/o completion codes */

#define x_HDHIOCOK	0001	/* successful completion */
#define x_HDHIOCOKP 	0002	/* successful completion, more data pending */
#define x_HDHIOCABT 	0361	/* i/o aborted */
#define x_HDHIOCERR 	0321	/* program error */
#define x_HDHIOCOVR 	0363	/* overrun error */
#define x_HDHIOCUBE 	0374	/* non-existant memory or unibus error */

/* UMR protocol transfer grant code definitions */

#define x_HDHXEVN		1	/* start with even address */
#define x_HDHXODD		2	/* start with odd address */
#define x_HDHNUMR		4	/* non-UMR transfer */
#define x_HDHXABT		8	/* abort transfer */

/* HDH supervisor request code definitions */
#define x_HDHINIT		0x42	/* SYSINIT opcode */

#define x_HDHSUP		0xf0	/* supervisor HDH status/line control prefix */
#define x_HDHIMP		0x400	/* IMP line up modifier */
#define x_HDHREFL		0x800	/* reflect mode modifier */
#define x_HDHINLB		0x1000	/* internal loopback modifier */
#define x_HDHEXLP		0x2000	/* external loopback modifier */
#define x_HDHRQST		(x_HDHSUP+0x0000)	/* line status request */
#define x_HDHRQUP		(x_HDHSUP+0x0100)	/* line up request */
#define x_HDHRQDN		(x_HDHSUP+0x0200)	/* line down request */

/* HDH supervisor reply code definitions */

#define x_HDHIACK		(x_HDHSUP+0x4200)	/* line init ack */
#define x_HDHLNUP		(x_HDHSUP+0x0100)	/* line up reply */
#define x_HDHLNDN		(x_HDHSUP+0x0200)	/* line down reply */
#define x_HDHLNACK	(x_HDHSUP+0x0300)	/* ack line up request (but line is down now) */
#define x_HDHTIMO		(x_HDHSUP+0x0400)	/* line timeout */
#define x_HDHLOOP		(x_HDHSUP+0x0500)	/* loopback message */
#define x_HDHDTERR	(x_HDHSUP+0x0600)	/* host data error detected */
#define x_HDHSQRCV	(x_HDHSUP+0x0700)	/* HDLC sequence error detected by IMP */
#define x_HDHSQERR	(x_HDHSUP+0x0800)	/* HDLC sequence error detected by if-11 */
