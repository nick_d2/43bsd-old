#include "x_.h"

/*	@(#)if_qereg.h	7.1 (Berkeley) 6/5/86 */

/* @(#)if_qereg.h	1.2 (ULTRIX) 1/3/85 */
 
/****************************************************************
 *								*
 *        Licensed from Digital Equipment Corporation 		*
 *                       Copyright (c) 				*
 *               Digital Equipment Corporation			*
 *                   Maynard, Massachusetts 			*
 *                         1985, 1986 				*
 *                    All rights reserved. 			*
 *								*
 *        The Information in this software is subject to change *
 *   without notice and should not be construed as a commitment *
 *   by  Digital  Equipment  Corporation.   Digital   makes  no *
 *   representations about the suitability of this software for *
 *   any purpose.  It is supplied "As Is" without expressed  or *
 *   implied  warranty. 					*
 *								*
 *        If the Regents of the University of California or its *
 *   licensees modify the software in a manner creating  	*
 *   diriviative copyright rights, appropriate copyright  	*
 *   legends may be placed on  the drivative work in addition   *
 *   to that set forth above. 					*
 *								*
 ****************************************************************/
/* ---------------------------------------------------------------------
 * Modification History 
 *
 *  13 Feb. 84 -- rjl
 *
 *	Initial version of driver. derived from IL driver.
 * 
 * ---------------------------------------------------------------------
 */
 
/*
 * Digital Q-BUS to NI Adapter 
 */
struct x_qedevice {
	x_u_short	x_qe_sta_addr[2]; 	/* Station address (actually 6 	*/
	x_u_short	x_qe_rcvlist_lo; 		/* Receive list lo address 	*/
	x_u_short	x_qe_rcvlist_hi; 		/* Receive list hi address 	*/
	x_u_short	x_qe_xmtlist_lo;		/* Transmit list lo address 	*/
	x_u_short	x_qe_xmtlist_hi;		/* Transmit list hi address 	*/
	x_u_short	x_qe_vector;		/* Interrupt vector 		*/
	x_u_short	x_qe_csr;			/* Command and Status Register 	*/
};
 
/*
 * Command and status bits (csr)
 */
#define x_QE_RCV_ENABLE	0x0001		/* Receiver enable		*/
#define x_QE_RESET	0x0002		/* Software reset		*/
#define x_QE_NEX_MEM_INT	0x0004		/* Non existant mem interrupt	*/
#define x_QE_LOAD_ROM	0x0008		/* Load boot/diag from rom	*/
#define x_QE_XL_INVALID	0x0010		/* Transmit list invalid	*/
#define x_QE_RL_INVALID	0x0020		/* Receive list invalid		*/
#define x_QE_INT_ENABLE	0x0040		/* Interrupt enable		*/
#define x_QE_XMIT_INT	0x0080		/* Transmit interrupt		*/
#define x_QE_ILOOP 	0x0100		/* Internal loopback		*/
#define x_QE_ELOOP	0x0200		/* External loopback		*/
#define x_QE_STIM_ENABLE	0x0400		/* Sanity timer enable		*/
#define x_QE_POWERUP	0x1000		/* Tranceiver power on		*/
#define x_QE_CARRIER	0x2000		/* Carrier detect		*/
#define x_QE_RCV_INT	0x8000		/* Receiver interrupt		*/
 
/*
 * Transmit and receive ring discriptor ---------------------------
 *
 * The QNA uses the flag, status1 and the valid bit as a handshake/semiphore
 * mechinism. 
 * 
 * The flag word is written on ( bits 15,15 set to 1 ) when it reads the
 * descriptor. If the valid bit is set it considers the address to be valid.
 * When it uses the buffer pointed to by the valid address it sets status word
 * one.
 */
struct x_qe_ring	{
	x_u_short x_qe_flag;		/* Buffer utilization flags	*/
	x_u_short x_qe_addr_hi:6,		/* Hi order bits of buffer addr	*/
	      x_qe_odd_begin:1,		/* Odd byte begin and end (xmit)*/
	      x_qe_odd_end:1,
	      x_qe_fill1:4,
	      x_qe_setup:1,		/* Setup packet			*/
	      x_qe_eomsg:1,		/* End of message flag		*/
	      x_qe_chain:1,		/* Chain address instead of buf */
	      x_qe_valid:1;		/* Address field is valid	*/
	x_u_short x_qe_addr_lo;		/* Low order bits of address	*/
	x_short x_qe_buf_len;		/* Negative buffer length	*/
	x_u_short x_qe_status1;		/* Status word one		*/
	x_u_short x_qe_status2;		/* Status word two		*/
};
 
/*
 * Status word definations (receive)
 *	word1
 */
#define x_QE_OVF			0x0001	/* Receiver overflow		*/
#define x_QE_CRCERR		0x0002	/* CRC error			*/
#define x_QE_FRAME		0x0004	/* Framing alignment error	*/
#define x_QE_SHORT		0x0008	/* Packet size < 10 bytes	*/
#define x_QE_RBL_HI		0x0700	/* Hi bits of receive len	*/
#define x_QE_RUNT			0x0800	/* Runt packet			*/
#define x_QE_DISCARD		0x1000	/* Discard the packet		*/
#define x_QE_ESETUP		0x2000	/* Looped back setup or eloop	*/
#define x_QE_ERROR		0x4000	/* Receiver error		*/
#define x_QE_LASTNOT		0x8000	/* Not the last in the packet	*/
/*	word2								*/
#define x_QE_RBL_LO		0x00ff	/* Low bits of receive len	*/
 
/*
 * Status word definations (transmit)
 *	word1
 */
#define x_QE_CCNT			0x00f0	/* Collision count this packet	*/
#define x_QE_FAIL			0x0100	/* Heart beat check failure	*/
#define x_QE_ABORT		0x0200	/* Transmission abort		*/
#define x_QE_STE16		0x0400	/* Sanity timer default on	*/
#define x_QE_NOCAR		0x0800	/* No carrier			*/
#define x_QE_LOSS			0x1000	/* Loss of carrier while xmit	*/
/*	word2								*/
#define x_QE_TDR			0x3fff	/* Time domain reflectometry	*/
 
/*
 * General constant definations
 */
#define x_QEALLOC 		0	/* Allocate an mbuf		*/
#define x_QENOALLOC		1	/* No mbuf allocation		*/
#define x_QEDEALLOC		2	/* Release an mbuf chain	*/
 
#define x_QE_NOTYET		0x8000	/* Descriptor not in use yet	*/
#define x_QE_INUSE		0x4000	/* Descriptor being used by QNA	*/
#define x_QE_MASK			0xc000	/* Lastnot/error/used mask	*/
