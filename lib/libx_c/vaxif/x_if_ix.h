#include "x_.h"

/*
 * Copyright (c) 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)if_ix.h	7.1 (Berkeley) 6/5/86
 */

union x_ix_stats {
	struct {				/* General statistics below */
		x_u_char	x_macg_physaddr[6];
		x_u_short x_macg_pad;
		x_u_long	x_dlag_rcvmac;	/* packets received by DLA from MAC */
		x_u_long	x_dlag_rcvpass;	/* packets passed to users by DLA */
		x_u_long	x_dlag_txreq;	/* packets sent by users to DLA */
		x_u_long	x_dlag_txsnt;	/* packets sent by DLA to MAC */
		x_u_short	x_dlag_chaopn;	/* channels open */
		x_u_short	x_dlag_maxopn;	/* max channels opened concurrently */
		x_u_long	x_macg_frmtos;	/* packets discarded by MAC */
		x_u_long	x_macg_frmpas;	/* packets sent to DLA by MAC */
		x_u_long	x_macg_x2x;	/* packets put on wire by MAC */
		x_u_long	x_macg_x2r;	/* packets looped by MAC */
		x_u_long	x_macg_xrty;	/* transmitter retries */
		x_u_short	x_macg_noap;	/* open MAC paths */
		x_u_short	x_macg_nprom;	/* open promiscuous paths */
		x_u_short	x_macg_conopn;	/* max concurrent MAC paths */
		x_u_short	x_sysg_crce;	/* CRC errors */
		x_u_short	x_sysg_alne;	/* alignment errors */
		x_u_short	x_sysg_rsce;	/* resource errors */
		x_u_short	x_sysg_ovre;	/* overrun errors */
	} x_ixg;
	struct {			/* Channel statistics below */
		x_u_long	x_dabc_rcvacc;	/* packets received */
		x_u_long	x_dabc_rcvtoss;	/* packets discarded, queue full */
		x_u_long	x_dabc_rcvpass;	/* packets passed to user */
		x_u_long	x_dabc_txreq;	/* packets sent by  user */
		x_u_long	x_dabc_txsent;	/* packets sent to MAC */
		x_u_long	x_macc_rcvcnt;	/* packets received by MAC */
		x_u_long	x_macc_txtcnt;	/* packets sent by MAC to wire */
		x_u_long	x_macc_lowmem;	/* packets discarded, no mem  */
	} x_ixc;
};
#define x_IXC_MAP(x_a)	(((x_a) << 6) | 0100077)

#define x_IXC_OPEN	x_IXC_MAP(1)		/* Open Channel */
#define x_IXC_CLOSE	x_IXC_MAP(2)		/* Close Channel */
#define x_IXC_MCAST	x_IXC_MAP(3)		/* Set Multicast Addresses */
#define x_IXC_RECV	x_IXC_MAP(4)		/* Receive Frame */
#define x_IXC_RECVF	x_IXC_MAP(5)		/* Receive Fragment */
#define x_IXC_XMIT	x_IXC_MAP(6)		/* Send Frame */
#define x_IXC_GSTAT	x_IXC_MAP(7)		/* Get General Statistics */
#define x_IXC_CSTAT	x_IXC_MAP(8)		/* Get Channel Statistics */
#define x_IXC_GSCLR	x_IXC_MAP(9)		/* Clear General Statistics */
#define x_IXC_CSCLR	x_IXC_MAP(10)		/* Clear Channel Statistics */
#define x_IXC_RESET	x_IXC_MAP(11)		/* Reset DLA module */
#define x_IXC_LDPA	x_IXC_MAP(12)		/* Load Physical Address */
