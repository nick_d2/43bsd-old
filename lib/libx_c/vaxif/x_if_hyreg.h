#include "x_.h"

/*
 *	@(#)if_hyreg.h	7.1 (Berkeley) 6/5/86
 *
 * $Header: if_hyreg.h,v 10.0 84/06/30 19:51:34 steveg Stable $
 * $Locker:  $
 *
 * Modifications from Berkeley 4.2 BSD
 * Copyright (c) 1983, Tektronix Inc.
 * All Rights Reserved
 *
 */


/*
 * Network Systems Corporation Hyperchannel interface
 *
 * supports A410 adapter interfaced via a DEC DR-11B, NSC PI-13 or PI-14
 *	(PI-14 is a PI-13 with different line drivers, software is
 *	identical to a PI-13)
 *
 * Written by Steve Glaser, Tektronix Inc., July 1982
 *
 * NOTE:
 *
 * DR11B code has not been fully checked out with 4.1a.
 * The first adapters at Tek came with DR11Bs, and the code once worked,
 * but those have been upgraded to PI-13s.
 */

/*
 * The HYPERchannel driver sends and receives messages formatted:
 *
 *	+---------------------------------------+	---
 *	|					|	/|\
 *	|  HYPERchannel adapter header (hy_hdr)	|	 |
 *	|					|	 |
 *	+---------------------------------------+	 |
 *	|					|	 |
 *	|     Internet Protocol header (ip)	|    message proper
 *	|					|    (64 bytes max)
 *	+---------------------------------------+	 |
 *	|					|	 |
 *	|	TCP header + user data		|	 |
 *	|	(if it all fits here)		|	 |
 *	|					|	\|/
 *	+---------------------------------------+	---
 *
 *	+---------------------------------------+	---
 *	|					|	/|\
 *	|					|	 |
 *	|	TCP header + user data		|  associated data
 *	|					|	 |
 *	|					|	\|/
 *	+---------------------------------------+	---
 *
 * If all of the datagram will fit in the message proper (including
 * the TCP header and user data) the entire datagram is passed in
 * the message proper and the associated data feature of the HYPERchannel
 * is not used.
 *
 * The mapping from internet addresses to HYPERchannel addresses is:
 *
 *	 0       7 8      15 16                   31
 *	+---------+---------+-----------------------+
 *	| network | special | HYPERchannel address  |
 *	+---------+---------+-----------------------+
 *
 *	|<------------ internet address ----------->|
 *
 * The hyperchannel address is decoded as follows:
 *
 *       0                 7 8             13 14  15
 *	+-------------------+----------------+------+
 *	|   adapter number  |      zero      | port |
 *	+-------------------+----------------+------+
 *
 * The low 2 bits are port number (interpreted by hyperchannel hardware).
 *
 * The encoding of special bits is:
 *
 *	00	normal packet
 *
 *	01	loop this packet back to the sender at the
 *		specified adapter (ip header source/destination addresses
 *		swapped before sending, command bits added to tell the
 *		remote HYPERchannel adapter debug & performance studies]
 *		this code acts like 02 (below) if the ip destination (before
 *		any swapping) and the destination address don't match (e.g.
 *		this packet is being routed through a gateway)
 *
 *	02	loop this packet back to the sender at the
 *		specified adapter, but go through the specified adapter's
 *		IP.  This is for testing IP's store and forward mechanism.
 *
 *	other	undefined, currently treated as normal packet
 *
 */
#define x_MPSIZE		64	/* "Message Proper" size */
#define x_MAXRETRY	4

/*
 * Device registers
 */
struct	x_hydevice {
	x_short	x_hyd_wcr;	/* word count (negated) */
	x_u_short	x_hyd_bar;	/* bus address bits 15-0 */
	x_u_short	x_hyd_csr;	/* control and status */
	x_u_short	x_hyd_dbuf;	/* data buffer */
};

/*
 * CSR bit layout
 */
#define	x_S_ERROR	   0100000	/* error */
#define	x_S_NEX	   0040000	/* non-existent memory error */
#define	x_S_ATTN	   0020000	/* attn (always zero) */
#ifdef x_PI13
#define x_S_STKINTR  0010000	/* stacked interrupt */
#else
#define	x_S_MAINT	   0010000	/* maintenance (not used) */
#endif
#define	x_S_A	   0004000	/* device status A (recieve data available) */
#define	x_S_B	   0002000	/* device status B (normal termination) */
#define	x_S_C	   0001000	/* device status C (abnormal termination) */
#ifdef x_PI13
#define x_S_POWEROFF 0000400	/* power off indicator */
#else
#define	x_S_CYCLE	   0000400	/* cycle (not used) */
#endif
#define	x_S_READY	   0000200	/* ready */
#define	x_S_IE	   0000100	/* interrupt enable */
#define	x_S_XBA	   0000060	/* bus address bit bits 17 and 16 */
#define x_S_CLRINT   0000014	/* clear stacked interrupt */
#define	x_S_IATTN    0000010	/* interrupt on attention only */
#define x_S_WC       0000004	/* interrupt on word count == 0 only */
#define x_S_IATTNWC  0000000	/* interrupt on word count == 0 and attention */
#define	x_S_BURST	   0000002	/* burst mode DMA (not used) */
#define	x_S_GO	   0000001	/* go */

#define x_XBASHIFT	12

#define x_HY_CSR_BITS "\20\
\20Ex_RROR\17x_NEX\16x_ATTN\15x_STKINTR\14x_RECV_DATA\13x_NORMAL\12x_ABNORMAL\11x_POWER\
\10x_READY\07x_IENABLE\06x_XBA17\05x_XBA16\04x_IATTN\03x_IWC\02x_BURST\01x_GO"

/*
 * PI13 status conditions
 */
#define	x_HYS_RECVDATA(x_x)	(((x_x)->x_hyd_csr & x_S_A) != 0)	/* get adapter data */
#define	x_HYS_NORMAL(x_x)	(((x_x)->x_hyd_csr & x_S_B) != 0)	/* done normally */
#define	x_HYS_ABNORMAL(x_x)	(((x_x)->x_hyd_csr & x_S_C) != 0)	/* done abnormally */
#define	x_HYS_ERROR(x_x)	(((x_x)->x_hyd_csr & x_S_ERROR) != 0)	/* error condition */
#define	x_HYS_DONE(x_x)	(((x_x)->x_hyd_csr & (x_S_ERROR|x_S_B|x_S_C)) != 0)

/*
 * Function Codes for the Hyperchannel Adapter
 * The codes are offset so they can be "or"ed into
 * the reg data buffer
 */
#define	x_HYF_XMITMSG	0x04	/* transmit message */
#define	x_HYF_XMITDATA	0x08	/* transmit associated data */
#define	x_HYF_XMITLSTDATA	0x0C	/* transmit last associated data */
#define	x_HYF_XMITLOCMSG	0x10	/* transmit local message */
#define	x_HYF_INPUTMSG	0x24	/* input message proper */
#define	x_HYF_INPUTDATA	0x28	/* input assiciated data */
#define	x_HYF_STATUS	0x40	/* request status */
#define	x_HYF_DUMPREGS	0x50	/* dump extention registers */
#define	x_HYF_MARKP0	0x60	/* mark down port 0 */
#define	x_HYF_MARKP1	0x64	/* mark down port 1 */
#define	x_HYF_MARKP2	0x68	/* mark down port 2 */
#define	x_HYF_MARKP3	0x6C	/* mark down port 3 */
#define	x_HYF_MP0RR	0x70	/* mark down port 0 and reroute messages */
#define	x_HYF_MP1RR	0x74	/* mark down port 1 and reroute messages */
#define	x_HYF_MP2RR	0x78	/* mark down port 2 and reroute messages */
#define	x_HYF_MP3RR	0x7C	/* mark down port 3 and reroute messages */
#define	x_HYF_RSTATS	0xA0	/* read statistics */
#define	x_HYF_RCSTATS	0xA4	/* read and clear statistics */
#define	x_HYF_SETTEST	0xC0	/* enable test operations *set test mode) */
#define	x_HYF_SADDR_LEN	0xC4	/* test mode: set address and length */
#define	x_HYF_WBUFF	0xC8	/* test mode: write buffer */
#define	x_HYF_RBUFF	0xCC	/* test mode: read buffer */
#define x_HYF_CLRADAPTER	0xE0	/* clear adapter */
#define	x_HYF_END_OP	0xE4	/* end operation */
#define	x_HYF_CLRWFMSG	0xE6	/* clear wait for mwssage */
#define	x_HYF_WAITFORMSG	0xE8	/* wait for message */
