#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)if_uba.h	7.1 (Berkeley) 6/5/86
 */

/*
 * Structure and routine definitions
 * for UNIBUS network interfaces.
 */

#define	x_IF_MAXNUBAMR	10
/*
 * Each interface has structures giving information
 * about UNIBUS resources held by the interface
 * for each send and receive buffer.
 *
 * We hold IF_NUBAMR map registers for datagram data, starting
 * at ifr_mr.  Map register ifr_mr[-1] maps the local network header
 * ending on the page boundary.  Bdp's are reserved for read and for
 * write, given by ifr_bdp.  The prototype of the map register for
 * read and for write is saved in ifr_proto.
 *
 * When write transfers are not full pages on page boundaries we just
 * copy the data into the pages mapped on the UNIBUS and start the
 * transfer.  If a write transfer is of a (1024 byte) page on a page
 * boundary, we swap in UNIBUS pte's to reference the pages, and then
 * remap the initial pages (from ifu_wmap) when the transfer completes.
 *
 * When read transfers give whole pages of data to be input, we
 * allocate page frames from a network page list and trade them
 * with the pages already containing the data, mapping the allocated
 * pages to replace the input pages for the next UNIBUS data input.
 */

/*
 * Information per interface.
 */
struct	x_ifubinfo {
	x_short	x_iff_uban;			/* uba number */
	x_short	x_iff_hlen;			/* local net header length */
	struct	x_uba_regs *x_iff_uba;		/* uba regs, in vm */
	x_short	x_iff_flags;			/* used during uballoc's */
};

/*
 * Information per buffer.
 */
struct x_ifrw {
	x_caddr_t	x_ifrw_addr;			/* virt addr of header */
	x_short	x_ifrw_bdp;			/* unibus bdp */
	x_short	x_ifrw_flags;			/* type, etc. */
#define	x_IFRW_W	0x01				/* is a transmit buffer */
	x_int	x_ifrw_info;			/* value from ubaalloc */
	x_int	x_ifrw_proto;			/* map register prototype */
	struct	x_pte *x_ifrw_mr;			/* base of map registers */
};

/*
 * Information per transmit buffer, including the above.
 */
struct x_ifxmt {
	struct	x_ifrw x_ifrw;
	x_caddr_t	x_ifw_base;			/* virt addr of buffer */
	struct	x_pte x_ifw_wmap[x_IF_MAXNUBAMR];	/* base pages for output */
	struct	x_mbuf *x_ifw_xtofree;		/* pages being dma'd out */
	x_short	x_ifw_xswapd;			/* mask of clusters swapped */
	x_short	x_ifw_nmr;			/* number of entries in wmap */
};
#define	x_ifw_addr	x_ifrw.x_ifrw_addr
#define	x_ifw_bdp		x_ifrw.x_ifrw_bdp
#define	x_ifw_flags	x_ifrw.x_ifrw_flags
#define	x_ifw_info	x_ifrw.x_ifrw_info
#define	x_ifw_proto	x_ifrw.x_ifrw_proto
#define	x_ifw_mr		x_ifrw.x_ifrw_mr

/*
 * Most interfaces have a single receive and a single transmit buffer,
 * and use struct ifuba to store all of the unibus information.
 */
struct x_ifuba {
	struct	x_ifubinfo x_ifu_info;
	struct	x_ifrw x_ifu_r;
	struct	x_ifxmt x_ifu_xmt;
};

#define	x_ifu_uban	x_ifu_info.x_iff_uban
#define	x_ifu_hlen	x_ifu_info.x_iff_hlen
#define	x_ifu_uba		x_ifu_info.x_iff_uba
#define	x_ifu_flags	x_ifu_info.x_iff_flags
#define	x_ifu_w		x_ifu_xmt.x_ifrw
#define	x_ifu_xtofree	x_ifu_xmt.x_ifw_xtofree

#ifdef 	x_KERNEL
#define	x_if_ubainit(x_ifuba, x_uban, x_hlen, x_nmr) \
		x_if_ubaminit(&(x_ifuba)->x_ifu_info, x_uban, x_hlen, x_nmr, \
			&(x_ifuba)->x_ifu_r, 1, &(x_ifuba)->x_ifu_xmt, 1)
#define	x_if_rubaget(x_ifu, x_totlen, x_off0, x_ifp) \
		x_if_ubaget(&(x_ifu)->x_ifu_info, &(x_ifu)->x_ifu_r, x_totlen, x_off0, x_ifp)
#define	x_if_wubaput(x_ifu, x_m) \
		x_if_ubaput(&(x_ifu)->x_ifu_info, &(x_ifu)->x_ifu_xmt, x_m)
struct	x_mbuf *x_if_ubaget();
#endif
