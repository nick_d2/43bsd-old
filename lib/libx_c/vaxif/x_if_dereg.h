#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)if_dereg.h	7.1 (Berkeley) 6/5/86
 */

/*
 * DEC DEUNA interface
 */
struct x_dedevice {
	union {
		x_short	x_p0_w;
		char	x_p0_b[2];
	} x_u_p0;
#define	x_pcsr0	x_u_p0.x_p0_w
#define	x_pclow		x_u_p0.x_p0_b[0]
#define	x_pchigh		x_u_p0.x_p0_b[1]
	x_short	x_pcsr1;
	x_short	x_pcsr2;
	x_short	x_pcsr3;
};

/*
 * PCSR 0 bit descriptions
 */
#define	x_PCSR0_SERI	0x8000		/* Status error interrupt */
#define	x_PCSR0_PCEI	0x4000		/* Port command error interrupt */
#define	x_PCSR0_RXI	0x2000		/* Receive done interrupt */
#define	x_PCSR0_TXI	0x1000		/* Transmit done interrupt */
#define	x_PCSR0_DNI	0x0800		/* Done interrupt */
#define	x_PCSR0_RCBI	0x0400		/* Receive buffer unavail intrpt */
#define	x_PCSR0_FATI	0x0100		/* Fatal error interrupt */
#define	x_PCSR0_INTR	0x0080		/* Interrupt summary */
#define	x_PCSR0_INTE	0x0040		/* Interrupt enable */
#define	x_PCSR0_RSET	0x0020		/* DEUNA reset */
#define	x_PCSR0_CMASK	0x000f		/* command mask */

#define	x_PCSR0_BITS	"\20\20SERI\17PCEI\16RXI\15TXI\14DNI\13RCBI\11FATI\10INTR\7INTE\6RSET"

/* bits 0-3 are for the PORT_COMMAND */
#define	x_CMD_NOOP	0x0
#define	x_CMD_GETPCBB	0x1		/* Get PCB Block */
#define	x_CMD_GETCMD	0x2		/* Execute command in PCB */
#define	x_CMD_STEST	0x3		/* Self test mode */
#define	x_CMD_START	0x4		/* Reset xmit and receive ring ptrs */
#define	x_CMD_BOOT	0x5		/* Boot DEUNA */
#define	x_CMD_PDMD	0x8		/* Polling demand */
#define	x_CMD_TMRO	0x9		/* Sanity timer on */
#define	x_CMD_TMRF	0xa		/* Sanity timer off */
#define	x_CMD_RSTT	0xb		/* Reset sanity timer */
#define	x_CMD_STOP	0xf		/* Suspend operation */

/*
 * PCSR 1 bit descriptions
 */
#define	x_PCSR1_XPWR	0x8000		/* Transceiver power BAD */
#define	x_PCSR1_ICAB	0x4000		/* Interconnect cabling BAD */
#define	x_PCSR1_STCODE	0x3f00		/* Self test error code */
#define	x_PCSR1_PCTO	0x0080		/* Port command timed out */
#define	x_PCSR1_ILLINT	0x0040		/* Illegal interrupt */
#define	x_PCSR1_TIMEOUT	0x0020		/* Timeout */
#define	x_PCSR1_POWER	0x0010		/* Power fail */
#define	x_PCSR1_RMTC	0x0008		/* Remote console reserved */
#define	x_PCSR1_STMASK	0x0007		/* State */

/* bit 0-3 are for STATE */
#define	x_STAT_RESET	0x0
#define	x_STAT_PRIMLD	0x1		/* Primary load */
#define	x_STAT_READY	0x2
#define	x_STAT_RUN	0x3
#define	x_STAT_UHALT	0x5		/* UNIBUS halted */
#define	x_STAT_NIHALT	0x6		/* NI halted */
#define	x_STAT_NIUHALT	0x7		/* NI and UNIBUS Halted */

#define	x_PCSR1_BITS	"\20\20XPWR\17ICAB\10PCTO\7ILLINT\6TIMEOUT\5POWER\4RMTC"

/*
 * Port Control Block Base
 */
struct x_de_pcbb {
	x_short	x_pcbb0;		/* function */
	x_short	x_pcbb2;		/* command specific */
	x_short	x_pcbb4;
	x_short	x_pcbb6;
};

/* PCBB function codes */
#define	x_FC_NOOP		0x00		/* NO-OP */
#define	x_FC_LSUADDR	0x01		/* Load and start microaddress */
#define	x_FC_RDDEFAULT	0x02		/* Read default physical address */
#define	x_FC_RDPHYAD	0x04		/* Read physical address */
#define	x_FC_WTPHYAD	0x05		/* Write physical address */
#define	x_FC_RDMULTI	0x06		/* Read multicast address list */
#define	x_FC_WTMULTI	0x07		/* Read multicast address list */
#define	x_FC_RDRING	0x08		/* Read ring format */
#define	x_FC_WTRING	0x09		/* Write ring format */
#define	x_FC_RDCNTS	0x0a		/* Read counters */
#define	x_FC_RCCNTS	0x0b		/* Read and clear counters */
#define	x_FC_RDMODE	0x0c		/* Read mode */
#define	x_FC_WTMODE	0x0d		/* Write mode */
#define	x_FC_RDSTATUS	0x0e		/* Read port status */
#define	x_FC_RCSTATUS	0x0f		/* Read and clear port status */
#define	x_FC_DUMPMEM	0x10		/* Dump internal memory */
#define	x_FC_LOADMEM	0x11		/* Load internal memory */
#define	x_FC_RDSYSID	0x12		/* Read system ID parameters */
#define	x_FC_WTSYSID	0x13		/* Write system ID parameters */
#define	x_FC_RDSERAD	0x14		/* Read load server address */
#define	x_FC_WTSERAD	0x15		/* Write load server address */

/*
 * Unibus Data Block Base (UDBB) for ring buffers
 */
struct x_de_udbbuf {
	x_short	x_b_tdrbl;	/* Transmit desc ring base low 16 bits */
	char	x_b_tdrbh;	/* Transmit desc ring base high 2 bits */
	char	x_b_telen;	/* Length of each transmit entry */
	x_short	x_b_trlen;	/* Number of entries in the XMIT desc ring */
	x_short	x_b_rdrbl;	/* Receive desc ring base low 16 bits */
	char	x_b_rdrbh;	/* Receive desc ring base high 2 bits */
	char	x_b_relen;	/* Length of each receive entry */
	x_short	x_b_rrlen;	/* Number of entries in the RECV desc ring */
};

/*
 * Transmit/Receive Ring Entry
 */
struct x_de_ring {
	x_short	x_r_slen;			/* Segment length */
	x_short	x_r_segbl;		/* Segment address (low 16 bits) */
	char	x_r_segbh;		/* Segment address (hi 2 bits) */
	x_u_char	x_r_flags;		/* Status flags */
	x_u_short	x_r_tdrerr;		/* Errors */
#define	x_r_lenerr	x_r_tdrerr
	x_short	x_r_rid;			/* Request ID */
};

#define	x_XFLG_OWN	0x80		/* If 0 then owned by driver */
#define	x_XFLG_ERRS	0x40		/* Error summary */
#define	x_XFLG_MTCH	0x20		/* Address match on xmit request */
#define	x_XFLG_MORE	0x10		/* More than one entry required */
#define	x_XFLG_ONE	0x08		/* One collision encountered */
#define	x_XFLG_DEF	0x04		/* Transmit deferred */
#define	x_XFLG_STP	0x02		/* Start of packet */
#define	x_XFLG_ENP	0x01		/* End of packet */

#define	x_XFLG_BITS	"\10\10OWN\7ERRS\6MTCH\5MORE\4ONE\3DEF\2STP\1ENP"

#define	x_XERR_BUFL	0x8000		/* Buffer length error */
#define	x_XERR_UBTO	0x4000		/* UNIBUS tiemout
#define	XERR_LCOL	0x1000		/* Late collision */
#define	x_XERR_LCAR	0x0800		/* Loss of carrier */
#define	x_XERR_RTRY	0x0400		/* Failed after 16 retries */
#define	x_XERR_TDR	0x03ff		/* TDR value */

#define	x_XERR_BITS	"\20\20BUFL\17UBTO\15LCOL\14LCAR\13RTRY"

#define	x_RFLG_OWN	0x80		/* If 0 then owned by driver */
#define	x_RFLG_ERRS	0x40		/* Error summary */
#define	x_RFLG_FRAM	0x20		/* Framing error */
#define	x_RFLG_OFLO	0x10		/* Message overflow */
#define	x_RFLG_CRC	0x08		/* CRC error */
#define	x_RFLG_STP	0x02		/* Start of packet */
#define	x_RFLG_ENP	0x01		/* End of packet */

#define	x_RFLG_BITS	"\10\10OWN\7ERRS\6FRAM\5OFLO\4CRC\2STP\1ENP"

#define	x_RERR_BUFL	0x8000		/* Buffer length error */
#define	x_RERR_UBTO	0x4000		/* UNIBUS tiemout */
#define	x_RERR_NCHN	0x2000		/* No data chaining */
#define	x_RERR_MLEN	0x0fff		/* Message length */

#define	x_RERR_BITS	"\20\20BUFL\17UBTO\16NCHN"

/* mode description bits */
#define	x_MOD_HDX		0x0001		/* Half duplex mode */
#define	x_MOD_LOOP	0x0004		/* Enable internal loopback */
#define	x_MOD_DTCR	0x0008		/* Disables CRC generation */
#define	x_MOD_DMNT	0x0200		/* Disable maintenance features */
#define	x_MOD_ECT		0x0400		/* Enable collision test */
#define	x_MOD_TPAD	0x1000		/* Transmit message pad enable */
#define	x_MOD_DRDC	0x2000		/* Disable data chaining */
#define	x_MOD_ENAL	0x4000		/* Enable all multicast */
#define	x_MOD_PROM	0x8000		/* Enable promiscuous mode */

struct	x_de_buf {
	struct x_ether_header x_db_head;	/* header */
	char	x_db_data[x_ETHERMTU];	/* packet data */
	x_int	x_db_crc;			/* CRC - on receive only */
};
