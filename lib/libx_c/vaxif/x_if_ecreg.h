#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)if_ecreg.h	7.1 (Berkeley) 6/5/86
 */

/*
 * 3Com Ethernet controller registers.
 */
struct x_ecdevice {
	x_short	x_ec_rcr;		/* Receive Control Register */
	x_short	x_ec_xcr;		/* Transmit Control Register */
};

/*
 * Control and status bits -- rcr
 */
#define	x_EC_SPIE		0x8000		/* set parity interrupt enable */
#define	x_EC_ASTEP	0x4000		/* increment address counter */
#define	x_EC_AROM		0x2000		/* 1: Use address ROM, 0: use RAM */
#define	x_EC_PE		0x2000		/* Parity error */
#define	x_EC_AWCLK	0x1000		/* address write clock bit */
#define	x_EC_PIE		0x1000		/* Parity interrupt enable (read) */
#define	x_EC_ADATA	0x0f00		/* address/filtering */
#define	x_EC_RDONE	0x0080		/* receive done */
#define	x_EC_MDISAB	0x0080		/* memory disable */
#define	x_EC_RINTEN	0x0040		/* receive interrupt enable */
#define	x_EC_RCLR		0x0020		/* clear RDONE bit */
#define	x_EC_RWBN		0x0010		/* submit buffer for receive */
#define	x_EC_RBN		0x000f		/* buffer number */

#define	x_EC_RBITS	"\10\16PE\15PIE\10RDONE\7RINTEN"

/*
 * Control and status bits -- xcr
 */
#define	x_EC_JAM		0x8000		/* collision dectected */
#define	x_EC_JINTEN	0x4000		/* collision interrupt enable */
#define	x_EC_JCLR		0x2000		/* clear collision detect */
#define	x_EC_UECLR	0x0100		/* reset controller */
#define	x_EC_XDONE	0x0080		/* transmit done */
#define	x_EC_XINTEN	0x0040		/* transmit interrupt enable */
#define	x_EC_XCLR		0x0020		/* clear XDONE bit */
#define	x_EC_XWBN		0x0010		/* submit buffer for transmit */
#define	x_EC_XBN		0x000f		/* buffer number */

#define	x_EC_XBITS	"\10\20JAM\17JINTEN\10XDONE\7XINTEN"

/*
 * Useful combinations
 */
#define	x_EC_READ		(0x600|x_EC_RINTEN|x_EC_RWBN)
#define	x_EC_WRITE	(x_EC_JINTEN|x_EC_XINTEN|x_EC_XWBN)
#define	x_EC_CLEAR	(x_EC_JINTEN|x_EC_XINTEN|x_EC_JCLR)

/*
 * Buffer number definitions
 */
#define	x_ECTBF		0		/* Buffer for transmit */
#define	x_ECRLBF		1		/* First buffer for receive */
#define	x_ECRHBF		15		/* Last buffer for receive */

#define	x_ECRDOFF		528		/* Packet offset in read buffer */
