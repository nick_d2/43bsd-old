#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)if_vv.h	7.1 (Berkeley) 6/5/86
 */

/*
 * ECO 176-748 changed the braodcast address from 0 to 0xff, at
 * CTL (p1002) serial number around 150.
 * It was implemented in August, 1982. This is a field-installable ECO,
 * which improves net reliability. If the broadcast address has not been
 * changed, comment out the following line.
 */
#define	x_NEW_BROADCAST		/* new chip for broadcast problem */

/*
 * Local network header for proNET Ring
 * This is arbitrated by "jas@proteon"
 * (aka John Shriver, 617-655-3340)
 */

struct x_vv_header {
	 /* the first two fields are required by the hardware */
	x_u_char	x_vh_dhost;	/* destination address */
	x_u_char	x_vh_shost;	/* source address */
	/* the next three fields are the local network header */
	x_u_char	x_vh_version;	/* header version */
	x_u_char	x_vh_type;	/* packet type => protocol number */
	x_short	x_vh_info;	/* protocol-specific information */
};

#define	x_RING_VERSION	2	/* current version of v2lni header */

/*
 * Packet types (protocol numbers) in proNET protocol header
 * Other types are defined, but are proprietary.
 */
#define	x_RING_IP		1
#define	x_RING_TRAILER	2	/* offset now in vh_info only */
#define x_RING_ARP	3
#define x_RING_HDLC	4
#define x_RING_VAXDB	5
#define x_RING_RINGWAY	6
#define x_RING_RINGWAYM	8
#define	x_RING_NOVELL	10
#define x_RING_PUP	12
#define x_RING_XNS	14
#define	x_RING_DIAGNOSTICS 15	/* protocol type for testing */
#define	x_RING_ECHO	16

#ifdef x_NEW_BROADCAST
#define	x_VV_BROADCAST	0xff	/* hardware-defined broadcast address */
#else
#define	x_VV_BROADCAST	0x00	/* hardware-defined broadcast address */
#endif

/*
 * Proteon proNET Hardware definitions
 * register bit definitions
 */
#define	x_VV_ENB	01		/* Enable Operation */
#define	x_VV_DEN	02		/* Enable DMA */
#define	x_VV_HEN	04		/* Host Relay Enable (Rcv) */
#define	x_VV_CPB	04		/* Clear Packet Buffer (Xmit) */
#define	x_VV_STE	010		/* Self Test Enable (Rcv) */
#define	x_VV_UT1	010		/* Unused (Xmit) */
#define	x_VV_LPB	020		/* Modem Disable (Rcv) */
#define	x_VV_INR	020		/* Initialize Ring (Xmit) */
#define	x_VV_RST	040		/* Reset */
#define	x_VV_IEN	0100		/* Interrupt Enable */
#define	x_VV_RDY	0200		/* Done */
#define	x_VV_DPR	0400		/* Data Present (Rcv) */
#define	x_VV_RFS	0400		/* Refused (Xmit) */
#define	x_VV_NXM	01000		/* Non Existent Memory */
#define	x_VV_OVR	02000		/* Overrun */
#define	x_VV_ODB	04000		/* Odd Byte (Rcv) */
#define	x_VV_UT2	04000		/* Unused (Xmit) */
#define	x_VV_LDE	010000		/* Parity on 10 megabit (Rcv), */
				/* Link Data Error on 80 megabit (Rcv) */
#define	x_VV_OPT	010000		/* Output Timeout (Xmit) */
#define	x_VV_NOK	020000		/* Ring Not OK */
#define	x_VV_BDF	040000		/* Bad Format in Operation */
#define	x_VV_NIR	0100000		/* Not in Ring */

#define	x_VVXERR	(x_VV_NXM|x_VV_OVR|x_VV_OPT|x_VV_BDF)	/* Xmit errs */
#define	x_VVRERR	(x_VV_NXM|x_VV_OVR|x_VV_ODB|x_VV_BDF|x_VV_DPR)	/* Rcv errs */
#define	x_VVFE	(x_VV_NXM|x_VV_OVR)			/* Fatal errors */

#define x_VV_IBITS \
"\10\20NIR\17BDF\16NOK\15LDE\14ODB\13OVR\12NXM\11DPR\10RDY\7IEN\6RST\5LPB\4STE\3HEN\2DEN\1ENB"

#define x_VV_OBITS \
"\10\20NIR\17BDF\16NOK\15OPT\13OVR\12NXM\11RFS\10RDY\7IEN\6RST\5INR\3HEN\2DEN\1ENB"

/* device registers */
struct x_vvreg {
	x_short	x_vvicsr;		/* input csr */
	x_u_short	x_vviwc;		/* input word count */
	x_u_short	x_vviba;		/* input addr lo */
	x_u_short	x_vviea;		/* input addr hi */
	x_short	x_vvocsr;		/* output csr */
	x_u_short	x_vvowc;		/* output word count */
	x_u_short	x_vvoba;		/* output addr lo */
	x_u_short	x_vvoea;		/* output addr hi */
};

#define	x_VVRETRY	7		/* output retry limit */
#define x_VVIDENTSUCC 5		/* number of successes required in self-test */
#define x_VVIDENTRETRY 10		/* identify loop attempt limit */
#define x_VVTIMEOUT 60		/* seconds before a transmit timeout */
