#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)if_hy.h	7.1 (Berkeley) 6/5/86
 */

/*
 * 4.2 BSD Unix Kernel - Vax Network Interface Support
 *
 * $Header: if_hy.h,v 10.0 84/06/30 19:51:21 steveg Stable $
 * $Locker:  $
 *
 * Modifications from Berkeley 4.2 BSD
 * Copyright (c) 1983, Tektronix Inc.
 * All Rights Reserved
 *
 *
 * $Log:	if_hy.h,v $
 *	Revision 10.0  84/06/30  19:51:21  steveg
 *	Big Build
 *	
 *	Revision 3.13  84/05/30  19:40:58  steveg
 *	update hy_stat to reflect new microcode
 *	
 *	Revision 3.12  84/05/30  19:06:57  steveg
 *	move driver state number definition here from if_hy.c
 *	
 *	Revision 3.11  84/05/30  18:56:15  steveg
 *	add definition of HYE_MAX and HYE_SIZE
 *	
 *	Revision 3.10  84/05/30  17:14:04  steveg
 *	add hyl_filter
 *	
 *	Revision 3.9  84/05/30  13:45:24  steveg
 *	rework logging
 *	
 *	Revision 3.8  84/05/04  05:18:59  steveg
 *	hyr_key now a u_long
 *	
 *	Revision 3.7  84/05/01  22:45:20  steveg
 *	add H_RLOOPBK for A710 remote end loopback command
 *	
 *
 */


/*
 * Structure of a HYPERchannel adapter header
 */
struct	x_hy_hdr {
	x_short	x_hyh_ctl;		/* control */
	x_short	x_hyh_access;		/* access code */
	union {
		x_short	x_hyh_addr;
		char	x_hyh_baddr[2];
	} x_hyh_uto, x_hyh_ufrom;		/* to/from address */
	x_short	x_hyh_param;		/* parameter word */
	x_short	x_hyh_type;		/* record type */
};


#define x_hyh_to		x_hyh_uto.x_hyh_addr
#define x_hyh_to_port	x_hyh_uto.x_hyh_baddr[1]
#define x_hyh_to_adapter	x_hyh_uto.x_hyh_baddr[0]

#define x_hyh_from	x_hyh_ufrom.x_hyh_addr
#define x_hyh_from_port	x_hyh_ufrom.x_hyh_baddr[1]
#define x_hyh_from_adapter x_hyh_ufrom.x_hyh_baddr[0]

/*
 * Structure of a HYPERchannel message header (from software)
 */
struct	x_hym_hdr {
	struct {
		x_short	x_hymd_mplen;	/* message proper len, if associated data */
	} x_hym_d;
	struct	x_hy_hdr x_hym_h;	/* hardware header, MUST BE LAST */
};

#define x_hym_mplen	x_hym_d.x_hymd_mplen

#define x_hym_ctl		x_hym_h.x_hyh_ctl
#define x_hym_access	x_hym_h.x_hyh_access
#define x_hym_param	x_hym_h.x_hyh_param
#define x_hym_type	x_hym_h.x_hyh_type

#define x_hym_to		x_hym_h.x_hyh_to
#define x_hym_to_port	x_hym_h.x_hyh_to_port
#define x_hym_to_adapter	x_hym_h.x_hyh_to_adapter

#define x_hym_from	x_hym_h.x_hyh_from
#define x_hym_from_port	x_hym_h.x_hyh_from_port
#define x_hym_from_adapter x_hym_h.x_hyh_from_adapter

#define x_HYM_SWLEN (sizeof(struct x_hym_hdr) - sizeof(struct x_hy_hdr))

/*
 * HYPERchannel header word control bits
 */
#define x_H_XTRUNKS	0x00F0	/* transmit trunks */
#define x_H_RTRUNKS	0x000F	/* remote trunks to transmit on for loopback */
#define x_H_ASSOC		0x0100	/* has associated data */
#define x_H_LOOPBK	0x00FF	/* loopback command */
#define x_H_RLOOPBK	0x008F	/* A710 remote loopback command */

/*
 * Hyperchannel record types
 */
#define x_HYLINK_IP	0	/* Internet Protocol Packet */

/*
 * Routing database
 */
#define x_HYRSIZE  37	/* max number of adapters in routing tables */

struct x_hy_route {
	x_time_t x_hyr_lasttime;		/* last update time */
	x_u_char x_hyr_gateway[256];
	struct x_hyr_hash {
		x_u_long	x_hyr_key;	/* desired address */
		x_u_short x_hyr_flags;	/* status flags - see below */
		x_u_short x_hyr_size;	/* number of entries */
		union {
			/*
			 * direct entry (can get there directly)
			 */
			struct {
				x_u_short x_hyru_dst;	/* adapter number & port */
				x_u_short x_hyru_ctl;	/* trunks to try */
				x_u_short x_hyru_access;	/* access code (mostly unused) */
			} x_hyr_d;
#define x_hyr_dst		x_hyr_u.x_hyr_d.x_hyru_dst
#define x_hyr_ctl		x_hyr_u.x_hyr_d.x_hyru_ctl
#define x_hyr_access	x_hyr_u.x_hyr_d.x_hyru_access
			/*
			 * indirect entry (one or more hops required)
			 */
			struct {
				x_u_char x_hyru_pgate;	/* 1st gateway slot */
				x_u_char x_hyru_egate;	/* # gateways */
				x_u_char x_hyru_nextgate;	/* gateway to use next */
			} x_hyr_i;
#define x_hyr_pgate	x_hyr_u.x_hyr_i.x_hyru_pgate
#define x_hyr_egate	x_hyr_u.x_hyr_i.x_hyru_egate
#define x_hyr_nextgate	x_hyr_u.x_hyr_i.x_hyru_nextgate
		} x_hyr_u;
	} x_hyr_hash[x_HYRSIZE];
};

/*
 * routing table set/get structure
 *
 * used to just pass the entire routing table through, but 4.2 ioctls
 * limit the data part of an ioctl to 128 bytes or so and use the
 * interface name to get things sent the right place.
 * see ../net/if.h for additional details.
 */
struct x_hyrsetget {
	char	x_hyrsg_name[x_IFNAMSIZ];	/* if name, e.g. "hy0" */
	struct x_hy_route *x_hyrsg_ptr;	/* pointer to routing table */
	x_unsigned_int	x_hyrsg_len;	/* size of routing table provided */
};

#define x_HYR_INUSE	0x01	/* entry in use */
#define x_HYR_DIR		0x02	/* direct entry */
#define x_HYR_GATE	0x04	/* gateway entry */
#define x_HYR_LOOP	0x08	/* hardware loopback entry */
#define x_HYR_RLOOP	0x10	/* remote adapter hardware loopback entry */

#define x_HYRHASH(x_x) (((x_x) ^ ((x_x) >> 16)) % x_HYRSIZE)

#define x_HYSETROUTE	x__IOW(x_i, 0x80, struct x_hyrsetget)
#define x_HYGETROUTE	x__IOW(x_i, 0x81, struct x_hyrsetget)

struct	x_hylsetget {
	char	x_hylsg_name[x_IFNAMSIZ];	/* if name, e.g. "hy0" */
	x_int	x_hylsg_cmd;		/* logging command */
	x_caddr_t	x_hylsg_ptr;		/* pointer to table */
	x_u_long	x_hylsg_len;		/* size of table provided */
};	

#define x_HYSETLOG	x__IOW(x_i, 0x82, struct x_hylsetget)
#define x_HYGETLOG	x__IOW(x_i, 0x83, struct x_hylsetget)
#define x_HYGETELOG	x__IOW(x_i, 0x84, struct x_hylsetget)

/*
 * Structure of Statistics Record (counters)
 */
struct	x_hy_stat {
	x_u_char	x_hyc_df0[3];		/* # data frames trunk 0 */
	x_u_char	x_hyc_df1[3];		/* # data frames trunk 1 */
	x_u_char	x_hyc_df2[3];		/* # data frames trunk 2 */
	x_u_char	x_hyc_df3[3];		/* # data frames trunk 3 */
	x_u_char	x_hyc_cancel[2];		/* # cancel operations */
	x_u_char	x_hyc_abort[2];		/* # aborts */
	x_u_char	x_hyc_ret0[3];		/* # retransmissions trunk 0 */
	x_u_char	x_hyc_ret1[3];		/* # retransmissions trunk 1 */
	x_u_char	x_hyc_ret2[3];		/* # retransmissions trunk 2 */
	x_u_char	x_hyc_ret3[3];		/* # retransmissions trunk 3 */
	x_u_char	x_hyc_atype[3];		/* adapter type and revision level */
	x_u_char	x_hyc_uaddr;		/* adapter unit number */
};

/*
 * Structure of the Status Record
 */
struct x_hy_status {
	x_u_char	x_hys_gen_status;		/* general status byte */
	x_u_char	x_hys_last_fcn;		/* last function code issued */
	x_u_char	x_hys_resp_trunk;		/* trunk response byte */
	x_u_char	x_hys_status_trunk;	/* trunk status byte */
	x_u_char	x_hys_recd_resp;		/* recieved response byte */
	x_u_char	x_hys_error;		/* error code */
	x_u_char	x_hys_caddr;		/* compressed addr of 1st msg on chain */
	x_u_char	x_hys_pad;		/* not used */
};

/*
 * Get port number from status record
 */
#define x_PORTNUM(x_p)	(((x_p)->x_hys_gen_status >> 6) & 0x03)

#define x_HYL_SIZE 16*1024
struct x_hy_log {
	struct	x_hy_log *x_hyl_self;
	x_u_char	x_hyl_enable;		/* logging enabled? */
	x_u_char	x_hyl_onerr;		/* state to enter on error */
	x_u_short	x_hyl_wait;		/* number of bytes till next wakeup */
	x_u_short	x_hyl_count;		/* number of samples till stop */
	x_u_short x_hyl_icount;		/* initial value of hyl_count */
	x_u_long	x_hyl_filter;		/* log items with specific bits set */
	x_u_char	*x_hyl_eptr;		/* &hy_log.hyl_buf[HYL_SIZE] */
	x_u_char	*x_hyl_ptr;		/* pointer into hyl_buf */
	x_u_char	x_hyl_buf[x_HYL_SIZE];	/* log buffer space */
};

#define x_HYL_NOP		0
#define x_HYL_UP		1	/* markup */
#define x_HYL_STATUS	2	/* status results (struct hy_status) */
#define x_HYL_STATISTICS	3	/* statistics (struct hy_stat) */
#define x_HYL_XMIT	4	/* packed being send (struct hym_hdr) */
#define x_HYL_RECV	5	/* recieved pkt (short len; struct hym_hdr) */
#define x_HYL_CMD		6	/* cmd issued (uchar cmd, state; short count) */
#define x_HYL_INT		7	/* interrupt (short csr, wcr) */
#define	x_HYL_CANCEL	8	/* cancel transmit attempt */
#define	x_HYL_RESET	9	/* hyinit or unibus reset */
#define	x_HYL_IOCTL	10	/* hyioctl */

#define x_HYL_DISABLED	0	/* logging disabled */
#define x_HYL_CONTINUOUS	1	/* continuous logging */
#define x_HYL_CATCHN	2	/* hyl_count transactions being captured */

/*
 * error code histograms
 */
#define	x_HYE_MAX		0x18		/* maximum adapter error code */
#define	x_HYE_BINS	4		/* number of command bins */
#define	x_HYE_SIZE  (x_HYE_MAX+1)*x_HYE_BINS	/* size of histogram buffer */

/*
 * Requests for service (in order by descending priority).
 */
#define x_RQ_ENDOP	001	/* end the last adapter function */
#define x_RQ_REISSUE	002	/* reissue previous cmd after status */
#define x_RQ_STATUS	004	/* get the status of the adapter */
#define x_RQ_STATISTICS	010	/* get the statistics of the adapter */
#define x_RQ_MARKDOWN	020	/* mark this adapter port down */
#define x_RQ_MARKUP	040	/* mark this interface up */

#define x_RQ_XASSOC	0100	/* associated data to transmit */

/* 
 * Driver states.
 */
#define	x_STARTUP		0	/* initial state (before fully there) */
#define	x_IDLE		1	/* idle state */
#define	x_STATSENT	2	/* status cmd sent to adapter */
#define	x_ENDOPSENT	3	/* end operation cmd sent */
#define	x_RECVSENT	4	/* input message cmd sent */
#define	x_RECVDATASENT	5	/* input data cmd sent */
#define	x_XMITSENT	6	/* transmit message cmd sent */
#define	x_XMITDATASENT	7	/* transmit data cmd sent */
#define	x_WAITING		8	/* waiting for messages */
#define	x_CLEARSENT	9	/* clear wait for message cmd sent */
#define x_MARKPORT	10	/* mark this host's adapter port down issued */
#define x_RSTATSENT	11	/* read statistics cmd sent to adapter */

#ifdef x_HYLOG
char *x_hy_state_names[] = {
	"Startup",
	"Idle",
	"Status Sent",
	"End op Sent",
	"Recieve Message Proper Sent",
	"Recieve Data Sent",
	"Transmit Message Proper Sent",
	"Transmit Data Sent",
	"Wait for Message Sent",
	"Clear Wait for Message Sent",
	"Mark Port Down Sent",
	"Read Statistics Sent"
};
#endif

