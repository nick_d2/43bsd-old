#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)if_exreg.h	7.1 (Berkeley) 6/5/86
 */


struct	x_exdevice {
	char	x_xd_porta;	/* write on porta resets EXOS */
	char	x_xd_pad_a;
	char	x_xd_portb;	/* write on portb interrupts EXOS */
				/* read on portb returns status bits */
	char	x_xd_pad_b;
};

/* EXOS I/O PORT A write definitions */
#define	x_EX_RESET	0	/* value doesn't really matter... */

/* EXOS I/O PORT B write definitions */
#define	x_EX_NTRUPT	0

/* EXOS I/O PORT B read definitions */
#define	x_EX_TESTOK	1	/* set when self-diagnostics passed */
#define	x_EX_UNREADY	(1<<3)	/* set until EXOS ready to read from B */

/* message buffer status field definitions */
#define	x_MH_OWNER	1	/* mask for status bit for owner */
#define	x_MH_HOST		0	/* if 0, the host owns the buffer */
#define	x_MH_EXOS		1	/* if 1, the EXOS owns the buffer */

/* EXOS Link Level request codes */
#define	x_LLTRANSMIT	0xC	/* send a packet */
#define	x_LLRTRANSMIT	0xE	/* send a packet, and self-receive */
#define	x_LLRECEIVE	0xD	/* receive a packet */
#define	x_LLNET_MODE	0x8	/* read/write mode control objects */
#define	x_LLNET_ADDRS	0x9	/* read/write receive address slots */
#define	x_LLNET_RECV	0xA	/* read/alter receive slot enable bit */
#define	x_LLNET_STSTCS	0xB	/* read/reset network statistics objects */

/* Link Level return codes common to all requests */
#define	x_LL_OK		0	/* successful completion */
#define	x_LLX_MODE	0xA1	/* EXOS not in link level mode (impossible) */

/* LLTRANSMIT unique return codes */
#define	x_LLXM_1RTRY	0x1	/* successful xmission, 1 retry */
#define	x_LLXM_RTRYS	0x2	/* successful xmission, more than 1 retry */
#define	x_LLXM_NSQE	0x8	/* successful xmission, no SQE TEST signal */
#define	x_LLXM_CLSN	0x10	/* xmission failed, excess retries */
#define	x_LLXM_NCS	0x20	/* xmission failed, no carrier sense */
#define	x_LLXM_LNGTH	0x40	/* xmission failed, bad packet length */
#define	x_XMIT_BITS	"\7\7LENGTH\6CARRIER\5XCLSNS\4SQETST"
#define	x_LLXM_ERROR	(x_LLXM_NSQE|x_LLXM_CLSN|x_LLXM_NCS|x_LLXM_LNGTH)

/* LLRECEIVE unique return codes */
#define	x_LLRC_TRUNC	0x4	/* pkt received, but truncated to fit buffer */
#define	x_LLRC_ALIGN	0x10	/* pkt received, but with alignment error */
#define	x_LLRC_CRC	0x20	/* pkt received, but with CRC error */
#define	x_LLRC_BUFLEN	0x40	/* no pkt received, buffer less than 64 bytes */
				/* this should never happen here */
#define	x_RECV_BITS	"\7\7BUFLEN\6CRC\5ALIGN\3TRUNC"

/* LLNET_ADDRS unique return codes */
#define	x_LLNA_BADSLOT	0xD1	/* slot doesn't exist or can't be accessed */
#define	x_LLNA_BADADDR	0xD3	/* invalid address for designated slot */

/* LLNET_RECV unique return codes */
#define	x_LLNR_BADSLOT	0xD1	/* slot doesn't exist or can't be accessed */
#define	x_LLNR_BADADDR	0xD2	/* designated slot was empty */

/* address slot object indices */
#define	x_NULLSLOT	0	/* the null slot */
#define	x_MINMCSLOT	1	/* minimum multicast slot index */
#define	x_MAXMCSLOT	8	/* default maximum multicast slot index */
#define	x_PHYSSLOT	253	/* physical slot index */
#define	x_UNVRSSLOT	254	/* universal slot index */
#define	x_BROADSLOT	255	/* broadcast slot index */

/* request mask bit definitions */
#define	x_WRITE_OBJ	1	/* write the object */
#define	x_READ_OBJ	2	/* read the object */
#define	x_ENABLE_RCV	4	/* enable reception on designated slot */

/* NET_MODE options mask bit definitions */
#define	x_OPT_ALIGN	0x10	/* receive packets with alignment errors */
#define	x_OPT_CRC		0x20	/* receive packets with CRC errors */
#define	x_OPT_DSABLE	0x80	/* disconnect controller hardware */

/* NET_MODE mode field value definitions */
#define	x_MODE_OFF	0	/* stop transmission and reception */
#define	x_MODE_PERF	1	/* perfect multicast address filtering */
#define	x_MODE_HW		2	/* hardware-only multicast address filtering */
#define	x_MODE_PROM	3	/* promiscuous reception */

#define	x_NFRAGMENTS 1	/* number fragments that the EXOS will scatter/gather */
#define	x_EXMAXRBUF 1520	/* per EXOS 101 manual 5.3.7 (maybe 1518 would do) */

/*
 * N.B.  Structures below are carefully constructed so that
 * they correspond to the message formats that NX firmware
 * defines.  None of them should contain any compiler-instigated
 * padding.  Be especially careful about VAX C longword alignment!
 */

struct	x_stat_array {
	x_u_long	x_sa_fsent;	/* frames sent without errors */
	x_u_long	x_sa_xsclsn;	/* frames aborted excess collisions */
	x_u_long	x_sa_nsqe;	/* frames subject to heartbeat failure */
	x_u_long	x_sa_undef;	/* undefined (TDR on EXOS 101) */
	x_u_long	x_sa_frcvd;	/* frames received no errors */
	x_u_long	x_sa_align;	/* frames received alignment error */
	x_u_long	x_sa_crc;		/* frames received crc error */
	x_u_long	x_sa_flost;	/* frames lost */
};

struct	x_buf_blk {		/* packet/buffer block descriptor */
	x_u_short	x_bb_len;			/* length of block, in bytes */
	x_u_short	x_bb_addr[2];		/* address of block */
	/*
	 * Array above is really a single u_long field.
	 * We kludge its definition to defeat word-alignment.
	 * Access would look like:
	 *     longaddr = *(u_long *)bp->.mb_er.er_blks[0].bb_addr;
	 */
};

struct	x_net_mode {		/* read/write mode control objects */
/*12*/	x_u_char	x_nm_rqst;	/* request code */
/*13*/	x_u_char	x_nm_rply;	/* reply code */
/*14*/	x_u_char	x_nm_mask;		/* bit-wise switches for read, write */
/*15*/	x_u_char	x_nm_optn;		/* acceptable packet reception errors */
/*16*/	x_u_char	x_nm_mode;		/* EXOS filtering mode */
/*17*/
};

struct	x_net_addrs {		/* read/write receive address slots */
/*12*/	x_u_char	x_na_rqst;	/* request code */
/*13*/	x_u_char	x_na_rply;	/* reply code */
/*14*/	x_u_char	x_na_mask;		/* bit-wise switches for read, write */
/*15*/	x_u_char	x_na_slot;		/* index of address slot */
/*16*/	x_u_char	x_na_addrs[6];		/* address read and/or written */
/*22*/
};

struct	x_net_recv {		/* read/alter receive slot enable bit */
/*12*/	x_u_char	x_nr_rqst;	/* request code */
/*13*/	x_u_char	x_nr_rply;	/* reply code */
/*14*/	x_u_char	x_nr_mask;		/* bit-wise switches for read, write */
/*15*/	x_u_char	x_nr_slot;		/* index of address slot */
/*16*/
};

struct	x_net_ststcs {		/* read/reset network statistics objects */
/*12*/	x_u_char	x_ns_rqst;	/* request code */
/*13*/	x_u_char	x_ns_rply;	/* reply code */
/*14*/	x_u_char	x_ns_mask;		/* bit-wise switches for read, write */
/*15*/	x_u_char	x_ns_rsrv;		/* reserved for EXOS */
/*16*/	x_u_short	x_ns_nobj;		/* number of objects to work on */
/*18*/	x_u_short	x_ns_xobj;		/* index of first object to work on */
/*20*/	x_u_long	x_ns_bufp;		/* pointer to statistics buffer */
/*24*/
};

struct	x_enet_xmit {		/* send a packet on the Ethernet */
/*12*/	x_u_char	x_et_rqst;	/* request code */
/*13*/	x_u_char	x_et_rply;	/* reply code */
/*14*/	x_u_char	x_et_slot;		/* address slot matching dest address */
/*15*/	x_u_char	x_et_nblock;		/* number of blocks composing packet */
/*16*/	struct	x_buf_blk x_et_blks[x_NFRAGMENTS];	/* array of block descriptors */
/*22-64*/
};

struct	x_enet_recv {		/* receive a packet on the Ethernet */
/*12*/	x_u_char	x_er_rqst;	/* request code */
/*13*/	x_u_char	x_er_rply;	/* reply code */
/*14*/	x_u_char	x_er_slot;		/* address slot matching dest address */
/*15*/	x_u_char	x_er_nblock;		/* number of blocks composing buffer */
/*16*/	struct	x_buf_blk x_er_blks[x_NFRAGMENTS];	/* array of block descriptors */
/*22-64*/
};

/* we send requests and receive replys with the EXOS using this structure */
struct	x_ex_msg {
/*00*/	x_u_short	x_mb_link;	/* address of next message buffer */
/*02*/	x_u_char	x_mb_rsrv;	/* reserved for use by EXOS */
/*03*/	x_u_char	x_mb_status;	/* used bit-wise for message protocol */
/*04*/	x_u_short	x_mb_length;	/* length, in bytes, of the rest */
/*06*/	x_short	x_mb_1rsrv;	/* reserved for used by EXOS */
/*08*/	x_long	x_mb_mid;		/* available to user */
/*12*/	union	x_mb_all {
		struct	x_net_mode	x_mb_net_mode;
		struct	x_net_addrs	x_mb_net_addrs;
		struct	x_net_recv	x_mb_net_recv;
		struct	x_net_ststcs	x_mb_net_ststcs;
		struct	x_enet_xmit	x_mb_enet_xmit;
		struct	x_enet_recv	x_mb_enet_recv;
	} x_mb_all;
/* following field is used only by host, not read by board */
	struct	x_ex_msg *x_mb_next;	/* host's pointer to next message */
};
#define	x_mb_nm	x_mb_all.x_mb_net_mode
#define	x_mb_na	x_mb_all.x_mb_net_addrs
#define	x_mb_nr	x_mb_all.x_mb_net_recv
#define	x_mb_ns	x_mb_all.x_mb_net_ststcs
#define	x_mb_et	x_mb_all.x_mb_enet_xmit
#define	x_mb_er	x_mb_all.x_mb_enet_recv
#define	x_mb_rqst	x_mb_nm.x_nm_rqst
#define	x_mb_rply	x_mb_nm.x_nm_rply
#define	x_MBDATALEN (sizeof(union x_mb_all)+6)

struct	x_confmsg {
/*00*/	x_u_short	x_cm_1rsrv;	/* reserved, must be 1 */
/*02*/	char	x_cm_vc[4];	/* returns ASCII version code */
/*06*/	x_u_char	x_cm_cc;		/* returns config completion code */
/*07*/	x_u_char	x_cm_opmode;	/* specifies operation mode */
/*08*/	x_u_short	x_cm_dfo;		/* specifies host data format option */
/*10*/	x_u_char	x_cm_dcn1;	/* reserved, must be 1 */
/*11*/	x_u_char	x_cm_2rsrv[2];	/* reserved, must be 0 */
/*13*/	x_u_char	x_cm_ham;		/* specifies host address mode */
/*14*/	x_u_char	x_cm_3rsrv;	/* reserved, must be 0 */
/*15*/	x_u_char	x_cm_mapsiz;	/* reserved, must be 0 */
/*16*/	x_u_char	x_cm_byteptrn[4];	/* host data format option test pattern */
/*20*/	x_u_short	x_cm_wordptrn[2];
/*24*/	x_u_long	x_cm_lwordptrn;
/*28*/	x_u_char	x_cm_rsrvd[20];
/*48*/	x_u_long	x_cm_mba;		/* use 0xFFFFFFFF in link level mode */
/*52*/	x_u_char	x_cm_nproc;	/* use 0xFF in link level mode */
/*53*/	x_u_char	x_cm_nmbox;	/* use 0xFF in link level mode */
/*54*/	x_u_char	x_cm_nmcast;	/* use 0xFF in link level mode */
/*55*/	x_u_char	x_cm_nhost;	/* use 1 in link level mode */

	/* the next five parameters define the request message queue */
/*56*/	x_u_long	x_cm_h2xba;	/* base address of message queue */
/*60*/	x_u_short	x_cm_h2xhdr;	/* address offset of msg Q header */
/*62*/	x_u_char	x_cm_h2xtyp;	/* interrupt type */
/*63*/	x_u_char	x_cm_h2xval;	/* interrupt value (not used) */
/*64*/	x_u_short	x_cm_h2xaddr;	/* interrupt vector */
/*66*/	x_u_short	x_cm_h2xpad;	/* pad out unused portion of vector */

	/* the next five parameters define the reply message queue */
/*68*/	x_u_long	x_cm_x2hba;	/* base address of message queue */
/*72*/	x_u_short	x_cm_x2hhdr;	/* address offset of msg Q header */
/*74*/	x_u_char	x_cm_x2htyp;	/* interrupt type */
/*75*/	x_u_char	x_cm_x2hval;	/* interrupt value (not used) */
/*76*/	x_u_short	x_cm_x2haddr;	/* interrupt vector */
/*78*/	x_u_short	x_cm_x2hpad;	/* pad out unused portion of vector */
/*80*/
};

