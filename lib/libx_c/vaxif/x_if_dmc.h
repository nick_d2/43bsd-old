#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)if_dmc.h	7.1 (Berkeley) 6/5/86
 */

/*
 * DMC-11 Interface
 */

struct x_dmcdevice {
        char    x_bsel0;
	char 	x_bsel1;
	char	x_bsel2;
	char 	x_bsel3;
	x_short	x_sel4;
	x_short	x_sel6;
};

/*
 * dmc software packet encapsulation.  This allows the dmc
 * link to be multiplexed among several protocols.
 * The first eight bytes of the dmc header are garbage,
 * since on a vax the uba has been known to mung these
 * bytes.  The next two bytes encapsulate packet type.
 */
struct x_dmc_header {
	char	x_dmc_buf[8];	/* space for uba on vax */
	x_short	x_dmc_type;	/* encapsulate packet type */
};

/* packet types */
#define	x_DMC_IPTYPE	1
#define	x_DMC_TRAILER	2
#define	x_DMC_NTRAILER	16

/*
 * DMCMTU includes space for data (1024) + 
 * protocol header (256) + trailer descriptor (4).
 * The software link encapsulation header (dmc_header)
 * is handled separately.
 */
#define x_DMCMTU  1284

#define	x_RDYSCAN	16	/* loop delay for RDYI after RQI */

/* defines for bsel0 */
#define	x_DMC_BACCI	0
#define	x_DMC_CNTLI	1
#define	x_DMC_PERR	2
#define	x_DMC_BASEI	3
#define	x_DMC_WRITE	0		/* transmit block */
#define	x_DMC_READ	4		/* read block */
#define	x_DMC_RQI		0040		/* port request bit */
#define	x_DMC_IEI		0100		/* enable input interrupts */
#define	x_DMC_RDYI	0200		/* port ready */
#define	x_DMC0BITS	"\10\8RDI\7IEI\6RQI"

/* defines for bsel1 */
#define	x_DMC_MCLR	0100		/* DMC11 Master Clear */
#define	x_DMC_RUN		0200		/* clock running */
#define	x_DMC1BITS	"\10\8RUN\7MCLR"

/* defines for bsel2 */
#define	x_DMC_BACCO	0
#define	x_DMC_CNTLO	1
#define	x_DMC_OUX		0		/* transmit block */
#define	x_DMC_OUR		4		/* read block */
#define	x_DMC_IEO		0100		/* enable output interrupts */
#define	x_DMC_RDYO	0200		/* port available */
#define	x_DMC2BITS	"\10\8RDO\7IEO"

/* defines for CNTLI mode */
#define	x_DMC_HDPLX	02000		/* half duplex DDCMP operation */
#define	x_DMC_SEC		04000		/* half duplex secondary station */
#define	x_DMC_MAINT	00400		/* enter maintenance mode */

/* defines for BACCI/O and BASEI mode */
#define	x_DMC_XMEM	0140000		/* xmem bit position */
#define	x_DMC_CCOUNT	0037777		/* character count mask */
#define	x_DMC_RESUME	0002000		/* resume (BASEI only) */

/* defines for CNTLO */
#define	x_DMC_CNTMASK	01777

#define	x_DMC_DATACK	01
#define	x_DMC_TIMEOUT	02
#define	x_DMC_NOBUFS	04
#define	x_DMC_MAINTREC	010
#define	x_DMC_LOSTDATA	020
#define	x_DMC_DISCONN	0100
#define	x_DMC_START	0200
#define	x_DMC_NEXMEM	0400
#define	x_DMC_ERROR	01000

#define	x_DMC_FATAL (x_DMC_ERROR|x_DMC_NEXMEM|x_DMC_START|x_DMC_LOSTDATA|x_DMC_MAINTREC)
#define	x_CNTLO_BITS	\
   "\10\12ERROR\11NEXMEM\10START\7DISC\5LSTDATA\4MAINT\3NOBUF\2TIMEO\1DATACK"
