#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)if_en.h	7.1 (Berkeley) 6/5/86
 */

/*
 * Structure of a Ethernet header.
 */
struct	x_en_header {
	x_u_char	x_en_shost;
	x_u_char	x_en_dhost;
	x_u_short	x_en_type;
};

#define	x_ENTYPE_PUP	0x0200		/* PUP protocol */
#define	x_ENTYPE_IP	0x0201		/* IP protocol */

/*
 * The ENTYPE_NTRAILER packet types starting at
 * ENTYPE_TRAIL have (type-ENTYPE_TRAIL)*512 bytes
 * of data followed by an Ethernet type (as given above)
 * and then the (variable-length) header.
 */
#define	x_ENTYPE_TRAIL	0x1000		/* Trailer type */
#define	x_ENTYPE_NTRAILER	16

#define x_EN_BROADCAST	0		/* Hardware broadcast address */
