#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)if_cssreg.h	7.1 (Berkeley) 6/5/86
 */

/* 
 * DEC/CSS IMP11-A ARPAnet interface
 */

struct x_cssdma {
	x_short	x_wc;		/* word count */
	x_u_short	x_ba;		/* bus address (low 16 bits) */
	x_short	x_csr;		/* status register */
	x_short	x_db;		/* data buffer*/
};

struct x_cssdevice {
	struct	x_cssdma	x_css_output;	/* transmit DR11-B */
	struct	x_cssdma	x_css_hole;	/* unclever gap */
	struct	x_cssdma	x_css_input;	/* receive DR11-B */
};

#define x_css_icsr        x_css_input.x_csr
#define x_css_iba         x_css_input.x_ba
#define x_css_iwc         x_css_input.x_wc
#define x_css_ocsr        x_css_output.x_csr
#define x_css_oba         x_css_output.x_ba
#define x_css_owc         x_css_output.x_wc

/*
 * Bits Common to both input and out CSR's
 */
#define x_CSS_ERR         0x8000          /* error present */
#define x_CSS_NXM         0x4000          /* non-existant memory */
#define	x_CSS_ATTN	0x2000		/* attention */
#define	x_CSS_MAINT	0x1000		/* maintenance mode */
#define	x_CSS_CYCLE	0x0100		/* force bus cycle */
#define x_CSS_RDY         0x0080          /* ready */
#define x_CSS_IE          0x0040          /* interrupt enable */
#define	x_CSS_XA		0x0030		/* extended address bits */
#define	x_CSS_CLR		0x0020		/* clear status (reset) */
#define x_CSS_GO          0x0001          /* start operation */

/*
 * Input Control Status Register
 */
#define x_IN_EOM          0x0800          /* end-of-message recieved */
#define x_IN_IMPNR	0x0400          /* IMP not ready */
#define x_IN_RLE          0x0200          /* ready line error */
#define x_IN_WEN          0x0008          /* write enable */
#define x_IN_HRDY         0x0004          /* host ready */

#define x_CSS_INBITS \
"\20\20ERR\17NXM\16ATTN\15MAINT\14EOM\13IMPNR\12RLE\11CYCLE\10RDY\7IE\6XBA17\5XBA16\4WE\3HRDY\2CLR\1GO"


/*
 * Output Control Status Register
 */
#define x_OUT_TXEC	0x0008          /* tx error clear */
#define x_OUT_ENLB	0x0004          /* enable last bit */

#define x_CSS_OUTBITS \
"\20\20ERR\17NXM\16ATTN\15MAINT\11CYCLE\10RDY\7IE\6XBA17\5XBA16\4TXEC\3ENLB\2CLR\1GO"
