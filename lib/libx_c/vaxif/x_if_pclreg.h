#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)if_pclreg.h	7.1 (Berkeley) 6/5/86
 */

/*
 * DEC CSS PCL-11B Parallel Communications Interface
 */

struct x_pcldevice  {
	x_u_short	x_pcl_tcr;	/* Transmitter Command Register */
	x_u_short	x_pcl_tsr;	/* Transmitter Status Register */
	x_u_short	x_pcl_tsdb;	/* Transmitter Source Data Buffer */
	x_short	x_pcl_tsbc;	/* Transmitter Source Byte Count */
	x_u_short	x_pcl_tsba;	/* Transmitter Source Bus Address */
	x_u_short	x_pcl_tmmr;	/* Transmitter Master/Maint Regs */
	x_u_short	x_pcl_tscrc;	/* Transmitter Source CRC */
	x_u_short	x_pcl_spare;
	x_u_short	x_pcl_rcr;	/* Receiver Command Register */
	x_u_short	x_pcl_rsr;	/* Receiver Status Register */
	x_u_short	x_pcl_rddb;	/* Receiver Destination Data Buffer */
	x_short	x_pcl_rdbc;	/* Receiver Destination Byte Count */
	x_u_short	x_pcl_rdba;	/* Receiver Destination Bus Address */
	x_u_short	x_pcl_rdcrc;	/* Receiver Destination CRC */
};

/* Transmitter Command and Status Bits */
#define x_PCL_STTXM	(1<<0)		/* Start transmission */
#define x_PCL_TXINIT	(1<<1)		/* Transmitter Initialize */
#define x_PCL_IE		(1<<6)		/* Interrupt Enable */
#define x_PCL_SNDWD	(1<<13)		/* Send word */
#define x_PCL_TXNPR	(1<<14)		/* Transmitter NPR */
#define x_PCL_RIB		(1<<15)		/* Retry if busy */

#define x_PCL_RESPA	(3<<0)		/* Response A bits (tsr & rsr) */
#define x_PCL_RESPB	(3<<2)		/* Response B bits (tsr & rsr) */
#define x_PCL_MSTDWN	(1<<11)		/* Master down */
#define x_PCL_ERR		(1<<15)		/* Error summary */

#define x_PCL_MASTER	(1<<8)		/* Set MASTER status */
#define x_PCL_AUTOADDR	(1<<12)		/* Auto time slicing */

/* Receiver Command and Status Bits */
#define x_PCL_RCVDAT	(1<<0)		/* Receive data */
#define x_PCL_RCINIT	(1<<1)		/* Receiver Initialize */
#define x_PCL_RCVWD	(1<<13)		/* Receive word */
#define x_PCL_RCNPR	(1<<14)		/* Receive NRP */
#define x_PCL_REJ		(1<<15)		/* Reject transmission */

#define x_PCL_BCOFL	(1<<9)		/* Byte Counter Overflow */

#define x_PCL_TERRBITS	"\20\20ERR\17NXL\16MEM_OFL\15TXM_ERR\14MST_DWN\13TIM_OUT\12OVERRUN\11DTI_RDY\10SUC_TXF\07BUSY\06SOREJ\05TBS_BUSY"
#define x_PCL_TCSRBITS	"\20\20RIB\17TX_NPR\16SND_WD\10RD_SILO\07IE\04DTO_RDY\03INH_ADI\02TX_INIT\01START_TXM"

#define x_PCL_RERRBITS	"\20\20ERR\17NXL\16MEM_OFL\15TXM_ERR\14PARITY\13TIM_OUT\12BC_OFL\11DTO_RDY\10SUC_TXF\07BUSY\06REJ_COMP\05CHN_OPN"
#define x_PCL_RCSRBITS	"\20\20REJ\17RC_NPR\16RCV_WD\10LD_SILO\07IE\04DTI_RDY\03INH_ADI\02RC_INIT\01RCV_DAT"
