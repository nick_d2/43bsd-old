#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)if_il.h	7.1 (Berkeley) 6/5/86
 */

/*
 * Structure of an Ethernet header -- receive format
 */
struct	x_il_rheader {
	x_u_char	x_ilr_status;		/* Frame Status */
	x_u_char	x_ilr_fill1;
	x_u_short	x_ilr_length;		/* Frame Length */
	x_u_char	x_ilr_dhost[6];		/* Destination Host */
	x_u_char	x_ilr_shost[6];		/* Source Host */
	x_u_short	x_ilr_type;		/* Type of packet */
};

/*
 * Structure of statistics record
 */
struct	x_il_stats {
	x_u_short	x_ils_fill1;
	x_u_short	x_ils_length;		/* Length (should be 62) */
	x_u_char	x_ils_addr[6];		/* Ethernet Address */
	x_u_short	x_ils_frames;		/* Number of Frames Received */
	x_u_short	x_ils_rfifo;		/* Number of Frames in Receive FIFO */
	x_u_short	x_ils_xmit;		/* Number of Frames Transmitted */
	x_u_short	x_ils_xcollis;		/* Number of Excess Collisions */
	x_u_short	x_ils_frag;		/* Number of Fragments Received */
	x_u_short	x_ils_lost;		/* Number of Times Frames Lost */
	x_u_short	x_ils_multi;		/* Number of Multicasts Accepted */
	x_u_short	x_ils_rmulti;		/* Number of Multicasts Rejected */
	x_u_short	x_ils_crc;		/* Number of CRC Errors */
	x_u_short	x_ils_align;		/* Number of Alignment Errors */
	x_u_short	x_ils_collis;		/* Number of Collisions */
	x_u_short	x_ils_owcollis;		/* Number of Out-of-window Collisions */
	x_u_short	x_ils_fill2[8];
	char	x_ils_module[8];		/* Module ID */
	char	x_ils_firmware[8];	/* Firmware ID */
};

/*
 * Structure of Collision Delay Time Record
 */
struct	x_il_collis {
	x_u_short	x_ilc_fill1;
	x_u_short	x_ilc_length;		/* Length (should be 0-32) */
	x_u_short	x_ilc_delay[16];		/* Delay Times */
};
