#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)if_ilreg.h	7.1 (Berkeley) 6/5/86
 */

/*
 * Interlan Ethernet Communications Controller interface
 */
struct x_ildevice {
	x_short	x_il_csr;		/* Command and Status Register */
	x_short	x_il_bar;		/* Buffer Address Register */
	x_short	x_il_bcr;		/* Byte Count Register */
};

/*
 * Command and status bits
 */
#define	x_IL_EUA		0xc000		/* Extended Unibus Address */
#define	x_IL_CMD		0x3f00		/* Command Function Code */
#define	x_IL_CDONE	0x0080		/* Command Done */
#define	x_IL_CIE		0x0040		/* Command Interrupt Enable */
#define	x_IL_RDONE	0x0020		/* Receive DMA Done */
#define	x_IL_RIE		0x0010		/* Receive Interrupt Enable */
#define	x_IL_STATUS	0x000f		/* Command Status Code */

#define	x_IL_BITS		"\20\10CDONE\7CIE\6RDONE\5RIE"

/* command definitions */
#define	x_ILC_MLPBAK	0x0100		/* Set Module Interface Loopback Mode */
#define	x_ILC_ILPBAK	0x0200		/* Set Internal Loopback Mode */
#define	x_ILC_CLPBAK	0x0300		/* Clear Loopback Mode */
#define	x_ILC_PRMSC	0x0400		/* Set Promiscuous Receive Mode */
#define	x_ILC_CLPRMSC	0x0500		/* Clear Promiscuous Receive Mode */
#define	x_ILC_RCVERR	0x0600		/* Set Receive-On-Error Bit */
#define	x_ILC_CRCVERR	0x0700		/* Clear Receive-On-Error Bit */
#define	x_ILC_OFFLINE	0x0800		/* Go Offline */
#define	x_ILC_ONLINE	0x0900		/* Go Online */
#define	x_ILC_DIAG	0x0a00		/* Run On-board Diagnostics */
#define	x_ILC_ISA		0x0d00		/* Set Insert Source Address Mode */
#define	x_ILC_CISA	0x0e00		/* Clear Insert Source Address Mode */
#define	x_ILC_DEFPA	0x0f00		/* Set Physical Address to Default */
#define	x_ILC_ALLMC	0x1000		/* Set Receive All Multicast Packets */
#define	x_ILC_CALLMC	0x1100		/* Clear Receive All Multicast */
#define	x_ILC_STAT	0x1800		/* Report and Reset Statistics */
#define	x_ILC_DELAYS	0x1900		/* Report Collision Delay Times */
#define	x_ILC_RCV		0x2000		/* Supply Receive Buffer */
#define	x_ILC_LDXMIT	0x2800		/* Load Transmit Data */
#define	x_ILC_XMIT	0x2900		/* Load Transmit Data and Send */
#define	x_ILC_LDGRPS	0x2a00		/* Load Group Addresses */
#define	x_ILC_RMGRPS	0x2b00		/* Delete Group Addresses */
#define	x_ILC_LDPA	0x2c00		/* Load Physical Address */
#define	x_ILC_FLUSH	0x3000		/* Flush Receive BAR/BCR Queue */
#define	x_ILC_RESET	0x3f00		/* Reset */

/*
 * Error codes found in the status bits of the csr.
 */
#define	x_ILERR_SUCCESS		0	/* command successful */
#define	x_ILERR_RETRIES		1	/* " " with retries */
#define	x_ILERR_BADCMD		2	/* illegal command */
#define	x_ILERR_INVCMD		3	/* invalid command */
#define	x_ILERR_RECVERR		4	/* receiver error */
#define	x_ILERR_BUFSIZ		5	/* buffer size too big */
#define	x_ILERR_FRAMESIZ		6	/* frame size too small */
#define	x_ILERR_COLLISIONS	8	/* excessive collisions */
#define	x_ILERR_BUFALIGNMENT	10	/* buffer not word aligned */
#define	x_ILERR_NXM		15	/* non-existent memory */

#define	x_NILERRS			16
#ifdef x_ILERRS
char *x_ilerrs[x_NILERRS] = {
	"success",			/*  0 */
	"success with retries", 	/*  1 */
	"illegal command",		/*  2 */
	"inappropriate command",	/*  3 */
	"failure",			/*  4 */
	"buffer size exceeded",		/*  5 */
	"frame too small",		/*  6 */
	0,				/*  7 */
	"excessive collisions",		/*  8 */
	0,				/*  9 */
	"buffer alignment error",	/* 10 */
	0,				/* 11 */
	0,				/* 12 */
	0,				/* 13 */
	0,				/* 14 */
	"non-existent memory"		/* 15 */
};
#endif

/*
 * Diagnostics codes.
 */
#define	x_ILDIAG_SUCCESS		0	/* no problems */
#define	x_ILDIAG_CHKSUMERR	1	/* ROM/RAM checksum error */
#define	x_ILDIAG_DMAERR		2	/* DMA not working */
#define	x_ILDIAG_XMITERR		3	/* xmit circuitry failure */
#define	x_ILDIAG_RECVERR		4	/* rcvr circuitry failure */
#define	x_ILDIAG_LOOPBACK		5	/* loopback test failed */

#define	x_NILDIAGS		6
#ifdef x_ILDIAGS
char *x_ildiag[x_NILDIAGS] = {
	"success",			/* 0 */
	"checksum error",		/* 1 */
	"NM10 dma error",		/* 2 */
	"transmitter error",		/* 3 */
	"receiver error",		/* 4 */
	"loopback failure",		/* 5 */
};
#endif

/*
 * Frame status bits, returned in frame status byte
 * at the top of each received packet.
 */
#define	x_ILFSTAT_C	0x1		/* CRC error */
#define	x_ILFSTAT_A	0x2		/* alignment error */
#define	x_ILFSTAT_L	0x4		/* 1+ frames lost just before */
