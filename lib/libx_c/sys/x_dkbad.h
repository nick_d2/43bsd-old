#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)dkbad.h	7.1 (Berkeley) 6/4/86
 */

/*
 * Definitions needed to perform bad sector
 * revectoring ala DEC STD 144.
 *
 * The bad sector information is located in the
 * first 5 even numbered sectors of the last
 * track of the disk pack.  There are five
 * identical copies of the information, described
 * by the dkbad structure.
 *
 * Replacement sectors are allocated starting with
 * the first sector before the bad sector information
 * and working backwards towards the beginning of
 * the disk.  A maximum of 126 bad sectors are supported.
 * The position of the bad sector in the bad sector table
 * determines which replacement sector it corresponds to.
 *
 * The bad sector information and replacement sectors
 * are conventionally only accessible through the
 * 'c' file system partition of the disk.  If that
 * partition is used for a file system, the user is
 * responsible for making sure that it does not overlap
 * the bad sector information or any replacement sector.s
 */

struct x_dkbad {
	x_long	x_bt_csn;			/* cartridge serial number */
	x_u_short	x_bt_mbz;			/* unused; should be 0 */
	x_u_short	x_bt_flag;		/* -1 => alignment cartridge */
	struct x_bt_bad {
		x_u_short	x_bt_cyl;		/* cylinder number of bad sector */
		x_u_short	x_bt_trksec;	/* track and sector number */
	} x_bt_bad[126];
};

#define	x_ECC	0
#define	x_SSE	1
#define	x_BSE	2
#define	x_CONT	3
