#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)resource.h	7.1 (Berkeley) 6/4/86
 */

/*
 * Process priority specifications to get/setpriority.
 */
#define	x_PRIO_MIN	-20
#define	x_PRIO_MAX	20

#define	x_PRIO_PROCESS	0
#define	x_PRIO_PGRP	1
#define	x_PRIO_USER	2

/*
 * Resource utilization information.
 */

#define	x_RUSAGE_SELF	0
#define	x_RUSAGE_CHILDREN	-1

struct	x_rusage {
	struct x_timeval x_ru_utime;	/* user time used */
	struct x_timeval x_ru_stime;	/* system time used */
	x_long	x_ru_maxrss;
#define	x_ru_first	x_ru_ixrss
	x_long	x_ru_ixrss;		/* integral shared memory size */
	x_long	x_ru_idrss;		/* integral unshared data " */
	x_long	x_ru_isrss;		/* integral unshared stack " */
	x_long	x_ru_minflt;		/* page reclaims */
	x_long	x_ru_majflt;		/* page faults */
	x_long	x_ru_nswap;		/* swaps */
	x_long	x_ru_inblock;		/* block input operations */
	x_long	x_ru_oublock;		/* block output operations */
	x_long	x_ru_msgsnd;		/* messages sent */
	x_long	x_ru_msgrcv;		/* messages received */
	x_long	x_ru_nsignals;		/* signals received */
	x_long	x_ru_nvcsw;		/* voluntary context switches */
	x_long	x_ru_nivcsw;		/* involuntary " */
#define	x_ru_last		x_ru_nivcsw
};

/*
 * Resource limits
 */
#define	x_RLIMIT_CPU	0		/* cpu time in milliseconds */
#define	x_RLIMIT_FSIZE	1		/* maximum file size */
#define	x_RLIMIT_DATA	2		/* data size */
#define	x_RLIMIT_STACK	3		/* stack size */
#define	x_RLIMIT_CORE	4		/* core file size */
#define	x_RLIMIT_RSS	5		/* resident set size */

#define	x_RLIM_NLIMITS	6		/* number of resource limits */

#define	x_RLIM_INFINITY	0x7fffffff

struct x_rlimit {
	x_int	x_rlim_cur;		/* current (soft) limit */
	x_int	x_rlim_max;		/* maximum value for rlim_cur */
};
