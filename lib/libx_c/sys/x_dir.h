#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)dir.h	7.1 (Berkeley) 6/4/86
 */

/*
 * A directory consists of some number of blocks of DIRBLKSIZ
 * bytes, where DIRBLKSIZ is chosen such that it can be transferred
 * to disk in a single atomic operation (e.g. 512 bytes on most machines).
 *
 * Each DIRBLKSIZ byte block contains some number of directory entry
 * structures, which are of variable length.  Each directory entry has
 * a struct direct at the front of it, containing its inode number,
 * the length of the entry, and the length of the name contained in
 * the entry.  These are followed by the name padded to a 4 byte boundary
 * with null bytes.  All names are guaranteed null terminated.
 * The maximum length of a name in a directory is MAXNAMLEN.
 *
 * The macro DIRSIZ(dp) gives the amount of space required to represent
 * a directory entry.  Free space in a directory is represented by
 * entries which have dp->d_reclen > DIRSIZ(dp).  All DIRBLKSIZ bytes
 * in a directory block are claimed by the directory entries.  This
 * usually results in the last entry in a directory having a large
 * dp->d_reclen.  When entries are deleted from a directory, the
 * space is returned to the previous entry in the same directory
 * block by increasing its dp->d_reclen.  If the first entry of
 * a directory block is free, then its dp->d_ino is set to 0.
 * Entries other than the first in a directory do not normally have
 * dp->d_ino set to 0.
 */
/* so user programs can just include dir.h */
#if !defined(x_KERNEL) && !defined(x_DEV_BSIZE)
#define	x_DEV_BSIZE	512
#endif
#define x_DIRBLKSIZ	x_DEV_BSIZE
#define	x_MAXNAMLEN	255

struct	x_direct {
	x_u_long	x_d_ino;			/* inode number of entry */
	x_u_short	x_d_reclen;		/* length of this record */
	x_u_short	x_d_namlen;		/* length of string in d_name */
	char	x_d_name[x_MAXNAMLEN + 1];	/* name must be no longer than this */
};

/*
 * The DIRSIZ macro gives the minimum record length which will hold
 * the directory entry.  This requires the amount of space in struct direct
 * without the d_name field, plus enough space for the name with a terminating
 * null byte (dp->d_namlen+1), rounded up to a 4 byte boundary.
 */
#undef x_DIRSIZ
#define x_DIRSIZ(x_dp) \
    ((sizeof (struct x_direct) - (x_MAXNAMLEN+1)) + (((x_dp)->x_d_namlen+1 + 3) &~ 3))

#ifndef x_KERNEL
/*
 * Definitions for library routines operating on directories.
 */
typedef struct x__dirdesc {
	x_int	x_dd_fd;
	x_long	x_dd_loc;
	x_long	x_dd_size;
	char	x_dd_buf[x_DIRBLKSIZ];
} x_DIR;
#ifndef x_NULL
#define x_NULL 0
#endif
extern	x_DIR *x_opendir();
extern	struct x_direct *x_readdir();
extern	x_long x_telldir();
extern	void x_seekdir();
#define x_rewinddir(x_dirp)	x_seekdir((x_dirp), (x_long)0)
extern	void x_closedir();
#endif

#ifdef x_KERNEL
/*
 * Template for manipulating directories.
 * Should use struct direct's, but the name field
 * is MAXNAMLEN - 1, and this just won't do.
 */
struct x_dirtemplate {
	x_u_long	x_dot_ino;
	x_short	x_dot_reclen;
	x_short	x_dot_namlen;
	char	x_dot_name[4];		/* must be multiple of 4 */
	x_u_long	x_dotdot_ino;
	x_short	x_dotdot_reclen;
	x_short	x_dotdot_namlen;
	char	x_dotdot_name[4];		/* ditto */
};
#endif

#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

/* gen/readdir.c */
struct x_direct *x_readdir __P((register x_DIR *x_dirp));
/* gen/opendir.c */
x_DIR *x_opendir __P((char *x_name));
/* gen/closedir.c */
void x_closedir __P((register x_DIR *x_dirp));
/* gen/scandir.c */
x_int x_scandir __P((char *x_dirname, struct x_direct *(*x_namelist[]), x_int (*x_select)(void), x_int (*x_dcomp)(void)));
x_int x_alphasort __P((struct x_direct **x_d1, struct x_direct **x_d2));
/* gen/telldir.c */
x_long x_telldir __P((x_DIR *x_dirp));
/* gen/seekdir.c */
void x_seekdir __P((register x_DIR *x_dirp, x_long x_loc));
