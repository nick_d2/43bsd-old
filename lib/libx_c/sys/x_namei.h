#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)namei.h	7.1 (Berkeley) 6/4/86
 */

#ifndef x__NAMEI_
#define	x__NAMEI_

#ifdef x_KERNEL
#include "x_uio.h"
#else
#include <sys/x_uio.h>
#endif

/*
 * Encapsulation of namei parameters.
 * One of these is located in the u. area to
 * minimize space allocated on the kernel stack.
 */
struct x_nameidata {
	x_caddr_t	x_ni_dirp;		/* pathname pointer */
	x_short	x_ni_nameiop;		/* see below */
	x_short	x_ni_error;		/* error return if any */
	x_off_t	x_ni_endoff;		/* end of useful stuff in directory */
	struct	x_inode *x_ni_pdir;		/* inode of parent directory of dirp */
	struct	x_iovec x_ni_iovec;		/* MUST be pointed to by ni_iov */
	struct	x_uio x_ni_uio;		/* directory I/O parameters */
	struct	x_direct x_ni_dent;		/* current directory entry */
};

#define	x_ni_base		x_ni_iovec.x_iov_base
#define	x_ni_count	x_ni_iovec.x_iov_len
#define	x_ni_iov		x_ni_uio.x_uio_iov
#define	x_ni_iovcnt	x_ni_uio.x_uio_iovcnt
#define	x_ni_offset	x_ni_uio.x_uio_offset
#define	x_ni_segflg	x_ni_uio.x_uio_segflg
#define	x_ni_resid	x_ni_uio.x_uio_resid

/*
 * namei operations and modifiers
 */
#define	x_LOOKUP		0	/* perform name lookup only */
#define	x_CREATE		1	/* setup for file creation */
#define	x_DELETE		2	/* setup for file deletion */
#define	x_LOCKPARENT	0x10	/* see the top of namei */
#define x_NOCACHE		0x20	/* name must not be left in cache */
#define x_FOLLOW		0x40	/* follow symbolic links */
#define	x_NOFOLLOW	0x0	/* don't follow symbolic links (pseudo) */

/*
 * This structure describes the elements in the cache of recent
 * names looked up by namei.
 */
struct	x_namecache {
	struct	x_namecache *x_nc_forw;	/* hash chain, MUST BE FIRST */
	struct	x_namecache *x_nc_back;	/* hash chain, MUST BE FIRST */
	struct	x_namecache *x_nc_nxt;	/* LRU chain */
	struct	x_namecache **x_nc_prev;	/* LRU chain */
	struct	x_inode *x_nc_ip;		/* inode the name refers to */
	x_ino_t	x_nc_ino;			/* ino of parent of name */
	x_dev_t	x_nc_dev;			/* dev of parent of name */
	x_dev_t	x_nc_idev;		/* dev of the name ref'd */
	x_long	x_nc_id;			/* referenced inode's id */
	char	x_nc_nlen;		/* length of name */
#define	x_NCHNAMLEN	15	/* maximum name segment length we bother with */
	char	x_nc_name[x_NCHNAMLEN];	/* segment name */
};
#ifdef x_KERNEL
struct	x_namecache *x_namecache;
x_int	x_nchsize;
#endif

/*
 * Stats on usefulness of namei caches.
 */
struct	x_nchstats {
	x_long	x_ncs_goodhits;		/* hits that we can reall use */
	x_long	x_ncs_badhits;		/* hits we must drop */
	x_long	x_ncs_falsehits;		/* hits with id mismatch */
	x_long	x_ncs_miss;		/* misses */
	x_long	x_ncs_long;		/* long names that ignore cache */
	x_long	x_ncs_pass2;		/* names found with passes == 2 */
	x_long	x_ncs_2passes;		/* number of times we attempt it */
};
#endif
