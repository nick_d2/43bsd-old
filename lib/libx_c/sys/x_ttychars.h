#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)ttychars.h	7.1 (Berkeley) 6/4/86
 */

/*
 * User visible structures and constants
 * related to terminal handling.
 */
#ifndef x__TTYCHARS_
#define	x__TTYCHARS_
struct x_ttychars {
	char	x_tc_erase;	/* erase last character */
	char	x_tc_kill;	/* erase entire line */
	char	x_tc_intrc;	/* interrupt */
	char	x_tc_quitc;	/* quit */
	char	x_tc_startc;	/* start output */
	char	x_tc_stopc;	/* stop output */
	char	x_tc_eofc;	/* end-of-file */
	char	x_tc_brkc;	/* input delimiter (like nl) */
	char	x_tc_suspc;	/* stop process signal */
	char	x_tc_dsuspc;	/* delayed stop process signal */
	char	x_tc_rprntc;	/* reprint line */
	char	x_tc_flushc;	/* flush output (toggles) */
	char	x_tc_werasc;	/* word erase */
	char	x_tc_lnextc;	/* literal next character */
};

#define	x_CTRL(x_c)	('c'&037)

/* default special characters */
#define	x_CERASE	0177
#define	x_CKILL	x_CTRL(x_u)
#define	x_CINTR	x_CTRL(x_c)
#define	x_CQUIT	034		/* FS, ^\ */
#define	x_CSTART	x_CTRL(x_q)
#define	x_CSTOP	x_CTRL(x_s)
#define	x_CEOF	x_CTRL(x_d)
#define	x_CEOT	x_CEOF
#define	x_CBRK	0377
#define	x_CSUSP	x_CTRL(x_z)
#define	x_CDSUSP	x_CTRL(x_y)
#define	x_CRPRNT	x_CTRL(x_r)
#define	x_CFLUSH	x_CTRL(x_o)
#define	x_CWERASE	x_CTRL(x_w)
#define	x_CLNEXT	x_CTRL(x_v)
#endif
