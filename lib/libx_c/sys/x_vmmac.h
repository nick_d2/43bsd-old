#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)vmmac.h	7.1 (Berkeley) 6/4/86
 */

/*
 * Virtual memory related conversion macros
 */

/* Core clicks to number of pages of page tables needed to map that much */
#define	x_ctopt(x_x)	(((x_x)+x_NPTEPG-1)/x_NPTEPG)

#ifdef x_vax
/* Virtual page numbers to text|data|stack segment page numbers and back */
#define	x_vtotp(x_p, x_v)	((x_int)(x_v))
#define	x_vtodp(x_p, x_v)	((x_int)((x_v) - x_stoc(x_ctos((x_p)->x_p_tsize))))
#define	x_vtosp(x_p, x_v)	((x_int)(x_BTOPUSRSTACK - 1 - (x_v)))
#define	x_tptov(x_p, x_i)	((unsigned)(x_i))
#define	x_dptov(x_p, x_i)	((unsigned)(x_stoc(x_ctos((x_p)->x_p_tsize)) + (x_i)))
#define	x_sptov(x_p, x_i)	((unsigned)(x_BTOPUSRSTACK - 1 - (x_i)))

/* Tell whether virtual page numbers are in text|data|stack segment */
#define	x_isassv(x_p, x_v)	((x_v) >= x_BTOPUSRSTACK - (x_p)->x_p_ssize)
#define	x_isatsv(x_p, x_v)	((x_v) < (x_p)->x_p_tsize)
#define	x_isadsv(x_p, x_v)	((x_v) >= x_stoc(x_ctos((x_p)->x_p_tsize)) && \
	(x_v) < (x_p)->x_p_tsize + (x_p)->x_p_dsize)
#else
/* Virtual page numbers to text|data|stack segment page numbers and back */
#define	x_vtotp(x_p, x_v)	((x_int)(x_v)-x_LOWPAGES)
#define	x_vtodp(x_p, x_v)	((x_int)((x_v) - x_stoc(x_ctos((x_p)->x_p_tsize)) - x_LOWPAGES))
#define	x_vtosp(x_p, x_v)	((x_int)(x_BTOPUSRSTACK - 1 - (x_v)))
#define	x_tptov(x_p, x_i)	((unsigned)(x_i) + x_LOWPAGES)
#define	x_dptov(x_p, x_i)	((unsigned)(x_stoc(x_ctos((x_p)->x_p_tsize)) + (x_i) + x_LOWPAGES))
#define	x_sptov(x_p, x_i)	((unsigned)(x_BTOPUSRSTACK - 1 - (x_i)))

/* Tell whether virtual page numbers are in text|data|stack segment */
#define	x_isassv(x_p, x_v)	((x_v) >= x_BTOPUSRSTACK - (x_p)->x_p_ssize)
#define	x_isatsv(x_p, x_v)	(((x_v) - x_LOWPAGES) < (x_p)->x_p_tsize)
#define	x_isadsv(x_p, x_v)	(((x_v) - x_LOWPAGES) >= x_stoc(x_ctos((x_p)->x_p_tsize)) && \
				!x_isassv(x_p, x_v))
#endif

/* Tell whether pte's are text|data|stack */
#define	x_isaspte(x_p, x_pte)		((x_pte) > x_sptopte(x_p, (x_p)->x_p_ssize))
#define	x_isatpte(x_p, x_pte)		((x_pte) < x_dptopte(x_p, 0))
#define	x_isadpte(x_p, x_pte)		(!x_isaspte(x_p, x_pte) && !x_isatpte(x_p, x_pte))

/* Text|data|stack pte's to segment page numbers and back */
#define	x_ptetotp(x_p, x_pte)		((x_pte) - (x_p)->x_p_p0br)
#define	x_ptetodp(x_p, x_pte)		(((x_pte) - (x_p)->x_p_p0br) - (x_p)->x_p_tsize)
#define	x_ptetosp(x_p, x_pte)		(((x_p)->x_p_addr - (x_pte)) - 1)

#define	x_tptopte(x_p, x_i)		((x_p)->x_p_p0br + (x_i))
#define	x_dptopte(x_p, x_i)		((x_p)->x_p_p0br + ((x_p)->x_p_tsize + (x_i)))
#define	x_sptopte(x_p, x_i)		((x_p)->x_p_addr - (1 + (x_i)))

/* Convert a virtual page number to a pte address. */
#define x_vtopte(x_p, x_v) \
	(((x_v) < (x_p)->x_p_tsize + (x_p)->x_p_dsize) ? ((x_p)->x_p_p0br + (x_v)) : \
	((x_p)->x_p_addr - (x_BTOPUSRSTACK - (x_v))))
#ifdef x_notdef
struct	x_pte *x_vtopte();
#endif

/* Bytes to pages without rounding, and back */
#define	x_btop(x_x)		(((unsigned)(x_x)) >> x_PGSHIFT)
#define	x_ptob(x_x)		((x_caddr_t)((x_x) << x_PGSHIFT))

/* Turn virtual addresses into kernel map indices */
#define	x_kmxtob(x_a)	(x_usrpt + (x_a) * x_NPTEPG)
#define	x_btokmx(x_b)	(((x_b) - x_usrpt) / x_NPTEPG)

/* User area address and pcb bases */
#define	x_uaddr(x_p)	(&((x_p)->x_p_p0br[(x_p)->x_p_szpt * x_NPTEPG - x_UPAGES]))
#ifdef x_vax
#define	x_pcbb(x_p)		((x_p)->x_p_addr[0].x_pg_pfnum)
#endif

/* Average new into old with aging factor time */
#define	x_ave(x_smooth, x_cnt, x_time) \
	x_smooth = ((x_time - 1) * (x_smooth) + (x_cnt)) / (x_time)

/* Abstract machine dependent operations */
#ifdef x_vax
#define	x_setp0br(x_x)	(x_u.x_u_pcb.x_pcb_p0br = (x_x), x_mtpr(x_P0BR, x_x))
#define	x_setp0lr(x_x)	(x_u.x_u_pcb.x_pcb_p0lr = \
			    (x_x) | (x_u.x_u_pcb.x_pcb_p0lr & x_AST_CLR), \
			 x_mtpr(x_P0LR, x_x))
#define	x_setp1br(x_x)	(x_u.x_u_pcb.x_pcb_p1br = (x_x), x_mtpr(x_P1BR, x_x))
#define	x_setp1lr(x_x)	(x_u.x_u_pcb.x_pcb_p1lr = (x_x), x_mtpr(x_P1LR, x_x))
#define	x_initp1br(x_x)	((x_x) - x_P1PAGES)
#endif

#define	x_outofmem()	x_wakeup((x_caddr_t)&x_proc[2]);

/*
 * Page clustering macros.
 * 
 * dirtycl(pte)			is the page cluster dirty?
 * anycl(pte,fld)		does any pte in the cluster has fld set?
 * zapcl(pte,fld) = val		set all fields fld in the cluster to val
 * distcl(pte)			distribute high bits to cluster; note that
 *				distcl copies everything but pg_pfnum,
 *				INCLUDING pg_m!!!
 *
 * In all cases, pte must be the low pte in the cluster, even if
 * the segment grows backwards (e.g. the stack).
 */
#define	x_H(x_pte)	((struct x_hpte *)(x_pte))

#if x_CLSIZE==1
#define	x_dirtycl(x_pte)	x_dirty(x_pte)
#define	x_anycl(x_pte,x_fld)	((x_pte)->x_fld)
#define	x_zapcl(x_pte,x_fld)	(x_pte)->x_fld
#define	x_distcl(x_pte)
#endif

#if x_CLSIZE==2
#define	x_dirtycl(x_pte)	(x_dirty(x_pte) || x_dirty((x_pte)+1))
#define	x_anycl(x_pte,x_fld)	((x_pte)->x_fld || (((x_pte)+1)->x_fld))
#define	x_zapcl(x_pte,x_fld)	(x_pte)[1].x_fld = (x_pte)[0].x_fld
#endif

#if x_CLSIZE==4
#define	x_dirtycl(x_pte) \
    (x_dirty(x_pte) || x_dirty((x_pte)+1) || x_dirty((x_pte)+2) || x_dirty((x_pte)+3))
#define	x_anycl(x_pte,x_fld) \
    ((x_pte)->x_fld || (((x_pte)+1)->x_fld) || (((x_pte)+2)->x_fld) || (((x_pte)+3)->x_fld))
#define	x_zapcl(x_pte,x_fld) \
    (x_pte)[3].x_fld = (x_pte)[2].x_fld = (x_pte)[1].x_fld = (x_pte)[0].x_fld
#endif

#ifndef x_distcl
#define	x_distcl(x_pte)	x_zapcl(x_H(x_pte),x_pg_high)
#endif

/*
 * Lock a page frame.
 */
#define x_MLOCK(x_c) { \
	while ((x_c)->x_c_lock) { \
		(x_c)->x_c_want = 1; \
		x_sleep((x_caddr_t)(x_c), x_PSWP+1); \
	} \
	(x_c)->x_c_lock = 1; \
}
/*
 * Unlock a page frame.
 */
#define x_MUNLOCK(x_c) { \
	if (x_c->x_c_want) { \
		x_wakeup((x_caddr_t)x_c); \
		x_c->x_c_want = 0; \
	} \
	x_c->x_c_lock = 0; \
}
