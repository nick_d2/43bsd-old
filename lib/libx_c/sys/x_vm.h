#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)vm.h	7.1 (Berkeley) 6/4/86
 */

/*
 *	#include "../h/vm.h"
 * or	#include <vm.h>		 in a user program
 * is a quick way to include all the vm header files.
 */
#ifdef x_KERNEL
#include "x_vmparam.h"
#include "x_vmmac.h"
#include "x_vmmeter.h"
#include "x_vmsystm.h"
#else
#include <sys/x_vmparam.h>
#include <sys/x_vmmac.h>
#include <sys/x_vmmeter.h>
#include <sys/x_vmsystm.h>
#endif
