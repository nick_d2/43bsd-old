#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)mbuf.h	7.3 (Berkeley) 9/11/86
 */

/*
 * Constants related to memory allocator.
 */
#define	x_MSIZE		128			/* size of an mbuf */

#define	x_MMINOFF		12			/* mbuf header length */
#define	x_MTAIL		4
#define	x_MMAXOFF		(x_MSIZE-x_MTAIL)		/* offset where data ends */
#define	x_MLEN		(x_MSIZE-x_MMINOFF-x_MTAIL)	/* mbuf data length */
#define	x_NMBCLUSTERS	256
#define	x_NMBPCL		(x_CLBYTES/x_MSIZE)		/* # mbufs per cluster */

/*
 * Macros for type conversion
 */

/* network cluster number to virtual address, and back */
#define	x_cltom(x_x) ((struct x_mbuf *)((x_int)x_mbutl + ((x_x) << x_CLSHIFT)))
#define	x_mtocl(x_x) (((x_int)x_x - (x_int)x_mbutl) >> x_CLSHIFT)

/* address in mbuf to mbuf head */
#define	x_dtom(x_x)		((struct x_mbuf *)((x_int)x_x & ~(x_MSIZE-1)))

/* mbuf head, to typed data */
#define	x_mtod(x_x,x_t)	((x_t)((x_int)(x_x) + (x_x)->x_m_off))

struct x_mbuf {
	struct	x_mbuf *x_m_next;		/* next buffer in chain */
	x_u_long	x_m_off;			/* offset of data */
	x_short	x_m_len;			/* amount of data in this mbuf */
	x_short	x_m_type;			/* mbuf type (0 == free) */
	x_u_char	x_m_dat[x_MLEN];		/* data storage */
	struct	x_mbuf *x_m_act;		/* link in higher-level mbuf list */
};

/* mbuf types */
#define	x_MT_FREE		0	/* should be on free list */
#define	x_MT_DATA		1	/* dynamic (data) allocation */
#define	x_MT_HEADER	2	/* packet header */
#define	x_MT_SOCKET	3	/* socket structure */
#define	x_MT_PCB		4	/* protocol control block */
#define	x_MT_RTABLE	5	/* routing tables */
#define	x_MT_HTABLE	6	/* IMP host tables */
#define	x_MT_ATABLE	7	/* address resolution tables */
#define	x_MT_SONAME	8	/* socket name */
#define	x_MT_ZOMBIE	9	/* zombie proc status */
#define	x_MT_SOOPTS	10	/* socket options */
#define	x_MT_FTABLE	11	/* fragment reassembly header */
#define	x_MT_RIGHTS	12	/* access rights */
#define	x_MT_IFADDR	13	/* interface address */

/* flags to m_get */
#define	x_M_DONTWAIT	0
#define	x_M_WAIT		1

/* flags to m_pgalloc */
#define	x_MPG_MBUFS	0		/* put new mbufs on free list */
#define	x_MPG_CLUSTERS	1		/* put new clusters on free list */
#define	x_MPG_SPACE	2		/* don't free; caller wants space */

/* length to m_copy to copy all */
#define	x_M_COPYALL	1000000000

/*
 * m_pullup will pull up additional length if convenient;
 * should be enough to hold headers of second-level and higher protocols. 
 */
#define	x_MPULL_EXTRA	32

#define	x_MGET(x_m, x_i, x_t) \
	{ x_int x_ms = x_splimp(); \
	  if ((x_m)=x_mfree) \
		{ if ((x_m)->x_m_type != x_MT_FREE) x_panic("mget"); (x_m)->x_m_type = x_t; \
		  x_mbstat.x_m_mtypes[x_MT_FREE]--; x_mbstat.x_m_mtypes[x_t]++; \
		  x_mfree = (x_m)->x_m_next; (x_m)->x_m_next = 0; \
		  (x_m)->x_m_off = x_MMINOFF; } \
	  else \
		(x_m) = x_m_more(x_i, x_t); \
	  x_splx(x_ms); }
/*
 * Mbuf page cluster macros.
 * MCLALLOC allocates mbuf page clusters.
 * Note that it works only with a count of 1 at the moment.
 * MCLGET adds such clusters to a normal mbuf.
 * m->m_len is set to CLBYTES upon success.
 * MCLFREE frees clusters allocated by MCLALLOC.
 */
#define	x_MCLALLOC(x_m, x_i) \
	{ x_int x_ms = x_splimp(); \
	  if (x_mclfree == 0) \
		(void)x_m_clalloc((x_i), x_MPG_CLUSTERS, x_M_DONTWAIT); \
	  if ((x_m)=x_mclfree) \
	     {++x_mclrefcnt[x_mtocl(x_m)];x_mbstat.x_m_clfree--;x_mclfree = (x_m)->x_m_next;} \
	  x_splx(x_ms); }
#define	x_M_HASCL(x_m)	((x_m)->x_m_off >= x_MSIZE)
#define	x_MTOCL(x_m)	((struct x_mbuf *)(x_mtod((x_m), x_int)&~x_CLOFSET))

#define	x_MCLGET(x_m) \
	{ struct x_mbuf *x_p; \
	  x_MCLALLOC(x_p, 1); \
	  if (x_p) { \
		(x_m)->x_m_off = (x_int)x_p - (x_int)(x_m); \
		(x_m)->x_m_len = x_CLBYTES; \
	  } \
	}
#define	x_MCLFREE(x_m) { \
	if (--x_mclrefcnt[x_mtocl(x_m)] == 0) \
	    { (x_m)->x_m_next = x_mclfree;x_mclfree = (x_m);x_mbstat.x_m_clfree++;} \
	}
#define	x_MFREE(x_m, x_n) \
	{ x_int x_ms = x_splimp(); \
	  if ((x_m)->x_m_type == x_MT_FREE) x_panic("mfree"); \
	  x_mbstat.x_m_mtypes[(x_m)->x_m_type]--; x_mbstat.x_m_mtypes[x_MT_FREE]++; \
	  (x_m)->x_m_type = x_MT_FREE; \
	  if (x_M_HASCL(x_m)) { \
		(x_n) = x_MTOCL(x_m); \
		x_MCLFREE(x_n); \
	  } \
	  (x_n) = (x_m)->x_m_next; (x_m)->x_m_next = x_mfree; \
	  (x_m)->x_m_off = 0; (x_m)->x_m_act = 0; x_mfree = (x_m); \
	  x_splx(x_ms); \
	  if (x_m_want) { \
		  x_m_want = 0; \
		  x_wakeup((x_caddr_t)&x_mfree); \
	  } \
	}

/*
 * Mbuf statistics.
 */
struct x_mbstat {
	x_short	x_m_mbufs;	/* mbufs obtained from page pool */
	x_short	x_m_clusters;	/* clusters obtained from page pool */
	x_short	x_m_clfree;	/* free clusters */
	x_short	x_m_drops;	/* times failed to find space */
	x_short	x_m_mtypes[256];	/* type specific mbuf allocations */
};

#ifdef	x_KERNEL
extern	struct x_mbuf x_mbutl[];		/* virtual address of net free mem */
extern	struct x_pte x_Mbmap[];		/* page tables to map Netutl */
struct	x_mbstat x_mbstat;
x_int	x_nmbclusters;
struct	x_mbuf *x_mfree, *x_mclfree;
char	x_mclrefcnt[x_NMBCLUSTERS + 1];
x_int	x_m_want;
struct	x_mbuf *x_m_get(),*x_m_getclr(),*x_m_free(),*x_m_more(),*x_m_copy(),*x_m_pullup();
x_caddr_t	x_m_clalloc();
#endif
