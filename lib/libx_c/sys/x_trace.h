#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)trace.h	7.1 (Berkeley) 6/4/86
 */

/*
 * File system buffer tracing points; all trace <pack(dev, size), bn>
 */
#define	x_TR_BREADHIT	0	/* buffer read found in cache */
#define	x_TR_BREADMISS	1	/* buffer read not in cache */
#define	x_TR_BWRITE	2	/* buffer written */
#define	x_TR_BREADHITRA	3	/* buffer read-ahead found in cache */
#define	x_TR_BREADMISSRA	4	/* buffer read-ahead not in cache */
#define	x_TR_XFODMISS	5	/* exe fod read */
#define	x_TR_XFODHIT	6	/* exe fod read */
#define	x_TR_BRELSE	7	/* brelse */
#define	x_TR_BREALLOC	8	/* expand/contract a buffer */

/*
 * Memory allocator trace points; all trace the amount of memory involved
 */
#define	x_TR_MALL		10	/* memory allocated */

/*
 * Paging trace points: all are <vaddr, pid>
 */
#define	x_TR_INTRANS	20	/* page intransit block */
#define	x_TR_EINTRANS	21	/* page intransit wait done */
#define	x_TR_FRECLAIM	22	/* reclaim from free list */
#define	x_TR_RECLAIM	23	/* reclaim from loop */
#define	x_TR_XSFREC	24	/* reclaim from free list instead of drum */
#define	x_TR_XIFREC	25	/* reclaim from free list instead of fsys */
#define	x_TR_WAITMEM	26	/* wait for memory in pagein */
#define	x_TR_EWAITMEM	27	/* end memory wait in pagein */
#define	x_TR_ZFOD		28	/* zfod page fault */
#define	x_TR_EXFOD	29	/* exec fod page fault */
#define	x_TR_VRFOD	30	/* vread fod page fault */
#define	x_TR_CACHEFOD	31	/* fod in file system cache */
#define	x_TR_SWAPIN	32	/* drum page fault */
#define	x_TR_PGINDONE	33	/* page in done */
#define	x_TR_SWAPIO	34	/* swap i/o request arrives */

/*
 * System call trace points.
 */
#define	x_TR_VADVISE	40	/* vadvise occurred with <arg, pid> */

/*
 * Miscellaneous
 */
#define	x_TR_STAMP	45	/* user said vtrace(VTR_STAMP, value); */

/*
 * This defines the size of the trace flags array.
 */
#define	x_TR_NFLAGS	100	/* generous */

#define	x_TRCSIZ		4096

/*
 * Specifications of the vtrace() system call, which takes one argument.
 */
#define	x_VTRACE		64+51

#define	x_VTR_DISABLE	0		/* set a trace flag to 0 */
#define	x_VTR_ENABLE	1		/* set a trace flag to 1 */
#define	x_VTR_VALUE	2		/* return value of a trace flag */
#define	x_VTR_UALARM	3		/* set alarm to go off (sig 16) */
					/* in specified number of hz */
#define	x_VTR_STAMP	4		/* user specified stamp */
#ifdef x_KERNEL
#ifdef x_TRACE
char	x_traceflags[x_TR_NFLAGS];
struct	x_proc *x_traceproc;
x_int	x_tracebuf[x_TRCSIZ];
x_unsigned_int x_tracex;
x_int	x_tracewhich;
#define	x_pack(x_a,x_b)	((x_a)<<16)|(x_b)
#define	x_trace(x_a,x_b,x_c)	if (x_traceflags[x_a]) x_trace1(x_a,x_b,x_c)
#else
#define	x_trace(x_a,x_b,x_b)	;
#endif
#endif
