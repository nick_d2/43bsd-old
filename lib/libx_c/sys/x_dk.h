#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)dk.h	7.1 (Berkeley) 6/4/86
 */

/*
 * Instrumentation
 */
#define	x_CPUSTATES	4

#define	x_CP_USER		0
#define	x_CP_NICE		1
#define	x_CP_SYS		2
#define	x_CP_IDLE		3

#define	x_DK_NDRIVE	4

#ifdef x_KERNEL
x_long	x_cp_time[x_CPUSTATES];
x_int	x_dk_ndrive;
x_int	x_dk_busy;
x_long	x_dk_time[x_DK_NDRIVE];
x_long	x_dk_seek[x_DK_NDRIVE];
x_long	x_dk_xfer[x_DK_NDRIVE];
x_long	x_dk_wds[x_DK_NDRIVE];
float	x_dk_mspw[x_DK_NDRIVE];

x_long	x_tk_nin;
x_long	x_tk_nout;
#endif
