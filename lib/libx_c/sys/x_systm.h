#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)systm.h	7.1 (Berkeley) 6/4/86
 */

/*
 * Random set of variables
 * used by more than one
 * routine.
 */
extern	char x_version[];		/* system version */

/*
 * Nblkdev is the number of entries
 * (rows) in the block switch. It is
 * set in binit/bio.c by making
 * a pass over the switch.
 * Used in bounds checking on major
 * device numbers.
 */
x_int	x_nblkdev;

/*
 * Number of character switch entries.
 * Set by cinit/prim.c
 */
x_int	x_nchrdev;

x_int	x_nswdev;			/* number of swap devices */
x_int	x_mpid;			/* generic for unique process id's */
char	x_runin;			/* scheduling flag */
char	x_runout;			/* scheduling flag */
x_int	x_runrun;			/* scheduling flag */
char	x_kmapwnt;		/* kernel map want flag */
char	x_curpri;			/* more scheduling */

x_int	x_maxmem;			/* actual max memory per process */
x_int	x_physmem;		/* physical memory on this CPU */

x_int	x_nswap;			/* size of swap space */
x_int	x_updlock;		/* lock for sync */
x_daddr_t	x_rablock;		/* block to be read ahead */
x_int	x_rasize;			/* size of block in rablock */
extern	x_int x_intstack[];		/* stack for interrupts */
x_dev_t	x_rootdev;		/* device of the root */
x_dev_t	x_dumpdev;		/* device to take dumps on */
x_long	x_dumplo;			/* offset into dumpdev */
x_dev_t	x_swapdev;		/* swapping device */
x_dev_t	x_argdev;			/* device for argument lists */

#ifdef x_vax
extern	x_int x_icode[];		/* user init code */
extern	x_int x_szicode;		/* its size */
#endif

x_daddr_t	x_bmap();
x_caddr_t	x_calloc();
x_int	x_memall();
x_int	x_vmemall();
x_caddr_t	x_wmemall();
x_swblk_t	x_vtod();

/*
 * Structure of the system-entry table
 */
extern struct x_sysent
{
	x_int	x_sy_narg;		/* total number of arguments */
	x_int	(*x_sy_call)();		/* handler */
} x_sysent[];

x_int	x_noproc;			/* no one is running just now */
char	*x_panicstr;
x_int	x_wantin;
x_int	x_boothowto;		/* reboot flags, from console subsystem */
x_int	x_selwait;

extern	char x_vmmap[];		/* poor name! */

/* casts to keep lint happy */
#define	x_insque(x_q,x_p)	x__insque((x_caddr_t)x_q,(x_caddr_t)x_p)
#define	x_remque(x_q)	x__remque((x_caddr_t)x_q)
