#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)quota.h	7.1 (Berkeley) 6/4/86
 */

/*
 * MELBOURNE DISC QUOTAS
 *
 * Various junk to do with various quotas (etc) imposed upon
 * the average user (big brother finally hits UNIX).
 *
 * The following structure exists in core for each logged on user.
 * It contains global junk relevant to that user's quotas.
 *
 * The u_quota field of each user struct contains a pointer to
 * the quota struct relevant to the current process, this is changed
 * by 'setuid' sys call, &/or by the Q_SETUID quota() call.
 */
struct x_quota {
	struct	x_quota *x_q_forw, *x_q_back;	/* hash chain, MUST be first */
	x_short	x_q_cnt;			/* ref count (# processes) */
	x_uid_t	x_q_uid;			/* real uid of owner */
	x_int	x_q_flags;		/* struct management flags */
#define	x_Q_LOCK	0x01		/* quota struct locked (for disc i/o) */
#define	x_Q_WANT	0x02		/* issue a wakeup when lock goes off */
#define	x_Q_NEW	0x04		/* new quota - no proc1 msg sent yet */
#define	x_Q_NDQ	0x08		/* account has NO disc quota */
	struct	x_quota *x_q_freef, **x_q_freeb;
	struct	x_dquot *x_q_dq[x_NMOUNT];	/* disc quotas for mounted filesys's */
};

#define	x_NOQUOTA	((struct x_quota *) 0)

#if defined(x_KERNEL) && defined(x_QUOTA)
struct	x_quota *x_quota, *x_quotaNQUOTA;
x_int	x_nquota;
struct	x_quota *x_getquota(), *x_qfind();
#endif

/*
 * The following structure defines the format of the disc quota file
 * (as it appears on disc) - the file is an array of these structures
 * indexed by user number.  The setquota sys call establishes the inode
 * for each quota file (a pointer is retained in the mount structure).
 *
 * The following constants define the number of warnings given a user
 * before the soft limits are treated as hard limits (usually resulting
 * in an allocation failure).  The warnings are normally manipulated
 * each time a user logs in through the Q_DOWARN quota call.  If
 * the user logs in and is under the soft limit the warning count
 * is reset to MAX_*_WARN, otherwise a message is printed and the
 * warning count is decremented.  This makes MAX_*_WARN equivalent to
 * the number of logins before soft limits are treated as hard limits.
 */
#define	x_MAX_IQ_WARN	3
#define	x_MAX_DQ_WARN	3

struct	x_dqblk {
	x_u_long	x_dqb_bhardlimit;	/* absolute limit on disc blks alloc */
	x_u_long	x_dqb_bsoftlimit;	/* preferred limit on disc blks */
	x_u_long	x_dqb_curblocks;	/* current block count */
	x_u_short	x_dqb_ihardlimit;	/* maximum # allocated inodes + 1 */
	x_u_short	x_dqb_isoftlimit;	/* preferred inode limit */
	x_u_short	x_dqb_curinodes;	/* current # allocated inodes */
	x_u_char	x_dqb_bwarn;	/* # warnings left about excessive disc use */
	x_u_char	x_dqb_iwarn;	/* # warnings left about excessive inodes */
};

/*
 * The following structure records disc usage for a user on a filesystem.
 * There is one allocated for each quota that exists on any filesystem
 * for the current user. A cache is kept of other recently used entries.
 */
struct	x_dquot {
	struct	x_dquot *x_dq_forw, *x_dq_back;/* MUST be first entry */
	union	{
		struct	x_quota *x_Dq_own;	/* the quota that points to this */
		struct {		/* free list */
			struct	x_dquot *x_Dq_freef, **x_Dq_freeb;
		} x_dq_f;
	} x_dq_u;
	x_short	x_dq_flags;
#define	x_DQ_LOCK		0x01		/* this quota locked (no MODS) */
#define	x_DQ_WANT		0x02		/* wakeup on unlock */
#define	x_DQ_MOD		0x04		/* this quota modified since read */
#define	x_DQ_FAKE		0x08		/* no limits here, just usage */
#define	x_DQ_BLKS		0x10		/* has been warned about blk limit */
#define	x_DQ_INODS	0x20		/* has been warned about inode limit */
	x_short	x_dq_cnt;			/* count of active references */
	x_uid_t	x_dq_uid;			/* user this applies to */
	x_dev_t	x_dq_dev;			/* filesystem this relates to */
	struct x_dqblk x_dq_dqb;		/* actual usage & quotas */
};

#define	x_dq_own		x_dq_u.x_Dq_own
#define	x_dq_freef	x_dq_u.x_dq_f.x_Dq_freef
#define	x_dq_freeb	x_dq_u.x_dq_f.x_Dq_freeb
#define	x_dq_bhardlimit	x_dq_dqb.x_dqb_bhardlimit
#define	x_dq_bsoftlimit	x_dq_dqb.x_dqb_bsoftlimit
#define	x_dq_curblocks	x_dq_dqb.x_dqb_curblocks
#define	x_dq_ihardlimit	x_dq_dqb.x_dqb_ihardlimit
#define	x_dq_isoftlimit	x_dq_dqb.x_dqb_isoftlimit
#define	x_dq_curinodes	x_dq_dqb.x_dqb_curinodes
#define	x_dq_bwarn	x_dq_dqb.x_dqb_bwarn
#define	x_dq_iwarn	x_dq_dqb.x_dqb_iwarn

#define	x_NODQUOT		((struct x_dquot *) 0)
#define	x_LOSTDQUOT	((struct x_dquot *) 1)

#if defined(x_KERNEL) && defined(x_QUOTA)
struct	x_dquot *x_dquot, *x_dquotNDQUOT;
x_int	x_ndquot;
struct	x_dquot *x_discquota(), *x_inoquota(), *x_dqalloc(), *x_dqp();
#endif

/*
 * Definitions for the 'quota' system call.
 */
#define	x_Q_SETDLIM	1	/* set disc limits & usage */
#define	x_Q_GETDLIM	2	/* get disc limits & usage */
#define	x_Q_SETDUSE	3	/* set disc usage only */
#define	x_Q_SYNC		4	/* update disc copy of quota usages */
#define	x_Q_SETUID	16	/* change proc to use quotas for uid */
#define	x_Q_SETWARN	25	/* alter inode/block warning counts */
#define	x_Q_DOWARN	26	/* warn user about excessive space/inodes */

/*
 * Used in Q_SETDUSE.
 */
struct	x_dqusage {
	x_u_short	x_du_curinodes;
	x_u_long	x_du_curblocks;
};

/*
 * Used in Q_SETWARN.
 */
struct	x_dqwarn {
	x_u_char	x_dw_bwarn;
	x_u_char	x_dw_iwarn;
};
