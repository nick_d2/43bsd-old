#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)file.h	7.1 (Berkeley) 6/4/86
 */

#ifdef x_KERNEL
/*
 * Descriptor table entry.
 * One for each kernel object.
 */
struct	x_file {
	x_int	x_f_flag;		/* see below */
	x_short	x_f_type;		/* descriptor type */
	x_short	x_f_count;	/* reference count */
	x_short	x_f_msgcount;	/* references from message queue */
	struct	x_fileops {
		x_int	(*x_fo_rw)();
		x_int	(*x_fo_ioctl)();
		x_int	(*x_fo_select)();
		x_int	(*x_fo_close)();
	} *x_f_ops;
	x_caddr_t	x_f_data;		/* inode */
	x_off_t	x_f_offset;
};

struct	x_file *x_file, *x_fileNFILE;
x_int	x_nfile;
struct	x_file *x_getf();
struct	x_file *x_falloc();
#endif

/*
 * flags- also for fcntl call.
 */
#define	x_FOPEN		(-1)
#define	x_FREAD		00001		/* descriptor read/receive'able */
#define	x_FWRITE		00002		/* descriptor write/send'able */
#ifndef	x_F_DUPFD
#define	x_FNDELAY		00004		/* no delay */
#define	x_FAPPEND		00010		/* append on each write */
#endif
#define	x_FMARK		00020		/* mark during gc() */
#define	x_FDEFER		00040		/* defer for next gc pass */
#ifndef	x_F_DUPFD
#define	x_FASYNC		00100		/* signal pgrp when data ready */
#endif
#define	x_FSHLOCK		00200		/* shared lock present */
#define	x_FEXLOCK		00400		/* exclusive lock present */

/* bits to save after open */
#define	x_FMASK		00113
#define	x_FCNTLCANT	(x_FREAD|x_FWRITE|x_FMARK|x_FDEFER|x_FSHLOCK|x_FEXLOCK)

/* open only modes */
#define	x_FCREAT		01000		/* create if nonexistant */
#define	x_FTRUNC		02000		/* truncate to zero length */
#define	x_FEXCL		04000		/* error if already created */

#ifndef	x_F_DUPFD
/* fcntl(2) requests--from <fcntl.h> */
#define	x_F_DUPFD	0	/* Duplicate fildes */
#define	x_F_GETFD	1	/* Get fildes flags */
#define	x_F_SETFD	2	/* Set fildes flags */
#define	x_F_GETFL	3	/* Get file flags */
#define	x_F_SETFL	4	/* Set file flags */
#define	x_F_GETOWN 5	/* Get owner */
#define x_F_SETOWN 6	/* Set owner */
#endif

/*
 * User definitions.
 */

/*
 * Open call.
 */
#define	x_O_RDONLY	000		/* open for reading */
#define	x_O_WRONLY	001		/* open for writing */
#define	x_O_RDWR		002		/* open for read & write */
#define	x_O_NDELAY	x_FNDELAY		/* non-blocking open */
#define	x_O_APPEND	x_FAPPEND		/* append on each write */
#define	x_O_CREAT		x_FCREAT		/* open with file create */
#define	x_O_TRUNC		x_FTRUNC		/* open with truncation */
#define	x_O_EXCL		x_FEXCL		/* error on create if file exists */

/*
 * Flock call.
 */
#define	x_LOCK_SH		1	/* shared lock */
#define	x_LOCK_EX		2	/* exclusive lock */
#define	x_LOCK_NB		4	/* don't block when locking */
#define	x_LOCK_UN		8	/* unlock */

/*
 * Access call.
 */
#define	x_F_OK		0	/* does file exist */
#define	x_X_OK		1	/* is it executable by caller */
#define	x_W_OK		2	/* writable by caller */
#define	x_R_OK		4	/* readable by caller */

/*
 * Lseek call.
 */
#define	x_L_SET		0	/* absolute offset */
#define	x_L_INCR		1	/* relative to current offset */
#define	x_L_XTND		2	/* relative to end of file */

#ifdef x_KERNEL
#define	x_GETF(x_fp, x_fd) { \
	if ((unsigned)(x_fd) >= x_NOFILE || ((x_fp) = x_u.x_u_ofile[x_fd]) == x_NULL) { \
		x_u.x_u_error = x_EBADF; \
		return; \
	} \
}
#define	x_DTYPE_INODE	1	/* file */
#define	x_DTYPE_SOCKET	2	/* communications endpoint */
#endif
