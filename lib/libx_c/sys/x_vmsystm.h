#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)vmsystm.h	7.1 (Berkeley) 6/4/86
 */

/*
 * Miscellaneous virtual memory subsystem variables and structures.
 */

#ifdef x_KERNEL
x_int	x_freemem;		/* remaining blocks of free memory */
x_int	x_avefree;		/* moving average of remaining free blocks */
x_int	x_avefree30;		/* 30 sec (avefree is 5 sec) moving average */
x_int	x_deficit;		/* estimate of needs of new swapped in procs */
x_int	x_nscan;			/* number of scans in last second */
x_int	x_multprog;		/* current multiprogramming degree */
x_int	x_desscan;		/* desired pages scanned per second */

/* writable copies of tunables */
x_int	x_maxpgio;		/* max paging i/o per sec before start swaps */
x_int	x_maxslp;			/* max sleep time before very swappable */
x_int	x_lotsfree;		/* max free before clock freezes */
x_int	x_minfree;		/* minimum free pages before swapping begins */
x_int	x_desfree;		/* no of pages to try to keep free via daemon */
x_int	x_saferss;		/* no pages not to steal; decays with slptime */
x_int	x_slowscan;		/* slowest scan rate, clusters/second */
x_int	x_fastscan;		/* fastest scan rate, clusters/second */
#endif

/*
 * Fork/vfork accounting.
 */
struct	x_forkstat
{
	x_int	x_cntfork;
	x_int	x_cntvfork;
	x_int	x_sizfork;
	x_int	x_sizvfork;
};
#ifdef x_KERNEL
struct	x_forkstat x_forkstat;
#endif

/*
 * Swap kind accounting.
 */
struct	x_swptstat
{
	x_int	x_pteasy;		/* easy pt swaps */
	x_int	x_ptexpand;	/* pt expansion swaps */
	x_int	x_ptshrink;	/* pt shrinking swaps */
	x_int	x_ptpack;		/* pt swaps involving spte copying */
};
#ifdef x_KERNEL
struct	x_swptstat x_swptstat;
#endif
