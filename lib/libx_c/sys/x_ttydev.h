#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)ttydev.h	7.1 (Berkeley) 6/4/86
 */

/*
 * Terminal definitions related to underlying hardware.
 */
#ifndef x__TTYDEV_
#define	x__TTYDEV_

/*
 * Speeds
 */
#define x_B0	0
#define x_B50	1
#define x_B75	2
#define x_B110	3
#define x_B134	4
#define x_B150	5
#define x_B200	6
#define x_B300	7
#define x_B600	8
#define x_B1200	9
#define	x_B1800	10
#define x_B2400	11
#define x_B4800	12
#define x_B9600	13
#define x_EXTA	14
#define x_EXTB	15

#ifdef x_KERNEL
/*
 * Hardware bits.
 * SHOULD NOT BE HERE.
 */
#define	x_DONE	0200
#define	x_IENABLE	0100

/*
 * Modem control commands.
 */
#define	x_DMSET		0
#define	x_DMBIS		1
#define	x_DMBIC		2
#define	x_DMGET		3
#endif
#endif
