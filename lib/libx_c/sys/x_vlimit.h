#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)vlimit.h	7.1 (Berkeley) 6/4/86
 */

/*
 * Limits for u.u_limit[i], per process, inherited.
 */
#define	x_LIM_NORAISE	0	/* if <> 0, can't raise limits */
#define	x_LIM_CPU		1	/* max secs cpu time */
#define	x_LIM_FSIZE	2	/* max size of file created */
#define	x_LIM_DATA	3	/* max growth of data space */
#define	x_LIM_STACK	4	/* max growth of stack */
#define	x_LIM_CORE	5	/* max size of ``core'' file */
#define	x_LIM_MAXRSS	6	/* max desired data+stack core usage */

#define	x_NLIMITS		6

#define	x_INFINITY	0x7fffffff

#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

/* compat-4.1/vlimit.c */
x_int x_vlimit __P((x_int x_limit, x_int x_value));
