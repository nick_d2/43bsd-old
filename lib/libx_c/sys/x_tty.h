#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)tty.h	7.1 (Berkeley) 6/4/86
 */

#ifdef x_KERNEL
#include "x_ttychars.h"
#include "x_ttydev.h"
#else
#include <sys/x_ttychars.h>
#include <sys/x_ttydev.h>
#endif

/*
 * A clist structure is the head of a linked list queue
 * of characters.  The characters are stored in blocks
 * containing a link and CBSIZE (param.h) characters. 
 * The routines in tty_subr.c manipulate these structures.
 */
struct x_clist {
	x_int	x_c_cc;		/* character count */
	char	*x_c_cf;		/* pointer to first char */
	char	*x_c_cl;		/* pointer to last char */
};

/*
 * Per-tty structure.
 *
 * Should be split in two, into device and tty drivers.
 * Glue could be masks of what to echo and circular buffer
 * (low, high, timeout).
 */
struct x_tty {
	union {
		struct {
			struct	x_clist x_T_rawq;
			struct	x_clist x_T_canq;
		} x_t_t;
#define	x_t_rawq	x_t_nu.x_t_t.x_T_rawq		/* raw characters or partial line */
#define	x_t_canq	x_t_nu.x_t_t.x_T_canq		/* raw characters or partial line */
		struct {
			struct	x_buf *x_T_bufp;
			char	*x_T_cp;
			x_int	x_T_inbuf;
			x_int	x_T_rec;
		} x_t_n;
#define	x_t_bufp	x_t_nu.x_t_n.x_T_bufp		/* buffer allocated to protocol */
#define	x_t_cp	x_t_nu.x_t_n.x_T_cp		/* pointer into the ripped off buffer */
#define	x_t_inbuf	x_t_nu.x_t_n.x_T_inbuf	/* number chars in the buffer */
#define	x_t_rec	x_t_nu.x_t_n.x_T_rec		/* have a complete record */
	} x_t_nu;
	struct	x_clist x_t_outq;		/* device */
	x_int	(*x_t_oproc)();		/* device */
	struct	x_proc *x_t_rsel;		/* tty */
	struct	x_proc *x_t_wsel;
				x_caddr_t	x_T_LINEP;	/* ### */
	x_caddr_t	x_t_addr;			/* ??? */
	x_dev_t	x_t_dev;			/* device */
	x_int	x_t_flags;		/* some of both */
	x_int	x_t_state;		/* some of both */
	x_short	x_t_pgrp;			/* tty */
	char	x_t_delct;		/* tty */
	char	x_t_line;			/* glue */
	char	x_t_col;			/* tty */
	char	x_t_ispeed, x_t_ospeed;	/* device */
	char	x_t_rocount, x_t_rocol;	/* tty */
	struct	x_ttychars x_t_chars;	/* tty */
	struct	x_winsize x_t_winsize;	/* window size */
/* be careful of tchars & co. */
#define	x_t_erase		x_t_chars.x_tc_erase
#define	x_t_kill		x_t_chars.x_tc_kill
#define	x_t_intrc		x_t_chars.x_tc_intrc
#define	x_t_quitc		x_t_chars.x_tc_quitc
#define	x_t_startc	x_t_chars.x_tc_startc
#define	x_t_stopc		x_t_chars.x_tc_stopc
#define	x_t_eofc		x_t_chars.x_tc_eofc
#define	x_t_brkc		x_t_chars.x_tc_brkc
#define	x_t_suspc		x_t_chars.x_tc_suspc
#define	x_t_dsuspc	x_t_chars.x_tc_dsuspc
#define	x_t_rprntc	x_t_chars.x_tc_rprntc
#define	x_t_flushc	x_t_chars.x_tc_flushc
#define	x_t_werasc	x_t_chars.x_tc_werasc
#define	x_t_lnextc	x_t_chars.x_tc_lnextc
};

#define	x_TTIPRI	28
#define	x_TTOPRI	29

/* limits */
#define	x_NSPEEDS	16
#define	x_TTMASK	15
#define	x_OBUFSIZ	100
#define	x_TTYHOG	255
#ifdef x_KERNEL
x_short	x_tthiwat[x_NSPEEDS], x_ttlowat[x_NSPEEDS];
#define	x_TTHIWAT(x_tp)	x_tthiwat[(x_tp)->x_t_ospeed&x_TTMASK]
#define	x_TTLOWAT(x_tp)	x_ttlowat[(x_tp)->x_t_ospeed&x_TTMASK]
extern	struct x_ttychars x_ttydefaults;
#endif

/* internal state bits */
#define	x_TS_TIMEOUT	0x000001	/* delay timeout in progress */
#define	x_TS_WOPEN	0x000002	/* waiting for open to complete */
#define	x_TS_ISOPEN	0x000004	/* device is open */
#define	x_TS_FLUSH	0x000008	/* outq has been flushed during DMA */
#define	x_TS_CARR_ON	0x000010	/* software copy of carrier-present */
#define	x_TS_BUSY		0x000020	/* output in progress */
#define	x_TS_ASLEEP	0x000040	/* wakeup when output done */
#define	x_TS_XCLUDE	0x000080	/* exclusive-use flag against open */
#define	x_TS_TTSTOP	0x000100	/* output stopped by ctl-s */
#define	x_TS_HUPCLS	0x000200	/* hang up upon last close */
#define	x_TS_TBLOCK	0x000400	/* tandem queue blocked */
#define	x_TS_RCOLL	0x000800	/* collision in read select */
#define	x_TS_WCOLL	0x001000	/* collision in write select */
#define	x_TS_NBIO		0x002000	/* tty in non-blocking mode */
#define	x_TS_ASYNC	0x004000	/* tty in async i/o mode */
/* state for intra-line fancy editing work */
#define	x_TS_BKSL		0x010000	/* state for lowercase \ work */
#define	x_TS_QUOT		0x020000	/* last character input was \ */
#define	x_TS_ERASE	0x040000	/* within a \.../ for PRTRUB */
#define	x_TS_LNCH		0x080000	/* next character is literal */
#define	x_TS_TYPEN	0x100000	/* retyping suspended input (PENDIN) */
#define	x_TS_CNTTB	0x200000	/* counting tab width; leave FLUSHO alone */

#define	x_TS_LOCAL	(x_TS_BKSL|x_TS_QUOT|x_TS_ERASE|x_TS_LNCH|x_TS_TYPEN|x_TS_CNTTB)

/* define partab character types */
#define	x_ORDINARY	0
#define	x_CONTROL		1
#define	x_BACKSPACE	2
#define	x_NEWLINE		3
#define	x_TAB		4
#define	x_VTAB		5
#define	x_RETURN		6
