#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)vmmeter.h	7.1 (Berkeley) 6/4/86
 */

/*
 * Virtual memory related instrumentation
 */
struct x_vmmeter
{
#define	x_v_first	x_v_swtch
	x_unsigned_int x_v_swtch;	/* context switches */
	x_unsigned_int x_v_trap;	/* calls to trap */
	x_unsigned_int x_v_syscall;	/* calls to syscall() */
	x_unsigned_int x_v_intr;	/* device interrupts */
	x_unsigned_int x_v_soft;	/* software interrupts */
	x_unsigned_int x_v_pdma;	/* pseudo-dma interrupts */
	x_unsigned_int x_v_pswpin;	/* pages swapped in */
	x_unsigned_int x_v_pswpout;	/* pages swapped out */
	x_unsigned_int x_v_pgin;	/* pageins */
	x_unsigned_int x_v_pgout;	/* pageouts */
	x_unsigned_int x_v_pgpgin;	/* pages paged in */
	x_unsigned_int x_v_pgpgout;	/* pages paged out */
	x_unsigned_int x_v_intrans;	/* intransit blocking page faults */
	x_unsigned_int x_v_pgrec;	/* total page reclaims */
	x_unsigned_int x_v_xsfrec;	/* found in free list rather than on swapdev */
	x_unsigned_int x_v_xifrec;	/* found in free list rather than in filsys */
	x_unsigned_int x_v_exfod;	/* pages filled on demand from executables */
	x_unsigned_int x_v_zfod;	/* pages zero filled on demand */
	x_unsigned_int x_v_vrfod;	/* fills of pages mapped by vread() */
	x_unsigned_int x_v_nexfod;	/* number of exfod's created */
	x_unsigned_int x_v_nzfod;	/* number of zfod's created */
	x_unsigned_int x_v_nvrfod;	/* number of vrfod's created */
	x_unsigned_int x_v_pgfrec;	/* page reclaims from free list */
	x_unsigned_int x_v_faults;	/* total faults taken */
	x_unsigned_int x_v_scan;	/* scans in page out daemon */
	x_unsigned_int x_v_rev;		/* revolutions of the hand */
	x_unsigned_int x_v_seqfree;	/* pages taken from sequential programs */
	x_unsigned_int x_v_dfree;	/* pages freed by daemon */
	x_unsigned_int x_v_fastpgrec;	/* fast reclaims in locore */
#define	x_v_last x_v_fastpgrec
	x_unsigned_int x_v_swpin;	/* swapins */
	x_unsigned_int x_v_swpout;	/* swapouts */
};
#ifdef x_KERNEL
struct	x_vmmeter x_cnt, x_rate, x_sum;
#endif

/* systemwide totals computed every five seconds */
struct x_vmtotal
{
	x_short	x_t_rq;		/* length of the run queue */
	x_short	x_t_dw;		/* jobs in ``disk wait'' (neg priority) */
	x_short	x_t_pw;		/* jobs in page wait */
	x_short	x_t_sl;		/* jobs sleeping in core */
	x_short	x_t_sw;		/* swapped out runnable/short block jobs */
	x_long	x_t_vm;		/* total virtual memory */
	x_long	x_t_avm;		/* active virtual memory */
	x_long	x_t_rm;		/* total real memory in use */
	x_long	x_t_arm;		/* active real memory */
	x_long	x_t_vmtxt;	/* virtual memory used by text */
	x_long	x_t_avmtxt;	/* active virtual memory used by text */
	x_long	x_t_rmtxt;	/* real memory used by text */
	x_long	x_t_armtxt;	/* active real memory used by text */
	x_long	x_t_free;		/* free memory pages */
};
#ifdef x_KERNEL
struct	x_vmtotal x_total;
#endif

/*
 * Optional instrumentation.
 */
#ifdef x_PGINPROF

#define	x_NDMON	128
#define	x_NSMON	128

#define	x_DRES	20
#define	x_SRES	5

#define	x_PMONMIN	20
#define	x_PRES	50
#define	x_NPMON	64

#define	x_RMONMIN	130
#define	x_RRES	5
#define	x_NRMON	64

/* data and stack size distribution counters */
x_unsigned_int	x_dmon[x_NDMON+1];
x_unsigned_int	x_smon[x_NSMON+1];

/* page in time distribution counters */
x_unsigned_int	x_pmon[x_NPMON+2];

/* reclaim time distribution counters */
x_unsigned_int	x_rmon[x_NRMON+2];

x_int	x_pmonmin;
x_int	x_pres;
x_int	x_rmonmin;
x_int	x_rres;

x_unsigned_int x_rectime;		/* accumulator for reclaim times */
x_unsigned_int x_pgintime;		/* accumulator for page in times */
#endif
