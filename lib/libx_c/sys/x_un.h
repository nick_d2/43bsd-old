#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)un.h	7.1 (Berkeley) 6/4/86
 */

/*
 * Definitions for UNIX IPC domain.
 */
struct	x_sockaddr_un {
	x_short	x_sun_family;		/* AF_UNIX */
	char	x_sun_path[108];		/* path name (gag) */
};

#ifdef x_KERNEL
x_int	x_unp_discard();
#endif
