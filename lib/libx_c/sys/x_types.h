#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)types.h	7.1 (Berkeley) 6/4/86
 */

#ifndef x__TYPES_
#define	x__TYPES_
/*
 * Basic system types and major/minor device constructing/busting macros.
 */

/* major part of a device */
#define	x_major(x_x)	((x_int)(((unsigned)(x_x)>>8)&0377))

/* minor part of a device */
#define	x_minor(x_x)	((x_int)((x_x)&0377))

/* make a device number */
#define	x_makedev(x_x,x_y)	((x_dev_t)(((x_x)<<8) | (x_y)))

typedef	unsigned char	x_u_char;
typedef	x_unsigned_short	x_u_short;
typedef	x_unsigned_int	x_u_int;
typedef	x_unsigned_long	x_u_long;
typedef	x_unsigned_short	x_ushort;		/* sys III compat */

#ifdef x_vax
typedef	struct	x__physadr { x_int x_r[1]; } *x_physadr;
typedef	struct	x_label_t	{
	x_int	x_val[14];
} x_label_t;
#endif
typedef	struct	x__quad { x_long x_val[2]; } x_quad;
typedef	x_long	x_daddr_t;
typedef	char *	x_caddr_t;
typedef	x_u_long	x_ino_t;
typedef	x_long	x_swblk_t;
typedef	x_long	x_size_t;
typedef	x_long	x_time_t;
typedef	x_short	x_dev_t;
typedef	x_long	x_off_t;
typedef	x_u_short	x_uid_t;
typedef	x_u_short	x_gid_t;

#define	x_NBBY	8		/* number of bits in a byte */
/*
 * Select uses bit masks of file descriptors in longs.
 * These macros manipulate such bit fields (the filesystem macros use chars).
 * FD_SETSIZE may be defined by the user, but the default here
 * should be >= NOFILE (param.h).
 */
#ifndef	x_FD_SETSIZE
#define	x_FD_SETSIZE	256
#endif

typedef x_long	x_fd_mask;
#define x_NFDBITS	(sizeof(x_fd_mask) * x_NBBY)	/* bits per mask */
#ifndef x_howmany
#define	x_howmany(x_x, x_y)	(((x_x)+((x_y)-1))/(x_y))
#endif

typedef	struct x_fd_set {
	x_fd_mask	x_fds_bits[x_howmany(x_FD_SETSIZE, x_NFDBITS)];
} x_fd_set;

#define	x_FD_SET(x_n, x_p)	((x_p)->x_fds_bits[(x_n)/x_NFDBITS] |= (1 << ((x_n) % x_NFDBITS)))
#define	x_FD_CLR(x_n, x_p)	((x_p)->x_fds_bits[(x_n)/x_NFDBITS] &= ~(1 << ((x_n) % x_NFDBITS)))
#define	x_FD_ISSET(x_n, x_p)	((x_p)->x_fds_bits[(x_n)/x_NFDBITS] & (1 << ((x_n) % x_NFDBITS)))
#define x_FD_ZERO(x_p)	x_bzero((char *)(x_p), sizeof(*(x_p)))

#endif
