#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)bkmac.h	7.1 (Berkeley) 6/4/86
 */

/*
 * Macro definition of bk.c/netinput().
 * This is used to replace a call to
 *		(*linesw[tp->t_line].l_rint)(c,tp);
 * with
 *
 *		if (tp->t_line == NETLDISC)
 *			BKINPUT(c, tp);
 *		else
 *			(*linesw[tp->t_line].l_rint)(c,tp);
 */
#define	x_BKINPUT(x_c, x_tp) { \
	if ((x_tp)->x_t_rec == 0) { \
		*(x_tp)->x_t_cp++ = x_c; \
		if (++(x_tp)->x_t_inbuf == 1024 || (x_c) == '\n') { \
			(x_tp)->x_t_rec = 1; \
			x_wakeup((x_caddr_t)&(x_tp)->x_t_rawq); \
		} \
	} \
}
