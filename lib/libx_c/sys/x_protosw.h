#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)protosw.h	7.1 (Berkeley) 6/4/86
 */

/*
 * Protocol switch table.
 *
 * Each protocol has a handle initializing one of these structures,
 * which is used for protocol-protocol and system-protocol communication.
 *
 * A protocol is called through the pr_init entry before any other.
 * Thereafter it is called every 200ms through the pr_fasttimo entry and
 * every 500ms through the pr_slowtimo for timer based actions.
 * The system will call the pr_drain entry if it is low on space and
 * this should throw away any non-critical data.
 *
 * Protocols pass data between themselves as chains of mbufs using
 * the pr_input and pr_output hooks.  Pr_input passes data up (towards
 * UNIX) and pr_output passes it down (towards the imps); control
 * information passes up and down on pr_ctlinput and pr_ctloutput.
 * The protocol is responsible for the space occupied by any the
 * arguments to these entries and must dispose it.
 *
 * The userreq routine interfaces protocols to the system and is
 * described below.
 */
struct x_protosw {
	x_short	x_pr_type;		/* socket type used for */
	struct	x_domain *x_pr_domain;	/* domain protocol a member of */
	x_short	x_pr_protocol;		/* protocol number */
	x_short	x_pr_flags;		/* see below */
/* protocol-protocol hooks */
	x_int	(*x_pr_input)();		/* input to protocol (from below) */
	x_int	(*x_pr_output)();		/* output to protocol (from above) */
	x_int	(*x_pr_ctlinput)();	/* control input (from below) */
	x_int	(*x_pr_ctloutput)();	/* control output (from above) */
/* user-protocol hook */
	x_int	(*x_pr_usrreq)();		/* user request: see list below */
/* utility hooks */
	x_int	(*x_pr_init)();		/* initialization hook */
	x_int	(*x_pr_fasttimo)();	/* fast timeout (200ms) */
	x_int	(*x_pr_slowtimo)();	/* slow timeout (500ms) */
	x_int	(*x_pr_drain)();		/* flush any excess space possible */
};

#define	x_PR_SLOWHZ	2		/* 2 slow timeouts per second */
#define	x_PR_FASTHZ	5		/* 5 fast timeouts per second */

/*
 * Values for pr_flags
 */
#define	x_PR_ATOMIC	0x01		/* exchange atomic messages only */
#define	x_PR_ADDR		0x02		/* addresses given with messages */
/* in the current implementation, PR_ADDR needs PR_ATOMIC to work */
#define	x_PR_CONNREQUIRED	0x04		/* connection required by protocol */
#define	x_PR_WANTRCVD	0x08		/* want PRU_RCVD calls */
#define	x_PR_RIGHTS	0x10		/* passes capabilities */

/*
 * The arguments to usrreq are:
 *	(*protosw[].pr_usrreq)(up, req, m, nam, opt);
 * where up is a (struct socket *), req is one of these requests,
 * m is a optional mbuf chain containing a message,
 * nam is an optional mbuf chain containing an address,
 * and opt is a pointer to a socketopt structure or nil.
 * The protocol is responsible for disposal of the mbuf chain m,
 * the caller is responsible for any space held by nam and opt.
 * A non-zero return from usrreq gives an
 * UNIX error number which should be passed to higher level software.
 */
#define	x_PRU_ATTACH		0	/* attach protocol to up */
#define	x_PRU_DETACH		1	/* detach protocol from up */
#define	x_PRU_BIND		2	/* bind socket to address */
#define	x_PRU_LISTEN		3	/* listen for connection */
#define	x_PRU_CONNECT		4	/* establish connection to peer */
#define	x_PRU_ACCEPT		5	/* accept connection from peer */
#define	x_PRU_DISCONNECT		6	/* disconnect from peer */
#define	x_PRU_SHUTDOWN		7	/* won't send any more data */
#define	x_PRU_RCVD		8	/* have taken data; more room now */
#define	x_PRU_SEND		9	/* send this data */
#define	x_PRU_ABORT		10	/* abort (fast DISCONNECT, DETATCH) */
#define	x_PRU_CONTROL		11	/* control operations on protocol */
#define	x_PRU_SENSE		12	/* return status into m */
#define	x_PRU_RCVOOB		13	/* retrieve out of band data */
#define	x_PRU_SENDOOB		14	/* send out of band data */
#define	x_PRU_SOCKADDR		15	/* fetch socket's address */
#define	x_PRU_PEERADDR		16	/* fetch peer's address */
#define	x_PRU_CONNECT2		17	/* connect two sockets */
/* begin for protocols internal use */
#define	x_PRU_FASTTIMO		18	/* 200ms timeout */
#define	x_PRU_SLOWTIMO		19	/* 500ms timeout */
#define	x_PRU_PROTORCV		20	/* receive from below */
#define	x_PRU_PROTOSEND		21	/* send to below */

#define	x_PRU_NREQ		21

#ifdef x_PRUREQUESTS
char *x_prurequests[] = {
	"ATTACH",	"DETACH",	"BIND",		"LISTEN",
	"CONNECT",	"ACCEPT",	"DISCONNECT",	"SHUTDOWN",
	"RCVD",		"SEND",		"ABORT",	"CONTROL",
	"SENSE",	"RCVOOB",	"SENDOOB",	"SOCKADDR",
	"PEERADDR",	"CONNECT2",	"FASTTIMO",	"SLOWTIMO",
	"PROTORCV",	"PROTOSEND",
};
#endif

/*
 * The arguments to the ctlinput routine are
 *	(*protosw[].pr_ctlinput)(cmd, arg);
 * where cmd is one of the commands below, and arg is
 * an optional argument (caddr_t).
 *
 * N.B. The IMP code, in particular, pressumes the values
 *      of some of the commands; change with extreme care.
 * TODO:
 *	spread out codes so new ICMP codes can be
 *	accomodated more easily
 */
#define	x_PRC_IFDOWN		0	/* interface transition */
#define	x_PRC_ROUTEDEAD		1	/* select new route if possible */
#define	x_PRC_QUENCH		4	/* some said to slow down */
#define	x_PRC_MSGSIZE		5	/* message size forced drop */
#define	x_PRC_HOSTDEAD		6	/* normally from IMP */
#define	x_PRC_HOSTUNREACH		7	/* ditto */
#define	x_PRC_UNREACH_NET		8	/* no route to network */
#define	x_PRC_UNREACH_HOST	9	/* no route to host */
#define	x_PRC_UNREACH_PROTOCOL	10	/* dst says bad protocol */
#define	x_PRC_UNREACH_PORT	11	/* bad port # */
#define	x_PRC_UNREACH_NEEDFRAG	12	/* IP_DF caused drop */
#define	x_PRC_UNREACH_SRCFAIL	13	/* source route failed */
#define	x_PRC_REDIRECT_NET	14	/* net routing redirect */
#define	x_PRC_REDIRECT_HOST	15	/* host routing redirect */
#define	x_PRC_REDIRECT_TOSNET	16	/* redirect for type of service & net */
#define	x_PRC_REDIRECT_TOSHOST	17	/* redirect for tos & host */
#define	x_PRC_TIMXCEED_INTRANS	18	/* packet lifetime expired in transit */
#define	x_PRC_TIMXCEED_REASS	19	/* lifetime expired on reass q */
#define	x_PRC_PARAMPROB		20	/* header incorrect */

#define	x_PRC_NCMDS		21

#ifdef x_PRCREQUESTS
char	*x_prcrequests[] = {
	"IFDOWN", "ROUTEDEAD", "#2", "#3",
	"QUENCH", "MSGSIZE", "HOSTDEAD", "HOSTUNREACH",
	"NET-UNREACH", "HOST-UNREACH", "PROTO-UNREACH", "PORT-UNREACH",
	"FRAG-UNREACH", "SRCFAIL-UNREACH", "NET-REDIRECT", "HOST-REDIRECT",
	"TOSNET-REDIRECT", "TOSHOST-REDIRECT", "TX-INTRANS", "TX-REASS",
	"PARAMPROB"
};
#endif

/*
 * The arguments to ctloutput are:
 *	(*protosw[].pr_ctloutput)(req, so, level, optname, optval);
 * req is one of the actions listed below, so is a (struct socket *),
 * level is an indication of which protocol layer the option is intended.
 * optname is a protocol dependent socket option request,
 * optval is a pointer to a mbuf-chain pointer, for value-return results.
 * The protocol is responsible for disposal of the mbuf chain *optval
 * if supplied,
 * the caller is responsible for any space held by *optval, when returned.
 * A non-zero return from usrreq gives an
 * UNIX error number which should be passed to higher level software.
 */
#define	x_PRCO_GETOPT	0
#define	x_PRCO_SETOPT	1

#define	x_PRCO_NCMDS	2

#ifdef x_PRCOREQUESTS
char	*x_prcorequests[] = {
	"GETOPT", "SETOPT",
};
#endif

#ifdef x_KERNEL
extern	struct x_protosw *x_pffindproto(), *x_pffindtype();
#endif
