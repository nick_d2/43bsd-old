#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)cmap.h	7.1 (Berkeley) 6/4/86
 */

/*
 * core map entry
 *
 * Limits imposed by this structure:
 *
 *		limit		     cur. size		fields
 *	Physical memory*		64 Mb	c_next, c_prev, c_hlink
 *	Mounted filesystems		255	c_mdev
 *	size of a process segment	1 Gb	c_page
 *	filesystem size			8 Gb	c_blkno
 *	proc, text table size		64K	c_ndx
 *
 *	* memory can be expanded by converting first three entries
 *	to bit fields of larger than 16 bits, shrinking c_ndx accordingly,
 *	and increasing MAXMEM below.  Also, the type of cmhash
 *	(below) must be changed to long.
 */
#ifndef	x_LOCORE
struct x_cmap
{
x_unsigned_short 	x_c_next,		/* index of next free list entry */
		x_c_prev,		/* index of previous free list entry */
		x_c_hlink;	/* hash link for <blkno,mdev> */
x_unsigned_short	x_c_ndx;		/* index of owner proc or text */
x_unsigned_int	x_c_page:21,	/* virtual page number in segment */
		x_c_lock:1,	/* locked for raw i/o or pagein */
		x_c_want:1,	/* wanted */
		x_c_intrans:1,	/* intransit bit */
		x_c_free:1,	/* on the free list */
		x_c_gone:1,	/* associated page has been released */
		x_c_type:2,	/* type CSYS or CTEXT or CSTACK or CDATA */
		:4,		/* to longword boundary */
		x_c_blkno:24,	/* disk block this is a copy of */
		x_c_mdev:8;	/* which mounted dev this is from */
};
#else
/*
 * bit offsets of elements in cmap
 */
#define	x_C_INTRANS	87
#define	x_C_FREE		88
#define	x_SZ_CMAP		16		/* sizeof(struct cmap) */

#define	x_MAXMEM		64*1024		/* maximum memory, in Kbytes */
#endif

#define	x_CMHEAD	0

/*
 * Shared text pages are not totally abandoned when a process
 * exits, but are remembered while in the free list hashed by <mdev,blkno>
 * off the cmhash structure so that they can be reattached
 * if another instance of the program runs again soon.
 */
#define	x_CMHSIZ	2048		/* SHOULD BE DYNAMIC */
#define	x_CMHASH(x_bn)	((x_bn)&(x_CMHSIZ-1))

#ifndef	x_LOCORE
#ifdef	x_KERNEL
struct	x_cmap *x_cmap;
struct	x_cmap *x_ecmap;
x_int	x_ncmap;
struct	x_cmap *x_mfind();
x_int	x_firstfree, x_maxfree;
x_int	x_ecmx;			/* cmap index of ecmap */
x_u_short	x_cmhash[x_CMHSIZ];
#endif

/* bits defined in c_type */

#define	x_CSYS		0		/* none of below */
#define	x_CTEXT		1		/* belongs to shared text segment */
#define	x_CDATA		2		/* belongs to data segment */
#define	x_CSTACK		3		/* belongs to stack segment */

#define	x_pgtocm(x_x)	(((x_int) ((x_x)-x_firstfree) / x_CLSIZE) + 1)
#define	x_cmtopg(x_x)	((((x_x)-1) * x_CLSIZE) + x_firstfree)
#endif
