#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)vcmd.h	7.1 (Berkeley) 6/4/86
 */

#ifndef x__IOCTL_
#ifdef x_KERNEL
#include "x_ioctl.h"
#else
#include <sys/x_ioctl.h>
#endif
#endif

#define	x_VPRINT		0100
#define	x_VPLOT		0200
#define	x_VPRINTPLOT	0400

#define	x_VGETSTATE	x__IOR(x_v, 0, x_int)
#define	x_VSETSTATE	x__IOW(x_v, 1, x_int)
