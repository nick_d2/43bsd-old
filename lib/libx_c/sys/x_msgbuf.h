#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)msgbuf.h	7.1 (Berkeley) 6/4/86
 */

#define	x_MSG_MAGIC	0x063061
#define	x_MSG_BSIZE	(4096 - 3 * sizeof (x_long))
struct	x_msgbuf {
	x_long	x_msg_magic;
	x_long	x_msg_bufx;
	x_long	x_msg_bufr;
	char	x_msg_bufc[x_MSG_BSIZE];
};
#ifdef x_KERNEL
struct	x_msgbuf x_msgbuf;
#endif
