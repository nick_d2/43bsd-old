#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)inode.h	7.1 (Berkeley) 6/4/86
 */

/*
 * The I node is the focus of all file activity in UNIX.
 * There is a unique inode allocated for each active file,
 * each current directory, each mounted-on file, text file, and the root.
 * An inode is 'named' by its dev/inumber pair. (iget/iget.c)
 * Data in icommon is read in from permanent inode on volume.
 */

#define	x_NDADDR	12		/* direct addresses in inode */
#define	x_NIADDR	3		/* indirect addresses in inode */

struct x_inode {
	struct	x_inode *x_i_chain[2];	/* must be first */
	x_u_short	x_i_flag;
	x_u_short	x_i_count;	/* reference count */
	x_dev_t	x_i_dev;		/* device where inode resides */
	x_u_short	x_i_shlockc;	/* count of shared locks on inode */
	x_u_short	x_i_exlockc;	/* count of exclusive locks on inode */
	x_ino_t	x_i_number;	/* i number, 1-to-1 with device address */
	x_long	x_i_id;		/* unique identifier */
	struct	x_fs *x_i_fs;	/* file sys associated with this inode */
	struct	x_dquot *x_i_dquot;	/* quota structure controlling this file */
	struct	x_text *x_i_text;	/* text entry, if any (should be region) */
	union {
		x_daddr_t	x_if_lastr;	/* last read (read-ahead) */
		struct	x_socket *x_is_socket;
		struct	{
			struct x_inode  *x_if_freef;	/* free list forward */
			struct x_inode **x_if_freeb;	/* free list back */
		} x_i_fr;
	} x_i_un;
	struct 	x_icommon
	{
		x_u_short	x_ic_mode;	/*  0: mode and type of file */
		x_short	x_ic_nlink;	/*  2: number of links to file */
		x_uid_t	x_ic_uid;		/*  4: owner's user id */
		x_gid_t	x_ic_gid;		/*  6: owner's group id */
		x_quad	x_ic_size;	/*  8: number of bytes in file */
		x_time_t	x_ic_atime;	/* 16: time last accessed */
		x_long	x_ic_atspare;
		x_time_t	x_ic_mtime;	/* 24: time last modified */
		x_long	x_ic_mtspare;
		x_time_t	x_ic_ctime;	/* 32: last time inode changed */
		x_long	x_ic_ctspare;
		x_daddr_t	x_ic_db[x_NDADDR];	/* 40: disk block addresses */
		x_daddr_t	x_ic_ib[x_NIADDR];	/* 88: indirect blocks */
		x_long	x_ic_flags;	/* 100: status, currently unused */
		x_long	x_ic_blocks;	/* 104: blocks actually held */
		x_long	x_ic_spare[5];	/* 108: reserved, currently unused */
	} x_i_ic;
};

struct x_dinode {
	union {
		struct	x_icommon x_di_icom;
		char	x_di_size[128];
	} x_di_un;
};

#define	x_i_mode		x_i_ic.x_ic_mode
#define	x_i_nlink		x_i_ic.x_ic_nlink
#define	x_i_uid		x_i_ic.x_ic_uid
#define	x_i_gid		x_i_ic.x_ic_gid
/* ugh! -- must be fixed */
#ifdef x_vax
#define	x_i_size		x_i_ic.x_ic_size.x_val[0]
#endif
#define	x_i_db		x_i_ic.x_ic_db
#define	x_i_ib		x_i_ic.x_ic_ib
#define	x_i_atime		x_i_ic.x_ic_atime
#define	x_i_mtime		x_i_ic.x_ic_mtime
#define	x_i_ctime		x_i_ic.x_ic_ctime
#define x_i_blocks	x_i_ic.x_ic_blocks
#define	x_i_rdev		x_i_ic.x_ic_db[0]
#define	x_i_lastr		x_i_un.x_if_lastr
#define	x_i_socket	x_i_un.x_is_socket
#define	x_i_forw		x_i_chain[0]
#define	x_i_back		x_i_chain[1]
#define	x_i_freef		x_i_un.x_i_fr.x_if_freef
#define	x_i_freeb		x_i_un.x_i_fr.x_if_freeb

#define x_di_ic		x_di_un.x_di_icom
#define	x_di_mode		x_di_ic.x_ic_mode
#define	x_di_nlink	x_di_ic.x_ic_nlink
#define	x_di_uid		x_di_ic.x_ic_uid
#define	x_di_gid		x_di_ic.x_ic_gid
#ifdef x_vax
#define	x_di_size		x_di_ic.x_ic_size.x_val[0]
#endif
#define	x_di_db		x_di_ic.x_ic_db
#define	x_di_ib		x_di_ic.x_ic_ib
#define	x_di_atime	x_di_ic.x_ic_atime
#define	x_di_mtime	x_di_ic.x_ic_mtime
#define	x_di_ctime	x_di_ic.x_ic_ctime
#define	x_di_rdev		x_di_ic.x_ic_db[0]
#define	x_di_blocks	x_di_ic.x_ic_blocks

#ifdef x_KERNEL
/*
 * Invalidate an inode. Used by the namei cache to detect stale
 * information. At an absurd rate of 100 calls/second, the inode
 * table invalidation should only occur once every 16 months.
 */
#define x_cacheinval(x_ip)	\
	(x_ip)->x_i_id = ++x_nextinodeid; \
	if (x_nextinodeid == 0) \
		x_cacheinvalall();

struct x_inode *x_inode;		/* the inode table itself */
struct x_inode *x_inodeNINODE;	/* the end of the inode table */
x_int	x_ninode;			/* number of slots in the table */
x_long	x_nextinodeid;		/* unique id generator */

struct	x_inode *x_rootdir;			/* pointer to inode of root directory */

struct	x_inode *x_ialloc();
struct	x_inode *x_iget();
#ifdef x_notdef
struct	x_inode *x_ifind();
#endif
struct	x_inode *x_owner();
struct	x_inode *x_maknode();
struct	x_inode *x_namei();

x_ino_t	x_dirpref();
#endif

/* flags */
#define	x_ILOCKED		0x1		/* inode is locked */
#define	x_IUPD		0x2		/* file has been modified */
#define	x_IACC		0x4		/* inode access time to be updated */
#define	x_IMOUNT		0x8		/* inode is mounted on */
#define	x_IWANT		0x10		/* some process waiting on lock */
#define	x_ITEXT		0x20		/* inode is pure text prototype */
#define	x_ICHG		0x40		/* inode has been changed */
#define	x_ISHLOCK		0x80		/* file has shared lock */
#define	x_IEXLOCK		0x100		/* file has exclusive lock */
#define	x_ILWAIT		0x200		/* someone waiting on file lock */
#define	x_IMOD		0x400		/* inode has been modified */
#define	x_IRENAME		0x800		/* inode is being renamed */

/* modes */
#define	x_IFMT		0170000		/* type of file */
#define	x_IFCHR		0020000		/* character special */
#define	x_IFDIR		0040000		/* directory */
#define	x_IFBLK		0060000		/* block special */
#define	x_IFREG		0100000		/* regular */
#define	x_IFLNK		0120000		/* symbolic link */
#define	x_IFSOCK		0140000		/* socket */

#define	x_ISUID		04000		/* set user id on execution */
#define	x_ISGID		02000		/* set group id on execution */
#define	x_ISVTX		01000		/* save swapped text even after use */
#define	x_IREAD		0400		/* read, write, execute permissions */
#define	x_IWRITE		0200
#define	x_IEXEC		0100

#define	x_ILOCK(x_ip) { \
	while ((x_ip)->x_i_flag & x_ILOCKED) { \
		(x_ip)->x_i_flag |= x_IWANT; \
		x_sleep((x_caddr_t)(x_ip), x_PINOD); \
	} \
	(x_ip)->x_i_flag |= x_ILOCKED; \
}

#define	x_IUNLOCK(x_ip) { \
	(x_ip)->x_i_flag &= ~x_ILOCKED; \
	if ((x_ip)->x_i_flag&x_IWANT) { \
		(x_ip)->x_i_flag &= ~x_IWANT; \
		x_wakeup((x_caddr_t)(x_ip)); \
	} \
}

#define	x_IUPDAT(x_ip, x_t1, x_t2, x_waitfor) { \
	if (x_ip->x_i_flag&(x_IUPD|x_IACC|x_ICHG|x_IMOD)) \
		x_iupdat(x_ip, x_t1, x_t2, x_waitfor); \
}

#define	x_ITIMES(x_ip, x_t1, x_t2) { \
	if ((x_ip)->x_i_flag&(x_IUPD|x_IACC|x_ICHG)) { \
		(x_ip)->x_i_flag |= x_IMOD; \
		if ((x_ip)->x_i_flag&x_IACC) \
			(x_ip)->x_i_atime = (x_t1)->x_tv_sec; \
		if ((x_ip)->x_i_flag&x_IUPD) \
			(x_ip)->x_i_mtime = (x_t2)->x_tv_sec; \
		if ((x_ip)->x_i_flag&x_ICHG) \
			(x_ip)->x_i_ctime = x_time.x_tv_sec; \
		(x_ip)->x_i_flag &= ~(x_IACC|x_IUPD|x_ICHG); \
	} \
}
