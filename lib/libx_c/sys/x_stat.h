#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)stat.h	7.1 (Berkeley) 6/4/86
 */

struct	x_stat
{
	x_dev_t	x_st_dev;
	x_ino_t	x_st_ino;
	x_unsigned_short x_st_mode;
	x_short	x_st_nlink;
	x_uid_t	x_st_uid;
	x_gid_t	x_st_gid;
	x_dev_t	x_st_rdev;
	x_off_t	x_st_size;
	x_time_t	x_st_atime;
	x_int	x_st_spare1;
	x_time_t	x_st_mtime;
	x_int	x_st_spare2;
	x_time_t	x_st_ctime;
	x_int	x_st_spare3;
	x_long	x_st_blksize;
	x_long	x_st_blocks;
	x_long	x_st_spare4[2];
};

#define	x_S_IFMT	0170000		/* type of file */
#define		x_S_IFDIR	0040000	/* directory */
#define		x_S_IFCHR	0020000	/* character special */
#define		x_S_IFBLK	0060000	/* block special */
#define		x_S_IFREG	0100000	/* regular */
#define		x_S_IFLNK	0120000	/* symbolic link */
#define		x_S_IFSOCK 0140000/* socket */
#define	x_S_ISUID	0004000		/* set user id on execution */
#define	x_S_ISGID	0002000		/* set group id on execution */
#define	x_S_ISVTX	0001000		/* save swapped text even after use */
#define	x_S_IREAD	0000400		/* read permission, owner */
#define	x_S_IWRITE 0000200	/* write permission, owner */
#define	x_S_IEXEC	0000100		/* execute/search permission, owner */
