#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)socketvar.h	7.1 (Berkeley) 6/4/86
 */

/*
 * Kernel structure per socket.
 * Contains send and receive buffer queues,
 * handle on protocol and pointer to protocol
 * private data and error information.
 */
struct x_socket {
	x_short	x_so_type;		/* generic type, see socket.h */
	x_short	x_so_options;		/* from socket call, see socket.h */
	x_short	x_so_linger;		/* time to linger while closing */
	x_short	x_so_state;		/* internal state flags SS_*, below */
	x_caddr_t	x_so_pcb;			/* protocol control block */
	struct	x_protosw *x_so_proto;	/* protocol handle */
/*
 * Variables for connection queueing.
 * Socket where accepts occur is so_head in all subsidiary sockets.
 * If so_head is 0, socket is not related to an accept.
 * For head socket so_q0 queues partially completed connections,
 * while so_q is a queue of connections ready to be accepted.
 * If a connection is aborted and it has so_head set, then
 * it has to be pulled out of either so_q0 or so_q.
 * We allow connections to queue up based on current queue lengths
 * and limit on number of queued connections for this socket.
 */
	struct	x_socket *x_so_head;	/* back pointer to accept socket */
	struct	x_socket *x_so_q0;		/* queue of partial connections */
	x_short	x_so_q0len;		/* partials on so_q0 */
	struct	x_socket *x_so_q;		/* queue of incoming connections */
	x_short	x_so_qlen;		/* number of connections on so_q */
	x_short	x_so_qlimit;		/* max number queued connections */
/*
 * Variables for socket buffering.
 */
	struct	x_sockbuf {
		x_u_short	x_sb_cc;		/* actual chars in buffer */
		x_u_short	x_sb_hiwat;	/* max actual char count */
		x_u_short	x_sb_mbcnt;	/* chars of mbufs used */
		x_u_short	x_sb_mbmax;	/* max chars of mbufs to use */
		x_u_short	x_sb_lowat;	/* low water mark (not used yet) */
		x_short	x_sb_timeo;	/* timeout (not used yet) */
		struct	x_mbuf *x_sb_mb;	/* the mbuf chain */
		struct	x_proc *x_sb_sel;	/* process selecting read/write */
		x_short	x_sb_flags;	/* flags, see below */
	} x_so_rcv, x_so_snd;
#define	x_SB_MAX		65535		/* max chars in sockbuf */
#define	x_SB_LOCK		0x01		/* lock on data queue (so_rcv only) */
#define	x_SB_WANT		0x02		/* someone is waiting to lock */
#define	x_SB_WAIT		0x04		/* someone is waiting for data/space */
#define	x_SB_SEL		0x08		/* buffer is selected */
#define	x_SB_COLL		0x10		/* collision selecting */
	x_short	x_so_timeo;		/* connection timeout */
	x_u_short	x_so_error;		/* error affecting connection */
	x_u_short	x_so_oobmark;		/* chars to oob mark */
	x_short	x_so_pgrp;		/* pgrp for signals */
};

/*
 * Socket state bits.
 */
#define	x_SS_NOFDREF		0x001	/* no file table ref any more */
#define	x_SS_ISCONNECTED		0x002	/* socket connected to a peer */
#define	x_SS_ISCONNECTING		0x004	/* in process of connecting to peer */
#define	x_SS_ISDISCONNECTING	0x008	/* in process of disconnecting */
#define	x_SS_CANTSENDMORE		0x010	/* can't send more data to peer */
#define	x_SS_CANTRCVMORE		0x020	/* can't receive more data from peer */
#define	x_SS_RCVATMARK		0x040	/* at mark on input */

#define	x_SS_PRIV			0x080	/* privileged for broadcast, raw... */
#define	x_SS_NBIO			0x100	/* non-blocking ops */
#define	x_SS_ASYNC		0x200	/* async i/o notify */


/*
 * Macros for sockets and socket buffering.
 */

/* how much space is there in a socket buffer (so->so_snd or so->so_rcv) */
#define	x_sbspace(x_sb) \
    (x_MIN((x_int)((x_sb)->x_sb_hiwat - (x_sb)->x_sb_cc),\
	 (x_int)((x_sb)->x_sb_mbmax - (x_sb)->x_sb_mbcnt)))

/* do we have to send all at once on a socket? */
#define	x_sosendallatonce(x_so) \
    ((x_so)->x_so_proto->x_pr_flags & x_PR_ATOMIC)

/* can we read something from so? */
#define	x_soreadable(x_so) \
    ((x_so)->x_so_rcv.x_sb_cc || ((x_so)->x_so_state & x_SS_CANTRCVMORE) || \
	(x_so)->x_so_qlen || (x_so)->x_so_error)

/* can we write something to so? */
#define	x_sowriteable(x_so) \
    (x_sbspace(&(x_so)->x_so_snd) > 0 && \
	(((x_so)->x_so_state&x_SS_ISCONNECTED) || \
	  ((x_so)->x_so_proto->x_pr_flags&x_PR_CONNREQUIRED)==0) || \
     ((x_so)->x_so_state & x_SS_CANTSENDMORE) || \
     (x_so)->x_so_error)

/* adjust counters in sb reflecting allocation of m */
#define	x_sballoc(x_sb, x_m) { \
	(x_sb)->x_sb_cc += (x_m)->x_m_len; \
	(x_sb)->x_sb_mbcnt += x_MSIZE; \
	if ((x_m)->x_m_off > x_MMAXOFF) \
		(x_sb)->x_sb_mbcnt += x_CLBYTES; \
}

/* adjust counters in sb reflecting freeing of m */
#define	x_sbfree(x_sb, x_m) { \
	(x_sb)->x_sb_cc -= (x_m)->x_m_len; \
	(x_sb)->x_sb_mbcnt -= x_MSIZE; \
	if ((x_m)->x_m_off > x_MMAXOFF) \
		(x_sb)->x_sb_mbcnt -= x_CLBYTES; \
}

/* set lock on sockbuf sb */
#define x_sblock(x_sb) { \
	while ((x_sb)->x_sb_flags & x_SB_LOCK) { \
		(x_sb)->x_sb_flags |= x_SB_WANT; \
		x_sleep((x_caddr_t)&(x_sb)->x_sb_flags, x_PZERO+1); \
	} \
	(x_sb)->x_sb_flags |= x_SB_LOCK; \
}

/* release lock on sockbuf sb */
#define	x_sbunlock(x_sb) { \
	(x_sb)->x_sb_flags &= ~x_SB_LOCK; \
	if ((x_sb)->x_sb_flags & x_SB_WANT) { \
		(x_sb)->x_sb_flags &= ~x_SB_WANT; \
		x_wakeup((x_caddr_t)&(x_sb)->x_sb_flags); \
	} \
}

#define	x_sorwakeup(x_so)	x_sowakeup((x_so), &(x_so)->x_so_rcv)
#define	x_sowwakeup(x_so)	x_sowakeup((x_so), &(x_so)->x_so_snd)

#ifdef x_KERNEL
struct	x_socket *x_sonewconn();
#endif
