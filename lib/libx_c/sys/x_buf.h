#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)buf.h	7.1 (Berkeley) 6/4/86
 */

/*
 * The header for buffers in the buffer pool and otherwise used
 * to describe a block i/o request is given here.  The routines
 * which manipulate these things are given in bio.c.
 *
 * Each buffer in the pool is usually doubly linked into 2 lists:
 * hashed into a chain by <dev,blkno> so it can be located in the cache,
 * and (usually) on (one of several) queues.  These lists are circular and
 * doubly linked for easy removal.
 *
 * There are currently three queues for buffers:
 *	one for buffers which must be kept permanently (super blocks)
 * 	one for buffers containing ``useful'' information (the cache)
 *	one for buffers containing ``non-useful'' information
 *		(and empty buffers, pushed onto the front)
 * The latter two queues contain the buffers which are available for
 * reallocation, are kept in lru order.  When not on one of these queues,
 * the buffers are ``checked out'' to drivers which use the available list
 * pointers to keep track of them in their i/o active queues.
 */

/*
 * Bufhd structures used at the head of the hashed buffer queues.
 * We only need three words for these, so this abbreviated
 * definition saves some space.
 */
struct x_bufhd
{
	x_long	x_b_flags;		/* see defines below */
	struct	x_buf *x_b_forw, *x_b_back;	/* fwd/bkwd pointer in chain */
};
struct x_buf
{
	x_long	x_b_flags;		/* too much goes here to describe */
	struct	x_buf *x_b_forw, *x_b_back;	/* hash chain (2 way street) */
	struct	x_buf *x_av_forw, *x_av_back;	/* position on free list if not BUSY */
#define	x_b_actf	x_av_forw			/* alternate names for driver queue */
#define	x_b_actl	x_av_back			/*    head - isn't history wonderful */
	x_long	x_b_bcount;		/* transfer count */
	x_long	x_b_bufsize;		/* size of allocated buffer */
#define	x_b_active x_b_bcount		/* driver queue head: drive active */
	x_short	x_b_error;		/* returned after I/O */
	x_dev_t	x_b_dev;			/* major+minor device name */
	union {
	    x_caddr_t x_b_addr;		/* low order core address */
	    x_int	*x_b_words;		/* words for clearing */
	    struct x_fs *x_b_fs;		/* superblocks */
	    struct x_csum *x_b_cs;		/* superblock summary information */
	    struct x_cg *x_b_cg;		/* cylinder group block */
	    struct x_dinode *x_b_dino;	/* ilist */
	    x_daddr_t *x_b_daddr;		/* indirect block */
	} x_b_un;
	x_daddr_t	x_b_blkno;		/* block # on device */
	x_long	x_b_resid;		/* words not transferred after error */
#define	x_b_errcnt x_b_resid		/* while i/o in progress: # retries */
	struct  x_proc *x_b_proc;		/* proc doing physical or swap I/O */
	x_int	(*x_b_iodone)();		/* function called by iodone */
	x_int	x_b_pfcent;		/* center page when swapping cluster */
};

#define	x_BQUEUES		4		/* number of free buffer queues */

#define	x_BQ_LOCKED	0		/* super-blocks &c */
#define	x_BQ_LRU		1		/* lru, useful buffers */
#define	x_BQ_AGE		2		/* rubbish */
#define	x_BQ_EMPTY	3		/* buffer headers with no memory */

#ifdef	x_KERNEL
#define	x_BUFHSZ	512
#define x_RND	(x_MAXBSIZE/x_DEV_BSIZE)
#if	((x_BUFHSZ&(x_BUFHSZ-1)) == 0)
#define	x_BUFHASH(x_dev, x_dblkno)	\
	((struct x_buf *)&x_bufhash[((x_int)(x_dev)+(((x_int)(x_dblkno))/x_RND))&(x_BUFHSZ-1)])
#else
#define	x_BUFHASH(x_dev, x_dblkno)	\
	((struct x_buf *)&x_bufhash[((x_int)(x_dev)+(((x_int)(x_dblkno))/x_RND)) % x_BUFHSZ])
#endif

struct	x_buf *x_buf;		/* the buffer pool itself */
char	*x_buffers;
x_int	x_nbuf;			/* number of buffer headers */
x_int	x_bufpages;		/* number of memory pages in the buffer pool */
struct	x_buf *x_swbuf;		/* swap I/O headers */
x_int	x_nswbuf;
struct	x_bufhd x_bufhash[x_BUFHSZ];	/* heads of hash lists */
struct	x_buf x_bfreelist[x_BQUEUES];	/* heads of available lists */
struct	x_buf x_bswlist;		/* head of free swap header list */
struct	x_buf *x_bclnlist;		/* head of cleaned page list */

struct	x_buf *x_alloc();
struct	x_buf *x_realloccg();
struct	x_buf *x_baddr();
struct	x_buf *x_getblk();
struct	x_buf *x_geteblk();
struct	x_buf *x_getnewbuf();
struct	x_buf *x_bread();
struct	x_buf *x_breada();

x_unsigned_int x_minphys();
#endif

/*
 * These flags are kept in b_flags.
 */
#define	x_B_WRITE		0x000000	/* non-read pseudo-flag */
#define	x_B_READ		0x000001	/* read when I/O occurs */
#define	x_B_DONE		0x000002	/* transaction finished */
#define	x_B_ERROR		0x000004	/* transaction aborted */
#define	x_B_BUSY		0x000008	/* not on av_forw/back list */
#define	x_B_PHYS		0x000010	/* physical IO */
#define	x_B_XXX		0x000020	/* was B_MAP, alloc UNIBUS on pdp-11 */
#define	x_B_WANTED	0x000040	/* issue wakeup when BUSY goes off */
#define	x_B_AGE		0x000080	/* delayed write for correct aging */
#define	x_B_ASYNC		0x000100	/* don't wait for I/O completion */
#define	x_B_DELWRI	0x000200	/* write at exit of avail list */
#define	x_B_TAPE		0x000400	/* this is a magtape (no bdwrite) */
#define	x_B_UAREA		0x000800	/* add u-area to a swap operation */
#define	x_B_PAGET		0x001000	/* page in/out of page table space */
#define	x_B_DIRTY		0x002000	/* dirty page to be pushed out async */
#define	x_B_PGIN		0x004000	/* pagein op, so swap() can count it */
#define	x_B_CACHE		0x008000	/* did bread find us in the cache ? */
#define	x_B_INVAL		0x010000	/* does not contain valid info  */
#define	x_B_LOCKED	0x020000	/* locked in core (not reusable) */
#define	x_B_HEAD		0x040000	/* a buffer header, not a buffer */
#define	x_B_BAD		0x100000	/* bad block revectoring in progress */
#define	x_B_CALL		0x200000	/* call b_iodone from iodone */

/*
 * Insq/Remq for the buffer hash lists.
 */
#define	x_bremhash(x_bp) { \
	(x_bp)->x_b_back->x_b_forw = (x_bp)->x_b_forw; \
	(x_bp)->x_b_forw->x_b_back = (x_bp)->x_b_back; \
}
#define	x_binshash(x_bp, x_dp) { \
	(x_bp)->x_b_forw = (x_dp)->x_b_forw; \
	(x_bp)->x_b_back = (x_dp); \
	(x_dp)->x_b_forw->x_b_back = (x_bp); \
	(x_dp)->x_b_forw = (x_bp); \
}

/*
 * Insq/Remq for the buffer free lists.
 */
#define	x_bremfree(x_bp) { \
	(x_bp)->x_av_back->x_av_forw = (x_bp)->x_av_forw; \
	(x_bp)->x_av_forw->x_av_back = (x_bp)->x_av_back; \
}
#define	x_binsheadfree(x_bp, x_dp) { \
	(x_dp)->x_av_forw->x_av_back = (x_bp); \
	(x_bp)->x_av_forw = (x_dp)->x_av_forw; \
	(x_dp)->x_av_forw = (x_bp); \
	(x_bp)->x_av_back = (x_dp); \
}
#define	x_binstailfree(x_bp, x_dp) { \
	(x_dp)->x_av_back->x_av_forw = (x_bp); \
	(x_bp)->x_av_back = (x_dp)->x_av_back; \
	(x_dp)->x_av_back = (x_bp); \
	(x_bp)->x_av_forw = (x_dp); \
}

/*
 * Take a buffer off the free list it's on and
 * mark it as being use (B_BUSY) by a device.
 */
#define	x_notavail(x_bp) { \
	x_int x_x = x_splbio(); \
	x_bremfree(x_bp); \
	(x_bp)->x_b_flags |= x_B_BUSY; \
	x_splx(x_x); \
}

#define	x_iodone	x_biodone
#define	x_iowait	x_biowait

/*
 * Zero out a buffer's data portion.
 */
#define	x_clrbuf(x_bp) { \
	x_blkclr((x_bp)->x_b_un.x_b_addr, (unsigned)(x_bp)->x_b_bcount); \
	(x_bp)->x_b_resid = 0; \
}
