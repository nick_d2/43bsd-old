#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)exec.h	7.1 (Berkeley) 6/4/86
 */

/*
 * Header prepended to each a.out file.
 */
struct x_exec {
	x_long	x_a_magic;	/* magic number */
x_unsigned_long	x_a_text;		/* size of text segment */
x_unsigned_long	x_a_data;		/* size of initialized data */
x_unsigned_long	x_a_bss;		/* size of uninitialized data */
x_unsigned_long	x_a_syms;		/* size of symbol table */
x_unsigned_long	x_a_entry;	/* entry point */
x_unsigned_long	x_a_trsize;	/* size of text relocation */
x_unsigned_long	x_a_drsize;	/* size of data relocation */
};

#define	x_OMAGIC	0407		/* old impure format */
#define	x_NMAGIC	0410		/* read-only text */
#define	x_ZMAGIC	0413		/* demand load format */
