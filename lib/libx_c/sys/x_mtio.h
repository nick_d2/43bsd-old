#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)mtio.h	7.1 (Berkeley) 6/4/86
 */

/*
 * Structures and definitions for mag tape io control commands
 */

/* structure for MTIOCTOP - mag tape op command */
struct	x_mtop	{
	x_short	x_mt_op;		/* operations defined below */
	x_daddr_t	x_mt_count;	/* how many of them */
};

/* operations */
#define x_MTWEOF	0	/* write an end-of-file record */
#define x_MTFSF	1	/* forward space file */
#define x_MTBSF	2	/* backward space file */
#define x_MTFSR	3	/* forward space record */
#define x_MTBSR	4	/* backward space record */
#define x_MTREW	5	/* rewind */
#define x_MTOFFL	6	/* rewind and put the drive offline */
#define x_MTNOP	7	/* no operation, sets status only */
#define x_MTCACHE	8	/* enable controller cache */
#define x_MTNOCACHE 9	/* disable controller cache */

/* structure for MTIOCGET - mag tape get status command */

struct	x_mtget	{
	x_short	x_mt_type;	/* type of magtape device */
/* the following two registers are grossly device dependent */
	x_short	x_mt_dsreg;	/* ``drive status'' register */
	x_short	x_mt_erreg;	/* ``error'' register */
/* end device-dependent registers */
	x_short	x_mt_resid;	/* residual count */
/* the following two are not yet implemented */
	x_daddr_t	x_mt_fileno;	/* file number of current position */
	x_daddr_t	x_mt_blkno;	/* block number of current position */
/* end not yet implemented */
};

/*
 * Constants for mt_type byte.  These are the same
 * for controllers compatible with the types listed.
 */
#define	x_MT_ISTS		0x01		/* TS-11 */
#define	x_MT_ISHT		0x02		/* TM03 Massbus: TE16, TU45, TU77 */
#define	x_MT_ISTM		0x03		/* TM11/TE10 Unibus */
#define	x_MT_ISMT		0x04		/* TM78/TU78 Massbus */
#define	x_MT_ISUT		0x05		/* SI TU-45 emulation on Unibus */
#define	x_MT_ISCPC	0x06		/* SUN */
#define	x_MT_ISAR		0x07		/* SUN */
#define	x_MT_ISTMSCP	0x08		/* DEC TMSCP protocol (TU81, TK50) */

/* mag tape io control commands */
#define	x_MTIOCTOP	x__IOW(x_m, 1, struct x_mtop)		/* do a mag tape op */
#define	x_MTIOCGET	x__IOR(x_m, 2, struct x_mtget)	/* get tape status */
#define x_MTIOCIEOT	x__IO(x_m, 3)			/* ignore EOT error */
#define x_MTIOCEEOT	x__IO(x_m, 4)			/* enable EOT error */

#ifndef x_KERNEL
#define	x_DEFTAPE	"/dev/rmt12"
#endif
