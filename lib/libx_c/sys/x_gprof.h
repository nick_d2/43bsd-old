#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)gprof.h	7.1 (Berkeley) 6/4/86
 */

struct x_phdr {
    char	*x_lpc;
    char	*x_hpc;
    x_int		x_ncnt;
};

    /*
     *	histogram counters are unsigned shorts (according to the kernel).
     */
#define	x_HISTCOUNTER	x_unsigned_short

    /*
     *	fraction of text space to allocate for histogram counters
     *	here, 1/2
     */
#define	x_HISTFRACTION	2

    /*
     *	Fraction of text space to allocate for from hash buckets.
     *	The value of HASHFRACTION is based on the minimum number of bytes
     *	of separation between two subroutine call points in the object code.
     *	Given MIN_SUBR_SEPARATION bytes of separation the value of
     *	HASHFRACTION is calculated as:
     *
     *		HASHFRACTION = MIN_SUBR_SEPARATION / (2 * sizeof(short) - 1);
     *
     *	For the VAX, the shortest two call sequence is:
     *
     *		calls	$0,(r0)
     *		calls	$0,(r0)
     *
     *	which is separated by only three bytes, thus HASHFRACTION is 
     *	calculated as:
     *
     *		HASHFRACTION = 3 / (2 * 2 - 1) = 1
     *
     *	Note that the division above rounds down, thus if MIN_SUBR_FRACTION
     *	is less than three, this algorithm will not work!
     *
     *	NB: for the kernel we assert that the shortest two call sequence is:
     *
     *		calls	$0,_name
     *		calls	$0,_name
     *
     *	which is separated by seven bytes, thus HASHFRACTION is calculated as:
     *
     *		HASHFRACTION = 7 / (2 * 2 - 1) = 2
     */
#define	x_HASHFRACTION	2

    /*
     *	percent of text space to allocate for tostructs
     *	with a minimum.
     */
#define x_ARCDENSITY	2
#define x_MINARCS		50

struct x_tostruct {
    char		*x_selfpc;
    x_long		x_count;
    x_unsigned_short	x_link;
};

    /*
     *	a raw arc,
     *	    with pointers to the calling site and the called site
     *	    and a count.
     */
struct x_rawarc {
    x_unsigned_long	x_raw_frompc;
    x_unsigned_long	x_raw_selfpc;
    x_long		x_raw_count;
};

    /*
     *	general rounding functions.
     */
#define x_ROUNDDOWN(x_x,x_y)	(((x_x)/(x_y))*(x_y))
#define x_ROUNDUP(x_x,x_y)	((((x_x)+(x_y)-1)/(x_y))*(x_y))
