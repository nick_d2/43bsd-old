#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)vtimes.h	7.1 (Berkeley) 6/4/86
 */

/*
 * Structure returned by vtimes() and in vwait().
 * In vtimes() two of these are returned, one for the process itself
 * and one for all its children.  In vwait() these are combined
 * by adding componentwise (except for maxrss, which is max'ed).
 */
struct x_vtimes {
	x_int	x_vm_utime;		/* user time (60'ths) */
	x_int	x_vm_stime;		/* system time (60'ths) */
	/* divide next two by utime+stime to get averages */
	x_unsigned_int x_vm_idsrss;		/* integral of d+s rss */
	x_unsigned_int x_vm_ixrss;		/* integral of text rss */
	x_int	x_vm_maxrss;		/* maximum rss */
	x_int	x_vm_majflt;		/* major page faults */
	x_int	x_vm_minflt;		/* minor page faults */
	x_int	x_vm_nswap;		/* number of swaps */
	x_int	x_vm_inblk;		/* block reads */
	x_int	x_vm_oublk;		/* block writes */
};

#ifdef x_KERNEL
#endif

#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

/* compat-4.1/vtimes.c */
x_int x_vtimes __P((register struct x_vtimes *x_par, register struct x_vtimes *x_chi));
