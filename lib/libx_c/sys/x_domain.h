#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)domain.h	7.1 (Berkeley) 6/4/86
 */

/*
 * Structure per communications domain.
 */
struct	x_domain {
	x_int	x_dom_family;		/* AF_xxx */
	char	*x_dom_name;
	x_int	(*x_dom_init)();		/* initialize domain data structures */
	x_int	(*x_dom_externalize)();	/* externalize access rights */
	x_int	(*x_dom_dispose)();	/* dispose of internalized rights */
	struct	x_protosw *x_dom_protosw, *x_dom_protoswNPROTOSW;
	struct	x_domain *x_dom_next;
};

#ifdef x_KERNEL
struct	x_domain *x_domains;
#endif
