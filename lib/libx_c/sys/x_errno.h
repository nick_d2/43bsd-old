#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)errno.h	7.1 (Berkeley) 6/4/86
 */

/*
 * Error codes
 */

#define	x_EPERM		1		/* Not owner */
#define	x_ENOENT		2		/* No such file or directory */
#define	x_ESRCH		3		/* No such process */
#define	x_EINTR		4		/* Interrupted system call */
#define	x_EIO		5		/* I/O error */
#define	x_ENXIO		6		/* No such device or address */
#define	x_E2BIG		7		/* Arg list too long */
#define	x_ENOEXEC		8		/* Exec format error */
#define	x_EBADF		9		/* Bad file number */
#define	x_ECHILD		10		/* No children */
#define	x_EAGAIN		11		/* No more processes */
#define	x_ENOMEM		12		/* Not enough core */
#define	x_EACCES		13		/* Permission denied */
#define	x_EFAULT		14		/* Bad address */
#define	x_ENOTBLK		15		/* Block device required */
#define	x_EBUSY		16		/* Mount device busy */
#define	x_EEXIST		17		/* File exists */
#define	x_EXDEV		18		/* Cross-device link */
#define	x_ENODEV		19		/* No such device */
#define	x_ENOTDIR		20		/* Not a directory*/
#define	x_EISDIR		21		/* Is a directory */
#define	x_EINVAL		22		/* Invalid argument */
#define	x_ENFILE		23		/* File table overflow */
#define	x_EMFILE		24		/* Too many open files */
#define	x_ENOTTY		25		/* Not a typewriter */
#define	x_ETXTBSY		26		/* Text file busy */
#define	x_EFBIG		27		/* File too large */
#define	x_ENOSPC		28		/* No space left on device */
#define	x_ESPIPE		29		/* Illegal seek */
#define	x_EROFS		30		/* Read-only file system */
#define	x_EMLINK		31		/* Too many links */
#define	x_EPIPE		32		/* Broken pipe */

/* math software */
#define	x_EDOM		33		/* Argument too large */
#define	x_ERANGE		34		/* Result too large */

/* non-blocking and interrupt i/o */
#define	x_EWOULDBLOCK	35		/* Operation would block */
#define	x_EDEADLK		x_EWOULDBLOCK	/* ditto */
#define	x_EINPROGRESS	36		/* Operation now in progress */
#define	x_EALREADY	37		/* Operation already in progress */

/* ipc/network software */

	/* argument errors */
#define	x_ENOTSOCK	38		/* Socket operation on non-socket */
#define	x_EDESTADDRREQ	39		/* Destination address required */
#define	x_EMSGSIZE	40		/* Message too long */
#define	x_EPROTOTYPE	41		/* Protocol wrong type for socket */
#define	x_ENOPROTOOPT	42		/* Protocol not available */
#define	x_EPROTONOSUPPORT	43		/* Protocol not supported */
#define	x_ESOCKTNOSUPPORT	44		/* Socket type not supported */
#define	x_EOPNOTSUPP	45		/* Operation not supported on socket */
#define	x_EPFNOSUPPORT	46		/* Protocol family not supported */
#define	x_EAFNOSUPPORT	47		/* Address family not supported by protocol family */
#define	x_EADDRINUSE	48		/* Address already in use */
#define	x_EADDRNOTAVAIL	49		/* Can't assign requested address */

	/* operational errors */
#define	x_ENETDOWN	50		/* Network is down */
#define	x_ENETUNREACH	51		/* Network is unreachable */
#define	x_ENETRESET	52		/* Network dropped connection on reset */
#define	x_ECONNABORTED	53		/* Software caused connection abort */
#define	x_ECONNRESET	54		/* Connection reset by peer */
#define	x_ENOBUFS		55		/* No buffer space available */
#define	x_EISCONN		56		/* Socket is already connected */
#define	x_ENOTCONN	57		/* Socket is not connected */
#define	x_ESHUTDOWN	58		/* Can't send after socket shutdown */
#define	x_ETOOMANYREFS	59		/* Too many references: can't splice */
#define	x_ETIMEDOUT	60		/* Connection timed out */
#define	x_ECONNREFUSED	61		/* Connection refused */

	/* */
#define	x_ELOOP		62		/* Too many levels of symbolic links */
#define	x_ENAMETOOLONG	63		/* File name too long */

/* should be rearranged */
#define	x_EHOSTDOWN	64		/* Host is down */
#define	x_EHOSTUNREACH	65		/* No route to host */
#define	x_ENOTEMPTY	66		/* Directory not empty */

/* quotas & mush */
#define	x_EPROCLIM	67		/* Too many processes */
#define	x_EUSERS		68		/* Too many users */
#define	x_EDQUOT		69		/* Disc quota exceeded */
