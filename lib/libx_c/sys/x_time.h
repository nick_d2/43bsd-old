#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)time.h	7.1 (Berkeley) 6/4/86
 */

/*
 * Structure returned by gettimeofday(2) system call,
 * and used in other calls.
 */
struct x_timeval {
	x_long	x_tv_sec;		/* seconds */
	x_long	x_tv_usec;	/* and microseconds */
};

struct x_timezone {
	x_int	x_tz_minuteswest;	/* minutes west of Greenwich */
	x_int	x_tz_dsttime;	/* type of dst correction */
};
#define	x_DST_NONE	0	/* not on dst */
#define	x_DST_USA		1	/* USA style dst */
#define	x_DST_AUST	2	/* Australian style dst */
#define	x_DST_WET		3	/* Western European dst */
#define	x_DST_MET		4	/* Middle European dst */
#define	x_DST_EET		5	/* Eastern European dst */
#define	x_DST_CAN		6	/* Canada */

/*
 * Operations on timevals.
 *
 * NB: timercmp does not work for >= or <=.
 */
#define	x_timerisset(x_tvp)		((x_tvp)->x_tv_sec || (x_tvp)->x_tv_usec)
#define	x_timercmp(x_tvp, x_uvp, x_cmp)	\
	((x_tvp)->x_tv_sec x_cmp (x_uvp)->x_tv_sec || \
	 (x_tvp)->x_tv_sec == (x_uvp)->x_tv_sec && (x_tvp)->x_tv_usec x_cmp (x_uvp)->x_tv_usec)
#define	x_timerclear(x_tvp)		(x_tvp)->x_tv_sec = (x_tvp)->x_tv_usec = 0

/*
 * Names of the interval timers, and structure
 * defining a timer setting.
 */
#define	x_ITIMER_REAL	0
#define	x_ITIMER_VIRTUAL	1
#define	x_ITIMER_PROF	2

struct	x_itimerval {
	struct	x_timeval x_it_interval;	/* timer interval */
	struct	x_timeval x_it_value;	/* current value */
};

#ifndef x_KERNEL
#include <x_time.h>
#endif

#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

/* gen/time.c */
x_long x_time __P((x_time_t *x_t));
/* gen/timezone.c */
char *x_timezone __P((x_int x_zone, x_int x_dst));
