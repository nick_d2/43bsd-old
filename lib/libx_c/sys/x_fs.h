#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)fs.h	7.1 (Berkeley) 6/4/86
 */

/*
 * Each disk drive contains some number of file systems.
 * A file system consists of a number of cylinder groups.
 * Each cylinder group has inodes and data.
 *
 * A file system is described by its super-block, which in turn
 * describes the cylinder groups.  The super-block is critical
 * data and is replicated in each cylinder group to protect against
 * catastrophic loss.  This is done at mkfs time and the critical
 * super-block data does not change, so the copies need not be
 * referenced further unless disaster strikes.
 *
 * For file system fs, the offsets of the various blocks of interest
 * are given in the super block as:
 *	[fs->fs_sblkno]		Super-block
 *	[fs->fs_cblkno]		Cylinder group block
 *	[fs->fs_iblkno]		Inode blocks
 *	[fs->fs_dblkno]		Data blocks
 * The beginning of cylinder group cg in fs, is given by
 * the ``cgbase(fs, cg)'' macro.
 *
 * The first boot and super blocks are given in absolute disk addresses.
 */
#define x_BBSIZE		8192
#define x_SBSIZE		8192
#define	x_BBLOCK		((x_daddr_t)(0))
#define	x_SBLOCK		((x_daddr_t)(x_BBLOCK + x_BBSIZE / x_DEV_BSIZE))

/*
 * Addresses stored in inodes are capable of addressing fragments
 * of `blocks'. File system blocks of at most size MAXBSIZE can 
 * be optionally broken into 2, 4, or 8 pieces, each of which is
 * addressible; these pieces may be DEV_BSIZE, or some multiple of
 * a DEV_BSIZE unit.
 *
 * Large files consist of exclusively large data blocks.  To avoid
 * undue wasted disk space, the last data block of a small file may be
 * allocated as only as many fragments of a large block as are
 * necessary.  The file system format retains only a single pointer
 * to such a fragment, which is a piece of a single large block that
 * has been divided.  The size of such a fragment is determinable from
 * information in the inode, using the ``blksize(fs, ip, lbn)'' macro.
 *
 * The file system records space availability at the fragment level;
 * to determine block availability, aligned fragments are examined.
 *
 * The root inode is the root of the file system.
 * Inode 0 can't be used for normal purposes and
 * historically bad blocks were linked to inode 1,
 * thus the root inode is 2. (inode 1 is no longer used for
 * this purpose, however numerous dump tapes make this
 * assumption, so we are stuck with it)
 * The lost+found directory is given the next available
 * inode when it is created by ``mkfs''.
 */
#define	x_ROOTINO		((x_ino_t)2)	/* i number of all roots */
#define x_LOSTFOUNDINO	(x_ROOTINO + 1)

/*
 * Cylinder group related limits.
 *
 * For each cylinder we keep track of the availability of blocks at different
 * rotational positions, so that we can lay out the data to be picked
 * up with minimum rotational latency.  NRPOS is the number of rotational
 * positions which we distinguish.  With NRPOS 8 the resolution of our
 * summary information is 2ms for a typical 3600 rpm drive.
 */
#define	x_NRPOS		8	/* number distinct rotational positions */

/*
 * MAXIPG bounds the number of inodes per cylinder group, and
 * is needed only to keep the structure simpler by having the
 * only a single variable size element (the free bit map).
 *
 * N.B.: MAXIPG must be a multiple of INOPB(fs).
 */
#define	x_MAXIPG		2048	/* max number inodes/cyl group */

/*
 * MINBSIZE is the smallest allowable block size.
 * In order to insure that it is possible to create files of size
 * 2^32 with only two levels of indirection, MINBSIZE is set to 4096.
 * MINBSIZE must be big enough to hold a cylinder group block,
 * thus changes to (struct cg) must keep its size within MINBSIZE.
 * MAXCPG is limited only to dimension an array in (struct cg);
 * it can be made larger as long as that structures size remains
 * within the bounds dictated by MINBSIZE.
 * Note that super blocks are always of size SBSIZE,
 * and that both SBSIZE and MAXBSIZE must be >= MINBSIZE.
 */
#define x_MINBSIZE	4096
#define	x_MAXCPG		32	/* maximum fs_cpg */

/*
 * The path name on which the file system is mounted is maintained
 * in fs_fsmnt. MAXMNTLEN defines the amount of space allocated in 
 * the super block for this name.
 * The limit on the amount of summary information per file system
 * is defined by MAXCSBUFS. It is currently parameterized for a
 * maximum of two million cylinders.
 */
#define x_MAXMNTLEN 512
#define x_MAXCSBUFS 32

/*
 * Per cylinder group information; summarized in blocks allocated
 * from first cylinder group data blocks.  These blocks have to be
 * read in from fs_csaddr (size fs_cssize) in addition to the
 * super block.
 *
 * N.B. sizeof(struct csum) must be a power of two in order for
 * the ``fs_cs'' macro to work (see below).
 */
struct x_csum {
	x_long	x_cs_ndir;	/* number of directories */
	x_long	x_cs_nbfree;	/* number of free blocks */
	x_long	x_cs_nifree;	/* number of free inodes */
	x_long	x_cs_nffree;	/* number of free frags */
};

/*
 * Super block for a file system.
 */
#define	x_FS_MAGIC	0x011954
struct	x_fs
{
	struct	x_fs *x_fs_link;		/* linked list of file systems */
	struct	x_fs *x_fs_rlink;		/*     used for incore super blocks */
	x_daddr_t	x_fs_sblkno;		/* addr of super-block in filesys */
	x_daddr_t	x_fs_cblkno;		/* offset of cyl-block in filesys */
	x_daddr_t	x_fs_iblkno;		/* offset of inode-blocks in filesys */
	x_daddr_t	x_fs_dblkno;		/* offset of first data after cg */
	x_long	x_fs_cgoffset;		/* cylinder group offset in cylinder */
	x_long	x_fs_cgmask;		/* used to calc mod fs_ntrak */
	x_time_t 	x_fs_time;    		/* last time written */
	x_long	x_fs_size;		/* number of blocks in fs */
	x_long	x_fs_dsize;		/* number of data blocks in fs */
	x_long	x_fs_ncg;			/* number of cylinder groups */
	x_long	x_fs_bsize;		/* size of basic blocks in fs */
	x_long	x_fs_fsize;		/* size of frag blocks in fs */
	x_long	x_fs_frag;		/* number of frags in a block in fs */
/* these are configuration parameters */
	x_long	x_fs_minfree;		/* minimum percentage of free blocks */
	x_long	x_fs_rotdelay;		/* num of ms for optimal next block */
	x_long	x_fs_rps;			/* disk revolutions per second */
/* these fields can be computed from the others */
	x_long	x_fs_bmask;		/* ``blkoff'' calc of blk offsets */
	x_long	x_fs_fmask;		/* ``fragoff'' calc of frag offsets */
	x_long	x_fs_bshift;		/* ``lblkno'' calc of logical blkno */
	x_long	x_fs_fshift;		/* ``numfrags'' calc number of frags */
/* these are configuration parameters */
	x_long	x_fs_maxcontig;		/* max number of contiguous blks */
	x_long	x_fs_maxbpg;		/* max number of blks per cyl group */
/* these fields can be computed from the others */
	x_long	x_fs_fragshift;		/* block to frag shift */
	x_long	x_fs_fsbtodb;		/* fsbtodb and dbtofsb shift constant */
	x_long	x_fs_sbsize;		/* actual size of super block */
	x_long	x_fs_csmask;		/* csum block offset */
	x_long	x_fs_csshift;		/* csum block number */
	x_long	x_fs_nindir;		/* value of NINDIR */
	x_long	x_fs_inopb;		/* value of INOPB */
	x_long	x_fs_nspf;		/* value of NSPF */
	x_long	x_fs_optim;		/* optimization preference, see below */
	x_long	x_fs_sparecon[5];		/* reserved for future constants */
/* sizes determined by number of cylinder groups and their sizes */
	x_daddr_t x_fs_csaddr;		/* blk addr of cyl grp summary area */
	x_long	x_fs_cssize;		/* size of cyl grp summary area */
	x_long	x_fs_cgsize;		/* cylinder group size */
/* these fields should be derived from the hardware */
	x_long	x_fs_ntrak;		/* tracks per cylinder */
	x_long	x_fs_nsect;		/* sectors per track */
	x_long  	x_fs_spc;   		/* sectors per cylinder */
/* this comes from the disk driver partitioning */
	x_long	x_fs_ncyl;   		/* cylinders in file system */
/* these fields can be computed from the others */
	x_long	x_fs_cpg;			/* cylinders per group */
	x_long	x_fs_ipg;			/* inodes per group */
	x_long	x_fs_fpg;			/* blocks per group * fs_frag */
/* this data must be re-computed after crashes */
	struct	x_csum x_fs_cstotal;	/* cylinder summary information */
/* these fields are cleared at mount time */
	char   	x_fs_fmod;    		/* super block modified flag */
	char   	x_fs_clean;    		/* file system is clean flag */
	char   	x_fs_ronly;   		/* mounted read-only flag */
	char   	x_fs_flags;   		/* currently unused flag */
	char	x_fs_fsmnt[x_MAXMNTLEN];	/* name mounted on */
/* these fields retain the current block allocation info */
	x_long	x_fs_cgrotor;		/* last cg searched */
	struct	x_csum *x_fs_csp[x_MAXCSBUFS];/* list of fs_cs info buffers */
	x_long	x_fs_cpc;			/* cyl per cycle in postbl */
	x_short	x_fs_postbl[x_MAXCPG][x_NRPOS];/* head of blocks for each rotation */
	x_long	x_fs_magic;		/* magic number */
	x_u_char	x_fs_rotbl[1];		/* list of blocks for each rotation */
/* actually longer */
};
/*
 * Preference for optimization.
 */
#define x_FS_OPTTIME	0	/* minimize allocation time */
#define x_FS_OPTSPACE	1	/* minimize disk fragmentation */

/*
 * Convert cylinder group to base address of its global summary info.
 *
 * N.B. This macro assumes that sizeof(struct csum) is a power of two.
 */
#define x_fs_cs(x_fs, x_indx) \
	x_fs_csp[(x_indx) >> (x_fs)->x_fs_csshift][(x_indx) & ~(x_fs)->x_fs_csmask]

/*
 * MAXBPC bounds the size of the rotational layout tables and
 * is limited by the fact that the super block is of size SBSIZE.
 * The size of these tables is INVERSELY proportional to the block
 * size of the file system. It is aggravated by sector sizes that
 * are not powers of two, as this increases the number of cylinders
 * included before the rotational pattern repeats (fs_cpc).
 * Its size is derived from the number of bytes remaining in (struct fs)
 */
#define	x_MAXBPC	(x_SBSIZE - sizeof (struct x_fs))

/*
 * Cylinder group block for a file system.
 */
#define	x_CG_MAGIC	0x090255
struct	x_cg {
	struct	x_cg *x_cg_link;		/* linked list of cyl groups */
	struct	x_cg *x_cg_rlink;		/*     used for incore cyl groups */
	x_time_t	x_cg_time;		/* time last written */
	x_long	x_cg_cgx;			/* we are the cgx'th cylinder group */
	x_short	x_cg_ncyl;		/* number of cyl's this cg */
	x_short	x_cg_niblk;		/* number of inode blocks this cg */
	x_long	x_cg_ndblk;		/* number of data blocks this cg */
	struct	x_csum x_cg_cs;		/* cylinder summary information */
	x_long	x_cg_rotor;		/* position of last used block */
	x_long	x_cg_frotor;		/* position of last used frag */
	x_long	x_cg_irotor;		/* position of last used inode */
	x_long	x_cg_frsum[x_MAXFRAG];	/* counts of available frags */
	x_long	x_cg_btot[x_MAXCPG];	/* block totals per cylinder */
	x_short	x_cg_b[x_MAXCPG][x_NRPOS];	/* positions of free blocks */
	char	x_cg_iused[x_MAXIPG/x_NBBY];	/* used inode map */
	x_long	x_cg_magic;		/* magic number */
	x_u_char	x_cg_free[1];		/* free block map */
/* actually longer */
};

/*
 * MAXBPG bounds the number of blocks of data per cylinder group,
 * and is limited by the fact that cylinder groups are at most one block.
 * Its size is derived from the size of blocks and the (struct cg) size,
 * by the number of remaining bits.
 */
#define	x_MAXBPG(x_fs) \
	(x_fragstoblks((x_fs), (x_NBBY * ((x_fs)->x_fs_bsize - (sizeof (struct x_cg))))))

/*
 * Turn file system block numbers into disk block addresses.
 * This maps file system blocks to device size blocks.
 */
#define x_fsbtodb(x_fs, x_b)	((x_b) << (x_fs)->x_fs_fsbtodb)
#define	x_dbtofsb(x_fs, x_b)	((x_b) >> (x_fs)->x_fs_fsbtodb)

/*
 * Cylinder group macros to locate things in cylinder groups.
 * They calc file system addresses of cylinder group data structures.
 */
#define	x_cgbase(x_fs, x_c)	((x_daddr_t)((x_fs)->x_fs_fpg * (x_c)))
#define x_cgstart(x_fs, x_c) \
	(x_cgbase(x_fs, x_c) + (x_fs)->x_fs_cgoffset * ((x_c) & ~((x_fs)->x_fs_cgmask)))
#define	x_cgsblock(x_fs, x_c)	(x_cgstart(x_fs, x_c) + (x_fs)->x_fs_sblkno)	/* super blk */
#define	x_cgtod(x_fs, x_c)	(x_cgstart(x_fs, x_c) + (x_fs)->x_fs_cblkno)	/* cg block */
#define	x_cgimin(x_fs, x_c)	(x_cgstart(x_fs, x_c) + (x_fs)->x_fs_iblkno)	/* inode blk */
#define	x_cgdmin(x_fs, x_c)	(x_cgstart(x_fs, x_c) + (x_fs)->x_fs_dblkno)	/* 1st data */

/*
 * Macros for handling inode numbers:
 *     inode number to file system block offset.
 *     inode number to cylinder group number.
 *     inode number to file system block address.
 */
#define	x_itoo(x_fs, x_x)	((x_x) % x_INOPB(x_fs))
#define	x_itog(x_fs, x_x)	((x_x) / (x_fs)->x_fs_ipg)
#define	x_itod(x_fs, x_x) \
	((x_daddr_t)(x_cgimin(x_fs, x_itog(x_fs, x_x)) + \
	(x_blkstofrags((x_fs), (((x_x) % (x_fs)->x_fs_ipg) / x_INOPB(x_fs))))))

/*
 * Give cylinder group number for a file system block.
 * Give cylinder group block number for a file system block.
 */
#define	x_dtog(x_fs, x_d)	((x_d) / (x_fs)->x_fs_fpg)
#define	x_dtogd(x_fs, x_d)	((x_d) % (x_fs)->x_fs_fpg)

/*
 * Extract the bits for a block from a map.
 * Compute the cylinder and rotational position of a cyl block addr.
 */
#define x_blkmap(x_fs, x_map, x_loc) \
    (((x_map)[(x_loc) / x_NBBY] >> ((x_loc) % x_NBBY)) & (0xff >> (x_NBBY - (x_fs)->x_fs_frag)))
#define x_cbtocylno(x_fs, x_bno) \
    ((x_bno) * x_NSPF(x_fs) / (x_fs)->x_fs_spc)
#define x_cbtorpos(x_fs, x_bno) \
    ((x_bno) * x_NSPF(x_fs) % (x_fs)->x_fs_spc % (x_fs)->x_fs_nsect * x_NRPOS / (x_fs)->x_fs_nsect)

/*
 * The following macros optimize certain frequently calculated
 * quantities by using shifts and masks in place of divisions
 * modulos and multiplications.
 */
#define x_blkoff(x_fs, x_loc)		/* calculates (loc % fs->fs_bsize) */ \
	((x_loc) & ~(x_fs)->x_fs_bmask)
#define x_fragoff(x_fs, x_loc)	/* calculates (loc % fs->fs_fsize) */ \
	((x_loc) & ~(x_fs)->x_fs_fmask)
#define x_lblkno(x_fs, x_loc)		/* calculates (loc / fs->fs_bsize) */ \
	((x_loc) >> (x_fs)->x_fs_bshift)
#define x_numfrags(x_fs, x_loc)	/* calculates (loc / fs->fs_fsize) */ \
	((x_loc) >> (x_fs)->x_fs_fshift)
#define x_blkroundup(x_fs, x_size)	/* calculates roundup(size, fs->fs_bsize) */ \
	(((x_size) + (x_fs)->x_fs_bsize - 1) & (x_fs)->x_fs_bmask)
#define x_fragroundup(x_fs, x_size)	/* calculates roundup(size, fs->fs_fsize) */ \
	(((x_size) + (x_fs)->x_fs_fsize - 1) & (x_fs)->x_fs_fmask)
#define x_fragstoblks(x_fs, x_frags)	/* calculates (frags / fs->fs_frag) */ \
	((x_frags) >> (x_fs)->x_fs_fragshift)
#define x_blkstofrags(x_fs, x_blks)	/* calculates (blks * fs->fs_frag) */ \
	((x_blks) << (x_fs)->x_fs_fragshift)
#define x_fragnum(x_fs, x_fsb)	/* calculates (fsb % fs->fs_frag) */ \
	((x_fsb) & ((x_fs)->x_fs_frag - 1))
#define x_blknum(x_fs, x_fsb)		/* calculates rounddown(fsb, fs->fs_frag) */ \
	((x_fsb) &~ ((x_fs)->x_fs_frag - 1))

/*
 * Determine the number of available frags given a
 * percentage to hold in reserve
 */
#define x_freespace(x_fs, x_percentreserved) \
	(x_blkstofrags((x_fs), (x_fs)->x_fs_cstotal.x_cs_nbfree) + \
	(x_fs)->x_fs_cstotal.x_cs_nffree - ((x_fs)->x_fs_dsize * (x_percentreserved) / 100))

/*
 * Determining the size of a file block in the file system.
 */
#define x_blksize(x_fs, x_ip, x_lbn) \
	(((x_lbn) >= x_NDADDR || (x_ip)->x_i_size >= ((x_lbn) + 1) << (x_fs)->x_fs_bshift) \
	    ? (x_fs)->x_fs_bsize \
	    : (x_fragroundup(x_fs, x_blkoff(x_fs, (x_ip)->x_i_size))))
#define x_dblksize(x_fs, x_dip, x_lbn) \
	(((x_lbn) >= x_NDADDR || (x_dip)->x_di_size >= ((x_lbn) + 1) << (x_fs)->x_fs_bshift) \
	    ? (x_fs)->x_fs_bsize \
	    : (x_fragroundup(x_fs, x_blkoff(x_fs, (x_dip)->x_di_size))))

/*
 * Number of disk sectors per block; assumes DEV_BSIZE byte sector size.
 */
#define	x_NSPB(x_fs)	((x_fs)->x_fs_nspf << (x_fs)->x_fs_fragshift)
#define	x_NSPF(x_fs)	((x_fs)->x_fs_nspf)

/*
 * INOPB is the number of inodes in a secondary storage block.
 */
#define	x_INOPB(x_fs)	((x_fs)->x_fs_inopb)
#define	x_INOPF(x_fs)	((x_fs)->x_fs_inopb >> (x_fs)->x_fs_fragshift)

/*
 * NINDIR is the number of indirects in a file system block.
 */
#define	x_NINDIR(x_fs)	((x_fs)->x_fs_nindir)

#ifdef x_KERNEL
struct	x_fs *x_getfs();
struct	x_fs *x_mountfs();
#endif
