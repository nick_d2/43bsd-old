#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)uio.h	7.1 (Berkeley) 6/4/86
 */

#ifndef x__UIO_
#define	x__UIO_

struct x_iovec {
	x_caddr_t	x_iov_base;
	x_int	x_iov_len;
};

struct x_uio {
	struct	x_iovec *x_uio_iov;
	x_int	x_uio_iovcnt;
	x_off_t	x_uio_offset;
	x_int	x_uio_segflg;
	x_int	x_uio_resid;
};

enum	x_uio_rw { x_UIO_READ, x_UIO_WRITE };

/*
 * Segment flag values (should be enum).
 */
#define x_UIO_USERSPACE	0		/* from user data space */
#define x_UIO_SYSSPACE	1		/* from system space */
#define x_UIO_USERISPACE	2		/* from user I space */
#endif
