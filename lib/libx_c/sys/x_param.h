#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)param.h	7.1 (Berkeley) 6/4/86
 */

#define	x_BSD	43		/* 4.3 * 10, as cpp doesn't do floats */
#define x_BSD4_3	1

/*
 * Machine type dependent parameters.
 */
#ifdef x_KERNEL
#include "../machine/x_machparam.h"
#else
#include <machine/x_machparam.h>
#endif

#define	x_NPTEPG		(x_NBPG/(sizeof (struct x_pte)))

/*
 * Machine-independent constants
 */
#define	x_NMOUNT	20		/* number of mountable file systems */
/* NMOUNT must be <= 255 unless c_mdev (cmap.h) is expanded */
#define	x_MSWAPX	x_NMOUNT		/* pseudo mount table index for swapdev */
#define	x_MAXUPRC	40		/* max processes per user */
#define	x_NOFILE	64		/* max open files per process */
#define	x_CANBSIZ	256		/* max size of typewriter line */
#define	x_NCARGS	20480		/* # characters in exec arglist */
#define	x_NGROUPS	16		/* max number groups */

#define	x_NOGROUP	65535		/* marker for empty group set member */

/*
 * Priorities
 */
#define	x_PSWP	0
#define	x_PINOD	10
#define	x_PRIBIO	20
#define	x_PRIUBA	24
#define	x_PZERO	25
#define	x_PPIPE	26
#define	x_PWAIT	30
#define	x_PLOCK	35
#define	x_PSLEP	40
#define	x_PUSER	50

#define	x_NZERO	0

/*
 * Signals
 */
#ifdef x_KERNEL
#include "x_signal.h"
#else
#include <x_signal.h>
#endif

#define	x_ISSIG(x_p) \
	((x_p)->x_p_sig && ((x_p)->x_p_flag&x_STRC || \
	 ((x_p)->x_p_sig &~ ((x_p)->x_p_sigignore | (x_p)->x_p_sigmask))) && x_issig())

#define	x_NBPW	sizeof(x_int)	/* number of bytes in an integer */

#define	x_NULL	0
#define	x_CMASK	022		/* default mask for file creation */
#define	x_NODEV	(x_dev_t)(-1)

/*
 * Clustering of hardware pages on machines with ridiculously small
 * page sizes is done here.  The paging subsystem deals with units of
 * CLSIZE pte's describing NBPG (from vm.h) pages each.
 *
 * NOTE: SSIZE, SINCR and UPAGES must be multiples of CLSIZE
 */
#define	x_CLBYTES		(x_CLSIZE*x_NBPG)
#define	x_CLOFSET		(x_CLSIZE*x_NBPG-1)	/* for clusters, like PGOFSET */
#define	x_claligned(x_x)	((((x_int)(x_x))&x_CLOFSET)==0)
#define	x_CLOFF		x_CLOFSET
#define	x_CLSHIFT		(x_PGSHIFT+x_CLSIZELOG2)

#if x_CLSIZE==1
#define	x_clbase(x_i)	(x_i)
#define	x_clrnd(x_i)	(x_i)
#else
/* give the base virtual address (first of CLSIZE) */
#define	x_clbase(x_i)	((x_i) &~ (x_CLSIZE-1))
/* round a number of clicks up to a whole cluster */
#define	x_clrnd(x_i)	(((x_i) + (x_CLSIZE-1)) &~ (x_CLSIZE-1))
#endif

/* CBLOCK is the size of a clist block, must be power of 2 */
#define	x_CBLOCK	64
#define	x_CBSIZE	(x_CBLOCK - sizeof(struct x_cblock *))	/* data chars/clist */
#define	x_CROUND	(x_CBLOCK - 1)				/* clist rounding */

#ifndef x_KERNEL
#include	<sys/x_types.h>
#else
#ifndef x_LOCORE
#include	"x_types.h"
#endif
#endif

/*
 * File system parameters and macros.
 *
 * The file system is made out of blocks of at most MAXBSIZE units,
 * with smaller units (fragments) only in the last direct block.
 * MAXBSIZE primarily determines the size of buffers in the buffer
 * pool. It may be made larger without any effect on existing
 * file systems; however making it smaller make make some file
 * systems unmountable.
 *
 * Note that the blocked devices are assumed to have DEV_BSIZE
 * "sectors" and that fragments must be some multiple of this size.
 * Block devices are read in BLKDEV_IOSIZE units. This number must
 * be a power of two and in the range of
 *	DEV_BSIZE <= BLKDEV_IOSIZE <= MAXBSIZE
 * This size has no effect upon the file system, but is usually set
 * to the block size of the root file system, so as to maximize the
 * speed of ``fsck''.
 */
#define	x_MAXBSIZE	8192
#define	x_DEV_BSIZE	512
#define	x_DEV_BSHIFT	9		/* log2(DEV_BSIZE) */
#define x_BLKDEV_IOSIZE	2048
#define x_MAXFRAG 	8

#define	x_btodb(x_bytes)	 		/* calculates (bytes / DEV_BSIZE) */ \
	((unsigned)(x_bytes) >> x_DEV_BSHIFT)
#define	x_dbtob(x_db)			/* calculates (db * DEV_BSIZE) */ \
	((unsigned)(x_db) << x_DEV_BSHIFT)

/*
 * Map a ``block device block'' to a file system block.
 * This should be device dependent, and will be after we
 * add an entry to cdevsw for that purpose.  For now though
 * just use DEV_BSIZE.
 */
#define	x_bdbtofsb(x_bn)	((x_bn) / (x_BLKDEV_IOSIZE/x_DEV_BSIZE))

/*
 * MAXPATHLEN defines the longest permissable path length
 * after expanding symbolic links. It is used to allocate
 * a temporary buffer from the buffer pool in which to do the
 * name expansion, hence should be a power of two, and must
 * be less than or equal to MAXBSIZE.
 * MAXSYMLINKS defines the maximum number of symbolic links
 * that may be expanded in a path name. It should be set high
 * enough to allow all legitimate uses, but halt infinite loops
 * reasonably quickly.
 */
#define x_MAXPATHLEN	1024
#define x_MAXSYMLINKS	8

/*
 * bit map related macros
 */
#define	x_setbit(x_a,x_i)	((x_a)[(x_i)/x_NBBY] |= 1<<((x_i)%x_NBBY))
#define	x_clrbit(x_a,x_i)	((x_a)[(x_i)/x_NBBY] &= ~(1<<((x_i)%x_NBBY)))
#define	x_isset(x_a,x_i)	((x_a)[(x_i)/x_NBBY] & (1<<((x_i)%x_NBBY)))
#define	x_isclr(x_a,x_i)	(((x_a)[(x_i)/x_NBBY] & (1<<((x_i)%x_NBBY))) == 0)

/*
 * Macros for fast min/max.
 */
#define	x_MIN(x_a,x_b) (((x_a)<(x_b))?(x_a):(x_b))
#define	x_MAX(x_a,x_b) (((x_a)>(x_b))?(x_a):(x_b))

/*
 * Macros for counting and rounding.
 */
#ifndef x_howmany
#define	x_howmany(x_x, x_y)	(((x_x)+((x_y)-1))/(x_y))
#endif
#define	x_roundup(x_x, x_y)	((((x_x)+((x_y)-1))/(x_y))*(x_y))

/*
 * Maximum size of hostname recognized and stored in the kernel.
 */
#define x_MAXHOSTNAMELEN	64
