#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)text.h	7.1 (Berkeley) 6/4/86
 */

/*
 * Text structure.
 * One allocated per pure
 * procedure on swap device.
 * Manipulated by text.c
 */
#define	x_NXDAD	12		/* param.h:MAXTSIZ / vmparam.h:DMTEXT */

struct x_text
{
	struct	x_text *x_x_forw;	/* forward link in free list */
	struct	x_text **x_x_back;	/* backward link in free list */
	x_swblk_t	x_x_daddr[x_NXDAD];	/* disk addresses of dmtext-page segments */
	x_swblk_t	x_x_ptdaddr;	/* disk address of page table */
	x_size_t	x_x_size;		/* size (clicks) */
	struct x_proc *x_x_caddr;	/* ptr to linked proc, if loaded */
	struct x_inode *x_x_iptr;	/* inode of prototype */
	x_short	x_x_rssize;
	x_short	x_x_swrss;
	x_short	x_x_count;	/* reference count */
	x_short	x_x_ccount;	/* number of loaded references */
	char	x_x_flag;		/* traced, written flags */
	char	x_x_slptime;
	x_short	x_x_poip;		/* page out in progress count */
};

#ifdef	x_KERNEL
struct	x_text *x_text, *x_textNTEXT;
x_int	x_ntext;
#endif

#define	x_XTRC	0x01		/* Text may be written, exclusive use */
#define	x_XWRIT	0x02		/* Text written into, must swap out */
#define	x_XLOAD	0x04		/* Currently being read from file */
#define	x_XLOCK	0x08		/* Being swapped in or out */
#define	x_XWANT	0x10		/* Wanted for swapping */
#define	x_XPAGI	0x20		/* Page in on demand from inode */
#define	x_XUNUSED	0x40		/* unused since swapped out for cache */

/*
 * Text table statistics
 */
struct x_xstats {
	x_u_long	x_alloc;			/* calls to xalloc */
	x_u_long	x_alloc_inuse;		/*	found in use/sticky */
	x_u_long	x_alloc_cachehit;		/*	found in cache */
	x_u_long	x_alloc_cacheflush;	/*	flushed cached text */
	x_u_long	x_alloc_unused;		/*	flushed unused cached text */
	x_u_long	x_free;			/* calls to xfree */
	x_u_long	x_free_inuse;		/*	still in use/sticky */
	x_u_long	x_free_cache;		/*	placed in cache */
	x_u_long	x_free_cacheswap;		/*	swapped out to place in cache */
};
