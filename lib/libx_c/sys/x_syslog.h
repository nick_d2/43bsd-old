#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)syslog.h	7.1 (Berkeley) 6/4/86
 */

/*
 *  Facility codes
 */

#define x_LOG_KERN	(0<<3)	/* kernel messages */
#define x_LOG_USER	(1<<3)	/* random user-level messages */
#define x_LOG_MAIL	(2<<3)	/* mail system */
#define x_LOG_DAEMON	(3<<3)	/* system daemons */
#define x_LOG_AUTH	(4<<3)	/* security/authorization messages */
#define x_LOG_SYSLOG	(5<<3)	/* messages generated internally by syslogd */
#define x_LOG_LPR		(6<<3)	/* line printer subsystem */
	/* other codes through 15 reserved for system use */
#define x_LOG_LOCAL0	(16<<3)	/* reserved for local use */
#define x_LOG_LOCAL1	(17<<3)	/* reserved for local use */
#define x_LOG_LOCAL2	(18<<3)	/* reserved for local use */
#define x_LOG_LOCAL3	(19<<3)	/* reserved for local use */
#define x_LOG_LOCAL4	(20<<3)	/* reserved for local use */
#define x_LOG_LOCAL5	(21<<3)	/* reserved for local use */
#define x_LOG_LOCAL6	(22<<3)	/* reserved for local use */
#define x_LOG_LOCAL7	(23<<3)	/* reserved for local use */

#define x_LOG_NFACILITIES	24	/* maximum number of facilities */
#define x_LOG_FACMASK	0x03f8	/* mask to extract facility part */

/*
 *  Priorities (these are ordered)
 */

#define x_LOG_EMERG	0	/* system is unusable */
#define x_LOG_ALERT	1	/* action must be taken immediately */
#define x_LOG_CRIT	2	/* critical conditions */
#define x_LOG_ERR		3	/* error conditions */
#define x_LOG_WARNING	4	/* warning conditions */
#define x_LOG_NOTICE	5	/* normal but signification condition */
#define x_LOG_INFO	6	/* informational */
#define x_LOG_DEBUG	7	/* debug-level messages */

#define x_LOG_PRIMASK	0x0007	/* mask to extract priority part (internal) */

/*
 * arguments to setlogmask.
 */
#define	x_LOG_MASK(x_pri)	(1 << (x_pri))		/* mask for one priority */
#define	x_LOG_UPTO(x_pri)	((1 << ((x_pri)+1)) - 1)	/* all priorities through pri */

/*
 *  Option flags for openlog.
 *
 *	LOG_ODELAY no longer does anything; LOG_NDELAY is the
 *	inverse of what it used to be.
 */
#define	x_LOG_PID		0x01	/* log the pid with each message */
#define	x_LOG_CONS	0x02	/* log on the console if errors in sending */
#define	x_LOG_ODELAY	0x04	/* delay open until syslog() is called */
#define x_LOG_NDELAY	0x08	/* don't delay open */
#define x_LOG_NOWAIT	0x10	/* if forking to log on console, don't wait() */

#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

/* gen/syslog.c */
x_int x_syslog __P((x_int x_pri, char *x_fmt, x_int x_p0, x_int x_p1, x_int x_p2, x_int x_p3, x_int x_p4));
x_int x_openlog __P((char *x_ident, x_int x_logstat, x_int x_logfac));
x_int x_closelog __P((void));
x_int x_setlogmask __P((x_int x_pmask));
