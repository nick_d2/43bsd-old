#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)dmap.h	7.1 (Berkeley) 6/4/86
 */

/*
 * Definitions for the mapping of vitual swap
 * space to the physical swap area - the disk map.
 */

#define	x_NDMAP 		38	/* size of the swap area map */

struct	x_dmap
{
	x_swblk_t	x_dm_size;	/* current size used by process */
	x_swblk_t	x_dm_alloc;	/* amount of physical swap space allocated */
	x_swblk_t	x_dm_map[x_NDMAP];	/* first disk block number in each chunk */
};
#ifdef x_KERNEL
struct	x_dmap x_zdmap;
x_int	x_dmmin, x_dmmax, x_dmtext;
#endif

/*
 * The following structure is that ``returned''
 * from a call to vstodb().
 */
struct	x_dblock
{
	x_swblk_t	x_db_base;	/* base of physical contig drum block */
	x_swblk_t	x_db_size;	/* size of block */
};
