#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)callout.h	7.1 (Berkeley) 6/4/86
 */

/*
 * The callout structure is for
 * a routine arranging
 * to be called by the clock interrupt
 * (clock.c) with a specified argument,
 * in a specified amount of time.
 * Used, for example, to time tab
 * delays on typewriters.
 */

struct	x_callout {
	x_int	x_c_time;		/* incremental time */
	x_caddr_t	x_c_arg;		/* argument to routine */
	x_int	(*x_c_func)();	/* routine */
	struct	x_callout *x_c_next;
};
#ifdef x_KERNEL
struct	x_callout *x_callfree, *x_callout, x_calltodo;
x_int	x_ncallout;
#endif
