#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)kernel.h	7.1 (Berkeley) 6/4/86
 */

/*
 * Global variables for the kernel
 */

x_long	x_rmalloc();

/* 1.1 */
x_long	x_hostid;
char	x_hostname[x_MAXHOSTNAMELEN];
x_int	x_hostnamelen;

/* 1.2 */
struct	x_timeval x_boottime;
struct	x_timeval x_time;
struct	x_timezone x_tz;			/* XXX */
x_int	x_hz;
x_int	x_phz;				/* alternate clock's frequency */
x_int	x_tick;
x_int	x_lbolt;				/* awoken once a second */
x_int	x_realitexpire();

double	x_avenrun[3];

#ifdef x_GPROF
extern	x_int x_profiling;
extern	char *x_s_lowpc;
extern	x_u_long x_s_textsize;
extern	x_u_short *x_kcount;
#endif
