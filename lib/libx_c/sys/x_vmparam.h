#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)vmparam.h	7.1 (Berkeley) 6/4/86
 */

/*
 * Machine dependent constants
 */
#ifdef x_KERNEL
#include "../machine/x_vmparam.h"
#else
#include <machine/x_vmparam.h>
#endif

#if defined(x_KERNEL) && !defined(x_LOCORE)
x_int	x_klseql;
x_int	x_klsdist;
x_int	x_klin;
x_int	x_kltxt;
x_int	x_klout;
#endif
