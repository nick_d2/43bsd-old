#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)times.h	7.1 (Berkeley) 6/4/86
 */

/*
 * Structure returned by times()
 */
struct x_tms {
	x_time_t	x_tms_utime;		/* user time */
	x_time_t	x_tms_stime;		/* system time */
	x_time_t	x_tms_cutime;		/* user time, children */
	x_time_t	x_tms_cstime;		/* system time, children */
};

#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

/* compat-4.1/times.c */
x_int x_times __P((register struct x_tms *x_tmsp));
