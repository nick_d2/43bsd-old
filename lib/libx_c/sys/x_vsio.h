#include "x_.h"

/* @(#)vsio.h	7.1 (MIT) 6/4/86 */
 /****************************************************************************
 *									    *
 *  Copyright (c) 1983, 1984 by						    *
 *  DIGITAL EQUIPMENT CORPORATION, Maynard, Massachusetts.		    *
 *  All rights reserved.						    *
 * 									    *
 *  This software is furnished on an as-is basis and may be used and copied *
 *  only with inclusion of the above copyright notice. This software or any *
 *  other copies thereof may be provided or otherwise made available to     *
 *  others only for non-commercial purposes.  No title to or ownership of   *
 *  the software is hereby transferred.					    *
 * 									    *
 *  The information in this software is  subject to change without notice   *
 *  and  should  not  be  construed as  a commitment by DIGITAL EQUIPMENT   *
 *  CORPORATION.							    *
 * 									    *
 *  DIGITAL assumes no responsibility for the use  or  reliability of its   *
 *  software on equipment which is not supplied by DIGITAL.		    *
 * 									    *
 *									    *
 ****************************************************************************/
/* 
 * vsio.h - VS100 I/O command definitions
 * 
 * Author:	Christopher A. Kent
 *		Digital Equipment Corporation
 *		Western Research Lab
 * Date:	Tue Jun 21 1983
 */

/* 
 * Possible ioctl calls
 */

#define	x_VSIOINIT	x__IO(x_V, 0)	/* init the device */
#define	x_VSIOSTART	x__IOW(x_V, 1, x_int)	/* start microcode */
#define	x_VSIOABORT	x__IO(x_V, 2)	/* abort a command chain */
#define	x_VSIOPWRUP	x__IO(x_V, 3)	/* power-up reset */
#define	x_VSIOGETVER	x__IOR(x_V, 4, x_int)	/* get rom version */
#define	x_VSIOSYNC	x__IO(x_V, 6)	/* synch with device */
#define	x_VSIOBBACTL	x__IOW(x_V, 8, x_int)	/* control the BBA */
#define	x_VSIOFIBCTL	x__IOW(x_V, 9, x_int)	/* lamp on/off */
#define	x_VSIOFIBRETRY	x__IOW(x_V,10, x_int)	/* fiber retries */
#define	x_VSIOGETSTATS	x__IOR(x_V,11, x_vsStats)	/* get statistics */
#define	x_VSIOGETIOA	x__IOR(x_V,13, x_vsIoAddrAddr)/* get ioreg address */
#define	x_VSIOUSERWAIT	x__IO(x_V, 15)	/* wait for user I/O completion */
#define x_VSIOWAITGO	x__IOW(x_V, 16, x_caddr_t)	/* wait then go */


#define	x_VSIO_OFF	0		/* option off */
#define	x_VSIO_ON		1		/* option on */

#define	x_VS_FIB_FINITE	1		/* finite retries */
#define	x_VS_FIB_INFINITE	2		/* infinite retries */

/* 
 * Event queue entries
 */

typedef struct	x__vs_event{
	x_u_short	x_vse_x;		/* x position */
	x_u_short	x_vse_y;		/* y position */
	x_u_short	x_vse_time;	/* 10 millisecond units (button only) */
	char	x_vse_type;	/* button or motion? */
	x_u_char	x_vse_key;	/* the key (button only) */
	char	x_vse_direction;	/* which direction (button only) */
	char	x_vse_device;	/* which device (button only) */
}x_vsEvent;

#define	x_VSE_BUTTON	0		/* button moved */
#define	x_VSE_MMOTION	1		/* mouse moved */
#define	x_VSE_TMOTION	2		/* tablet moved */

#define	x_VSE_KBTUP	0		/* up */
#define	x_VSE_KBTDOWN	1		/* down */

#define	x_VSE_MOUSE	1		/* mouse */
#define	x_VSE_DKB		2		/* main keyboard */
#define	x_VSE_TABLET	3		/* graphics tablet */
#define	x_VSE_AUX		4		/* auxiliary */
#define	x_VSE_CONSOLE	5		/* console */

typedef struct x__vsStats{
	x_int	x_errors;			/* count errors */
	x_int	x_unsolIntr;		/* count unsolicited interrupts */
	x_int	x_overruns;		/* event queue overruns */
	x_int	x_flashes;		/* flashes on fiber link */
	x_int	x_ignites;		/* times turned on */
	x_int	x_douses;			/* times turned off */
	x_int	x_linkErrors;		/* link errors */
}x_vsStats;

typedef struct x__vs_cursor{
	x_short x_x;
	x_short x_y;
}x_vsCursor;

typedef struct x__vs_box {
	x_short x_bottom;
	x_short x_right;
	x_short x_left;
	x_short x_top;
}x_vsBox;

typedef struct x__vsIoAddr {
	x_short	 *x_ioreg;
	x_short	 x_status;
	x_caddr_t  x_obuff;
	x_int	 x_obufflen;
	x_int	 x_reloc;
	x_vsEvent  *x_ibuff;
	x_int	 x_iqsize;		/* may assume power of 2 */
	x_int	 x_ihead;			/* atomic write */
	x_int	 x_itail;			/* atomic read */
	x_vsCursor x_mouse;			/* atomic read/write */
	x_vsBox	 x_mbox;			/* atomic read/write */
} x_vsIoAddr;
typedef x_vsIoAddr *x_vsIoAddrAddr;
