#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)mount.h	7.1 (Berkeley) 6/4/86
 */

/*
 * Mount structure.
 * One allocated on every mount.
 * Used to find the super block.
 */
struct	x_mount
{
	x_dev_t	x_m_dev;		/* device mounted */
	struct	x_buf *x_m_bufp;	/* pointer to superblock */
	struct	x_inode *x_m_inodp;	/* pointer to mounted on inode */
	struct	x_inode *x_m_qinod;	/* QUOTA: pointer to quota file */
};
#ifdef x_KERNEL
struct	x_mount x_mount[x_NMOUNT];
#endif
