#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)acct.h	7.1 (Berkeley) 6/4/86
 */

/*
 * Accounting structures;
 * these use a comp_t type which is a 3 bits base 8
 * exponent, 13 bit fraction ``floating point'' number.
 * Units are 1/AHZ seconds.
 */
typedef	x_u_short x_comp_t;

struct	x_acct
{
	char	x_ac_comm[10];		/* Accounting command name */
	x_comp_t	x_ac_utime;		/* Accounting user time */
	x_comp_t	x_ac_stime;		/* Accounting system time */
	x_comp_t	x_ac_etime;		/* Accounting elapsed time */
	x_time_t	x_ac_btime;		/* Beginning time */
	x_uid_t	x_ac_uid;			/* Accounting user ID */
	x_gid_t	x_ac_gid;			/* Accounting group ID */
	x_short	x_ac_mem;			/* average memory usage */
	x_comp_t	x_ac_io;			/* number of disk IO blocks */
	x_dev_t	x_ac_tty;			/* control typewriter */
	char	x_ac_flag;		/* Accounting flag */
};

#define	x_AFORK	0001		/* has executed fork, but no exec */
#define	x_ASU	0002		/* used super-user privileges */
#define	x_ACOMPAT	0004		/* used compatibility mode */
#define	x_ACORE	0010		/* dumped core */
#define	x_AXSIG	0020		/* killed by a signal */

/*
 * 1/AHZ is the granularity of the data encoded in the various
 * comp_t fields.  This is not necessarily equal to hz.
 */
#define x_AHZ 64

#ifdef x_KERNEL
struct	x_acct	x_acctbuf;
struct	x_inode	*x_acctp;
#endif
