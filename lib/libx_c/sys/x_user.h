#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)user.h	7.1 (Berkeley) 6/4/86
 */

#ifdef x_KERNEL
#include "../machine/x_pcb.h"
#include "x_dmap.h"
#include "x_time.h"
#include "x_resource.h"
#include "x_namei.h"
#else
#include <machine/x_pcb.h>
#include <sys/x_dmap.h>
#include <sys/x_time.h>
#include <sys/x_resource.h>
#include <sys/x_namei.h>
#endif

/*
 * Per process structure containing data that
 * isn't needed in core when the process is swapped out.
 */
 
#define	x_MAXCOMLEN	16		/* <= MAXNAMLEN, >= sizeof(ac_comm) */
 
struct	x_user {
	struct	x_pcb x_u_pcb;
	struct	x_proc *x_u_procp;		/* pointer to proc structure */
	x_int	*x_u_ar0;			/* address of users saved R0 */
	char	x_u_comm[x_MAXCOMLEN + 1];

/* syscall parameters, results and catches */
	x_int	x_u_arg[8];		/* arguments to current system call */
	x_int	*x_u_ap;			/* pointer to arglist */
	x_label_t	x_u_qsave;		/* for non-local gotos on interrupts */
	union {				/* syscall return values */
		struct	{
			x_int	x_R_val1;
			x_int	x_R_val2;
		} x_u_rv;
#define	x_r_val1	x_u_rv.x_R_val1
#define	x_r_val2	x_u_rv.x_R_val2
		x_off_t	x_r_off;
		x_time_t	x_r_time;
	} x_u_r;
	char	x_u_error;		/* return error code */
	char	x_u_eosys;		/* special action on end of syscall */

/* 1.1 - processes and protection */
	x_uid_t	x_u_uid;			/* effective user id */
	x_uid_t	x_u_ruid;			/* real user id */
	x_gid_t	x_u_gid;			/* effective group id */
	x_gid_t	x_u_rgid;			/* real group id */
	x_gid_t	x_u_groups[x_NGROUPS];	/* groups, 0 terminated */

/* 1.2 - memory management */
	x_size_t	x_u_tsize;		/* text size (clicks) */
	x_size_t	x_u_dsize;		/* data size (clicks) */
	x_size_t	x_u_ssize;		/* stack size (clicks) */
	struct	x_dmap x_u_dmap;		/* disk map for data segment */
	struct	x_dmap x_u_smap;		/* disk map for stack segment */
	struct	x_dmap x_u_cdmap, x_u_csmap;	/* shadows of u_dmap, u_smap, for
					   use of parent during fork */
	x_label_t x_u_ssave;		/* label variable for swapping */
	x_size_t	x_u_odsize, x_u_ossize;	/* for (clumsy) expansion swaps */
	x_time_t	x_u_outime;		/* user time at last sample */

/* 1.3 - signal management */
	x_int	(*x_u_signal[x_NSIG])();	/* disposition of signals */
	x_int	x_u_sigmask[x_NSIG];	/* signals to be blocked */
	x_int	x_u_sigonstack;		/* signals to take on sigstack */
	x_int	x_u_sigintr;		/* signals that interrupt syscalls */
	x_int	x_u_oldmask;		/* saved mask from before sigpause */
	x_int	x_u_code;			/* ``code'' to trap */
	struct	x_sigstack x_u_sigstack;	/* sp & on stack state variable */
#define	x_u_onstack	x_u_sigstack.x_ss_onstack
#define	x_u_sigsp		x_u_sigstack.x_ss_sp

/* 1.4 - descriptor management */
	struct	x_file *x_u_ofile[x_NOFILE];	/* file structures for open files */
	char	x_u_pofile[x_NOFILE];	/* per-process flags of open files */
	x_int	x_u_lastfile;		/* high-water mark of u_ofile */
#define	x_UF_EXCLOSE 	0x1		/* auto-close on exec */
#define	x_UF_MAPPED 	0x2		/* mapped from device */
	struct	x_inode *x_u_cdir;		/* current directory */
	struct	x_inode *x_u_rdir;		/* root directory of current process */
	struct	x_tty *x_u_ttyp;		/* controlling tty pointer */
	x_dev_t	x_u_ttyd;			/* controlling tty dev */
	x_short	x_u_cmask;		/* mask for file creation */

/* 1.5 - timing and statistics */
	struct	x_rusage x_u_ru;		/* stats for this proc */
	struct	x_rusage x_u_cru;		/* sum of stats for reaped children */
	struct	x_itimerval x_u_timer[3];
	x_int	x_u_XXX[3];
	struct	x_timeval x_u_start;
	x_short	x_u_acflag;

	struct x_uprof {			/* profile arguments */
		x_short	*x_pr_base;	/* buffer base */
		x_unsigned_int x_pr_size;	/* buffer size */
		x_unsigned_int x_pr_off;	/* pc offset */
		x_unsigned_int x_pr_scale;	/* pc scaling */
	} x_u_prof;

/* 1.6 - resource controls */
	struct	x_rlimit x_u_rlimit[x_RLIM_NLIMITS];
	struct	x_quota *x_u_quota;		/* user's quota structure */
	x_int	x_u_qflags;		/* per process quota flags */

/* namei & co. */
	struct x_nameicache {		/* last successful directory search */
		x_int x_nc_prevoffset;	/* offset at which last entry found */
		x_ino_t x_nc_inumber;	/* inum of cached directory */
		x_dev_t x_nc_dev;		/* dev of cached directory */
		x_time_t x_nc_time;		/* time stamp for cache entry */
	} x_u_ncache;
	struct	x_nameidata x_u_nd;

	x_int	x_u_stack[1];
};

/* u_eosys values */
#define	x_JUSTRETURN	1
#define	x_RESTARTSYS	2
#define x_NORMALRETURN	3

/* u_error codes */
#ifdef x_KERNEL
#include "x_errno.h"
#else
#include <x_errno.h>
#endif

#ifdef x_KERNEL
extern	struct x_user x_u;
extern	struct x_user x_swaputl;
extern	struct x_user x_forkutl;
extern	struct x_user x_xswaputl;
extern	struct x_user x_xswap2utl;
extern	struct x_user x_pushutl;
extern	struct x_user x_vfutl;
#endif
