#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)timeb.h	7.1 (Berkeley) 6/4/86
 */

/*
 * Structure returned by ftime system call
 */
struct x_timeb
{
	x_time_t	x_time;
	x_unsigned_short x_millitm;
	x_short	x_timezone;
	x_short	x_dstflag;
};

#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

/* compat-4.1/ftime.c */
x_int x_ftime __P((register struct x_timeb *x_tp));
