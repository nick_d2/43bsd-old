#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)proc.h	7.1 (Berkeley) 6/4/86
 */

/*
 * One structure allocated per active
 * process. It contains all data needed
 * about the process while the
 * process may be swapped out.
 * Other per process data (user.h)
 * is swapped with the process.
 */
struct	x_proc {
	struct	x_proc *x_p_link;	/* linked list of running processes */
	struct	x_proc *x_p_rlink;
	struct	x_proc *x_p_nxt;	/* linked list of allocated proc slots */
	struct	x_proc **x_p_prev;		/* also zombies, and free proc's */
	struct	x_pte *x_p_addr;	/* u-area kernel map address */
	char	x_p_usrpri;	/* user-priority based on p_cpu and p_nice */
	char	x_p_pri;		/* priority, negative is high */
	char	x_p_cpu;		/* cpu usage for scheduling */
	char	x_p_stat;
	char	x_p_time;		/* resident time for scheduling */
	char	x_p_nice;		/* nice for cpu usage */
	char	x_p_slptime;	/* time since last block */
	char	x_p_cursig;
	x_int	x_p_sig;		/* signals pending to this process */
	x_int	x_p_sigmask;	/* current signal mask */
	x_int	x_p_sigignore;	/* signals being ignored */
	x_int	x_p_sigcatch;	/* signals being caught by user */
	x_int	x_p_flag;
	x_uid_t	x_p_uid;		/* user id, used to direct tty signals */
	x_short	x_p_pgrp;		/* name of process group leader */
	x_short	x_p_pid;		/* unique process id */
	x_short	x_p_ppid;		/* process id of parent */
	x_u_short	x_p_xstat;	/* Exit status for wait */
	struct	x_rusage *x_p_ru;	/* mbuf holding exit information */
	x_short	x_p_poip;		/* page outs in progress */
	x_short	x_p_szpt;		/* copy of page table size */
	x_size_t	x_p_tsize;	/* size of text (clicks) */
	x_size_t	x_p_dsize;	/* size of data space (clicks) */
	x_size_t	x_p_ssize;	/* copy of stack size (clicks) */
	x_size_t 	x_p_rssize; 	/* current resident set size in clicks */
	x_size_t	x_p_maxrss;	/* copy of u.u_limit[MAXRSS] */
	x_size_t	x_p_swrss;	/* resident set size before last swap */
	x_swblk_t	x_p_swaddr;	/* disk address of u area when swapped */
	x_caddr_t x_p_wchan;	/* event process is awaiting */
	struct	x_text *x_p_textp;	/* pointer to text structure */
	struct	x_pte *x_p_p0br;	/* page table base P0BR */
	struct	x_proc *x_p_xlink;	/* linked list of procs sharing same text */
	x_short	x_p_cpticks;	/* ticks of cpu time */
	float	x_p_pctcpu;	/* %cpu for this process during p_time */
	x_short	x_p_ndx;		/* proc index for memall (because of vfork) */
	x_short	x_p_idhash;	/* hashed based on p_pid for kill+exit+... */
	struct	x_proc *x_p_pptr;	/* pointer to process structure of parent */
	struct	x_proc *x_p_cptr;	/* pointer to youngest living child */
	struct	x_proc *x_p_osptr;	/* pointer to older sibling processes */
	struct	x_proc *x_p_ysptr;	/* pointer to younger siblings */
	struct	x_itimerval x_p_realtimer;
	struct	x_quota *x_p_quota;	/* quotas for this process */
};

#define	x_PIDHSZ		64
#define	x_PIDHASH(x_pid)	((x_pid) & (x_PIDHSZ - 1))

#ifdef x_KERNEL
x_short	x_pidhash[x_PIDHSZ];
struct	x_proc *x_pfind();
struct	x_proc *x_proc, *x_procNPROC;	/* the proc table itself */
struct	x_proc *x_freeproc, *x_zombproc, *x_allproc;
			/* lists of procs in various states */
x_int	x_nproc;

#define	x_NQS	32		/* 32 run queues */
struct	x_prochd {
	struct	x_proc *x_ph_link;	/* linked list of running processes */
	struct	x_proc *x_ph_rlink;
} x_qs[x_NQS];
x_int	x_whichqs;		/* bit mask summarizing non-empty qs's */
#endif

/* stat codes */
#define	x_SSLEEP	1		/* awaiting an event */
#define	x_SWAIT	2		/* (abandoned state) */
#define	x_SRUN	3		/* running */
#define	x_SIDL	4		/* intermediate state in process creation */
#define	x_SZOMB	5		/* intermediate state in process termination */
#define	x_SSTOP	6		/* process being traced */

/* flag codes */
#define	x_SLOAD	0x0000001	/* in core */
#define	x_SSYS	0x0000002	/* swapper or pager process */
#define	x_SLOCK	0x0000004	/* process being swapped out */
#define	x_SSWAP	0x0000008	/* save area flag */
#define	x_STRC	0x0000010	/* process is being traced */
#define	x_SWTED	0x0000020	/* another tracing flag */
#define	x_SULOCK	0x0000040	/* user settable lock in core */
#define	x_SPAGE	0x0000080	/* process in page wait state */
#define	x_SKEEP	0x0000100	/* another flag to prevent swap out */
#define	x_SOMASK	0x0000200	/* restore old mask after taking signal */
#define	x_SWEXIT	0x0000400	/* working on exiting */
#define	x_SPHYSIO	0x0000800	/* doing physical i/o (bio.c) */
#define	x_SVFORK	0x0001000	/* process resulted from vfork() */
#define	x_SVFDONE	0x0002000	/* another vfork flag */
#define	x_SNOVM	0x0004000	/* no vm, parent in a vfork() */
#define	x_SPAGI	0x0008000	/* init data space on demand, from inode */
#define	x_SSEQL	0x0010000	/* user warned of sequential vm behavior */
#define	x_SUANOM	0x0020000	/* user warned of random vm behavior */
#define	x_STIMO	0x0040000	/* timing out during sleep */
/* was SDETACH */
#define	x_SOUSIG	0x0100000	/* using old signal mechanism */
#define	x_SOWEUPC	0x0200000	/* owe process an addupc() call at next ast */
#define	x_SSEL	0x0400000	/* selecting; wakeup/waiting danger */
#define	x_SLOGIN	0x0800000	/* a login process (legit child of init) */
#define	x_SPTECHG	0x1000000	/* pte's for process have changed */
