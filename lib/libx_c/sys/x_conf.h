#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)conf.h	7.1 (Berkeley) 6/4/86
 */

/*
 * Declaration of block device
 * switch. Each entry (row) is
 * the only link between the
 * main unix code and the driver.
 * The initialization of the
 * device switches is in the
 * file conf.c.
 */
struct x_bdevsw
{
	x_int	(*x_d_open)();
	x_int	(*x_d_close)();
	x_int	(*x_d_strategy)();
	x_int	(*x_d_dump)();
	x_int	(*x_d_psize)();
	x_int	x_d_flags;
};
#ifdef x_KERNEL
struct	x_bdevsw x_bdevsw[];
#endif

/*
 * Character device switch.
 */
struct x_cdevsw
{
	x_int	(*x_d_open)();
	x_int	(*x_d_close)();
	x_int	(*x_d_read)();
	x_int	(*x_d_write)();
	x_int	(*x_d_ioctl)();
	x_int	(*x_d_stop)();
	x_int	(*x_d_reset)();
	struct x_tty *x_d_ttys;
	x_int	(*x_d_select)();
	x_int	(*x_d_mmap)();
};
#ifdef x_KERNEL
struct	x_cdevsw x_cdevsw[];
#endif

/*
 * tty line control switch.
 */
struct x_linesw
{
	x_int	(*x_l_open)();
	x_int	(*x_l_close)();
	x_int	(*x_l_read)();
	x_int	(*x_l_write)();
	x_int	(*x_l_ioctl)();
	x_int	(*x_l_rint)();
	x_int	(*x_l_rend)();
	x_int	(*x_l_meta)();
	x_int	(*x_l_start)();
	x_int	(*x_l_modem)();
};
#ifdef x_KERNEL
struct	x_linesw x_linesw[];
#endif

/*
 * Swap device information
 */
struct x_swdevt
{
	x_dev_t	x_sw_dev;
	x_int	x_sw_freed;
	x_int	x_sw_nblks;
};
#ifdef x_KERNEL
struct	x_swdevt x_swdevt[];
#endif
