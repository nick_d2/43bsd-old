#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)signal.h	7.1 (Berkeley) 6/4/86
 */

#ifndef	x_NSIG
#define x_NSIG	32

#define	x_SIGHUP	1	/* hangup */
#define	x_SIGINT	2	/* interrupt */
#define	x_SIGQUIT	3	/* quit */
#define	x_SIGILL	4	/* illegal instruction (not reset when caught) */
#define	    x_ILL_RESAD_FAULT	0x0	/* reserved addressing fault */
#define	    x_ILL_PRIVIN_FAULT	0x1	/* privileged instruction fault */
#define	    x_ILL_RESOP_FAULT	0x2	/* reserved operand fault */
/* CHME, CHMS, CHMU are not yet given back to users reasonably */
#define	x_SIGTRAP	5	/* trace trap (not reset when caught) */
#define	x_SIGIOT	6	/* IOT instruction */
#define	x_SIGABRT	x_SIGIOT	/* compatibility */
#define	x_SIGEMT	7	/* EMT instruction */
#define	x_SIGFPE	8	/* floating point exception */
#define	    x_FPE_INTOVF_TRAP	0x1	/* integer overflow */
#define	    x_FPE_INTDIV_TRAP	0x2	/* integer divide by zero */
#define	    x_FPE_FLTOVF_TRAP	0x3	/* floating overflow */
#define	    x_FPE_FLTDIV_TRAP	0x4	/* floating/decimal divide by zero */
#define	    x_FPE_FLTUND_TRAP	0x5	/* floating underflow */
#define	    x_FPE_DECOVF_TRAP	0x6	/* decimal overflow */
#define	    x_FPE_SUBRNG_TRAP	0x7	/* subscript out of range */
#define	    x_FPE_FLTOVF_FAULT	0x8	/* floating overflow fault */
#define	    x_FPE_FLTDIV_FAULT	0x9	/* divide by zero floating fault */
#define	    x_FPE_FLTUND_FAULT	0xa	/* floating underflow fault */
#define	x_SIGKILL	9	/* kill (cannot be caught or ignored) */
#define	x_SIGBUS	10	/* bus error */
#define	x_SIGSEGV	11	/* segmentation violation */
#define	x_SIGSYS	12	/* bad argument to system call */
#define	x_SIGPIPE	13	/* write on a pipe with no one to read it */
#define	x_SIGALRM	14	/* alarm clock */
#define	x_SIGTERM	15	/* software termination signal from kill */
#define	x_SIGURG	16	/* urgent condition on IO channel */
#define	x_SIGSTOP	17	/* sendable stop signal not from tty */
#define	x_SIGTSTP	18	/* stop signal from tty */
#define	x_SIGCONT	19	/* continue a stopped process */
#define	x_SIGCHLD	20	/* to parent on child stop or exit */
#define	x_SIGCLD	x_SIGCHLD	/* compatibility */
#define	x_SIGTTIN	21	/* to readers pgrp upon background tty read */
#define	x_SIGTTOU	22	/* like TTIN for output if (tp->t_local&LTOSTOP) */
#define	x_SIGIO	23	/* input/output possible signal */
#define	x_SIGXCPU	24	/* exceeded CPU time limit */
#define	x_SIGXFSZ	25	/* exceeded file size limit */
#define	x_SIGVTALRM 26	/* virtual time alarm */
#define	x_SIGPROF	27	/* profiling time alarm */
#define x_SIGWINCH 28	/* window size changes */
#define x_SIGUSR1 30	/* user defined signal 1 */
#define x_SIGUSR2 31	/* user defined signal 2 */

#ifndef x_KERNEL
x_int	(*x_signal())();
#endif

/*
 * Signal vector "template" used in sigvec call.
 */
struct	x_sigvec {
	x_int	(*x_sv_handler)();	/* signal handler */
	x_int	x_sv_mask;		/* signal mask to apply */
	x_int	x_sv_flags;		/* see signal options below */
};
#define x_SV_ONSTACK	0x0001	/* take signal on signal stack */
#define x_SV_INTERRUPT	0x0002	/* do not restart system on signal return */
#define x_sv_onstack x_sv_flags	/* isn't compatibility wonderful! */

/*
 * Structure used in sigstack call.
 */
struct	x_sigstack {
	char	*x_ss_sp;			/* signal stack pointer */
	x_int	x_ss_onstack;		/* current status */
};

/*
 * Information pushed on stack when a signal is delivered.
 * This is used by the kernel to restore state following
 * execution of the signal handler.  It is also made available
 * to the handler to allow it to properly restore state if
 * a non-standard exit is performed.
 */
struct	x_sigcontext {
	x_int	x_sc_onstack;		/* sigstack state to restore */
	x_int	x_sc_mask;		/* signal mask to restore */
	x_int	x_sc_sp;			/* sp to restore */
	x_int	x_sc_fp;			/* fp to restore */
	x_int	x_sc_ap;			/* ap to restore */
	x_int	x_sc_pc;			/* pc to restore */
	x_int	x_sc_ps;			/* psl to restore */
};

#define	x_BADSIG		(x_int (*)())-1
#define	x_SIG_DFL		(x_int (*)())0
#define	x_SIG_IGN		(x_int (*)())1

#ifdef x_KERNEL
#define	x_SIG_CATCH	(x_int (*)())2
#define	x_SIG_HOLD	(x_int (*)())3
#endif
#endif

/*
 * Macro for converting signal number to a mask suitable for
 * sigblock().
 */
#define x_sigmask(x_m)	(1 << ((x_m)-1))

#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

/* gen/siginterrupt.c */
x_int x_siginterrupt __P((x_int x_sig, x_int x_flag));
/* gen/psignal.c */
x_int x_psignal __P((x_unsigned_int x_sig, char *x_s));
/* gen/signal.c */
x_int (*x_signal __P((x_int x_s, x_int (*x_a)(void)))) __P((void));
