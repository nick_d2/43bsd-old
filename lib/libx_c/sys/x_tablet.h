#include "x_.h"

/*
 * Copyright (c) 1985, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)tablet.h	7.1 (Berkeley) 6/4/86
 */

#ifndef x__TABLET_
/*
 * Tablet line discipline.
 */
#ifdef x_KERNEL
#include "../h/x_ioctl.h"
#else
#include <sys/x_ioctl.h>
#endif

/*
 * Reads on the tablet return one of the following
 * structures, depending on the underlying tablet type.
 * The first two are defined such that a read of
 * sizeof (gtcopos) on a non-gtco tablet will return
 * meaningful info.  The in-proximity bit is simulated
 * where the tablet does not directly provide the information.
 */
struct	x_tbpos {
	x_int	x_xpos, x_ypos;	/* raw x-y coordinates */
	x_short	x_status;		/* buttons/pen down */
#define	x_TBINPROX	0100000		/* pen in proximity of tablet */
	x_short	x_scount;		/* sample count */
};

struct	x_gtcopos {
	x_int	x_xpos, x_ypos;	/* raw x-y coordinates */
	x_short	x_status;		/* as above */
	x_short	x_scount;		/* sample count */
	x_short	x_xtilt, x_ytilt;	/* raw tilt */
	x_short	x_pressure;
	x_short	x_pad;		/* pad to longword boundary */
};

struct	x_polpos {
	x_short	x_p_x, x_p_y, x_p_z;	/* raw 3-space coordinates */
	x_short	x_p_azi, x_p_pit, x_p_rol;	/* azimuth, pitch, and roll */
	x_short	x_p_stat;		/* status, as above */
	char	x_p_key;		/* calculator input keyboard */
};

#define x_BIOSMODE	x__IOW(x_b, 1, x_int)		/* set mode bit(s) */
#define x_BIOGMODE	x__IOR(x_b, 2, x_int)		/* get mode bit(s) */
#define	x_TBMODE		0xfff0		/* mode bits: */
#define		x_TBPOINT		0x0010		/* single point */
#define		x_TBRUN		0x0000		/* runs contin. */
#define		x_TBSTOP		0x0020		/* shut-up */
#define		x_TBGO		0x0000		/* ~TBSTOP */
#define	x_TBTYPE		0x000f		/* tablet type: */
#define		x_TBUNUSED	0x0000
#define		x_TBHITACHI	0x0001		/* hitachi tablet */
#define		x_TBTIGER		0x0002		/* hitachi tiger */
#define		x_TBGTCO		0x0003		/* gtco */
#define		x_TBPOL		0x0004		/* polhemus 3space */
#define		x_TBHDG		0x0005		/* hdg-1111b, low res */
#define		x_TBHDGHIRES	0x0006		/* hdg-1111b, high res */
#define x_BIOSTYPE	x__IOW(x_b, 3, x_int)		/* set tablet type */
#define x_BIOGTYPE	x__IOR(x_b, 4, x_int)		/* get tablet type*/
#endif
