#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)wait.h	7.1 (Berkeley) 6/4/86
 */

/*
 * This file holds definitions relevent to the wait system call.
 * Some of the options here are available only through the ``wait3''
 * entry point; the old entry point with one argument has more fixed
 * semantics, never returning status of unstopped children, hanging until
 * a process terminates if any are outstanding, and never returns
 * detailed information about process resource utilization (<vtimes.h>).
 */

/*
 * Structure of the information in the first word returned by both
 * wait and wait3.  If w_stopval==WSTOPPED, then the second structure
 * describes the information returned, else the first.  See WUNTRACED below.
 */
union x_wait	{
	x_int	x_w_status;		/* used in syscall */
	/*
	 * Terminated process status.
	 */
	struct {
		x_unsigned_short	x_w_Termsig:7;	/* termination signal */
		x_unsigned_short	x_w_Coredump:1;	/* core dump indicator */
		x_unsigned_short	x_w_Retcode:8;	/* exit code if w_termsig==0 */
	} x_w_T;
	/*
	 * Stopped process status.  Returned
	 * only for traced children unless requested
	 * with the WUNTRACED option bit.
	 */
	struct {
		x_unsigned_short	x_w_Stopval:8;	/* == W_STOPPED if stopped */
		x_unsigned_short	x_w_Stopsig:8;	/* signal that stopped us */
	} x_w_S;
};
#define	x_w_termsig	x_w_T.x_w_Termsig
#define x_w_coredump	x_w_T.x_w_Coredump
#define x_w_retcode	x_w_T.x_w_Retcode
#define x_w_stopval	x_w_S.x_w_Stopval
#define x_w_stopsig	x_w_S.x_w_Stopsig


#define	x_WSTOPPED	0177	/* value of s.stopval if process is stopped */

/*
 * Option bits for the second argument of wait3.  WNOHANG causes the
 * wait to not hang if there are no stopped or terminated processes, rather
 * returning an error indication in this case (pid==0).  WUNTRACED
 * indicates that the caller should receive status about untraced children
 * which stop due to signals.  If children are stopped and a wait without
 * this option is done, it is as though they were still running... nothing
 * about them is returned.
 */
#define x_WNOHANG		1	/* dont hang in wait */
#define x_WUNTRACED	2	/* tell about stopped, untraced children */

#define x_WIFSTOPPED(x_x)	((x_x).x_w_stopval == x_WSTOPPED)
#define x_WIFSIGNALED(x_x)	((x_x).x_w_stopval != x_WSTOPPED && (x_x).x_w_termsig != 0)
#define x_WIFEXITED(x_x)	((x_x).x_w_stopval != x_WSTOPPED && (x_x).x_w_termsig == 0)
