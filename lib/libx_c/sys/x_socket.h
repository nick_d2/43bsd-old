#include "x_.h"

/*
 * Copyright (c) 1982,1985, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)socket.h	7.1 (Berkeley) 6/4/86
 */

/*
 * Definitions related to sockets: types, address families, options.
 */

/*
 * Types
 */
#define	x_SOCK_STREAM	1		/* stream socket */
#define	x_SOCK_DGRAM	2		/* datagram socket */
#define	x_SOCK_RAW	3		/* raw-protocol interface */
#define	x_SOCK_RDM	4		/* reliably-delivered message */
#define	x_SOCK_SEQPACKET	5		/* sequenced packet stream */

/*
 * Option flags per-socket.
 */
#define	x_SO_DEBUG	0x0001		/* turn on debugging info recording */
#define	x_SO_ACCEPTCONN	0x0002		/* socket has had listen() */
#define	x_SO_REUSEADDR	0x0004		/* allow local address reuse */
#define	x_SO_KEEPALIVE	0x0008		/* keep connections alive */
#define	x_SO_DONTROUTE	0x0010		/* just use interface addresses */
#define	x_SO_BROADCAST	0x0020		/* permit sending of broadcast msgs */
#define	x_SO_USELOOPBACK	0x0040		/* bypass hardware when possible */
#define	x_SO_LINGER	0x0080		/* linger on close if data present */
#define	x_SO_OOBINLINE	0x0100		/* leave received OOB data in line */

/*
 * Additional options, not kept in so_options.
 */
#define x_SO_SNDBUF	0x1001		/* send buffer size */
#define x_SO_RCVBUF	0x1002		/* receive buffer size */
#define x_SO_SNDLOWAT	0x1003		/* send low-water mark */
#define x_SO_RCVLOWAT	0x1004		/* receive low-water mark */
#define x_SO_SNDTIMEO	0x1005		/* send timeout */
#define x_SO_RCVTIMEO	0x1006		/* receive timeout */
#define	x_SO_ERROR	0x1007		/* get error status and clear */
#define	x_SO_TYPE		0x1008		/* get socket type */

/*
 * Structure used for manipulating linger option.
 */
struct	x_linger {
	x_int	x_l_onoff;		/* option on/off */
	x_int	x_l_linger;		/* linger time */
};

/*
 * Level number for (get/set)sockopt() to apply to socket itself.
 */
#define	x_SOL_SOCKET	0xffff		/* options for socket level */

/*
 * Address families.
 */
#define	x_AF_UNSPEC	0		/* unspecified */
#define	x_AF_UNIX		1		/* local to host (pipes, portals) */
#define	x_AF_INET		2		/* internetwork: UDP, TCP, etc. */
#define	x_AF_IMPLINK	3		/* arpanet imp addresses */
#define	x_AF_PUP		4		/* pup protocols: e.g. BSP */
#define	x_AF_CHAOS	5		/* mit CHAOS protocols */
#define	x_AF_NS		6		/* XEROX NS protocols */
#define	x_AF_NBS		7		/* nbs protocols */
#define	x_AF_ECMA		8		/* european computer manufacturers */
#define	x_AF_DATAKIT	9		/* datakit protocols */
#define	x_AF_CCITT	10		/* CCITT protocols, X.25 etc */
#define	x_AF_SNA		11		/* IBM SNA */
#define x_AF_DECnet	12		/* DECnet */
#define x_AF_DLI		13		/* Direct data link interface */
#define x_AF_LAT		14		/* LAT */
#define	x_AF_HYLINK	15		/* NSC Hyperchannel */
#define	x_AF_APPLETALK	16		/* Apple Talk */

#define	x_AF_MAX		17

/*
 * Structure used by kernel to store most
 * addresses.
 */
struct x_sockaddr {
	x_u_short	x_sa_family;		/* address family */
	char	x_sa_data[14];		/* up to 14 bytes of direct address */
};

/*
 * Structure used by kernel to pass protocol
 * information in raw sockets.
 */
struct x_sockproto {
	x_u_short	x_sp_family;		/* address family */
	x_u_short	x_sp_protocol;		/* protocol */
};

/*
 * Protocol families, same as address families for now.
 */
#define	x_PF_UNSPEC	x_AF_UNSPEC
#define	x_PF_UNIX		x_AF_UNIX
#define	x_PF_INET		x_AF_INET
#define	x_PF_IMPLINK	x_AF_IMPLINK
#define	x_PF_PUP		x_AF_PUP
#define	x_PF_CHAOS	x_AF_CHAOS
#define	x_PF_NS		x_AF_NS
#define	x_PF_NBS		x_AF_NBS
#define	x_PF_ECMA		x_AF_ECMA
#define	x_PF_DATAKIT	x_AF_DATAKIT
#define	x_PF_CCITT	x_AF_CCITT
#define	x_PF_SNA		x_AF_SNA
#define x_PF_DECnet	x_AF_DECnet
#define x_PF_DLI		x_AF_DLI
#define x_PF_LAT		x_AF_LAT
#define	x_PF_HYLINK	x_AF_HYLINK
#define	x_PF_APPLETALK	x_AF_APPLETALK

#define	x_PF_MAX		x_AF_MAX

/*
 * Maximum queue length specifiable by listen.
 */
#define	x_SOMAXCONN	5

/*
 * Message header for recvmsg and sendmsg calls.
 */
struct x_msghdr {
	x_caddr_t	x_msg_name;		/* optional address */
	x_int	x_msg_namelen;		/* size of address */
	struct	x_iovec *x_msg_iov;		/* scatter/gather array */
	x_int	x_msg_iovlen;		/* # elements in msg_iov */
	x_caddr_t	x_msg_accrights;		/* access rights sent/received */
	x_int	x_msg_accrightslen;
};

#define	x_MSG_OOB		0x1		/* process out-of-band data */
#define	x_MSG_PEEK	0x2		/* peek at incoming message */
#define	x_MSG_DONTROUTE	0x4		/* send without using routing tables */

#define	x_MSG_MAXIOVLEN	16
