#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)ioctl.h	7.1 (Berkeley) 6/4/86
 */

/*
 * Ioctl definitions
 */
#ifndef	x__IOCTL_
#define	x__IOCTL_
#ifdef x_KERNEL
#include "x_ttychars.h"
#include "x_ttydev.h"
#else
#include <sys/x_ttychars.h>
#include <sys/x_ttydev.h>
#endif

struct x_tchars {
	char	x_t_intrc;	/* interrupt */
	char	x_t_quitc;	/* quit */
	char	x_t_startc;	/* start output */
	char	x_t_stopc;	/* stop output */
	char	x_t_eofc;		/* end-of-file */
	char	x_t_brkc;		/* input delimiter (like nl) */
};
struct x_ltchars {
	char	x_t_suspc;	/* stop process signal */
	char	x_t_dsuspc;	/* delayed stop process signal */
	char	x_t_rprntc;	/* reprint line */
	char	x_t_flushc;	/* flush output (toggles) */
	char	x_t_werasc;	/* word erase */
	char	x_t_lnextc;	/* literal next character */
};

/*
 * Structure for TIOCGETP and TIOCSETP ioctls.
 */

#ifndef x__SGTTYB_
#define	x__SGTTYB_
struct x_sgttyb {
	char	x_sg_ispeed;		/* input speed */
	char	x_sg_ospeed;		/* output speed */
	char	x_sg_erase;		/* erase character */
	char	x_sg_kill;		/* kill character */
	x_short	x_sg_flags;		/* mode flags */
};
#endif

/*
 * Window/terminal size structure.
 * This information is stored by the kernel
 * in order to provide a consistent interface,
 * but is not used by the kernel.
 *
 * Type must be "unsigned short" so that types.h not required.
 */
struct x_winsize {
	x_unsigned_short	x_ws_row;			/* rows, in characters */
	x_unsigned_short	x_ws_col;			/* columns, in characters */
	x_unsigned_short	x_ws_xpixel;		/* horizontal size, pixels */
	x_unsigned_short	x_ws_ypixel;		/* vertical size, pixels */
};

/*
 * Pun for SUN.
 */
struct x_ttysize {
	x_unsigned_short	x_ts_lines;
	x_unsigned_short	x_ts_cols;
	x_unsigned_short	x_ts_xxx;
	x_unsigned_short	x_ts_yyy;
};
#define	x_TIOCGSIZE	x_TIOCGWINSZ
#define	x_TIOCSSIZE	x_TIOCSWINSZ

#ifndef x__IO
/*
 * Ioctl's have the command encoded in the lower word,
 * and the size of any in or out parameters in the upper
 * word.  The high 2 bits of the upper word are used
 * to encode the in/out status of the parameter; for now
 * we restrict parameters to at most 128 bytes.
 */
#define	x_IOCPARM_MASK	0x7f		/* parameters must be < 128 bytes */
#define	x_IOC_VOID	0x20000000	/* no parameters */
#define	x_IOC_OUT		0x40000000	/* copy out parameters */
#define	x_IOC_IN		0x80000000	/* copy in parameters */
#define	x_IOC_INOUT	(x_IOC_IN|x_IOC_OUT)
/* the 0x20000000 is so we can distinguish new ioctl's from old */
#define	x__IO(x_x,x_y)	(x_IOC_VOID|('x'<<8)|x_y)
#define	x__IOR(x_x,x_y,x_t)	(x_IOC_OUT|((sizeof(x_t)&x_IOCPARM_MASK)<<16)|('x'<<8)|x_y)
#define	x__IOW(x_x,x_y,x_t)	(x_IOC_IN|((sizeof(x_t)&x_IOCPARM_MASK)<<16)|('x'<<8)|x_y)
/* this should be _IORW, but stdio got there first */
#define	x__IOWR(x_x,x_y,x_t)	(x_IOC_INOUT|((sizeof(x_t)&x_IOCPARM_MASK)<<16)|('x'<<8)|x_y)
#endif

/*
 * tty ioctl commands
 */
#define	x_TIOCGETD	x__IOR(x_t, 0, x_int)		/* get line discipline */
#define	x_TIOCSETD	x__IOW(x_t, 1, x_int)		/* set line discipline */
#define	x_TIOCHPCL	x__IO(x_t, 2)		/* hang up on last close */
#define	x_TIOCMODG	x__IOR(x_t, 3, x_int)		/* get modem control state */
#define	x_TIOCMODS	x__IOW(x_t, 4, x_int)		/* set modem control state */
#define		x_TIOCM_LE	0001		/* line enable */
#define		x_TIOCM_DTR	0002		/* data terminal ready */
#define		x_TIOCM_RTS	0004		/* request to send */
#define		x_TIOCM_ST	0010		/* secondary transmit */
#define		x_TIOCM_SR	0020		/* secondary receive */
#define		x_TIOCM_CTS	0040		/* clear to send */
#define		x_TIOCM_CAR	0100		/* carrier detect */
#define		x_TIOCM_CD	x_TIOCM_CAR
#define		x_TIOCM_RNG	0200		/* ring */
#define		x_TIOCM_RI	x_TIOCM_RNG
#define		x_TIOCM_DSR	0400		/* data set ready */
#define	x_TIOCGETP	x__IOR(x_t, 8,struct x_sgttyb)/* get parameters -- gtty */
#define	x_TIOCSETP	x__IOW(x_t, 9,struct x_sgttyb)/* set parameters -- stty */
#define	x_TIOCSETN	x__IOW(x_t,10,struct x_sgttyb)/* as above, but no flushtty */
#define	x_TIOCEXCL	x__IO(x_t, 13)		/* set exclusive use of tty */
#define	x_TIOCNXCL	x__IO(x_t, 14)		/* reset exclusive use of tty */
#define	x_TIOCFLUSH	x__IOW(x_t, 16, x_int)	/* flush buffers */
#define	x_TIOCSETC	x__IOW(x_t,17,struct x_tchars)/* set special characters */
#define	x_TIOCGETC	x__IOR(x_t,18,struct x_tchars)/* get special characters */
#define		x_TANDEM		0x00000001	/* send stopc on out q full */
#define		x_CBREAK		0x00000002	/* half-cooked mode */
#define		x_LCASE		0x00000004	/* simulate lower case */
#define		x_ECHO		0x00000008	/* echo input */
#define		x_CRMOD		0x00000010	/* map \r to \r\n on output */
#define		x_RAW		0x00000020	/* no i/o processing */
#define		x_ODDP		0x00000040	/* get/send odd parity */
#define		x_EVENP		0x00000080	/* get/send even parity */
#define		x_ANYP		0x000000c0	/* get any parity/send none */
#define		x_NLDELAY		0x00000300	/* \n delay */
#define			x_NL0	0x00000000
#define			x_NL1	0x00000100	/* tty 37 */
#define			x_NL2	0x00000200	/* vt05 */
#define			x_NL3	0x00000300
#define		x_TBDELAY		0x00000c00	/* horizontal tab delay */
#define			x_TAB0	0x00000000
#define			x_TAB1	0x00000400	/* tty 37 */
#define			x_TAB2	0x00000800
#define		x_XTABS		0x00000c00	/* expand tabs on output */
#define		x_CRDELAY		0x00003000	/* \r delay */
#define			x_CR0	0x00000000
#define			x_CR1	0x00001000	/* tn 300 */
#define			x_CR2	0x00002000	/* tty 37 */
#define			x_CR3	0x00003000	/* concept 100 */
#define		x_VTDELAY		0x00004000	/* vertical tab delay */
#define			x_FF0	0x00000000
#define			x_FF1	0x00004000	/* tty 37 */
#define		x_BSDELAY		0x00008000	/* \b delay */
#define			x_BS0	0x00000000
#define			x_BS1	0x00008000
#define		x_ALLDELAY	(x_NLDELAY|x_TBDELAY|x_CRDELAY|x_VTDELAY|x_BSDELAY)
#define		x_CRTBS		0x00010000	/* do backspacing for crt */
#define		x_PRTERA		0x00020000	/* \ ... / erase */
#define		x_CRTERA		0x00040000	/* " \b " to wipe out char */
#define		x_TILDE		0x00080000	/* hazeltine tilde kludge */
#define		x_MDMBUF		0x00100000	/* start/stop output on carrier intr */
#define		x_LITOUT		0x00200000	/* literal output */
#define		x_TOSTOP		0x00400000	/* SIGSTOP on background output */
#define		x_FLUSHO		0x00800000	/* flush output to terminal */
#define		x_NOHANG		0x01000000	/* no SIGHUP on carrier drop */
#define		x_L001000		0x02000000
#define		x_CRTKIL		0x04000000	/* kill line with " \b " */
#define		x_PASS8		0x08000000
#define		x_CTLECH		0x10000000	/* echo control chars as ^X */
#define		x_PENDIN		0x20000000	/* tp->t_rawq needs reread */
#define		x_DECCTQ		0x40000000	/* only ^Q starts after ^S */
#define		x_NOFLSH		0x80000000	/* no output flush on signal */
/* locals, from 127 down */
#define	x_TIOCLBIS	x__IOW(x_t, 127, x_int)	/* bis local mode bits */
#define	x_TIOCLBIC	x__IOW(x_t, 126, x_int)	/* bic local mode bits */
#define	x_TIOCLSET	x__IOW(x_t, 125, x_int)	/* set entire local mode word */
#define	x_TIOCLGET	x__IOR(x_t, 124, x_int)	/* get local modes */
#define		x_LCRTBS		(x_CRTBS>>16)
#define		x_LPRTERA		(x_PRTERA>>16)
#define		x_LCRTERA		(x_CRTERA>>16)
#define		x_LTILDE		(x_TILDE>>16)
#define		x_LMDMBUF		(x_MDMBUF>>16)
#define		x_LLITOUT		(x_LITOUT>>16)
#define		x_LTOSTOP		(x_TOSTOP>>16)
#define		x_LFLUSHO		(x_FLUSHO>>16)
#define		x_LNOHANG		(x_NOHANG>>16)
#define		x_LCRTKIL		(x_CRTKIL>>16)
#define		x_LPASS8		(x_PASS8>>16)
#define		x_LCTLECH		(x_CTLECH>>16)
#define		x_LPENDIN		(x_PENDIN>>16)
#define		x_LDECCTQ		(x_DECCTQ>>16)
#define		x_LNOFLSH		(x_NOFLSH>>16)
#define	x_TIOCSBRK	x__IO(x_t, 123)		/* set break bit */
#define	x_TIOCCBRK	x__IO(x_t, 122)		/* clear break bit */
#define	x_TIOCSDTR	x__IO(x_t, 121)		/* set data terminal ready */
#define	x_TIOCCDTR	x__IO(x_t, 120)		/* clear data terminal ready */
#define	x_TIOCGPGRP	x__IOR(x_t, 119, x_int)	/* get pgrp of tty */
#define	x_TIOCSPGRP	x__IOW(x_t, 118, x_int)	/* set pgrp of tty */
#define	x_TIOCSLTC	x__IOW(x_t,117,struct x_ltchars)/* set local special chars */
#define	x_TIOCGLTC	x__IOR(x_t,116,struct x_ltchars)/* get local special chars */
#define	x_TIOCOUTQ	x__IOR(x_t, 115, x_int)	/* output queue size */
#define	x_TIOCSTI		x__IOW(x_t, 114, char)	/* simulate terminal input */
#define	x_TIOCNOTTY	x__IO(x_t, 113)		/* void tty association */
#define	x_TIOCPKT		x__IOW(x_t, 112, x_int)	/* pty: set/clear packet mode */
#define		x_TIOCPKT_DATA		0x00	/* data packet */
#define		x_TIOCPKT_FLUSHREAD	0x01	/* flush packet */
#define		x_TIOCPKT_FLUSHWRITE	0x02	/* flush packet */
#define		x_TIOCPKT_STOP		0x04	/* stop output */
#define		x_TIOCPKT_START		0x08	/* start output */
#define		x_TIOCPKT_NOSTOP		0x10	/* no more ^S, ^Q */
#define		x_TIOCPKT_DOSTOP		0x20	/* now do ^S ^Q */
#define	x_TIOCSTOP	x__IO(x_t, 111)		/* stop output, like ^S */
#define	x_TIOCSTART	x__IO(x_t, 110)		/* start output, like ^Q */
#define	x_TIOCMSET	x__IOW(x_t, 109, x_int)	/* set all modem bits */
#define	x_TIOCMBIS	x__IOW(x_t, 108, x_int)	/* bis modem bits */
#define	x_TIOCMBIC	x__IOW(x_t, 107, x_int)	/* bic modem bits */
#define	x_TIOCMGET	x__IOR(x_t, 106, x_int)	/* get all modem bits */
#define	x_TIOCREMOTE	x__IOW(x_t, 105, x_int)	/* remote input editing */
#define	x_TIOCGWINSZ	x__IOR(x_t, 104, struct x_winsize)	/* get window size */
#define	x_TIOCSWINSZ	x__IOW(x_t, 103, struct x_winsize)	/* set window size */
#define	x_TIOCUCNTL	x__IOW(x_t, 102, x_int)	/* pty: set/clr usr cntl mode */
#define		x_UIOCCMD(x_n)	x__IO(x_u, x_n)		/* usr cntl op "n" */

#define	x_OTTYDISC	0		/* old, v7 std tty driver */
#define	x_NETLDISC	1		/* line discip for berk net */
#define	x_NTTYDISC	2		/* new tty discipline */
#define	x_TABLDISC	3		/* tablet discipline */
#define	x_SLIPDISC	4		/* serial IP discipline */

#define	x_FIOCLEX		x__IO(x_f, 1)		/* set exclusive use on fd */
#define	x_FIONCLEX	x__IO(x_f, 2)		/* remove exclusive use */
/* another local */
#define	x_FIONREAD	x__IOR(x_f, 127, x_int)	/* get # bytes to read */
#define	x_FIONBIO		x__IOW(x_f, 126, x_int)	/* set/clear non-blocking i/o */
#define	x_FIOASYNC	x__IOW(x_f, 125, x_int)	/* set/clear async i/o */
#define	x_FIOSETOWN	x__IOW(x_f, 124, x_int)	/* set owner */
#define	x_FIOGETOWN	x__IOR(x_f, 123, x_int)	/* get owner */

/* socket i/o controls */
#define	x_SIOCSHIWAT	x__IOW(x_s,  0, x_int)		/* set high watermark */
#define	x_SIOCGHIWAT	x__IOR(x_s,  1, x_int)		/* get high watermark */
#define	x_SIOCSLOWAT	x__IOW(x_s,  2, x_int)		/* set low watermark */
#define	x_SIOCGLOWAT	x__IOR(x_s,  3, x_int)		/* get low watermark */
#define	x_SIOCATMARK	x__IOR(x_s,  7, x_int)		/* at oob mark? */
#define	x_SIOCSPGRP	x__IOW(x_s,  8, x_int)		/* set process group */
#define	x_SIOCGPGRP	x__IOR(x_s,  9, x_int)		/* get process group */

#define	x_SIOCADDRT	x__IOW(x_r, 10, struct x_rtentry)	/* add route */
#define	x_SIOCDELRT	x__IOW(x_r, 11, struct x_rtentry)	/* delete route */

#define	x_SIOCSIFADDR	x__IOW(x_i, 12, struct x_ifreq)	/* set ifnet address */
#define	x_SIOCGIFADDR	x__IOWR(x_i,13, struct x_ifreq)	/* get ifnet address */
#define	x_SIOCSIFDSTADDR	x__IOW(x_i, 14, struct x_ifreq)	/* set p-p address */
#define	x_SIOCGIFDSTADDR	x__IOWR(x_i,15, struct x_ifreq)	/* get p-p address */
#define	x_SIOCSIFFLAGS	x__IOW(x_i, 16, struct x_ifreq)	/* set ifnet flags */
#define	x_SIOCGIFFLAGS	x__IOWR(x_i,17, struct x_ifreq)	/* get ifnet flags */
#define	x_SIOCGIFBRDADDR	x__IOWR(x_i,18, struct x_ifreq)	/* get broadcast addr */
#define	x_SIOCSIFBRDADDR	x__IOW(x_i,19, struct x_ifreq)	/* set broadcast addr */
#define	x_SIOCGIFCONF	x__IOWR(x_i,20, struct x_ifconf)	/* get ifnet list */
#define	x_SIOCGIFNETMASK	x__IOWR(x_i,21, struct x_ifreq)	/* get net addr mask */
#define	x_SIOCSIFNETMASK	x__IOW(x_i,22, struct x_ifreq)	/* set net addr mask */
#define	x_SIOCGIFMETRIC	x__IOWR(x_i,23, struct x_ifreq)	/* get IF metric */
#define	x_SIOCSIFMETRIC	x__IOW(x_i,24, struct x_ifreq)	/* set IF metric */

#define	x_SIOCSARP	x__IOW(x_i, 30, struct x_arpreq)	/* set arp entry */
#define	x_SIOCGARP	x__IOWR(x_i,31, struct x_arpreq)	/* get arp entry */
#define	x_SIOCDARP	x__IOW(x_i, 32, struct x_arpreq)	/* delete arp entry */

#endif
