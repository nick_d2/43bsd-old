#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)clist.h	7.1 (Berkeley) 6/4/86
 */

/*
 * Raw structures for the character list routines.
 */
struct x_cblock {
	struct x_cblock *x_c_next;
	char	x_c_info[x_CBSIZE];
};
#ifdef x_KERNEL
struct	x_cblock *x_cfree;
x_int	x_nclist;
struct	x_cblock *x_cfreelist;
x_int	x_cfreecount;
#endif
