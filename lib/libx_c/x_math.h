#include "x_.h"

/*	math.h	4.6	9/11/85	*/

extern double x_asinh(), x_acosh(), x_atanh();
extern double x_erf(), x_erfc();
extern double x_exp(), x_expm1(), x_log(), x_log10(), x_log1p(), x_pow();
extern double x_fabs(), x_floor(), x_ceil(), x_rint();
extern double x_lgamma();
extern double x_hypot(), x_cabs();
extern double x_copysign(), x_drem(), x_logb(), x_scalb();
extern x_int x_finite();
#ifdef x_vax
extern double x_infnan();
#endif
extern double x_j0(), x_j1(), x_jn(), x_y0(), x_y1(), x_yn();
extern double x_sin(), x_cos(), x_tan(), x_asin(), x_acos(), x_atan(), x_atan2();
extern double x_sinh(), x_cosh(), x_tanh();
extern double x_cbrt(), x_sqrt();
extern double x_modf(), x_ldexp(), x_frexp(), x_atof();

#define x_HUGE	1.701411733192644270e38

#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

/* gen/frexp.c */
double x_frexp __P((double x_x, x_int *x_i));
