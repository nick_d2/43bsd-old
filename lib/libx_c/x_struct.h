#include "x_.h"

/*	struct.h	4.1	83/05/03	*/

/*
 * access to information relating to the fields of a structure
 */

#define	x_fldoff(x_str, x_fld)	((x_int)&(((struct x_str *)0)->x_fld))
#define	x_fldsiz(x_str, x_fld)	(sizeof(((struct x_str *)0)->x_fld))
#define	x_strbase(x_str, x_ptr, x_fld)	((struct x_str *)((char *)(x_ptr)-x_fldoff(x_str, x_fld)))
