#ifndef x_
#define x_

#include <stdint.h>

typedef int16_t x_short;
typedef uint16_t x_unsigned_short;
typedef int32_t x_int;
typedef uint32_t x_unsigned_int;
typedef int32_t x_long;
typedef uint32_t x_unsigned_long;

#endif
