#include "x_.h"

/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)pcc.h	5.1 (Berkeley) 5/30/85
 */

/*
 * This file contains definitions for all the constants and structures
 *	needed to use the intermediate code files generated and read by
 *	the Portable C Compiler and related compilers.
 *
 * Rules for changing this code:
 *   1)	All op values must be integer constants -- this permits us to run
 *	a 'sed' script on this file to create %term declarations for yacc.
 *   2)	Because the PCC uses fancy ASG and UNARY macros, assignment
 *	operators must have values 1 greater than corresponding normal
 *	operators, and unary operators must have values 2 greater ditto.
 *   3) Ops used only by f1 must have values >= 150 (PCCF_FORTOPS).
 *   4)	Other language-dependent ops must have values >= 200.
 */

# ifndef	x_PCC_TOKENS

# define	x_PCC_TOKENS	0

# define	x_PCC_ERROR	1	/* an error node */
# define	x_PCC_FREE	2	/* an unused node */

/*
 * Constants.
 */
# define	x_PCC_STRING	3	/* a string constant */
# define	x_PCC_ICON	4	/* an integer constant */
# define	x_PCC_FCON	5	/* a floating point constant */
# define	x_PCC_DCON	6	/* a double precision f.p. constant */

/*
 * Leaf types.
 */
# define	x_PCC_NAME	7	/* an identifier */
# define	x_PCC_REG		8	/* a register */
# define	x_PCC_OREG	9	/* register and offset */
# define	x_PCC_CCODES	10	/* condition codes */
# define	x_PCC_FLD		11	/* a bit field */

/*
 * Arithmetic operators.
 */
# define	x_PCC_PLUS	12	/* + */
# define	x_PCC_PLUSEQ	13	/* += */
# define	x_PCC_UPLUS	14	/* unary + (for completeness) */
# define	x_PCC_MINUS	15	/* - */
# define	x_PCC_MINUSEQ	16	/* -= */
# define	x_PCC_UMINUS	17	/* unary - */
# define	x_PCC_MUL		18	/* * */
# define	x_PCC_MULEQ	19	/* *= */
/* Reserve a slot for 'unary *', which is PCC jargon for PCC_DEREF (yech) */
# define	x_PCC_DIV		21	/* / */
# define	x_PCC_DIVEQ	22	/* /= */
# define	x_PCC_MOD		23	/* % */
# define	x_PCC_MODEQ	24	/* %= */
# define	x_PCC_INCR	25	/* ++ */
# define	x_PCC_DECR	26	/* -- */
# define	x_PCC_ASSIGN	27	/* = (these last 3 are stretching it) */

/*
 * Bit operators.
 */
# define	x_PCC_AND		28	/* & */
# define	x_PCC_ANDEQ	29	/* &= */
/* Reserve a slot for 'unary &', jargon for PCC_ADDROF */
# define	x_PCC_OR		31	/* | */
# define	x_PCC_OREQ	32	/* |= */
# define	x_PCC_ER		33	/* ^ */
# define	x_PCC_EREQ	34	/* ^= */
# define	x_PCC_LS		35	/* << */
# define	x_PCC_LSEQ	36	/* <<= */
# define	x_PCC_RS		37	/* >> */
# define	x_PCC_RSEQ	38	/* >>= */
# define	x_PCC_COMPL	39	/* ~ */

/*
 * Booleans.
 */
# define	x_PCC_EQ		40	/* == */
# define	x_PCC_NE		41	/* != */
# define	x_PCC_LE		42	/* <= */
# define	x_PCC_LT		43	/* < */
# define	x_PCC_GE		44	/* >= */
# define	x_PCC_GT		45	/* > */
# define	x_PCC_ULE		46	/* unsigned <= */
# define	x_PCC_ULT		47	/* unsigned < */
# define	x_PCC_UGE		48	/* unsigned >= */
# define	x_PCC_UGT		49	/* unsigned > */
# define	x_PCC_QUEST	50	/* ? (for conditional expressions) */
# define	x_PCC_COLON	51	/* : (for conditional expressions) */
# define	x_PCC_ANDAND	52	/* && */
# define	x_PCC_OROR	53	/* || */
# define	x_PCC_NOT		54	/* ! */

/*
 * Function calls.
 */
# define	x_PCC_CALL	55	/* call by value */
/* no ASG */
# define	x_PCC_UCALL	57	/* call with no arguments */
# define	x_PCC_FORTCALL	58	/* call by reference? */
/* no ASG */
# define	x_PCC_UFORTCALL	60	/* ??? */
# ifdef x_INLINE
# define	x_PCC_INLINE	61	/* inline function */
/* no ASG */
# define	x_PCC_UINLINE	63	/* inline with no arguments */
# endif

/*
 * Referencing and dereferencing.
 */
# define	x_PCC_DEREF	20	/* * */
# define	x_PCC_ADDROF	30	/* & */

/*
 * Special structure operators.
 */
# define	x_PCC_DOT		64	/* . */
# define	x_PCC_STREF	65	/* -> */
# define	x_PCC_STASG	66	/* structure assignment */
# define	x_PCC_STARG	67	/* an argument of type structure */
# define	x_PCC_STCALL	68	/* a function of type structure */
/* no ASG */
# define	x_PCC_USTCALL	70	/* unary structure function */

/*
 * Conversions.
 */
# define	x_PCC_SCONV	71	/* scalar conversion */
# define	x_PCC_PCONV	72	/* pointer conversion */
# define	x_PCC_PMCONV	73	/* pointer multiply conversion */
# define	x_PCC_PVCONV	74	/* pointer divide conversion */
# define	x_PCC_CAST	75	/* redundant? */

/*
 * Bracket types.
 */
# define	x_PCC_LB		76	/* [ */
# define	x_PCC_RB		77	/* ] */

/*
 * Comma nodes.
 */
# define	x_PCC_COMOP	78	/* , (in expressions) */
# define	x_PCC_CM		79	/* , (in argument lists) */

/*
 * Miscellaneous.
 */
# define	x_PCC_FORCE	80	/* result of last expression goes in r0 */
# define	x_PCC_GOTO	81	/* unconditional goto */
# define	x_PCC_CBRANCH	82	/* goto label if !test */
# define	x_PCC_RETURN	83	/* return from function */
# define	x_PCC_INIT	84	/* initialized data */
# define	x_PCC_TYPE	85	/* a type */
# define	x_PCC_CLASS	86	/* a storage class */

# define	x_PCC_MAXOP	86	/* highest numbered PCC op */

/*
 * Special codes for interfacing to /lib/f1.
 */
# define	x_PCCF_FORTOPS	150
# define	x_PCCF_FTEXT	150	/* pass literal assembler text */
# define	x_PCCF_FEXPR	151	/* a statement */
# define	x_PCCF_FSWITCH	152	/* not implemented */
# define	x_PCCF_FLBRAC	153	/* beginning of subroutine */
# define	x_PCCF_FRBRAC	154	/* end of subroutine */
# define	x_PCCF_FEOF	155	/* end of file */
# define	x_PCCF_FARIF	156	/* not implemented */
# define	x_PCCF_FLABEL	157	/* an f77 label */

# endif


/*
 * Types, as encoded in intermediate file cookies.
 */
# define	x_PCCT_UNDEF	0
# define	x_PCCT_FARG	1	/* function argument */
# define	x_PCCT_CHAR	2
# define	x_PCCT_SHORT	3
# define	x_PCCT_INT	4
# define	x_PCCT_LONG	5
# define	x_PCCT_FLOAT	6
# define	x_PCCT_DOUBLE	7
# define	x_PCCT_STRTY	8
# define	x_PCCT_UNIONTY	9
# define	x_PCCT_ENUMTY	10
# define	x_PCCT_MOETY	11	/* member of enum */
# define	x_PCCT_UCHAR	12
# define	x_PCCT_USHORT	13
# define	x_PCCT_UNSIGNED	14
# define	x_PCCT_ULONG	15

/*
 * Type modifiers.
 */
# define	x_PCCTM_PTR	020
# define	x_PCCTM_FTN	040
# define	x_PCCTM_ARY	060
# define	x_PCCTM_BASETYPE	017
# define	x_PCCTM_TYPESHIFT	2


/*
 * Useful macros.  'PCCOM' macros apply to ops.
 */
# define	x_PCCOM_ASG	1+
# define	x_PCCOM_UNARY	2+
# define	x_PCCOM_NOASG	(-1)+
# define	x_PCCOM_NOUNARY	(-2)+

# define	x_PCCM_TRIPLE(x_op, x_var, x_type)	\
	((x_op) | ((x_var) << 8) | (x_long) (x_type) << 16)
# define	x_PCCM_TEXT(x_s)			\
	x_PCCM_TRIPLE(x_PCCF_FTEXT, (x_strlen(x_s) + 3) / 4, 0)
# define	x_PCCM_ADDTYPE(x_t, x_m)		\
	((((x_t) &~ x_PCCTM_BASETYPE) << x_PCCTM_TYPESHIFT) | \
	(x_m) | ((x_t) & x_PCCTM_BASETYPE))
