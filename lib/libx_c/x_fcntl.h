#include "x_.h"

/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)fcntl.h	5.2 (Berkeley) 1/8/86
 */

/*
 * Flag values accessible to open(2) and fcntl(2)-- copied from
 * <sys/file.h>.  (The first three can only be set by open.)
 */
#define	x_O_RDONLY	000		/* open for reading */
#define	x_O_WRONLY	001		/* open for writing */
#define	x_O_RDWR		002		/* open for read & write */
#define	x_O_NDELAY	x_FNDELAY		/* non-blocking open */
					/* really non-blocking I/O for fcntl */
#define	x_O_APPEND	x_FAPPEND		/* append on each write */
#define	x_O_CREAT		x_FCREAT		/* open with file create */
#define	x_O_TRUNC		x_FTRUNC		/* open with truncation */
#define	x_O_EXCL		x_FEXCL		/* error on create if file exists */

#ifndef	x_F_DUPFD
/* fcntl(2) requests */
#define	x_F_DUPFD	0	/* Duplicate fildes */
#define	x_F_GETFD	1	/* Get fildes flags */
#define	x_F_SETFD	2	/* Set fildes flags */
#define	x_F_GETFL	3	/* Get file flags */
#define	x_F_SETFL	4	/* Set file flags */
#define	x_F_GETOWN 5	/* Get owner */
#define x_F_SETOWN 6	/* Set owner */

/* flags for F_GETFL, F_SETFL-- copied from <sys/file.h> */
#define	x_FNDELAY		00004		/* non-blocking reads */
#define	x_FAPPEND		00010		/* append on each write */
#define	x_FASYNC		00100		/* signal pgrp when data ready */
#define	x_FCREAT		01000		/* create if nonexistant */
#define	x_FTRUNC		02000		/* truncate to zero length */
#define	x_FEXCL		04000		/* error if already created */
#endif
