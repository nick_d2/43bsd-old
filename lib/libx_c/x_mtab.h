#include "x_.h"

/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)mtab.h	5.1 (Berkeley) 5/30/85
 */

/*
 * Mounted device accounting file.
 */
struct x_mtab {
	char	x_m_path[32];		/* mounted on pathname */
	char	x_m_dname[32];		/* block device pathname */
	char	x_m_type[4];		/* read-only, quotas */
};
