#include "x_.h"

/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)disktab.h	5.2 (Berkeley) 10/1/85
 */

/*
 * Disk description table, see disktab(5)
 */
#define	x_DISKTAB		"/etc/disktab"

struct	x_disktab {
	char	*x_d_name;		/* drive name */
	char	*x_d_type;		/* drive type */
	x_int	x_d_secsize;		/* sector size in bytes */
	x_int	x_d_ntracks;		/* # tracks/cylinder */
	x_int	x_d_nsectors;		/* # sectors/track */
	x_int	x_d_ncylinders;		/* # cylinders */
	x_int	x_d_rpm;			/* revolutions/minute */
	x_int	x_d_badsectforw;		/* supports DEC bad144 std */
	x_int	x_d_sectoffset;		/* use sect rather than cyl offsets */
	struct	x_partition {
		x_int	x_p_size;		/* #sectors in partition */
		x_short	x_p_bsize;	/* block size in bytes */
		x_short	x_p_fsize;	/* frag size in bytes */
	} x_d_partitions[8];
};

struct	x_disktab *x_getdiskbyname();
