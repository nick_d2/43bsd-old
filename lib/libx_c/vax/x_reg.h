#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)reg.h	7.1 (Berkeley) 6/5/86
 */

/*
 * Location of the users' stored
 * registers relative to R0.
 * Usage is u.u_ar0[XX].
 */
#define	x_R0	(-18)
#define	x_R1	(-17)
#define	x_R2	(-16)
#define	x_R3	(-15)
#define	x_R4	(-14)
#define	x_R5	(-13)
#define	x_R6	(-12)
#define	x_R7	(-11)
#define	x_R8	(-10)
#define	x_R9	(-9)
#define	x_R10	(-8)
#define	x_R11	(-7)
#define	x_R12	(-21)
#define	x_R13	(-20)

#define x_AP	(-21)
#define	x_FP	(-20)
#define	x_SP	(-5)
#define	x_PS	(-1)
#define	x_PC	(-2)
