#include "x_.h"

/*	@(#)tmscp.h	7.1 (Berkeley) 6/5/86 */

/*
 *	@(#)tmscp.h	1.3	10/21/85
 * Definitions for the Tape Mass Storage Control Protocol
 */

/****************************************************************
 *                                                              *
 *        Licensed from Digital Equipment Corporation           *
 *                       Copyright (c)                          *
 *               Digital Equipment Corporation                  *
 *                   Maynard, Massachusetts                     *
 *                         1985, 1986                           *
 *                    All rights reserved.                      *
 *                                                              *
 *        The Information in this software is subject to change *
 *   without notice and should not be construed as a commitment *
 *   by  Digital  Equipment  Corporation.   Digital   makes  no *
 *   representations about the suitability of this software for *
 *   any purpose.  It is supplied "As Is" without expressed  or *
 *   implied  warranty. 					*
 *								*
 *        If the Regents of the University of California or its *
 *   licensees modify the software in a manner creating  	*
 *   diriviative copyright rights, appropriate copyright  	*
 *   legends may be placed on  the drivative work in addition   *
 *   to that set forth above. 					*
 *								*
 ****************************************************************
 *
 * Modification history: /sys/vax/tmscp.h
 *
 * 18-Oct-85 - afd
 *	Added: defines for tape format (density) flag values.
 *
 * 18-Jul-85 - afd
 *	Added: #define	M_UF_WBKNV	0000100
 *		for write back (which enables cache).
 ************************************************************************/
 
 
/*
 * Control message opcodes
 */
#define	x_M_OP_ABORT	0001	/* Abort command */
#define	x_M_OP_GTCMD	0002	/* Get command status command */
#define	x_M_OP_GTUNT	0003	/* Get unit status command */
#define	x_M_OP_STCON	0004	/* Set controller characteristics command */
#define	x_M_OP_AVAIL	0010	/* Available command */
#define	x_M_OP_ONLIN	0011	/* Online command */
#define	x_M_OP_STUNT	0012	/* Set unit characteristics command */
#define	x_M_OP_DTACP	0013	/* Determine access paths command */
#define	x_M_OP_ACCES	0020	/* Access command */
#define	x_M_OP_CMPCD	0021	/* Compare controller data command */
#define	x_M_OP_ERASE	0022	/* Erase command */
#define	x_M_OP_FLUSH	0023	/* Flush command */
#define x_M_OP_ERGAP	0026	/* Erase gap command */
#define	x_M_OP_COMP	0040	/* Compare host data command */
#define	x_M_OP_READ	0041	/* Read command */
#define	x_M_OP_WRITE	0042	/* Write command */
#define	x_M_OP_WRITM	0044	/* Write tape mark command */
#define	x_M_OP_REPOS	0045	/* Reposition command */
#define	x_M_OP_AVATN	0100	/* Available attention message */
#define	x_M_OP_DUPUN	0101	/* Duplicate unit number attention message */
#define	x_M_OP_ACPTH	0102	/* Access path attention message */
#define	x_M_OP_END	0200	/* End message flag */
 
 
/*
 * Generic command modifiers
 */
#define	x_M_MD_COMP	0040000		/* Compare */
#define	x_M_MD_CLSEX	0020000		/* Clear serious exception */
#define	x_M_MD_SECOR	0001000		/* Suppress error correction */
#define	x_M_MD_SEREC	0000400		/* Suppress error recovery */
#define	x_M_MD_STWRP	0000004		/* Set write protect */
#define	x_M_MD_ALLCD	0000002		/* All class drivers */
#define	x_M_MD_NXUNT	0000001		/* Next unit */
 
/*
 * TMSCP command modifiers
 */
#define	x_M_MD_DLEOT	0000200		/* Delete LEOT */
#define	x_M_MD_IMMED	0000100		/* Immediate completion */
#define	x_M_MD_EXCAC	0000040		/* Exclusive access */
#define	x_M_MD_UNLOD	0000020		/* Unload */
#define	x_M_MD_REVRS	0000010		/* reverse */
#define	x_M_MD_OBJCT	0000004		/* object count */
#define	x_M_MD_REWND	0000002		/* rewind */
 
/*
 * End message flags
 */
#define	x_M_EF_ERLOG	0040	/* Error log generated */
#define	x_M_EF_SEREX	0020	/* Serious exception */
#define	x_M_EF_EOT	0010	/* End of tape encountered */
#define	x_M_EF_PLS	0004	/* Position lost */
 
 
/*
 * Controller flags
 */
#define	x_M_CF_ATTN	0200	/* Enable attention messages */
#define	x_M_CF_MISC	0100	/* Enable miscellaneous error log messages */
#define	x_M_CF_OTHER	0040	/* Enable other host's error log messages */
#define	x_M_CF_THIS	0020	/* Enable this host's error log messages */
 
 
/*
 * Unit flags
 */
#define	x_M_UF_WRTPH	0020000		/* Write protect (hardware) */
#define	x_M_UF_WRTPS	0010000		/* Write protect (software or volume) */
#define	x_M_UF_WBKNV	0000100		/* Write back (enables cache) */
#define	x_M_UF_VSMSU	0000040		/* Variable speed mode suppression */
#define	x_M_UF_VARSP	0000020		/* Variable speed unit */
#define	x_M_UF_CMPWR	0000002		/* Compare writes */
#define	x_M_UF_CMPRD	0000001		/* Compare reads */
 
 
/*
 * Status codes
 */
#define	x_M_ST_MASK	037		/* Status code mask */
#define	x_M_ST_SUCC	000		/* Success */
#define	x_M_ST_ICMD	001		/* Invalid command */
#define	x_M_ST_ABRTD	002		/* Command aborted */
#define	x_M_ST_OFFLN	003		/* Unit offline */
#define	x_M_ST_AVLBL	004		/* Unit available */
#define	x_M_ST_WRTPR	006		/* Write protected */
#define	x_M_ST_COMP	007		/* Compare error */
#define	x_M_ST_DATA	010		/* Data error */
#define	x_M_ST_HSTBF	011		/* Host buffer access error */
#define	x_M_ST_CNTLR	012		/* Controller error */
#define	x_M_ST_DRIVE	013		/* Drive error */
#define	x_M_ST_FMTER	014		/* Formatter error */
#define	x_M_ST_BOT	015		/* BOT encountered */
#define	x_M_ST_TAPEM	016		/* Tape mark encountered */
#define	x_M_ST_RDTRN	020		/* Record data truncated */
#define	x_M_ST_PLOST	021		/* Position lost */
#define	x_M_ST_SEX	022		/* Serious exception */
#define	x_M_ST_LED	023		/* LEOT detected */
#define	x_M_ST_DIAG	037		/* Message from an internal diagnostic */
 
/*
 * An MSCP packet
 */
 
struct x_mscp {
	struct	x_mscp_header x_mscp_header;/* device specific header */
	x_long	x_mscp_cmdref;		/* command reference number */
	x_short	x_mscp_unit;		/* unit number */
	x_short	x_mscp_xxx1;		/* unused */
	x_u_char	x_mscp_opcode;		/* opcode */
	x_u_char	x_mscp_flags;		/* end message flags */
	x_short	x_mscp_modifier;		/* modifiers */
	union {
	struct {
		x_long	x_Mscp_bytecnt;	/* byte count */
		x_long	x_Mscp_buffer;	/* buffer descriptor */
		x_long	x_Mscp_mapbase;   /* physical addr of map registers */
		x_long	x_Mscp_xxx2;	/* unused */
		x_long	x_Mscp_lbn;	/* logical block number */
		x_long	x_Mscp_xxx4;	/* unused */
		x_long	*x_Mscp_dscptr;	/* pointer to descriptor (software) */
		x_long	x_Mscp_sftwds[17];/* software words, padding */
	} x_mscp_generic;
	struct {
		x_short	x_Mscp_version;	/* MSCP version */
		x_short	x_Mscp_cntflgs;	/* controller flags */
		x_short	x_Mscp_hsttmo;	/* host timeout */
		x_short	x_Mscp_usefrac;	/* use fraction */
		x_quad	x_Mscp_time;	/* time and date */
		x_long	x_Mscp_cntdep;	/* controller dependent parameters */
	} x_mscp_setcntchar;
	struct {
		x_short	x_Mscp_multunt;	/* multi-unit code */
		x_short	x_Mscp_unitflgs;	/* unit flags */
		x_long	x_Mscp_hostid;	/* host identifier */
		x_quad	x_Mscp_unitid;	/* unit identifier */
		x_long	x_Mscp_mediaid;	/* media type identifier */
		x_short	x_Mscp_format;	/* format (tape density) */
		x_short	x_Mscp_speed;	/* tape speed = (ips * bpi) /1000 */
		x_short	x_Mscp_fmtmenu;	/* format menu */
		x_short	x_Mscp_group;	/* group size */
		x_short	x_Mscp_cylinder;	/* cylinder size */
		x_short	x_Mscp_xxx3;	/* reserved */
		x_short	x_Mscp_rctsize;	/* RCT table size */
		char	x_Mscp_rbns;	/* RBNs / track */
		char	x_Mscp_rctcpys;	/* RCT copies */
	} x_mscp_getunitsts;
	} x_mscp_un;
	x_short x_mscp_fil1;
	x_short x_mscp_fil2;
	x_short x_mscp_fil3;
};
 
#define x_mscp_msglen (sizeof (struct x_mscp) - sizeof(struct x_mscp_header))
 
/*
 * generic packet
 */
 
#define	x_mscp_bytecnt	x_mscp_un.x_mscp_generic.x_Mscp_bytecnt
#define	x_mscp_buffer	x_mscp_un.x_mscp_generic.x_Mscp_buffer
#define	x_mscp_mapbase	x_mscp_un.x_mscp_generic.x_Mscp_mapbase
#define	x_mscp_lbn	x_mscp_un.x_mscp_generic.x_Mscp_lbn
#define	x_mscp_dscptr	x_mscp_un.x_mscp_generic.x_Mscp_dscptr
#define	x_mscp_sftwds	x_mscp_un.x_mscp_generic.x_Mscp_sftwds
#define	x_mscp_status	x_mscp_modifier
 
/*
 * Abort / Get Command Status packet
 */
 
#define	x_mscp_outref	x_mscp_bytecnt
 
/*
 * Set Controller Characteristics packet
 */
 
#define	x_mscp_version	x_mscp_un.x_mscp_setcntchar.x_Mscp_version
#define	x_mscp_cntflgs	x_mscp_un.x_mscp_setcntchar.x_Mscp_cntflgs
#define	x_mscp_hsttmo	x_mscp_un.x_mscp_setcntchar.x_Mscp_hsttmo
#define	x_mscp_usefrac	x_mscp_un.x_mscp_setcntchar.x_Mscp_usefrac
#define	x_mscp_time	x_mscp_un.x_mscp_setcntchar.x_Mscp_time
#define	x_mscp_cntdep	x_mscp_un.x_mscp_setcntchar.x_Mscp_cntdep
 
/*
 * Reposition command packet fields
 */
 
#define x_mscp_reccnt x_mscp_bytecnt	/* record/object count */
#define x_mscp_tmkcnt x_mscp_buffer		/* tape mark count */
 
/*
 * Get Unit Status end packet
 */
 
#define	x_mscp_multunt	x_mscp_un.x_mscp_getunitsts.x_Mscp_multunt
#define	x_mscp_unitflgs	x_mscp_un.x_mscp_getunitsts.x_Mscp_unitflgs
#define	x_mscp_hostid	x_mscp_un.x_mscp_getunitsts.x_Mscp_hostid
#define	x_mscp_unitid	x_mscp_un.x_mscp_getunitsts.x_Mscp_unitid
#define	x_mscp_mediaid	x_mscp_un.x_mscp_getunitsts.x_Mscp_mediaid
#define	x_mscp_format	x_mscp_un.x_mscp_getunitsts.x_Mscp_format /* density:0=high */
#define	x_mscp_speed	x_mscp_un.x_mscp_getunitsts.x_Mscp_speed  /* (ips*bpi)/1000 */
#define	x_mscp_fmtmenu	x_mscp_un.x_mscp_getunitsts.x_Mscp_fmtmenu
 
/*
 * Online / Set Unit Characteristics end packet
 */
 
#define	x_mscp_maxwrt	x_mscp_dscptr	/* max write byte count */
#define	x_mscp_noiserec	x_mscp_cylinder	/* noise record */
 
/*
 * Set Controller Characteristics end packet
 */
 
#define	x_mscp_cnttmo	x_mscp_hsttmo	/* controller timeout */
#define	x_mscp_cntcmdl	x_mscp_usefrac	/* controller soft & hardware version */
#define	x_mscp_cntid	x_mscp_unitid	/* controller id */
 
 
/*
 * Error Log message format codes
 */
#define	x_M_FM_CNTERR	0	/* Controller error */
#define	x_M_FM_BUSADDR	1	/* Host memory access error */
#define	x_M_FM_TAPETRN	5	/* Tape transfer error */
#define	x_M_FM_STIERR	6	/* STI communication or command failure */
#define	x_M_FM_STIDEL	7	/* STI drive error log */
#define	x_M_FM_STIFEL   010	/* STI formatter error log */
 
/*
 * Error Log message flags
 */
#define	x_M_LF_SUCC	0200	/* Operation successful */
#define	x_M_LF_CONT	0100	/* Operation continuing */
#define	x_M_LF_SQNRS	0001	/* Sequence number reset */
 
/*
 * Tape Format Flag Values
 */
#define	x_M_TF_800	001	/* NRZI 800 bpi */
#define	x_M_TF_PE		002	/* Phase Encoded 1600 bpi */
#define	x_M_TF_GCR	004	/* Group Code Recording 6250 bpi */
#define	x_M_TF_BLK	010	/* Cartridge Block Mode */
 
/*
 * MSCP Error Log packet
 *
 *	NOTE: MSCP packet must be padded to this size.
 */
 
struct x_mslg {
	struct	x_mscp_header x_mslg_header;/* device specific header */
	x_long	x_mslg_cmdref;		/* command reference number */
	x_short	x_mslg_unit;		/* unit number */
	x_short	x_mslg_seqnum;		/* sequence number */
	x_u_char	x_mslg_format;		/* format */
	x_u_char	x_mslg_flags;		/* error log message flags */
	x_short	x_mslg_event;		/* event code */
	x_quad	x_mslg_cntid;		/* controller id */
	x_u_char	x_mslg_cntsvr;		/* controller software version */
	x_u_char	x_mslg_cnthvr;		/* controller hardware version */
	x_short	x_mslg_multunt;		/* multi-unit code */
	x_quad	x_mslg_unitid;		/* unit id */
	x_u_char	x_mslg_unitsvr;		/* unit software version */
	x_u_char	x_mslg_unithvr;		/* unit hardware version */
	x_short	x_mslg_group;		/* group; retry + level */
	x_long	x_mslg_position;		/* position (object count) */
	x_u_char	x_mslg_fmtsvr;		/* formatter software version */
	x_u_char	x_mslg_fmthvr;		/* formatter hardware version */
	x_short	x_mslg_xxx2;		/* unused */
	char	x_mslg_stiunsucc[62];	/* STI status information */
};
 
#define	x_mslg_busaddr	x_mslg_unitid.x_val[0]
#define	x_mslg_sdecyl	x_mslg_group

