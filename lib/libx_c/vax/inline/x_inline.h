#include "x_.h"

/*
 * Copyright (c) 1984, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)inline.h	7.1 (Berkeley) 6/5/86
 */

/*
 * COMMENTCHAR is the character delimiting comments in the assembler.
 * LABELCHAR is the character that separates labels from instructions.
 * ARGSEPCHAR is the character that separates arguments in instructions.
 */
#define x_COMMENTCHAR	'#'
#define x_LABELCHAR	':'
#define x_ARGSEPCHAR	','

/*
 * Expansion parameters:
 *   QUEUESIZE is the number of instructions to be considered for 
 *	integration of argument pushes and pops
 *   MAXLINELEN is the longest expected input line
 *   MAXARGS is the maximum number of arguments in an assembly instruction
 */
#define x_QUEUESIZE	16
#define x_MAXLINELEN	1024
#define x_MAXARGS		10

/*
 * The following global variables are used to manipulate the queue of
 * recently seen instructions.
 *	line - The queue of instructions.
 *	bufhead - Pointer to next availble queue slot. It is not
 *		considered part of te instruction stream until
 *		bufhead is advanced.
 *	buftail - Pointer to last instruction in queue.
 * Note that bufhead == buftail implies that the queue is empty.
 */
x_int x_bufhead, x_buftail;
char x_line[x_QUEUESIZE][x_MAXLINELEN];

#define x_SUCC(x_qindex) ((x_qindex) + 1 == x_QUEUESIZE ? 0 : (x_qindex) + 1)
#define x_PRED(x_qindex) ((x_qindex) - 1 < 0 ? x_QUEUESIZE - 1 : (x_qindex) - 1)

/*
 * Hash table headers should be twice as big as the number of patterns.
 * They must be a power of two.
 */
#define x_HSHSIZ	128

/*
 * These tables specify the substitutions that are to be done.
 */
struct x_pats {
	x_int	x_args;
	char	*x_name;
	char	*x_replace;
	struct	x_pats *x_next;
	x_int	x_size;
};
struct x_pats *x_patshdr[x_HSHSIZ];
extern struct x_pats x_language_ptab[], x_libc_ptab[], x_machine_ptab[];
extern struct x_pats x_vax_libc_ptab[], x_vaxsubset_libc_ptab[];
extern struct x_pats x_vax_ptab[], x_vaxsubset_ptab[];

/*
 * This table defines the set of instructions that demark the
 * end of a basic block.
 */
struct x_inststoptbl {
	char	*x_name;
	struct	x_inststoptbl *x_next;
	x_int	x_size;
};
struct x_inststoptbl *x_inststoptblhdr[x_HSHSIZ];
extern struct x_inststoptbl x_inststoptable[];

/*
 * Miscellaneous functions.
 */
char *x_newline(), *x_copyline(), *x_doreplaceon();
