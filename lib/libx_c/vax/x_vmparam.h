#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)vmparam.h	7.1 (Berkeley) 6/5/86
 */

/*
 * Machine dependent constants for VAX
 */
/*
 * USRTEXT is the start of the user text/data space, while USRSTACK
 * is the top (end) of the user stack.  LOWPAGES and HIGHPAGES are
 * the number of pages from the beginning of the P0 region to the
 * beginning of the text and from the beginning of the P1 region to the
 * beginning of the stack respectively.
 */
#define	x_USRTEXT		0
#define	x_USRSTACK	(0x80000000-x_UPAGES*x_NBPG) /* Start of user stack */
#define	x_BTOPUSRSTACK	(0x400000 - x_UPAGES)	 /* btop(USRSTACK) */
/* number of ptes per page */
#define	x_P1PAGES		0x200000	/* number of pages in P1 region */
#define	x_LOWPAGES	0
#define	x_HIGHPAGES	x_UPAGES

/*
 * Virtual memory related constants, all in bytes
 */
#ifndef x_MAXTSIZ
#define	x_MAXTSIZ		(6*1024*1024)		/* max text size */
#endif
#ifndef x_DFLDSIZ
#define	x_DFLDSIZ		(6*1024*1024)		/* initial data size limit */
#endif
#ifndef x_MAXDSIZ
#define	x_MAXDSIZ		(16*1024*1024)		/* max data size */
#endif
#ifndef	x_DFLSSIZ
#define	x_DFLSSIZ		(512*1024)		/* initial stack size limit */
#endif
#ifndef	x_MAXSSIZ
#define	x_MAXSSIZ		x_MAXDSIZ			/* max stack size */
#endif

/*
 * Default sizes of swap allocation chunks (see dmap.h).
 * The actual values may be changed in vminit() based on MAXDSIZ.
 * With MAXDSIZ of 16Mb and NDMAP of 38, dmmax will be 1024.
 */
#define	x_DMMIN	32			/* smallest swap allocation */
#define	x_DMMAX	4096			/* largest potential swap allocation */
#define	x_DMTEXT	1024			/* swap allocation for text */

/*
 * Sizes of the system and user portions of the system page table.
 */
/* SYSPTSIZE IS SILLY; IT SHOULD BE COMPUTED AT BOOT TIME */
#define	x_SYSPTSIZE	((20+x_MAXUSERS)*x_NPTEPG)
#define	x_USRPTSIZE 	(32*x_NPTEPG)

/*
 * The size of the clock loop.
 */
#define	x_LOOPPAGES	(x_maxfree - x_firstfree)

/*
 * The time for a process to be blocked before being very swappable.
 * This is a number of seconds which the system takes as being a non-trivial
 * amount of real time.  You probably shouldn't change this;
 * it is used in subtle ways (fractions and multiples of it are, that is, like
 * half of a ``long time'', almost a long time, etc.)
 * It is related to human patience and other factors which don't really
 * change over time.
 */
#define	x_MAXSLP 		20

/*
 * A swapped in process is given a small amount of core without being bothered
 * by the page replacement algorithm.  Basically this says that if you are
 * swapped in you deserve some resources.  We protect the last SAFERSS
 * pages against paging and will just swap you out rather than paging you.
 * Note that each process has at least UPAGES+CLSIZE pages which are not
 * paged anyways (this is currently 8+2=10 pages or 5k bytes), so this
 * number just means a swapped in process is given around 25k bytes.
 * Just for fun: current memory prices are 4600$ a megabyte on VAX (4/22/81),
 * so we loan each swapped in process memory worth 100$, or just admit
 * that we don't consider it worthwhile and swap it out to disk which costs
 * $30/mb or about $0.75.
 */
#define	x_SAFERSS		32		/* nominal ``small'' resident set size
					   protected against replacement */

/*
 * DISKRPM is used to estimate the number of paging i/o operations
 * which one can expect from a single disk controller.
 */
#define	x_DISKRPM		60

/*
 * Klustering constants.  Klustering is the gathering
 * of pages together for pagein/pageout, while clustering
 * is the treatment of hardware page size as though it were
 * larger than it really is.
 *
 * KLMAX gives maximum cluster size in CLSIZE page (cluster-page)
 * units.  Note that KLMAX*CLSIZE must be <= DMMIN in dmap.h.
 */

#define	x_KLMAX	(32/x_CLSIZE)
#define	x_KLSEQL	(16/x_CLSIZE)		/* in klust if vadvise(VA_SEQL) */
#define	x_KLIN	(8/x_CLSIZE)		/* default data/stack in klust */
#define	x_KLTXT	(4/x_CLSIZE)		/* default text in klust */
#define	x_KLOUT	(32/x_CLSIZE)

/*
 * KLSDIST is the advance or retard of the fifo reclaim for sequential
 * processes data space.
 */
#define	x_KLSDIST	3		/* klusters advance/retard for seq. fifo */

/*
 * Paging thresholds (see vm_sched.c).
 * Strategy of 1/19/85:
 *	lotsfree is 512k bytes, but at most 1/4 of memory
 *	desfree is 200k bytes, but at most 1/8 of memory
 *	minfree is 64k bytes, but at most 1/2 of desfree
 */
#define	x_LOTSFREE	(512 * 1024)
#define	x_LOTSFREEFRACT	4
#define	x_DESFREE		(200 * 1024)
#define	x_DESFREEFRACT	8
#define	x_MINFREE		(64 * 1024)
#define	x_MINFREEFRACT	2

/*
 * There are two clock hands, initially separated by HANDSPREAD bytes
 * (but at most all of user memory).  The amount of time to reclaim
 * a page once the pageout process examines it increases with this
 * distance and decreases as the scan rate rises.
 */
#define	x_HANDSPREAD	(2 * 1024 * 1024)

/*
 * The number of times per second to recompute the desired paging rate
 * and poke the pagedaemon.
 */
#define	x_RATETOSCHEDPAGING	4

/*
 * Believed threshold (in megabytes) for which interleaved
 * swapping area is desirable.
 */
#define	x_LOTSOFMEM	2

/*
 * BEWARE THIS DEFINITION WORKS ONLY WITH COUNT OF 1
 */
#define	x_mapin(x_pte, x_v, x_pfnum, x_count, x_prot) \
	(*(x_int *)(x_pte) = (x_pfnum) | (x_prot), x_mtpr(x_TBIS, x_ptob(x_v)))
