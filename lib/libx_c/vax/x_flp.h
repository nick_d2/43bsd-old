#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)flp.h	7.1 (Berkeley) 6/5/86
 */

#if x_VAX780
/*
 * Console floppy command/status and sectoring information.
 */
#define	x_FL_FFC		0x200		/* floppy function complete */
#define	x_FL_ERR		0x80		/* error bit in floppy status byte */
#define	x_FL_PERR		0x905		/* floppy protocol error */
#define	x_FL_DATA		0x100		/* floppy data select code */
#define	x_FL_RS		0x900		/* floppy read sector command */
#define	x_FL_WS		0x901		/* floppy write sector command*/
#define	x_FL_STAT		0x902		/* floppy get status command*/
#define	x_FL_CANCEL	0x904		/* cancel floppy function */

#define	x_RXFTRK	77		/* tracks/floppy */
#define	x_RXSTRK	26		/* sectors/track */
#define	x_RXBYSEC	128		/* bytes/sector */
#define	x_MAXSEC (x_RXFTRK*x_RXSTRK) 	/* sectors/floppy */

/*
 * In the floppy driver routines, the device active byte is used
 * not as a boolean, but as an indicator of the state we are in.
 * That is, it contains what to do on the next interrupt.
 */

#define	x_FL_IDLE		0	/* floppy idle */
#define	x_FL_MAND		1	/* about to send read/write command */
#define	x_FL_SEC		2	/* about to send sector # to LSI */
#define	x_FL_TRACK	3	/* about to send track # to LSI */
#define	x_FL_DAX		4	/* transmitting data */
#define	x_FL_DAR		5	/* receiving data */
#define	x_FL_COM		6	/* completing transmission */
#define	x_FL_CAN		7	/* give cancel order - we had an error,
				   and are to restart */

#define	x_FLERRS		5	/* number of retries before quitting */

/*
 * The state byte is used to retain exclusivity,
 * and contains the busy flag.
 */
#define	x_FL_OPEN		1
#define	x_FL_BUSY		2
#endif
