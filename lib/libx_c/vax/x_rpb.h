#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)rpb.h	7.1 (Berkeley) 6/5/86
 */

/*
 * The restart parameter block, which is a page in (very) low
 * core which runs after a crash.  Currently, the restart
 * procedure takes a dump.
 */
struct x_rpb {
	struct	x_rpb *x_rp_selfref;	/* self-reference */
	x_int	(*x_rp_dumprout)();	/* routine to be called */
	x_long	x_rp_checksum;		/* checksum of 31 words of dumprout */
	x_long	x_rp_flag;		/* set to 1 when dumprout runs */
/* the dump stack grows from the end of the rpb page not to reach here */
};
#ifdef x_KERNEL
extern	struct x_rpb x_rpb;
#endif
