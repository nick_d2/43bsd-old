#include "x_.h"

/*
 * @(#)ioa.h	7.1 (Berkeley) 6/5/86
 */

/****************************************************************
 *                                                              *
 *        Licensed from Digital Equipment Corporation           *
 *                       Copyright (c)                          *
 *               Digital Equipment Corporation                  *
 *                   Maynard, Massachusetts                     *
 *                         1985, 1986                           *
 *                    All rights reserved.                      *
 *                                                              *
 *        The Information in this software is subject to change *
 *   without notice and should not be construed as a commitment *
 *   by  Digital  Equipment  Corporation.   Digital   makes  no *
 *   representations about the suitability of this software for *
 *   any purpose.  It is supplied "As Is" without expressed  or *
 *   implied  warranty.                                         *
 *                                                              *
 *        If the Regents of the University of California or its *
 *   licensees modify the software in a manner creating         *
 *   diriviative copyright rights, appropriate copyright        *
 *   legends may be placed on  the drivative work in addition   *
 *   to that set forth above.                                   *
 *								*
 ****************************************************************/

#define	x_MAXNIOA 4
#define	x_NIOA8600 2
#define x_IOASIZE 0x2000000
#define x_IOAMAPSIZ 512			/* Map one page to get at SBIA regs */
#define	x_IOA8600(x_i)	((x_caddr_t)(0x20080000+x_IOASIZE*x_i))

#ifndef x_LOCORE
struct	x_sbia_regs
{
	x_int x_sbi_cfg;
	x_int x_sbi_csr;
	x_int x_sbi_errsum;
	x_int x_sbi_dctl;
	x_int x_sbi_dmaica;
	x_int x_sbi_dmaiid;
	x_int x_sbi_dmaaca;
	x_int x_sbi_dmaaid;
	x_int x_sbi_dmabcs;
	x_int x_sbi_dmabid;
	x_int x_sbi_dmaccs;
	x_int x_sbi_dmacid;
	x_int x_sbi_silo;
	x_int x_sbi_error;
	x_int x_sbi_timo;
	x_int x_sbi_fltsts;
	x_int x_sbi_silcmp;
	x_int x_sbi_maint;
	x_int x_sbi_unjam;
	x_int x_sbi_qclr;
	x_int x_sbi_unused[12];
	x_int x_sbi_iv10;
	x_int x_sbi_iv11;
	x_int x_sbi_iv12;
	x_int x_sbi_iv13;
	x_int x_sbi_iv14;
	x_int x_sbi_iv15;
	x_int x_sbi_iv16;
	x_int x_sbi_iv17;
	x_int x_sbi_iv18;
	x_int x_sbi_iv19;
	x_int x_sbi_iv1a;
	x_int x_sbi_iv1b;
	x_int x_sbi_iv1c;
	x_int x_sbi_iv1d;
	x_int x_sbi_iv1e;
};
struct	x_ioa {
	union x_ioacsr {
		x_long	x_ioa_csr;
		x_u_char	x_ioa_type;
	} x_ioacsr;
	x_long	x_ioa_pad[x_IOAMAPSIZ / sizeof (x_long) - 1];
};
#ifdef	x_KERNEL
struct x_ioa x_ioa[x_MAXNIOA];
#endif
#endif

#define x_IOA_TYPMSK 0xf0
#define x_IOA_SBIA	0x10
