#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)frame.h	7.1 (Berkeley) 6/5/86
 */

/*
 * Definition of the vax calls/callg frame.
 */
struct x_frame {
	x_int	x_fr_handler;
	x_u_int	x_fr_psw:16,		/* saved psw */
		x_fr_mask:12,		/* register save mask */
		:1,
		x_fr_s:1,			/* call was a calls, not callg */
		x_fr_spa:2;		/* stack pointer alignment */
	x_int	x_fr_savap;		/* saved arg pointer */
	x_int	x_fr_savfp;		/* saved frame pointer */
	x_int	x_fr_savpc;		/* saved program counter */
};
