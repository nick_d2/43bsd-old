#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)mtpr.h	7.1 (Berkeley) 6/5/86
 */

/*
 * VAX processor register numbers
 */

#define	x_KSP	0x0		/* kernel stack pointer */
#define	x_ESP	0x1		/* exec stack pointer */
#define	x_SSP	0x2		/* supervisor stack pointer */
#define	x_USP	0x3		/* user stack pointer */
#define	x_ISP	0x4		/* interrupt stack pointer */
#define	x_P0BR	0x8		/* p0 base register */
#define	x_P0LR	0x9		/* p0 length register */
#define	x_P1BR	0xa		/* p1 base register */
#define	x_P1LR	0xb		/* p1 length register */
#define	x_SBR	0xc		/* system segment base register */
#define	x_SLR	0xd		/* system segment length register */
#define	x_PCBB	0x10		/* process control block base */
#define	x_SCBB	0x11		/* system control block base */
#define	x_IPL	0x12		/* interrupt priority level */
#define	x_ASTLVL	0x13		/* async. system trap level */
#define	x_SIRR	0x14		/* software interrupt request */
#define	x_SISR	0x15		/* software interrupt summary */
#define	x_ICCS	0x18		/* interval clock control */
#define	x_NICR	0x19		/* next interval count */
#define	x_ICR	0x1a		/* interval count */
#define	x_TODR	0x1b		/* time of year (day) */
#define	x_RXCS	0x20		/* console receiver control and status */
#define	x_RXDB	0x21		/* console receiver data buffer */
#define	x_TXCS	0x22		/* console transmitter control and status */
#define	x_TXDB	0x23		/* console transmitter data buffer */
#define	x_MAPEN	0x38		/* memory management enable */
#define	x_TBIA	0x39		/* translation buffer invalidate all */
#define	x_TBIS	0x3a		/* translation buffer invalidate single */
#define	x_PMR	0x3d		/* performance monitor enable */
#define	x_SID	0x3e		/* system identification */

#if defined(x_VAX780) || defined(x_VAX8600)
#define	x_ACCS	0x28		/* accelerator control and status */
#endif

#if defined(x_VAX8600)
#define x_TBCHK	0x3f		/* Translation Buffer Check */
#define x_PAMACC	0x40		/* PAMM access */
#define x_PAMLOC	0x41		/* PAMM location */
#define x_CSWP	0x42		/* Cache sweep */
#define x_MDECC	0x43		/* MBOX data ecc register */
#define x_MENA	0x44		/* MBOX error enable register */
#define x_MDCTL	0x45		/* MBOX data control register */
#define x_MCCTL	0x46		/* MBOX mcc control register */
#define x_MERG	0x47		/* MBOX	error generator register */
#define x_CRBT	0x48		/* Console reboot */
#define x_DFI	0x49		/* Diag fault insertion register */
#define x_EHSR	0x4a		/* Error handling status register */
#define x_STXCS	0x4c		/* Console block storage C/S */
#define x_STXDB	0x4d		/* Console block storage D/B */
#define x_ESPA	0x4e		/* EBOX scratchpad address */
#define x_ESPD	0x4f		/* EBOX sratchpad data */
#endif

#if defined(x_VAX780)
#define	x_ACCR	0x29		/* accelerator maintenance */
#define	x_WCSA	0x2c		/* WCS address */
#define	x_WCSD	0x2d		/* WCS data */
#define	x_SBIFS	0x30		/* SBI fault and status */
#define	x_SBIS	0x31		/* SBI silo */
#define	x_SBISC	0x32		/* SBI silo comparator */
#define	x_SBIMT	0x33		/* SBI maintenance */
#define	x_SBIER	0x34		/* SBI error register */
#define	x_SBITA	0x35		/* SBI timeout address */
#define	x_SBIQC	0x36		/* SBI quadword clear */
#define	x_MBRK	0x3c		/* micro-program breakpoint */
#endif

#if defined(x_VAX750) || defined(x_VAX730)
#define	x_MCSR	0x17		/* machine check status register */
#define	x_CSRS	0x1c		/* console storage receive status register */
#define	x_CSRD	0x1d		/* console storage receive data register */
#define	x_CSTS	0x1e		/* console storage transmit status register */
#define	x_CSTD	0x1f		/* console storage transmit data register */
#define	x_TBDR	0x24		/* translation buffer disable register */
#define	x_CADR	0x25		/* cache disable register */
#define	x_MCESR	0x26		/* machine check error summary register */
#define	x_CAER	0x27		/* cache error */
#define	x_IUR	0x37		/* init unibus register */
#define	x_TB	0x3b		/* translation buffer */
#endif
