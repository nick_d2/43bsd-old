#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)mscp.h	7.1 (Berkeley) 6/5/86
 */

/*
 * Definitions for the Mass Storage Control Protocol
 */


/*
 * Control message opcodes
 */
#define	x_M_OP_ABORT	0001	/* Abort command */
#define	x_M_OP_GTCMD	0002	/* Get command status command */
#define	x_M_OP_GTUNT	0003	/* Get unit status command */
#define	x_M_OP_STCON	0004	/* Set controller characteristics command */
#define	x_M_OP_SEREX	0007	/* Serious exception end message */
#define	x_M_OP_AVAIL	0010	/* Available command */
#define	x_M_OP_ONLIN	0011	/* Online command */
#define	x_M_OP_STUNT	0012	/* Set unit characteristics command */
#define	x_M_OP_DTACP	0013	/* Determine access paths command */
#define	x_M_OP_ACCES	0020	/* Access command */
#define	x_M_OP_CMPCD	0021	/* Compare controller data command */
#define	x_M_OP_ERASE	0022	/* Erase command */
#define	x_M_OP_FLUSH	0023	/* Flush command */
#define	x_M_OP_REPLC	0024	/* Replace command */
#define	x_M_OP_COMP	0040	/* Compare host data command */
#define	x_M_OP_READ	0041	/* Read command */
#define	x_M_OP_WRITE	0042	/* Write command */
#define	x_M_OP_AVATN	0100	/* Available attention message */
#define	x_M_OP_DUPUN	0101	/* Duplicate unit number attention message */
#define	x_M_OP_ACPTH	0102	/* Access path attention message */
#define	x_M_OP_END	0200	/* End message flag */


/*
 * Generic command modifiers
 */
#define	x_M_MD_EXPRS	0100000		/* Express request */
#define	x_M_MD_COMP	0040000		/* Compare */
#define	x_M_MD_CLSEX	0020000		/* Clear serious exception */
#define	x_M_MD_ERROR	0010000		/* Force error */
#define	x_M_MD_SCCHH	0004000		/* Suppress caching (high speed) */
#define	x_M_MD_SCCHL	0002000		/* Suppress caching (low speed) */
#define	x_M_MD_SECOR	0001000		/* Suppress error correction */
#define	x_M_MD_SEREC	0000400		/* Suppress error recovery */
#define	x_M_MD_SSHDW	0000200		/* Suppress shadowing */
#define	x_M_MD_WBKNV	0000100		/* Write back (non-volatile) */
#define	x_M_MD_WBKVL	0000040		/* Write back (volatile) */
#define	x_M_MD_WRSEQ	0000020		/* Write shadow set one unit at a time */

/*
 * AVAILABLE command modifiers
 */
#define	x_M_MD_ALLCD	0000002		/* All class drivers */
#define	x_M_MD_SPNDW	0000001		/* Spin down */

/*
 * FLUSH command modifiers
 */
#define	x_M_MD_FLENU	0000001		/* Flush entire unit */
#define	x_M_MD_VOLTL	0000002		/* Volatile only */

/*
 * GET UNIT STATUS command modifiers
 */
#define	x_M_MD_NXUNT	0000001		/* Next unit */

/*
 * ONLINE command modifiers
 */
#define	x_M_MD_RIP	0000001		/* Allow self destruction */
#define	x_M_MD_IGNMF	0000002		/* Ignore media format error */

/*
 * ONLINE and SET UNIT CHARACTERISTICS command modifiers
 */
#define	x_M_MD_ALTHI	0000040		/* Alter host identifier */
#define	x_M_MD_SHDSP	0000020		/* Shadow unit specified */
#define	x_M_MD_CLWBL	0000010		/* Clear write-back data lost */
#define	x_M_MD_STWRP	0000004		/* Set write protect */

/*
 * REPLACE command modifiers
 */
#define	x_M_MD_PRIMR	0000001		/* Primary replacement block */


/*
 * End message flags
 */
#define	x_M_EF_BBLKR	0200	/* Bad block reported */
#define	x_M_EF_BBLKU	0100	/* Bad block unreported */
#define	x_M_EF_ERLOG	0040	/* Error log generated */
#define	x_M_EF_SEREX	0020	/* Serious exception */


/*
 * Controller flags
 */
#define	x_M_CF_ATTN	0200	/* Enable attention messages */
#define	x_M_CF_MISC	0100	/* Enable miscellaneous error log messages */
#define	x_M_CF_OTHER	0040	/* Enable other host's error log messages */
#define	x_M_CF_THIS	0020	/* Enable this host's error log messages */
#define	x_M_CF_MLTHS	0004	/* Multi-host */
#define	x_M_CF_SHADW	0002	/* Shadowing */
#define	x_M_CF_576	0001	/* 576 byte sectors */


/*
 * Unit flags
 */
#define	x_M_UF_REPLC	0100000		/* Controller initiated bad block replacement */
#define	x_M_UF_INACT	0040000		/* Inactive shadow set unit */
#define	x_M_UF_WRTPH	0020000		/* Write protect (hardware) */
#define	x_M_UF_WRTPS	0010000		/* Write protect (software or volume) */
#define	x_M_UF_SCCHH	0004000		/* Suppress caching (high speed) */
#define	x_M_UF_SCCHL	0002000		/* Suppress caching (low speed) */
#define	x_M_UF_RMVBL	0000200		/* Removable media */
#define	x_M_UF_WBKNV	0000100		/* Write back (non-volatile) */
#define	x_M_UF_576	0000004		/* 576 byte sectors */
#define	x_M_UF_CMPWR	0000002		/* Compare writes */
#define	x_M_UF_CMPRD	0000001		/* Compare reads */


/*
 * Status codes
 */
#define	x_M_ST_MASK	037		/* Status code mask */
#define	x_M_ST_SUCC	000		/* Success */
#define	x_M_ST_ICMD	001		/* Invalid command */
#define	x_M_ST_ABRTD	002		/* Command aborted */
#define	x_M_ST_OFFLN	003		/* Unit offline */
#define	x_M_ST_AVLBL	004		/* Unit available */
#define	x_M_ST_MFMTE	005		/* Media format error */
#define	x_M_ST_WRTPR	006		/* Write protected */
#define	x_M_ST_COMP	007		/* Compare error */
#define	x_M_ST_DATA	010		/* Data error */
#define	x_M_ST_HSTBF	011		/* Host buffer access error */
#define	x_M_ST_CNTLR	012		/* Controller error */
#define	x_M_ST_DRIVE	013		/* Drive error */
#define	x_M_ST_DIAG	037		/* Message from an internal diagnostic */

/*
 * An MSCP packet
 */

struct x_mscp {
	struct	x_mscp_header x_mscp_header;/* device specific header */
	x_long	x_mscp_cmdref;		/* command reference number */
	x_short	x_mscp_unit;		/* unit number */
	x_short	x_mscp_xxx1;		/* unused */
	x_u_char	x_mscp_opcode;		/* opcode */
	x_u_char	x_mscp_flags;		/* end message flags */
	x_short	x_mscp_modifier;		/* modifiers */
	union {
	struct {
		x_long	x_Mscp_bytecnt;	/* byte count */
		x_long	x_Mscp_buffer;	/* buffer descriptor */
		x_long	x_Mscp_xxx2[2];	/* unused */
		x_long	x_Mscp_lbn;	/* logical block number */
		x_long	x_Mscp_xxx4;	/* unused */
		x_long	*x_Mscp_dscptr;	/* pointer to descriptor (software) */
		x_long	x_Mscp_sftwds[4];	/* software words, padding */
	} x_mscp_generic;
	struct {
		x_short	x_Mscp_version;	/* MSCP version */
		x_short	x_Mscp_cntflgs;	/* controller flags */
		x_short	x_Mscp_hsttmo;	/* host timeout */
		x_short	x_Mscp_usefrac;	/* use fraction */
		x_long	x_Mscp_time;	/* time and date */
	} x_mscp_setcntchar;
	struct {
		x_short	x_Mscp_multunt;	/* multi-unit code */
		x_short	x_Mscp_unitflgs;	/* unit flags */
		x_long	x_Mscp_hostid;	/* host identifier */
		x_quad	x_Mscp_unitid;	/* unit identifier */
		x_long	x_Mscp_mediaid;	/* media type identifier */
		x_short	x_Mscp_shdwunt;	/* shadow unit */
		x_short	x_Mscp_shdwsts;	/* shadow status */
		x_short	x_Mscp_track;	/* track size */
		x_short	x_Mscp_group;	/* group size */
		x_short	x_Mscp_cylinder;	/* cylinder size */
		x_short	x_Mscp_xxx3;	/* reserved */
		x_short	x_Mscp_rctsize;	/* RCT table size */
		char	x_Mscp_rbns;	/* RBNs / track */
		char	x_Mscp_rctcpys;	/* RCT copies */
	} x_mscp_getunitsts;
	} x_mscp_un;
	x_short x_mscp_fil1;
	x_short x_mscp_fil2;
	x_short x_mscp_fil3;
};

#define x_mscp_msglen (sizeof (struct x_mscp) - sizeof(struct x_mscp_header))

/*
 * generic packet
 */

#define	x_mscp_bytecnt	x_mscp_un.x_mscp_generic.x_Mscp_bytecnt
#define	x_mscp_buffer	x_mscp_un.x_mscp_generic.x_Mscp_buffer
#define	x_mscp_lbn	x_mscp_un.x_mscp_generic.x_Mscp_lbn
#define	x_mscp_dscptr	x_mscp_un.x_mscp_generic.x_Mscp_dscptr
#define	x_mscp_sftwds	x_mscp_un.x_mscp_generic.x_Mscp_sftwds
#define	x_mscp_status	x_mscp_modifier

/*
 * Abort / Get Command Status packet
 */

#define	x_mscp_outref	x_mscp_bytecnt

/*
 * Online / Set Unit Characteristics packet
 */

#define	x_mscp_errlgfl	x_mscp_lbn
#define	x_mscp_copyspd	x_mscp_shdwsts

/*
 * Replace packet
 */

#define	x_mscp_rbn	x_mscp_bytecnt

/*
 * Set Controller Characteristics packet
 */

#define	x_mscp_version	x_mscp_un.x_mscp_setcntchar.x_Mscp_version
#define	x_mscp_cntflgs	x_mscp_un.x_mscp_setcntchar.x_Mscp_cntflgs
#define	x_mscp_hsttmo	x_mscp_un.x_mscp_setcntchar.x_Mscp_hsttmo
#define	x_mscp_usefrac	x_mscp_un.x_mscp_setcntchar.x_Mscp_usefrac
#define	x_mscp_time	x_mscp_un.x_mscp_setcntchar.x_Mscp_time

/*
 * Get Unit Status end packet
 */

#define	x_mscp_multunt	x_mscp_un.x_mscp_getunitsts.x_Mscp_multunt
#define	x_mscp_unitflgs	x_mscp_un.x_mscp_getunitsts.x_Mscp_unitflgs
#define	x_mscp_hostid	x_mscp_un.x_mscp_getunitsts.x_Mscp_hostid
#define	x_mscp_unitid	x_mscp_un.x_mscp_getunitsts.x_Mscp_unitid
#define	x_mscp_mediaid	x_mscp_un.x_mscp_getunitsts.x_Mscp_mediaid
#define	x_mscp_shdwunt	x_mscp_un.x_mscp_getunitsts.x_Mscp_shdwunt
#define	x_mscp_shdwsts	x_mscp_un.x_mscp_getunitsts.x_Mscp_shdwsts
#define	x_mscp_track	x_mscp_un.x_mscp_getunitsts.x_Mscp_track
#define	x_mscp_group	x_mscp_un.x_mscp_getunitsts.x_Mscp_group
#define	x_mscp_cylinder	x_mscp_un.x_mscp_getunitsts.x_Mscp_cylinder
#define	x_mscp_rctsize	x_mscp_un.x_mscp_getunitsts.x_Mscp_rctsize
#define	x_mscp_rbns	x_mscp_un.x_mscp_getunitsts.x_Mscp_rbns
#define	x_mscp_rctcpys	x_mscp_un.x_mscp_getunitsts.x_Mscp_rctcpys

/*
 * Online / Set Unit Characteristics end packet
 */

#define	x_mscp_untsize	x_mscp_dscptr
#define	x_mscp_volser	x_mscp_sftwds[0]

/*
 * Set Controller Characteristics end packet
 */

#define	x_mscp_cnttmo	x_mscp_hsttmo
#define	x_mscp_cntcmdl	x_mscp_usefrac
#define	x_mscp_cntid	x_mscp_unitid


/*
 * Error Log message format codes
 */
#define	x_M_FM_CNTERR	0	/* Controller error */
#define	x_M_FM_BUSADDR	1	/* Host memory access error */
#define	x_M_FM_DISKTRN	2	/* Disk transfer error */
#define	x_M_FM_SDI	3	/* SDI error */
#define	x_M_FM_SMLDSK	4	/* Small disk error */

/*
 * Error Log message flags
 */
#define	x_M_LF_SUCC	0200	/* Operation successful */
#define	x_M_LF_CONT	0100	/* Operation continuing */
#define	x_M_LF_SQNRS	0001	/* Sequence number reset */

/*
 * MSCP Error Log packet
 *
 *	NOTE: MSCP packet must be padded to this size.
 */

struct x_mslg {
	struct	x_mscp_header x_mslg_header;/* device specific header */
	x_long	x_mslg_cmdref;		/* command reference number */
	x_short	x_mslg_unit;		/* unit number */
	x_short	x_mslg_seqnum;		/* sequence number */
	x_u_char	x_mslg_format;		/* format */
	x_u_char	x_mslg_flags;		/* error log message flags */
	x_short	x_mslg_event;		/* event code */
	x_quad	x_mslg_cntid;		/* controller id */
	x_u_char	x_mslg_cntsvr;		/* controller software version */
	x_u_char	x_mslg_cnthvr;		/* controller hardware version */
	x_short	x_mslg_multunt;		/* multi-unit code */
	x_quad	x_mslg_unitid;		/* unit id */
	x_u_char	x_mslg_unitsvr;		/* unit software version */
	x_u_char	x_mslg_unithvr;		/* unit hardware version */
	x_short	x_mslg_group;		/* group; retry + level */
	x_long	x_mslg_volser;		/* volume serial number */
	x_long	x_mslg_hdr;		/* header */
	char	x_mslg_sdistat[12];	/* SDI status information */
};

#define	x_mslg_busaddr	x_mslg_unitid.x_val[0]
#define	x_mslg_sdecyl	x_mslg_group

