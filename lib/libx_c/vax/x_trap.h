#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)trap.h	7.1 (Berkeley) 6/5/86
 */

/*
 * Trap type values
 */

/* The first three constant values are known to the real world <signal.h> */
#define	x_T_RESADFLT	0		/* reserved addressing fault */
#define	x_T_PRIVINFLT	1		/* privileged instruction fault */
#define	x_T_RESOPFLT	2		/* reserved operand fault */
/* End of known constants */
#define	x_T_BPTFLT	3		/* bpt instruction fault */
#define	x_T_XFCFLT	4		/* xfc instruction fault */
#define	x_T_SYSCALL	5		/* chmk instruction (syscall trap) */
#define	x_T_ARITHTRAP	6		/* arithmetic trap */
#define	x_T_ASTFLT	7		/* software level 2 trap (ast deliv) */
#define	x_T_SEGFLT	8		/* segmentation fault */
#define	x_T_PROTFLT	9		/* protection fault */
#define	x_T_TRCTRAP	10		/* trace trap */
#define	x_T_COMPATFLT	11		/* compatibility mode fault */
#define	x_T_PAGEFLT	12		/* page fault */
#define	x_T_TABLEFLT	13		/* page table fault */
