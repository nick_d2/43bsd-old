#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)mem.h	7.1 (Berkeley) 6/5/86
 */

/*
 * Memory controller registers
 *
 * The way in which the data is stored in these registers varies
 * per controller and cpu, so we define macros here to mask that.
 */
struct	x_mcr {
	x_int	x_mc_reg[6];
};

/*
 * Compute maximum possible number of memory controllers,
 * for sizing of the mcraddr array.
 */
#if x_VAX780
#define	x_MAXNMCR		4
#else
#define	x_MAXNMCR		1
#endif

/*
 * For each controller type we define 5 macros:
 *	M???_INH(mcr)		inhibits further crd interrupts from mcr
 *	M???_ENA(mcr)		enables another crd interrupt from mcr
 *	M???_ERR(mcr)		tells whether an error is waiting
 *	M???_SYN(mcr)		gives the syndrome bits of the error
 *	M???_ADDR(mcr)		gives the address of the error
 */

#if x_VAX8600
/*
 * 8600 register bit definitions
 */
#define	x_M8600_ICRD	0x400		/* inhibit crd interrupts */
#define x_M8600_TB_ERR	0xf00		/* translation buffer error mask */
/*
 * MDECC register
 */
#define	x_M8600_ADDR_PE	0x080000	/* address parity error */
#define x_M8600_DBL_ERR	0x100000	/* data double bit error */
#define	x_M8600_SNG_ERR	0x200000	/* data single bit error */
#define	x_M8600_BDT_ERR	0x400000	/* bad data error */

/*
 * ESPA register is used to address scratch pad registers in the Ebox.
 * To access a register in the scratch pad, write the ESPA with the address
 * and then read the ESPD register.  
 *
 * NOTE:  In assmebly code, the the mfpr instruction that reads the ESPD
 *	  register must immedately follow the mtpr instruction that setup
 *	  the ESPA register -- per the VENUS processor register spec.
 *
 * The scratchpad registers that are supplied for a single bit ECC 
 * error are:
 */
#define	x_SPAD_MSTAT1	0x25		/* scratch pad mstat1 register	*/
#define x_SPAD_MSTAT2	0x26		/* scratch pad mstat2 register	*/
#define x_SPAD_MDECC	0x27		/* scratch pad mdecc register	*/
#define x_SPAD_MEAR	0x2a		/* scratch pad mear register	*/

#define x_M8600_MEMERR(x_mdecc) ((x_mdecc) & 0x780000)
#define x_M8600_HRDERR(x_mdecc) ((x_mdecc) & 0x580000)
#define x_M8600_ENA (x_mtpr(x_MERG, (x_mfpr(x_MERG) & ~x_M8600_ICRD)))
#define x_M8600_INH (x_mtpr(x_EHSR, 0), x_mtpr(x_MERG, (x_mfpr(x_MERG) | x_M8600_ICRD)))
#define x_M8600_SYN(x_mdecc) (((x_mdecc) >> 9) & 0x3f)
#define x_M8600_ADDR(x_mear) ((x_mear) & 0x3ffffffc)
#define x_M8600_ARRAY(x_mear) (((x_mear) >> 22) & 0x0f)

#define x_M8600_MDECC_BITS "\20\27BAD_DT_ERR\26SNG_BIT_ERR\25DBL_BIT_ERR\
\24x_ADDR_PE"
#define x_M8600_MSTAT1_BITS "\20\30CPR_PE_A\27CPR_PE_B\26ABUS_DT_PE\
\25x_ABUS_CTL_MSK_PE\24x_ABUS_ADR_PE\23x_ABUS_C/x_A_CYCLE\22x_ABUS_ADP_1\21x_ABUS_ADP_0\
\20x_TB_MISS\17x_BLK_HIT\16x_C0_TAG_MISS\15x_CHE_MISS\14x_TB_VAL_ERR\13x_TB_PTE_B_PE\
\12x_TB_PTE_A_PE\11x_TB_TAG_PE\10x_WR_DT_PE_B3\7x_WR_DT_PE_B2\6x_WR_DT_PE_B1\
\5x_WR_DT_PE_B0\4x_CHE_RD_DT_PE\3x_CHE_SEL\2x_ANY_REFL\1x_CP_BW_CHE_DT_PE"
#define x_M8600_MSTAT2_BITS "\20\20CP_BYT_WR\17ABUS_BD_DT_CODE\10MULT_ERR\
\7x_CHE_TAG_PE\6x_CHE_TAG_W_PE\5x_CHE_WRTN_BIT\4x_NXM\3x_CP-x_IO_BUF_ERR\2x_MBOX_LOCK"
#endif

#if x_VAX780
#define	x_M780_ICRD	0x40000000	/* inhibit crd interrupts, in [2] */
#define	x_M780_HIER	0x20000000	/* high error rate, in reg[2] */
#define	x_M780_ERLOG	0x10000000	/* error log request, in reg[2] */
/* on a 780, memory crd's occur only when bit 15 is set in the SBIER */
/* register; bit 14 there is an error bit which we also clear */
/* these bits are in the back of the ``red book'' (or in the VMS code) */

#define	x_M780C_INH(x_mcr)	\
	(((x_mcr)->x_mc_reg[2] = (x_M780_ICRD|x_M780_HIER|x_M780_ERLOG)), x_mtpr(x_SBIER, 0))
#define	x_M780C_ENA(x_mcr)	\
	(((x_mcr)->x_mc_reg[2] = (x_M780_HIER|x_M780_ERLOG)), x_mtpr(x_SBIER, 3<<14))
#define	x_M780C_ERR(x_mcr)	\
	((x_mcr)->x_mc_reg[2] & (x_M780_ERLOG))

#define	x_M780C_SYN(x_mcr)	((x_mcr)->x_mc_reg[2] & 0xff)
#define	x_M780C_ADDR(x_mcr)	(((x_mcr)->x_mc_reg[2] >> 8) & 0xfffff)

#define	x_M780EL_INH(x_mcr)	\
	(((x_mcr)->x_mc_reg[2] = (x_M780_ICRD|x_M780_HIER|x_M780_ERLOG)), x_mtpr(x_SBIER, 0))
#define	x_M780EL_ENA(x_mcr)	\
	(((x_mcr)->x_mc_reg[2] = (x_M780_HIER|x_M780_ERLOG)), x_mtpr(x_SBIER, 3<<14))
#define	x_M780EL_ERR(x_mcr)	\
	((x_mcr)->x_mc_reg[2] & (x_M780_ERLOG))

#define	x_M780EL_SYN(x_mcr)	((x_mcr)->x_mc_reg[2] & 0x7f)
#define	x_M780EL_ADDR(x_mcr)	(((x_mcr)->x_mc_reg[2] >> 11) & 0x1ffff)

#define	x_M780EU_INH(x_mcr)	\
	(((x_mcr)->x_mc_reg[3] = (x_M780_ICRD|x_M780_HIER|x_M780_ERLOG)), x_mtpr(x_SBIER, 0))
#define	x_M780EU_ENA(x_mcr)	\
	(((x_mcr)->x_mc_reg[3] = (x_M780_HIER|x_M780_ERLOG)), x_mtpr(x_SBIER, 3<<14))
#define	x_M780EU_ERR(x_mcr)	\
	((x_mcr)->x_mc_reg[3] & (x_M780_ERLOG))

#define	x_M780EU_SYN(x_mcr)	((x_mcr)->x_mc_reg[3] & 0x7f)
#define	x_M780EU_ADDR(x_mcr)	(((x_mcr)->x_mc_reg[3] >> 11) & 0x1ffff)
#endif

#if x_VAX750
#define	x_M750_ICRD	0x10000000	/* inhibit crd interrupts, in [1] */
#define	x_M750_UNCORR	0xc0000000	/* uncorrectable error, in [0] */
#define	x_M750_CORERR	0x20000000	/* correctable error, in [0] */

#define	x_M750_INH(x_mcr)	((x_mcr)->x_mc_reg[1] = 0)
#define	x_M750_ENA(x_mcr)	((x_mcr)->x_mc_reg[0] = (x_M750_UNCORR|x_M750_CORERR), \
			    (x_mcr)->x_mc_reg[1] = x_M750_ICRD)
#define	x_M750_ERR(x_mcr)	((x_mcr)->x_mc_reg[0] & (x_M750_UNCORR|x_M750_CORERR))

#define	x_M750_SYN(x_mcr)	((x_mcr)->x_mc_reg[0] & 0x7f)
#define	x_M750_ADDR(x_mcr)	(((x_mcr)->x_mc_reg[0] >> 9) & 0x7fff)
#endif

#if x_VAX730
#define	x_M730_UNCORR	0x80000000	/* rds, uncorrectable error, in [1] */
#define	x_M730_CRD	0x40000000	/* crd, in [1] */
#define	x_M730_FTBPE	0x20000000	/* force tbuf parity error, in [1] */
#define	x_M730_ENACRD	0x10000000	/* enable crd interrupt, in [1] */
#define	x_M730_MME	0x08000000	/* mem-man enable (ala ipr), in [1] */
#define	x_M730_DM		0x04000000	/* diagnostic mode, in [1] */
#define	x_M730_DISECC	0x02000000	/* disable ecc, in [1] */

#define	x_M730_INH(x_mcr)	((x_mcr)->x_mc_reg[1] = x_M730_MME)
#define	x_M730_ENA(x_mcr)	((x_mcr)->x_mc_reg[1] = (x_M730_MME|x_M730_ENACRD))
#define	x_M730_ERR(x_mcr)	((x_mcr)->x_mc_reg[1] & (x_M730_UNCORR|x_M730_CRD))
#define	x_M730_SYN(x_mcr)	((x_mcr)->x_mc_reg[0] & 0x7f)
#define	x_M730_ADDR(x_mcr)	(((x_mcr)->x_mc_reg[0] >> 8) & 0x7fff)
#endif

/* controller types */
#define	x_M780C	1
#define	x_M780EL	2
#define	x_M780EU	3
#define	x_M750	4
#define	x_M730	5

#define	x_MEMINTVL	(60*10)		/* 10 minutes */

#ifdef	x_KERNEL
x_int	x_nmcr;
struct	x_mcr *x_mcraddr[x_MAXNMCR];
x_int	x_mcrtype[x_MAXNMCR];
#endif
