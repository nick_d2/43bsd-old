#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)rsp.h	7.1 (Berkeley) 6/5/86
 */

/*
 * TU58 transfer protocol constants and data structures
 */

/*
 * Structure of a command packet
 */
struct x_packet {
	x_u_char	x_pk_flag;	/* indicates packet type (cmd, data, etc.) */
	x_u_char	x_pk_mcount;	/* length of packet (bytes) */
	x_u_char	x_pk_op;		/* operation to perform (read, write, etc.) */
	x_u_char	x_pk_mod;		/* modifier for op or returned status */
	x_u_char	x_pk_unit;	/* unit number */
	x_u_char	x_pk_sw;		/* switches */
	x_u_short	x_pk_seq;		/* sequence number, always zero */
	x_u_short	x_pk_count;	/* requested byte count for read or write */
	x_u_short	x_pk_block;	/* block number for read, write, or seek */
	x_u_short	x_pk_chksum;	/* checksum, by words with end around carry */
};

/*
 * States
 */
#define	x_TUS_INIT1	0	/* sending nulls */
#define	x_TUS_INIT2	1	/* sending inits */
#define	x_TUS_IDLE	2	/* initialized, no transfer in progress */
#define	x_TUS_SENDH	3	/* sending header */
#define	x_TUS_SENDD	4	/* sending data */
#define	x_TUS_SENDC	5	/* sending checksum */
#define	x_TUS_SENDR	6	/* sending read command packet */
#define	x_TUS_SENDW	7	/* sending write command packet */
#define	x_TUS_GETH	8	/* reading header */
#define	x_TUS_GETD	9	/* reading data */
#define	x_TUS_GETC	10	/* reading checksum */
#define	x_TUS_GET		11	/* reading an entire packet */
#define	x_TUS_WAIT	12	/* waiting for continue */
#define	x_TUS_RCVERR	13	/* receiver error in pseudo DMA routine */
#define	x_TUS_CHKERR	14	/* checksum error in pseudo DMA routine */

#define	x_TUS_NSTATES	15

#define	x_printstate(x_state) \
	if ((x_state) < x_TUS_NSTATES) \
		x_printf("%s", x_tustates[(x_state)]); \
	else \
		x_printf("%d", (x_state));

/*
 * Packet Flags
 */
#define	x_TUF_DATA	1		/* data packet */
#define	x_TUF_CMD		2		/* command packet */
#define	x_TUF_INITF	4		/* initialize */
#define	x_TUF_CONT	020		/* continue */
#define	x_TUF_XOFF	023		/* flow control */

/*
 * Op Codes
 */
#define	x_TUOP_NOOP	0		/* no operation */
#define	x_TUOP_INIT	1		/* initialize */
#define	x_TUOP_READ	2		/* read block */
#define	x_TUOP_WRITE	3		/* write block */
#define	x_TUOP_SEEK	5		/* seek to block */
#define x_TUOP_DIAGNOSE	7		/* run micro-diagnostics */
#define	x_TUOP_END	0100		/* end packet */

/*
 * Mod Flags
 */
#define x_TUMD_WRV        1               /* write with read verify */

/*
 * Switches
 */
#define	x_TUSW_MRSP	010		/* use Modified RSP */

