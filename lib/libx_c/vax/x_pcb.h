#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)pcb.h	7.1 (Berkeley) 6/5/86
 */

/*
 * VAX process control block
 */

struct x_pcb
{
	x_int	x_pcb_ksp; 	/* kernel stack pointer */
	x_int	x_pcb_esp; 	/* exec stack pointer */
	x_int	x_pcb_ssp; 	/* supervisor stack pointer */
	x_int	x_pcb_usp; 	/* user stack pointer */
	x_int	x_pcb_r0; 
	x_int	x_pcb_r1; 
	x_int	x_pcb_r2; 
	x_int	x_pcb_r3; 
	x_int	x_pcb_r4; 
	x_int	x_pcb_r5; 
	x_int	x_pcb_r6; 
	x_int	x_pcb_r7; 
	x_int	x_pcb_r8; 
	x_int	x_pcb_r9; 
	x_int	x_pcb_r10; 
	x_int	x_pcb_r11; 
	x_int	x_pcb_r12; 
#define	x_pcb_ap x_pcb_r12
	x_int	x_pcb_r13; 
#define	x_pcb_fp x_pcb_r13
	x_int	x_pcb_pc; 	/* program counter */
	x_int	x_pcb_psl; 	/* program status longword */
	struct  x_pte *x_pcb_p0br; 	/* seg 0 base register */
	x_int	x_pcb_p0lr; 	/* seg 0 length register and astlevel */
	struct  x_pte *x_pcb_p1br; 	/* seg 1 base register */
	x_int	x_pcb_p1lr; 	/* seg 1 length register and pme */
/*
 * Software pcb (extension)
 */
	x_int	x_pcb_szpt; 	/* number of pages of user page table */
	x_int	x_pcb_cmap2;
	x_int	*x_pcb_sswap;
	x_int	x_pcb_sigc[5];
};

#define	x_AST_NONE	0x04000000	/* ast level */
#define	x_AST_USER	0x03000000	/* ast for user mode */

#define	x_ASTLVL_NONE	4
#define	x_ASTLVL_USER	3

#define	x_AST_CLR		0x07000000
#define	x_PME_CLR		0x80000000

#define	x_aston() \
	{ \
		x_mtpr(x_ASTLVL, x_ASTLVL_USER); \
	}

#define	x_astoff() \
	{ \
		x_mtpr(x_ASTLVL, x_ASTLVL_NONE); \
	}
