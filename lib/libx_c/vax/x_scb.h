#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)scb.h	7.1 (Berkeley) 6/5/86
 */

/*
 * VAX System control block layout
 */

struct x_scb {
	x_int	(*x_scb_stray)();		/* reserved */
	x_int	(*x_scb_machchk)();	/* machine chack */
	x_int	(*x_scb_kspinval)();	/* KSP invalid */
	x_int	(*x_scb_powfail)();	/* power fail */
	x_int	(*x_scb_resinstr)();	/* reserved instruction */
	x_int	(*x_scb_custinst)();	/* XFC instr */
	x_int	(*x_scb_resopnd)();	/* reserved operand */
	x_int	(*x_scb_resaddr)();	/* reserved addr mode */
	x_int	(*x_scb_acv)();		/* access control violation */
	x_int	(*x_scb_tnv)();		/* translation not valid */
	x_int	(*x_scb_tracep)();	/* trace pending */
	x_int	(*x_scb_bpt)();		/* breakpoint instr */
	x_int	(*x_scb_compat)();	/* compatibility mode fault */
	x_int	(*x_scb_arith)();		/* arithmetic fault */
	x_int	(*x_scb_stray2)();
	x_int	(*x_scb_stray3)();
	x_int	(*x_scb_chmk)();		/* CHMK instr */
	x_int	(*x_scb_chme)();		/* CHME instr */
	x_int	(*x_scb_chms)();		/* CHMS instr */
	x_int	(*x_scb_chmu)();		/* CHMU instr */
	x_int	(*x_scb_sbisilo)();	/* SBI silo compare */
	x_int	(*x_scb_cmrd)();		/* corrected mem read data */
	x_int	(*x_scb_sbialert)();	/* SBI alert */
	x_int	(*x_scb_sbiflt)();	/* SBI fault */
	x_int	(*x_scb_wtime)();		/* memory write timeout */
	x_int	(*x_scb_stray4[8])();
	x_int	(*x_scb_soft[15])();	/* software interrupt */
	x_int	(*x_scb_timer)();		/* interval timer interrupt */
	x_int	(*x_scb_stray5[7])();
	x_int	(*x_scb_stray6[4])();
	x_int	(*x_scb_csdr)();		/* console storage receive */
	x_int	(*x_scb_csdx)();		/* console storage transmit */
	x_int	(*x_scb_ctr)();		/* console terminal receive */
	x_int	(*x_scb_ctx)();		/* console terminal transmit */
	x_int	(*x_scb_ipl14[16])();	/* device interrupts IPL 14 */
	x_int	(*x_scb_ipl15[16])();	/*   "		"    IPL 15 */
	x_int	(*x_scb_ipl16[16])();	/*   "		"    IPL 16 */
	x_int	(*x_scb_ipl17[16])();	/*   "		"    IPL 17 */
	x_int	(*x_scb_ubaint[128])();	/* Unibus device intr */
	x_int	(*x_scb_uba1int[128])();	/* Unibus 1 device intr */
};

#ifdef x_KERNEL
extern	struct x_scb x_scb;
/* scb.scb_ubaint is the same as UNIvec */
#endif

#define	x_scbentry(x_f, x_how)		((x_int (*)())(((x_int)x_f)+x_how))

#define	x_SCB_KSTACK	0
#define	x_SCB_ISTACK	1
#define	x_SCB_WCS		2
#define	x_SCB_HALT	3
