#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)cpu.h	7.1 (Berkeley) 6/5/86
 */

#ifndef x_LOCORE
/*
 * Cpu identification, from SID register.
 */
union x_cpusid {
	x_int	x_cpusid;
	struct x_cpuany {
		x_u_int	:24,
			x_cp_type:8;
	} x_cpuany;
	struct x_cpu8600 {
		x_u_int	x_cp_sno:12,		/* serial number */
			x_cp_plant:4,		/* plant number */
			x_cp_eco:8,		/* eco level */
			x_cp_type:8;		/* VAX_8600 */
	} x_cpu8600;
	struct x_cpu780 {
		x_u_int	x_cp_sno:12,		/* serial number */
			x_cp_plant:3,		/* plant number */
			x_cp_eco:9,		/* eco level */
			x_cp_type:8;		/* VAX_780 */
	} x_cpu780;
	struct x_cpu750 {
		x_u_int	x_cp_hrev:8,		/* hardware rev level */
			x_cp_urev:8,		/* ucode rev level */
			:8,
			x_cp_type:8;		/* VAX_750 */
	} x_cpu750;
	struct x_cpu730 {
		x_u_int	:8,			/* reserved */
			x_cp_urev:8,		/* ucode rev level */
			:8,			/* reserved */
			x_cp_type:8;		/* VAX_730 */
	} x_cpu730;
 	struct x_cpu630 {
 		x_u_int	:24,			/* reserved */
 			x_cp_type:8;		/* VAX_630 */
 	} x_cpu630;
};
#endif
/*
 * Vax CPU types.
 * Similar types are grouped with their earliest example.
 */
#define	x_VAX_780		1
#define	x_VAX_750		2
#define	x_VAX_730		3
#define x_VAX_8600	4
#define	x_VAX_630		8

#define	x_VAX_MAX		8

/*
 * Main IO backplane types.
 * This gives us a handle on how to do autoconfiguration.
 */
#define	x_IO_SBI780	1
#define	x_IO_CMI750	2
#define	x_IO_XXX730	3
#define x_IO_ABUS		4
#define x_IO_QBUS		5

#ifndef x_LOCORE
/*
 * Per-cpu information for system.
 */
struct	x_percpu {
	x_short	x_pc_cputype;		/* cpu type code */
	x_short	x_pc_cpuspeed;		/* relative speed of cpu */
	x_short	x_pc_nioa;		/* number of IO adaptors/nexus blocks */
	struct	x_iobus *x_pc_io;		/* descriptions of IO adaptors */
};

struct x_iobus {
	x_int	x_io_type;		/* io adaptor types */
	x_caddr_t	x_io_addr;		/* phys address of IO adaptor */
	x_int	x_io_size;		/* size of an IO space */
	x_caddr_t	x_io_details;		/* specific to adaptor types */
};

/*
 * Description of a main bus that maps "nexi", ala the 780 SBI.
 */
struct x_nexusconnect {
	x_short	x_psb_nnexus;		/* number of nexus slots */
	struct	x_nexus *x_psb_nexbase;	/* base of nexus space */
/* we should be able to have just one address for the unibus memories */
/* and calculate successive addresses by adding to the base, but the 750 */
/* doesn't obey the sensible rule: uba1 has a lower address than uba0! */
	x_caddr_t	*x_psb_umaddr;		/* unibus memory addresses */
	x_short	x_psb_nubabdp;		/* number of bdp's per uba */
	x_short	x_psb_haveubasr;		/* have uba status register */
/* the 750 has some slots which don't promise to tell you their types */
/* if this pointer is non-zero, then you get the type from this array */
/* rather than from the (much more sensible) low byte of the config register */
	x_short	*x_psb_nextype;		/* botch */
};

#ifdef x_KERNEL
x_int	x_cpu;
struct	x_percpu x_percpu[];
#endif
#endif
