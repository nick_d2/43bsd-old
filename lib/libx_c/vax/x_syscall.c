#include "x_.h"

#include <x_unistd.h>
/* see usr.bin/lint/llib-lc */

#include <stdio.h>
#include <stdlib.h>

#include <sys/x_types.h>
#include <sys/x_time.h>

#include <sys/x_dir.h>
#include <sys/x_resource.h>
#include <sys/x_socket.h>
#include <sys/x_stat.h>
#include <sys/x_timeb.h>
#include <sys/x_times.h>
#include <sys/x_uio.h>
#include <sys/x_vtimes.h>
#include <sys/x_wait.h>

#include <netinet/x_in.h>

#include <netns/x_ns.h>

#include <arpa/x_inet.h>

#include <x_disktab.h>
#include <x_fstab.h>
#include <x_grp.h>
#include <x_ndbm.h>
#include <x_netdb.h>
#include <x_nlist.h>
#include <x_pwd.h>
#include <x_setjmp.h>
#include <x_sgtty.h>
#include <x_signal.h>
#include <x_stdio.h>
#include <x_ttyent.h>

#ifdef __STDC__
#include <stdarg.h>
#else
#ifdef __STDC__
#include <stdarg.h>
#define x__va_start(x_ap, x_arg) va_start(x_ap, x_arg)
#else
#include <varargs.h>
#define x__va_start(x_ap, x_arg) va_start(x_ap)
#endif
#endif

#include <x_errno.h>
#include <x_syscall.h>

x_int x_errno;

x_int x_accept(x_s, x_a, x_l) x_int x_s; struct x_sockaddr *x_a; x_int *x_l; {
  fprintf(stderr, "accept() called\n");
  abort();
}

x_int x_access(x_p, x_m) char *x_p; x_int x_m; {
  fprintf(stderr, "access() called\n");
  abort();
}

x_int x_acct(x_f) char *x_f; {
  fprintf(stderr, "acct() called\n");
  abort();
}

x_int x_adjtime(x_delta, x_odelta) struct x_timeval *x_delta; struct x_timeval *x_odelta; {
  fprintf(stderr, "adjtime() called\n");
  abort();
}

x_int x_bind(x_s, x_n, x_l) x_int x_s; struct x_sockaddr *x_n; x_int x_l; {
  fprintf(stderr, "bind() called\n");
  abort();
}

char *x_brk(x_a) char *x_a; {
  fprintf(stderr, "brk() called\n");
  abort();
}

x_int x_chdir(x_s) char *x_s; {
  fprintf(stderr, "chdir() called\n");
  abort();
}

x_int x_chmod(x_s, x_m) char *x_s; x_int x_m; {
  fprintf(stderr, "chmod() called\n");
  abort();
}

x_int x_chown(x_s, x_u, x_g) char *x_s; x_int x_u; x_int x_g; {
  fprintf(stderr, "chown() called\n");
  abort();
}

x_int x_chroot(x_d) char *x_d; {
  fprintf(stderr, "chroot() called\n");
  abort();
}

x_int x_close(x_f) x_int x_f; {
  fprintf(stderr, "close() called\n");
  abort();
}

x_int x_connect(x_s, x_n, x_l) x_int x_s; struct x_sockaddr *x_n; x_int x_l; {
  fprintf(stderr, "connect() called\n");
  abort();
}

x_int x_dup(x_f) x_int x_f; {
  fprintf(stderr, "dup() called\n");
  abort();
}

x_int x_dup2(x_o, x_n) x_int x_o; x_int x_n; {
  fprintf(stderr, "dup2() called\n");
  abort();
}

void x_execve(x_s, x_v, x_e) char *x_s; char *x_v[]; char *x_e[]; {
  fprintf(stderr, "execve() called\n");
  abort();
}

void x__exit(x_s) x_int x_s; {
  fprintf(stderr, "_exit() called\n");
  abort();
}

x_int x_fchmod(x_f, x_m) x_int x_f; x_int x_m; {
  fprintf(stderr, "fchmod() called\n");
  abort();
}

x_int x_fchown(x_f, x_u, x_g) x_int x_f; x_int x_u; x_int x_g; {
  fprintf(stderr, "fchown() called\n");
  abort();
}

x_int x_fcntl(x_f, x_c, x_a) x_int x_f; x_int x_c; x_int x_a; {
  fprintf(stderr, "fcntl() called\n");
  abort();
}

x_int x_flock(x_f, x_o) x_int x_f; x_int x_o; {
  fprintf(stderr, "flock() called\n");
  abort();
}

x_int x_fork() {
  fprintf(stderr, "fork() called\n");
  abort();
}

x_int x_fsync(x_f) x_int x_f; {
  fprintf(stderr, "fsync() called\n");
  abort();
}

x_int x_fstat(x_f, x_b) x_int x_f; struct x_stat *x_b; {
  fprintf(stderr, "fstat() called\n");
  abort();
}

x_int x_ftruncate(x_d, x_l) x_int x_d; x_off_t x_l; {
  fprintf(stderr, "ftruncate() called\n");
  abort();
}

x_int x_getdtablesize() {
  fprintf(stderr, "getdtablesize() called\n");
  abort();
}

x_gid_t x_getegid() {
  fprintf(stderr, "getegid() called\n");
  abort();
}

x_uid_t x_geteuid() {
  fprintf(stderr, "geteuid() called\n");
  abort();
}

x_gid_t x_getgid() {
  fprintf(stderr, "getgid() called\n");
  abort();
}

x_int x_getgroups(x_n, x_g) x_int x_n; x_int *x_g; {
  fprintf(stderr, "getgroups() called\n");
  abort();
}

x_long x_gethostid() {
  fprintf(stderr, "gethostid() called\n");
  abort();
}

x_int x_gethostname(x_n, x_l) char *x_n; x_int x_l; {
  fprintf(stderr, "gethostname() called\n");
  abort();
}

x_int x_getitimer(x_w, x_v) x_int x_w; struct x_itimerval *x_v; {
  fprintf(stderr, "getitimer() called\n");
  abort();
}

x_int x_getpagesize() {
  fprintf(stderr, "getpagesize() called\n");
  abort();
}

x_int x_getpeername(x_s, x_n, x_l) x_int x_s; struct x_sockaddr *x_n; x_int *x_l; {
  fprintf(stderr, "getpeername() called\n");
  abort();
}

x_int x_getpgrp(x_p) x_int x_p; {
  fprintf(stderr, "getpgrp() called\n");
  abort();
}

x_int x_getpid() {
  fprintf(stderr, "getpid() called\n");
  abort();
}

x_int x_getppid() {
  fprintf(stderr, "getppid() called\n");
  abort();
}

x_int x_getpriority(x_w, x_who) x_int x_w; x_int x_who; {
  fprintf(stderr, "getpriority() called\n");
  abort();
}

x_int x_getrlimit(x_res, x_rip) x_int x_res; struct x_rlimit *x_rip; {
  fprintf(stderr, "getrlimit() called\n");
  abort();
}

x_int x_getrusage(x_res, x_rip) x_int x_res; struct x_rusage *x_rip; {
  fprintf(stderr, "getrusage() called\n");
  abort();
}

x_int x_getsockname(x_s, x_name, x_namelen) x_int x_s; char *x_name; x_int *x_namelen; {
  fprintf(stderr, "getsockname() called\n");
  abort();
}

x_int x_getsockopt(x_s, x_level, x_opt, x_buf, x_len) x_int x_s; x_int x_level; x_int x_opt; char *x_buf; x_int *x_len; {
  fprintf(stderr, "getsockopt() called\n");
  abort();
}

x_int x_gettimeofday(x_t, x_z) struct x_timeval *x_t; struct x_timezone *x_z; {
  fprintf(stderr, "gettimeofday() called\n");
  abort();
}

x_uid_t x_getuid() {
  fprintf(stderr, "getuid() called\n");
  abort();
}

x_int x_ioctl(x_d, x_r, x_p) x_int x_d; x_u_long x_r; char *x_p; {
  fprintf(stderr, "ioctl() called\n");
  abort();
}

x_int x_kill(x_p, x_s) x_int x_p; x_int x_s; {
  fprintf(stderr, "kill() called\n");
  abort();
}

x_int x_killpg(x_pg, x_s) x_int x_pg; x_int x_s; {
  fprintf(stderr, "killpg() called\n");
  abort();
}

x_int x_link(x_a, x_b) char *x_a; char *x_b; {
  fprintf(stderr, "link() called\n");
  abort();
}

x_int x_listen(x_s, x_b) x_int x_s; x_int x_b; {
  fprintf(stderr, "listen() called\n");
  abort();
}

x_off_t x_lseek(x_f, x_o, x_d) x_int x_f; x_off_t x_o; x_int x_d; {
  fprintf(stderr, "lseek() called\n");
  abort();
}

x_int x_lstat(x_s, x_b) char *x_s; struct x_stat *x_b; {
  fprintf(stderr, "lstat() called\n");
  abort();
}

#ifdef x_notdef
x_int x_madvise(x_a, x_l, x_b) char *x_a; x_int x_l; x_int x_b; {
  fprintf(stderr, "madvise() called\n");
  abort();
}

x_int x_mmap(x_a, x_l, x_p, x_s, x_f, x_o) char *x_a; x_int x_l; x_int x_p; x_int x_s; x_int x_f; x_off_t x_o; {
  fprintf(stderr, "mmap() called\n");
  abort();
}

x_int x_mincore(x_a, x_l, x_v) char *x_a; x_int x_l; char *x_v; {
  fprintf(stderr, "mincore() called\n");
  abort();
}

#endif
x_int x_mkdir(x_p, x_m) char *x_p; x_int x_m; {
  fprintf(stderr, "mkdir() called\n");
  abort();
}

x_int x_mknod(x_n, x_m, x_a) char *x_n; x_int x_m; x_int x_a; {
  fprintf(stderr, "mknod() called\n");
  abort();
}

x_int x_mount(x_s, x_n, x_f) char *x_s; char *x_n; x_int x_f; {
  fprintf(stderr, "mount() called\n");
  abort();
}

#ifdef x_notdef
x_int x_mprotect(x_a, x_l, x_p) char *x_a; x_int x_l; x_int x_p; {
  fprintf(stderr, "mprotect() called\n");
  abort();
}

x_int x_mremap(x_a, x_l, x_p, x_s, x_f) char *x_a; x_int x_l; x_int x_p; x_int x_s; x_int x_f; {
  fprintf(stderr, "mremap() called\n");
  abort();
}

x_int x_munmap(x_a, x_l) char *x_a; x_int x_l; {
  fprintf(stderr, "munmap() called\n");
  abort();
}
#endif

#ifdef __STDC__
x_int x_open(char *x_f, x_int x_m, ...)
#else
x_int x_open(x_f, x_m, va_alist) char *x_f; x_int x_m; va_dcl
#endif
{
  fprintf(stderr, "open() called\n");
  abort();
}

x_int x_pipe(x_f) x_int x_f[2]; {
  fprintf(stderr, "pipe() called\n");
  abort();
}

void x_profil(x_b, x_s, x_o, x_i) char *x_b; x_int x_s; x_int x_o; x_int x_i; {
  fprintf(stderr, "profil() called\n");
  abort();
}

x_int x_ptrace(x_r, x_p, x_a, x_d) x_int x_r; x_int x_p; x_int *x_a; x_int x_d; {
  fprintf(stderr, "ptrace() called\n");
  abort();
}

x_int x_quota(x_c, x_u, x_a, x_ad) x_int x_c; x_int x_u; x_int x_a; char *x_ad; {
  fprintf(stderr, "quota() called\n");
  abort();
}

x_int x_read(x_f, x_b, x_l) x_int x_f; char *x_b; x_int x_l; {
  fprintf(stderr, "read() called\n");
  abort();
}

x_int x_readv(x_d, x_v, x_l) x_int x_d; struct x_iovec *x_v; x_int x_l; {
  fprintf(stderr, "readv() called\n");
  abort();
}

x_int x_readlink(x_p, x_b, x_s) char *x_p; char *x_b; x_int x_s; {
  fprintf(stderr, "readlink() called\n");
  abort();
}

void x_reboot(x_h) x_int x_h; {
  fprintf(stderr, "reboot() called\n");
  abort();
}

x_int x_recv(x_s, x_b, x_l, x_f) x_int x_s; char *x_b; x_int x_l; x_int x_f; {
  fprintf(stderr, "recv() called\n");
  abort();
}

x_int x_recvfrom(x_s, x_b, x_l, x_f, x_fr, x_fl) x_int x_s; char *x_b; x_int x_l; x_int x_f; struct x_sockaddr *x_fr; x_int *x_fl; {
  fprintf(stderr, "recvfrom() called\n");
  abort();
}

x_int x_recvmsg(x_s, x_m, x_f) x_int x_s; struct x_msghdr x_m[]; x_int x_f; {
  fprintf(stderr, "recvmsg() called\n");
  abort();
}

x_int x_rename(x_f, x_t) char *x_f; char *x_t; {
  fprintf(stderr, "rename() called\n");
  abort();
}

x_int x_rmdir(x_p) char *x_p; {
  fprintf(stderr, "rmdir() called\n");
  abort();
}

char *x_sbrk(x_i) x_int x_i; {
  fprintf(stderr, "sbrk() called\n");
  abort();
}

x_int x_select(x_n, x_r, x_w, x_e, x_t) x_int x_n; x_fd_set *x_r; x_fd_set *x_w; x_fd_set *x_e; struct x_timeval *x_t; {
  fprintf(stderr, "select() called\n");
  abort();
}

x_int x_send(x_s, x_m, x_l, x_f) x_int x_s; char *x_m; x_int x_l; x_int x_f; {
  fprintf(stderr, "send() called\n");
  abort();
}

x_int x_sendto(x_s, x_m, x_l, x_f, x_t, x_tl) x_int x_s; char *x_m; x_int x_l; x_int x_f; struct x_sockaddr *x_t; x_int x_tl; {
  fprintf(stderr, "sendto() called\n");
  abort();
}

x_int x_sendmsg(x_s, x_m, x_l) x_int x_s; struct x_msghdr x_m[]; x_int x_l; {
  fprintf(stderr, "sendmsg() called\n");
  abort();
}

x_int x_setgroups(x_n, x_g) x_int x_n; x_int *x_g; {
  fprintf(stderr, "setgroups() called\n");
  abort();
}

x_int x_sethostid(x_h) x_long x_h; {
  fprintf(stderr, "sethostid() called\n");
  abort();
}

x_int x_sethostname(x_n, x_l) char *x_n; x_int x_l; {
  fprintf(stderr, "sethostname() called\n");
  abort();
}

x_int x_setitimer(x_w, x_v, x_ov) x_int x_w; struct x_itimerval *x_v; struct x_itimerval *x_ov; {
  fprintf(stderr, "setitimer() called\n");
  abort();
}

x_int x_setpgrp(x_g, x_pg) x_int x_g; x_int x_pg; {
  fprintf(stderr, "setpgrp() called\n");
  abort();
}

x_int x_setpriority(x_w, x_who, x_pri) x_int x_w; x_int x_who; x_int x_pri; {
  fprintf(stderr, "setpriority() called\n");
  abort();
}

x_int x_setquota(x_s, x_f) char *x_s; char *x_f; {
  fprintf(stderr, "setquota() called\n");
  abort();
}

x_int x_setregid(x_r, x_e) x_int x_r; x_int x_e; {
  fprintf(stderr, "setregid() called\n");
  abort();
}

x_int x_setreuid(x_r, x_e) x_int x_r; x_int x_e; {
  fprintf(stderr, "setreuid() called\n");
  abort();
}

x_int x_setrlimit(x_res, x_rip) x_int x_res; struct x_rlimit *x_rip; {
  fprintf(stderr, "setrlimit() called\n");
  abort();
}

x_int x_setsockopt(x_s, x_level, x_opt, x_buf, x_len) x_int x_s; x_int x_level; x_int x_opt; char *x_buf; x_int x_len; {
  fprintf(stderr, "setsockopt() called\n");
  abort();
}

x_int x_settimeofday(x_t, x_z) struct x_timeval *x_t; struct x_timezone *x_z; {
  fprintf(stderr, "settimeofday() called\n");
  abort();
}

x_int x_shutdown(x_s, x_h) x_int x_s; x_int x_h; {
  fprintf(stderr, "shutdown() called\n");
  abort();
}

x_int (*x_signal(x_c, x_f))() x_int x_c; x_int (*x_f)(); {
  fprintf(stderr, "signal() called\n");
  abort();
}

x_int x_sigvec(x_c, x_f, x_m) x_int x_c; struct x_sigvec *x_f; struct x_sigvec *x_m; {
  fprintf(stderr, "sigvec() called\n");
  abort();
}

x_int x_sigblock(x_m) x_int x_m; {
  fprintf(stderr, "sigblock() called\n");
  abort();
}

x_int x_sigsetmask(x_m) x_int x_m; {
  fprintf(stderr, "sigsetmask() called\n");
  abort();
}

void x_sigpause(x_m) x_int x_m; {
  fprintf(stderr, "sigpause() called\n");
  abort();
}

x_int x_sigreturn(x_scp) struct x_sigcontext *x_scp; {
  fprintf(stderr, "sigreturn() called\n");
  abort();
}

x_int x_sigstack(x_ss, x_oss) struct x_sigstack *x_ss; struct x_sigstack *x_oss; {
  fprintf(stderr, "sigstack() called\n");
  abort();
}

x_int x_socket(x_a, x_t, x_p) x_int x_a; x_int x_t; x_int x_p; {
  fprintf(stderr, "socket() called\n");
  abort();
}

x_int x_socketpair(x_d, x_t, x_p, x_s) x_int x_d; x_int x_t; x_int x_p; x_int x_s[2]; {
  fprintf(stderr, "socketpair() called\n");
  abort();
}

x_int x_stat(x_s, x_b) char *x_s; struct x_stat *x_b; {
  fprintf(stderr, "stat() called\n");
  abort();
}

#ifdef x_notdef
char *x_stk(x_a) char *x_a; {
  fprintf(stderr, "stk() called\n");
  abort();
}

char *x_sstk(x_a) x_int x_a; {
  fprintf(stderr, "sstk() called\n");
  abort();
}
#endif

x_int x_swapon(x_s) char *x_s; {
  fprintf(stderr, "swapon() called\n");
  abort();
}

x_int x_symlink(x_t, x_f) char *x_t; char *x_f; {
  fprintf(stderr, "symlink() called\n");
  abort();
}

void x_sync() {
  fprintf(stderr, "sync() called\n");
  abort();
}

x_int x_truncate(x_p, x_l) char *x_p; x_off_t x_l; {
  fprintf(stderr, "truncate() called\n");
  abort();
}

x_int x_umask(x_n) x_int x_n; {
  fprintf(stderr, "umask() called\n");
  abort();
}

x_int x_umount(x_s) char *x_s; {
  fprintf(stderr, "umount() called\n");
  abort();
}

x_int x_unlink(x_s) char *x_s; {
  fprintf(stderr, "unlink() called\n");
  abort();
}

x_int x_utimes(x_f, x_t) char *x_f; struct x_timeval x_t[2]; {
  fprintf(stderr, "utimes() called\n");
  abort();
}

x_int x_vfork() {
  fprintf(stderr, "vfork() called\n");
  abort();
}

void x_vhangup() {
  fprintf(stderr, "vhangup() called\n");
  abort();
}

x_int x_wait(x_s) union x_wait *x_s; {
  fprintf(stderr, "wait() called\n");
  abort();
}

x_int x_wait3(x_s, x_o, x_r) union x_wait *x_s; x_int x_o; struct x_rusage *x_r; {
  fprintf(stderr, "wait3() called\n");
  abort();
}

x_int x_write(x_f, x_b, x_l) x_int x_f; char *x_b; x_int x_l; {
  fprintf(stderr, "write() called\n");
  abort();
}

x_int x_writev(x_f, x_v, x_l) x_int x_f; struct x_iovec *x_v; x_int x_l; {
  fprintf(stderr, "writev() called\n");
  abort();
}
