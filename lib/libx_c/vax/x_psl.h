#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)psl.h	7.1 (Berkeley) 6/5/86
 */

/*
 * VAX program status longword
 */

#define	x_PSL_C		0x00000001	/* carry bit */
#define	x_PSL_V		0x00000002	/* overflow bit */
#define	x_PSL_Z		0x00000004	/* zero bit */
#define	x_PSL_N		0x00000008	/* negative bit */
#define	x_PSL_ALLCC	0x0000000f	/* all cc bits - unlikely */
#define	x_PSL_T		0x00000010	/* trace enable bit */
#define	x_PSL_IV		0x00000020	/* integer overflow enable bit */
#define	x_PSL_FU		0x00000040	/* floating point underflow enable */
#define	x_PSL_DV		0x00000080	/* decimal overflow enable bit */
#define	x_PSL_IPL		0x001f0000	/* interrupt priority level */
#define	x_PSL_PRVMOD	0x00c00000	/* previous mode (all on is user) */
#define	x_PSL_CURMOD	0x03000000	/* current mode (all on is user) */
#define	x_PSL_IS		0x04000000	/* interrupt stack */
#define	x_PSL_FPD		0x08000000	/* first part done */
#define	x_PSL_TP		0x40000000	/* trace pending */
#define	x_PSL_CM		0x80000000	/* compatibility mode */

#define	x_PSL_MBZ		0x3020ff00	/* must be zero bits */

#define	x_PSL_USERSET	(x_PSL_PRVMOD|x_PSL_CURMOD)
#define	x_PSL_USERCLR	(x_PSL_IS|x_PSL_IPL|x_PSL_MBZ)
