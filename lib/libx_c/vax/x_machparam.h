#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)machparam.h	7.1 (Berkeley) 6/5/86
 */

/*
 * Machine dependent constants for vax.
 */
#define	x_NBPG	512		/* bytes/page */
#define	x_PGOFSET	(x_NBPG-1)	/* byte offset into page */
#define	x_PGSHIFT	9		/* LOG2(NBPG) */

#define	x_CLSIZE		2
#define	x_CLSIZELOG2	1

#define	x_SSIZE	4		/* initial stack size/NBPG */
#define	x_SINCR	4		/* increment of stack/NBPG */

#define	x_UPAGES	10		/* pages of u-area */

/*
 * Some macros for units conversion
 */
/* Core clicks (512 bytes) to segments and vice versa */
#define	x_ctos(x_x)	(x_x)
#define	x_stoc(x_x)	(x_x)

/* Core clicks (512 bytes) to disk blocks */
#define	x_ctod(x_x)	(x_x)
#define	x_dtoc(x_x)	(x_x)
#define	x_dtob(x_x)	((x_x)<<9)

/* clicks to bytes */
#define	x_ctob(x_x)	((x_x)<<9)

/* bytes to clicks */
#define	x_btoc(x_x)	((((unsigned)(x_x)+511)>>9))

/*
 * Macros to decode processor status word.
 */
#define	x_USERMODE(x_ps)	(((x_ps) & x_PSL_CURMOD) == x_PSL_CURMOD)
#define	x_BASEPRI(x_ps)	(((x_ps) & x_PSL_IPL) == 0)

#ifdef x_KERNEL
#ifndef x_LOCORE
x_int	x_cpuspeed;
#endif
#define	x_DELAY(x_n)	{ register x_int x_N = x_cpuspeed * (x_n); while (--x_N > 0); }

#else
#define	x_DELAY(x_n)	{ register x_int x_N = (x_n); while (--x_N > 0); }
#endif
