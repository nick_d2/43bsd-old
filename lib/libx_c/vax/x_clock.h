#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)clock.h	7.1 (Berkeley) 6/5/86
 */

/*
 * VAX clock registers
 */

#define	x_ICCS_RUN	0x00000001
#define	x_ICCS_TRANS	0x00000010
#define	x_ICCS_SS		0x00000020
#define	x_ICCS_IE		0x00000040
#define	x_ICCS_INT	0x00000080
#define	x_ICCS_ERR	0x80000000
	
#define	x_SECDAY		((unsigned)(24*60*60))		/* seconds per day */
#define	x_SECYR		((unsigned)(365*x_SECDAY))	/* per common year */
/*
 * TODRZERO is the what the TODR should contain when the ``year'' begins.
 * The TODR should always contain a number between 0 and SECYR+SECDAY.
 */
#define	x_TODRZERO	((unsigned)(1<<28))

#define	x_YRREF		1970
#define	x_LEAPYEAR(x_year)	((x_year)%4==0)	/* good till time becomes negative */

/*
 * Has the time-of-day clock wrapped around?
 */
#define	x_clkwrap()	(((unsigned)x_mfpr(x_TODR) - x_TODRZERO)/100 > x_SECYR+x_SECDAY)

/*
 * Software clock is software interrupt level 8,
 * implemented as mtpr(SIRR, 0x8) in asm.sed.
 */
