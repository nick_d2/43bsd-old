#include "x_.h"

/*
 * Copyright (c) 1982, 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)nexus.h	7.1 (Berkeley) 6/5/86
 */

/*
 * Information about nexus's.
 *
 * Each machine has an address of backplane slots (nexi).
 * Each nexus is some type of adapter, whose code is the low
 * byte of the first word of the adapter address space.
 * At boot time the system looks through the array of available
 * slots and finds the interconnects for the machine.
 */
#define	x_NNEXSBI		16
#if x_VAX8600
#define	x_NNEX8600	x_NNEXSBI
#define	x_NEXA8600	((struct x_nexus *)(0x20000000))
#define	x_NEXB8600	((struct x_nexus *)(0x22000000))
#endif
#if x_VAX780
#define	x_NNEX780	x_NNEXSBI
#define	x_NEX780	((struct x_nexus *)0x20000000)
#endif
#if x_VAX750
#define	x_NNEX750	x_NNEXSBI
#define	x_NEX750	((struct x_nexus *)0xf20000)
#endif
#if x_VAX730
#define	x_NNEX730	x_NNEXSBI
#define	x_NEX730	((struct x_nexus *)0xf20000)
#endif
#if x_VAX630
#define	x_NNEX630	1
#define	x_NEX630	((struct x_nexus *)0x20088000)
#endif
#define	x_NEXSIZE	0x2000

#if x_VAX8600
#define	x_MAXNNEXUS (2 * x_NNEXSBI)
#else
#define	x_MAXNNEXUS x_NNEXSBI
#endif

#ifndef x_LOCORE
struct	x_nexus {
	union x_nexcsr {
		x_long	x_nex_csr;
		x_u_char	x_nex_type;
	} x_nexcsr;
	x_long	x_nex_pad[x_NEXSIZE / sizeof (x_long) - 1];
};
#ifdef	x_KERNEL
struct x_nexus x_nexus[x_MAXNNEXUS];
#endif
#endif

/*
 * Bits in high word of nexus's.
 */
#define	x_SBI_PARFLT	(1<<31)		/* sbi parity fault */
#define	x_SBI_WSQFLT	(1<<30)		/* write sequence fault */
#define	x_SBI_URDFLT	(1<<29)		/* unexpected read data fault */
#define	x_SBI_ISQFLT	(1<<28)		/* interlock sequence fault */
#define	x_SBI_MXTFLT	(1<<27)		/* multiple transmitter fault */
#define	x_SBI_XMTFLT	(1<<26)		/* transmit fault */

#define	x_NEX_CFGFLT	(0xfc000000)

#ifndef x_LOCORE
#if defined(x_VAX780) || defined(x_VAX8600)
#define	x_NEXFLT_BITS \
"\20\40PARFLT\37WSQFLT\36URDFLT\35ISQFLT\34MXTFLT\33XMTFLT"
#endif
#endif

#define	x_NEX_APD		(1<<23)		/* adaptor power down */
#define	x_NEX_APU		(1<<22)		/* adaptor power up */

#define	x_MBA_OT		(1<<21)		/* overtemperature */

#define	x_UBA_UBINIT	(1<<18)		/* unibus init */
#define	x_UBA_UBPDN	(1<<17)		/* unibus power down */
#define	x_UBA_UBIC	(1<<16)		/* unibus initialization complete */

/*
 * Types for nex_type.
 */
#define	x_NEX_ANY		0		/* pseudo for handling 11/750 */
#define	x_NEX_MEM4	0x08		/* 4K chips, non-interleaved mem */
#define	x_NEX_MEM4I	0x09		/* 4K chips, interleaved mem */
#define	x_NEX_MEM16	0x10		/* 16K chips, non-interleaved mem */
#define	x_NEX_MEM16I	0x11		/* 16K chips, interleaved mem */
#define	x_NEX_MBA		0x20		/* Massbus adaptor */
#define	x_NEX_UBA0	0x28		/* Unibus adaptor */
#define	x_NEX_UBA1	0x29		/* 4 flavours for 4 addr spaces */
#define	x_NEX_UBA2	0x2a
#define	x_NEX_UBA3	0x2b
#define	x_NEX_DR32	0x30		/* DR32 user i'face to SBI */
#define	x_NEX_CI		0x38		/* CI adaptor */
#define	x_NEX_MPM0	0x40		/* Multi-port mem */
#define	x_NEX_MPM1	0x41		/* Who knows why 4 different ones ? */
#define	x_NEX_MPM2	0x42
#define	x_NEX_MPM3	0x43
#define	x_NEX_MEM64L	0x68		/* 64K chips, non-interleaved, lower */
#define	x_NEX_MEM64LI	0x69		/* 64K chips, ext-interleaved, lower */
#define	x_NEX_MEM64U	0x6a		/* 64K chips, non-interleaved, upper */
#define	x_NEX_MEM64UI	0x6b		/* 64K chips, ext-interleaved, upper */
#define	x_NEX_MEM64I	0x6c		/* 64K chips, interleaved */
#define	x_NEX_MEM256L	0x70		/* 256K chips, non-interleaved, lower */
#define	x_NEX_MEM256LI	0x71		/* 256K chips, ext-interleaved, lower */
#define	x_NEX_MEM256U	0x72		/* 256K chips, non-interleaved, upper */
#define	x_NEX_MEM256UI	0x73		/* 256K chips, ext-interleaved, upper */
#define	x_NEX_MEM256I	0x74		/* 256K chips, interleaved */
