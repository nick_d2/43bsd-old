#include "x_.h"

/*
 *	@(#)ka630.h	7.1 (Berkeley) 6/5/86
 *
 * Definitions specific to the ka630 uvax2 cpu card. Includes the tod
 * clock chip and the cpu registers.
 */
#ifdef x_VAX630
/* Bdr register bits */
#define	x_KA630BDR_PWROK	0x8000
#define	x_KA630BDR_HLTENB	0x4000
#define	x_KA630BDR_CPU	0x0c00
#define	x_KA630BDR_BDG	0x0300
#define	x_KA630BDR_DSPL	0x000f

/* Memory system err reg. */
#define	x_KA630MSER_CD	0x00000300
#define	x_KA630MSER_NXM	0x00000080
#define	x_KA630MSER_LPE	0x00000040
#define	x_KA630MSER_QPE	0x00000020
#define	x_KA630MSER_MERR	0x000000f0
#define	x_KA630MSER_CPUER	0x00000060
#define	x_KA630MSER_DQPE	0x00000010
#define	x_KA630MSER_LEB	0x00000008
#define	x_KA630MSER_WRWP	0x00000002
#define	x_KA630MSER_PAREN	0x00000001

/* Mem. error address regs. */
#define	x_KA630CEAR_PG	0x00007fff
#define	x_KA630DEAR_PG	0x00007fff

/* Clock registers and constants */
#define	x_MINSEC	60
#define	x_HRSEC	3600

#define	x_KA630CLK_VRT	0200
#define	x_KA630CLK_UIP	0200
#define	x_KA630CLK_RATE	040
#define	x_KA630CLK_ENABLE	06
#define	x_KA630CLK_SET	0206
/* cpmbx bits */
#define	x_KA630CLK_HLTACT	03
/* halt action values */
#define	x_KA630CLK_RESTRT	01
#define	x_KA630CLK_REBOOT	02
#define	x_KA630CLK_HALT	03
/* in progress flags */
#define	x_KA630CLK_BOOT	04
#define	x_KA630CLK_RSTRT	010
#define	x_KA630CLK_LANG	0360

#ifndef x_LOCORE
struct x_cldevice {
	x_u_short	x_sec;
	x_u_short	x_secalrm;
	x_u_short	x_min;
	x_u_short	x_minalrm;
	x_u_short	x_hr;
	x_u_short	x_hralrm;
	x_u_short	x_dayofwk;
	x_u_short	x_day;
	x_u_short	x_mon;
	x_u_short	x_yr;
	x_u_short	x_csr0;
	x_u_short	x_csr1;
	x_u_short	x_csr2;
	x_u_short	x_csr3;
	x_u_short	x_cpmbx;	/* CPMBX is used by the boot rom. see ka630-ug-3.3.3 */
};

struct x_ka630cpu {
	x_u_short x_ka630_bdr;
	x_u_short x_ka630_xxx;
	x_u_long  x_ka630_mser;
	x_u_long  x_ka630_cear;
	x_u_long  x_ka630_dear;
};
#endif
#endif
