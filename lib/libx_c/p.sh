#!/bin/sh

find temp_c -name '*.c.extern' -print |\
sed -e 's:^temp_c/\(.*\)\.extern$:\1:' |\
while read i
do
  echo "i=$i" >&2
  set `head --lines=1 temp_c/$i.hfiles`
  echo "count=$1 hfile=$2" >&2
  if ! test -s temp_h/$2.append
  then
    cat <<EOF >temp_h/$2.append

#ifndef __P
#ifdef __STDC__
#define __P(args) args
#else
#define __P(args) ()
#endif
#endif

EOF
  fi
  cat temp_c/$i.extern >>temp_h/$2.append
done

find temp_h -name '*.h.append' -print |\
sed -e 's:^temp_h/\(.*\)\.append$:\1:' |\
while read i
do
  echo "i=$i" >&2
  cat temp_h/$i.append >>temp_h/$i
  funcdefs=`sed -ne 's/^.*[^0-9A-Za-z_]\([A-Za-z_][0-9A-Za-z_]*\) __P((.*$/\1/p' temp_h/$i.append`
  echo "funcdefs=$funcdefs" >&2
  find temp_c -name '*.c' -print |\
  sed -e 's:^temp_c/::' |\
  while read j
  do
    echo "j=$j" >&2
    calls=`for k in $funcdefs; do grep "^$k[	 ]*(" temp_c/$j; grep "[^0-9A-Za-z_]$k[	 ]*(" temp_c/$j; done |wc -l`
    echo "calls=$calls" >&2
    if test $calls -gt 0
    then
      count=`(grep "^#[	 ]*include[	 ]*<$i>" temp_c/$j; grep "^#[	 ]*include[	 ]*\"$i\"" temp_c/$j) |wc -l`
      echo "count=$count" >&2
      if test $count -eq 0
      then
        (
          echo "#include <$i>"
          cat temp_c/$j
        ) >a
        mv a temp_c/$j
      fi
    fi
  done
done
