#include "x_.h"

/*	sgtty.h	4.2	85/01/03	*/

#ifndef	x__IOCTL_
#include <sys/x_ioctl.h>
#endif

#ifndef __P
#ifdef __STDC__
#define __P(x_args) x_args
#else
#define __P(x_args) ()
#endif
#endif

/* compat-4.1/stty.c */
x_int x_stty __P((x_int x_fd, struct x_sgttyb *x_ap));
/* compat-4.1/gtty.c */
x_int x_gtty __P((x_int x_fd, struct x_sgttyb *x_ap));
