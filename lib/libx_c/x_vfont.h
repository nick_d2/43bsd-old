#include "x_.h"

/*
 * Copyright (c) 1980 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)vfont.h	5.1 (Berkeley) 5/30/85
 */

/*
 * The structures header and dispatch define the format of a font file.
 *
 * See vfont(5) for more details.
 */
struct x_header {
	x_short x_magic;
	x_unsigned_short x_size;
	x_short x_maxx;
	x_short x_maxy;
	x_short x_xtend;
}; 

struct x_dispatch {
	x_unsigned_short x_addr;
	x_short x_nbytes;
	char x_up,x_down,x_left,x_right;
	x_short x_width;
};
