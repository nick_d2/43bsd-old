#include <ctype.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char buf[1024];

int main() {
  char *p, *q;
  int l, m;
  int e;

  printf("#include \"x_.h\"\n\n");
  while (fgets(buf, sizeof(buf), stdin)) {
    p = buf;
    l = 0;
    if (p[l] == '#') {
      ++l;
      while (isblank(p[l]))
        ++l;
      q = p + l;
      m = 0;
      if (isalpha(q[m])) {
        ++m;
        while (isalnum(q[m]) || q[m] == '_')
          ++m;
        l += m;
        if (m == 7 && memcmp(q, "include", 7) == 0) {
          while (isblank(p[l]))
            ++l;
          if (p[l] == '<') {
            e = '>';
            goto include;
          }
          else if (p[l] == '"') {
            e = '"';
          include:
            fwrite(p, ++l, 1, stdout);
            p += l;
            l = 0;
            while (p[l]) {
              if (p[l] == '/') {
                fwrite(p, ++l, 1, stdout);
                p += l;
                l = 0;
              } 
              else if (p[l] == e) {
                if (l >= 4 && memcmp(p, "nox_", 4) == 0) {
                  p += 4;
                  l -= 4;
                }
                else if (
                  (l != 8 || memcmp(p, "stdarg.h", 8) != 0) &&
                  (l != 9 || memcmp(p, "varargs.h", 9) != 0)
                )
                  fwrite("x_", 2, 1, stdout);
                break;
              }
              else
                ++l;
            }
          }
        }
      }
      fwrite(p, l, 1, stdout);
      p += l;
      l = 0;
    }
    while (p[l]) {
      if (isalpha(p[l]) || p[l] == '_') {
        ++l;
        while (isalnum(p[l]) || p[l] == '_')
          ++l;
        if (
          (l == 8 && memcmp(p, "register", 8) == 0) ||
          (l == 6 && memcmp(p, "static", 6) == 0)
        ) {
          fwrite(p, l, 1, stdout);
          p += l;
          l = 0;
          while (isblank(p[l]))
            ++l;
          q = p + l;
          m = 0;
          if (isalpha(q[m]) || q[m] == '_') {
            ++m;
            while (isalnum(q[m]) || q[m] == '_')
              ++m;
            if (
              (m != 4 || memcmp(q, "char", 4) != 0) &&
              (m != 6 || memcmp(q, "double", 6) != 0) &&
              (m != 4 || memcmp(q, "enum", 4) != 0) &&
              (m != 5 || memcmp(q, "float", 5) != 0) &&
              (m != 3 || memcmp(q, "int", 3) != 0) &&
              (m != 8 || memcmp(q, "intptr_t", 8) != 0) &&
              (m != 4 || memcmp(q, "long", 4) != 0) &&
              (m != 5 || memcmp(q, "short", 5) != 0) &&
              (m != 6 || memcmp(q, "struct", 6) != 0) &&
              (m != 5 || memcmp(q, "union", 5) != 0) &&
              (m != 8 || memcmp(q, "unsigned", 8) != 0) &&
              (m != 7 || memcmp(q, "va_list", 7) != 0)
            ) {
              q += m;
              m = 0;
              while (isblank(q[m]))
                ++m;
              if (q[m] != '*') /* avoid something like: register FILE *fp; */
                fwrite(" x_int", 6, 1, stdout);
            }
          }
        else /* maybe something like: register *p; */
          fwrite(" x_int", 6, 1, stdout);
        }
        else if (l == 3 && memcmp(p, "int", 3) == 0) {
          fwrite("x_int", 5, 1, stdout);
          p += l;
          l = 0;
        }
        else if (
          (l == 5 && memcmp(p, "short", 5) == 0) ||
          (l == 4 && memcmp(p, "long", 4) == 0) 
        ) {
          fwrite("x_", 2, 1, stdout);
          fwrite(p, l, 1, stdout);
          p += l;
          l = 0;
          while (isblank(p[l]))
            ++l;
          q = p + l;
          m = 0;
          if (isalpha(q[m])) {
            ++m;
            while (isalnum(q[m]) || q[m] == '_')
              ++m;
            if (m == 3 && memcmp(q, "int", 3) == 0) {
              p += l + m;
              l = 0;
            }
          }
        }
        else if (l == 8 && memcmp(p, "unsigned", 8) == 0) {
          while (isblank(p[l]))
            ++l;
          q = p + l;
          m = 0;
          if (isalpha(q[m])) {
            ++m;
            while (isalnum(q[m]) || q[m] == '_')
              ++m;
            if (m == 3 && memcmp(q, "int", 3) == 0) {
              fwrite("x_unsigned_int", 14, 1, stdout);
              p += l + m;
              l = 0;
            }
            else if (
              (m == 5 && memcmp(q, "short", 5) == 0) ||
              (m == 4 && memcmp(q, "long", 4) == 0) 
            ) {
              fwrite("x_unsigned_", 11, 1, stdout);
              fwrite(q, m, 1, stdout);
              p += l + m;
              l = 0;
              while (isblank(p[l]))
                ++l;
              q = p + l;
              m = 0;
              if (isalpha(q[m])) {
                ++m;
                while (isalnum(q[m]) || q[m] == '_')
                  ++m;
                if (m == 3 && memcmp(q, "int", 3) == 0) {
                  p += l + m;
                  l = 0;
                }
              }
            }
            else if (m != 4 || memcmp(q, "char", 4) != 0) {
              fwrite("x_unsigned_int", 14, 1, stdout);
              p += 8;
              l -= 8;
            }
          }
        }
        else if (l >= 4 && memcmp(p, "nox_", 4) == 0) {
          p += 4;
          l -= 4;
        }
        else if (
          (l != 5 || memcmp(p, "break", 5) != 0) &&
          (l != 4 || memcmp(p, "case", 4) != 0) &&
          (l != 4 || memcmp(p, "char", 4) != 0) &&
          (l != 8 || memcmp(p, "continue", 8) != 0) &&
          (l != 7 || memcmp(p, "defined", 7) != 0 || buf[0] != '#') &&
          (l != 2 || memcmp(p, "do", 2) != 0) &&
          (l != 6 || memcmp(p, "double", 6) != 0) &&
          (l != 4 || memcmp(p, "else", 4) != 0) &&
          (l != 4 || memcmp(p, "enum", 4) != 0) &&
          (l != 6 || memcmp(p, "extern", 6) != 0) &&
          (l != 5 || memcmp(p, "float", 5) != 0) &&
          (l != 2 || memcmp(p, "if", 2) != 0) &&
          (l != 8 || memcmp(p, "intptr_t", 8) != 0) &&
          (l != 3 || memcmp(p, "for", 3) != 0) &&
          (l != 4 || memcmp(p, "goto", 4) != 0) &&
          (l != 6 || memcmp(p, "return", 6) != 0) &&
          (l != 8 || memcmp(p, "register", 8) != 0) &&
          (l != 6 || memcmp(p, "sizeof", 6) != 0) &&
          (l != 6 || memcmp(p, "static", 6) != 0) &&
          (l != 6 || memcmp(p, "struct", 6) != 0) &&
          (l != 6 || memcmp(p, "switch", 6) != 0) &&
          (l != 7 || memcmp(p, "typedef", 7) != 0) &&
          (l != 5 || memcmp(p, "union", 5) != 0) &&
          (l != 8 || memcmp(p, "va_alist", 8) != 0) &&
          (l != 6 || memcmp(p, "va_arg", 6) != 0) &&
          (l != 6 || memcmp(p, "va_dcl", 6) != 0) &&
          (l != 6 || memcmp(p, "va_end", 6) != 0) &&
          (l != 7 || memcmp(p, "va_list", 7) != 0) &&
          (l != 8 || memcmp(p, "va_start", 8) != 0) &&
          (l != 4 || memcmp(p, "void", 4) != 0) &&
          (l != 5 || memcmp(p, "while", 5) != 0) &&
          (l != 3 || memcmp(p, "__P", 3) != 0) &&
          (l != 8 || memcmp(p, "__FILE__", 8) != 0) &&
          (l != 8 || memcmp(p, "__LINE__", 8) != 0) &&
          (l != 8 || memcmp(p, "__STDC__", 8) != 0)
        )
          fwrite("x_", 2, 1, stdout);
      }
      else if (p[l] == '.' && isdigit(p[l + 1])) {
        l += 2;
        goto decimal;
      }
      else if (p[l] == '0' && (p[l + 1] == 'x' || p[l + 1] == 'X')) {
        l += 2;
        while (isxdigit(p[l]))
          ++l;
        goto integer;
      }
      else if (isdigit(p[l])) {
        ++l;
        while (isdigit(p[l]))
          ++l;
        if (p[l] == '.') {
          ++l;
        decimal:
          while (isdigit(p[l]))
            ++l;
          if (p[l] == 'E' || p[l] == 'e' || p[l] == 'D' || p[l] == 'd')
            goto exponent;
        }
        else if (p[l] == 'E' || p[l] == 'e' || p[l] == 'D' || p[l] == 'd') {
        exponent:
          ++l;
          if (p[l] == '+' || p[l] == '-')
            ++l;
          while (isdigit(p[l]))
            ++l;
        }
        else {
        integer:
          if (p[l] == 'U' || p[l] == 'u')
            ++l;
          if (p[l] == 'L' || p[l] == 'l')
            ++l;
        }
      }
      else if (p[l] == '\'' || p[l] == '"') {
        e = p[l++];
        while (p[l]) {
          if (p[l++] == e)
            break;
          if (p[l - 1] == '\\' && p[l])
            ++l;
        }
      }
      else if (p[l] == '/' && p[l + 1] == '*') {
        l += 2;
        while (p[l] != '*' || p[l + 1] != '/')
          if (p[l] == 0) {
            fwrite(p, l, 1, stdout);
            if (fgets(buf, sizeof(buf), stdin) == 0)
              return 0;
            p = buf;
            l = 0;
          }
          else
            ++l;
        l += 2;
      }
      else
        ++l;
      fwrite(p, l, 1, stdout);
      p += l;
      l = 0;
    }
  }
  return 0;
}
